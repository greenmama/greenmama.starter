<?php session_start(); ?>
<?php 
include("system/include.php");

/*
VERSION 2.0
USER'S MANUAL

VARIABLES
$pages_data: Data from Pages module (Example: $pages_data["pages_title"] => "Index")
$pages_link: Link of current page (Example: $pages_link => "index.php")
$page: Page for pagination of current page (Example: $page => "24")

GET DATA LIST FILTER
For passing filter into "Get Data List" function
$filters = array(
	"user_id" => "",
	"languages_short_name" => "",
);

GET DATA LIST EXTENDED COMMAND
For passing filter into "Get Data List" function
$extended_command = array(
	array(
		"conjunction" => "", //Leave empty for 1st command
		"key" => "user_name",
		"operator" => "LIKE",
		"value" => "%Creator%",
	),
	array(
		"conjunction" => "OR",
		"key" => "user_name",
		"operator" => "LIKE",
		"value" => "%Creator%",
	),
)
//command will be like "AND (`user_name` LIKE '%Michael%' OR `user_name` LIKE '%Creator%')"
*/
?>