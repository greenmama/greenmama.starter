<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "get_groups_data_by_id" 
		|| $method == "get_groups_data_all" 
		|| $method == "count_groups_data_all" 
		|| $method == "get_groups_data_all_excluding" 
		|| $method == "count_groups_data_all_excluding" 

		|| $method == "get_groups_data_table_all" 
		|| $method == "count_groups_data_table_all" 
		|| $method == "get_search_groups_data_table_all" 
		|| $method == "count_search_groups_data_table_all" 
		|| $method == "create_groups_data" 
		|| $method == "update_groups_data" 
		|| $method == "patch_groups_data" 
		|| $method == "delete_groups_data" 
		|| $method == "create_groups_table" 
		|| $method == "create_groups_table_row" 
		|| $method == "update_groups_table_row" 
		|| $method == "delete_groups_table_row" 
		|| $method == "create_groups_table_row" 
		|| $method == "update_groups_table_row" 
		|| $method == "inform_groups_table_row" 
		|| $method == "view_groups_table_row" 
		|| $method == "create_sort_groups" 
		|| $method == "sort_groups" 
		|| $method == "get_groups_edit_log_data" 
		|| $method == "count_groups_edit_log_data" 
		|| $method == "get_groups_edit_log_data_by_id" 
		|| $method == "count_groups_edit_log_data_by_id"
		
		|| $method == "get_groups_data_all_by_modules_ids" 
		|| $method == "count_groups_data_all_by_modules_ids" 
		|| $method == "get_modules_ids_data_dynamic_list" 
		|| $method == "count_modules_ids_data_dynamic_list" 
		|| $method == "get_modules_ids_data_dynamic_list_by_id")
) {
	
	/* include: configurations */
	require_once("../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	require_once("mail.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");

	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "get_groups_data_by_id"){
		get_groups_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_groups_data_all"){
		get_groups_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_groups_data_all"){
		count_groups_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_groups_data_all_excluding"){
		get_groups_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_groups_data_all_excluding"){
		count_groups_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_groups_data_table_all"){
		get_groups_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_groups_data_table_all"){
		count_groups_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_groups_data_table_all"){
		get_search_groups_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_groups_data_table_all"){
		count_search_groups_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_groups_data"){
		create_groups_data($parameters);
	} else if ($method == "update_groups_data"){
		update_groups_data($parameters);
	} else if ($method == "patch_groups_data"){
		patch_groups_data($parameters);
	} else if ($method == "delete_groups_data"){
		delete_groups_data($parameters);
	} else if ($method == "create_groups_table"){
		create_groups_table($parameters, $table_module_field); 
	} else if ($method == "create_groups_table_row"){
		create_groups_table_row($parameters, $table_module_field);
	} else if ($method == "update_groups_table_row"){
		update_groups_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_groups_table_row"){
		inform_groups_table_row($parameters, $table_module_field);
	} else if ($method == "view_groups_table_row"){
		view_groups_table_row($parameters, $table_module_field);
	} else if ($method == "create_sort_groups"){
		create_sort_groups($table_module_field);
	} else if ($method == "sort_groups"){
		sort_groups($parameters);
	} else if ($method == "get_groups_edit_log_data"){
		get_groups_edit_log_data($parameters);
	} else if ($method == "count_groups_edit_log_data"){
		count_groups_edit_log_data($parameters);
	} else if ($method == "get_groups_edit_log_data_by_id"){
		get_groups_edit_log_data_by_id($parameters);
	} else if ($method == "count_groups_edit_log_data_by_id"){
		count_groups_edit_log_data_by_id($parameters);
	} else if ($method == "get_groups_data_all_by_modules_ids"){
		get_groups_data_all_by_modules_ids($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_groups_data_all_by_modules_ids"){
		count_groups_data_all_by_modules_ids($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_ids_data_dynamic_list"){
		get_modules_ids_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_ids_data_dynamic_list"){
		count_modules_ids_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_ids_data_dynamic_list_by_id"){
		get_modules_ids_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	}
}

function get_groups_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "groups" */
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups`.`groups_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
			$query["groups_date_created_formatted"] = datetime_reformat($query["groups_date_created"]);
			
		if (isset($query["modules_ids"]) && !empty($query["modules_ids"])) {
			$query["modules_ids"] = unserialize($query["modules_ids"]);
			for ($j = 0; $j < count($query["modules_ids"]); $j++) {
				if (!empty($query["modules_ids"][$j]) && is_array($query["modules_ids"][$j])) {
					foreach($query["modules_ids"][$j] as $key => $value) {
						$query["modules_ids"][$j][] = $value;
					}
				}
			}
			$query["modules_ids_json"] = json_encode($query["modules_ids"]);
		}
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "groups_id",
					"modules_record_target" => $target,
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_data_all($start = 0, $limit = "", $sort_by = "groups_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "groups" */
	$sql = "
	SELECT *
	FROM   `groups`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_ids"]) && !empty($query["modules_ids"])) {
			$query["modules_ids"] = unserialize($query["modules_ids"]);
			for ($j = 0; $j < count($query["modules_ids"]); $j++) {
				if (!empty($query["modules_ids"][$j]) && is_array($query["modules_ids"][$j])) {
					foreach($query["modules_ids"][$j] as $key => $value) {
						$query["modules_ids"][$j][] = $value;
					}
				}
			}
			$query["modules_ids_json"] = json_encode($query["modules_ids"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "groups" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "groups_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "groups" */
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_ids"]) && !empty($query["modules_ids"])) {
			$query["modules_ids"] = unserialize($query["modules_ids"]);
			for ($j = 0; $j < count($query["modules_ids"]); $j++) {
				if (!empty($query["modules_ids"][$j]) && is_array($query["modules_ids"][$j])) {
					foreach($query["modules_ids"][$j] as $key => $value) {
						$query["modules_ids"][$j][] = $value;
					}
				}
			}
			$query["modules_ids_json"] = json_encode($query["modules_ids"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "groups_id",
					"modules_record_target" => $target,
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "groups" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `groups`
	WHERE `groups_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "groups_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "groups" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_ids"]) && !empty($query["modules_ids"])) {
			$query["modules_ids"] = unserialize($query["modules_ids"]);
			for ($j = 0; $j < count($query["modules_ids"]); $j++) {
				if (!empty($query["modules_ids"][$j]) && is_array($query["modules_ids"][$j])) {
					foreach($query["modules_ids"][$j] as $key => $value) {
						$query["modules_ids"][$j][] = $value;
					}
				}
			}
			$query["modules_ids_json"] = json_encode($query["modules_ids"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "groups" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_groups_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "groups_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'groups'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "groups_actions" 
				&& $key != "groups_date_created" 
				&& $key != "groups_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "groups" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_ids"]) && !empty($query["modules_ids"])) {
			$query["modules_ids"] = unserialize($query["modules_ids"]);
			for ($j = 0; $j < count($query["modules_ids"]); $j++) {
				if (!empty($query["modules_ids"][$j]) && is_array($query["modules_ids"][$j])) {
					foreach($query["modules_ids"][$j] as $key => $value) {
						$query["modules_ids"][$j][] = $value;
					}
				}
			}
			$query["modules_ids_json"] = json_encode($query["modules_ids"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_groups_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'groups'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "groups_actions" 
				&& $key != "groups_date_created" 
				&& $key != "groups_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "groups" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `groups_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `groups`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_groups_data($parameters){

	/* get global: configurations */
	global $configs;
	$groups_configs = get_modules_data_by_id("7");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["groups_activate"] == "1") {
	
		if(!isset($parameters["groups_name"]) || empty($parameters["groups_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is required");
			$response["target"] = "#groups_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["groups_name"]) > 100 && $parameters["groups_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#groups_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["groups_name"]) > 100 && $parameters["groups_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#groups_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}

	}

	/* initialize: default */
	$parameters["modules_ids"] = stripslashes(trim(serialize($parameters["modules_ids"])));
	date_default_timezone_set($configs["timezone"]);
	$parameters["groups_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */

	/* database: insert to module "groups" (begin) */
	$sql = "INSERT INTO `groups` (
				`groups_id`,
				`groups_name`,
				`groups_protected`,
				`modules_ids`,
				`groups_order`,
				`groups_date_created`,
				`groups_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["groups_name"] . "',
				'" . $parameters["groups_protected"] . "',
				'" . $parameters["modules_ids"] . "',
				'" . $parameters["groups_order"] . "',
				'" . $parameters["groups_date_created"] . "',
				'" . $parameters["groups_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "')";
	$result = mysqli_query($con, $sql);
	$parameters["groups_id"] = mysqli_insert_id($con);
	/* database: insert to module "groups" (end) */

	if ($result) {
		

		/* response: additional data */
		

		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "groups_id",
				"modules_record_target" => $parameters["groups_id"],
				"modules_id" => "7",
				"modules_name" => "Groups",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_groups_data($parameters){

	/* get global: configurations */
	global $configs;
	$groups_configs = get_modules_data_by_id("7");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["groups_activate"] == "1") {
	
		if(!isset($parameters["groups_name"]) || empty($parameters["groups_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is required");
			$response["target"] = "#groups_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["groups_name"]) > 100 && $parameters["groups_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#groups_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["groups_name"]) > 100 && $parameters["groups_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#groups_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}

	/* database: get old data from module "groups" (begin) */
	$sql = "SELECT *
	        FROM   `groups`
			WHERE  `groups_id` = '" . $parameters["groups_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["groups_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "groups" (end) */
	
	/* initialize: default */
	$parameters["modules_ids"] = stripslashes(trim(serialize($parameters["modules_ids"])));
	
	$parameters["groups_date_created"] = $query["groups_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	/* database: update to module "groups" (begin) */
	$sql = "UPDATE `groups`
			SET 
				   `groups_name` = '" . $parameters["groups_name"] . "',
				   `groups_protected` = '" . $parameters["groups_protected"] . "',
				   `modules_ids` = '" . $parameters["modules_ids"] . "',
				   `groups_order` = '" . $parameters["groups_order"] . "',
				   `groups_date_created` = '" . $parameters["groups_date_created"] . "',
				   `groups_activate` = '" . $parameters["groups_activate"] . "'
			WHERE  `groups_id` = '" . $parameters["groups_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "groups" (end) */

	if ($result) {
		
		/* response: additional data */
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "groups_id",
				"modules_record_target" => $parameters["groups_id"],
				"modules_id" => "7",
				"modules_name" => "Groups",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "groups";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_groups_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "groups" (begin) */
	$sql = "SELECT *
	        FROM   `groups`
			WHERE  `groups_id` = '" . $parameters["groups_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["groups_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "groups" (end) */
	
	/* initialize: default */
	$parameters["groups_date_created"] = $query["groups_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "groups" (begin) */
	$sql = "UPDATE `groups`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `groups_id` = '" . $parameters["groups_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "groups" (end) */

	if ($result) {
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "groups_id",
				"modules_record_target" => $parameters["groups_id"],
				"modules_id" => "7",
				"modules_name" => "Groups",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "groups";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_groups_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["groups_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "groups" (begin) */
		$sql = "
		DELETE FROM `groups`
		WHERE `groups_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "groups" (end) */

		if ($result) {
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "groups_id",
					"modules_record_target" => $target,
					"modules_id" => "7",
					"modules_name" => "Groups",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_sort_groups($table_module_field) {
	
	/* get global: configurations */
	global $configs;
	$groups_configs = get_modules_data_by_id("7");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);

	
	$table_module_field_unassoc = array_keys($table_module_field);
	$table_module_field_key = "groups_title";
	for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
		if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
			$table_module_field_key = $table_module_field_unassoc[$j];
			break;
		}
	}

	$data_table = get_groups_data_all("", "", "groups_order", "asc");
	
	
	$html = '';

	for($i = 0; $i < count($data_table); $i++){
		if ($data_table[$i]["groups_id"] > 0) {

			$table_row_value = render_groups_table_data($data_table[$i][$table_module_field_key]).'<br />('.$data_table[$i][$table_module_field_key].')';

			$html .= '<li id="list_'.$data_table[$i]["groups_id"].'" class="mjs-nestedSortable" style="display: list-item;">
						<div class="sort-item">
							<span>
								<div style="display: inline-table">
									<i class="si si-cursor-move"></i>
								</div>
								<div style="display: inline-table">
									'.$table_row_value.' ('.translate("ID").': '.$data_table[$i]["groups_id"].')
								</div>
							</span>
						</div>
					  </li>';

		}

	}
	
	$response["status"] = true;
	$response["message"] = translate("Successfully created sort");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function sort_groups($parameters) {
	
	/* get global: configurations */
	global $configs;
	$groups_configs = get_modules_data_by_id("7");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	
	$target_next = 1;
	
	for ($i = 0; $i <= count($parameters); $i++) {
		$target_item_id = "";
		for($s = 0; $s <= count($parameters); $s++) {
			if ($parameters[$s]["left"] == $target_next) {
				$target_item_id = $parameters[$s]["item_id"];
				$target_depth = $parameters[$s]["depth"];
				if ($target_depth == 1) {
					$target_next = $parameters[$s]["right"] + 1;
				} else {
					$target_next = $target_next + 1;
				}
				break;
			}
		}
		
		if ($target_depth != "0" && $target_depth == 1 && !empty($target_item_id)) {
			
			/* database: update to module "groups" (begin) */
			$sql = "UPDATE `groups`
					SET    `groups_order` = '" . $i . "'
					WHERE  `groups_id` = '" . $target_item_id . "'";
			$result = mysqli_query($con, $sql);
			/* database: update to module "groups" (end) */
			
		}
		
	}
		
	/* log */
	if ($configs["log"]) {
		$log = array(
			"log_name" => "sort data",
			"log_function" => __FUNCTION__,
			"log_violation" => "normal",
			"log_content_hash" => serialize($parameters),
			"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
			"log_type" => "frontend",
			"log_ip" => $_SERVER["REMOTE_ADDR"],
			"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"log_date_created" => gmdate("Y-m-d H:i:s"),
			"modules_record_key" => "",
			"modules_record_target" => "",
			"modules_id" => "7",
			"modules_name" => "Groups",
			"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
			"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
			"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
			"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
		);
		create_log_data($log);
		unset($log);
		$log = array();
	}
		
	$response["status"] = true;
	$response["message"] = translate("Successfully sorted data");
	$response["values"] = $parameters;
	unset($parameters);
	$parameters = array();
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function create_groups_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Modules")));
			} else {
				$column_name = str_replace("groups", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "groups_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "groups_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "groups_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "groups_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "groups_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . groups_name . '" data-toggle="tooltip" title="Name" data-original-title="Name">' . htmlspecialchars(strip_tags(translate("Name"))) . '</th>';
			} else if ($key == "groups_protected") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . groups_protected . '" data-toggle="tooltip" title="Protected" data-original-title="Protected">' . htmlspecialchars(strip_tags(translate("Protected"))) . '</th>';
			} else if ($key == "modules_ids") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_ids . '" data-toggle="tooltip" title="Modules" data-original-title="Modules">' . htmlspecialchars(strip_tags(translate("Modules"))) . '</th>';
			} else if ($key == "groups_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "groups_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["groups_id"].'" data-target="'.$data_table_row["groups_id"].'">';

		$html .= inform_groups_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_groups_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "groups_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "groups_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "groups_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "groups_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "groups_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "groups_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_groups_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_groups_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_groups_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_groups_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_groups_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_groups_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_groups_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_groups_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_groups_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_groups_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_groups_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_groups_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["groups_id"].'" data-target="'.$data_table["groups_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_groups_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_groups_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_groups_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "groups_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "groups_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "groups_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "groups_actions") {
				if ($key == "groups_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "groups_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["groups_name"]));
				} else if ($key == "groups_protected") {
					$td_align_class = ".text-center";
					$groups_protected_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($groups_protected_default_value[0] != "" && isset($groups_protected_default_value[0])) {
						if ($data_table["groups_protected"] == $groups_protected_default_value[0]) {
							if ($groups_protected_default_value[0] == "1") {
								$groups_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$groups_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["groups_protected"])) . '</span>';
							}
						} else {
							if ($groups_protected_default_value[0] == "1") {
								$groups_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$groups_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["groups_protected"] == "1") {
							$groups_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$groups_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $groups_protected_data_table_value;
				} else if ($key == "modules_ids") {
					$td_align_class = "";
					$con = start();
					if (count($data_table["modules_ids"]) > 0 && is_array($data_table["modules_ids"]) && $data_table["modules_ids"] != "") {
						$modules_ids_sql = "`modules_id` = '" . implode("' OR `modules_id` = '", $data_table["modules_ids"]) . "'";
						$sql_modules_ids  = "
						SELECT `modules_id`, `modules_name`
						FROM   `modules`
						WHERE  " . $modules_ids_sql;
						$result_modules_ids = mysqli_query($con, $sql_modules_ids);
						$num_modules_ids = mysqli_num_rows($result_modules_ids);
						if ($num_modules_ids > 0) {
							$module_link = false;
							unset($queries_modules_ids);
							$queries_modules_ids = array();
							$k = 0;
							while($query_modules_ids = mysqli_fetch_array($result_modules_ids)) {
								$queries_modules_ids[$k] = $query_modules_ids;
								$k++;
							}
							for($k = 0; $k < count($queries_modules_ids); $k++) {
								foreach (array_keys($queries_modules_ids[$k]) as $pages_key) {
									if ($pages_key == "pages_link" && !empty($pages_key)) {
										$module_link = true;
									}
								}
							}
							unset($queries_modules_ids_list);
							$queries_modules_ids_list = array();
							for($k = 0; $k < count($queries_modules_ids); $k++) {
								if ($module_link) {
									$queries_modules_ids_list[$k] = '<a href="'.rewrite_url($queries_modules_ids[$k]['pages_link']).'" target="_blank">' . htmlspecialchars(strip_tags($queries_modules_ids[$k]['modules_name'])) . '</a>';
								} else {
									$queries_modules_ids_list[$k] = htmlspecialchars(strip_tags($queries_modules_ids[$k]['modules_name'])) . '';
								}
							}
							$table_row_value = implode(", ", $queries_modules_ids_list);
						}
						mysqli_free_result($result_modules_ids);
					}
					stop($con);
				} else if ($key == "groups_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_short_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="users.php?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_groups_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["groups_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "groups_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}

				$html .= '
				<td id="datatable-actions-'.$data_table["groups_id"].'" class="text-center">
					<div class="btn-group">';

				$html .= '
						<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["groups_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
						<i class="fa fa-eye"></i>
						</button>';
				
				if ($_SESSION["users_level"] != "5") {

		
			
						if (!isset($data_table["groups_protected"]) || (isset($data_table["groups_protected"]) && $data_table["groups_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

						$html .= '
						<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["groups_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
						<i class="fa fa-pencil"></i>
						</button>';
						
						}

						$html .= '
						<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["groups_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
						<i class="fa fa-copy"></i>
						</button>';

						$html .= '
						<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["groups_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?">
						<i class="fa fa-times"></i>
						</button>';
			
				}

				$html .= '
					</div>
				</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_groups_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_groups_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Modules")));
			} else {
				$column_name = str_replace("groups", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "groups_actions") {
				if ($key == "groups_id") {
					if (!empty($data_table["groups_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "groups_name") {
				if ($data_table["groups_name"] != "") {
					$table_row_value = '<strong>' . translate("Name") . '</strong><br />' . $data_table["groups_name"] . '<br /><br />';
					$html .= $table_row_value;
					$i++;
				}
			} else if ($key == "groups_protected") {
				if ($data_table["groups_protected"] != "") {
					$groups_protected_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($groups_protected_default_value[0] != "" && isset($groups_protected_default_value[0])) {
						if ($data_table["groups_protected"] == $groups_protected_default_value[0]) {
							if ($groups_protected_default_value[0] == "1") {
								$groups_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$groups_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["groups_protected"])) . '</span>';
							}
						} else {
							if ($groups_protected_default_value[0] == "1") {
								$groups_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$groups_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["groups_protected"] == "1") {
							$groups_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$groups_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = '<strong>' . translate("Protected") . '</strong><br />' . $groups_protected_data_table_value . '<br /><br />';
					$html .= $table_row_value;
					$i++;
				}
			} else if ($key == "modules_ids") {
				if ($data_table["modules_ids"] != "") {
					$con = start();
					if (count($data_table["modules_ids"]) > 0 && is_array($data_table["modules_ids"]) && $data_table["modules_ids"] != "") {
						$modules_ids_sql = "`modules_id` = '" . implode("' OR `modules_id` = '", $data_table["modules_ids"]) . "'";
						$sql_modules_ids  = "
						SELECT `modules_id`, `modules_name`
						FROM   `modules`
						WHERE  " . $modules_ids_sql;
						$result_modules_ids = mysqli_query($con, $sql_modules_ids);
						$num_modules_ids = mysqli_num_rows($result_modules_ids);
						if ($num_modules_ids > 0) {
							unset($queries_modules_ids);
							$queries_modules_ids = array();
							$k = 0;
							while($query_modules_ids = mysqli_fetch_array($result_modules_ids)) {
								$queries_modules_ids[$k] = $query_modules_ids;
								$k++;
							}
							for($k = 0; $k < count($queries_modules_ids); $k++) {
								foreach (array_keys($queries_modules_ids[$k]) as $pages_key) {
									if ($pages_key == "pages_link" && !empty($pages_key)) {
										$module_link = true;
									}
								}
							}
							unset($queries_modules_ids_list);
							$queries_modules_ids_list = array();
							for($k = 0; $k < count($queries_modules_ids); $k++) {
								if ($module_link) {
									$queries_modules_ids_list[$k] = '<a href="'.rewrite_url($queries_modules_ids[$k]['pages_link']).'" target="_blank">' . htmlspecialchars(strip_tags($queries_modules_ids[$k]['modules_name'])) . '</a>';
								} else {
									$queries_modules_ids_list[$k] = htmlspecialchars(strip_tags($queries_modules_ids[$k]['modules_name']));
								}
							}
							$table_row_value = '<strong>' . translate("Modules") . '</strong><br />' . implode(", ", $queries_modules_ids_list) . '<br /><br />';
						}
						mysqli_free_result($result_modules_ids);
					}
					stop($con);
					$html .= $table_row_value;
					$i++;
				}
			} else if ($key == "groups_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "groups_date_created") {
					if (!empty($data_table["groups_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["groups_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_groups_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_groups_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_groups_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_groups_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_groups_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	$file_check = curl_init($data_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_groups_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "groups" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '7'
	AND (`log_function` = 'create_groups_data' OR `log_function` = 'update_groups_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "groups" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '7'
	AND (`log_function` = 'create_groups_data' OR `log_function` = 'update_groups_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "groups" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["modules_ids"]) && !empty($query_log_content_hash["modules_ids"])) {
			
			for ($j = 0; $j < count($query_log_content_hash["modules_ids"]); $j++) {
				if (!empty($query_log_content_hash["modules_ids"][$j]) && is_array($query_log_content_hash["modules_ids"][$j])) {
					foreach($query_log_content_hash["modules_ids"][$j] as $key => $value) {
						$query_log_content_hash["modules_ids"][$j][] = $value;
					}
				}
			}
			$query_log_content_hash["modules_ids_json"] = json_encode($query_log_content_hash["modules_ids"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "groups" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_data_all_by_modules_ids($target, $start = 0, $limit = "", $sort_by = "groups_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	$target_serialized = serialize($target);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `groups`
	WHERE `modules_ids` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_data_all_by_modules_ids($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	$target_serialized = serialize($target);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `groups_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `groups_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "groups" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `modules_ids` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `groups`
	WHERE `modules_ids` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_ids_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_ids_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_ids_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_ids_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}


function get_modules_ids_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` = '".$target."'
	".$extra_sql_data_dynamic_list."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["modules_ids_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_ids_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "groups";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>groups</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}
?>