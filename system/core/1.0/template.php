<?php mb_internal_encoding($configs["encoding"]); ?>
<?php
function menu_active($menu, $target){
	if($menu == $target){
		$active = ' class="active"';
	} else { 
		$active = '';
	}
	return $active;
}

function menu_open($menu, $target){
	if($menu == $target){
		$open = ' class="open"';
	} else { 
		$open = '';
	}
	return $open;
}
?>