<?php 

function translate ($text) {
	
	global $configs;
	global $translations;
	
	if ($configs["translate"]) {
		if (isset($translations) && isset($translations[$configs["language"]])) {

			$translation_lower = array();
			foreach ($translations as $key => $value) {
				foreach ($value as $key_inside => $value_inside) {
					$translation_lower[$key][strtolower($key_inside)] = $value_inside;
				}
			}
	
			if (isset($translation_lower[$configs["language"]][strtolower($text)]) && $translation_lower[$configs["language"]][strtolower($text)] != "") {
				return $translation_lower[$configs["language"]][strtolower($text)];
			} else {
				return $text;
			}
	
			unset($translation_lower);
			
		} else {
			
			return $text;
			
		}
	} else {
		return $text;
	}
	
	
}

function translate_value ($text) {
	
	global $configs;
	global $translations;
	
    if ($configs["translate_value"]) {
        if (isset($translations) && isset($translations[$configs["language"]])) {
            $translation_lower = array();
            foreach ($translations as $key => $value) {
                foreach ($value as $key_inside => $value_inside) {
                    $translation_lower[$key][strtolower($key_inside)] = $value_inside;
                }
            }

            if (isset($translation_lower[$configs["language"]][strtolower($text)]) && $translation_lower[$configs["language"]][strtolower($text)] != "") {
                return $translation_lower[$configs["language"]][strtolower($text)]." (".$text.")";
            } else {
                return $text;
            }

            unset($translation_lower);
        } else {
            return $text;
        }
    } else {
		return $text;
	}
	
}
?>