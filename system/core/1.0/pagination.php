<?php mb_internal_encoding('UTF-8');

//1 setup the right var when you call the function
//2 modify the while with the right data name
//3 modify the css blocs to display the right data



function pagination_test_first($string){
	//test if we are in the first page of the pagination
	$array=explode(',',$string);
	
	if($array[0]=='0') {return true;}
	else return $false;
	
	}





function initPagination($table,$order,$searchdirection){
	
$link=start();	


// $searchdirection DESC or ASC

// Paramétrage de la requête (ne pas modifier le nom des variable)
//$table = "newsN"; // Table à sélectionner dans la base

//$champ = "idN"; Field to display for testing the script 
//Champ de la table à afficher pour tester ce script


$sql = "SELECT * FROM $table ORDER BY $order $searchdirection"; // Requête initiale (à compléter si nécessaire)

$parpage = 10; // Nombre d'enregistrements par page à afficher	
	
//==============================================================================
// Déclaration et initialisation des variables (ici ne rien modifier)
//==============================================================================

// On définit le suffixe du lien url qui affichera les pages
// $_SERVEUR['PHP_SELF'] donne l'arborescence de la page courante
$url = $_SERVER['PHP_SELF']."?limit=";

$total = mysqli_query($link,$sql); // Résultat total de la requête $sql
$nblignes = mysqli_num_rows($total); // Nbre total d'enregistrements

// On calcule le nombre de pages à afficher en arrondissant
// le résultat au nombre supérieur grâce à la fonction ceil()
$nbpages = ceil($nblignes/$parpage);	
	
	
//echo "<p>La table <b>".$table."</b> compte ".$nblignes." <b>".$champ."</b>.";
//echo "<br />\n"."On affiche <b>".$parpage." enregistrements</b> par page, ";
//echo "soit un total de <b>".$nbpages." pages</b>.</p>\n";

// Si une valeur 'limit' est passée par url, on vérifie la validité de
// cette valeur par mesure de sécurité avec la fonction validlimit()
// cette fonction retourne automatiquement le résultat de la requête
$result = validlimit($nblignes,$parpage,$sql);

// On affiche le résultat de la requête
// On crée donc ici son propre tableau pour lequel on souhaite une pagination

$i=0;




while ($row = mysqli_fetch_array($result)) {
  //echo $row[$champ]."<br />\n";
  
$listeActu[$i]=array(
'idN' => $row['idN'],
'titreN' => $row['titreN'],
'dateN' => $row['dateN'],
'contenuN' => $row['contenuN'],
'picN' => $row['picN'],
'altN' => $row['altN']);
$i++; 
}



mysqli_free_result($result);
stop($link);
	
	
	
	
$compteur=count($listeActu);

$conca;
$ct='';
for($i=0;$i<$compteur;$i++){
	//<!--<p class="style15">-->
	
	
	
	
$conca=' <div class="containerActu">    
   
	<div class="titreH2">
	  <h2>'.$listeActu[$i]['dateN'].'</h2> 
    	<h1> - '.$listeActu[$i]['titreN'].'</h1>   
    </div>
   
   <img class="picDes" src="images/actualites/'.$listeActu[$i]['picN'].'" width="220" height="220" alt="'.$listeActu[$i]['altN'].'" />	
   
<!-- description -->
  	<p class="description">


	'.$listeActu[$i]['contenuN'].'
	
      </p>
<!-- fin description --> 
	<br class="clearfloat" />

</div><!-- fin container-->'."\n";	
	
$ct.=$conca;	
}	


$pagination='<div class="link-pages">'.pagination($url,$parpage,$nblignes,$nbpages).'</div>';	

return $ct.$pagination;	
}




	function pagination($url,$parpage,$nblignes,$nbpages){
		
		

		
		
  // On crée le code html pour la pagination
  $html = precedent($url,$parpage,$nblignes); // On crée le lien precedent
  // On vérifie que l'on a plus d'une page à afficher
  if ($nbpages > 1) {
    // On boucle sur les numéros de pages à afficher
    for ($i = 0 ; $i < $nbpages ; ++$i) {
      $limit = $i * $parpage; // On calcule le début de la valeur 'limit'
      $limit = $limit.",".$parpage; // On fait une concaténation avec $parpage
      // On affiche les liens des numéros de pages
	  
	  
	  // active the active page tag
	  if($limit == $_GET['limit']){$html .= '<a class="active" href="'.$url.$limit.'">'.($i + 1).'</a> ';}
	  else $html .= '<a href="'.$url.$limit.'">'.($i + 1).'</a> ';
     // $html .= '<a href="'.$url.$limit.'">'.($i + 1).'</a> ';
    }
  }
  // Si l'on a qu'une page on affiche rien
  else {
    $html .= "";
  }
  $html .= suivant($url,$parpage,$nblignes); // On crée le lien suivant
  // On retourne le code html
  return $html;
}

	//check if the get limit is here
	function validlimit($nblignes,$parpage,$sql){
  // On vérifie l'existence de la variable $_GET['limit']
  // $limit correspond à la clause LIMIT que l'on ajoute à la requête $sql
  
  $link=start();
  
  if (isset($_GET['limit'])) { 
    $pointer = split('[,]', $_GET['limit']); // On scinde $_GET['limit'] en 2
    $debut = $pointer[0];
    $fin = $pointer[1];
    // On vérifie la conformité de la variable $_GET['limit']
    if (($debut >= 0) && ($debut < $nblignes) && ($fin == $parpage)) {
      // Si $_GET['limit'] est valide on lance la requête pour afficher la page
      $limit = $_GET['limit']; // On récupère la valeur 'limit' passée par url
      $sql .= " LIMIT ".$limit.";"; // On ajoute $limit à la requête $sql
      $result = mysqli_query($link,$sql); // Nouveau résultat de la requête
    }
    // Sinon on affiche la première page
    else {
      $sql .= " LIMIT 0,".$parpage.";"; // On ajoute la valeur LIMIT à la requête
      $result = mysqli_query($link,$sql); // Nouveau résultat de la requête
    }
  }
  // Si la valeur 'limit' n'est pas connue, on affiche la première page
  else {
    $sql .= " LIMIT 0,".$parpage.";"; // On ajoute la valeur LIMIT à la requête
    $result = mysqli_query($link,$sql); // Nouveau résultat de la requête
  }
  // On retourne le résultat de la requête
  
  stop($link);
  return $result;
}

	// previous function
	function precedent($url,$parpage,$nblignes){
  // On vérifie qu'il y a au moins 2 pages à afficher
  if ($nblignes > $parpage) {
    // On vérifie l'existence de la variable $_GET['limit']
    if (isset($_GET['limit'])) {
      // On scinde la variable 'limit' en utilisant la virgule comme séparateur
      $pointer = split('[,]', $_GET['limit']);
      // On récupère le nombre avant la virgule et on soustrait la valeur $parpage
      $pointer = $pointer[0]-$parpage;
      // Si on atteint la première page, pas besoin de lien 'Précédent'
      if ($pointer < 0) {
        $precedent = "";
      }
      // Sinon on affiche le lien avec l'url de la page précédente
      else {
        $limit = "$pointer,$parpage";
        $precedent = '<a href="'.$url.$limit.'" class="prev"><i class="fa fa-angle-left"></i></a> ';
      }
    }
    else {
      $precedent = ""; // On est à la première page, pas besoin de lien 'Précédent'
    }
  }
  else {
  $precedent = ""; // On a qu'une page, pas besoin de lien 'Précédent'
  }
  return $precedent;
}

	// next function
	function suivant($url,$parpage,$nblignes){
  // On vérifie qu'il y a au moins 2 pages à afficher
  if ($nblignes > $parpage) {
    // On vérifie l'existence de la variable $_GET['limit']
    if (isset($_GET['limit'])) {
      // On scinde la variable 'limit' en utilisant la virgule comme séparateur
      $pointer = split('[,]', $_GET['limit']);
      // On récupère le nombre avant la virgule auquel on ajoute la valeur $parpage
      $pointer = $pointer[0] + $parpage;
      // Si on atteint la dernière page, pas besoin de lien 'Suivant'
      if ($pointer >= $nblignes) {
        $suivant = "";
      }
      // Sinon on affiche le lien avec l'url de la page suivante
      else {
        $limit = "$pointer,$parpage";
        $suivant = '<a class="next" href="'.$url.$limit.'"><i class="fa fa-angle-right"></i></a>';
      }
    }
    // Si pas de valeur 'limit' on affiche le lien de la deuxième page
    if (@$_GET['limit']== false) {
      $suivant = '<a class="next" href="'.$url.$parpage.'","'.$parpage.'"><i class="fa fa-angle-right"></i></a>';
    }
  }
  else {
  $suivant = ""; // On a qu'une page, pas besoin de lien 'Suivant'
  }
  return $suivant;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function paginate_sql ($sql,$limit) {

	global $configs;
	
	$con=start();
	
	if (isset($configs['page_limit']) && !empty($configs['page_limit']) && $configs['page_limit'] != 0) {
		$page = mysqli_real_escape_string($con,$_GET[$configs['page_parameter_name']]);
		if (!isset($page) || empty($page)) {
			$page = 1;
		}
    if($limit == ''){
		$page_limit = $configs['page_limit'];
    }else {
      $page_limit = $limit;
    }
		$limit_start = ($page_limit * ($page - 1));
		$sql_limit_extension = ' LIMIT '.$limit_start.', '.$page_limit;
		
		return $sql.$sql_limit_extension;
	} else {
		return $sql;
	}
	
	stop($con);
	
}

function get_pagination ($result,$limit) {
	
	global $configs;
	
	$con=start();
	
	$num = mysqli_num_rows($result);
	
	if (isset($configs['page_limit']) && !empty($configs['page_limit']) && $configs['page_limit'] != 0) {
		$page = mysqli_real_escape_string($con,$_GET[$configs['page_parameter_name']]);
		if (!isset($page) || empty($page)) {
			$page = 1;
		}
		if($limit == ''){
    $page_limit = $configs['page_limit'];
    }else {
      $page_limit = $limit;
    }
		$limit_start = ($page_limit * ($page - 1));
		
		$response['page_number'] = ceil($num/$page_limit);
		$response['page_current'] = $page;
		if ($page >= $response['page_number']) {
			$response['page_next'] = false;
		} else {
			$response['page_next'] = true;
		}
		if ($page <= 1) {
			$response['page_prev'] = false;
		} else {
			$response['page_prev'] = true;
		}
		
		return $response;
	}
	
	stop($con);
}



function paginate_sql_search($sql) {
  
  global $configs;
  
  $con=start();
  
  if (isset($configs['page_limit']) && !empty($configs['page_limit']) && $configs['page_limit'] != 0) {
    $page = mysqli_real_escape_string($con,$_GET['หน้า']);
    if (!isset($page) || empty($page)) {
      $page = 1;
    }
    $page_limit = $configs['page_limit'];
    $limit_start = ($page_limit * ($page - 1));
    $sql_limit_extension = ' LIMIT '.$limit_start.', '.$page_limit;
    
    return $sql.$sql_limit_extension;
  } else {
    return $sql;
  }
  
  stop($con);
  
}

function get_pagination_search($result) {
  
  global $configs;
  
  $con=start();
  
  $num = mysqli_num_rows($result);
  
  if (isset($configs['page_limit']) && !empty($configs['page_limit']) && $configs['page_limit'] != 0) {
    $page = mysqli_real_escape_string($con,$_GET['หน้า']);
    if (!isset($page) || empty($page)) {
      $page = 1;
    }
    $page_limit = $configs['page_limit'];
    $limit_start = ($page_limit * ($page - 1));
    
    $response['page_number'] = ceil($num/$page_limit);
    $response['page_current'] = $page;
    if ($page >= $response['page_number']) {
      $response['page_next'] = false;
    } else {
      $response['page_next'] = true;
    }
    if ($page <= 1) {
      $response['page_prev'] = false;
    } else {
      $response['page_prev'] = true;
    }
    
    return $response;
  }
  
  stop($con);
}
?>