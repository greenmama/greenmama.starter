<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}

if ($_SERVER['REMOTE_ADDR'] == '203.146.127.115' && isset($_GET['request'])) {
	$method = "tmtopup_update";
} else if (isset($_GET["transaction_id"])
	&& isset($_GET["password"])
	&& isset($_GET["real_amount"])
	&& isset($_GET["status"])) {
	$method = "tmpay_update";
}

if (isset($method) && $method != "" 
	&&  ($method == "omise_creditcard_submit"
		|| $method == "omise_internetbanking_submit"
		|| $method == "omise_alipay_submit"
		|| $method == "tmwallet_submit"
		|| $method == "tmtopup_submit"
		|| $method == "tmtopup_update"
		|| $method == "tmpay_submit"
		|| $method == "tmpay_update"
		|| $method == "tmpay_check")) {

	/* include: configurations */
	require_once("../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");
	

	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);
	
	if ($method == "omise_creditcard_submit"){
		omise_creditcard_submit($parameters);
	} else if ($method == "omise_internetbanking_submit"){
		omise_internetbanking_submit($parameters);
	} else if ($method == "omise_alipay_submit"){
		omise_alipay_submit($parameters);
	} else if ($method == "tmwallet_submit"){
		tmwallet_submit($_POST);
	} else if ($method == "tmtopup_submit") {
		tmtopup_submit($_POST);
	} else if ($method == "tmtopup_update") {
		tmtopup_update($_GET["request"]);
	} else if ($method == "tmpay_submit") {
		tmpay_submit($_POST);
	} else if ($method == "tmpay_update") {
		tmpay_update($_GET["transaction_id"], $_GET["password"], $_GET["real_amount"], $_GET["status"]);
	} else if ($method == "tmpay_check") {
		tmpay_check($parameters);
	}
	
}

function omise_creditcard_submit ($parameters) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	if (file_exists("../../../plugins/omise-php/lib/Omise.php") 
		&& file_exists("../../../plugins/omise-php/config.php")) {
		require_once("../../../plugins/omise-php/lib/Omise.php");
		require_once("../../../plugins/omise-php/config.php");
	} else {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("needs") . "<strong>" . translate("Omise") . "</strong>" .  translate("library");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

    $con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	define('OMISE_API_VERSION', $omise_configs["api_version"]);
	define('OMISE_PUBLIC_KEY', $omise_configs["public_key"]);
	define('OMISE_SECRET_KEY', $omise_configs["secret_key"]);

	if(!isset($omise_configs["api_version"]) || empty($omise_configs["api_version"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("OMISE Api Version") . "</strong> " . translate("is required");
		$response["target"] = "#api_version";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	if(!isset($omise_configs["public_key"]) || empty($omise_configs["public_key"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("OMISE Public Key") . "</strong> " . translate("is required");
		$response["target"] = "#public_key";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	if(!isset($omise_configs["secret_key"]) || empty($omise_configs["secret_key"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("OMISE Secret Key") . "</strong> " . translate("is required");
		$response["target"] = "#secret_key";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["card"]) || empty($parameters["card"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Card Token") . "</strong> " . translate("is required");
		$response["target"] = "#card";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["currency"]) || empty($parameters["currency"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Currency") . "</strong> " . translate("is required");
		$response["target"] = "#currency";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	if(!isset($parameters["amount"]) || empty($parameters["amount"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Amount") . "</strong> " . translate("is required");
		$response["target"] = "#amount";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	$parameters["omise_amount"] = $parameters["amount"] * 100;
	
	$charge = OmiseCharge::create(array(
		'amount' => $parameters["omise_amount"],
		'currency' => $parameters["currency"],
		'card' => $parameters["card"],
		'description' => $parameters["description"]
	));
	
	if ($charge['status'] == "successful") {

		if (isset($configs["vat_amount"]) && !empty($configs["vat_amount"])) {
			$parameters["amount_total"] = $parameters["amount"] - (($configs["vat_amount"] * $parameters["amount"]) / 100);
		} else {
			$parameters["amount_total"] = $parameters["amount"];
		}
		$parameters["users_id"] = $_SESSION["users_id"];
		$parameters["charge_id"] = $charge['id'];
		
		if (file_exists("../../transactions.php")) {
			require_once("../../transactions.php");

			$transactions_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "add",
				"transactions_amount" => $parameters["amount"],
				"transactions_vat_amount" => $configs["vat_amount"],
				"transactions_amount_total" => $parameters["amount_total"],
				"transactions_currency" => $parameters["currency"],
				"transactions_payment_method" => "creditcard",
				"transactions_payment_gateway" => "omise",
				"transactions_charge_id" => $parameters["charge_id"],
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_parameters);
			
			$parameters["omise_commission_amount"] = $parameters["amount"] * ($omise_configs["fee"]/100);
			$parameters["omise_vat_amount"] = $parameters["omise_commission_amount"] * ($omise_configs["fee_vat_amount"]/100);
			$parameters["omise_commission_amount_total"] = $parameters["omise_commission_amount"] + $parameters["omise_vat_amount"];

			$transactions_commission_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "remove",
				"transactions_amount" => $parameters["omise_commission_amount"],
				"transactions_vat_amount" => $parameters["omise_vat_amount"],
				"transactions_amount_total" => $parameters["omise_commission_amount_total"],
				"transactions_currency" => $parameters["currency"],
				"transactions_payment_method" => "system",
				"transactions_payment_gateway" => "omise",
				"transactions_charge_id" => "",
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_commission_parameters);
		}
		
		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "pay",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "",
				"modules_record_target" => "",
				"modules_id" => "",
				"modules_name" => "",
				"users_id" => $_SESSION["users_id"],
				"users_username" => $_SESSION["users_username"],
				"users_name" => $_SESSION["users_name"],
				"users_last_name" => $_SESSION["users_last_name"]
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}
		
		
		$response["status"] = true;
		$response["message"] = translate("Successfully paid");
		$response["values"] = $parameters;
		
	} else {

		$response["status"] = false;
		$response["message"] = translate("Payment encountered error");

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function tmwallet_submit ($parameters) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	if (file_exists("../../../plugins/tmwallet/common.php") 
		&& file_exists("../../../plugins/tmwallet/config.php")) {
		require_once("../../../plugins/tmwallet/common.php");
		require_once("../../../plugins/tmwallet/config.php");
	} else {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("needs") . "<strong>" . translate("tmwallet") . "</strong>" .  translate("library");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

    $con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	
	
	if(empty($parameters["tmwallet_number"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("TrueWallet Number") . "</strong> " . translate("is required");
		$response["target"] = "#tmwallet_number";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (strlen($parameters["tmwallet_number"]) < 14){
		
		$response["status"] = false;
		$response["message"] = "<strong>" . translate("TrueWallet Number") . "</strong> " . translate("needs 14 length");
		$response["target"] = "#tmwallet_number";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
		
	}
	
	$returnserver = tmtopupconnect($tmwallet_configs["api_user"],$tmwallet_configs["api_password"],$tmwallet_configs["truewallet_email"],$tmwallet_configs["truewallet_password"],my_ip(),$parameters["tmwallet_session"],$parameters["tmwallet_number"],"yes",$_SESSION["users_id"]);

	
	if (substr($returnserver,0,2) == "ok") {
		
		$money_total = intval(substr($returnserver, 2));
		$parameters["amount"] = $money_total;
		if (isset($configs["vat_amount"]) && !empty($configs["vat_amount"])) {
			$parameters["amount_total"] = $parameters["amount"] - (($configs["vat_amount"] * $parameters["amount"]) / 100);
		} else {
			$parameters["amount_total"] = $parameters["amount"];
		}
		$parameters["users_id"] = $_SESSION["users_id"];
		$parameters["charge_id"] = $returnserver;
		
		if (file_exists("../../transactions.php")) {
			require_once("../../transactions.php");

			$tmwallet_currency = "THB";

			$transactions_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "add",
				"transactions_amount" => $parameters["amount"],
				"transactions_vat_amount" => $configs["vat_amount"],
				"transactions_amount_total" => $parameters["amount_total"],
				"transactions_currency" => $tmwallet_currency,
				"transactions_payment_method" => "internet_banking",
				"transactions_payment_gateway" => "tmwallet",
				"transactions_charge_id" => $parameters["charge_id"],
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_parameters);

			$tmwallet_commission_amount = $tmwallet_configs["fee"];

			$transactions_commission_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "remove",
				"transactions_amount" => $tmwallet_commission_amount,
				"transactions_vat_amount" => 0,
				"transactions_amount_total" => $tmwallet_commission_amount,
				"transactions_currency" => $tmwallet_currency,
				"transactions_payment_method" => "system",
				"transactions_payment_gateway" => "tmwallet",
				"transactions_charge_id" => "",
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_commission_parameters);
		}
		
		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "pay",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "",
				"modules_record_target" => "",
				"modules_id" => "",
				"modules_name" => "",
				"users_id" => $_SESSION["users_id"],
				"users_username" => $_SESSION["users_username"],
				"users_name" => $_SESSION["users_name"],
				"users_last_name" => $_SESSION["users_last_name"]
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}
		
		
		$response["status"] = true;
		$response["message"] = translate("Successfully paid");
		$response["values"] = $parameters;
		
	} else {

		$response["status"] = false;
		$response["message"] = translate("Payment encountered error");

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function tmtopup_update ($request) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (file_exists("../../../plugins/tmtopup/AES.php") 
		&& file_exists("../../../plugins/tmtopup/config.php")) {
		require_once("../../../plugins/tmtopup/AES.php");
		require_once("../../../plugins/tmtopup/config.php");
	} else {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("needs") . "<strong>" . translate("tmtopup") . "</strong>" .  translate("library");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	$con = start();
	
	$parameters = array();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$request = escape_string($con, $request);

	$aes = new Crypt_AES();
	$aes->setKey($tmtopup_configs["api_passkey"]);
	$request = base64_decode(strtr($request, '-_,', '+/='));
	$request = $aes->decrypt($request);
	if($request != false) {
		parse_str($request, $request);
		$request['Ref1'] = base64_decode($request['Ref1']);

		$parameters["amount"] = $request['cardcard_amount'];
		if (isset($configs["vat_amount"]) && !empty($configs["vat_amount"])) {
			$parameters["amount_total"] = $parameters["amount"] - (($configs["vat_amount"] * $parameters["amount"]) / 100);
		} else {
			$parameters["amount_total"] = $parameters["amount"];
		}
		$parameters["users_id"] = $_SESSION["users_id"];
		$parameters["charge_id"] = $request['Ref1'];

		
		if (file_exists("../../transactions.php")) {
			require_once("../../transactions.php");
			
			$tmtopup_currency = "THB";

			$transactions_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "add",
				"transactions_amount" => $parameters["amount"],
				"transactions_vat_amount" => $configs["vat_amount"],
				"transactions_amount_total" => $parameters["amount_total"],
				"transactions_currency" => $tmtopup_currency,
				"transactions_payment_method" => "internet_banking",
				"transactions_payment_gateway" => "tmtopup",
				"transactions_charge_id" => $parameters["charge_id"],
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_parameters);

			$tmtopup_commission_amount = $parameters["amount"] - (($tmtopup_configs["fee"] * $parameters["amount"])/100);

			$transactions_commission_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "remove",
				"transactions_amount" => $tmtopup_commission_amount,
				"transactions_vat_amount" => 0,
				"transactions_amount_total" => $tmtopup_commission_amount,
				"transactions_currency" => $tmtopup_currency,
				"transactions_payment_method" => "system",
				"transactions_payment_gateway" => "tmtopup",
				"transactions_charge_id" => "",
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_commission_parameters);
		}
		
		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "pay",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "",
				"modules_record_target" => "",
				"modules_id" => "",
				"modules_name" => "",
				"users_id" => $_SESSION["users_id"],
				"users_username" => $_SESSION["users_username"],
				"users_name" => $_SESSION["users_name"],
				"users_last_name" => $_SESSION["users_last_name"]
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}
		
		$response["status"] = true;
		$response["message"] = translate("Successfully paid");
		$response["values"] = $parameters;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Payment encountered error");

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function tmpay_submit ($parameters) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	
	if (file_exists("../../../plugins/tmpay/config.php")) {
		require_once("../../../plugins/tmpay/config.php");
	} else {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("needs") . "<strong>" . translate("tmpay") . "</strong>" .  translate("library");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

    $con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	$parameters["users_id"] = $_SESSION["users_id"];
	
	if(empty($parameters["tmpay_number"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("TrueWallet Number") . "</strong> " . translate("is required");
		$response["target"] = "#tmpay_number";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (strlen($parameters["tmpay_number"]) < 14){
		
		$response["status"] = false;
		$response["message"] = "<strong>" . translate("TrueMoney Pay Number") . "</strong> " . translate("needs 14 length");
		$response["target"] = "#tmpay_number";
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
		
	}
	
	$curl = curl_init('https://www.tmpay.net/TPG/backend.php?merchant_id=' . $tmpay_configs["merchant_id"] . '&password=' . $parameters["tmpay_number"] . '&resp_url=' . $tmpay_configs["resp_url"]."?users_id=".$_SESSION["users_id"]);
	curl_setopt($curl, CURLOPT_TIMEOUT, 10);
	curl_setopt($curl, CURLOPT_HEADER, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	$curl_content = curl_exec($curl);
	curl_close($curl);
	
	if (strpos($curl_content, 'SUCCEED') !== FALSE) {
		
		$response["status"] = true;
		$response["message"] = translate("Successfully sent");
		$response["values"] = $parameters;
		
	} else {

		$response["status"] = false;
		$response["message"] = translate(ucfirst(strtolower(str_replace("_", " ", str_replace("ERROR|", "", $curl_content)))));
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function tmpay_update ($transaction_id, $password, $amount, $status) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	
	if (file_exists("../../../plugins/tmpay/config.php")) {
		require_once("../../../plugins/tmpay/config.php");
	} else {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("needs") . "<strong>" . translate("tmpay") . "</strong>" .  translate("library");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	$con = start();
	
	$parameters = array();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$transaction_id = escape_string($con, $transaction_id);
	$password = escape_string($con, $password);
	$amount = escape_string($con, $amount);
	$status = escape_string($con, $status);

	
	if ($status == 1) {

		if (round($amount) != 150 && round($amount) > 150 && round($amount) < 300) {
			$amount = 150;
		} else if (round($amount) != 300 && round($amount) > 300 && round($amount) < 500) {
			$amount = 300;
		} else if (round($amount) != 500 && round($amount) > 500) {
			$amount = 500;
		}
		
		$parameters["amount"] = round($amount);
		if (isset($configs["vat_amount"]) && !empty($configs["vat_amount"])) {
			$parameters["amount_total"] = $parameters["amount"] - (($configs["vat_amount"] * $parameters["amount"]) / 100);
		} else {
			$parameters["amount_total"] = $parameters["amount"];
		}
		$parameters["users_id"] = $_SESSION["users_id"];
		$parameters["charge_id"] = $transaction_id;
		
		if (file_exists("../../transactions.php")) {
			require_once("../../transactions.php");

			$tmpay_currency = "THB";

			$transactions_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "add",
				"transactions_amount" => $parameters["amount"],
				"transactions_vat_amount" => $configs["vat_amount"],
				"transactions_amount_total" => $parameters["amount_total"],
				"transactions_currency" => $tmpay_currency,
				"transactions_payment_method" => "internet_banking",
				"transactions_payment_gateway" => "tmpay",
				"transactions_charge_id" => $parameters["charge_id"],
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_parameters);

			$tmpay_commission_amount = $parameters["amount"] - (($tmpay_configs["fee"] * $parameters["amount"])/100);

			$transactions_commission_parameters = array(
				"transactions_user" => $_SESSION["users_id"],
				"transactions_type" => "remove",
				"transactions_amount" => $tmpay_commission_amount,
				"transactions_vat_amount" => 0,
				"transactions_amount_total" => $tmpay_commission_amount,
				"transactions_currency" => $tmpay_currency,
				"transactions_payment_method" => "system",
				"transactions_payment_gateway" => "tmpay",
				"transactions_charge_id" => "",
				"transactions_status" => "success",
				"transactions_activate" => 1,
			);
			create_transactions_data($transactions_commission_parameters);
		}
		
		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "pay",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "",
				"modules_record_target" => "",
				"modules_id" => "",
				"modules_name" => "",
				"users_id" => $_SESSION["users_id"],
				"users_username" => $_SESSION["users_username"],
				"users_name" => $_SESSION["users_name"],
				"users_last_name" => $_SESSION["users_last_name"]
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}
		
		
		$response["status"] = true;
		$response["message"] = translate("Successfully paid");
		$response["values"] = $parameters;
		
	} else {

		$response["status"] = false;
		$response["message"] = translate("Payment encountered error");

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function tmpay_check ($users_id) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	
	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	$con = start();
	
	$parameters = array();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$users_id = escape_string($con, $users_id);

		
	if (file_exists("../../transactions.php")) {
		require_once("../../transactions.php");

		$transactions_filter = array(
			"transactions_user" => $users_id,
			"transactions_type" => "add",
			"transactions_payment_gateway" => "tmpay"
		);
		$transactions = get_transactions_data_all(0, 1, "", "", 1, $transactions_filter);

		if (count($transactions) > 0) {

			if ($transactions[0]["transactions_status"] == "success") {
				$response["status"] = true;
				$response["message"] = translate($transactions[0]["transactions_status"]);
				$response["values"] = $transactions;
			} else {
				$response["status"] = false;
				$response["message"] = translate($transactions[0]["transactions_status"]);
				$response["values"] = $transactions;
			}

		} else {

			$response["status"] = false;
			$response["status_code"] = "404";
			$response["message"] = translate("Empty data");

		}


	} else {

		$response["status"] = false;
		$response["message"] = translate("Unable to check");

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}
?>