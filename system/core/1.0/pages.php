<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "get_pages_data_by_id" 
		|| $method == "get_pages_data_all" 
		|| $method == "count_pages_data_all" 
		|| $method == "get_pages_data_all_excluding" 
		|| $method == "count_pages_data_all_excluding" 
		|| $method == "get_pages_main_language_data_all" 
		|| $method == "count_pages_main_language_data_all" 
		|| $method == "get_pages_main_language_data_all_excluding" 
		|| $method == "count_pages_main_language_data_all_excluding" 
		|| $method == "get_pages_by_languages_id_data_all" 
		|| $method == "count_pages_by_languages_id_data_all" 
		|| $method == "get_pages_by_languages_short_name_data_all" 
		|| $method == "count_pages_by_languages_short_name_data_all" 
		|| $method == "get_pages_translation_data_all" 
		|| $method == "count_pages_translation_data_all" 
		|| $method == "get_pages_data_table_all" 
		|| $method == "count_pages_data_table_all" 
		|| $method == "get_search_pages_data_table_all" 
		|| $method == "count_search_pages_data_table_all" 
		|| $method == "create_pages_data" 
		|| $method == "update_pages_data" 
		|| $method == "patch_pages_data" 
		|| $method == "delete_pages_data" 
		|| $method == "create_pages_table" 
		|| $method == "create_pages_table_row" 
		|| $method == "update_pages_table_row" 
		|| $method == "delete_pages_table_row" 
		|| $method == "create_pages_table_row" 
		|| $method == "update_pages_table_row" 
		|| $method == "delete_pages_table_row" 
		|| $method == "inform_pages_table_row" 
		|| $method == "view_pages_table_row" 
		|| $method == "check_pages_title" 
		|| $method == "check_pages_link" 
		|| $method == "check_pages_meta_title" 
		|| $method == "check_pages_meta_description" 
		|| $method == "generate_pages_link"
		|| $method == "get_pages_edit_log_data" 
		|| $method == "count_pages_edit_log_data" 
		|| $method == "get_pages_edit_log_data_by_id" 
		|| $method == "count_pages_edit_log_data_by_id")
) {
	
	/* include: configurations */
	require_once("../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	require_once("mail.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");

	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "get_pages_data_by_id"){
		get_pages_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_pages_data_all"){
		get_pages_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_data_all"){
		count_pages_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_data_all_excluding"){
		get_pages_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_data_all_excluding"){
		count_pages_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_main_language_data_all"){
		get_pages_main_language_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_main_language_data_all"){
		count_pages_main_language_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_main_language_data_all_excluding"){
		get_pages_main_language_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_main_language_data_all_excluding"){
		count_pages_main_language_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_by_languages_id_data_all"){
		get_pages_by_languages_id_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_by_languages_id_data_all"){
		count_pages_by_languages_id_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_by_languages_short_name_data_all"){
		get_pages_by_languages_short_name_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_by_languages_short_name_data_all"){
		count_pages_by_languages_short_name_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_pages_translation_data_all"){
		get_pages_translation_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_translation_data_all"){
		count_pages_translation_data_all($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]); 
	} else if ($method == "get_pages_data_table_all"){
		get_pages_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_pages_data_table_all"){
		count_pages_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_pages_data_table_all"){
		get_search_pages_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_pages_data_table_all"){
		count_search_pages_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_pages_data"){
		create_pages_data($parameters);
	} else if ($method == "update_pages_data"){
		update_pages_data($parameters);
	} else if ($method == "patch_pages_data"){
		patch_pages_data($parameters);
	} else if ($method == "delete_pages_data"){
		delete_pages_data($parameters);
	} else if ($method == "create_pages_table"){
		create_pages_table($parameters, $table_module_field); 
	} else if ($method == "create_pages_table_row"){
		create_pages_table_row($parameters, $table_module_field);
	} else if ($method == "update_pages_table_row"){
		update_pages_table_row($parameters, $_POST["data_table_old_id"], $table_module_field);
	} else if ($method == "delete_pages_table_row"){
		delete_pages_table_row($_POST["data_table_old_id"], $_POST["data_table_old_transform_id"], $table_module_field); 
	} else if ($method == "inform_pages_table_row"){
		inform_pages_table_row($parameters, $table_module_field);
	} else if ($method == "view_pages_table_row"){
		view_pages_table_row($parameters, $table_module_field);

	} else if ($method == "check_pages_title"){
		check_pages_title($parameters["pages_title"], $parameters["pages_id"]); 
	} else if ($method == "check_pages_link"){
		check_pages_link($parameters["pages_link"], $parameters["pages_id"]); 
	} else if ($method == "check_pages_meta_title"){
		check_pages_meta_title($parameters["pages_meta_title"], $parameters["pages_id"]); 
	} else if ($method == "check_pages_meta_description"){
		check_pages_meta_description($parameters["pages_meta_description"], $parameters["pages_id"]); 

	} else if ($method == "generate_pages_link"){
		generate_pages_link($parameters); 

	} else if ($method == "get_pages_edit_log_data"){
		get_pages_edit_log_data($parameters);
	} else if ($method == "count_pages_edit_log_data"){
		count_pages_edit_log_data($parameters);
	} else if ($method == "get_pages_edit_log_data_by_id"){
		get_pages_edit_log_data_by_id($parameters);
	} else if ($method == "count_pages_edit_log_data_by_id"){
		count_pages_edit_log_data_by_id($parameters);
	}
}

function get_pages_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages`.`pages_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
			$query["pages_date_created_formatted"] = datetime_reformat($query["pages_date_created"]);
			
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
			$pages_translation = get_pages_translation_data_all($query["pages_id"]);
			$query["pages_translation"] = $pages_translation;
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "pages_id",
					"modules_record_target" => $target,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_data_all($start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `pages_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "pages" */
	$sql = "
	SELECT *
	FROM   `pages`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
				if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
					$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
				}
				if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
					$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
				}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `pages_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_by_languages_id_data_all($target, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all by language ID from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `languages_id` = '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data by language ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "languages_id",
					"modules_record_target" => $target,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_by_languages_id_data_all($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count data all by language ID from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `languages_id` = '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `languages_id` = '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_by_languages_short_name_data_all($target, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all by language short name from module "pages" */
	$sql = "

	SELECT *
	FROM   `pages`
	WHERE  `languages_short_name` = '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				


				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data by language short name",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "languages_short_name",
					"modules_record_target" => $target,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_by_languages_short_name_data_all($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count data all by language short name from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM   `pages`
	WHERE  `languages_short_name` = '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM   `pages`
	WHERE  `languages_short_name` = '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_main_language_data_all($start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all by main language from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data from main language",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_main_language_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count data all by main language from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_translation_data_all($target, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all by sub language from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data from main language",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_translation_data_all($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count data all by sub language from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_translate` = '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result     = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "pages_id",
					"modules_record_target" => $target,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `pages_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_main_language_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding by main language from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	AND `pages_translate` = '0'
	".$extra_sql_data_all.";";
	$result     = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data from main language excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "pages_id",
					"modules_record_target" => $target,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_main_language_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count data all excluding by main language from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	AND `pages_translate` = '0'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_id` != '" . $target . "'
	AND `pages_translate` = '0'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	

	/* database: count data all for datatable from module "pages" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_pages_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "pages_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'pages'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "pages_actions" 
				&& $key != "pages_date_created" 
				&& $key != "pages_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
		if (isset($query["pages_meta_description"]) && !empty($query["pages_meta_description"])) {
			$query["pages_meta_description"] = str_replace("<br />", "\n", $query["pages_meta_description"]);
		}
		if (isset($query["pages_template"]) && !empty($query["pages_template"])) {
			$query["pages_template_path"] = $configs["base_url"]."/templates/".$configs["template"]."/".$query["pages_template"];
		}
				$pages_translation = get_pages_translation_data_all($query["pages_id"]);
				$query["pages_translation"] = $pages_translation;

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "9",
					"modules_name" => "Pages",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_pages_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `pages_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `pages_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'pages'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "pages_actions" 
				&& $key != "pages_date_created" 
				&& $key != "pages_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_translate` = '0'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_pages_data($parameters){
	
	/* get global: configurations */
	global $configs;
	$pages_configs = get_modules_data_by_id("9");

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["pages_activate"] == "1") {
	
		if(!isset($parameters["pages_title"]) || empty($parameters["pages_title"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is required");
			$response["target"] = "#pages_title";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_title"]) > 240 && $parameters["pages_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#pages_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_title"]) > 240 && $parameters["pages_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["pages_title"]) && $parameters["pages_title"] != "") {
			$sql_check_unique_pages_title = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_title` LIKE '".$parameters["pages_title"]."' 
			
			LIMIT 1";
			$result_check_unique_pages_title = mysqli_query($con, $sql_check_unique_pages_title);
			$num_check_unique_pages_title = mysqli_num_rows($result_check_unique_pages_title);
			if ($num_check_unique_pages_title > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_title";
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["pages_link"]) || empty($parameters["pages_link"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is required");
			$response["target"] = "#pages_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_link"]) > 200 && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_link"]) > 200 && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strpos")) {
			if(mb_strpos($parameters["pages_link"], " ") && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow white spaces");
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strpos($parameters["pages_link"], " ") && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow white spaces") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["pages_link"]) && $parameters["pages_link"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#pages_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["pages_link"]) && $parameters["pages_link"] != "") {
			$sql_check_unique_pages_link = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_link` LIKE '".$parameters["pages_link"]."' 
			
			LIMIT 1";
			$result_check_unique_pages_link = mysqli_query($con, $sql_check_unique_pages_link);
			$num_check_unique_pages_link = mysqli_num_rows($result_check_unique_pages_link);
			if ($num_check_unique_pages_link > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_link";
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_meta_title"]) > 240 && $parameters["pages_meta_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#pages_meta_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_meta_title"]) > 240 && $parameters["pages_meta_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_meta_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["pages_meta_title"]) && $parameters["pages_meta_title"] != "") {
			$sql_check_unique_pages_meta_title = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_meta_title` LIKE '".$parameters["pages_meta_title"]."' 
			
			LIMIT 1";
			$result_check_unique_pages_meta_title = mysqli_query($con, $sql_check_unique_pages_meta_title);
			$num_check_unique_pages_meta_title = mysqli_num_rows($result_check_unique_pages_meta_title);
			if ($num_check_unique_pages_meta_title > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_meta_title";
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_meta_description"]) > 400 && $parameters["pages_meta_description"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is longer than maximum length of strings at") . " 400 " . translate("character(s)");
				$response["target"] = "#pages_meta_description";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_meta_description"]) > 400 && $parameters["pages_meta_description"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is longer than maximum length of strings at") . " 400 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_meta_description";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}



	}

	/* initialize: default */
	date_default_timezone_set($configs["timezone"]);
	$parameters["pages_date_created"] = gmdate("Y-m-d H:i:s");
	$parameters["pages_link"] = str_replace(".php", "", $parameters["pages_link"]);
	$parameters["pages_link"] = $parameters["pages_link"].".php";

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */
	if ($parameters["languages_short_name"] == "") {
		$sql_language = "
		SELECT *
		FROM `languages`
		WHERE `languages_short_name` = '" . $configs["language"] . "'
		LIMIT 1";
		$result_language = mysqli_query($con, $sql_language);
		$num_language = mysqli_num_rows($result_language);
		if ($num_language > 0) {
			$query_language = mysqli_fetch_array($result_language);
			$languages_id = $query_language["languages_id"];
			$languages_short_name = $query_language["languages_short_name"];
			$languages_name = $query_language["languages_name"];
		} else {
			$languages_short_name = $configs["language"];
		}
	} else {
		$languages_id = $parameters["languages_id"];
		$languages_short_name = $parameters["languages_short_name"];
		$languages_name = $parameters["languages_name"];
	}

	/* database: insert to module "pages" (begin) */
	$sql = "INSERT INTO `pages` (
				`pages_id`,
				`pages_title`,
				`pages_link`,
				`pages_meta_title`,
				`pages_meta_description`,
				`pages_meta_keyword`,
				`pages_template`,
				`modules_id`,
				`modules_name`,
				`modules_record_target`,
				`modules_record_key`,
				`pages_translate`,
				`pages_date_created`,
				`pages_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`,
				`languages_id`,
				`languages_short_name`,
				`languages_name`)
			VALUES (
				NULL,
				'" . $parameters["pages_title"] . "',
				'" . $parameters["pages_link"] . "',
				'" . $parameters["pages_meta_title"] . "',
				'" . $parameters["pages_meta_description"] . "',
				'" . $parameters["pages_meta_keyword"] . "',
				'" . $parameters["pages_template"] . "',
				'" . $parameters["modules_id"] . "',
				'" . $parameters["modules_name"] . "',
				'" . $parameters["modules_record_target"] . "',
				'" . $parameters["modules_record_key"] . "',
				'" . $parameters["pages_translate"] . "',
				'" . $parameters["pages_date_created"] . "',
				'" . $parameters["pages_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "',
				'" . $parameters["languages_id"] . "',
				'" . $parameters["languages_short_name"] . "',
				'" . $parameters["languages_name"] . "')";
	$result = mysqli_query($con, $sql);
	$parameters["pages_id"] = mysqli_insert_id($con);
	/* database: insert to module "pages" (end) */

	if ($result) {
		

		/* response: additional data */
		

		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "pages_id",
				"modules_record_target" => $parameters["pages_id"],
				"modules_id" => "9",
				"modules_name" => "Pages",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_pages_data($parameters){

	/* get global: configurations */
	global $configs;
	$pages_configs = get_modules_data_by_id("9");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["pages_activate"] == "1") {
	
		if(!isset($parameters["pages_title"]) || empty($parameters["pages_title"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is required");
			$response["target"] = "#pages_title";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_title"]) > 240 && $parameters["pages_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#pages_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_title"]) > 240 && $parameters["pages_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["pages_title"]) && $parameters["pages_title"] != "") {
			$sql_check_unique_pages_title = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_title` LIKE '".$parameters["pages_title"]."' 
			AND `pages_id` != '".$parameters["pages_id"]."'
			LIMIT 1";
			$result_check_unique_pages_title = mysqli_query($con, $sql_check_unique_pages_title);
			$num_check_unique_pages_title = mysqli_num_rows($result_check_unique_pages_title);
			if ($num_check_unique_pages_title > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_title";
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["pages_link"]) || empty($parameters["pages_link"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is required");
			$response["target"] = "#pages_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_link"]) > 200 && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_link"]) > 200 && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strpos")) {
			if(mb_strpos($parameters["pages_link"], " ") && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow white spaces");
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strpos($parameters["pages_link"], " ") && $parameters["pages_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow white spaces") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["pages_link"]) && $parameters["pages_link"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#pages_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["pages_link"]) && $parameters["pages_link"] != "") {
			$sql_check_unique_pages_link = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_link` LIKE '".$parameters["pages_link"]."' 
			AND `pages_id` != '".$parameters["pages_id"]."'
			LIMIT 1";
			$result_check_unique_pages_link = mysqli_query($con, $sql_check_unique_pages_link);
			$num_check_unique_pages_link = mysqli_num_rows($result_check_unique_pages_link);
			if ($num_check_unique_pages_link > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_link";
				$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_meta_title"]) > 240 && $parameters["pages_meta_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#pages_meta_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_meta_title"]) > 240 && $parameters["pages_meta_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_meta_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["pages_meta_title"]) && $parameters["pages_meta_title"] != "") {
			$sql_check_unique_pages_meta_title = "
			SELECT `pages_id` FROM `pages` 
			WHERE `pages_meta_title` LIKE '".$parameters["pages_meta_title"]."' 
			AND `pages_id` != '".$parameters["pages_id"]."'
			LIMIT 1";
			$result_check_unique_pages_meta_title = mysqli_query($con, $sql_check_unique_pages_meta_title);
			$num_check_unique_pages_meta_title = mysqli_num_rows($result_check_unique_pages_meta_title);
			if ($num_check_unique_pages_meta_title > 0) {
				
				$response["status"] = false;
				$response["target"] = "#pages_meta_title";
				$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["pages_meta_description"]) > 400 && $parameters["pages_meta_description"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is longer than maximum length of strings at") . " 400 " . translate("character(s)");
				$response["target"] = "#pages_meta_description";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["pages_meta_description"]) > 400 && $parameters["pages_meta_description"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is longer than maximum length of strings at") . " 400 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#pages_meta_description";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}

	/* database: get old data from module "pages" (begin) */
	$sql = "SELECT *
	        FROM   `pages`
			WHERE  `pages_id` = '" . $parameters["pages_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["pages_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "pages" (end) */
	
	/* initialize: default */
	
	$parameters["pages_date_created"] = $query["pages_date_created"];
	$parameters["pages_link"] = str_replace(".php", "", $parameters["pages_link"]);
	$parameters["pages_link"] = $parameters["pages_link"].".php";
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	if ($parameters["languages_short_name"] == "") {
		$sql_language = "
		SELECT *
		FROM `languages`
		WHERE `languages_short_name` = '" . $configs["language"] . "'
		LIMIT 1";
		$result_language = mysqli_query($con, $sql_language);
		$num_language = mysqli_num_rows($result_language);
		if ($num_language > 0) {
			$query_language = mysqli_fetch_array($result_language);
			$languages_id = $query_language["languages_id"];
			$languages_short_name = $query_language["languages_short_name"];
			$languages_name = $query_language["languages_name"];
		} else {
			$languages_short_name = $configs["language"];
		}
	} else {
		$languages_id = $parameters["languages_id"];
		$languages_short_name = $parameters["languages_short_name"];
		$languages_name = $parameters["languages_name"];
	}
	/* database: update to module "pages" (begin) */
	$sql = "UPDATE `pages`
			SET 
				   `pages_title` = '" . $parameters["pages_title"] . "',
				   `pages_link` = '" . $parameters["pages_link"] . "',
				   `pages_meta_title` = '" . $parameters["pages_meta_title"] . "',
				   `pages_meta_description` = '" . $parameters["pages_meta_description"] . "',
				   `pages_meta_keyword` = '" . $parameters["pages_meta_keyword"] . "',
				   `pages_template` = '" . $parameters["pages_template"] . "',
				   `pages_translate` = '" . $parameters["pages_translate"] . "',
				   `pages_date_created` = '" . $parameters["pages_date_created"] . "',
				   `pages_activate` = '" . $parameters["pages_activate"] . "',
				   `modules_id` = '".$parameters["modules_id"]."',
				   `modules_name` = '".$parameters["modules_name"]."',
				   `modules_record_target` = '".$parameters["modules_record_target"]."',
				   `modules_record_key` = '".$parameters["modules_record_key"]."',
				   `languages_id` = '" . $languages_id . "',
				   `languages_short_name` = '" . $languages_short_name . "',
				   `languages_name` = '" . $languages_name . "'
			WHERE  `pages_id` = '" . $parameters["pages_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "pages" (end) */

	if ($result) {
		
		if (isset($parameters["pages_old"]["modules_id"]) && !empty($parameters["pages_old"]["modules_id"])) {
			/* database: get module data of "pages" (begin) */
			$sql_modules = "
			SELECT * 
			FROM   `modules` 
			WHERE  `modules_id` = '".$parameters["pages_old"]["modules_id"]."' 
			LIMIT 1";
			$result_modules = mysqli_query($con, $sql_modules);
			$num_modules = mysqli_num_rows($result_modules);
			if ($num_modules > 0) {
				$query_modules = mysqli_fetch_array($result_modules);

				/* database: update to content page link of "pages" (begin) */
				$sql_page_link = "
				UPDATE `".$query_modules["modules_db_name"]."`
				SET 
				`".$query_modules["modules_db_name"]."_title` = '".$parameters["pages_title"]."', 
				`pages_link` = '".$parameters["pages_link"]."' 
				WHERE  `".$query_modules["modules_db_name"]."_id` = '".$parameters["pages_old"]["pages_module_id"]."'";
				$result_page_link = mysqli_query($con, $sql_page_link);
				/* database: update to content page link of "pages" (end) */

				$sql_module_self = "SELECT `".$query_modules["modules_db_name"]."_id` 
								  FROM `".$query_modules["modules_db_name"]."` 
								  WHERE  `pages_id` = '".$parameters["pages_translate"]."'";
				$result_module_self = mysqli_query($con, $sql_module_self);
				$query_module_self = mysqli_fetch_array($result_module_self);
				
				global $database_access;
				
				$sql_pages_table_fields = "
				SELECT * from INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
				AND TABLE_NAME = '" . $query_modules["modules_db_name"] . "'";
				$result_pages_table_fields = mysqli_query($con, $sql_pages_table_fields);
				$i = 0;
				$pages_table_fields_array = array();
				while ($query_pages_table_fields = mysqli_fetch_assoc($result_pages_table_fields)) {
					$pages_table_fields_array[$i] = strtolower($query_pages_table_fields['COLUMN_NAME']);
					$i++;
				}
				
				if (in_array('languages_short_name', $pages_table_fields_array)) {
					/* database: update to content language of "pages" (begin) */
					$sql_language = "
					UPDATE `".$query_modules["modules_db_name"]."`
					SET `languages_id` = '".$parameters["languages_id"]."', 
						`languages_short_name` = '".$parameters["languages_short_name"]."', 
						`languages_name` = '".$parameters["languages_name"]."' 
					WHERE `".$query_modules["modules_db_name"]."_id` = '".$parameters["pages_old"]["pages_module_id"]."'";
					$result_language = mysqli_query($con, $sql_language);
					/* database: update to content language of "pages" (end) */
				}
				
				if (in_array($query_modules["modules_db_name"]."_translate", $pages_table_fields_array)) {
					$sql_module = "
					UPDATE `".$query_modules["modules_db_name"]."`
					SET `".$query_modules["modules_db_name"]."_translate` = '".$query_module_self[$query_modules["modules_db_name"]."_id"]."'
					WHERE `".$query_modules["modules_db_name"]."_id` = '".$parameters["pages_old"]["modules_record_target"]."'";
					$result_module = mysqli_query($con, $sql_module);

					if ($parameters["pages_translate"] != $parameters["pages_old"]["pages_translate"]) {

						$sql_module_self = "
						SELECT `".$query_modules["modules_db_name"]."_id` 
						FROM `".$query_modules["modules_db_name"]."` 
						WHERE `pages_id` = '".$parameters["pages_translate"]."'";
						$result_module_self = mysqli_query($con, $sql_module_self);
						$query_module_self = mysqli_fetch_array($result_module_self);

						$sql_translate = "
						UPDATE `".$query_modules["modules_db_name"]."`
						SET `".$query_modules["modules_db_name"]."_translate` = '".$query_module_self[$query_modules["modules_db_name"]."_id"]."'
						WHERE `".$query_modules["modules_db_name"]."_translate` = '".$parameters["pages_old"]["modules_record_target"]."'";
						$result_translate = mysqli_query($con, $sql_translate);

					}
				}
				mysqli_free_result($result_module_self);
				mysqli_free_result($result_modules);
				/* database: get module data of "pages" (end) */
				
			}
		}
		
		if ($parameters["pages_translate"] != $parameters["pages_old"]["pages_translate"]) {
			$sql_translate = "UPDATE `pages`
							  SET    `pages_translate` = '".$parameters["pages_translate"]."'
							  WHERE  `pages_translate` = '".$parameters["pages_id"]."'";
			$result_translate = mysqli_query($con, $sql_translate);
		}
		
		/* response: additional data */
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "pages_id",
				"modules_record_target" => $parameters["pages_id"],
				"modules_id" => "9",
				"modules_name" => "Pages",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "pages";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_pages_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "pages" (begin) */
	$sql = "SELECT *
	        FROM   `pages`
			WHERE  `pages_id` = '" . $parameters["pages_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["pages_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "pages" (end) */
	
	/* initialize: default */
	$parameters["pages_date_created"] = $query["pages_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "pages" (begin) */
	$sql = "UPDATE `pages`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `pages_id` = '" . $parameters["pages_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "pages" (end) */

	if ($result) {
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "pages_id",
				"modules_record_target" => $parameters["pages_id"],
				"modules_id" => "9",
				"modules_name" => "Pages",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "pages";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_pages_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["pages_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
			/* database: update sub language module "pages" (begin) */
			$parameters["pages_transform_id"] = "";
			$sql = "
			SELECT `pages_id`
			FROM `pages`
			WHERE `pages_translate` = '" . $target . "'
			ORDER BY `pages_id` ASC
			LIMIT 1";
			$result = mysqli_query($con, $sql);
			$num = mysqli_num_rows($result);
			if ($num > 0) {
				$query = mysqli_fetch_array($result);

				$sql = "
				UPDATE `pages`
				SET `pages_translate` = '0'
				WHERE `pages_translate` = '" . $target . "'
				ORDER BY `pages_id` ASC
				LIMIT 1";
				$result = mysqli_query($con, $sql);
				$pages_id_translate = $query["pages_id"];
				$pages_id_translate_main = mysqli_insert_id($con);

				$sql = "
				UPDATE `pages`
				SET `pages_translate` = '" . $query["pages_id"] . "'
				WHERE `pages_translate` = '" . $target . "'";
				$result = mysqli_query($con, $sql);
				$pages_id_translate_sub = mysqli_insert_id($con);
				$parameters["pages_transform_id"] = $pages_id_translate;
				if (isset($parameters["pages_old"]["modules_id"]) && !empty($parameters["pages_old"]["modules_id"])) {
				
				/* database: update module data of "pages" (begin) */
				$sql_modules = "
				SELECT * 
				FROM `modules` 
				WHERE `modules_id` = '".$parameters["pages_old"]["modules_id"]."' 
				LIMIT 1";
				$result_modules = mysqli_query($con, $sql_modules);
				$num_modules = mysqli_num_rows($result_modules);
				if ($num_modules > 0) {
					$query_modules = mysqli_fetch_array($result_modules);
					
					global $database_access;

					$sql_pages_table_fields = "
					SELECT * from INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
					AND TABLE_NAME = '" . $query_modules["modules_db_name"] . "'";
					$result_pages_table_fields = mysqli_query($con, $sql_pages_table_fields);
					$i = 0;
					$pages_table_fields_array = array();
					while ($query_pages_table_fields = mysqli_fetch_assoc($result_pages_table_fields)) {
						$pages_table_fields_array[$i] = strtolower($query_pages_table_fields['COLUMN_NAME']);
						$i++;
					}
					
					if (in_array($query_modules["modules_db_name"]."_translate", $pages_table_fields_array)) {
					
						$sql_module = "
						SELECT `".$query_modules["modules_db_name"]."_id`
						FROM `".$query_modules["modules_db_name"]."`
						WHERE `".$query_modules["modules_db_name"]."_translate` = '".$parameters["pages_old"]["modules_record_target"]."'
						ORDER BY `".$query_modules["modules_db_name"]."_id` ASC
						LIMIT 1";
						$result_module = mysqli_query($con, $sql_module);
						$num_module = mysqli_num_rows($result_module);
						if ($num_module > 0) {
							$query_module = mysqli_fetch_array($result_module);

							$sql_translate = "
							UPDATE `".$query_modules["modules_db_name"]."`
							SET `".$query_modules["modules_db_name"]."_translate` = '0'
							WHERE `".$query_modules["modules_db_name"]."_translate` = '".$parameters["pages_old"]["modules_record_target"]."'
							ORDER BY `".$query_modules["modules_db_name"]."_id` ASC
							LIMIT 1";
							$result_translate = mysqli_query($con, $sql_translate);

							$sql_translate = "
							UPDATE `".$query_modules["modules_db_name"]."`
							SET    `".$query_modules["modules_db_name"]."_translate` = '".$query_module[$query_modules["modules_db_name"]."_id"]."'
							WHERE  `".$query_modules["modules_db_name"]."_translate` = '".$parameters["pages_old"]["modules_record_target"]."'";
							$result_translate = mysqli_query($con, $sql_translate);

						}
						
					}

				}
				/* database: update module data of "pages" (end) */

			}
		}
		/* database: update sub language module "pages" (end) */
			
		/* database: delete from module "pages" (begin) */
		$sql = "
		DELETE FROM `pages`
		WHERE `pages_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "pages" (end) */

		if ($result) {
			
			if (isset($parameters["pages_old"]["modules_id"]) && !empty($parameters["pages_old"]["modules_id"])) {
				
				$sql_modules = "
				SELECT * 
				FROM `modules` 
				WHERE `modules_id` = '".$parameters["pages_old"]["modules_id"]."' 
				LIMIT 1";
				$result_modules = mysqli_query($con, $sql_modules);
				$num_modules = mysqli_num_rows($result_modules);
				if ($num_modules > 0) {
					$query_modules = mysqli_fetch_array($result_modules);
				
					/* database: delete module data of "pages" (begin) */
					$sql = "
					DELETE FROM `".$query_modules["modules_db_name"]."`
					WHERE `".$query_modules["modules_db_name"]."_id` = '" . $parameters["pages_old"]["modules_record_target"] . "'";
					$result = mysqli_query($con, $sql);
					/* database: delete module data of "pages" (end) */
					
				}
				
			}
			
			$log = array(
				"log_name" => "delete data",
				"log_function" => __FUNCTION__,
				"modules_record_target" => $target,
				"log_violation" => "risk",
				"log_content_hash" => "",
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_id" => "8",
				"modules_name" => "Pages",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();


}

function check_pages_title($title, $target = "") {
	
	global $method;
	

	$con = start();
	
	$title = escape_string($con, trim($title));
	$target = escape_string($con, trim($target));
	
	if(!isset($title) or empty($title)){

		$response["status"] = false;
		$response["target"] = "#pages_title";
		$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is required");
		$response["values"] = $title;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($title) > 240 && $title != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings") . " 240";
		$response["target"] = "#pages_title";
		$response["values"] = $title;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (!empty($target)) {
		$extra_sql = "AND `pages_id` != '" . $target . "'";
	}
	
	$sql = "SELECT *
			FROM   `pages`
			WHERE  `pages_link` LIKE '" . generate_title_link($title) . "'
			" . $extra_sql . "
			LIMIT  1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num <= 0) {

			$response["status"] = true;
			$response["message"] = translate("Empty data");

		} else {

			$query = mysqli_fetch_array($result);
			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is duplicated with") . " <strong>" . $query["pages_title"] . "</strong> (ID: " . $query["pages_id"] . ") *" . translate("Not recommended for Search Engine Optimization (SEO)");
			$response["target"] = "#pages_title";

			unset($query);
			$query = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}
	
function check_pages_link($link, $target = "") {

	global $method;
	

	$con = start();

	$link = escape_string($con, trim($link));
	$target = escape_string($con, trim($target));
	
	if(!isset($link) or empty($link)){

		$response["status"] = false;
		$response["target"] = "#pages_link";
		$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is required");
		$response["values"] = $link;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($link) > 200 && $link != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("is longer than maximum length of strings") . " 200";
		$response["target"] = "#pages_link";
		$response["values"] = $link;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strpos($link, " ") && $link != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow white spaces");
		$response["target"] = "#pages_link";
		$response["values"] = $link;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $link) && $link != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
		$response["target"] = "#pages_link";
		$response["values"] = $link;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (!empty($target)) {
		$extra_sql = "AND `pages_id` != '" . $target . "'";
	}
		
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_link` LIKE '". $link.".php" . "' 
	" . $extra_sql . "
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);

		if ($num <= 0) {

			$response["status"] = true;
			$response["message"] = translate("Empty data");

		} else {

			$query = mysqli_fetch_array($result);

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Link") . "</strong> " . translate("cannot be duplicated with") . " <strong>" . $query["pages_title"] . "</strong> (ID: " . $query["pages_id"] . ")";
			$response["target"] = "#pages_link";

			unset($query);
			$query = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}
	
function check_pages_meta_title($meta_title, $target = "") {

	global $method;
	

	$con = start();
	
	$meta_title = escape_string($con, trim($meta_title));
	$target = escape_string($con, trim($target));
	
	if(!isset($meta_title) or empty($meta_title)){

		$response["status"] = false;
		$response["target"] = "#pages_meta_title";
		$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is empty") . " *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["values"] = $meta_title;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($meta_title) < 10 && $meta_title != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is shorter than best length of strings at") . " 10 *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["target"] = "#pages_meta_title";
		$response["values"] = $meta_title;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($meta_title) > 200 && $meta_title != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is longer than best length of strings at") . " 200 *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["target"] = "#pages_meta_title";
		$response["values"] = $meta_title;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (!empty($target)) {
		$extra_sql = "AND `pages_id` != '" . $target . "'";
	}
	
	$sql = "SELECT *
			FROM   `pages`
			WHERE  `pages_meta_title` LIKE '" . $meta_title . "'
			" . $extra_sql . "
			LIMIT  1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num <= 0) {

			$response["status"] = true;
			$response["message"] = translate("Empty data");

		} else {

			$query = mysqli_fetch_array($result);

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Meta Title") . "</strong> " . translate("is duplicated with") . " <strong>" . $query["pages_title"] . "</strong> (ID: " . $query["pages_id"] . ") *" . translate("Not recommended for Search Engine Optimization (SEO)");
			$response["target"] = "#pages_meta_title";

			unset($query);
			$query = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function check_pages_meta_description($meta_description, $target = "") {

	global $method;
	

	$con = start();
	
	$meta_description = escape_string($con, trim($meta_description));
	$target = escape_string($con, trim($target));
	
	if(!isset($meta_description) or empty($meta_description)){

		$response["status"] = false;
		$response["target"] = "#pages_meta_description";
		$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is empty") . " *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["values"] = $meta_description;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($meta_description) < 100 && $meta_description != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is shorter than best length of strings at") . " 100 *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["target"] = "#pages_meta_description";
		$response["values"] = $meta_description;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(mb_strlen($meta_description) > 200 && $meta_description != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is longer than best length of strings at") . " 200 *" . translate("Not recommended for Search Engine Optimization (SEO)");
		$response["target"] = "#pages_meta_description";
		$response["values"] = $meta_description;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (!empty($target)) {
		$extra_sql = "AND `pages_id` != '" . $target . "'";
	}
	
	$sql = "SELECT *
			FROM   `pages`
			WHERE  `pages_meta_description` LIKE '" . $meta_description . "'
			" . $extra_sql . "
			LIMIT  1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num <= 0) {

			$response["status"] = true;
			$response["message"] = translate("Empty data");

		} else {

			$query = mysqli_fetch_array($result);

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Meta Description") . "</strong> " . translate("is duplicated with") . " <strong>" . $query["pages_title"] . "</strong> (ID: " . $query["pages_id"] . ") *" . translate("Not recommended for Search Engine Optimization (SEO)");
			$response["target"] = "#pages_meta_description";

			unset($query);
			$query = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function generate_pages_link($target) {

	global $method;
	

	$con = start();

	$target = escape_string($con, trim($target));
	
	if (generate_title_link($target) != "") {
		$response["status"] = true;
		$response["values"] = str_replace(".php", "", generate_title_link($target));
	} else {
		$sql = "SELECT *
				FROM   `pages`
				WHERE  `pages_link` LIKE '". generate_title_link($target) . "'
				LIMIT  1";

		$result = mysqli_query($con, $sql);
		$query = mysqli_fetch_array($result);
		$num = mysqli_num_rows($result);
		if ($result) {
			$response["status"] = true;
			$response["values"] = $num + 1;
		} else {
			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		}

		stop($con);
		mysqli_free_result($result);
	}
	

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_pages_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Description")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Keywords")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Template")));
			} else {
				$column_name = str_replace("pages", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "pages_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "pages_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "pages_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "pages_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "pages_title") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_title . '" data-toggle="tooltip" title="Title" data-original-title="Title">' . htmlspecialchars(strip_tags(translate("Title"))) . '</th>';
			} else if ($key == "pages_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_link . '" data-toggle="tooltip" title="Link" data-original-title="Link">' . htmlspecialchars(strip_tags(translate("Link"))) . '</th>';
			} else if ($key == "pages_meta_title") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_meta_title . '" data-toggle="tooltip" title="Meta Title" data-original-title="Meta Title">' . htmlspecialchars(strip_tags(translate("Meta Title"))) . '</th>';
			} else if ($key == "pages_meta_description") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_meta_description . '" data-toggle="tooltip" title="Meta Description" data-original-title="Meta Description">' . htmlspecialchars(strip_tags(translate("Meta Description"))) . '</th>';
			} else if ($key == "pages_meta_keyword") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_meta_keyword . '" data-toggle="tooltip" title="Meta Keywords" data-original-title="Meta Keywords">' . htmlspecialchars(strip_tags(translate("Meta Keywords"))) . '</th>';
			} else if ($key == "pages_template") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . pages_template . '" data-toggle="tooltip" title="Template" data-original-title="Template">' . htmlspecialchars(strip_tags(translate("Template"))) . '</th>';
			} else if ($key == "pages_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "pages_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["pages_id"].'" data-target="'.$data_table_row["pages_id"].'">';

		$html .= inform_pages_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_pages_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "pages_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "pages_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "pages_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "pages_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "pages_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "pages_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_pages_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_pages_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_pages_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_pages_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_pages_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_pages_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_pages_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_pages_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_pages_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_pages_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_pages_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	if ($data_table["pages_translate"] != 0 && !empty($data_table["pages_translate"])) {
		$data_table = get_pages_data_by_id($data_table["pages_translate"]);
	} else {
		$data_table = get_pages_data_by_id($data_table["pages_id"]);
	}
	$html = inform_pages_table_row($data_table, $table_module_field);

	$html = '<tr id="datatable-'.$data_table["pages_id"].'" data-target="'.$data_table["pages_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_pages_table_row($data_table, $data_table_old_id, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	if ($data_table["pages_id"] == $data_table_old_id) {
		if ($data_table["pages_translate"] != 0 && !empty($data_table["pages_translate"])) {
			$data_table = get_pages_data_by_id($data_table["pages_translate"]);
		} else {
			$data_table = get_pages_data_by_id($data_table["pages_id"]);
		}
		$html = inform_pages_table_row($data_table, $table_module_field);
		$html = '<tr id="datatable-'.$data_table["pages_id"].'" data-target="'.$data_table["pages_id"].'">'.$html.'</tr>';
	} else {
		if ($data_table["pages_translate"] != 0 && !empty($data_table["pages_translate"])) {
			$data_table = get_pages_data_by_id($data_table["pages_translate"]);
		} else {
			$data_table = get_pages_data_by_id($data_table["pages_id"]);
		}
		$html = inform_pages_table_row($data_table, $table_module_field);
		$html = '<tr id="datatable-'.$data_table["pages_id"].'" data-target="'.$data_table["pages_id"].'">'.$html.'</tr>';
		$data_table_old = get_pages_data_by_id($data_table_old_id);
		if ($data_table_old["pages_id"] != $data_table["pages_id"]) {
			$html_old = inform_pages_table_row($data_table_old, $table_module_field);
			$html_old = '<tr id="datatable-'.$data_table_old["pages_id"].'" data-target="'.$data_table_old["pages_id"].'">'.$html_old.'</tr>';
		}
	}

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["values"] = $data_table;
	if (count($data_table_old) > 0) {
		$response["values_old"] = $data_table_old;
	}
	$response["html"] = $html.$html_old;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($html_old);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function delete_pages_table_row($data_table_old_id, $data_table_old_transform_id, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	if (!empty($data_table_old_id)) {
		$data_table_old = get_pages_data_by_id($data_table_old_id);
		if ($data_table_old != "") {
			$html_old = inform_pages_table_row($data_table_old, $table_module_field);
			$html_old = '<tr id="datatable-'.$data_table_old_id.'" data-target="'.$data_table_old_id.'">'.$html_old.'</tr>';
		}
	}
	if (!empty($data_table_old_transform_id)) {
		$data_table_old_transform = get_pages_data_by_id($data_table_old_transform_id);
		if ($data_table_old_transform != "") {
			$html_old_transform = inform_pages_table_row($data_table_old_transform, $table_module_field);
			$html_old_transform = '<tr id="datatable-'.$data_table_old_transform_id.'" data-target="'.$data_table_old_transform_id.'">'.$html_old_transform.'</tr>';
		}
	}

	$response["status"] = true;
	$response["message"] = translate("Successfully deleted table row");
	$response["values"] = $data_table_old;
	$response["values_old_transform"] = $data_table_old_transform;
	$response["html"] = $html_old;
	$response["html_old_transform"] = $html_old_transform;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"].$response["html_old_transform"];
	}
	unset($html_old);
	unset($html_old_transform);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_pages_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

    $con = start();

	$sql_languages  = "
	SELECT `languages_id`, `languages_short_name`, `languages_name`
	FROM   `languages`";
	$result_languages = mysqli_query($con, $sql_languages);
	$num_languages = mysqli_num_rows($result_languages);
	$languages = array();
	if ($num_languages > 0) {
		$i = 0;
		while ($query_languages = mysqli_fetch_array($result_languages)) {
			$languages[$i] = $query_languages;
			$i++;
		}
	}
	mysqli_free_result($result_languages);
	if (isset($con)) {
		stop($con);
	}

	$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
	$i = 0;
	foreach ($table_module_field as $key => $value) {
		
		if ($value == "true" || $value === true) {
		
			$table_row_value_key = "";
			$table_row_value = "";
			$td_align_class = "";
			$td_weight_class = "";

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";

			}

			if ($i >= 3 && $key != "pages_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "pages_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "pages_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "pages_actions") {
				if ($key == "pages_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table["pages_id"];
					$table_row_value = '<div id="datatable-pages_id-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.$data_table["pages_id"].'</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j=0;$j<count($data_table["pages_translation"]);$j++){
							$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.$data_table["pages_translation"][$j]["pages_id"].'</div>';
						}
					}

				} else if ($key == "pages_title") {
					$td_align_class = "";
					$table_row_value = '<div id="datatable-pages_title-' . $data_table["pages_id"] . '" class="datatable-language-main">' . $flag . htmlspecialchars(strip_tags($data_table["pages_title"])) . '</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j = 0; $j < count($data_table["pages_translation"]); $j++){
							$flag_translate = '<img src="templates/' . $configs["backend_template"] . '/images/languages/' . strtolower($data_table["pages_translation"][$j]["languages_short_name"]) . '.gif" alt="' . $data_table["pages_translation"][$j]["languages_name"] . '" title="' . $data_table["pages_translation"][$j]["languages_name"] . '" /> ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_title"].'" class="datatable-language-sub">' . $flag_translate . htmlspecialchars(strip_tags($data_table["pages_translation"][$j]["pages_title"])) . '</div>';
						}
					}
				} else if ($key == "pages_link") {
					$td_align_class = "";
					$table_row_value = '<div id="datatable-pages_link-' . $data_table["pages_id"] . '" class="datatable-language-main">' . $flag . htmlspecialchars(strip_tags($data_table["pages_link"])) . '</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j = 0; $j < count($data_table["pages_translation"]); $j++){
							$flag_translate = '<img src="templates/' . $configs["backend_template"] . '/images/languages/' . strtolower($data_table["pages_translation"][$j]["languages_short_name"]) . '.gif" alt="' . $data_table["pages_translation"][$j]["languages_name"] . '" title="' . $data_table["pages_translation"][$j]["languages_name"] . '" /> ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_link"].'" class="datatable-language-sub">' . $flag_translate . htmlspecialchars(strip_tags($data_table["pages_translation"][$j]["pages_link"])) . '</div>';
						}
					}
				} else if ($key == "pages_meta_title") {
					$td_align_class = "";
					$table_row_value = '<div id="datatable-pages_meta_title-' . $data_table["pages_id"] . '" class="datatable-language-main">' . $flag . htmlspecialchars(strip_tags($data_table["pages_meta_title"])) . '</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j = 0; $j < count($data_table["pages_translation"]); $j++){
							$flag_translate = '<img src="templates/' . $configs["backend_template"] . '/images/languages/' . strtolower($data_table["pages_translation"][$j]["languages_short_name"]) . '.gif" alt="' . $data_table["pages_translation"][$j]["languages_name"] . '" title="' . $data_table["pages_translation"][$j]["languages_name"] . '" /> ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_meta_title"].'" class="datatable-language-sub">' . $flag_translate . htmlspecialchars(strip_tags($data_table["pages_translation"][$j]["pages_meta_title"])) . '</div>';
						}
					}
				} else if ($key == "pages_meta_description") {
					$td_align_class = "";
					$table_row_value = '<div id="datatable-pages_meta_description-' . $data_table["pages_id"] . '" class="datatable-language-main">' . $flag . htmlspecialchars(strip_tags($data_table["pages_meta_description"])) . '</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j = 0; $j < count($data_table["pages_translation"]); $j++){
							$flag_translate = '<img src="templates/' . $configs["backend_template"] . '/images/languages/' . strtolower($data_table["pages_translation"][$j]["languages_short_name"]) . '.gif" alt="' . $data_table["pages_translation"][$j]["languages_name"] . '" title="' . $data_table["pages_translation"][$j]["languages_name"] . '" /> ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_meta_description"].'" class="datatable-language-sub">' . $flag_translate . htmlspecialchars(strip_tags($data_table["pages_translation"][$j]["pages_meta_description"])) . '</div>';
						}
					}
				} else if ($key == "pages_meta_keyword") {
					$td_align_class = "";
					$table_row_value = '<div id="datatable-pages_meta_keyword-' . $data_table["pages_id"] . '" class="datatable-language-main">' . $flag . htmlspecialchars(strip_tags($data_table["pages_meta_keyword"])) . '</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j = 0; $j < count($data_table["pages_translation"]); $j++){
							$flag_translate = '<img src="templates/' . $configs["backend_template"] . '/images/languages/' . strtolower($data_table["pages_translation"][$j]["languages_short_name"]) . '.gif" alt="' . $data_table["pages_translation"][$j]["languages_name"] . '" title="' . $data_table["pages_translation"][$j]["languages_name"] . '" /> ';
							$table_row_value .= '<div id="datatable-pages_id-'.$data_table["pages_translation"][$j]["pages_meta_keyword"].'" class="datatable-language-sub">' . $flag_translate . htmlspecialchars(strip_tags($data_table["pages_translation"][$j]["pages_meta_keyword"])) . '</div>';
						}
					}
				} else if ($key == "pages_template") {
					$td_align_class = "text-center";
					$table_row_value = render_pages_table_template_file($data_table["pages_template"]);
				} else if ($key == "pages_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<div id="datatable-pages_activate-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.'<span class="label label-success">Published</span></div>';
					} else {
						$table_row_value = '<div id="datatable-pages_activate-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.'<span class="label label-danger">Unpublished</span></div>';
					}
					if (count($data_table["pages_translation"]) > 0) {
						for($j=0;$j<count($data_table["pages_translation"]);$j++){
							$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
							if ($data_table[$key] == "1") {
								$table_row_value .= '<div id="datatable-pages_activate-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.'<span class="label label-success">Published</span></div>';
							} else {
								$table_row_value .= '<div id="datatable-pages_activate-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.'<span class="label label-danger">Unpublished</span></div>';
							}
						}
					}
				} else if ($key == "modules_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<div id="datatable-modules_name-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.$data_table["modules_name"].'</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j=0;$j<count($data_table["pages_translation"]);$j++){
							$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
							$table_row_value .= '<div id="datatable-modules_name-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.$data_table["pages_translation"][$j]["modules_name"].'</div>';
						}
					}
				
				} else if ($key == "languages_short_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<div id="datatable-languages_short_name-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.$data_table["languages_name"].'</div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j=0;$j<count($data_table["pages_translation"]);$j++){
							$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
							$table_row_value .= '<div id="datatable-languages_short_name-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.$data_table["pages_translation"][$j]["languages_name"].'</div>';
						}
					}
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<div id="datatable-users_name-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.'<a href="users.php?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table["users_name"].'</a></div>';
					if (count($data_table["pages_translation"]) > 0) {
						for($j=0;$j<count($data_table["pages_translation"]);$j++){
							$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
							$table_row_value .= '<div id="datatable-users_name-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.'<a href="users.php?action=view&users_id='.$data_table["pages_translation"][$j]["users_id"].'" target="_blank">'.$data_table["pages_translation"][$j]["users_name"].'</a></div>';
						}
					}
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_pages_table_data($data_table[$key]);
					}

				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-field-'.$key.'-'.$data_table["pages_id"].'">'.$table_row_value.'</td>';
					$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "pages_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}
					
					$html .= '
		<td id="datatable-actions-'.$data_table["pages_id"].'" class="text-center">';
		$html .= '
			<div id="datatable-pages_actions-'.$data_table["pages_id"].'" class="datatable-language-main">'.$flag.'
				<div class="btn-group">';

					$html .= '
					<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["pages_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
					<i class="fa fa-eye"></i>
					</button>';
				
					if ($_SESSION["users_level"] != "5") {
							
							if (!isset($data_table["pages_protected"]) || (isset($data_table["pages_protected"]) && $data_table["pages_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {
							
							$html .= '
					<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["pages_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
					<i class="fa fa-pencil"></i>
					</button>';
					
							}

							$html .= '
					<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["pages_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
					<i class="fa fa-copy"></i>
					</button>';
					
						if (!is_array($data_table[$table_module_field_key])) {
							$html .= '
					<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["pages_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?">
					<i class="fa fa-times"></i>
					</button>';
						} else {
							$html .= '
					<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["pages_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table["pages_id"])).'</strong>?">
					<i class="fa fa-times"></i>
					</button>';
						}
			
					}

					$html .= '
				</div>
			</div>';
						if (count($data_table["pages_translation"]) > 0) {
							for($j=0;$j<count($data_table["pages_translation"]);$j++){
				$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["pages_translation"][$j]["languages_short_name"]).'.gif" alt="'.$data_table["pages_translation"][$j]["languages_name"].'" title="'.$data_table["pages_translation"][$j]["languages_name"].'" > ';
								$html .= '
			<div id="datatable-pages_actions-'.$data_table["pages_translation"][$j]["pages_id"].'" class="datatable-language-sub">'.$flag_translate.'
				<div class="btn-group">';

								$html .= '
					<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["pages_translation"][$j]["pages_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
					<i class="fa fa-eye"></i>
					</button>';
						
								if ($_SESSION["users_level"] != "5") {
			
										if (!isset($data_table["pages_translation"][$j]["pages_protected"]) || (isset($data_table["pages_translation"][$j]["pages_protected"]) && $data_table["pages_translation"][$j]["pages_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {
			
										$html .= '
					<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["pages_translation"][$j]["pages_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
					<i class="fa fa-pencil"></i>
					</button>';
					
										}

										$html .= '
					<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["pages_translation"][$j]["pages_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
					<i class="fa fa-copy"></i>
					</button>';

										$html .= '
					<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["pages_translation"][$j]["pages_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table["pages_translation"][$j][$table_module_field_unassoc[$table_module_field_key]])).'</strong>?">
					<i class="fa fa-times"></i>
					</button>';
			
								}

							$html .= '
				</div>
			</div>';
							}
						}

						$html .= '
			<div id="datatable-pages_actions-translate" class="datatable-language-sub">';
					
						if ($_SESSION["users_level"] != "5") {
					
							for($k=0;$k<count($languages);$k++) {
								if ($data_table["languages_short_name"] != $languages[$k]["languages_short_name"]) {
									$translation_button_activate = true;
									for($j=0;$j<count($data_table["pages_translation"]);$j++){
										if ($languages[$k]["languages_short_name"] == $data_table["pages_translation"][$j]["languages_short_name"]) {
										$translation_button_activate = false;
									}
								}
								if ($translation_button_activate) {
									$flag_translate = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($languages[$k]["languages_short_name"]).'.gif" alt="'.$languages[$k]["languages_name"].'" title="'.$languages[$k]["languages_name"].'" > ';
									$html .= '
				<button class="btn btn-xs btn-default btn-translate" type="button" data-target="'.$data_table["pages_id"].'" data-language="'.$languages[$k]["languages_short_name"].'" data-toggle="tooltip" title="Translate into '.$languages[$k]["languages_name"].'">'.$flag_translate.'<i class="fa fa-plus"></i></button>';
								}
							}
							
						}
							
					}
					$html .= '
			</div>';

					$html .= '
		</td>';
			
			}
			
		}
	}

	unset($data_table);
	$data_table = array();

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_pages_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_pages_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Description")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Meta Keywords")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Template")));
			} else {
				$column_name = str_replace("pages", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "pages_actions") {
				if ($key == "pages_id") {
					if (!empty($data_table["pages_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_title") {
					if ($data_table["pages_title"] != "") {
						$table_row_value = '<strong>' . translate("Title") . '</strong><br />' . $data_table["pages_title"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_link") {
					if ($data_table["pages_link"] != "") {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br />' . $data_table["pages_link"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if ($data_table["pages_meta_title"] != "") {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />' . $data_table["pages_meta_title"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if ($data_table["pages_meta_description"] != "") {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />' . $data_table["pages_meta_description"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if ($data_table["pages_meta_keyword"] != "") {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />' . $data_table["pages_meta_keyword"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_template") {
					if ($data_table["pages_template"] != "") {
						$table_row_value = '<strong>' . translate("Template") . '</strong><br />'.render_pages_view_template_file($data_table["pages_template"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "pages_date_created") {
					if (!empty($data_table["pages_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["pages_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_pages_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_pages_table_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_pages_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_pages_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_pages_view_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_pages_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_pages_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_pages_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "pages" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '9'
	AND (`log_function` = 'create_pages_data' OR `log_function` = 'update_pages_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "pages" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '9'
	AND (`log_function` = 'create_pages_data' OR `log_function` = 'update_pages_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_pages_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "pages" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["pages_meta_description"]) && !empty($query_log_content_hash["pages_meta_description"])) {
			$query_log_content_hash["pages_meta_description"] = str_replace("<br />", "\n", $query_log_content_hash["pages_meta_description"]);
		}
		if (isset($query_log_content_hash["pages_template"]) && !empty($query_log_content_hash["pages_template"])) {
			$query_log_content_hash["pages_template_path"] = $configs["base_url"]."/media/user/".$query_log_content_hash["pages_template"];
		}
			$pages_translation = get_pages_translation_data_all($query_log_content_hash["pages_id"]);
			$query_log_content_hash["pages_translation"] = $pages_translation;

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_pages_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "pages" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;

	} else {

		$response["status"] = false;
		$response["target"] = "pages";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>pages</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

?>