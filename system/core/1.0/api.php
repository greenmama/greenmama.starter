<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "authorize_api_credential" 
		|| $method == "check_api_source" 
		|| $method == "check_api_modules_permission" 
		|| $method == "get_api_data_by_id" 
		|| $method == "get_api_data_all" 
		|| $method == "count_api_data_all" 
		|| $method == "get_api_data_all_excluding" 
		|| $method == "count_api_data_all_excluding" 
		|| $method == "get_api_data_table_all" 
		|| $method == "count_api_data_table_all" 
		|| $method == "get_search_api_data_table_all" 
		|| $method == "count_search_api_data_table_all" 
		|| $method == "create_api_data" 
		|| $method == "update_api_data" 
		|| $method == "patch_api_data" 
		|| $method == "delete_api_data" 
		|| $method == "create_api_table" 
		|| $method == "create_api_table_row" 
		|| $method == "update_api_table_row" 
		|| $method == "delete_api_table_row" 
		|| $method == "create_api_table_row" 
		|| $method == "update_api_table_row" 
		|| $method == "inform_api_table_row" 
		|| $method == "view_api_table_row" 
		|| $method == "get_api_edit_log_data" 
		|| $method == "count_api_edit_log_data" 
		|| $method == "get_api_edit_log_data_by_id" 
		|| $method == "count_api_edit_log_data_by_id"
		
		|| $method == "get_api_data_all_by_api_allowed_modules" 
		|| $method == "count_api_data_all_by_api_allowed_modules" 
		|| $method == "get_api_allowed_modules_data_dynamic_list" 
		|| $method == "count_api_allowed_modules_data_dynamic_list" 
		|| $method == "get_api_allowed_modules_data_dynamic_list_by_id")
) {
	
	/* include: configurations */
	require_once("../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");

	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "authorize_api_credential"){
		authorize_api_credential($parameters["api_key"], $parameters["api_secret"]);
	} else if ($method == "check_api_source"){
		check_api_source();
	} else if ($method == "check_api_modules_permission"){
		check_api_modules_permission($parameters["api_id"], $parameters["modules_id"]);
	} else if ($method == "get_api_data_by_id"){
		get_api_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_api_data_by_key"){
		get_api_data_by_key($parameters, $_POST["activate"]);
	} else if ($method == "get_api_data_all"){
		get_api_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_api_data_all"){
		count_api_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_api_data_all_excluding"){
		get_api_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_api_data_all_excluding"){
		count_api_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_api_data_table_all"){
		get_api_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_api_data_table_all"){
		count_api_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_api_data_table_all"){
		get_search_api_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_api_data_table_all"){
		count_search_api_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_api_data"){
		create_api_data($parameters);
	} else if ($method == "update_api_data"){
		update_api_data($parameters);
	} else if ($method == "patch_api_data"){
		patch_api_data($parameters);
	} else if ($method == "delete_api_data"){
		delete_api_data($parameters);
	} else if ($method == "create_api_table"){
		create_api_table($parameters, $table_module_field); 
	} else if ($method == "create_api_table_row"){
		create_api_table_row($parameters, $table_module_field);
	} else if ($method == "update_api_table_row"){
		update_api_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_api_table_row"){
		inform_api_table_row($parameters, $table_module_field);
	} else if ($method == "view_api_table_row"){
		view_api_table_row($parameters, $table_module_field);
	} else if ($method == "get_api_edit_log_data"){
		get_api_edit_log_data($parameters);
	} else if ($method == "count_api_edit_log_data"){
		count_api_edit_log_data($parameters);
	} else if ($method == "get_api_edit_log_data_by_id"){
		get_api_edit_log_data_by_id($parameters);
	} else if ($method == "count_api_edit_log_data_by_id"){
		count_api_edit_log_data_by_id($parameters);
	} else if ($method == "get_api_data_all_by_api_allowed_modules"){
		get_api_data_all_by_api_allowed_modules($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_api_data_all_by_api_allowed_modules"){
		count_api_data_all_by_api_allowed_modules($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_api_allowed_modules_data_dynamic_list"){
		get_api_allowed_modules_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_api_allowed_modules_data_dynamic_list"){
		count_api_allowed_modules_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_api_allowed_modules_data_dynamic_list_by_id"){
		get_api_allowed_modules_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	}
}


function authorize_api_credential($api_key, $api_secret) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$api_key = escape_string($con, $api_key);
	$api_secret = escape_string($con, $api_secret);

	$sql = "SELECT * 
			FROM   `api` 
			WHERE  `api_key` = '".$api_key."' 
				AND  `api_secret` = '".$api_secret."' 
				AND  `api_activate` = true 
			LIMIT  1";
	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);
	
	if ($result) {
		
		if ($num == 1) {
			
			$response["status"] = true;
			$response["message"] = translate("Valid Credential");
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Invalid Credential");
			
		}
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>API</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}
	
	mysqli_free_result($result);
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function check_api_modules_permission($api_id, $modules_id) {
	
	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$api_id = escape_string($con, trim($api_id));
	$modules_id = escape_string($con, trim($modules_id));

		
	$sql_modules = "
	SELECT `api_allowed_modules`
	FROM   `api` 
	WHERE  `api_id` = '".$api_id."' 
	LIMIT  1";
	$result_modules = mysqli_query($con, $sql_modules);
	$num_modules = mysqli_num_rows($result_modules);
	
	if ($result_modules) {
	
		if ($num_modules > 0) {

			$query_modules = mysqli_fetch_array($result_modules);
			$module_list = unserialize($query_modules["api_allowed_modules"]);

			$sql = "SELECT `modules_id`  
					FROM   `modules` 
					WHERE  `modules_id` = '".$modules_id."' 
						AND `modules_activate` = true 
					LIMIT  1";
			$result = mysqli_query($con, $sql);
			$num = mysqli_num_rows($result);

			if ($result) {

				$query = mysqli_fetch_array($result);

				$module_id = $query['modules_id']; 

				if (count($module_list) > 0 && $module_list[0] != "") {
					
					if(in_array($module_id, $module_list)){
						
						$response["status"] = true;
						$response["message"] = translate("Found modules");
						
					} else { 
						
						$response["status"] = false;
						$response["message"] = translate("Not found");
						
					}
					
				} else {
					
					$response["status"] = false;
					$response["message"] = translate("Empty data");
					
				}
				
				mysqli_free_result($result);

			} else {
				
				$response["status"] = false;
				$response["target"] = "modules";
				if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
					$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
				} else {
					$response["message"] = translate("Database encountered error");
				}
				
			}
			
			mysqli_free_result($result_modules);

		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Empty data");
			
		}
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>API</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}

	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function check_api_source() {
	
	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);

		
	if (!isset($configs["allowed_remote_address"]) || empty($configs["allowed_remote_address"])) {
		$allowed_remote_address = array("127.0.0.1", "::1");
	} else {
		$allowed_remote_address = explode(",", $configs["allowed_remote_address"]);
	}
	if(in_array($_SERVER["REMOTE_ADDR"], $allowed_remote_address)){
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if(in_array($_SERVER["HTTP_CLIENT_IP"], $allowed_remote_address)){
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if(in_array($_SERVER["HTTP_X_FORWARDED_FOR"], $allowed_remote_address)){
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if(in_array((isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[SERVER_NAME]", $allowed_remote_address)){
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if(in_array((isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]", $allowed_remote_address)){
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if ((!isset($_POST["api_id"]) && !isset($_POST["modules_id"])) &&
				check_api_modules_permission(
					escape_string($con, $_POST["api_id"]), 
					escape_string($con, $_POST["modules_id"]))) {
		
		$response["status"] = true;
		$response["message"] = translate("Valid source");
		
	} else if (authorize_api_credential($configs["api_key"], $configs["api_secret"])) {

		$response["status"] = true;
		$response["message"] = translate("Valid source");

	} else {
		
		$response["status"] = false;
		$response["message"] = translate("Invalid source");
		
	}
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function get_api_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "api" */
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api`.`api_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
			$query["api_date_created_formatted"] = datetime_reformat($query["api_date_created"]);
			
		if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
			$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
			for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
				if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
					foreach($query["api_allowed_modules"][$j] as $key => $value) {
						$query["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
		}
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "api_id",
					"modules_record_target" => $target,
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_data_by_key($target, $activate = ""){
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "api" */
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api`.`api_key` = '" . $target . "'
	".$extra_sql_data_all."
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
			$query["api_date_created_formatted"] = datetime_reformat($query["api_date_created"]);
			
			if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
				$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
				for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
					if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
						foreach($query["api_allowed_modules"][$j] as $key => $value) {
							$query["api_allowed_modules"][$j][] = $value;
						}
					}
				}
				$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
			}
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by key",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "api_id",
					"modules_record_target" => $target,
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>API</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_data_all($start = 0, $limit = "", $sort_by = "api_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "api" */
	$sql = "
	SELECT *
	FROM   `api`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
		if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
			$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
			for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
				if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
					foreach($query["api_allowed_modules"][$j] as $key => $value) {
						$query["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "api" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "api_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "api" */
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
		if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
			$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
			for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
				if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
					foreach($query["api_allowed_modules"][$j] as $key => $value) {
						$query["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "api_id",
					"modules_record_target" => $target,
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "api" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `api`
	WHERE `api_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "api_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "api" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
		if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
			$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
			for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
				if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
					foreach($query["api_allowed_modules"][$j] as $key => $value) {
						$query["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "api" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_api_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "api_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'api'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "api_actions" 
				&& $key != "api_date_created" 
				&& $key != "api_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "api" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
		if (isset($query["api_allowed_modules"]) && !empty($query["api_allowed_modules"])) {
			$query["api_allowed_modules"] = unserialize($query["api_allowed_modules"]);
			for ($j = 0; $j < count($query["api_allowed_modules"]); $j++) {
				if (!empty($query["api_allowed_modules"][$j]) && is_array($query["api_allowed_modules"][$j])) {
					foreach($query["api_allowed_modules"][$j] as $key => $value) {
						$query["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query["api_allowed_modules_json"] = json_encode($query["api_allowed_modules"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_api_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'api'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "api_actions" 
				&& $key != "api_date_created" 
				&& $key != "api_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "api" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `api_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `api`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_api_data($parameters){

	/* get global: configurations */
	global $configs;
	$api_configs = get_modules_data_by_id("12");

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["api_activate"] == "1") {
	
		if(!isset($parameters["api_title"]) || empty($parameters["api_title"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is required");
			$response["target"] = "#api_title";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["api_title"]) > 240 && $parameters["api_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#api_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["api_title"]) > 240 && $parameters["api_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#api_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}

	}

	/* initialize: default */
	$parameters["api_allowed_modules"] = stripslashes(trim(serialize($parameters["api_allowed_modules"])));
	date_default_timezone_set($configs["timezone"]);
	$parameters["api_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */
	$api_key_seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
	shuffle($api_key_seed);
	$api_key_random = "";
	foreach (array_rand($api_key_seed, 20) as $k) $api_key_random .= $api_key_seed[$k];
	$parameters["api_key"] = $api_key_random;

	$api_secret_seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789!@#$%^&*()');
	shuffle($api_secret_seed);
	$api_secret_random = "";
	foreach (array_rand($api_secret_seed, 8) as $k) $api_secret_random .= $api_secret_seed[$k];
	$parameters["api_secret"] = $api_secret_random;

	/* database: insert to module "api" (begin) */
	$sql = "INSERT INTO `api` (
				`api_id`,
				`api_title`,
				`api_key`,
				`api_secret`,
				`api_allowed_modules`,
				`api_date_created`,
				`api_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["api_title"] . "',
				'" . $parameters["api_key"] . "',
				'" . $parameters["api_secret"] . "',
				'" . $parameters["api_allowed_modules"] . "',
				'" . $parameters["api_date_created"] . "',
				'" . $parameters["api_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "')";
	$result = mysqli_query($con, $sql);
	$parameters["api_id"] = mysqli_insert_id($con);
	/* database: insert to module "api" (end) */

	if ($result) {
		

		/* response: additional data */
		

		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "api_id",
				"modules_record_target" => $parameters["api_id"],
				"modules_id" => "12",
				"modules_name" => "API",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_api_data($parameters){

	/* get global: configurations */
	global $configs;
	$api_configs = get_modules_data_by_id("12");

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["api_activate"] == "1") {
	
		if(!isset($parameters["api_title"]) || empty($parameters["api_title"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is required");
			$response["target"] = "#api_title";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["api_title"]) > 240 && $parameters["api_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)");
				$response["target"] = "#api_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["api_title"]) > 240 && $parameters["api_title"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Title") . "</strong> " . translate("is longer than maximum length of strings at") . " 240 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#api_title";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}

	/* database: get old data from module "api" (begin) */
	$sql = "SELECT *
	        FROM   `api`
			WHERE  `api_id` = '" . $parameters["api_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["api_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "api" (end) */
	
	/* initialize: default */
	$parameters["api_allowed_modules"] = stripslashes(trim(serialize($parameters["api_allowed_modules"])));
	
	$parameters["api_date_created"] = $query["api_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	/* database: update to module "api" (begin) */
	$sql = "UPDATE `api`
			SET 
				   `api_title` = '" . $parameters["api_title"] . "',
				   `api_allowed_modules` = '" . $parameters["api_allowed_modules"] . "',
				   `api_date_created` = '" . $parameters["api_date_created"] . "',
				   `api_activate` = '" . $parameters["api_activate"] . "'
			WHERE  `api_id` = '" . $parameters["api_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "api" (end) */

	if ($result) {
		
		/* response: additional data */
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "api_id",
				"modules_record_target" => $parameters["api_id"],
				"modules_id" => "12",
				"modules_name" => "API",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "api";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_api_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "api" (begin) */
	$sql = "SELECT *
	        FROM   `api`
			WHERE  `api_id` = '" . $parameters["api_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["api_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "api" (end) */
	
	/* initialize: default */
	$parameters["api_date_created"] = $query["api_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "api" (begin) */
	$sql = "UPDATE `api`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `api_id` = '" . $parameters["api_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "api" (end) */

	if ($result) {
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "api_id",
				"modules_record_target" => $parameters["api_id"],
				"modules_id" => "12",
				"modules_name" => "API",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "api";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_api_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["api_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "api" (begin) */
		$sql = "
		DELETE FROM `api`
		WHERE `api_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "api" (end) */

		if ($result) {
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "api_id",
					"modules_record_target" => $target,
					"modules_id" => "12",
					"modules_name" => "API",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_api_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "API Key")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "API Secret")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Allowed Modules")));
			} else {
				$column_name = str_replace("api", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "api_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "api_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "api_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "api_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "api_title") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . api_title . '" data-toggle="tooltip" title="Title" data-original-title="Title">' . htmlspecialchars(strip_tags(translate("Title"))) . '</th>';
			} else if ($key == "api_key") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . api_key . '" data-toggle="tooltip" title="API Key" data-original-title="API Key">' . htmlspecialchars(strip_tags(translate("API Key"))) . '</th>';
			} else if ($key == "api_secret") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . api_secret . '" data-toggle="tooltip" title="API Secret" data-original-title="API Secret">' . htmlspecialchars(strip_tags(translate("API Secret"))) . '</th>';
			} else if ($key == "api_allowed_modules") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . api_allowed_modules . '" data-toggle="tooltip" title="Allowed Modules" data-original-title="Allowed Modules">' . htmlspecialchars(strip_tags(translate("Allowed Modules"))) . '</th>';
			} else if ($key == "api_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "api_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["api_id"].'" data-target="'.$data_table_row["api_id"].'">';

		$html .= inform_api_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_api_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "api_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "api_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "api_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "api_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "api_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "api_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_api_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_api_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_api_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_api_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_api_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_api_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_api_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_api_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_api_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_api_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_api_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_api_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["api_id"].'" data-target="'.$data_table["api_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_api_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_api_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_api_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "api_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "api_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "api_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "api_actions") {
				if ($key == "api_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "api_title") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["api_title"]));
				} else if ($key == "api_key") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["api_key"]));
				} else if ($key == "api_secret") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["api_secret"]));
				} else if ($key == "api_allowed_modules") {
					$td_align_class = "";
					$con = start();
					if (count($data_table["api_allowed_modules"]) > 0 && is_array($data_table["api_allowed_modules"]) && $data_table["api_allowed_modules"] != "") {
						$api_allowed_modules_sql = "`modules_id` = '" . implode("' OR `modules_id` = '", $data_table["api_allowed_modules"]) . "'";
						$sql_api_allowed_modules  = "
						SELECT `modules_id`, `modules_name`
						FROM   `modules`
						WHERE  " . $api_allowed_modules_sql;
						$result_api_allowed_modules = mysqli_query($con, $sql_api_allowed_modules);
						$num_api_allowed_modules = mysqli_num_rows($result_api_allowed_modules);
						if ($num_api_allowed_modules > 0) {
							$module_link = false;
							unset($queries_api_allowed_modules);
							$queries_api_allowed_modules = array();
							$k = 0;
							while($query_api_allowed_modules = mysqli_fetch_array($result_api_allowed_modules)) {
								$queries_api_allowed_modules[$k] = $query_api_allowed_modules;
								$k++;
							}
							for($k = 0; $k < count($queries_api_allowed_modules); $k++) {
								foreach (array_keys($queries_api_allowed_modules[$k]) as $pages_key) {
									if ($pages_key == "pages_link" && !empty($pages_key)) {
										$module_link = true;
									}
								}
							}
							unset($queries_api_allowed_modules_list);
							$queries_api_allowed_modules_list = array();
							for($k = 0; $k < count($queries_api_allowed_modules); $k++) {
								if ($module_link) {
									$queries_api_allowed_modules_list[$k] = '<a href="'.rewrite_url($queries_api_allowed_modules[$k]['pages_link']).'" target="_blank">' . htmlspecialchars(strip_tags($queries_api_allowed_modules[$k]['modules_name'])) . '</a>';
								} else {
									$queries_api_allowed_modules_list[$k] = htmlspecialchars(strip_tags($queries_api_allowed_modules[$k]['modules_name'])) . '';
								}
							}
							$table_row_value = implode(", ", $queries_api_allowed_modules_list);
						}
						mysqli_free_result($result_api_allowed_modules);
					}
					stop($con);
				} else if ($key == "api_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_short_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="users.php?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_api_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["api_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "api_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}

				$html .= '
				<td id="datatable-actions-'.$data_table["api_id"].'" class="text-center">
					<div class="btn-group">';

				$html .= '
						<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["api_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
						<i class="fa fa-eye"></i>
						</button>';
				
				if ($_SESSION["users_level"] != "5") {

		
			
						if (!isset($data_table["api_protected"]) || (isset($data_table["api_protected"]) && $data_table["api_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

						$html .= '
						<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["api_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
						<i class="fa fa-pencil"></i>
						</button>';
						
						}

						$html .= '
						<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["api_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
						<i class="fa fa-copy"></i>
						</button>';

						$html .= '
						<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["api_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?">
						<i class="fa fa-times"></i>
						</button>';
			
				}

				$html .= '
					</div>
				</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_api_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_api_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Title")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "API Key")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "API Secret")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Allowed Modules")));
			} else {
				$column_name = str_replace("api", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "api_actions") {
				if ($key == "api_id") {
					if (!empty($data_table["api_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "api_title") {
					if ($data_table["api_title"] != "") {
						$table_row_value = '<strong>' . translate("Title") . '</strong><br />' . $data_table["api_title"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "api_key") {
					if ($data_table["api_key"] != "") {
						$table_row_value = '<strong>' . translate("API Key") . '</strong><br />' . $data_table["api_key"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "api_secret") {
					if ($data_table["api_secret"] != "") {
						$table_row_value = '<strong>' . translate("API Secret") . '</strong><br />' . $data_table["api_secret"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "api_allowed_modules") {
					if ($data_table["api_allowed_modules"] != "") {
						$con = start();
						if (count($data_table["api_allowed_modules"]) > 0 && is_array($data_table["api_allowed_modules"]) && $data_table["api_allowed_modules"] != "") {
							$api_allowed_modules_sql = "`modules_id` = '" . implode("' OR `modules_id` = '", $data_table["api_allowed_modules"]) . "'";
							$sql_api_allowed_modules  = "
							SELECT `modules_id`, `modules_name`
							FROM   `modules`
							WHERE  " . $api_allowed_modules_sql;
							$result_api_allowed_modules = mysqli_query($con, $sql_api_allowed_modules);
							$num_api_allowed_modules = mysqli_num_rows($result_api_allowed_modules);
							if ($num_api_allowed_modules > 0) {
								unset($queries_api_allowed_modules);
								$queries_api_allowed_modules = array();
								$k = 0;
								while($query_api_allowed_modules = mysqli_fetch_array($result_api_allowed_modules)) {
									$queries_api_allowed_modules[$k] = $query_api_allowed_modules;
									$k++;
								}
								for($k = 0; $k < count($queries_api_allowed_modules); $k++) {
									foreach (array_keys($queries_api_allowed_modules[$k]) as $pages_key) {
										if ($pages_key == "pages_link" && !empty($pages_key)) {
											$module_link = true;
										}
									}
								}
								unset($queries_api_allowed_modules_list);
								$queries_api_allowed_modules_list = array();
								for($k = 0; $k < count($queries_api_allowed_modules); $k++) {
									if ($module_link) {
										$queries_api_allowed_modules_list[$k] = '<a href="'.rewrite_url($queries_api_allowed_modules[$k]['pages_link']).'" target="_blank">' . htmlspecialchars(strip_tags($queries_api_allowed_modules[$k]['modules_name'])) . '</a>';
									} else {
										$queries_api_allowed_modules_list[$k] = htmlspecialchars(strip_tags($queries_api_allowed_modules[$k]['modules_name']));
									}
								}
								$table_row_value = '<strong>' . translate("Allowed Modules") . '</strong><br />' . implode(", ", $queries_api_allowed_modules_list) . '<br /><br />';
							}
							mysqli_free_result($result_api_allowed_modules);
						}
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "api_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "api_date_created") {
					if (!empty($data_table["api_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["api_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_api_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_api_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_api_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_api_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_api_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_api_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "api" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '12'
	AND (`log_function` = 'create_api_data' OR `log_function` = 'update_api_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "api" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '12'
	AND (`log_function` = 'create_api_data' OR `log_function` = 'update_api_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "api" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["api_allowed_modules"]) && !empty($query_log_content_hash["api_allowed_modules"])) {
			
			for ($j = 0; $j < count($query_log_content_hash["api_allowed_modules"]); $j++) {
				if (!empty($query_log_content_hash["api_allowed_modules"][$j]) && is_array($query_log_content_hash["api_allowed_modules"][$j])) {
					foreach($query_log_content_hash["api_allowed_modules"][$j] as $key => $value) {
						$query_log_content_hash["api_allowed_modules"][$j][] = $value;
					}
				}
			}
			$query_log_content_hash["api_allowed_modules_json"] = json_encode($query_log_content_hash["api_allowed_modules"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "api" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;

	} else {

		$response["status"] = false;
		$response["target"] = "api";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>api</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_data_all_by_api_allowed_modules($target, $start = 0, $limit = "", $sort_by = "api_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	$target_serialized = serialize($target);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `api`
	WHERE `api_allowed_modules` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["api_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["api_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "api";
		$response["message"] = translate("Database encountered error");
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_data_all_by_api_allowed_modules($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	$target_serialized = serialize($target);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `api_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `api_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "api" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `api`
	WHERE `api_allowed_modules` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `api`
	WHERE `api_allowed_modules` LIKE '%" . $target_serialized . "%'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "api";
		$response["message"] = translate("Database encountered error");
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_api_allowed_modules_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["allowed_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["allowed_modules_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "api";
		$response["message"] = translate("Database encountered error");
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_api_allowed_modules_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_id` != '0'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "api";
		$response["message"] = translate("Database encountered error");
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}


function get_api_allowed_modules_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` = '".$target."'
	".$extra_sql_data_dynamic_list."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["allowed_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["allowed_modules_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "api";
		$response["message"] = translate("Database encountered error");
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}
?>