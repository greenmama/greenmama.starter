<?php
function datetime_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strDay . " " . $strMonthThai . " " . $strYear . " - " . $strHour . ":" . $strMinute . "";
    } else {
        return "-";
    }
}

function datetime_reformat_plussecond($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strDay . " " . $strMonthThai . " " . $strYear . " - " . $strHour . ":" . $strMinute . ":" . $strSeconds . "";
    } else {
        return "-";
    }
}

function datetime_short_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("Jan"),
            translate("Feb"),
            translate("Mar"),
            translate("Apr"),
            translate("May"),
            translate("Jun"),
            translate("Jul"),
            translate("Aug"),
            translate("Sep"),
            translate("Oct"),
            translate("Nov"),
            translate("Dec")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strDay . " " . $strMonthThai . " " . $strYearI . " (" . $strHour . ":" . $strMinute . ")";
        
    } else {
        return "-";
    }
}

function date_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strDay . " " . $strMonthThai . " " . $strYearI;
        
    } else {
        return "-";
    }
}

function date_short_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("Jan"),
            translate("Feb"),
            translate("Mar"),
            translate("Apr"),
            translate("May"),
            translate("Jun"),
            translate("Jul"),
            translate("Aug"),
            translate("Sep"),
            translate("Oct"),
            translate("Nov"),
            translate("Dec")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strDay . " " . $strMonthThai . " " . $strYearI;
        
    } else {
        return "-";
    }
}

function timedate_short_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strHour . ":" . $strMinute . " - " . $strDay . " " . $strMonthThai . " " . $strYearI;
        
    } else {
        return "-";
    }
}

function time_short_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear      = date("Y", strtotime($datetime));
        $strYearI     = date("Y", strtotime($datetime));
        $strMonth     = date("n", strtotime($datetime));
        $strDay       = date("j", strtotime($datetime));
        $strHour      = date("H", strtotime($datetime));
        $strMinute    = date("i", strtotime($datetime));
        $strSeconds   = date("s", strtotime($datetime));
        $strMonthThai = $strMonthCut[$strMonth];
        
        return $strHour . ":" . $strMinute . "";
        
    } else {
        return "-";
    }
}

function monthyear_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        return $strMonthThai . " " . $strYearI;
    } else {
        return "-";
    }
}

function year_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        return $strYear;
    } else {
        return "-";
    }
}

function month_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("January"),
            translate("February"),
            translate("March"),
            translate("April"),
            translate("May"),
            translate("June"),
            translate("July"),
            translate("August"),
            translate("September"),
            translate("October"),
            translate("November"),
            translate("December")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        return $strMonthThai;
    } else {
        return "-";
    }
}

function month_short_reformat($datetime) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" 
    && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        $strMonthCut = array(
            "",
            translate("Jan"),
            translate("Feb"),
            translate("Mar"),
            translate("Apr"),
            translate("May"),
            translate("Jun"),
            translate("Jul"),
            translate("Aug"),
            translate("Sep"),
            translate("Oct"),
            translate("Nov"),
            translate("Dec")
        );
        
        $strMonthThai = $strMonthCut[$strMonth];
        return $strMonthThai;
    } else {
        return "-";
    }
}

function week_of_month_reformat($date) {
    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" 
    && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        $date_parts     = explode('-', $date);
        $date_parts[2]  = '01';
        $first_of_month = implode('-', $date_parts);
        $day_of_first   = date('N', strtotime($first_of_month));
        $day_of_month   = date('j', strtotime($date));
        return floor(($day_of_first + $day_of_month - 1) / 7) + 1;
    } else {
        return "-";
    }
}

function datetime_convert($datetime_source, $datetimezone_source) {
    if (!empty($datetime_source) && !empty($datetimezone_source)) {
        $datetime_temp = new DateTime($datetime_source . " +00");
        $datetime_temp->setTimezone($datetimezone_source);
        $datetime_new = $datetime_temp->format('Y-m-d H:i:s');
        return $datetime_new;
    } else {
        return "-";
    }
}

function datetime_revert($datetime_source, $datetimezone_source) {
    if (!empty($datetime_source) && !empty($datetimezone_source)) {
        $datetime_temp = new DateTime($datetime_source . " " . $datetimezone_source);
        $datetime_temp->setTimezone(new DateTimeZone('UTC'));
        $datetime_new = $datetime_temp->format('Y-m-d H:i:s');
        return $datetime_new;
    } else {
        return "-";
    }
}

function datetime_reformat_timeago($datetime, $full = false) {
    
    /* get global: configurations */
    global $configs;
    
    /* get global: ajax function */
    global $call_function;
    
    $response = array();
    
    /* initialize: clean strings */
    $datetime = escape_string($datetime);
    $full     = escape_string($full);

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" 
    && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
    
        $now = new DateTime;
        $ago = new DateTime($datetime);
        
        $diff = $now->diff($ago);
        
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        $final_string = $string ? implode(', ', $string) . ' ago' : 'now';

    } else {

        $final_string = "-";

    }
    
    $response["status"] = true;
    $response["values"] = $final_string;
    
    if (isset($call_function) && $call_function == "datetime_reformat_timeago") {
        echo json_encode($response);
        unset($response);
        $response = array();
        unset($call_function);
        $call_function = array();
    } else {
        return $response["values"];
        unset($response);
        $response = array();
        unset($call_function);
        $call_function = array();
    }
    
}

function datetime_reformat_timeleft($datetime, $full = false) {
    
    /* get global: configurations */
    global $configs;
    
    /* get global: ajax function */
    global $call_function;
    
    $response = array();
    
    /* initialize: clean strings */
    $datetime = escape_string($datetime);
    $full     = escape_string($full);

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" 
    && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
    
        $now    = new DateTime;
        $expire = new DateTime($datetime);
        
        $diff = $expire->diff($now);
        
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        
        if (!$full) $string = array_slice($string, 0, 1);
        $final_string = $string ? implode(', ', $string) . '' : 'time is up';

    } else {

        $final_string = "-";

    }
    
    $response["status"] = true;
    $response["values"] = $final_string;
    
    if (isset($call_function) && $call_function == "datetime_reformat_timeleft") {
        echo json_encode($response);
        unset($response);
        $response = array();
        unset($call_function);
        $call_function = array();
    } else {
        return $response["values"];
        unset($response);
        $response = array();
        unset($call_function);
        $call_function = array();
    }
    
}
function datetime_short_format($datetime, $lang) {

    global $configs;

    if ($datetime != "" && $datetime != NULL 
    && $datetime != "0000-00-00 00:00:00" 
    && $datetime != "0000-00-00" 
    && $datetime[0] != "-") {
        
        date_default_timezone_set($configs["timezone"]);
        $strYear    = date("Y", strtotime($datetime));
        $strYearI   = date("Y", strtotime($datetime));
        $strMonth   = date("n", strtotime($datetime));
        $strDay     = date("j", strtotime($datetime));
        $strHour    = date("H", strtotime($datetime));
        $strMinute  = date("i", strtotime($datetime));
        $strSeconds = date("s", strtotime($datetime));
        
        if ($lang == 'EN') {
            $strMonthCut = Array(
                "",
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            );
        } else if ($lang == 'TH') {
            $strMonthCut = Array(
                "",
                "ม.ค.",
                "ก.พ.",
                "มี.ค.",
                "เม.ย.",
                "พ.ค.",
                "มิ.ย.",
                "ก.ค.",
                "ส.ค.",
                "ก.ย.",
                "ต.ค.",
                "พ.ย.",
                "ธ.ค."
            );
        } else {
            $strMonthCut = Array(
                "",
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            );
        }
        
        $strMonthThai = $strMonthCut[$strMonth];
        
        if ($lang == 'EN') {
            return $strDay . " " . $strMonthThai . " " . $strYearI . "";
        } else if ($lang == 'TH') {
            return $strDay . " " . $strMonthThai . " " . $strYear . "";
        } else {
            $strMonthCut = Array(
                "",
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            );
        }
        
        
    } else {
        return "-";
    }
}
?>