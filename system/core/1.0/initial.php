<?php 
set_time_limit(0);
ini_set('display_errors', 'Off');
ini_set('error_reporting', E_ALL);
mb_internal_encoding($configs["encoding"]);   
date_default_timezone_set($configs["timezone"]);
$hour_zone = round((date('Z')/60)/60);
$configs["datetimezone"] = new DateTimeZone($configs["timezone"]);
if (date('Z') >= 0) {
	$configs["timezone_difference"] = "+".sprintf("%02d", $hour_zone);
} else {
	$configs["timezone_difference"] = "-".sprintf("%02d", $hour_zone);
}

$base_last_string_position = strlen($configs["base_url"]) - 1;
if ($configs["base_url"][$base_last_string_position] == "/") {
	$configs["base_url"] = rtrim($configs["base_url"], "/");
}

$backend_last_string_position = strlen($configs["backend_url"]) - 1;
if ($configs["backend_url"][$backend_last_string_position] == "/") {
	$configs["backend_url"] = rtrim($configs["backend_url"], "/");
}

$configs["backend_folder"] = $configs["backend_url"];
$backend_folder_last_string_position = strlen($configs["backend_folder"]) - 1;
if ($configs["backend_folder"][$backend_folder_last_string_position] != "/") {
	$configs["backend_folder"] = $configs["backend_folder"] . "/";
}

$configs["website_url"] = $configs["base_url"].'/';

$configs["template_folder"] = 'templates/'.$configs["template"].'/';
$configs["upload_folder"] = 'media/';
$configs["system_folder"] = 'system/';
$configs["system_core_folder"] = 'system/core/'.$configs["version"].'/';
$configs["backend_system_folder"] = $configs["backend_folder"].'/system/';
$configs["backend_system_core_folder"] = $configs["backend_folder"].'/system/core/'.$configs["version"].'/';

$configs["template_path"] = $configs["website_url"].$configs["template_folder"];
$configs["upload_path"] = realpath($configs["website_url"].$configs["upload_folder"]);
$configs["system_path"] = $configs["website_url"].$configs["system_folder"];
$configs["system_core_path"] = $configs["website_url"].$configs["system_core_folder"];
$configs["backend_system_path"] = $configs["website_url"].$configs["backend_system_folder"];
$configs["backend_system_core_path"] = $configs["website_url"].$configs["backend_system_core_path"];

$configs["encrypt_key_filemanger"] = 'dd17e9c5f93007885229a2049be6a678';

/* open database connection function */
function start() {
	
	global $configs;
	global $database_access;

	$connect = false;
	
    $connect = mysqli_connect($database_access["host"], $database_access["login"], $database_access["password"], $database_access["name"]);
	if ($connect) {
		if (!mysqli_select_db($connect, $database_access["name"])) {
			$connect = false;
		} else {
			if (!mysqli_set_charset($connect, str_replace("-", "", strtolower($configs["encoding"])))) {
				$connect = false;
			}
		}
	} else {
		$connect = false;
	}
	
	return $connect;
	
}

/* close database connection function */
function stop($connect) {
	mysqli_close($connect);
}
?>