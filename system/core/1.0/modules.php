<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "get_modules_data_by_id" 
		|| $method == "get_modules_data_by_link" 
		|| $method == "get_modules_data_all" 
		|| $method == "count_modules_data_all" 
		|| $method == "get_modules_data_all_excluding" 
		|| $method == "count_modules_data_all_excluding" 
		|| $method == "get_modules_main_language_data_all" 
		|| $method == "count_modules_main_language_data_all" 
		|| $method == "get_modules_main_language_data_all_excluding" 
		|| $method == "count_modules_main_language_data_all_excluding" 
		|| $method == "get_modules_by_languages_id_data_all" 
		|| $method == "count_modules_by_languages_id_data_all" 
		|| $method == "get_modules_by_languages_short_name_data_all" 
		|| $method == "count_modules_by_languages_short_name_data_all" 
		|| $method == "get_modules_translation_data_all" 
		|| $method == "count_modules_translation_data_all" 
		|| $method == "get_modules_data_table_all" 
		|| $method == "count_modules_data_table_all" 
		|| $method == "get_search_modules_data_table_all" 
		|| $method == "count_search_modules_data_table_all" 
		|| $method == "create_modules_data" 
		|| $method == "update_modules_data" 
		|| $method == "delete_modules_data" 
		|| $method == "create_modules_table"
		|| $method == "create_modules_table_row" 
		|| $method == "update_modules_table_row" 
		|| $method == "delete_modules_table_row" 
		|| $method == "create_modules_table_row" 
		|| $method == "update_modules_table_row" 
		|| $method == "delete_modules_table_row" 
		|| $method == "inform_modules_table_row" 
		|| $method == "view_modules_table_row" 
		|| $method == "get_modules_edit_log_data" 
		|| $method == "count_modules_edit_log_data" 
		|| $method == "get_modules_edit_log_data_by_id" 
		|| $method == "count_modules_edit_log_data_by_id"
		|| $method == "create_sort_modules"
		|| $method == "sort_modules"
		|| $method == "get_modules_data_all_by_modules_pages_parent_link_field" 
		|| $method == "count_modules_data_all_by_modules_pages_parent_link_field" 
		|| $method == "get_modules_pages_parent_link_field_data_dynamic_list" 
		|| $method == "count_modules_pages_parent_link_field_data_dynamic_list" 
		|| $method == "get_modules_pages_parent_link_field_data_dynamic_list_by_id"
		|| $method == "get_modules_data_all_by_modules_datatable_field_name" 
		|| $method == "count_modules_data_all_by_modules_datatable_field_name" 
		|| $method == "get_modules_datatable_field_name_data_dynamic_list" 
		|| $method == "count_modules_datatable_field_name_data_dynamic_list" 
		|| $method == "get_modules_datatable_field_name_data_dynamic_list_by_id" 
		|| $method == "get_modules_data_table_field_data_dynamic_list"
		|| $method == "get_modules_content_data_all_by_modules_datatable_field_name")
   ) {
	
	/* include: configurations */
	require_once("../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("users.php");

	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}

	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "get_modules_data_by_id"){
		get_modules_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_modules_data_by_link"){
		get_modules_data_by_link($parameters, $_POST["activate"]);
	} else if ($method == "get_modules_data_all"){
		get_modules_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_data_all"){
		count_modules_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_data_all_excluding"){
		get_modules_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_data_all_excluding"){
		count_modules_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_data_table_all"){
		get_modules_data_table_all($table_module_field);
	} else if ($method == "count_modules_data_table_all"){
		count_modules_data_table_all($table_module_field);
	} else if ($method == "get_search_modules_data_table_all"){
		get_search_modules_data_table_all($parameters, $table_module_field);
	} else if ($method == "count_search_modules_data_table_all"){
		count_search_modules_data_table_all($parameters, $table_module_field);
	} else if ($method == "create_modules_data"){
		create_modules_data($parameters);
	} else if ($method == "update_modules_data"){
		update_modules_data($parameters);
	} else if ($method == "delete_modules_data"){
		delete_modules_data($parameters);
	} else if ($method == "create_modules_table"){
		create_modules_table($parameters, $table_module_field); 
	} else if ($method == "create_modules_table_row"){
		create_modules_table_row($parameters, $table_module_field);
	} else if ($method == "update_modules_table_row"){
		update_modules_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_modules_table_row"){
		inform_modules_table_row($parameters, $table_module_field);
	} else if ($method == "view_modules_table_row"){
		view_modules_table_row($parameters, $table_module_field);
	} else if ($method == "get_modules_edit_log_data"){
		get_modules_edit_log_data($parameters);
	} else if ($method == "count_modules_edit_log_data"){
		count_modules_edit_log_data($parameters);
	} else if ($method == "get_modules_edit_log_data_by_id"){
		get_modules_edit_log_data_by_id($parameters);
	} else if ($method == "count_modules_edit_log_data_by_id"){
		count_modules_edit_log_data_by_id($parameters);
	} else if ($method == "create_sort_modules"){
		create_sort_modules($table_module_field);
	} else if ($method == "sort_modules"){
		sort_modules($parameters);
	} else if ($method == "get_modules_data_all_by_modules_pages_parent_link_field"){
		get_modules_data_all_by_modules_pages_parent_link_field($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_data_all_by_modules_pages_parent_link_field"){
		count_modules_data_all_by_modules_pages_parent_link_field($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_pages_parent_link_field_data_dynamic_list"){
		get_modules_pages_parent_link_field_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_pages_parent_link_field_data_dynamic_list"){
		count_modules_pages_parent_link_field_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_pages_parent_link_field_data_dynamic_list_by_id"){
		get_modules_pages_parent_link_field_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_modules_data_all_by_modules_datatable_field_name"){
		get_modules_data_all_by_modules_datatable_field_name($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_data_all_by_modules_datatable_field_name"){
		count_modules_data_all_by_modules_datatable_field_name($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_datatable_field_name_data_dynamic_list"){
		get_modules_datatable_field_name_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_modules_datatable_field_name_data_dynamic_list"){
		count_modules_datatable_field_name_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_modules_datatable_field_name_data_dynamic_list_by_id"){
		get_modules_datatable_field_name_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_modules_data_table_field_data_dynamic_list"){
		get_modules_data_table_field_data_dynamic_list($parameters, $_POST["activate"]);
	} else if ($method == "get_modules_content_data_all_by_modules_datatable_field_name"){
		get_modules_content_data_all_by_modules_datatable_field_name($parameters, $_POST["activate"]);
	}
}

function get_modules_data_by_id($target, $activate = "", $internal_use = false){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	

	/* database: get data by ID from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules`.`modules_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
			$query["modules_date_created_formatted"] = datetime_reformat($query["modules_date_created"]);
			
			if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
				$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
			}
			if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
				$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
				for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
					if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
						foreach($query["modules_datatable_field"][$j] as $key => $value) {
							$query["modules_datatable_field"][$j][] = $value;
						}
					}
				}
				$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
			}
			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "modules_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__ && !$internal_use) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_by_link($target, $activate = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	

	/* database: get data by link from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	LEFT JOIN `pages` ON `modules`.`pages_id` = `pages`.`pages_id`
	WHERE `pages`.`pages_link` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
			$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
		}
		if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
			$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
			for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
				if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
					foreach($query["modules_datatable_field"][$j] as $key => $value) {
						$query["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
		}
			
			
			

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get data by link",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "pages_link",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_all($start = 0, $limit = "", $sort_by = "modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "modules" */
	$sql = "
	SELECT *
	FROM   `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
			$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
		}
		if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
			$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
			for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
				if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
					foreach($query["modules_datatable_field"][$j] as $key => $value) {
						$query["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}


		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
			$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
		}
		if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
			$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
			for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
				if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
					foreach($query["modules_datatable_field"][$j] as $key => $value) {
						$query["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "modules_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
			$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
		}
		if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
			$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
			for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
				if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
					foreach($query["modules_datatable_field"][$j] as $key => $value) {
						$query["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_modules_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "modules_order", $sort_direction = "asc"){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'modules'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "modules_actions" 
				&& $key != "modules_date_created" 
				&& $key != "modules_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
		if (isset($query["modules_pages_template_settings"]) && !empty($query["modules_pages_template_settings"])) {
			$query["modules_pages_template_settings"] = str_replace("<br />", "\n", $query["modules_pages_template_settings"]);
		}
		if (isset($query["modules_datatable_field"]) && !empty($query["modules_datatable_field"])) {
			$query["modules_datatable_field"] = unserialize($query["modules_datatable_field"]);
			for ($j = 0; $j < count($query["modules_datatable_field"]); $j++) {
				if (!empty($query["modules_datatable_field"][$j]) && is_array($query["modules_datatable_field"][$j])) {
					foreach($query["modules_datatable_field"][$j] as $key => $value) {
						$query["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query["modules_datatable_field_json"] = json_encode($query["modules_datatable_field"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_modules_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = ""){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'modules'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "modules_actions" 
				&& $key != "modules_date_created" 
				&& $key != "modules_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	$extra_sql_data_all = "";
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_modules_data($parameters){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	/* get global: image configurations */
	global $image_users_quality;

	/* get global: image thumbnail configurations */
	global $image_users_thumbnail;
	global $image_users_thumbnail_aspecratio;
	global $image_users_thumbnail_quality;
	global $image_users_thumbnail_width;
	global $image_users_thumbnail_height;

	/* get global: image large thumbnail configurations */
	global $image_users_large;
	global $image_users_large_aspecratio;
	global $image_users_large_quality;
	global $image_users_large_width;
	global $image_users_large_height;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["modules_individual_pages_parent_link"] == "1") {
		if(empty($parameters["pages_id"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Individual Page") . "</strong> " . translate("is required");
			$response["target"] = "#pages_id";
			$response["values"] = $parameters;
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if ($parameters["modules_activate"] == "1") {
		if(!isset($parameters["modules_name"]) || empty($parameters["modules_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is required");
			$response["target"] = "#modules_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_name"]) > 250 && $parameters["modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_name"]) > 250 && $parameters["modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_icon"]) > 100 && $parameters["modules_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_icon"]) > 100 && $parameters["modules_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_category"]) > 100 && $parameters["modules_category"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Category") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_category";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_category"]) > 100 && $parameters["modules_category"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Category") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_category";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_subject"]) > 100 && $parameters["modules_subject"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_subject";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_subject"]) > 100 && $parameters["modules_subject"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_subject";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_subject_icon"]) > 100 && $parameters["modules_subject_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_subject_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_subject_icon"]) > 100 && $parameters["modules_subject_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_subject_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["modules_link"]) || empty($parameters["modules_link"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is required");
			$response["target"] = "#modules_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_link"]) > 250 && $parameters["modules_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_link"]) > 250 && $parameters["modules_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_function_link"]) > 250 && $parameters["modules_function_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Function Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_function_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_function_link"]) > 250 && $parameters["modules_function_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Function Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_function_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["modules_key"]) || empty($parameters["modules_key"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is required");
			$response["target"] = "#modules_key";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_key"]) > 20 && $parameters["modules_key"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is longer than maximum length of strings at") . " 20 " . translate("character(s)");
				$response["target"] = "#modules_key";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_key"]) > 20 && $parameters["modules_key"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is longer than maximum length of strings at") . " 20 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_key";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["modules_key"]) && $parameters["modules_key"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#modules_key";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["modules_db_name"]) || empty($parameters["modules_db_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is required");
			$response["target"] = "#modules_db_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_db_name"]) > 100 && $parameters["modules_db_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_db_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_db_name"]) > 100 && $parameters["modules_db_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_db_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["modules_db_name"]) && $parameters["modules_db_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#modules_db_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_pages_template"]) > 250 && $parameters["modules_pages_template"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_pages_template";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_pages_template"]) > 250 && $parameters["modules_pages_template"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_pages_template";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_pages_template_settings"]) > 250 && $parameters["modules_pages_template_settings"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages Settings") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_pages_template_settings";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_pages_template_settings"]) > 250 && $parameters["modules_pages_template_settings"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages Settings") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_pages_template_settings";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_quality"] < 0 and $parameters["modules_image_crop_quality"] > 100 && $parameters["modules_image_crop_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";

			$response["target"] = "#modules_image_crop_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_quality"]) > 250 && $parameters["modules_image_crop_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_quality"]) > 250 && $parameters["modules_image_crop_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_thumbnail_quality"]) > 250 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_thumbnail_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_thumbnail_quality"]) > 250 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_thumbnail_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_thumbnail_quality"] < 0 and $parameters["modules_image_crop_thumbnail_quality"] > 100 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";
			$response["target"] = "#modules_image_crop_thumbnail_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_thumbnail_width"] < 0 && $parameters["modules_image_crop_thumbnail_width"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Width") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_thumbnail_width";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_thumbnail_height"] < 0 && $parameters["modules_image_crop_thumbnail_height"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Height") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_thumbnail_height";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_large_quality"]) > 250 && $parameters["modules_image_crop_large_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_large_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_large_quality"]) > 250 && $parameters["modules_image_crop_large_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_large_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_large_quality"] < 0 and $parameters["modules_image_crop_large_quality"] > 100 && $parameters["modules_image_crop_large_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";
			$response["target"] = "#modules_image_crop_large_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_large_width"] < 0 && $parameters["modules_image_crop_large_width"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Width") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_large_width";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_large_height"] < 0 && $parameters["modules_image_crop_large_height"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Height") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_large_height";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}

	/* initialize: default */
	$modules_datatable_field_name_array = array();
	$modules_datatable_field_display_array = array();
	for ($i = 0; $i < count($parameters["modules_datatable_field"]); $i++) {
	}
	$modules_datatable_field = serialize($parameters["modules_datatable_field"]);
	date_default_timezone_set($configs["timezone"]);
	$parameters["modules_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */

	/* database: get pages link from module "pages" to restore (begin) */
	$sql = "SELECT `pages_link` FROM `pages`
		WHERE `pages_id` = '" . $parameters["pages_id"] . "'
		AND `modules_record_target` = '" . $parameters["modules_old"]["modules_id"] . "'
		LIMIT 1";
	$result = mysqli_query($con, $sql);
	if ($result && mysqli_num_rows($result) > 0) {
	$parameters_pages = mysqli_fetch_assoc($result);
	$parameters["pages_link"] = $parameters_pages["pages_link"];
	mysqli_free_result($result);
	}
	/* database: get pages link from module "pages" to restore (end) */

	$sql_template = "
	SELECT `modules_pages_template`
	FROM `modules`
	WHERE `modules_id` = '0'";
	$result_template = mysqli_query($con, $sql_template);
	$num_template = mysqli_num_rows($result_template);
	if ($num_template > 0) {
		$query_template = mysqli_fetch_array($result_template);
		$pages_template = $query_template["modules_pages_template"];
	} else {
		$pages_template = "1";
	}

	/* database: insert to module "modules" (begin) */
	$sql = "INSERT INTO `modules` (
				`modules_id`,
				`modules_name`,
				`modules_description`,
				`modules_icon`,
				`modules_category`,
				`modules_subject`,
				`modules_subject_icon`,
				`modules_link`,
				`modules_function_link`,
				`modules_key`,
				`modules_db_name`,
				`modules_protected`,
				`modules_individual_pages_parent_link`,
				`modules_pages_parent_link_field`,
				`modules_pages_template`,
				`modules_pages_template_settings`,
				`modules_image_crop_quality`,
				`modules_image_crop_thumbnail`,
				`modules_image_crop_thumbnail_aspectratio`,
				`modules_image_crop_thumbnail_quality`,
				`modules_image_crop_thumbnail_width`,
				`modules_image_crop_thumbnail_height`,
				`modules_image_crop_large`,
				`modules_image_crop_large_aspectratio`,
				`modules_image_crop_large_quality`,
				`modules_image_crop_large_width`,
				`modules_image_crop_large_height`,
				`modules_datatable_field`,
				`modules_order`,
				`modules_date_created`,
				`modules_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`,
				`pages_id`,
				`pages_link`)
			VALUES (
				NULL,
				'" . $parameters["modules_name"] . "',
				'" . $parameters["modules_description"] . "',
				'" . $parameters["modules_icon"] . "',
				'" . $parameters["modules_category"] . "',
				'" . $parameters["modules_subject"] . "',
				'" . $parameters["modules_subject_icon"] . "',
				'" . $parameters["modules_link"] . "',
				'" . $parameters["modules_function_link"] . "',
				'" . $parameters["modules_key"] . "',
				'" . $parameters["modules_db_name"] . "',
				'" . $parameters["modules_protected"] . "',
				'" . $parameters["modules_individual_pages_parent_link"] . "',
				'" . $parameters["modules_pages_parent_link_field"] . "',
				'" . $parameters["modules_pages_template"] . "',
				'" . $parameters["modules_pages_template_settings"] . "',
				'" . $parameters["modules_image_crop_quality"] . "',
				'" . $parameters["modules_image_crop_thumbnail"] . "',
				'" . $parameters["modules_image_crop_thumbnail_aspectratio"] . "',
				'" . $parameters["modules_image_crop_thumbnail_quality"] . "',
				'" . $parameters["modules_image_crop_thumbnail_width"] . "',
				'" . $parameters["modules_image_crop_thumbnail_height"] . "',
				'" . $parameters["modules_image_crop_large"] . "',
				'" . $parameters["modules_image_crop_large_aspectratio"] . "',
				'" . $parameters["modules_image_crop_large_quality"] . "',
				'" . $parameters["modules_image_crop_large_width"] . "',
				'" . $parameters["modules_image_crop_large_height"] . "',
				'" . serialize($parameters["modules_datatable_field"]) . "',
				'" . $parameters["modules_order"] . "',
				'" . $parameters["modules_date_created"] . "',
				'" . $parameters["modules_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "',
				'" . $parameters["pages_id"] . "',
				'" . $parameters["pages_link"] . "')";
	$result = mysqli_query($con, $sql);
	$parameters["modules_id"] = mysqli_insert_id($con);
	/* database: insert to module "modules" (end) */

	if ($result) {

		/* database: update data to module "pages" (end) */
		for ($i = 0; $i < count($parameters["modules_datatable_field"]); $i++) {
		}
		
		/* response: additional data */
		

		/* log (begin) */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "modules_id",
				"modules_record_target" => $parameters["modules_id"],
				"modules_id" => "5",
				"modules_name" => "modules",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		
		$response["status"] = false;
		$response["message"] = translate("Database encountered error");
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}
	/* pages handler (end) */
	

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_modules_data($parameters){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	/* get global: image configurations */
	global $image_users_quality;

	/* get global: image thumbnail configurations */
	global $image_users_thumbnail;
	global $image_users_thumbnail_aspecratio;
	global $image_users_thumbnail_quality;
	global $image_users_thumbnail_width;
	global $image_users_thumbnail_height;

	/* get global: image large thumbnail configurations */
	global $image_users_large;
	global $image_users_large_aspecratio;
	global $image_users_large_quality;
	global $image_users_large_width;
	global $image_users_large_height;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["modules_individual_pages_parent_link"] == "1") {
		if(empty($parameters["pages_id"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Individual Page") . "</strong> " . translate("is required");
			$response["target"] = "#pages_id";
			$response["values"] = $parameters;
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if ($parameters["modules_activate"] == "1") {
		if(!isset($parameters["modules_name"]) || empty($parameters["modules_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is required");
			$response["target"] = "#modules_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_name"]) > 250 && $parameters["modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_name"]) > 250 && $parameters["modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_icon"]) > 100 && $parameters["modules_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_icon"]) > 100 && $parameters["modules_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_category"]) > 100 && $parameters["modules_category"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Category") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_category";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_category"]) > 100 && $parameters["modules_category"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Category") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_category";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_subject"]) > 100 && $parameters["modules_subject"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_subject";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_subject"]) > 100 && $parameters["modules_subject"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_subject";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_subject_icon"]) > 100 && $parameters["modules_subject_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_subject_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_subject_icon"]) > 100 && $parameters["modules_subject_icon"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Subject Icon") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_subject_icon";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["modules_link"]) || empty($parameters["modules_link"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is required");
			$response["target"] = "#modules_link";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_link"]) > 250 && $parameters["modules_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_link"]) > 250 && $parameters["modules_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Backend Page") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_function_link"]) > 250 && $parameters["modules_function_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Function Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_function_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_function_link"]) > 250 && $parameters["modules_function_link"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Function Link") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_function_link";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["modules_key"]) || empty($parameters["modules_key"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is required");
			$response["target"] = "#modules_key";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_key"]) > 20 && $parameters["modules_key"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is longer than maximum length of strings at") . " 20 " . translate("character(s)");
				$response["target"] = "#modules_key";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_key"]) > 20 && $parameters["modules_key"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("is longer than maximum length of strings at") . " 20 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_key";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["modules_key"]) && $parameters["modules_key"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Key") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#modules_key";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		
		if(!isset($parameters["modules_db_name"]) || empty($parameters["modules_db_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is required");
			$response["target"] = "#modules_db_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_db_name"]) > 100 && $parameters["modules_db_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#modules_db_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_db_name"]) > 100 && $parameters["modules_db_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_db_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["modules_db_name"]) && $parameters["modules_db_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Table Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#modules_db_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_pages_template"]) > 250 && $parameters["modules_pages_template"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_pages_template";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_pages_template"]) > 250 && $parameters["modules_pages_template"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_pages_template";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_pages_template_settings"]) > 250 && $parameters["modules_pages_template_settings"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages Settings") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_pages_template_settings";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_pages_template_settings"]) > 250 && $parameters["modules_pages_template_settings"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Front-end Template Inner Pages Settings") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_pages_template_settings";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_quality"]) > 250 && $parameters["modules_image_crop_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_quality"]) > 250 && $parameters["modules_image_crop_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_quality"] < 0 and $parameters["modules_image_crop_quality"] > 100 && $parameters["modules_image_crop_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";
			$response["target"] = "#modules_image_crop_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_thumbnail_quality"]) > 250 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_thumbnail_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_thumbnail_quality"]) > 250 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_thumbnail_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_thumbnail_quality"] < 0 and $parameters["modules_image_crop_thumbnail_quality"] > 100 && $parameters["modules_image_crop_thumbnail_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";
			$response["target"] = "#modules_image_crop_thumbnail_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_thumbnail_width"] < 0 && $parameters["modules_image_crop_thumbnail_width"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Width") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_thumbnail_width";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_thumbnail_height"] < 0 && $parameters["modules_image_crop_thumbnail_height"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Thumbnail Height") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_thumbnail_height";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["modules_image_crop_large_quality"]) > 250 && $parameters["modules_image_crop_large_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)");
				$response["target"] = "#modules_image_crop_large_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["modules_image_crop_large_quality"]) > 250 && $parameters["modules_image_crop_large_quality"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is longer than maximum length of strings at") . " 250 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#modules_image_crop_large_quality";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if($parameters["modules_image_crop_large_quality"] < 0 and $parameters["modules_image_crop_large_quality"] > 100 && $parameters["modules_image_crop_large_quality"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Quality") . "</strong> " . translate("is out of range for number between") . " 0 - 100";
			$response["target"] = "#modules_image_crop_large_quality";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_large_width"] < 0 && $parameters["modules_image_crop_large_width"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Width") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_large_width";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["modules_image_crop_large_height"] < 0 && $parameters["modules_image_crop_large_height"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Image Crop Large Thumbnail Height") . "</strong> " . translate("is less than minimum number at") . " 0";
			$response["target"] = "#modules_image_crop_large_height";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		
	}

	/* initialize: default */
	$modules_datatable_field_name_array = array();
	$modules_datatable_field_display_array = array();
	for ($i = 0; $i < count($parameters["modules_datatable_field"]); $i++) {
	}
	$modules_datatable_field = serialize($parameters["modules_datatable_field"]);
	/* database: get old data from module "modules" (begin) */
	$sql = "SELECT *
	        FROM   `modules`
			WHERE  `modules_id` = '" . $parameters["modules_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["modules_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "modules" (end) */

	$parameters["modules_date_created"] = $query["modules_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	$sql_template = "
	SELECT `modules_pages_template`
	FROM `modules`
	WHERE `modules_id` = '0'";
	$result_template = mysqli_query($con, $sql_template);
	$num_template = mysqli_num_rows($result_template);
	if ($num_template > 0) {
		$query_template = mysqli_fetch_array($result_template);
		$pages_template = $query_template["modules_pages_template"];
	} else {
		$pages_template = "1";
	}

	/* pages handler (begin) */

	/* database: get pages link from module "pages" to restore (begin) */
	$sql = "SELECT `pages_link` FROM `pages`
	        WHERE `pages_id` = '" . $parameters["pages_id"] . "'
			  AND `modules_record_target` = '" . $parameters["modules_old"]["modules_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	if ($result && mysqli_num_rows($result) > 0) {
		$parameters_pages = mysqli_fetch_assoc($result);
		$parameters["pages_link"] = $parameters_pages["pages_link"];
		mysqli_free_result($result);
	}
	/* database: get pages link from module "pages" to restore (end) */

	/* database: update to module "modules" (begin) */
	$sql = "UPDATE `modules`
			SET 
				   `modules_name` = '" . $parameters["modules_name"] . "',
				   `modules_description` = '" . $parameters["modules_description"] . "',
				   `modules_icon` = '" . $parameters["modules_icon"] . "',
				   `modules_category` = '" . $parameters["modules_category"] . "',
				   `modules_subject` = '" . $parameters["modules_subject"] . "',
				   `modules_subject_icon` = '" . $parameters["modules_subject_icon"] . "',
				   `modules_link` = '" . $parameters["modules_link"] . "',
				   `modules_function_link` = '" . $parameters["modules_function_link"] . "',
				   `modules_key` = '" . $parameters["modules_key"] . "',
				   `modules_db_name` = '" . $parameters["modules_db_name"] . "',
				   `modules_protected` = '" . $parameters["modules_protected"] . "',
				   `modules_individual_pages_parent_link` = '" . $parameters["modules_individual_pages_parent_link"] . "',
				   `modules_pages_parent_link_field` = '" . $parameters["modules_pages_parent_link_field"] . "',
				   `modules_pages_template` = '" . $parameters["modules_pages_template"] . "',
				   `modules_pages_template_settings` = '" . $parameters["modules_pages_template_settings"] . "',
				   `modules_image_crop_quality` = '" . $parameters["modules_image_crop_quality"] . "',
				   `modules_image_crop_thumbnail` = '" . $parameters["modules_image_crop_thumbnail"] . "',
				   `modules_image_crop_thumbnail_aspectratio` = '" . $parameters["modules_image_crop_thumbnail_aspectratio"] . "',
				   `modules_image_crop_thumbnail_quality` = '" . $parameters["modules_image_crop_thumbnail_quality"] . "',
				   `modules_image_crop_thumbnail_width` = '" . $parameters["modules_image_crop_thumbnail_width"] . "',
				   `modules_image_crop_thumbnail_height` = '" . $parameters["modules_image_crop_thumbnail_height"] . "',
				   `modules_image_crop_large` = '" . $parameters["modules_image_crop_large"] . "',
				   `modules_image_crop_large_aspectratio` = '" . $parameters["modules_image_crop_large_aspectratio"] . "',
				   `modules_image_crop_large_quality` = '" . $parameters["modules_image_crop_large_quality"] . "',
				   `modules_image_crop_large_width` = '" . $parameters["modules_image_crop_large_width"] . "',
				   `modules_image_crop_large_height` = '" . $parameters["modules_image_crop_large_height"] . "',
				   `modules_datatable_field` = '" . serialize($parameters["modules_datatable_field"]) . "',
				   `modules_order` = '" . $parameters["modules_order"] . "',
				   `modules_date_created` = '" . $parameters["modules_date_created"] . "',
				   `modules_activate` = '" . $parameters["modules_activate"] . "',
				   `pages_id` = '" . $parameters["pages_id"] . "',
				   `pages_link` = '" . $parameters["pages_link"] . "'
			WHERE  `modules_id` = '" . $parameters["modules_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "modules" (end) */

	if ($result) {
		
		for ($i = 0; $i < count($parameters["modules_datatable_field"]); $i++) {
			if (isset($parameters["modules_old"]["modules_datatable_field"]) && !empty($parameters["modules_old"]["modules_datatable_field"])) {
				$modules_datatable_field_old = unserialize($parameters["modules_old"]["modules_datatable_field"]);
			} else {
				$modules_datatable_field_old = array();
			}
		}
		/* response: additional data */
		

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "modules_id",
				"modules_record_target" => $parameters["modules_id"],
				"modules_id" => "5",
				"modules_name" => "modules",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
		
		if (isset($meta_parameters_old) && !empty($meta_parameters_old)) {
			if (count($meta_parameters_old) > 0) {
				foreach ($meta_parameters_old as $key => $value){
					$value = "'".$value."'";
					$updates_old[] = "`".$key."` = ".$value;
				}
			}
			$implode_meta_old = implode(", ", $updates_old);

			$sql = "UPDATE `pages`
					SET    " . $implode_meta_old . "
					WHERE  `pages_link` = '" . $pages_link . "'";
			$result = mysqli_query($con, $sql);
		}

	

		$response["status"] = false;
		$response["message"] = translate("Database encountered error");
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "modules";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_modules_data($target){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: image thumbnail configurations */
	global $image_users_thumbnail;

	/* get global: image large thumbnail configurations */
	global $image_users_large;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["modules_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "modules" (begin) */
		$sql = "
		DELETE FROM `modules`
		WHERE `modules_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "modules" (end) */
		
		if ($result) {
			for ($i = 0; $i < count($parameters["modules_datatable_field"]); $i++) {
				if (isset($parameters["modules_old"]["modules_datatable_field"]) && !empty($parameters["modules_old"]["modules_datatable_field"])) {
					$modules_datatable_field_old = unserialize($parameters["modules_old"]["modules_datatable_field"]);
				} else {
					$modules_datatable_field_old = array();
				}
			}

			/* log */
			if ($configs["log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "frontend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "modules_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {
			
			if (isset($meta_parameters_old) && !empty($meta_parameters_old)) {
				if (count($meta_parameters_old) > 0) {
					foreach ($meta_parameters_old as $key => $value){
						$value = "'".$value."'";
						$updates_old[] = "`".$key."` = ".$value;
					}
				}
				$implode_meta_old = implode(', ', $updates_old);

				$sql = "UPDATE `pages`
						SET    ".$implode_meta_old."
						WHERE  `pages_id` = '".$meta_parameters_old["pages_id"]."'";
				$result = mysqli_query($con, $sql);
			}

			$response["status"] = false;
			$response["message"] = translate("Database encountered error");
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		$response["message"] = translate("Database encountered error");
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_modules_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
		
		if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Name")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Description")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Icon")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Category")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Subject")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Subject Icon")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Backend Page")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Function Link")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Key")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Database Table Name")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Enable Module Individual Front-end Page Parent Link")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Front-end Parent Link Field")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Front-end Template Inner Pages")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Front-end Template Inner Pages Settings")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Quality")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Enable Image Crop Thumbnail")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Keep Aspect Ratio for Image Crop Thumbnail")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Quality")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Width")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Height")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Enable Image Crop Large Thumbnail")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Keep Aspect Ratio for Image Crop Large Thumbnail")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Quality")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Width")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Height")));
		} else if ($key == "") {
			$column_name = ucwords(trim(str_replace("_", " ", "Data Table Field")));
		} else {
			$column_name = str_replace("modules", "", $key);
			$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
		}
		if ($i >= 2) {
			$th_hidden_xs_class = "hidden-xs";
		} else {
			$th_hidden_xs_class = "";
		}

		if ($i >= 3 && $key != "modules_activate") {
			$th_hidden_sm_class = "hidden-sm";
		} else {
			$th_hidden_sm_class = "";

		}

		if ($i >= 4 && $key != "modules_activate") {
			$th_hidden_md_class = "hidden-md";
		} else {
			$th_hidden_md_class = "";
		}

		if ($i >= 5 && $key != "modules_activate") {
			$th_hidden_lg_class = "hidden-lg";
		} else {
			$th_hidden_lg_class = "";
		}

		if ($value == "true" || $value === true) {
			if ($key == "modules_id") {
                $html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "modules_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_name . '" data-toggle="tooltip" title="Name" data-original-title="Name">' . htmlspecialchars(strip_tags(translate("Name"))) . '</th>';
			} else if ($key == "modules_description") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_description . '" data-toggle="tooltip" title="Description" data-original-title="Description">' . htmlspecialchars(strip_tags(translate("Description"))) . '</th>';
			} else if ($key == "modules_icon") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_icon . '" data-toggle="tooltip" title="Icon" data-original-title="Icon">' . htmlspecialchars(strip_tags(translate("Icon"))) . '</th>';
			} else if ($key == "modules_category") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_category . '" data-toggle="tooltip" title="Category" data-original-title="Category">' . htmlspecialchars(strip_tags(translate("Category"))) . '</th>';
			} else if ($key == "modules_subject") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_subject . '" data-toggle="tooltip" title="Subject" data-original-title="Subject">' . htmlspecialchars(strip_tags(translate("Subject"))) . '</th>';
			} else if ($key == "modules_subject_icon") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_subject_icon . '" data-toggle="tooltip" title="Subject Icon" data-original-title="Subject Icon">' . htmlspecialchars(strip_tags(translate("Subject Icon"))) . '</th>';
			} else if ($key == "modules_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_link . '" data-toggle="tooltip" title="Backend Page" data-original-title="Backend Page">' . htmlspecialchars(strip_tags(translate("Backend Page"))) . '</th>';
			} else if ($key == "modules_function_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_function_link . '" data-toggle="tooltip" title="Function Link" data-original-title="Function Link">' . htmlspecialchars(strip_tags(translate("Function Link"))) . '</th>';
			} else if ($key == "modules_key") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_key . '" data-toggle="tooltip" title="Key" data-original-title="Key">' . htmlspecialchars(strip_tags(translate("Key"))) . '</th>';
			} else if ($key == "modules_db_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_db_name . '" data-toggle="tooltip" title="Database Table Name" data-original-title="Database Table Name">' . htmlspecialchars(strip_tags(translate("Database Table Name"))) . '</th>';
			} else if ($key == "modules_protected") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_protected . '" data-toggle="tooltip" title="Protected" data-original-title="Protected">' . htmlspecialchars(strip_tags(translate("Protected"))) . '</th>';
			} else if ($key == "modules_individual_pages_parent_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_individual_pages_parent_link . '" data-toggle="tooltip" title="Enable Module Individual Front-end Page Parent Link" data-original-title="Enable Module Individual Front-end Page Parent Link">' . htmlspecialchars(strip_tags(translate("Enable Module Individual Front-end Page Parent Link"))) . '</th>';
			} else if ($key == "modules_pages_parent_link_field") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_pages_parent_link_field . '" data-toggle="tooltip" title="Front-end Parent Link Field" data-original-title="Front-end Parent Link Field">' . htmlspecialchars(strip_tags(translate("Front-end Parent Link Field"))) . '</th>';
			} else if ($key == "modules_pages_template") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_pages_template . '" data-toggle="tooltip" title="Front-end Template Inner Pages" data-original-title="Front-end Template Inner Pages">' . htmlspecialchars(strip_tags(translate("Front-end Template Inner Pages"))) . '</th>';
			} else if ($key == "modules_pages_template_settings") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_pages_template_settings . '" data-toggle="tooltip" title="Front-end Template Inner Pages Settings" data-original-title="Front-end Template Inner Pages Settings">' . htmlspecialchars(strip_tags(translate("Front-end Template Inner Pages Settings"))) . '</th>';
			} else if ($key == "modules_image_crop_quality") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_quality . '" data-toggle="tooltip" title="Image Crop Quality" data-original-title="Image Crop Quality">' . htmlspecialchars(strip_tags(translate("Image Crop Quality"))) . '</th>';
			} else if ($key == "modules_image_crop_thumbnail") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_thumbnail . '" data-toggle="tooltip" title="Enable Image Crop Thumbnail" data-original-title="Enable Image Crop Thumbnail">' . htmlspecialchars(strip_tags(translate("Enable Image Crop Thumbnail"))) . '</th>';
			} else if ($key == "modules_image_crop_thumbnail_aspectratio") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_thumbnail_aspectratio . '" data-toggle="tooltip" title="Keep Aspect Ratio for Image Crop Thumbnail" data-original-title="Keep Aspect Ratio for Image Crop Thumbnail">' . htmlspecialchars(strip_tags(translate("Keep Aspect Ratio for Image Crop Thumbnail"))) . '</th>';
			} else if ($key == "modules_image_crop_thumbnail_quality") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_thumbnail_quality . '" data-toggle="tooltip" title="Image Crop Thumbnail Quality" data-original-title="Image Crop Thumbnail Quality">' . htmlspecialchars(strip_tags(translate("Image Crop Thumbnail Quality"))) . '</th>';
			} else if ($key == "modules_image_crop_thumbnail_width") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_thumbnail_width . '" data-toggle="tooltip" title="Image Crop Thumbnail Width" data-original-title="Image Crop Thumbnail Width">' . htmlspecialchars(strip_tags(translate("Image Crop Thumbnail Width"))) . '</th>';
			} else if ($key == "modules_image_crop_thumbnail_height") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_thumbnail_height . '" data-toggle="tooltip" title="Image Crop Thumbnail Height" data-original-title="Image Crop Thumbnail Height">' . htmlspecialchars(strip_tags(translate("Image Crop Thumbnail Height"))) . '</th>';
			} else if ($key == "modules_image_crop_large") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_large . '" data-toggle="tooltip" title="Enable Image Crop Large Thumbnail" data-original-title="Enable Image Crop Large Thumbnail">' . htmlspecialchars(strip_tags(translate("Enable Image Crop Large Thumbnail"))) . '</th>';
			} else if ($key == "modules_image_crop_large_aspectratio") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_large_aspectratio . '" data-toggle="tooltip" title="Keep Aspect Ratio for Image Crop Large Thumbnail" data-original-title="Keep Aspect Ratio for Image Crop Large Thumbnail">' . htmlspecialchars(strip_tags(translate("Keep Aspect Ratio for Image Crop Large Thumbnail"))) . '</th>';
			} else if ($key == "modules_image_crop_large_quality") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_large_quality . '" data-toggle="tooltip" title="Image Crop Large Thumbnail Quality" data-original-title="Image Crop Large Thumbnail Quality">' . htmlspecialchars(strip_tags(translate("Image Crop Large Thumbnail Quality"))) . '</th>';
			} else if ($key == "modules_image_crop_large_width") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_large_width . '" data-toggle="tooltip" title="Image Crop Large Thumbnail Width" data-original-title="Image Crop Large Thumbnail Width">' . htmlspecialchars(strip_tags(translate("Image Crop Large Thumbnail Width"))) . '</th>';
			} else if ($key == "modules_image_crop_large_height") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_image_crop_large_height . '" data-toggle="tooltip" title="Image Crop Large Thumbnail Height" data-original-title="Image Crop Large Thumbnail Height">' . htmlspecialchars(strip_tags(translate("Image Crop Large Thumbnail Height"))) . '</th>';
			} else if ($key == "modules_datatable_field") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . modules_datatable_field . '" data-toggle="tooltip" title="Data Table Field" data-original-title="Data Table Field">' . htmlspecialchars(strip_tags(translate("Data Table Field"))) . '</th>';
			} else if ($key == "modules_activate") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	if ($table_module_field["modules_actions"] == true || $table_module_field["modules_actions"] === true) {

		$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th></tr></thead>';

	}

	$html_open .= '<tbody id="datatable-list">';



	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["modules_id"].'" data-target="'.$data_table_row["modules_id"].'">';

		$html .= inform_modules_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_modules_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "modules_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "modules_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	$search = escape_string($con, trim($_GET["search"]));

	$count_all_true = count_modules_data_table_all($table_module_field);
	if ($search != "") {
		$count_all = count_search_modules_data_table_all($search, $table_module_field);
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;

			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			$data_table = get_search_modules_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction);
		} else {
			$data_table = get_modules_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction);
		}
	} else {
		$data_table = get_modules_data_table_all($table_module_field);
	}

	if (isset($con)) {
		stop($con);
	}

	if ($page_limit == "") {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_modules_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["modules_id"].'" data-target="'.$data_table["modules_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_modules_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "modules_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "modules_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "modules_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "modules_actions") {
				if ($key == "modules_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "modules_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_name"]));
				} else if ($key == "modules_description") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_description"]));
				} else if ($key == "modules_icon") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_icon"]));
				} else if ($key == "modules_category") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_category"]));
				} else if ($key == "modules_subject") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_subject"]));
				} else if ($key == "modules_subject_icon") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_subject_icon"]));
				} else if ($key == "modules_link") {
					$td_align_class = "text-center";
					$table_row_value = render_modules_table_backend_file($data_table["modules_link"]);
				} else if ($key == "modules_function_link") {
					$td_align_class = "text-center";
					$table_row_value = render_modules_table_system_file($data_table["modules_function_link"]);
				} else if ($key == "modules_key") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_key"]));
				} else if ($key == "modules_db_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_db_name"]));
				} else if ($key == "modules_protected") {
					$td_align_class = ".text-center";
					$modules_protected_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($modules_protected_default_value[0] != "" && isset($modules_protected_default_value[0])) {
						if ($data_table["modules_protected"] == $modules_protected_default_value[0]) {
							if ($modules_protected_default_value[0] == "1") {
								$modules_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_protected"])) . '</span>';
							}
						} else {
							if ($modules_protected_default_value[0] == "1") {
								$modules_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["modules_protected"] == "1") {
							$modules_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $modules_protected_data_table_value;
				} else if ($key == "modules_individual_pages_parent_link") {
					$td_align_class = ".text-center";
					$modules_individual_pages_parent_link_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($modules_individual_pages_parent_link_default_value[0] != "" && isset($modules_individual_pages_parent_link_default_value[0])) {
						if ($data_table["modules_individual_pages_parent_link"] == $modules_individual_pages_parent_link_default_value[0]) {
							if ($modules_individual_pages_parent_link_default_value[0] == "1") {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_individual_pages_parent_link"])) . '</span>';
							}
						} else {
							if ($modules_individual_pages_parent_link_default_value[0] == "1") {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["modules_individual_pages_parent_link"] == "1") {
							$modules_individual_pages_parent_link_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_individual_pages_parent_link_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $modules_individual_pages_parent_link_data_table_value;
				} else if ($key == "modules_pages_parent_link_field") {
					$td_align_class = "";
					$con = start();
					$sql_modules_pages_parent_link_field  = "
					SELECT `pages_link`, `pages_title`
					FROM   `pages`
					WHERE  `pages_link` = '".$data_table["modules_pages_parent_link_field"]."'
					LIMIT  1";
					$result_modules_pages_parent_link_field = mysqli_query($con, $sql_modules_pages_parent_link_field);
					$num_modules_pages_parent_link_field = mysqli_num_rows($result_modules_pages_parent_link_field);
					if ($num_modules_pages_parent_link_field > 0) {
						$query_modules_pages_parent_link_field = mysqli_fetch_array($result_modules_pages_parent_link_field);
						$module_link = false;
						foreach (array_keys($query_modules_pages_parent_link_field) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_modules_pages_parent_link_field['pages_link']).'" target="_blank">'.strip_tags($query_modules_pages_parent_link_field['pages_title']).'</a>';
						} else {
							$table_row_value = strip_tags($query_modules_pages_parent_link_field['pages_title']);
						}
					}
					mysqli_free_result($result_modules_pages_parent_link_field);
					stop($con);
				} else if ($key == "modules_pages_template") {
					$td_align_class = "text-center";
					$table_row_value = render_modules_table_template_file($data_table["modules_pages_template"]);
				} else if ($key == "modules_pages_template_settings") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_pages_template_settings"]));
				} else if ($key == "modules_image_crop_quality") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_quality"]));
				} else if ($key == "modules_image_crop_thumbnail") {
					$td_align_class = ".text-center";
					$modules_image_crop_thumbnail_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($data_table["modules_image_crop_thumbnail"] == $modules_image_crop_thumbnail_default_value[0]) {
						if ($modules_image_crop_thumbnail_default_value[0] == "1") {
							$modules_image_crop_thumbnail_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_image_crop_thumbnail_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail"])) . '</span>';
						}
					} else {
						if ($modules_image_crop_thumbnail_default_value[0] == "1") {
							$modules_image_crop_thumbnail_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						} else {
							$modules_image_crop_thumbnail_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
						}
					}
					$table_row_value = $modules_image_crop_thumbnail_data_table_value;
				} else if ($key == "modules_image_crop_thumbnail_aspectratio") {
					$td_align_class = ".text-center";
					$modules_image_crop_thumbnail_aspectratio_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($data_table["modules_image_crop_thumbnail_aspectratio"] == $modules_image_crop_thumbnail_aspectratio_default_value[0]) {
						if ($modules_image_crop_thumbnail_aspectratio_default_value[0] == "1") {
							$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail_aspectratio"])) . '</span>';
						}
					} else {
						if ($modules_image_crop_thumbnail_aspectratio_default_value[0] == "1") {
							$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						} else {
							$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
						}
					}
					$table_row_value = $modules_image_crop_thumbnail_aspectratio_data_table_value;
				} else if ($key == "modules_image_crop_thumbnail_quality") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail_quality"]));
				} else if ($key == "modules_image_crop_thumbnail_width") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail_width"]));
				} else if ($key == "modules_image_crop_thumbnail_height") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail_height"]));
				} else if ($key == "modules_image_crop_large") {
					$td_align_class = ".text-center";
					$modules_image_crop_large_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($data_table["modules_image_crop_large"] == $modules_image_crop_large_default_value[0]) {
						if ($modules_image_crop_large_default_value[0] == "1") {
							$modules_image_crop_large_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_image_crop_large_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_large"])) . '</span>';
						}
					} else {
						if ($modules_image_crop_large_default_value[0] == "1") {
							$modules_image_crop_large_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						} else {
							$modules_image_crop_large_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
						}
					}
					$table_row_value = $modules_image_crop_large_data_table_value;
				} else if ($key == "modules_image_crop_large_aspectratio") {
					$td_align_class = ".text-center";
					$modules_image_crop_large_aspectratio_default_value = unserialize("a:1:{i:0;i:1;}");
					if ($data_table["modules_image_crop_large_aspectratio"] == $modules_image_crop_large_aspectratio_default_value[0]) {
						if ($modules_image_crop_large_aspectratio_default_value[0] == "1") {
							$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_large_aspectratio"])) . '</span>';
						}
					} else {
						if ($modules_image_crop_large_aspectratio_default_value[0] == "1") {
							$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						} else {
							$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
						}
					}
					$table_row_value = $modules_image_crop_large_aspectratio_data_table_value;
				} else if ($key == "modules_image_crop_large_quality") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_large_quality"]));
				} else if ($key == "modules_image_crop_large_width") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_large_width"]));
				} else if ($key == "modules_image_crop_large_height") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["modules_image_crop_large_height"]));
				} else if ($key == "modules_datatable_field") {
					$td_align_class = "";
					for($l = 0; $l < count($data_table["modules_datatable_field"]); $l++) {
						$table_row_value .= '<div style="border-bottom: 1px dashed #ddd; padding-bottom: 5px; padding-top: 5px;">';
						$table_row_value .= '<strong>' . translate("Field Name") . ':</strong><br />';
						$table_row_value .= htmlspecialchars(strip_tags($data_table["modules_datatable_field"][$l][0])) . '<br />';
						$table_row_value .= '<strong>' . translate("Display") . ':</strong><br />';
						$table_row_value .= htmlspecialchars(strip_tags($data_table["modules_datatable_field"][$l][1])) . '<br />';
						$table_row_value .= '</div>';
					}
				} else if ($key == "modules_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="users.php?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_modules_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
			}
			$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["modules_id"].'">'.$table_row_value.'</td>';

			$i++;
		}
	}
	
	$table_module_field_unassoc = array_keys($table_module_field);
	$table_module_field_key = 1;
	for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
		if ($table_module_field[$table_module_field_unassoc[$j]] == true) {
			$table_module_field_key = $j;
			break;
		}
	}

	if ($table_module_field["modules_actions"] == true || $table_module_field["modules_actions"] === true) {
		$html .= '
		<td id="datatable-actions-'.$data_table["modules_id"].'" class="text-center">
			<div class="btn-group">';

			$html .= '
				<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["modules_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
				<i class="fa fa-eye"></i>
				</button>';

			if ($_SESSION["users_level"] != "5") {
			
				if (!isset($data_table["modules_protected"]) || (isset($data_table["modules_protected"]) && $data_table["modules_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

			$html .= '
				<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["modules_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
				<i class="fa fa-pencil"></i>
				</button>';
					
				}

			$html .= '
				<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["modules_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
				<i class="fa fa-copy"></i>
				</button>';

			$html .= '
				<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["modules_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_unassoc[$table_module_field_key]])).'</strong>?">
				<i class="fa fa-times"></i>
				</button>';
				
			}

		$html .= '
			</div>
		</td>';
	}

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_modules_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_modules_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Description")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Icon")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Category")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Subject")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Subject Icon")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Backend Page")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Function Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Key")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Database Table Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Enable Module Individual Front-end Page Parent Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Front-end Parent Link Field")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Front-end Template Inner Pages")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Front-end Template Inner Pages Settings")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Quality")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Enable Image Crop Thumbnail")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Keep Aspect Ratio for Image Crop Thumbnail")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Quality")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Width")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Thumbnail Height")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Enable Image Crop Large Thumbnail")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Keep Aspect Ratio for Image Crop Large Thumbnail")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Quality")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Width")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Crop Large Thumbnail Height")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Data Table Field")));
			} else {
				$column_name = str_replace("modules", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "modules_actions") {
				if ($key == "modules_id") {
					if (!empty($data_table["modules_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_name") {
					if ($data_table["modules_name"] != "") {
						$table_row_value = '<strong>' . translate("Name") . '</strong><br />' . $data_table["modules_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_description") {
					if ($data_table["modules_description"] != "") {
						$table_row_value = '<strong>' . translate("Description") . '</strong><br />' . $data_table["modules_description"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_icon") {
					if ($data_table["modules_icon"] != "") {
						$table_row_value = '<strong>' . translate("Icon") . '</strong><br />' . $data_table["modules_icon"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_category") {
					if ($data_table["modules_category"] != "") {
						$table_row_value = '<strong>' . translate("Category") . '</strong><br />' . $data_table["modules_category"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_subject") {
					if ($data_table["modules_subject"] != "") {
						$table_row_value = '<strong>' . translate("Subject") . '</strong><br />' . $data_table["modules_subject"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_subject_icon") {
					if ($data_table["modules_subject_icon"] != "") {
						$table_row_value = '<strong>' . translate("Subject Icon") . '</strong><br />' . $data_table["modules_subject_icon"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_link") {
					if ($data_table["modules_link"] != "") {
						$table_row_value = '<strong>' . translate("Backend Page") . '</strong><br />'.render_modules_view_backend_file($data_table["modules_link"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_function_link") {
					if ($data_table["modules_function_link"] != "") {
						$table_row_value = '<strong>' . translate("Function Link") . '</strong><br />'.render_modules_view_system_file($data_table["modules_function_link"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_key") {
					if ($data_table["modules_key"] != "") {
						$table_row_value = '<strong>' . translate("Key") . '</strong><br />' . $data_table["modules_key"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_db_name") {
					if ($data_table["modules_db_name"] != "") {
						$table_row_value = '<strong>' . translate("Database Table Name") . '</strong><br />' . $data_table["modules_db_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_protected") {
					if ($data_table["modules_protected"] != "") {
						$modules_protected_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_protected"] == $modules_protected_default_value[0]) {
							if ($modules_protected_default_value[0] == "1") {
								$modules_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_protected"])) . '</span>';
							}
						} else {
							if ($modules_protected_default_value[0] == "1") {
								$modules_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Protected") . '</strong><br />' . $modules_protected_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_individual_pages_parent_link") {
					if ($data_table["modules_individual_pages_parent_link"] != "") {
						$modules_individual_pages_parent_link_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_individual_pages_parent_link"] == $modules_individual_pages_parent_link_default_value[0]) {
							if ($modules_individual_pages_parent_link_default_value[0] == "1") {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_individual_pages_parent_link"])) . '</span>';
							}
						} else {
							if ($modules_individual_pages_parent_link_default_value[0] == "1") {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_individual_pages_parent_link_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Enable Module Individual Front-end Page Parent Link") . '</strong><br />' . $modules_individual_pages_parent_link_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_pages_parent_link_field") {
					if ($data_table["modules_pages_parent_link_field"] != "") {
						$con = start();
						$sql_modules_pages_parent_link_field  = "
						SELECT `pages_link`, `pages_title`
						FROM   `pages`
						WHERE  `pages_link` = '".$data_table["modules_pages_parent_link_field"]."'
						LIMIT  1";
						$result_modules_pages_parent_link_field = mysqli_query($con, $sql_modules_pages_parent_link_field);
						$num_modules_pages_parent_link_field = mysqli_num_rows($result_modules_pages_parent_link_field);
						if ($num_modules_pages_parent_link_field > 0) {
							$query_modules_pages_parent_link_field = mysqli_fetch_array($result_modules_pages_parent_link_field);
							$module_link = false;
							foreach (array_keys($query_modules_pages_parent_link_field) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Front-end Parent Link Field") . '</strong><br /><a href="'.rewrite_url($query_modules_pages_parent_link_field['pages_link']).'" target="_blank">'.strip_tags($query_modules_pages_parent_link_field['pages_title']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Front-end Parent Link Field") . '</strong><br />'.strip_tags($query_modules_pages_parent_link_field['pages_title']).'<br /><br />';
							}
						}
						mysqli_free_result($result_modules_pages_parent_link_field);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_pages_template") {
					if ($data_table["modules_pages_template"] != "") {
						$table_row_value = '<strong>' . translate("Front-end Template Inner Pages") . '</strong><br />'.render_modules_view_template_file($data_table["modules_pages_template"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_pages_template_settings") {
					if ($data_table["modules_pages_template_settings"] != "") {
						$table_row_value = '<strong>' . translate("Front-end Template Inner Pages Settings") . '</strong><br />' . $data_table["modules_pages_template_settings"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_quality") {
					if ($data_table["modules_image_crop_quality"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Quality") . '</strong><br />' . $data_table["modules_image_crop_quality"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_thumbnail") {
					if ($data_table["modules_image_crop_thumbnail"] != "") {
						$modules_image_crop_thumbnail_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_image_crop_thumbnail"] == $modules_image_crop_thumbnail_default_value[0]) {
							if ($modules_image_crop_thumbnail_default_value[0] == "1") {
								$modules_image_crop_thumbnail_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_image_crop_thumbnail_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail"])) . '</span>';
							}
						} else {
							if ($modules_image_crop_thumbnail_default_value[0] == "1") {
								$modules_image_crop_thumbnail_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_image_crop_thumbnail_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Enable Image Crop Thumbnail") . '</strong><br />' . $modules_image_crop_thumbnail_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_thumbnail_aspectratio") {
					if ($data_table["modules_image_crop_thumbnail_aspectratio"] != "") {
						$modules_image_crop_thumbnail_aspectratio_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_image_crop_thumbnail_aspectratio"] == $modules_image_crop_thumbnail_aspectratio_default_value[0]) {
							if ($modules_image_crop_thumbnail_aspectratio_default_value[0] == "1") {
								$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_thumbnail_aspectratio"])) . '</span>';
							}
						} else {
							if ($modules_image_crop_thumbnail_aspectratio_default_value[0] == "1") {
								$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_image_crop_thumbnail_aspectratio_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Keep Aspect Ratio for Image Crop Thumbnail") . '</strong><br />' . $modules_image_crop_thumbnail_aspectratio_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_thumbnail_quality") {
					if ($data_table["modules_image_crop_thumbnail_quality"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Thumbnail Quality") . '</strong><br />' . $data_table["modules_image_crop_thumbnail_quality"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_thumbnail_width") {
					if ($data_table["modules_image_crop_thumbnail_width"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Thumbnail Width") . '</strong><br />' . $data_table["modules_image_crop_thumbnail_width"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_thumbnail_height") {
					if ($data_table["modules_image_crop_thumbnail_height"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Thumbnail Height") . '</strong><br />' . $data_table["modules_image_crop_thumbnail_height"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_large") {
					if ($data_table["modules_image_crop_large"] != "") {
						$modules_image_crop_large_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_image_crop_large"] == $modules_image_crop_large_default_value[0]) {
							if ($modules_image_crop_large_default_value[0] == "1") {
								$modules_image_crop_large_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_image_crop_large_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_large"])) . '</span>';
							}
						} else {
							if ($modules_image_crop_large_default_value[0] == "1") {
								$modules_image_crop_large_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_image_crop_large_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Enable Image Crop Large Thumbnail") . '</strong><br />' . $modules_image_crop_large_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_large_aspectratio") {
					if ($data_table["modules_image_crop_large_aspectratio"] != "") {
						$modules_image_crop_large_aspectratio_default_value = unserialize("a:1:{i:0;i:1;}");
						if ($data_table["modules_image_crop_large_aspectratio"] == $modules_image_crop_large_aspectratio_default_value[0]) {
							if ($modules_image_crop_large_aspectratio_default_value[0] == "1") {
								$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["modules_image_crop_large_aspectratio"])) . '</span>';
							}
						} else {
							if ($modules_image_crop_large_aspectratio_default_value[0] == "1") {
								$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$modules_image_crop_large_aspectratio_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Keep Aspect Ratio for Image Crop Large Thumbnail") . '</strong><br />' . $modules_image_crop_large_aspectratio_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_large_quality") {
					if ($data_table["modules_image_crop_large_quality"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Large Thumbnail Quality") . '</strong><br />' . $data_table["modules_image_crop_large_quality"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_large_width") {
					if ($data_table["modules_image_crop_large_width"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Large Thumbnail Width") . '</strong><br />' . $data_table["modules_image_crop_large_width"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_image_crop_large_height") {
					if ($data_table["modules_image_crop_large_height"] != "") {
						$table_row_value = '<strong>' . translate("Image Crop Large Thumbnail Height") . '</strong><br />' . $data_table["modules_image_crop_large_height"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_datatable_field") {
					if ($data_table["modules_datatable_field"] != "") {
						$table_row_value = '<strong>' . translate("Data Table Field") . '</strong><br />';
						for($l = 0; $l < count($data_table["modules_datatable_field"]); $l++) {
							$table_row_value .= '<div style="border-bottom: 1px dashed #ddd; padding-bottom: 5px; padding-top: 5px;">';
							$table_row_value .= '<strong>' . translate("Field Name") . ':</strong><br />';
							$table_row_value .= $data_table["modules_datatable_field"][$l][0] . '<br />';
							$table_row_value .= '<strong>' . translate("Display") . ':</strong><br />';
							$table_row_value .= $data_table["modules_datatable_field"][$l][1] . '<br />';
							$table_row_value .= '</div>';
						}
						$table_row_value .= '<br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "modules_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "modules_date_created") {
					if (!empty($data_table["modules_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["modules_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_modules_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_modules_table_backend_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/console") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/console") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_table_system_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/".$configs["backend_url"]."/system") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/system/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/".$configs["backend_url"]."/system") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/system/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_table_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_view_backend_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/console") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/console") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_view_system_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/".$configs["backend_url"]."/system") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/system/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/".$configs["backend_url"]."/system") === false) {
				$file_path = $configs["base_url"]."/".$configs["backend_url"]."/system/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_view_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check, CURLOPT_RETURNTRANSFER, true);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_modules_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_modules_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	global $modules_configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "modules" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '0'
	AND (`log_function` = 'create_modules_data' OR `log_function` = 'update_modules_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	global $modules_configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '0'
	AND (`log_function` = 'create_modules_data' OR `log_function` = 'update_modules_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "modules" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["modules_pages_template_settings"]) && !empty($query_log_content_hash["modules_pages_template_settings"])) {
			$query_log_content_hash["modules_pages_template_settings"] = str_replace("<br />", "\n", $query_log_content_hash["modules_pages_template_settings"]);
		}
		if (isset($query_log_content_hash["modules_datatable_field"]) && !empty($query_log_content_hash["modules_datatable_field"])) {
			
			for ($j = 0; $j < count($query_log_content_hash["modules_datatable_field"]); $j++) {
				if (!empty($query_log_content_hash["modules_datatable_field"][$j]) && is_array($query_log_content_hash["modules_datatable_field"][$j])) {
					foreach($query_log_content_hash["modules_datatable_field"][$j] as $key => $value) {
						$query_log_content_hash["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query_log_content_hash["modules_datatable_field_json"] = json_encode($query_log_content_hash["modules_datatable_field"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "modules" */
    $sql = "
	EXPLAIN SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["modules_pages_template_settings"]) && !empty($query_log_content_hash["modules_pages_template_settings"])) {
			$query_log_content_hash["modules_pages_template_settings"] = str_replace("<br />", "\n", $query_log_content_hash["modules_pages_template_settings"]);
		}
		if (isset($query_log_content_hash["modules_datatable_field"]) && !empty($query_log_content_hash["modules_datatable_field"])) {
			
			for ($j = 0; $j < count($query_log_content_hash["modules_datatable_field"]); $j++) {
				if (!empty($query_log_content_hash["modules_datatable_field"][$j]) && is_array($query_log_content_hash["modules_datatable_field"][$j])) {
					foreach($query_log_content_hash["modules_datatable_field"][$j] as $key => $value) {
						$query_log_content_hash["modules_datatable_field"][$j][] = $value;
					}
				}
			}
			$query_log_content_hash["modules_datatable_field_json"] = json_encode($query_log_content_hash["modules_datatable_field"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_sort_modules($table_module_field) {
	
	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$data_table = escape_string($con, $data_table);

	
	$table_module_field_unassoc = array_keys($table_module_field);
	$table_module_field_key = "modules_name";
	for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
		if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
			$table_module_field_key = $table_module_field_unassoc[$j];
			break;
		}
	}

	$data_table = get_modules_data_all("", "", "modules_order", "asc");
	
	
	$html = '';

	for($i = 0; $i < count($data_table); $i++){
		if ($data_table[$i]["modules_id"] > 0) {

			$table_row_value = render_modules_table_data($data_table[$i][$table_module_field_key]).'<br />('.$data_table[$i][$table_module_field_key].')';

			$html .= '<li id="list_'.$data_table[$i]['modules_id'].'" class="mjs-nestedSortable" style="display: list-item;">
						<div class="sort-item">
							<span>
								<div style="display: inline-table">
									<i class="si si-cursor-move"></i>
								</div>
								<div style="display: inline-table">
									'.$table_row_value.' ('.translate("ID").': '.$data_table[$i]["modules_id"].')
								</div>
							</span>
						</div>
					  </li>';

		}

	}
	
	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function sort_modules($parameters) {
	
	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;

	
    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	
	$target_next = 1;
	
	for ($i = 0; $i <= count($parameters); $i++) {
		$target_item_id = "";
		for($s = 0; $s <= count($parameters); $s++) {
			if ($parameters[$s]["left"] == $target_next) {
				$target_item_id = $parameters[$s]["item_id"];
				$target_depth = $parameters[$s]["depth"];
				if ($target_depth == 1) {
					$target_next = $parameters[$s]["right"] + 1;
				} else {
					$target_next = $target_next + 1;
				}
				break;
			}
		}
		
		if ($target_depth != "0" && $target_depth == 1 && !empty($target_item_id)) {
			
			/* database: update to module "modules" (begin) */
			$sql = "UPDATE `modules`
					SET    `modules_order` = '" . $i . "'
					WHERE  `modules_id` = '" . $target_item_id . "'";
			$result = mysqli_query($con, $sql);
			/* database: update to module "modules" (end) */
			
		}
		
	}
		
	/* log */
	if ($configs["log"]) {
		$log = array(
			"log_name" => "sort data",
			"log_function" => __FUNCTION__,
			"log_violation" => "normal",
			"log_content_hash" => serialize($parameters),
			"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
			"log_type" => "frontend",
			"log_ip" => $_SERVER["REMOTE_ADDR"],
			"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"log_date_created" => gmdate("Y-m-d H:i:s"),
			"modules_record_key" => "",
			"modules_record_target" => "",
			"modules_id" => "5",
			"modules_name" => "modules",
			"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
			"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
			"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
			"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
		);
		create_log_data($log);
		unset($log);
		$log = array();
	}
		
	$response["status"] = true;
	$response["message"] = translate("Successfully sorted data");
	$response["values"] = $parameters;
	unset($parameters);
	$parameters = array();
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function get_modules_data_all_by_modules_pages_parent_link_field($target, $start = 0, $limit = "", $sort_by = "modules_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_pages_parent_link_field` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_data_all_by_modules_pages_parent_link_field($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_pages_parent_link_field` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_pages_parent_link_field_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "pages" */
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				if ($query["COLUMN_NAME"] != "pages_id" && $query["COLUMN_NAME"] != "pages_link") {
					$query["COLUMN_NAME_HUMAN"] = trim(ucwords(str_replace("_", " ", str_replace($query["TABLE_NAME"], "", $query["COLUMN_NAME"]))));
					if ($query["COLUMN_NAME_HUMAN"] == "Id") {
						$query["COLUMN_NAME_HUMAN"] = "ID";
					}
					$queries[$i] = $query;
					$i++;
				}
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			$response["target"] = $_POST["target"];
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_table_field_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "pages" */
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				$query["COLUMN_NAME_HUMAN"] = trim(ucwords(str_replace("_", " ", str_replace($query["TABLE_NAME"], "", $query["COLUMN_NAME"]))));
				if ($query["COLUMN_NAME_HUMAN"] == "Id") {
					$query["COLUMN_NAME_HUMAN"] = "ID";
				}
				$queries[$i] = $query;
				$i++;
				$table_name = $query["TABLE_NAME"];
			}
			$action_field = array(
				"COLUMN_NAME" => $table_name."_actions",
				"TABLE_NAME" => $table_name,
				"COLUMN_NAME_HUMAN" => "Actions",
				0 => $table_name."_actions",
				1 => $table_name,
				2 => "Actions",
			);
			$queries[$i] = $action_field;

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			$response["target"] = $_POST["target"];
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_pages_parent_link_field_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "pages" */
	$sql = "
	EXPLAIN 
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_pages_parent_link_field_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_link` = '".$target."'
	".$extra_sql_data_all."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["pages_parent_link_field_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_parent_link_field_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_data_all_by_modules_datatable_field_name($target, $start = 0, $limit = "", $sort_by = "modules_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_datatable_field_name` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["modules_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_data_all_by_modules_datatable_field_name($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_datatable_field_name` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_datatable_field_name_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_modules_datatable_field_name_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "pages" */
	$sql = "
	EXPLAIN SELECT *
	FROM `pages`
	WHERE `pages_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_datatable_field_name_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `modules_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "pages" */
	$sql = "
	SELECT *
	FROM `pages`
	WHERE `pages_id` = '".$target."'
	".$extra_sql_data_all."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["pages_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["pages_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_modules_content_data_all_by_modules_datatable_field_name($target, $activate = "") {

	/* get global: configurations */
	global $configs;
	global $modules_configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	$modules = get_modules_data_by_id($target["modules_id"]);
	
	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `".$modules["modules_key"]."_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "pages" */
	if (!empty($target["modules_record_target"]) && $target["modules_record_target"] != "") {
		$sql = "
		SELECT *
		FROM `".$modules["modules_db_name"]."`
		WHERE `".$modules["modules_key"]."_id` = '".$target["modules_record_target"]."'
		".$extra_sql_data_all."
		LIMIT 1";
	} else {
		$sql = "
		SELECT *
		FROM `".$modules["modules_db_name"]."`
		".$extra_sql_data_all;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {

            if (!empty($target["modules_record_target"]) && $target["modules_record_target"] != "") {

                $query = mysqli_fetch_array($result);

				$query[$target."_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query[$target."_date_created"], $configs["datetimezone"])));
				
            } else {

				$queries = array();
				$i = 0;
				while($query = mysqli_fetch_array($result)) {

					$query[$target."_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query[$target."_date_created"], $configs["datetimezone"])));

					$queries[$i] = $query;
					$i++;

				}

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			if (!empty($target["modules_record_target"]) && $target["modules_record_target"] != "") {
				$response["values"] = $query;
				
				unset($query);
				$query = array();
			} else {
				$response["values"] = $queries;
				
				unset($query);
				$query = array();
				unset($queries);
				$queries = array();
			}

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
		mysqli_free_result($result);
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>".$target."</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

?>