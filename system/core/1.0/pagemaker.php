<?php
function page_load($pages_link){

	/* get global: configurations */
	global $configs;
	
	global $page;

	$con = start();
	$pages_link = mysqli_real_escape_string($con, $pages_link);
	if (isset($_GET["language"]) && !empty($_GET["language"])) {	
		$languages_short_name = mysqli_real_escape_string($con, $_GET["language"]);
	}

	if ($configs["online"]) {

		$sql_languages = "
		SELECT * FROM `languages`
		WHERE `languages_short_name` LIKE '" . str_replace(".php", "", $pages_link) . "'";
		$result_languages = mysqli_query($con, $sql_languages);
		if ($result_languages) {
			$num_languages = mysqli_num_rows($result_languages);
			if ($num_languages > 0) {
				while ($query_languages = mysqli_fetch_assoc($result_languages)) {
					if ($pages_data["languages_id"] != $query_languages["languages_id"]) {
						$languages_sql_index_command = "
		  OR (`pages_link` = 'index.php' 
		  AND `languages_short_name` LIKE '" . str_replace(".php", "", $pages_link) . "'
		  AND `languages_short_name_parent_link` = '1')";
					} else {
						$languages_sql_command = "
		  AND `languages_short_name_parent_link` = '1'";
					}
				}
			} else {
				$languages_sql_command = "
		  AND `languages_short_name_parent_link` = '1'";
			}
		}

		$ids = explode("/", $_GET["id"]);
		$languages_parent_link = $ids[0];
		$sql_languages = "
		SELECT * FROM `languages`
		WHERE `languages_short_name` LIKE '" . $languages_parent_link . "'";
		$result_languages = mysqli_query($con, $sql_languages);
		if ($result_languages) {
			$num_languages = mysqli_num_rows($result_languages);
			if ($num_languages > 0) {
				while ($query_languages = mysqli_fetch_assoc($result_languages)) {
					if ($pages_data["languages_id"] != $query_languages["languages_id"]) {
						$languages_sql_command = "
		  AND (`languages_short_name` LIKE '" . $languages_parent_link . "'
		  AND `languages_short_name_parent_link` = '1')";
					} else {
						$languages_sql_command = "
		  AND `languages_short_name_parent_link` = '0'";
					}
				}
			} else {
				$languages_sql_command = "
				AND `languages_short_name_parent_link` = '0'";
			}
		} else {
			$languages_sql_command = "
			AND `languages_short_name_parent_link` = '0'";
		}

		$sql = "
		SELECT * FROM `pages`
		WHERE `pages_link` = '" . $pages_link . "' 
		  " . $languages_sql_index_command . "
		  " . $languages_sql_command . "
		  AND `pages_activate` = '1' 
		LIMIT 1;";
		$result = mysqli_query($con, $sql);

		if ($result) {

			$num = mysqli_num_rows($result);

			if ($num > 0) {

				global $pages_data;

				unset($pages_data);
				$pages_data = array();

				$pages_data = mysqli_fetch_array($result);

				$configs["language"] = strtolower($pages_data["languages_short_name"]);
				$pages_data["canonical_url"] = rewrite_url($pages_link, $configs["language"], true);

				/* log */
				if ($configs["log"]) {
					$log = array(
						"log_name" => "load page",
						"log_function" => __FUNCTION__,
						"log_violation" => "low",
						"log_content_hash" => "",
						"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
						"log_type" => "frontend",
						"log_ip" => $_SERVER["REMOTE_ADDR"],
						"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
						"log_date_created" => gmdate("Y-m-d H:i:s"),
						"modules_record_key" => "pages_link",
						"modules_record_target" => rewrite_url($pages_link, $configs["language"], true),
						"modules_id" => "8",
						"modules_name" => "pages",
						"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
						"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
						"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
						"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
					);
					create_log_data($log);
					unset($log);
					$log = array();
				}
									
				$sql_languages = "
				SELECT * FROM `languages`
				WHERE `languages_activate` = '1'";
				$result_languages = mysqli_query($con, $sql_languages);
				if ($result_languages) {
					$num_languages = mysqli_num_rows($result_languages);
					if ($num_languages > 0) {
						while ($query_languages = mysqli_fetch_assoc($result_languages)) {
							if ($pages_data["languages_id"] != $query_languages["languages_id"]) {
								if ($pages_data["pages_translate"] == 0) {
									$sql_transalate = "
									SELECT `pages_link` FROM `pages`
									WHERE `pages_translate` = '" . $pages_data["pages_id"] . "' 
									  AND `languages_id` = '" . $query_languages["languages_id"] . "'
									  AND `pages_activate` = '1'
									LIMIT 1";
									$result_transalate = mysqli_query($con, $sql_transalate);
									if ($result_transalate) {
										$num_translate = mysqli_num_rows($result_transalate);
										if ($num_translate > 0) {
											$query_translate = mysqli_fetch_assoc($result_transalate);
											$pages_data["pages_translate_link"][$query_languages["languages_id"]] = $query_translate["pages_link"];
											$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = $query_translate["pages_link"];
										} else {
											$pages_data["pages_translate_link"][$query_languages["languages_id"]] = "index.php";
											$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = "index.php";
										}
										mysqli_free_result($result_transalate);
									} else {
										$pages_data["pages_translate_link"][$query_languages["languages_id"]] = "index.php";
										$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = "index.php";
									}
								} else {
									$sql_transalate = "
									SELECT `pages_link` FROM `pages`
									WHERE `pages_id` = '" . $pages_data["pages_translate"] . "' 
									  AND `languages_id` = '" . $query_languages["languages_id"] . "'
									  AND `pages_activate` = '1'
									LIMIT 1";
									$result_transalate = mysqli_query($con, $sql_transalate);
									if ($result_transalate) {
										$num_translate = mysqli_num_rows($result_transalate);
										if ($num_translate > 0) {
											$query_translate = mysqli_fetch_assoc($result_transalate);
											$pages_data["pages_translate_link"][$query_languages["languages_id"]] = $query_translate["pages_link"];
											$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = $query_translate["pages_link"];
										} else {
											$sql_transalate = "
											SELECT `pages_link` FROM `pages`
											WHERE `pages_translate` = '" . $pages_data["pages_translate"] . "' 
											  AND `languages_id` = '" . $query_languages["languages_id"] . "'
											  AND `pages_activate` = '1'
											LIMIT 1";
											$result_transalate = mysqli_query($con, $sql_transalate);
											$num_translate = mysqli_num_rows($result_transalate);
											if ($num_translate > 0) {
												$query_translate = mysqli_fetch_assoc($result_transalate);
												$pages_data["pages_translate_link"][$query_languages["languages_id"]] = $query_translate["pages_link"];
												$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = $query_translate["pages_link"];
											} else {
												$pages_data["pages_translate_link"][$query_languages["languages_id"]] = "index.php";
												$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = "index.php";
											}
										}
										mysqli_free_result($result_transalate);
									} else {
										$pages_data["pages_translate_link"][$query_languages["languages_id"]] = "index.php";
										$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = "index.php";
									}
								}
							} else {
								$pages_data["pages_translate_link"][$query_languages["languages_id"]] = $pages_data["pages_link"];
								$pages_data["pages_translate_link"][$query_languages["languages_short_name"]] = $pages_data["pages_link"];
							}
						}
					}
					mysqli_free_result($result_languages);
				}
					
				include("templates/".$configs["template"]."/".$pages_data["pages_template"]);

			} else {

				$sql_404 = "
				SELECT * FROM pages
				WHERE pages_link = '404.php'
				LIMIT 1;";
				$result_404 = mysqli_query($con, $sql_404);
				if ($result_404) {
					$num_404 = mysqli_num_rows($result_404);
					if ($num_404 > 0) {
						$query_404 = mysqli_fetch_array($result_404);
						header("Location: " . rewrite_url($query_404["pages_link"]));
					} else {
						if (file_exists("templates/".$configs["template"]."/404.php")) {
							include("templates/".$configs["template"]."/404.php");
						} else {
							if (file_exists("templates/".$configs["template"]."/404.php")) {
								include("templates/".$configs["template"]."/404.php");
							} else {
								echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 404") . '</h1><p>' . translate("We are sorry but the page you are looking for was not found..") . '</p>
</body>
</html>';
							}
						}
					}
				} else {
					if (file_exists("templates/".$configs["template"]."/404.php")) {
						include("templates/".$configs["template"]."/404.php");
					} else {
						if (file_exists("templates/".$configs["template"]."/404.php")) {
							include("templates/".$configs["template"]."/404.php");
						} else {
							echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 404") . '</h1><p>' . translate("We are sorry but the page you are looking for was not found..") . '</p>
</body>
</html>';
						}
					}
				}
				mysqli_free_result($result_404);
				exit();
			}

		} else {

			$sql_500 = "
			SELECT * FROM pages
			WHERE pages_link = '500.php'
			LIMIT 1;";
			$result_500 = mysqli_query($con, $sql_500);
			if ($result_500) {
				$num_500 = mysqli_num_rows($result_500);
				if ($num_500 > 0) {
					$query_500 = mysqli_fetch_array($result_500);
					header("Location: " . rewrite_url($query_500["pages_link"]));
				} else {
					if (file_exists("templates/".$configs["template"]."/500.php")) {
						include("templates/".$configs["template"]."/500.php");
					} else {
						if (file_exists("templates/".$configs["template"]."/500.php")) {
							include("templates/".$configs["template"]."/500.php");
						} else {
							echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 500") . '</h1><p>' . translate("We are sorry but our server encountered an internal error..") . '</p>
</body>
</html>';
						}
					}
				}
			} else {
				if (file_exists("templates/".$configs["template"]."/500.php")) {
					include("templates/".$configs["template"]."/500.php");
				} else {
					if (file_exists("templates/".$configs["template"]."/500.php")) {
						include("templates/".$configs["template"]."/500.php");
					} else {
						echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 500") . '</h1><p>' . translate("We are sorry but our server encountered an internal error..") . '</p>
</body>
</html>';
					}
				}
			}
			mysqli_free_result($result_500);
		}
		exit();
	} else {

		$sql_503 = "
		SELECT * FROM pages
		WHERE pages_link = '503.php'
		LIMIT 1;";
		$result_503 = mysqli_query($con, $sql_503);
		if ($result_503) {
			$num_503 = mysqli_num_rows($result_503);
			if ($num_503 > 0) {
				$query_503 = mysqli_fetch_array($result_503);
				header("Location: " . rewrite_url($query_503["pages_link"]));
			} else {
				if (file_exists("templates/".$configs["template"]."/503.php")) {
					include("templates/".$configs["template"]."/503.php");
				} else {
					if (file_exists("templates/".$configs["template"]."/503.php")) {
						include("templates/".$configs["template"]."/503.php");
					} else {
						echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 503") . '</h1><p>' . translate("We are sorry but our service is currently not available..") . '</p>
</body>
</html>';
					}
				}
			}
		} else {
			if (file_exists("templates/".$configs["template"]."/500.php")) {
				include("templates/".$configs["template"]."/500.php");
			} else {
				if (file_exists("templates/".$configs["template"]."/500.php")) {
					include("templates/".$configs["template"]."/500.php");
				} else {
					echo '
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<style>
	body {
		font-family: "Prompt", sans-serif;
		text-align: center;
	}
	</style>
</head>
<body>
<h1>' . translate("ERROR 500") . '</h1><p>' . translate("We are sorry but our server encountered an internal error..") . '</p>
</body>
</html>';
				}
			}
		}
		mysqli_free_result($result_503);
		exit();
	}

	mysqli_free_result($result);
	stop($con);

}

if (isset($_GET["id"]) && $_GET["id"] != "") {
	$ids = explode("/", $_GET["id"]);
	$last_array = count($ids) - 1;
	$last_id = $ids[$last_array];
	$pages_link = get_link_file($last_id);
} else {
	$pages_link = 'index.php';
}
if (!isset($_GET["page"]) || $_GET["page"] == "" || empty($_GET["page"])) {
	$page = 1;
} else {
	$page = $_GET["page"];
}
page_load($pages_link);
?>
