<!DOCTYPE HTML>

<html>

<head>

    <base href="<?php echo $configs["template_path"]; ?>">

    <meta charset="utf-8" />
    
    <title><?php echo $pages_data["pages_title"]; ?></title>
    <meta name="description" content="<?php echo $pages_data["pages_meta_description"]; ?>" />
    <meta name="keywords" content="<?php echo $pages_data["pages_meta_keyword"]; ?>" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    
</head>
    
<body>

    <!-- Banner -->
        <section id="banner">
            <h2><strong>Sample</strong> template</h2>
            <p>For GreenMama Framework</p>
            <ul class="actions">
                <li><a href="<?php echo rewrite_url("index.php"); ?>" class="button special">Start</a></li>
            </ul>
        </section>

    <!-- Footer -->
        <footer id="footer">
            <div class="copyright">
                &copy; <a href="http://templated.co/">GreenMama</a> Framework
            </div>
        </footer>

    <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="assets/js/main.js"></script>

</body>
</html>