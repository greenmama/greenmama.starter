-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2018 at 09:09 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `starter`
--

-- --------------------------------------------------------

--
-- Table structure for table `abstract_modules`
--

CREATE TABLE `abstract_modules` (
  `abstract_modules_id` int(11) NOT NULL,
  `abstract_modules_name` varchar(80) NOT NULL,
  `abstract_modules_varname` varchar(80) DEFAULT NULL,
  `abstract_modules_description` text,
  `abstract_modules_form_method` text NOT NULL,
  `abstract_modules_component_modules` text,
  `abstract_modules_component_groups` text,
  `abstract_modules_component_users` text,
  `abstract_modules_component_languages` text,
  `abstract_modules_component_pages` text,
  `abstract_modules_component_media` text,
  `abstract_modules_component_commerce` text,
  `abstract_modules_database_engine` text NOT NULL,
  `abstract_modules_data_sortable` text,
  `abstract_modules_data_addable` text,
  `abstract_modules_data_viewonly` text,
  `abstract_modules_template` text,
  `abstract_modules_icon` text,
  `abstract_modules_category` text,
  `abstract_modules_subject` text,
  `abstract_modules_subject_icon` text,
  `abstract_modules_order` int(11) DEFAULT '0',
  `abstract_modules_date_created` datetime DEFAULT NULL,
  `abstract_modules_activate` tinyint(11) DEFAULT '0',
  `users_id` int(11) DEFAULT NULL,
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table for Module Modules';

-- --------------------------------------------------------

--
-- Table structure for table `abstract_references`
--

CREATE TABLE `abstract_references` (
  `abstract_references_id` int(11) NOT NULL,
  `abstract_references_label` varchar(50) NOT NULL,
  `abstract_references_varname` varchar(50) DEFAULT NULL,
  `abstract_references_type` text NOT NULL,
  `abstract_references_modules` text NOT NULL,
  `abstract_references_placeholder` varchar(100) DEFAULT NULL,
  `abstract_references_help` varchar(200) DEFAULT NULL,
  `abstract_references_require` text,
  `abstract_references_readonly` text,
  `abstract_references_disable` text,
  `abstract_references_hidden` text,
  `abstract_references_validate_string_min` int(11) DEFAULT NULL,
  `abstract_references_validate_string_max` int(11) DEFAULT NULL,
  `abstract_references_validate_number_min` int(11) DEFAULT NULL,
  `abstract_references_validate_number_max` int(11) DEFAULT NULL,
  `abstract_references_validate_date_min` date DEFAULT NULL,
  `abstract_references_validate_date_max` date DEFAULT NULL,
  `abstract_references_validate_datetime_min` datetime DEFAULT NULL,
  `abstract_references_validate_datetime_max` datetime DEFAULT NULL,
  `abstract_references_validate_password_equal_to` text,
  `abstract_references_validate_email` text,
  `abstract_references_validate_password` text,
  `abstract_references_validate_website` text,
  `abstract_references_validate_no_space` text,
  `abstract_references_validate_no_specialchar_soft` text,
  `abstract_references_validate_no_specialchar_hard` text,
  `abstract_references_validate_upper` text,
  `abstract_references_validate_lower` text,
  `abstract_references_validate_number` text,
  `abstract_references_validate_digit` text,
  `abstract_references_validate_unique` text,
  `abstract_references_prefix` varchar(100) DEFAULT NULL,
  `abstract_references_suffix` varchar(100) DEFAULT NULL,
  `abstract_references_default_value_type` text,
  `abstract_references_default_value` text,
  `abstract_references_default_switch` text,
  `abstract_references_input_list` text,
  `abstract_references_input_list_static_value` text,
  `abstract_references_input_list_dynamic_module` text,
  `abstract_references_input_list_dynamic_id_column` varchar(200) DEFAULT NULL,
  `abstract_references_input_list_dynamic_value_column` varchar(200) DEFAULT NULL,
  `abstract_references_file_selector_type` text,
  `abstract_references_date_format` text,
  `abstract_references_color_format` text,
  `abstract_references_tags_format` text,
  `abstract_references_image_folder` varchar(200) DEFAULT NULL,
  `abstract_references_image_width` int(11) DEFAULT NULL,
  `abstract_references_image_height` int(11) DEFAULT NULL,
  `abstract_references_image_width_ratio` int(11) DEFAULT NULL,
  `abstract_references_image_height_ratio` int(11) DEFAULT NULL,
  `abstract_references_gridWidth` varchar(200) DEFAULT NULL,
  `abstract_references_alignment` text,
  `abstract_references_order` int(11) DEFAULT '0',
  `abstract_references_date_created` datetime DEFAULT NULL,
  `abstract_references_activate` tinyint(11) DEFAULT '0',
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table for Module References';

-- --------------------------------------------------------

--
-- Table structure for table `abstract_spreadinputs`
--

CREATE TABLE `abstract_spreadinputs` (
  `abstract_spreadinputs_id` int(11) NOT NULL,
  `abstract_spreadinputs_label` varchar(50) NOT NULL,
  `abstract_spreadinputs_varname` varchar(50) DEFAULT NULL,
  `abstract_spreadinputs_type` text NOT NULL,
  `abstract_spreadinputs_references` text NOT NULL,
  `abstract_spreadinputs_placeholder` varchar(100) DEFAULT NULL,
  `abstract_spreadinputs_help` varchar(200) DEFAULT NULL,
  `abstract_spreadinputs_require` text,
  `abstract_spreadinputs_readonly` text,
  `abstract_spreadinputs_disable` text,
  `abstract_spreadinputs_hidden` text,
  `abstract_spreadinputs_validate_string_min` int(11) DEFAULT NULL,
  `abstract_spreadinputs_validate_string_max` int(11) DEFAULT NULL,
  `abstract_spreadinputs_validate_number_min` int(11) DEFAULT NULL,
  `abstract_spreadinputs_validate_number_max` int(11) DEFAULT NULL,
  `abstract_spreadinputs_validate_date_min` date DEFAULT NULL,
  `abstract_spreadinputs_validate_date_max` date DEFAULT NULL,
  `abstract_spreadinputs_validate_datetime_min` datetime DEFAULT NULL,
  `abstract_spreadinputs_validate_datetime_max` datetime DEFAULT NULL,
  `abstract_spreadinputs_validate_password_equal_to` text,
  `abstract_spreadinputs_validate_email` text,
  `abstract_spreadinputs_validate_password` text,
  `abstract_spreadinputs_validate_website` text,
  `abstract_spreadinputs_validate_no_space` text,
  `abstract_spreadinputs_validate_no_specialchar_soft` text,
  `abstract_spreadinputs_validate_no_specialchar_hard` text,
  `abstract_spreadinputs_validate_upper` text,
  `abstract_spreadinputs_validate_lower` text,
  `abstract_spreadinputs_validate_number` text,
  `abstract_spreadinputs_validate_digit` text,
  `abstract_spreadinputs_validate_unique` text,
  `abstract_spreadinputs_prefix` varchar(100) DEFAULT NULL,
  `abstract_spreadinputs_suffix` varchar(100) DEFAULT NULL,
  `abstract_spreadinputs_default_value_type` text,
  `abstract_spreadinputs_default_value` text,
  `abstract_spreadinputs_default_switch` text,
  `abstract_spreadinputs_input_list` text,
  `abstract_spreadinputs_input_list_static_value` text,
  `abstract_spreadinputs_input_list_dynamic_module` text,
  `abstract_spreadinputs_input_list_dynamic_id_column` varchar(200) DEFAULT NULL,
  `abstract_spreadinputs_input_list_dynamic_value_column` varchar(200) DEFAULT NULL,
  `abstract_spreadinputs_file_selector_type` text,
  `abstract_spreadinputs_date_format` text,
  `abstract_spreadinputs_color_format` text,
  `abstract_spreadinputs_tags_format` text,
  `abstract_spreadinputs_image_folder` varchar(200) DEFAULT NULL,
  `abstract_spreadinputs_image_width` int(11) DEFAULT NULL,
  `abstract_spreadinputs_image_height` int(11) DEFAULT NULL,
  `abstract_spreadinputs_image_width_ratio` int(11) DEFAULT NULL,
  `abstract_spreadinputs_image_height_ratio` int(11) DEFAULT NULL,
  `abstract_spreadinputs_gridWidth` varchar(200) DEFAULT NULL,
  `abstract_spreadinputs_alignment` text,
  `abstract_spreadinputs_order` int(11) DEFAULT '0',
  `abstract_spreadinputs_date_created` datetime DEFAULT NULL,
  `abstract_spreadinputs_activate` tinyint(11) DEFAULT '0',
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table for Module Spread Inputs';

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE `api` (
  `api_id` int(11) NOT NULL,
  `api_title` varchar(240) NOT NULL,
  `api_key` varchar(240) DEFAULT NULL,
  `api_secret` varchar(240) DEFAULT NULL,
  `api_allowed_modules` text,
  `api_date_created` datetime DEFAULT NULL,
  `api_activate` tinyint(11) DEFAULT '0',
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table for Module API';

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `groups_id` int(11) NOT NULL,
  `groups_name` varchar(100) NOT NULL,
  `groups_order` int(11) DEFAULT '0',
  `groups_date_created` datetime DEFAULT NULL,
  `groups_protected` tinyint(1) DEFAULT '0',
  `groups_activate` tinyint(1) DEFAULT '0',
  `modules_ids` text NOT NULL,
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`groups_id`, `groups_name`, `groups_order`, `groups_date_created`, `groups_protected`, `groups_activate`, `modules_ids`, `users_id`, `users_username`, `users_name`, `users_last_name`) VALUES
(1, 'Creator', 1, '2017-10-01 00:00:00', 1, 1, 's:0:\"\";', 1, 'admin', '', ''),
(2, 'Super Administrator', 2, '2017-10-01 00:00:00', 1, 1, 's:0:\"\";', 1, 'admin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `languages_id` int(10) UNSIGNED NOT NULL,
  `languages_name` varchar(100) NOT NULL,
  `languages_short_name` varchar(5) NOT NULL,
  `languages_order` int(11) DEFAULT '0',
  `languages_date_created` datetime DEFAULT NULL,
  `languages_activate` tinyint(1) DEFAULT '0',
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`languages_id`, `languages_name`, `languages_short_name`, `languages_order`, `languages_date_created`, `languages_activate`, `users_id`, `users_username`, `users_name`, `users_last_name`) VALUES
(1, 'English', 'EN', 1, '2017-10-01 00:00:00', 1, 1, 'admin', '', ''),
(2, 'Thai', 'TH', 2, '2017-10-01 00:00:00', 1, 1, 'admin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `log_name` varchar(200) NOT NULL,
  `log_function` varchar(150) NOT NULL,
  `modules_record_target` varchar(100) NOT NULL,
  `modules_record_key` varchar(240) NOT NULL,
  `log_violation` enum('low','normal','risk','danger') NOT NULL,
  `log_content_hash` text NOT NULL,
  `log_link` text NOT NULL,
  `log_type` enum('frontend','backend') NOT NULL DEFAULT 'frontend',
  `log_ip` varchar(15) NOT NULL,
  `log_user_agent` text NOT NULL,
  `log_date_created` datetime DEFAULT NULL,
  `modules_id` varchar(100) NOT NULL,
  `modules_name` varchar(100) NOT NULL,
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='	';

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` int(11) NOT NULL,
  `media_name` varchar(200) NOT NULL,
  `media_file` varchar(300) NOT NULL,
  `media_type` varchar(10) NOT NULL,
  `modules_record_target` varchar(100) NOT NULL,
  `modules_record_key` varchar(240) NOT NULL,
  `media_date_created` datetime DEFAULT NULL,
  `media_activate` tinyint(1) DEFAULT '0',
  `modules_id` varchar(100) NOT NULL,
  `modules_name` varchar(100) NOT NULL,
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `modules_id` int(11) NOT NULL,
  `modules_name` varchar(100) NOT NULL,
  `modules_link` varchar(250) NOT NULL,
  `modules_description` text NOT NULL,
  `modules_icon` varchar(100) NOT NULL,
  `modules_category` varchar(100) NOT NULL,
  `modules_subject` varchar(100) NOT NULL,
  `modules_subject_icon` varchar(100) NOT NULL,
  `modules_key` varchar(20) NOT NULL,
  `modules_db_name` varchar(100) NOT NULL,
  `modules_function_link` varchar(250) NOT NULL,
  `modules_pages_template` text NOT NULL,
  `modules_individual_pages_parent_link` int(11) DEFAULT '0',
  `modules_pages_template_settings` text NOT NULL,
  `modules_pages_parent_link_field` text NOT NULL,
  `modules_image_crop_quality` int(11) DEFAULT '75',
  `modules_image_crop_thumbnail` tinyint(1) DEFAULT '1',
  `modules_image_crop_thumbnail_aspectratio` tinyint(1) DEFAULT '1',
  `modules_image_crop_thumbnail_quality` int(11) DEFAULT '75',
  `modules_image_crop_thumbnail_width` int(11) NOT NULL,
  `modules_image_crop_thumbnail_height` int(11) NOT NULL,
  `modules_image_crop_large` tinyint(1) DEFAULT '0',
  `modules_image_crop_large_aspectratio` tinyint(1) DEFAULT '1',
  `modules_image_crop_large_quality` int(11) DEFAULT '75',
  `modules_image_crop_large_width` int(11) NOT NULL,
  `modules_image_crop_large_height` int(11) NOT NULL,
  `modules_datatable_field` text NOT NULL,
  `modules_order` int(11) DEFAULT '0',
  `modules_date_created` datetime DEFAULT NULL,
  `modules_protected` tinyint(1) DEFAULT '0',
  `modules_activate` tinyint(1) DEFAULT '0',
  `pages_id` int(11) DEFAULT NULL,
  `pages_link` varchar(250) NOT NULL,
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`modules_id`, `modules_name`, `modules_link`, `modules_description`, `modules_icon`, `modules_category`, `modules_subject`, `modules_subject_icon`, `modules_key`, `modules_db_name`, `modules_function_link`, `modules_pages_template`, `modules_individual_pages_parent_link`, `modules_pages_template_settings`, `modules_pages_parent_link_field`, `modules_image_crop_quality`, `modules_image_crop_thumbnail`, `modules_image_crop_thumbnail_aspectratio`, `modules_image_crop_thumbnail_quality`, `modules_image_crop_thumbnail_width`, `modules_image_crop_thumbnail_height`, `modules_image_crop_large`, `modules_image_crop_large_aspectratio`, `modules_image_crop_large_quality`, `modules_image_crop_large_width`, `modules_image_crop_large_height`, `modules_datatable_field`, `modules_order`, `modules_date_created`, `modules_protected`, `modules_activate`, `pages_id`, `pages_link`, `users_id`, `users_username`, `users_name`, `users_last_name`) VALUES
(1, 'Modules Abstract', 'abstract-modules.php', '', '<i class=\'si si-grid\'></i>', 'System', 'Creator', '<i class=\'si si-layers\'></i>', 'abstract_modules', 'abstract_modules', 'core/1.0/abstract-modules.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 15, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(2, 'References', 'abstract-references.php', '', '<i class=\'fa fa-database\'></i>', 'System', 'Creator', '<i class=\'si si-layers\'></i>', 'abstract_references', 'abstract_references', 'core/1.0/abstract-references.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 16, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(3, 'Spread Inputs', 'abstract-spread-inputs.php', '', '<i class=\'fa fa-list-ol\'></i>', 'System', 'Creator', '<i class=\'si si-layers\'></i>', 'abstract_spreadinput', 'abstract_spreadinputs', 'core/1.0/abstract-spread-inputs.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 17, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(5, 'Modules', 'modules.php', '', '<i class=\'si si-grid\'></i>', 'System', '', '', 'modules', 'modules', 'core/1.0/modules.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 5, '2017-10-01 00:00:00', 1, 1, 0, '', 1, 'admin', '', ''),
(6, 'Users', 'users.php', '', '<i class=\'si si-user\'></i>', 'Administrators', '', '', 'users', 'users', 'core/1.0/users.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 6, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(7, 'Groups', 'groups.php', '', '<i class=\'si si-users\'></i>', 'Administrators', '', '', 'groups', 'groups', 'core/1.0/groups.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 7, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(8, 'Languages', 'languages.php', '', '<i class=\'si si-speech\'></i>', 'System', '', '', 'languages', 'languages', 'core/1.0/languages.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 8, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(9, 'Pages', 'pages.php', '', '<i class=\'si si-docs\'></i>', 'Files', '', '', 'pages', 'pages', 'core/1.0/pages.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 9, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(10, 'Media', 'media.php', '', '<i class=\'si si-picture\'></i>', 'Files', '', '', 'media', 'media', 'core/1.0/media.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 10, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(11, 'Log', 'log.php', '', '<i class=\'si si-clock\'></i>', 'Maintenance', '', '', 'log', 'log', 'core/1.0/log.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 11, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', ''),
(12, 'API', 'api.php', '', '<i class=\'si si-share\'></i>', 'System', '', '', 'api', 'api', 'core/1.0/api.php', '', 0, '', '', 75, 1, 1, 75, 200, 200, 1, 1, 75, 400, 400, '', 12, '2017-10-01 00:00:00', 0, 1, 0, '', 1, 'admin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pages_id` int(11) NOT NULL,
  `pages_title` varchar(240) NOT NULL,
  `pages_link` varchar(250) NOT NULL,
  `pages_meta_title` varchar(240) NOT NULL,
  `pages_meta_description` varchar(400) NOT NULL,
  `pages_meta_keyword` text NOT NULL,
  `pages_template` varchar(250) NOT NULL,
  `pages_translate` int(11) DEFAULT '0',
  `modules_record_key` varchar(100) NOT NULL,
  `modules_record_target` varchar(240) NOT NULL,
  `pages_date_created` datetime DEFAULT NULL,
  `pages_activate` tinyint(1) DEFAULT '0',
  `modules_id` varchar(100) NOT NULL,
  `modules_name` varchar(100) NOT NULL,
  `users_id` int(11) DEFAULT '1',
  `users_username` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL,
  `languages_id` int(11) DEFAULT '1',
  `languages_short_name` varchar(5) NOT NULL,
  `languages_short_name_parent_link` tinyint(1) DEFAULT '0',
  `languages_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `pages_title`, `pages_link`, `pages_meta_title`, `pages_meta_description`, `pages_meta_keyword`, `pages_template`, `pages_translate`, `modules_record_key`, `modules_record_target`, `pages_date_created`, `pages_activate`, `modules_id`, `modules_name`, `users_id`, `users_username`, `users_name`, `users_last_name`, `languages_id`, `languages_short_name`, `languages_short_name_parent_link`, `languages_name`) VALUES
(1, 'Index', 'index.php', '', '', '', 'index.php', 0, '', '', '2017-10-01 00:00:00', 1, '', '', 1, 'admin', '', '', 1, 'EN', 0, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `users_username` varchar(100) NOT NULL,
  `users_password` varchar(200) NOT NULL,
  `users_email` varchar(100) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_last_name` varchar(100) NOT NULL,
  `users_nick_name` varchar(50) DEFAULT NULL,
  `users_image` text,
  `users_order` int(11) DEFAULT '0',
  `users_remember_login` text,
  `users_api_sessions` text,
  `users_email_notify` tinyint(1) DEFAULT '1',
  `users_ip_recent` varchar(15) DEFAULT '',
  `users_user_agent_recent` text,
  `users_date_recent` datetime DEFAULT NULL,
  `users_date_created` datetime DEFAULT NULL,
  `users_level` int(11) DEFAULT '6',
  `users_protected` tinyint(1) DEFAULT '0',
  `users_activate` tinyint(1) DEFAULT '0',
  `groups_id` int(11) DEFAULT '2',
  `groups_name` varchar(100) NOT NULL,
  `users_users_id` int(11) DEFAULT '1',
  `users_users_username` varchar(100) NOT NULL,
  `users_users_name` varchar(100) NOT NULL,
  `users_users_last_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `users_username`, `users_password`, `users_email`, `users_name`, `users_last_name`, `users_nick_name`, `users_image`, `users_order`, `users_remember_login`, `users_api_sessions`, `users_email_notify`, `users_ip_recent`, `users_user_agent_recent`, `users_date_recent`, `users_date_created`, `users_level`, `users_protected`, `users_activate`, `groups_id`, `groups_name`, `users_users_id`, `users_users_username`, `users_users_name`, `users_users_last_name`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', '', 0, 'a:0:{}', '', 0, '::1', '', '2017-10-01 00:00:00', '2017-10-01 00:00:00', 1, 1, 1, 1, '', 1, 'admin', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abstract_modules`
--
ALTER TABLE `abstract_modules`
  ADD PRIMARY KEY (`abstract_modules_id`);

--
-- Indexes for table `abstract_references`
--
ALTER TABLE `abstract_references`
  ADD PRIMARY KEY (`abstract_references_id`);

--
-- Indexes for table `abstract_spreadinputs`
--
ALTER TABLE `abstract_spreadinputs`
  ADD PRIMARY KEY (`abstract_spreadinputs_id`);

--
-- Indexes for table `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`groups_id`),
  ADD KEY `groups_name` (`groups_name`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`languages_id`),
  ADD KEY `languages_short_name` (`languages_short_name`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `log_name` (`log_name`),
  ADD KEY `log_function` (`log_function`),
  ADD KEY `log_violation` (`log_violation`),
  ADD KEY `log_ip` (`log_ip`),
  ADD KEY `log_date_created` (`log_date_created`),
  ADD KEY `modules_id` (`modules_id`),
  ADD KEY `modules_name` (`modules_name`),
  ADD KEY `users_id` (`users_id`),
  ADD KEY `users_name` (`users_name`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`),
  ADD KEY `media_file` (`media_file`(255)),
  ADD KEY `media_type` (`media_type`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`modules_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pages_id`),
  ADD UNIQUE KEY `pages_link_2` (`pages_link`),
  ADD KEY `pages_link` (`pages_link`),
  ADD KEY `pages_meta_title` (`pages_meta_title`),
  ADD KEY `pages_meta_description` (`pages_meta_description`(333)),
  ADD KEY `pages_section` (`pages_template`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`),
  ADD UNIQUE KEY `users_username_2` (`users_username`),
  ADD KEY `users_username` (`users_username`),
  ADD KEY `users_email` (`users_email`),
  ADD KEY `users_name` (`users_name`),
  ADD KEY `users_nick_name` (`users_nick_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abstract_modules`
--
ALTER TABLE `abstract_modules`
  MODIFY `abstract_modules_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `abstract_references`
--
ALTER TABLE `abstract_references`
  MODIFY `abstract_references_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `abstract_spreadinputs`
--
ALTER TABLE `abstract_spreadinputs`
  MODIFY `abstract_spreadinputs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `groups_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `languages_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `modules_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
