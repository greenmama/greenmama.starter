<?php 
header('Content-type: application/xml');
echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<?php 
ini_set('max_execution_time', 0);
set_time_limit(0);
include("../config.php");
include("../system/core/".$configs["version"]."/initial.php");
include("../system/core/".$configs["version"]."/security.php");
include("../system/core/".$configs["version"]."/urlrewritting.php");
include("../system/core/".$configs["version"]."/datetime.php");
?>
<?php
echo '
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

$con = start();

$sql = "
SELECT *
FROM `pages` 
WHERE `pages_activate` = '1'";
$result = mysqli_query($con, $sql);
$num = mysqli_num_rows($result);
/* database: get data from module "pages" (begin) */

if ($num > 0) {
	while($query = mysqli_fetch_array($result)) {
		echo '
	<url>
  	  <loc>'.rewrite_url($query["pages_link"]).'</loc>
	</url>';	
	}
}	

unset($query);
$query = array();

stop($con);
mysqli_free_result($result);

echo '
</urlset>';
?>
