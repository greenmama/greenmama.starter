<?php
require("system/include.php");
include("system/core/".$configs["version"]."/dashboard.php");

/* configurations: Dashboard */
$title = translate("Dashboard");
$module_page = "index.php";

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Dashboard */
if (!authentication_session_users()) {
	authentication_deny();
}

include("templates/".$configs["backend_template"]."/header.php");
include("templates/".$configs["backend_template"]."/overlay.php");
include("templates/".$configs["backend_template"]."/sidebar.php");
include("templates/".$configs["backend_template"]."/navbar.php");

global $configs;
$con = start();

$count_pages = count_pages_data_all();

$total_visit_today = count_log_by_page_load(date("H:i:s"),"HOUR_SECOND");
$total_visit_month = count_log_by_page_load((date("d")-1)." ".date("H:i:s"),"DAY_SECOND");
$total_visit_all = count_log_by_page_load();

$start = date("Y-m-d", strtotime(datetime_convert(get_start_date(), $configs["datetimezone"])));
$now = date("Y-m-d");
$total_date = count_log_date_created_by_page_load();
$avg_visit = $total_visit_all / ($total_date + 1);

$agent = count_agent();

if(!empty($agent)) {
	$mobile = $agent["count_desktop"];
	$desktop = $agent["count_mobile"];
}

$top_page = top_page_traffic();

$users = users();
$users_today = user_date("today");
$users_week = user_date("week");
$users_month = user_date("month");
?>
<link href="plugins/jquery-jvectormap/jquery-jvectormap.min.css" rel="stylesheet">

<!-- Page Header -->
<div class="content bg-image-index overflow-hidden">
	<div class="push-50-t push-15">
		<h1 class="h2 text-white animated zoomIn"><?php echo translate("Dashboard"); ?></h1>
		<h2 class="h5 text-white-op animated zoomIn"><?php echo translate("Welcome"); ?>, <?php echo $_SESSION["users_name"]; ?></h2>
	</div>
</div>
<!-- END Page Header -->

<?php
if ($_SESSION["users_level"] != "4") {
?>

<!-- Stats -->
<div class="content bg-white border-b">
	<div class="row items-push text-uppercase">
		<div class="col-xs-6 col-sm-3">
			<div class="font-w700 text-gray-darker animated fadeIn"><?php echo translate("Total visit"); ?></div>
			<div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> <?php echo translate("Today"); ?></small></div>
			<a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">
				<?php echo number_format($total_visit_today); ?>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3">
			<div class="font-w700 text-gray-darker animated fadeIn"><?php echo translate("Total visit"); ?></div>
			<div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> <?php echo translate("This Month"); ?></small></div>
			<a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">
				<?php echo number_format($total_visit_month); ?>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3">
			<div class="font-w700 text-gray-darker animated fadeIn"><?php echo translate("Total visit"); ?></div>
			<div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> <?php echo translate("All time"); ?></small></div>
			<a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">
				<?php echo number_format($total_visit_all); ?>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3">
			<div class="font-w700 text-gray-darker animated fadeIn"><?php echo translate("Total visit"); ?></div>
			<div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> <?php echo translate("Since"); ?> <?php echo date_reformat($start); ?></small></div>
			<a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">
				<?php echo number_format($avg_visit, 2); ?>
			</a> <?php echo translate("per day"); ?>
		</div>
	</div>
</div>
<!-- END Stats -->

<div class="content">

	<div class="row">
		<div class="col-lg-12">
			<div class="block">
				<div class="block-header">
					<h3 class="block-title"><?php echo translate("Monthly Visit"); ?></h3>
				</div>
				<div class="block-content block-content-full">
					<div class="js-flot-lines"></div>
				</div>
			</div>
		</div>
		
	</div>

	<div class="row">
			
			<div class="col-lg-8">
			<div class="block">
				<div class="block-header">
					<h3 class="block-title"><?php echo translate("Top Pageview"); ?></h3>
				</div>
				<div class="block-content bg-gray-lighter">
					<div class="row items-push">
						<div class="col-lg-12">
							<div class="h1 font-w700 text-right"><?php echo number_format($count_pages); ?></div>
							<div class="font-w600 text-uppercase text-muted text-right"><small><?php echo translate("Total Pages"); ?></small></div>
						</div>
					</div>
				</div>
				<?php if (!empty($top_page)) { ?>
					<div class="block-content block-content-full remove-padding">
						<table class="table remove-margin-b font-s13">
							<tbody>
								<?php for ($i=0; $i < count($top_page); $i++) {  ?>
									<tr>
										<td class="font-w600">
											<a href="<?php echo $top_page[$i]["log_link"]; ?>">
												<?php echo urldecode($top_page[$i]["name"]); ?>
											</a>
										</td>
										<td class="font-w600 text-muted text-right" style="width: 70px;"><?php echo $top_page[$i]["visit"]; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="col-lg-4">
			<div class="block">
				<div class="block-header">
					<h3 class="block-title"><?php echo translate("Traffic Share"); ?></h3>
				</div>
				<div class="block-content block-content-full bg-gray-lighter text-center">
					<div class="chart-pie" style="height: 330px;"><canvas class="js-chartjs-pie"></canvas></div>
				</div>
				<div class="block-content text-center">
					<div class="row items-push text-center">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 border-r">
							<div class="font-s34 push-10"><i class="si si-screen-desktop fa-2x"></i></div>
							<div class="h5 font-w400"><?php echo number_format($desktop); ?></div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<div class="font-s34 push-10"><i class="si si-screen-smartphone fa-2x"></i></div>
							<div class="h5 font-w400"><?php echo number_format($mobile); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<?php if($users != "") { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="block">
					<div class="block-header bg-green">
						<h3 class="block-title"><?php echo translate("Content Editors Stats"); ?></h3>
					</div>
					<div class="block-content">
						<table class="table table-bordered table-striped js-dataTable-simple">
							<thead>
								<tr>
									<th class="text-center"><?php echo translate("Name"); ?> </th>
									<th class="text-center hidden-xs hidden-sm"><?php echo translate("Contents"); ?><br /><small class="text-muted"><?php echo translate("Today"); ?></small></th>
									<th class="text-center hidden-xs hidden-sm"><?php echo translate("Contents"); ?><br /><small class="text-muted"><?php echo translate("This Week"); ?></small></th>
									<th class="text-center"><?php echo translate("Contents"); ?><br /><small class="text-muted"><?php echo translate("This Month"); ?></small></th>
									<th class="text-center hidden-xs hidden-sm"><?php echo translate("Contents"); ?><br /><small class="text-muted"><?php echo translate("All"); ?></small></th>
									<th class="text-center"><?php echo translate("Last Added"); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i = 0; $i < count($users); $i++) { ?>
									<tr>
										<td class="font-w600"><?php echo $users[$i]["users_name"]; ?></td>
										<td class="hidden-xs hidden-sm">
											<?php
											if ($users[$i]["users_name"] == $users_today[$i]["users_name"]) {
												echo $users_today[$i]["all_content"];
											} else { echo "0"; }
											?>
										</td>
										<td class="hidden-xs hidden-sm">
											<?php
											if ($users[$i]["users_name"] == $users_week[$i]["users_name"]) {
												echo $users_week[$i]["all_content"];
											} else { echo "0"; }
											?>
										</td>
										<td>
											<?php
											if ($users[$i]["users_name"] == $users_month[$i]["users_name"]) {
												echo $users_month[$i]["all_content"];
											} else { echo "0"; }
											?>
										</td>
										<td class="hidden-xs hidden-sm"><?php echo $users[$i]["all_content"]; ?></td>
										<td><?php echo date("Y-m-d H:i:s", strtotime(datetime_convert($users[$i]["log_date_created"], $configs["datetimezone"]))); ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
<!-- END Pages -->

<?php
}
?>

<!-- Page JS Plugins -->
<script src="plugins/chartjs/Chart.min.js"></script>
<script src="plugins/flot/jquery.flot.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="plugins/jquery-jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>

<?php
if ($_SESSION["users_level"] != "4") {
?>

<!-- Page script -->
<script>
$( document ).ready(function() {
	var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

	if (isMobile) {
		$('.js-flot-lines').css("height","250px");
		$('.chart-pie').css("height","150px");
	} else {
		$('.js-flot-lines').css("height","495px");
		$('.chart-pie').css("height","300px");
	}

});

var BaseTableDatatables = function() {

	var initDataTableSimple = function() {
        jQuery('.js-dataTable-simple').dataTable({
            columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            oLanguage: {
                sLengthMenu: ""
            },
            dom:
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>"
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination pull-right remove-margin-t"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            initDataTableSimple();
        }
    };
}();

jQuery(function(){
	BaseTableDatatables.init();
});

	var BaseCompCharts = function() {

		var initChartsChartJS = function () {

			var $chartPieCon    = jQuery('.js-chartjs-pie')[0].getContext('2d');
			var $chartPie, $chartPolarPieDonutData;

			var $globalOptions = {
				scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
				scaleFontColor: '#999',
				scaleFontStyle: '600',
				tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
				tooltipCornerRadius: 3,
				maintainAspectRatio: false,
				responsive: true
			};

			var $chartPolarPieDonutData = [
				{
					value: <?php echo $mobile; ?>,
					color: 'rgba(250, 219, 125, 1)',
					highlight: 'rgba(250, 219, 125, .75)',
					label: '<?php echo translate("Mobile"); ?>'
				},
				{
					value: <?php echo $desktop; ?>,
					color: 'rgba(171, 227, 125, 1)',
					highlight: 'rgba(171, 227, 125, .75)',
					label: '<?php echo translate("Desktop"); ?>'
				}
			];

			$chartPie   = new Chart($chartPieCon).Pie($chartPolarPieDonutData, $globalOptions);

		};

		var initChartsFlot = function(){

			var $flotLines = jQuery('.js-flot-lines');

			<?php
			$year = date("Y")."-01-01";
			$jan = count_monthly_visit($year,"1");
			$feb = count_monthly_visit($year,"2");
			$mar = count_monthly_visit($year,"3");
			$apr = count_monthly_visit($year,"4");
			$may = count_monthly_visit($year,"5");
			$jun = count_monthly_visit($year,"6");
			$jul = count_monthly_visit($year,"7");
			$aug = count_monthly_visit($year,"8");
			$sep = count_monthly_visit($year,"9");
			$oct = count_monthly_visit($year,"10");
			$nov = count_monthly_visit($year,"11");
			$dec = count_monthly_visit($year,"12");
			?>

			var $dataEarnings = [
				[1, <?php echo $jan["count"]; ?>],
				[2, <?php echo $feb["count"]; ?>],
				[3, <?php echo $mar["count"]; ?>],
				[4, <?php echo $apr["count"]; ?>],
				[5, <?php echo $may["count"]; ?>],
				[6, <?php echo $jun["count"]; ?>],
				[7, <?php echo $jul["count"]; ?>],
				[8, <?php echo $aug["count"]; ?>],
				[9, <?php echo $sep["count"]; ?>],
				[10, <?php echo $oct["count"]; ?>],
				[11, <?php echo $nov["count"]; ?>],
				[12, <?php echo $dec["count"]; ?>]
			];

			var $dataMonths = [
				[1, 'Jan'],
				[2, 'Feb'],
				[3, 'Mar'],
				[4, 'Apr'],
				[5, 'May'],
				[6, 'Jun'],
				[7, 'Jul'],
				[8, 'Aug'],
				[9, 'Sep'],
				[10, 'Oct'],
				[11, 'Nov'],
				[12, 'Dec']
			];

			jQuery.plot($flotLines,
				[
					{
						label: '<?php echo translate("Total Visit"); ?>',
						data: $dataEarnings,
						lines: {
							show: true,
							fill: true,
							fillColor: {
								colors: [{opacity: .7}, {opacity: .7}]
							}
						},
						points: {
							show: true,
							radius: 6
						}
					},
					// {
					// 	label: 'Sales',
					// 	data: $dataSales,
					// 	lines: {
					// 		show: true,
					// 		fill: true,
					// 		fillColor: {
					// 			colors: [{opacity: .5}, {opacity: .5}]
					// 		}
					// 	},
					// 	points: {
					// 		show: true,
					// 		radius: 6
					// 	}
					// }
				],
				{
					colors: ['#abe37d', '#333333'],
					legend: {
						show: true,
						position: 'nw',
						backgroundOpacity: 0
					},
					grid: {
						borderWidth: 0,
						hoverable: true,
						clickable: true
					},
					yaxis: {
						tickColor: '#ffffff',
						ticks: 3
					},
					xaxis: {
						ticks: $dataMonths,
						tickColor: '#f5f5f5'
					}
				}
			);

			// Creating and attaching a tooltip to the classic chart
			var previousPoint = null, ttlabel = null;
			$flotLines.bind('plothover', function(event, pos, item) {
				if (item) {
					if (previousPoint !== item.dataIndex) {
						previousPoint = item.dataIndex;

						jQuery('.js-flot-tooltip').remove();
						var x = item.datapoint[0], y = item.datapoint[1];

						if (item.seriesIndex === 0) {
							ttlabel = '<strong>' + y + '</strong> <?php echo translate("views"); ?>';
						} else if (item.seriesIndex === 1) {
							ttlabel = '<strong>' + y + '</strong> <?php echo translate("sales"); ?>';
						} else {
							ttlabel = '<strong>' + y + '</strong> <?php echo translate("tickets"); ?>';
						}

						jQuery('<div class="js-flot-tooltip flot-tooltip">' + ttlabel + '</div>')
							.css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
					}
				}
				else {
					jQuery('.js-flot-tooltip').remove();
					previousPoint = null;
				}
			});
		};

		return {
			init: function () {
				initChartsChartJS();
				initChartsFlot();
			}
		};
	}();

	jQuery(function(){
		BaseCompCharts.init();
	});
</script>

<?php
}
?>

<?php
include("templates/".$configs["backend_template"]."/footer.php");
?>
