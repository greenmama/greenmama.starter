<?php 
require_once("system/include.php");

/* configurations: Pages */
$pages = get_modules_data_by_id("9");
if (isset($pages) && !empty($pages)) {
	$title = translate($pages["modules_name"]);
	$module_page = $pages["modules_link"];
	$module_page_link = backend_rewrite_url($pages["modules_link"]);
	$module_function_page = htmlspecialchars("system/".$pages["modules_function_link"]);
} else {
	$title = translate("Pages");
	$module_page = "pages.php";
	$module_page_link = backend_rewrite_url("pages.php");
	$module_function_page = htmlspecialchars("system/core/".$configs["version"]."/pages.php");
 }

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Pages */
if (!authentication_session_users()) {
	authentication_deny();
}
if (!authentication_session_modules($module_page)) {
	authentication_permission_deny();
}

if (isset($pages["modules_datatable_field"]) && !empty($pages["modules_datatable_field"])) {
	$table_module_field = array();
	for ($i = 0; $i < count($pages["modules_datatable_field"]); $i++) {
		if ($pages["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
			$pages_modules_datatable_field_display = true;
		} else {
			$pages_modules_datatable_field_display = false;
		}
		$table_module_field[$pages["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $pages_modules_datatable_field_display;
	}
} else {
	$table_module_field = array(
	"pages_id" => true,
	"pages_title" => true,
	"pages_link" => false,
	"pages_meta_title" => false,
	"pages_meta_description" => false,
	"pages_meta_keyword" => false,
	"pages_template" => true,
	"pages_date_created" => true,
	"users_id" => false,
	"users_username" => false,
	"users_name" => true,
	"users_last_name" => false,
	"pages_activate" => true,
	"modules_id" => false,
	"modules_name" => false,
	"languages_id" => false,
	"languages_short_name" => false,
	"languages_name" => false,
	"pages_actions" => true,
);
}

require_once("templates/".$configs["backend_template"]."/header.php");
require_once("templates/".$configs["backend_template"]."/overlay.php");
require_once("templates/".$configs["backend_template"]."/sidebar.php");
require_once("templates/".$configs["backend_template"]."/navbar.php");
require_once("templates/".$configs["backend_template"]."/container-header.php");
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header bg-mama-green">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo htmlspecialchars($module_function_page); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="pages">
					<input type="hidden" id="pages_id" name="pages_id">
					<input type="hidden" id="pages_action" name="pages_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="pages_title"><?php echo translate("Title"); ?><span class="text-danger">*</span> <span id="pages_title_stringlength" class="text-success pull-right push-10-l">0/240</span></label>
							<div class="col-md-12">
								<input class="form-control" type="text" id="pages_title" name="pages_title" value="<?php $pages_title_default_value[0]; ?>"placeholder="<?php echo translate("Title of page");?>">
								<div id="title-check-loading" class="progress-greenmama" style="display: none;"><div class="progress-bar-greenmama"></div></div>
								<div class="help-block"><?php echo "<strong>" . translate("Title")."</strong> ".translate("cannot be longer than maximum length of strings 240"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="pages_link"><?php echo translate("Link"); ?><span class="text-danger">*</span> <span id="pages_link_stringlength" class="text-success pull-right push-10-l">0/200</span></label>
							<div class="col-md-12">
								<div class="input-group">
								<span class="input-group-addon" title="<?php echo $configs["website_url"]; ?>">
								<?php echo substr_utf8(str_replace(array("http://", "https://", "www."), "", $configs["base_url"]), 0, 15); ?>/
								</span>
								<input class="form-control" type="text" id="pages_link" name="pages_link" value="<?php $pages_link_default_value[0]; ?>"placeholder="<?php echo translate("Link (URL)");?>">
								<span class="input-group-addon">
								<?php 
								if ($configs["url_rewritting"]) {
								?>
								/
								<?php 
								} else {
								?>
								.html
								<?php 
								}
								?>
								</span>
								</div>
								<div id="link-check-loading" class="progress-greenmama" style="display: none;"><div class="progress-bar-greenmama"></div></div>
								<div class="help-block"><?php echo "<strong>" . translate("Link")."</strong> ".translate("does not allow special characters and white spaces excluding ., -, _ and can not be duplicated"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-graph push-5-r"></i> <?php echo translate("Search Engine Optimization (SEO)"); ?></h3></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="pages_meta_title"><?php echo translate("Meta Title"); ?> </label>
												<div class="col-md-12">
													<input class="form-control" type="text" id="pages_meta_title" name="pages_meta_title" value="<?php $pages_meta_title_default_value[0]; ?>"placeholder="<?php echo translate("Meta title for SEO");?>">
													<div id="meta_title-check-loading" class="progress-greenmama" style="display: none;"><div class="progress-bar-greenmama"></div></div>
													<div class="help-block"><?php echo translate("Let search engine see title of the page"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="pages_meta_description"><?php echo translate("Meta Description"); ?></label>
												<div class="col-md-12">
													<textarea class="form-control" type="text" id="pages_meta_description" name="pages_meta_description" placeholder="<?php echo translate("Meta description for SEO");?>"><?php $pages_meta_description_default_value[0]; ?></textarea>
													<div id="meta_description-check-loading" class="progress-greenmama" style="display: none;"><div class="progress-bar-greenmama"></div></div>
													<div class="help-block"><?php echo translate("Let search engine see description of the page (Best when have at least 100 characters)"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="pages_meta_keyword"><?php echo translate("Meta Keywords"); ?> </label>
												<div class="col-md-12">
													<input class="form-control" type="text" id="pages_meta_keyword" name="pages_meta_keyword" value="<?php $pages_meta_keyword_default_value[0]; ?>"placeholder="<?php echo translate("Meta keyword for SEO");?>" >
													<div class="help-block"><?php echo translate("Let search engine know keywords of the page (Separate by comma , )"); ?></div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="pages_template"><?php echo translate("Template"); ?></label>
							<div class="col-md-5">
								<input class="form-control" type="text" id="pages_template" name="pages_template" value="<?php $pages_template_default_value[0]; ?>"placeholder="<?php echo translate("Template of page");?>">
								
								<div class="help-block"><?php echo translate("Selected template file will be activated to page"); ?></div>
							</div>
							<div class="col-md-5">
								<a href="plugins/filemanager/dialog-template.php?type=2&amp;field_id=pages_template&amp;relative_url=1" id="pages_template-fileselector-btn" class="btn" type="button" style="padding: 0">
								<i class="si si-folder-alt fa-2x"></i> <?php echo translate("Select"); ?>
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-grid push-5-r"></i> <?php echo translate("Modules"); ?> <a href="modules.php" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>"><i class="si si-settings push-5-l"></i></a></h3></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-12" for="modules_id"><?php echo translate("Module"); ?></label>
												<div class="col-md-12">
													<select class="form-control" type="text" id="modules_id" name="modules_id" disabled>
														<option value=""><?php echo translate("Automatically link by system"); ?></option>
														<option value="" disabled>-</option>
														<?php
														$modules = get_modules_data_all();
														for($i=0;$i<count($modules);$i++){
															?>
														<option value="<?php echo $modules[$i]["modules_id"];?>" data-key="<?php echo $modules[$i]["modules_key"];?>" data-name="<?php echo $modules[$i]["modules_name"];?>"><?php echo $modules[$i]["modules_name"];?> (ID: <?php echo $modules[$i]["modules_id"];?>)</option>
															<?php
														}
														?>
													</select>
													<div class="help-block"><?php echo translate("*You cannot link module from here");?></div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-12" for="modules_record_target"><?php echo translate("Module Content"); ?></label>
												<div class="col-md-12">
													<input type="hidden" id="modules_record_target_temp" name="modules_record_target_temp" value="" disabled />
													<select class="form-control" type="text" id="modules_record_target" name="modules_record_target" disabled>
													</select>
													<div class="help-block"><?php echo translate("*You cannot link module content from here");?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list"><i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span></a>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content");?></div>
                                        </div>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
	
					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<?php
							$count_languages_short_name = count_languages_data_all();
							if ($count_languages_short_name > 1) {
							?>
							<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="languages_short_name" style="margin-top: 7px;"><?php echo translate("Language"); ?></label>
									<div class="col-md-9">
										<select class="form-control" type="text" id="languages_short_name" name="languages_short_name">
											<?php
											if ($count_languages_short_name <= $configs["datatable_data_limit"]) {
												$languages = get_languages_data_all();
												for($i=0;$i<count($languages);$i++){
													if ($configs["language"] != $languages[$i]["languages_short_name"]) {
													?>
														<option value="<?php echo $languages[$i]["languages_short_name"];?>" data-id="<?php echo $languages[$i]["languages_id"];?>"  data-name="<?php echo $languages[$i]["languages_name"];?>"><?php echo $languages[$i]["languages_name"];?></option>
													<?php
													} else {
													?>
														<option value="<?php echo $languages[$i]["languages_short_name"];?>" data-id="<?php echo $languages[$i]["languages_id"];?>"  data-name="<?php echo $languages[$i]["languages_name"];?>" selected><?php echo $languages[$i]["languages_name"];?></option>
													<?php
													}
												}
											} else {
												?>
												<option label="default" value=""><?php echo translate("Loading");?>...</option>
												<?php
											}
											?>
										</select>
										<div class="help-block"><?php echo translate("Language of content");?></div>
									</div>
								</div>
							</div>
							<?php
							} else {
							?>
							<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-12" style="margin-top: 7px;">
									<a href="languages.php" target="_blank" title="" data-toggle="tooltip" data-original-title="Manage Languages"><i class="si si-speech" style="width: 16px;"></i> <?php echo translate("Add more Languages to enable translation"); ?></a></label>
									<?php
									$languages = get_languages_data_all();
									?>
									<input class="form-control" type="hidden" id="languages_short_name" name="languages_short_name" value="<?php echo $languages[0]["languages_short_name"];?>" data-id="<?php echo $languages[0]["languages_id"];?>"  data-name="<?php echo $languages[0]["languages_name"];?>">
								</div>
							</div>
							<?php
							}
							?>
							<?php 
							if ($count_languages_short_name > 1) {
							$count_pages_translate = count_pages_data_all();
							?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="pages_translate" style="margin-top: 7px;"><?php echo translate("Translate"); ?></label>
									<div class="col-md-9">
										<select class="form-control js-select2" type="text" id="pages_translate" name="pages_translate">
											<option label="default" value=""><?php echo translate("Original language"); ?></option>
											<option label="seperator" value="">-</option>
											<?php
											$count_pages_translate = count_pages_data_all();
											if ($count_pages_translate <= $configs["datatable_data_limit"]) {
												$pages_translate = get_pages_data_all();
												for($i=0;$i<count($pages_translate);$i++){
												?>
													<option value="<?php echo $pages_translate[$i][0];?>"><?php echo $pages_translate[$i][1];?> (ID: <?php echo $pages_translate[$i][0];?>, <?php echo $pages_translate[$i]["languages_short_name"];?>)</option>
												<?php
												}
											} else {
												?>
												<option label="default" value=""><?php echo translate("Loading");?>...</option>
												<?php
											}
											?>
										</select>
										<div class="help-block"><?php echo translate("Source language content");?></div>
									</div>
								</div>
							</div>
							<?php
							}
							?>
							<div class="col-lg-2 col-md-3 col-sm-9 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="pages_activate" name="pages_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full">
			</div>
		</div>
	</div>
</div>

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<li>
                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("Pages"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
				<div id="filter_parent" class="panel-group">
					<div class="panel panel-default">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#filter_parent" href="#filter">
							<div class="panel-heading">
								<span class="panel-title"><?php echo translate("Advanced Filter"); ?></span> <i class="fa fa-angle-down"></i>
							</div>
						</a>
						<div id="filter" class="panel-collapse collapse">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<form action="" method="get">
											<input type="hidden" value="<?php if (isset($_GET["search"]) && !empty($_GET["search"])) { echo $_GET["search"]; } ?>">
											<div class="row items-push">
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-4 push-5-t" for="published"><?php echo translate("Status"); ?></label>
														<div class="col-md-8">
															<?php
															if (isset($_GET["published"])) {
																if ($_GET["published"] == "") {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "1") {
																	$status_all_selected = "";
																	$status_published_selected = " selected";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "0") {
																	$status_all_selected = "";
																	$status_published_selected = "";
																	$status_unpublished_selected = " selected";
																} else {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																}
															} else {
																$status_all_selected = " selected";
																$status_published_selected = "";
																$status_unpublished_selected = "";
															}
															?>
															<select name="published" aria-controls="datatable" class="form-control">
																<option value="" <?php echo $status_all_selected; ?>><?php echo translate("All");?></option>
																<option value="" disabled>-</option>
																<option value="1" <?php echo $status_published_selected; ?>><?php echo translate("Published");?></option>
																<option value="0" <?php echo $status_unpublished_selected; ?>><?php echo translate("Unpublished");?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="from"><?php echo translate("From"); ?></label>
														<div class="col-md-9">
															<input class="form-control from" type="text" data-role="date" id="from" name="from" value="<?php if (isset($_GET["from"]) && !empty($_GET["from"])) { echo $_GET["from"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="to"><?php echo translate("To"); ?></label>
														<div class="col-md-9">
															<input class="form-control to" type="text" data-role="date" id="to" name="to" value="<?php if (isset($_GET["to"]) && !empty($_GET["to"])) { echo $_GET["to"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3 text-right">
													<div class="form-group">
														<div class="col-xs-6">
															<button id="button-filter-submit" class="btn btn-sm btn-primary" type="submit" style="width: 100%;"><?php echo translate("Show"); ?></button>
														</div>
														<div class="col-xs-6">
															<button id="button-filter-reset" class="btn btn-sm btn-default" type="reset" onclick="location.href = '<?php echo $module_page_link; ?>'" style="width: 100%;"><?php echo translate("Reset"); ?></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            	<?php
            	$data_table_info = prepare_pages_table_defer($table_module_field);
                echo create_pages_table($data_table_info["values"], $table_module_field);
            	?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->

<?php
include("templates/".$configs["backend_template"]."/container-footer.php");
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->
	
<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php 
if (file_exists("../../../plugins/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
	echo "<script src=\"plugins/bootstrap-datepicker/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js\"></script>";
}
?>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo htmlspecialchars($module_function_page); ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;
</script>

<script>

function reset_pages_data() {

	$("#pages_id").val("");
	$("#pages_title").val("");
	$("#pages_link").val("");
	$("#pages_meta_title").val("");
	$("#pages_meta_description").val("");
	$("#pages_meta_keyword").tagsInputDestroy();
	$("#pages_meta_keyword").val("");
	$("#pages_meta_keyword").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "+",
		removeWithBackspace: true,
		delimiter: [","]
	});
	$("#pages_template").val("");
	$("#modules_record_target").empty();
	$("#modules_record_target").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
	$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_all"
		},
		success: function(response) {
			$("#modules_id").empty();
			$("#modules_id").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
			$("#modules_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_id").append("<option value=\"" + response.values[i]["modules_id"] + "\" data-key=\"" + response.values[i]["modules_key"] + "\" data-name=\"" + response.values[i]["modules_name"] + "\">" + response.values[i]["modules_name"] + " (ID: " + response.values[i]["modules_id"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#modules_id").val($("#modules_id option:first").val()).trigger("change");
					}
				}
			}
		}
	});
	<?php
	if ($count_languages_short_name > 1) {
	?>
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/languages.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_languages_data_all"
		},
		success: function(response) {
			var languages_short_name = $("#languages_short_name").val();
			$("#languages_short_name").empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#languages_short_name").append("<option value=\"" + response.values[i]["languages_short_name"] + "\" data-id=\"" + response.values[i]["languages_id"] + "\" data-name=\"" + response.values[i]["languages_name"] + "\">" + response.values[i]["languages_name"] + "</option>");
					if (response.values.length - i == 1) {
						$("#languages_short_name").select2("val", "<?php echo strtoupper($configs["language"]); ?>");
					}
				}
			}
		}
	});
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_main_language_data_all"
		},
		success: function(response) {
			$("#pages_translate").empty();
			$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
			$("#pages_translate").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#pages_translate").append("<option value=\"" + response.values[i][0] + "\">" + response.values[i][1] + " (ID:" + response.values[i]["pages_id"] + ", " + response.values[i]["languages_short_name"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#pages_translate").val($("#pages_translate option:first").val()).trigger("change");
					}
				}
			}
		}
	});
	<?php
	}
	?>
	$("#pages_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#pages_action").val("");

}

function update_pages_data(target_id) {
	$("#modules_record_target").empty();
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_all"
		},
		success: function(response) {
			var modules_id = $("#modules_id").val();
			$("#modules_id").empty();
			$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
			$("#modules_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_id").append("<option value=\"" + response.values[i]["modules_id"] + "\" data-key=\"" + response.values[i]["modules_key"] + "\" data-name=\"" + response.values[i]["modules_name"] + "\">" + response.values[i]["modules_name"] + " (ID: " + response.values[i]["modules_id"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#modules_id").select2("val", modules_id);
					}
				}
			}
		}
	});
	<?php
	if ($count_languages_short_name > 1) {
	?>
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/languages.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_languages_data_all"
		},
		success: function(response) {
			var languages_short_name = $("#languages_short_name").val();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#languages_short_name").append("<option value=\"" + response.values[i]["languages_short_name"] + "\" data-id=\"" + response.values[i]["languages_id"] + "\" data-name=\"" + response.values[i]["languages_name"] + "\">" + response.values[i]["languages_name"] + "</option>");
					if (response.values.length - i == 1) {
						$("#languages_short_name").select2("val", languages_short_name);
					}
				}
			}
		}
	});
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_main_language_data_all"
		},
		success: function(response) {
			var pages_translate = $("#pages_translate").val();
			$("#pages_translate").empty();
			$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
			$("#pages_translate").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#pages_translate").append("<option value=\"" + response.values[i][0] + "\">" + response.values[i][1] + " (ID:" + response.values[i]["pages_id"] + ", " + response.values[i]["languages_short_name"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#pages_translate").select2("val", pages_translate);
					}
				}
			}
		}
	});
	<?php
	}
	?>
}

function submit_pages_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);

    if ($("#pages_id").val() == "") {
		var method = "create_pages_data";
	} else {
		var method = "update_pages_data";
	}

	if ($("#pages_activate").is(":checked") === true){
		var pages_activate = "1";
	} else {
		var pages_activate = "0";
	}
	var modules_id = $("#modules_id").val();
	var modules_name = $("#modules_id").find("option:selected", this).attr("data-name");
	var modules_record_target = $("#modules_record_target").val();
	if ($("#languages_short_name").val() == null || $("#languages_short_name").val() == "") {
		var languages_short_name = "<?php echo $configs["language"]; ?>";
		var languages_id = "";
		var languages_name = "";
	} else {
		var languages_short_name = $("#languages_short_name").val();
		var languages_id = $("#languages_short_name").find("option:selected", this).attr("data-id");
		var languages_name = $("#languages_short_name").find("option:selected", this).attr("data-name");
	}

	if ($("#pages_translate").val() == null || $("#pages_translate").val() == "") {
		var pages_translate = "0";
	} else {
		var pages_translate = $("#pages_translate").val();
	}
	
	
	
	
	
	

	var form_values   = {
		"pages_id": $("#pages_id").val(),
		"pages_action": $("#pages_action").val(),
		"pages_title": $("#pages_title").val(),
		"pages_link": $("#pages_link").val(),
		"pages_meta_title": $("#pages_meta_title").val(),
		"pages_meta_description": $("#pages_meta_description").val().trim().replace(/[\n\r]/g, "<br />"),
		"pages_meta_keyword": $("#pages_meta_keyword").val(),
		"pages_template": $("#pages_template").val(),
		"modules_id": modules_id,
		"modules_name": modules_name,
		"modules_record_target": modules_record_target,
		"pages_translate": pages_translate,

		"users_id": $("#users_id").val(),
		"users_username": $("#users_username").val(),
		"users_name": $("#users_name").val(),
		"users_last_name": $("#users_last_name").val(),
		"pages_activate": pages_activate,
		"languages_id": languages_id,
		"languages_short_name": languages_short_name,
		"languages_name": languages_name,
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				if (method == "create_pages_data") {
					create_pages_table_row(response.values);
					show_pages_modal_response(response.status, response.message);
					reset_pages_data();
					$("#form-content").slideUp("fast");
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");

					var limit = $("#datatable_length_selector").val();
					var page = get_url_param().page;
					if (page == null) {
						page = 1;
					}
					var search_text = $("#datatable_search").val();
					var published = get_url_param().published;
					var from = get_url_param().from;
					var to = get_url_param().to;
					if (limit != null) {
						if (search_text != "") {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
							}
						}
					} else {
						if (search_text != "") {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>");
							}
						}
					}
					
            		$("#button-data-submit").prop("disabled", false);
				} else if (method == "update_pages_data") {
					update_pages_table_row(response.values, pages_translate);
				    show_pages_modal_response(response.status, response.message);
					update_pages_data($("#pages_id").val());
					edit_pages_open($("#pages_id").val());
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
            		$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
            	$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function delete_pages_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_pages_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				delete_pages_table_row(target_id, response.values["pages_transform_id"]);
				if ($("#pages_id").val() == target_id) {
					reset_pages_data();
					form_pages_close();
				}
                show_pages_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function show_module_pages_content_list(target_value) {
	
	var modules_record_target = $("#modules_record_target_temp").val();
	$("#modules_record_target").empty();
	$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("Loading"); ?>...</option>");
	
	var form_values   = {
		"modules_id": target_value,
		"modules_record_target": modules_record_target
	}

	$.ajax({
		type: "POST",
		url: "system/core/<?php echo $configs["version"]; ?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "html",
		data: {
			method: "get_modules_content_data_all_by_modules_datatable_field_name",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == true){
				$("#modules_record_target").empty();
				$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("Please select a content"); ?></option>");
				$("#modules_record_target").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
				if (response.values != null) {
					if (modules_record_target == null || modules_record_target == "") {
						for(var i = 0; i < response.values.length; i++) {
							$("#modules_record_target").append("<option value=\"" + response.values[i][0] + "\">" + response.values[i][1] + " (ID: " + response.values[i][0] + ")</option>");
						}
						$("#modules_record_target").select2("val", "");
					} else {
						$("#modules_record_target").append("<option value=\"" + response.values[0] + "\">" + response.values[1] + " (ID: " + response.values[0] + ")</option>");
						$("#modules_record_target").select2("val", modules_record_target);
					}
				} else {
					$("#modules_record_target").empty();
					$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("No content found"); ?></option>");
					$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
				}
			} else {
				$("#modules_record_target").empty();
				$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("No content found"); ?></option>");
				$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#modules_record_target").empty();
			$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("No content found"); ?></option>");
			$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
		}
	});

}

function form_pages_close() {
	
	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "") {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "") {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}
	
}
function create_pages_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add")." ".$title; ?>");

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

    var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "") {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "") {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_pages_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#pages_action").val("create");

}

function edit_pages_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_pages_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#pages_id").val(response.values["pages_id"]);
				$("#pages_title").val(response.values["pages_title"]);
				$("#pages_title_stringlength").empty().append($("#pages_title").val().length + "/240");
				$("#pages_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_title").val().length < 240) {
					$("#pages_title_stringlength").addClass("text-success");
				} else {
					$("#pages_title_stringlength").addClass("text-danger");
				}
				if (response.values["pages_link"] != "") {
					$("#pages_link").val(response.values["pages_link"].replace(".php", ""));
				}
				$("#pages_link_stringlength").empty().append($("#pages_link").val().length + "/200");
				$("#pages_link_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_link").val().length < 240) {
					$("#pages_link_stringlength").addClass("text-success");
				} else {
					$("#pages_link_stringlength").addClass("text-danger");
				}
				$("#pages_meta_title").val(response.values["pages_meta_title"]);
				$("#pages_meta_title_stringlength").empty().append($("#pages_meta_title").val().length + "/240");
				$("#pages_meta_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_meta_title").val().length < 240) {
					$("#pages_meta_title_stringlength").addClass("text-success");
				} else {
					$("#pages_meta_title_stringlength").addClass("text-danger");
				}
				$("#pages_meta_description").val(response.values["pages_meta_description"]);
				$("#pages_meta_keyword").tagsInputDestroy();
				$("#pages_meta_keyword").val(response.values["pages_meta_keyword"]);
				$("#pages_meta_keyword").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "+",
					removeWithBackspace: true,
					delimiter: [","]
				});
				$("#pages_template").val(response.values["pages_template"]);
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
								if (response_modules.values.length - i == 1) {
									if (response.values["modules_id"] != "") {
										$("#modules_id").select2("val", response.values["modules_id"]);
									} else {
										$("#modules_id").empty();
										$("#modules_id").append("<option value=\"\"><?php echo translate("Not linked"); ?></option>");
										$("#modules_id").select2("val", "");
									}
								}
							}
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				<?php
				if ($count_languages_short_name > 1) {
				?>
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/languages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_languages_data_all",
						parameters: target_id
					},
					success: function(response_languages) {
						$("#languages_short_name").empty();
						if (response_languages.values != null) {
							for(var i = 0; i < response_languages.values.length; i++) {
								$("#languages_short_name").append("<option value=\"" + response_languages.values[i]["languages_short_name"] + "\" data-id=\"" + response_languages.values[i]["languages_id"] + "\" data-name=\"" + response_languages.values[i]["languages_name"] + "\">" + response_languages.values[i]["languages_name"] + "</option>");
								if (response_languages.values.length - i == 1) {
									if (response.values["languages_short_name"] != "") {
										$("#languages_short_name").select2("val", response.values["languages_short_name"]);
									} else {
										$("#languages_short_name").select2("val", "");
									}
								}
							}
						}
					}
				});
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all_excluding",
						parameters: target_id
					},
					success: function(response_translate) {
						$("#pages_translate").empty();
						$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
						$("#pages_translate").append("<option value=\"\" disabled>-</option>");
						if (response_translate.values != null) {
							for(var i = 0; i < response_translate.values.length; i++) {
								$("#pages_translate").append("<option value=\"" + response_translate.values[i][0] + "\">" + response_translate.values[i][1] + " (ID:" + response_translate.values[i]["pages_id"] + ", " + response_translate.values[i]["languages_short_name"] + ")</option>");
								if (response_translate.values.length - i == 1) {
									if (response.values["pages_translate"] != "0") {
										$("#pages_translate").select2("val", response.values["pages_translate"]);
									} else {
										$("#pages_translate").select2("val", "");
									}
								}
							}
						}
					}
				});
				<?php
				}
				?>
				if(response.values["pages_activate"] == "1" ){
					$("#pages_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#pages_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["pages_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#pages_action").val("edit");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "") {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&pages_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&pages_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&pages_id=" + target_id);
						}
					}
				} else {
					if (search_text != "") {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&pages_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&pages_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&pages_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {
					
					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_pages_modal_prompt($(this).attr("data-content"), "revision_pages_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function copy_pages_open(target_id) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	reset_pages_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy")." ".$title; ?>");

				$("#pages_id").val("");
				$("#pages_title").val(response.values["pages_title"]);
				$("#pages_title_stringlength").empty().append($("#pages_title").val().length + "/240");
				$("#pages_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_title").val().length < 240) {
					$("#pages_title_stringlength").addClass("text-success");
				} else {
					$("#pages_title_stringlength").addClass("text-danger");
				}
				check_pages_title(response.values["pages_title"]);
				if (response.values["pages_link"] != "") {
					$("#pages_link").val(response.values["pages_link"].replace(".php", ""));
				}
				$("#pages_link_stringlength").empty().append($("#pages_link").val().length + "/200");
				$("#pages_link_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_link").val().length < 240) {
					$("#pages_link_stringlength").addClass("text-success");
				} else {
					$("#pages_link_stringlength").addClass("text-danger");
				}
				check_pages_link(response.values["pages_link"]);
				$("#pages_meta_title").val(response.values["pages_meta_title"]);
				$("#pages_meta_title_stringlength").empty().append($("#pages_meta_title").val().length + "/240");
				$("#pages_meta_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_meta_title").val().length < 240) {
					$("#pages_meta_title_stringlength").addClass("text-success");
				} else {
					$("#pages_meta_title_stringlength").addClass("text-danger");
				}
				check_pages_meta_title(response.values["pages_meta_title"]);
				$("#pages_meta_description").val(response.values["pages_meta_description"]);
				check_pages_meta_description(response.values["pages_meta_description"]);
				$("#pages_meta_keyword").tagsInputDestroy();
				$("#pages_meta_keyword").val(response.values["pages_meta_keyword"]);
				$("#pages_meta_keyword").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "+",
					removeWithBackspace: true,
					delimiter: [","]
				});
				$("#pages_template").val(response.values["pages_template"]);
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
								if (response_modules.values.length - i == 1) {
									if (response.values["modules_id"] != "") {
										$("#modules_id").select2("val", response.values["modules_id"]);
									} else {
										$("#modules_id").select2("val", "");
									}
								}
							}
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				<?php
				if ($count_languages_short_name > 1) {
				?>
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/languages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_languages_data_all",
						parameters: target_id
					},
					success: function(response_languages) {
						$("#languages_short_name").empty();
						if (response_languages.values != null) {
							for(var i = 0; i < response_languages.values.length; i++) {
								$("#languages_short_name").append("<option value=\"" + response_languages.values[i]["languages_short_name"] + "\" data-id=\"" + response_languages.values[i]["languages_id"] + "\" data-name=\"" + response_languages.values[i]["languages_name"] + "\">" + response_languages.values[i]["languages_name"] + "</option>");
								if (response_languages.values.length - i == 1) {
									if (response.values["languages_short_name"] != "") {
										$("#languages_short_name").select2("val", response.values["languages_short_name"]);
									} else {
										$("#languages_short_name").select2("val", "");
									}
								}
							}
						}
					}
				});
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all_excluding",
						parameters: target_id
					},
					success: function(response_translate) {
						$("#pages_translate").empty();
						$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
						$("#pages_translate").append("<option value=\"\" disabled>-</option>");
						if (response_translate.values != null) {
							for(var i = 0; i < response_translate.values.length; i++) {
								$("#pages_translate").append("<option value=\"" + response_translate.values[i][0] + "\">" + response_translate.values[i][1] + " (ID:" + response_translate.values[i]["pages_id"] + ", " + response_translate.values[i]["languages_short_name"] + ")</option>");
								if (response_translate.values.length - i == 1) {
									if (response.values["pages_translate"] != "0") {
										$("#pages_translate").select2("val", response.values["pages_translate"]);
									} else {
										$("#pages_translate").select2("val", "");
									}
								}
							}
						}
					}
				});
				<?php
				}
				?>
				if(response.values["pages_activate"] == "1" ){
					$("#pages_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#pages_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#pages_action").val("copy");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "") {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&pages_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&pages_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&pages_id=" + target_id);
						}
					}
				} else {
					if (search_text != "") {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&pages_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&pages_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&pages_id=" + target_id);
						}
					}
				}
			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function translate_pages_open(target_id, target_language) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	
	$("#form-footer").empty();

	reset_pages_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Translate")." ".$title; ?>");

				$("#pages_id").val("")
				$("#pages_title").val(response.values["pages_title"]);
				$("#pages_title_stringlength").empty().append($("#pages_title").val().length + "/240");
				$("#pages_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_title").val().length < 240) {
					$("#pages_title_stringlength").addClass("text-success");
				} else {
					$("#pages_title_stringlength").addClass("text-danger");
				}
				check_pages_title(response.values["pages_title"]);
				if (response.values["pages_link"] != "") {
					$("#pages_link").val(response.values["pages_link"].replace(".php", ""));
				}
				$("#pages_link_stringlength").empty().append($("#pages_link").val().length + "/200");
				$("#pages_link_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_link").val().length < 240) {
					$("#pages_link_stringlength").addClass("text-success");
				} else {
					$("#pages_link_stringlength").addClass("text-danger");
				}
				check_pages_link(response.values["pages_link"]);
				$("#pages_meta_title").val(response.values["pages_meta_title"]);
				$("#pages_meta_title_stringlength").empty().append($("#pages_meta_title").val().length + "/240");
				$("#pages_meta_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_meta_title").val().length < 240) {
					$("#pages_meta_title_stringlength").addClass("text-success");
				} else {
					$("#pages_meta_title_stringlength").addClass("text-danger");
				}
				check_pages_meta_title(response.values["pages_meta_title"]);
				$("#pages_meta_description").val(response.values["pages_meta_description"]);
				check_pages_meta_description(response.values["pages_meta_description"]);
				$("#pages_meta_keyword").tagsInputDestroy();
				$("#pages_meta_keyword").val(response.values["pages_meta_keyword"]);
				$("#pages_meta_keyword").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "+",
					removeWithBackspace: true,
					delimiter: [","]
				});
				$("#pages_template").val(response.values["pages_template"]);
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
								if (response_modules.values.length - i == 1) {
									if (response.values["modules_id"] != "") {
										$("#modules_id").select2("val", response.values["modules_id"]);
									} else {
										$("#modules_id").select2("val", "");
									}
								}
							}
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				<?php
				if ($count_languages_short_name > 1) {
				?>
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/languages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_languages_data_all"
					},
					success: function(response_languages) {
						$("#languages_short_name").empty();
						if (response_languages.values != null) {
							for(var i = 0; i < response_languages.values.length; i++) {
								$("#languages_short_name").append("<option value=\"" + response_languages.values[i]["languages_short_name"] + "\" data-id=\"" + response_languages.values[i]["languages_id"] + "\" data-name=\"" + response_languages.values[i]["languages_name"] + "\">" + response_languages.values[i]["languages_name"] + "</option>");
								if (response_languages.values.length - i == 1) {
									if (target_language != "") {
										$("#languages_short_name").select2("val", target_language);
									} else {
										$("#languages_short_name").select2("val", "");
									}
								}
							}
						}
					}
				});
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all"
					},
					success: function(response_translate) {
						$("#pages_translate").empty();
						$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
						$("#pages_translate").append("<option value=\"\" disabled>-</option>");
						if (response_translate.values != null) {
							for(var i = 0; i < response_translate.values.length; i++) {
								$("#pages_translate").append("<option value=\"" + response_translate.values[i][0] + "\">" + response_translate.values[i][1] + " (ID:" + response_translate.values[i]["pages_id"] + ", " + response_translate.values[i]["languages_short_name"] + ")</option>");
								if (response_translate.values.length - i == 1) {
									if (target_id != "0") {
										$("#pages_translate").select2("val", target_id);
									} else {
										$("#pages_translate").select2("val", "");
									}
								}
							}
						}
					}
				});
				<?php
				}
				?>
				if(response.values["pages_activate"] == "1" ){
					$("#pages_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#pages_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#pages_action").val("translate");
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=translate&pages_id=" + target_id + "&pages_language=" + target_language);
			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function view_pages_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_pages_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_pages_modal_response(response.status, response.html);
				
				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_pages_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_pages_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#pages_id").val(response.values["pages_id"]);
				$("#pages_title").val(response.values["pages_title"]);
				$("#pages_title_stringlength").empty().append($("#pages_title").val().length + "/240");
				$("#pages_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_title").val().length < 240) {
					$("#pages_title_stringlength").addClass("text-success");
				} else {
					$("#pages_title_stringlength").addClass("text-danger");
				}
				if (response.values["pages_link"] != "") {
					$("#pages_link").val(response.values["pages_link"].replace(".php", ""));
				}
				$("#pages_link_stringlength").empty().append($("#pages_link").val().length + "/200");
				$("#pages_link_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_link").val().length < 240) {
					$("#pages_link_stringlength").addClass("text-success");
				} else {
					$("#pages_link_stringlength").addClass("text-danger");
				}
				$("#pages_meta_title").val(response.values["pages_meta_title"]);
				$("#pages_meta_title_stringlength").empty().append($("#pages_meta_title").val().length + "/240");
				$("#pages_meta_title_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#pages_meta_title").val().length < 240) {
					$("#pages_meta_title_stringlength").addClass("text-success");
				} else {
					$("#pages_meta_title_stringlength").addClass("text-danger");
				}
				$("#pages_meta_description").val(response.values["pages_meta_description"]);
				$("#pages_meta_keyword").tagsInputDestroy();
				$("#pages_meta_keyword").val(response.values["pages_meta_keyword"]);
				$("#pages_meta_keyword").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "+",
					removeWithBackspace: true,
					delimiter: [","]
				});
				$("#pages_template").val(response.values["pages_template"]);
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
							}
						}
						if (response.values["modules_id"] != "") {
							$("#modules_id").select2("val", response.values["modules_id"]);
						} else {
							$("#modules_id").select2("val", "");
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				<?php
				if ($count_languages_short_name > 1) {
				?>
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/languages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_language_data_all",
						parameters: target_id
					},
					success: function(response) {
						$("#languages_short_name").empty();
						if (response.values != null) {
							for(var i = 0; i < response.values.length; i++) {
								$("#languages_short_name").append("<option value=\"" + response.values[i]["languages_short_name"] + "\" data-id=\"" + response.values[i]["languages_id"] + "\" data-name=\"" + response.values[i]["languages_name"] + "\">" + response.values[i]["languages_name"] + "</option>");
							}
						}
						if (response.values["languages_short_name"] != "") {
							$("#languages_short_name").select2("val", response.values["languages_short_name"]);
						} else {
							$("#languages_short_name").select2("val", "");
						}
					}
				});
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all_excluding",
						parameters: target_id
					},
					success: function(response_translate) {
						$("#pages_translate").empty();
						$("#pages_translate").append("<option value=\"\"><?php echo translate("Original language"); ?></option>");
						$("#pages_translate").append("<option value=\"\" disabled>-</option>");
						if (response_translate.values != null) {
							for(var i = 0; i < response_translate.values.length; i++) {
								$("#pages_translate").append("<option value=\"" + response_translate.values[i][0] + "\">" + response_translate.values[i][1] + " (ID:" + response_translate.values[i]["pages_id"] + ", " + response_translate.values[i]["languages_short_name"] + ")</option>");
							}
						}
						if (response.values["pages_translate"] != "0") {
							$("#pages_translate").select2("val", response.values["pages_translate"]);
						} else {
							$("#pages_translate").select2("val", "");
						}
					}
				});
				<?php
				}
				?>
				if(response.values["pages_activate"] == "1" ){
					$("#pages_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#pages_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["pages_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_pages_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_pages_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}
	
function title_pages_change_detect(target_value, e) {

	$("#title-check-loading").fadeIn("fast");

	if(e.keyCode === 13){
		check_pages_title(target_value);
	} else if (e.keyCode === 8 || e.keyCode === 46) {
		clearTimeout(title_stay_value);
		var title_stay_value = setTimeout(function(){ check_pages_title(target_value, $("#pages_id").val()); }, 1500);
		generate_pages_link();
	} else {
		clearTimeout(title_stay_value);
		var title_stay_value = setTimeout(function(){ check_pages_title(target_value, $("#pages_id").val()); }, 1500);
		generate_pages_link();
	}

}
	
function link_pages_change_detect(target_value, e) {

	$("#link-check-loading").fadeIn("fast");

	if(e.keyCode === 13){
		check_pages_link(target_value);
	} else if (e.keyCode === 8 || e.keyCode === 46) {
		clearTimeout(link_stay_value);
		var link_stay_value = setTimeout(function(){ check_pages_link(target_value, $("#pages_id").val()); }, 1500);
	} else {
		clearTimeout(link_stay_value);
		var link_stay_value = setTimeout(function(){ check_pages_link(target_value, $("#pages_id").val()); }, 1500);
	}

}
	
function meta_title_pages_change_detect(target_value, e) {

	$("#meta_title-check-loading").fadeIn("fast");

	if(e.keyCode === 13){
		check_pages_meta_title(target_value);
	} else if (e.keyCode === 8 || e.keyCode === 46) {
		clearTimeout(meta_title_stay_value);
		var meta_title_stay_value = setTimeout(function(){ check_pages_meta_title(target_value, $("#pages_id").val()); }, 1500);
	} else {
		clearTimeout(meta_title_stay_value);
		var meta_title_stay_value = setTimeout(function(){ check_pages_meta_title(target_value, $("#pages_id").val()); }, 1500);
	}

}

function meta_description_pages_change_detect(target_value, e) {

	$("#meta_description-check-loading").fadeIn("fast");

	if(e.keyCode === 13){
		check_pages_meta_description(target_value);
	} else if (e.keyCode === 8 || e.keyCode === 46) {
		clearTimeout(meta_description_stay_value);
		var meta_description_stay_value = setTimeout(function(){ check_pages_meta_description(target_value, $("#pages_id").val()); }, 1500);
	} else {
		clearTimeout(meta_description_stay_value);
		var meta_description_stay_value = setTimeout(function(){ check_pages_meta_description(target_value, $("#pages_id").val()); }, 1500);
	}

}
	
function generate_pages_link() {
	
	var pages_title = $("#pages_title").val();
	
	$.ajax({
		type: "POST",
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "generate_pages_link",
			parameters: pages_title
		},
		success: function(response) {
			if(response.status == true){
				$("#pages_link").val(response.values);
				check_pages_link(response.values, $("#pages_id").val());
			}
		}
	});

}
	
function check_pages_title(target_title, target_id) {
	
	var form_values   = {
		"pages_id": target_id,
		"pages_title": target_title
	}
	
	$.ajax({
		type: "POST",
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "check_pages_title",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == false){
				$("#pages_title").parent().removeClass("has-recommend");
				$("#pages_title").parent().removeClass("has-error");
				if (response.message == "<strong><?php echo translate("Title"); ?></strong> <?php echo translate("is required"); ?>") {
					$("#pages_title").parent().addClass("has-error");
				} else {
					$("#pages_title").parent().addClass("has-recommend");
				}
				$("#title-check-loading").fadeOut("fast");
				$("#pages_title").parent().find(".help-block").empty();
				$("#pages_title").parent().find(".help-block").append(response.message);
			} else {
				$("#pages_title").parent().removeClass("has-recommend");
				$("#title-check-loading").fadeOut("fast");
				$("#pages_title").parent().find(".help-block").empty();
                $("#pages_title").parent().find(".help-block").append('<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> <strong><?php echo translate("Title"); ?></strong> <?php echo translate("is good"); ?></span>');
			}
		}
	});

}
	
function check_pages_link(target_link, target_id) {
	
	var form_values   = {
		"pages_id": target_id,
		"pages_link": target_link
	}

	$.ajax({
		type: "POST",
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "check_pages_link",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == false){
				$("#pages_link").parent().parent().removeClass("has-error");
				$("#pages_link").parent().parent().addClass("has-error");
				$("#link-check-loading").fadeOut("fast");
				$("#pages_link").parent().parent().find(".help-block").empty();
				$("#pages_link").parent().parent().find(".help-block").append(response.message);
			} else {
				$("#pages_link").parent().parent().removeClass("has-error");
				$("#link-check-loading").fadeOut("fast");
				$("#pages_link").parent().parent().find(".help-block").empty();
                $("#pages_link").parent().parent().find(".help-block").append('<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> <strong><?php echo translate("Link"); ?></strong> <?php echo translate("is available"); ?></span>');
			}
		}
	});

}
	
function check_pages_meta_title(target_meta_title, target_id) {
	
	var form_values   = {
		"pages_id": target_id,
		"pages_meta_title": target_meta_title
	}
	
	$.ajax({
		type: "POST",
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "check_pages_meta_title",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == false){
				$("#pages_meta_title").parent().removeClass("has-recommend");
				$("#pages_meta_title").parent().removeClass("has-error");
				$("#pages_meta_title").parent().addClass("has-recommend");
				$("#meta_title-check-loading").fadeOut("fast");
				$("#pages_meta_title").parent().find(".help-block").empty();
				$("#pages_meta_title").parent().find(".help-block").append(response.message);
			} else {
				$("#pages_meta_title").parent().removeClass("has-recommend");
				$("#meta_title-check-loading").fadeOut("fast");
				$("#pages_meta_title").parent().find(".help-block").empty();
                $("#pages_meta_title").parent().find(".help-block").append('<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> <strong><?php echo translate("Meta Title"); ?></strong> <?php echo translate("is good"); ?></span>');
			}
		}
	});

}
	
function check_pages_meta_description(target_meta_description, target_id) {
	
	var form_values   = {
		"pages_id": target_id,
		"pages_meta_description": target_meta_description
	}
	
	$.ajax({
		type: "POST",
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "check_pages_meta_description",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == false){
				$("#pages_meta_description").parent().removeClass("has-recommend");
				$("#pages_meta_description").parent().removeClass("has-error");
				$("#pages_meta_description").parent().addClass("has-recommend");
				$("#meta_description-check-loading").fadeOut("fast");
				$("#pages_meta_description").parent().find(".help-block").empty();
				$("#pages_meta_description").parent().find(".help-block").append(response.message);
			} else {
				$("#pages_meta_description").parent().removeClass("has-recommend");
				$("#meta_description-check-loading").fadeOut("fast");
				$("#pages_meta_description").parent().find(".help-block").empty();
                $("#pages_meta_description").parent().find(".help-block").append('<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i> <strong><?php echo translate("Meta Description"); ?></strong> <?php echo translate("is good"); ?></span>');
			}
		}
	});

}

function create_pages_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: <?php if (isset($configs["page_limit"]) && !empty($configs["page_limit"])) { echo $configs["page_limit"]; } else { echo "20"; } ?>,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_pages_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "") {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "") {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "") {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "") {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "") {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "") {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_pages_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "") {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "") {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "") {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "") {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_pages_table_row(data_table) {

	mainDataTable.fnDestroy();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_pages_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (data_table["pages_translate"] != null && data_table["pages_translate"] != "") {
				if(data_table["pages_translate"] != 0 && data_table["pages_translate"] != null) {
					$("#datatable-list").find("#datatable-" + response.values["pages_id"]).remove();
				}
			}
			$("#datatable-list").prepend(response.html);
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_pages_open", $(this).attr("data-target"));
				} else {
					edit_pages_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_pages_open", $(this).attr("data-target"));
				} else {
					copy_pages_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_pages_open", $(this).attr("data-target") + "\', \'" + $(this).attr("data-language"));
				} else {
					translate_pages_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_pages_modal_prompt($(this).attr("data-content"), "delete_pages_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_pages_open($(this).attr("data-target"));
			});
			if (data_table["pages_translate"] != null && data_table["pages_translate"] != "") {
				if(data_table["pages_translate"] != 0 && data_table["pages_translate"] != null) {
					refine_pages_table_row(response.values);
				} else {
					refine_pages_table_row(data_table);
				}
			} else {
				refine_pages_table_row(data_table);
			}
			if (count_data_table < datatable_data_limit) {
				create_pages_table();
			} else {
				create_pages_table_defer();
			}
		}
	});

}

function update_pages_table_row(data_table, data_table_renew = false) {

	mainDataTable.fnDestroy();

	var data_table_old_id = $("#datatable-list").find("#datatable-pages_id-" + data_table["pages_id"]).parent().parent().attr("data-target");
	$("#datatable-list").find("#datatable-pages_id-" + data_table["pages_id"]).parent().parent().remove();
	if (data_table_renew != false) {
		$("#datatable-list").find("#datatable-pages_id-" + data_table_renew).parent().parent().remove();
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_pages_table_row",
			parameters: data_table,
			data_table_old_id: data_table_old_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (data_table_old_id != data_table["pages_id"]) {
				$("#datatable-list").find("#datatable-pages_id-" + data_table["pages_id"]).parent().parent().remove();
			}
			$("#datatable-list").prepend(response.html);
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_pages_open", $(this).attr("data-target"));
				} else {
					edit_pages_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_pages_open", $(this).attr("data-target"));
				} else {
					copy_pages_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_pages_open", $(this).attr("data-target") + "\', \'" + $(this).attr("data-language"));
				} else {
					translate_pages_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_pages_modal_prompt($(this).attr("data-content"), "delete_pages_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_pages_open($(this).attr("data-target"));
			});
			if (response.values_old != null) {
				refine_pages_table_row(response.values_old);
			} else {
				refine_pages_table_row(response.values);
			}
			if (count_data_table < datatable_data_limit) {
				create_pages_table();
			} else {
				create_pages_table_defer();
			}
		}
	});

}

function delete_pages_table_row(field_id, data_table_old_transform_id = null) {

	mainDataTable.fnDestroy();

	var data_table_old_id = $("#datatable-list").find("#datatable-pages_id-" + field_id).parent().parent().attr("data-target");
	$("#datatable-list").find("#datatable-pages_id-" + field_id).parent().parent().remove();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_pages_table_row",
			data_table_old_id: data_table_old_id,
			data_table_old_transform_id: data_table_old_transform_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if(response.values != null && response.values != "") {
				$("#datatable-list").find("#datatable-pages_id-" + response.values["pages_id"]).parent().parent().remove();
				$("#datatable-list").prepend(response.html);
				$(".btn-edit").unbind();
				$(".btn-edit").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_pages_open", $(this).attr("data-target"));
					} else {
						edit_pages_open($(this).attr("data-target"));
					}
				});
				$(".btn-copy").unbind();
				$(".btn-copy").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_pages_open", $(this).attr("data-target"));
					} else {
						copy_pages_open($(this).attr("data-target"));
					}
				});
				$(".btn-translate").unbind();
				$(".btn-translate").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_pages_open", $(this).attr("data-target") + "\', \'" + $(this).attr("data-language"));
					} else {
						translate_pages_open($(this).attr("data-target"), $(this).attr("data-language"));
					}
				});
				$(".btn-delete").unbind();
				$(".btn-delete").click(function () {
					show_pages_modal_prompt($(this).attr("data-content"), "delete_pages_data", $(this).attr("data-target"));
				});
				$(".btn-view").unbind();
				$(".btn-view").click(function () {
					view_pages_open($(this).attr("data-target"));
				});
				if (response.values_old_transform != null && response.values_old_transform != "") {
					refine_pages_table_row(response.values_old_transform);
				} else {
					refine_pages_table_row(response.values);
				}
			} else {
				$("#datatable-list").find("#datatable-" + field_id).remove();
			}
			if(response.values_old_transform != null && response.values_old_transform != "") {
				$("#datatable-list").prepend(response.html_old_transform);
				$(".btn-edit").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_pages_open", $(this).attr("data-target"));
					} else {
						edit_pages_open($(this).attr("data-target"));
					}
				});
				$(".btn-copy").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_pages_open", $(this).attr("data-target"));
					} else {
						copy_pages_open($(this).attr("data-target"));
					}
				});
				$(".btn-translate").click(function () {
					if ($("#form-content").is(":visible")) {
						show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_pages_open", $(this).attr("data-target") + "\', \'" + $(this).attr("data-language"));
					} else {
						translate_pages_open($(this).attr("data-target"), $(this).attr("data-language"));
					}
				});
				$(".btn-delete").click(function () {
					show_pages_modal_prompt($(this).attr("data-content"), "delete_pages_data", $(this).attr("data-target"));
				});
				$(".btn-view").click(function () {
					view_pages_open($(this).attr("data-target"));
				});
				refine_pages_table_row(response.values_old_transform);
			}
			if (count_data_table < datatable_data_limit) {
				create_pages_table();
			} else {
				create_pages_table_defer();
			}
		}
	});

}

function refine_pages_table_row(data_table) {

	if (data_table !== null) {
		var tallest_field = 0;
		var data_table_id = data_table["pages_id"];
		for(var key in table_module_field) {
			if(table_module_field.hasOwnProperty(key)) {
				if (table_module_field[key] == true && document.getElementById("datatable-" + key + "-" + data_table_id)) {
					if (document.getElementById("datatable-" + key + "-" + data_table_id).offsetHeight > tallest_field) {
						tallest_field = document.getElementById("datatable-" + key + "-" + data_table_id).offsetHeight;
					}
				}
			}
		}
		var data_table_id = data_table["pages_id"];
		for(var key in table_module_field) {
			if(table_module_field.hasOwnProperty(key)) {
				if (table_module_field[key] == true && document.getElementById("datatable-" + key + "-" + data_table_id)) {
					$("#datatable-" + key + "-" + data_table_id).height(tallest_field - 10);
				}
			}
		}
		if (data_table["pages_translation"] != null) {
			var tallest_field = 0;
			for (var j = 0; j < data_table["pages_translation"].length; j++) {
				var data_table_id = data_table["pages_translation"][j]["pages_id"];
				for(var key in table_module_field) {
					if(table_module_field.hasOwnProperty(key)) {
						if (table_module_field[key] == true && document.getElementById("datatable-" + key + "-" + data_table_id)) {
							if (document.getElementById("datatable-" + key + "-" + data_table_id).offsetHeight > tallest_field) {
								tallest_field = document.getElementById("datatable-" + key + "-" + data_table_id).offsetHeight;
							}
						}
					}
				}
			}
			for (var j = 0; j < data_table["pages_translation"].length; j++) {
				var data_table_id = data_table["pages_translation"][j]["pages_id"];
				for(var key in table_module_field) {
					if(table_module_field.hasOwnProperty(key)) {
						if (table_module_field[key] == true && document.getElementById("datatable-" + key + "-" + data_table_id)) {
							$("#datatable-" + key + "-" + data_table_id).height(tallest_field - 10);
						}
					}
				}
			}
		}
	}

}

function show_pages_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_pages_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {
	
	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_pages_open", $(this).attr("data-target"));
		} else {
			create_pages_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_pages_open", $(this).attr("data-target"));
		} else {
			edit_pages_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_pages_open", $(this).attr("data-target"));
		} else {
			copy_pages_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_pages_open", $(this).attr("data-target") + "\', \'" + $(this).attr("data-language"));
		} else {
			translate_pages_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});
	
	$(".btn-delete").click(function () {
		show_pages_modal_prompt($(this).attr("data-content"), "delete_pages_data", $(this).attr("data-target"));
	});
	
	$(".btn-view").click(function () {
		view_pages_open($(this).attr("data-target"));
	});
	
	$(".btn-form-close").click(function () {
		show_pages_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_pages_close", "");
	});
	
	$("#from").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});
	
	$("#to").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});
	
	$("#pages_activate").click(function () {
		if ($("#pages_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});
	
	
	$("#modules_id").select2();
	$("#modules_record_target").select2();
	$("#modules_id").change(function(event){
		show_module_pages_content_list(this.value);
	});
	
	<?php
	if ($count_languages_short_name > 1) {
	?>
	$("#languages_short_name").select2();
	$("#pages_translate").select2();
	<?php
	}
	?>
	
	$("#pages_title").keyup(function(event){
		$("#pages_title_stringlength").empty().append($(this).val().length + "/240");
		$("#pages_title_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#pages_title_stringlength").addClass("text-success");
		} else {
			$("#pages_title_stringlength").addClass("text-danger");
		}
	});
	$("#pages_link").keyup(function(event){
		$("#pages_link_stringlength").empty().append($(this).val().length + "/200");
		$("#pages_link_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#pages_link_stringlength").addClass("text-success");
		} else {
			$("#pages_link_stringlength").addClass("text-danger");
		}
	});
	$("#pages_meta_title").keyup(function(event){
		$("#pages_meta_title_stringlength").empty().append($(this).val().length + "/240");
		$("#pages_meta_title_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#pages_meta_title_stringlength").addClass("text-success");
		} else {
			$("#pages_meta_title_stringlength").addClass("text-danger");
		}
	});
	$("#pages_title").keyup(function(event){
		title_pages_change_detect($(this).val(), event);
	});
	$("#pages_link").keyup(function(event){
		link_pages_change_detect($(this).val(), event);
	});
	$("#pages_meta_title").keyup(function(event){
		meta_title_pages_change_detect($(this).val(), event);
	});	
	$("#pages_meta_description").keyup(function(event){
		meta_description_pages_change_detect($(this).val(), event);
	});
	$("#pages_meta_keyword").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "+",
		removeWithBackspace: true,
		delimiter: [","]
	});
	$("#pages_template-fileselector-btn").fancybox({
	  "width": 880,
	  "height": 900,
	  "minHeight": 500,
	  "type": "iframe",
	  "autoScale": "auto",
	  "padding": 0
	});
	
	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_pages_table();
	} else {
		create_pages_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_pages_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_pages_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_pages_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().pages_id != null) {
			edit_pages_open(get_url_param().pages_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().pages_id != null) {
			copy_pages_open(get_url_param().pages_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().pages_id != null) {
			translate_pages_open(get_url_param().pages_id, get_url_param().pages_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().pages_id != null) {
			view_pages_open(get_url_param().pages_id);
		}
	}
	/* URL response (end) */
	
	if (data_table !== null) {
		for (var i = 0; i < data_table.length; i++) {
			refine_pages_table_row(data_table[i]);
		}
	}
	
	if ($("#pages_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}
	
	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");
	
});
</script>
<!-- initialization: JavaScript (end) -->

<script>
/* 
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");
        
jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_pages_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"languages_short_name": {
					required: true,
				},
				"pages_title": {
					required: true,
				},
				"pages_link": {
					required: true,
					no_space: true,
					no_specialchar_soft: true,
				},
				"pages_meta_title": {
					required: false,
				},
				"pages_meta_description": {
					required: false,
				},
				"pages_meta_keyword": {
					required: false,
				},
				"pages_template": {
					required: true,
				},
			 },
			 messages: {
				"languages_short_name": {
					required: "<strong><?php echo translate("Language"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"pages_title": {
					required: "<strong><?php echo translate("Title"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"pages_link": {
					required: "<strong><?php echo translate("Link"); ?></strong> <?php echo translate("is required"); ?>",
					no_space: "<strong><?php echo translate("Link"); ?></strong> <?php echo translate("does not allow white spaces"); ?>",
					no_specialchar_soft: "<strong><?php echo translate("Link"); ?></strong> <?php echo translate("does not allow special characters and white spaces excluding ., -, _"); ?>",
				},
				"pages_meta_title": {
					required: "<strong><?php echo translate("Meta Title"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"pages_meta_description": {
					required: "<strong><?php echo translate("Meta Description"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"pages_meta_keyword": {
					required: "<strong><?php echo translate("Meta Keywords"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"pages_template": {
					required: "<strong><?php echo translate("Template"); ?></strong> <?php echo translate("is required"); ?>",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once("templates/".$configs["backend_template"]."/footer.php");
?>