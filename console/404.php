<?php 
require ("system/include.php");

date_default_timezone_set($configs["timezone"]); 
mb_internal_encoding($configs["encoding"]); global $configs;

$title="Error 403";
$module_page="403.php";
$module_sql_page = $configs['function_path'].'authentication.php';

/* render - header */
include("templates/".$configs["backend_template"]."/header-login.php");
?>

<body>

<div class="bg-login bg-image"></div>

<!-- Error Content -->
<div class="content bg-white text-center pulldown overflow-hidden">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <!-- Error Titles -->
            <h1 class="font-s128 font-w300 text-city animated bounceIn">404</h1>
            <h2 class="h3 font-w300 push-50 animated fadeInUp">We are sorry but the page you are looking for was not found..</h2>
            <!-- END Error Titles -->
        </div>
    </div>
</div>
<!-- END Error Content -->

<!-- Error Footer -->
<div class="content pulldown text-muted text-center">
    <a class="link-effect" href="./">Go back to Dashboard</a>
</div>
<!-- END Error Footer -->
        
</body>
<script>
$( document ).ready(function() {
	$("#modal-response").modal('show');
});

	$(function () {
		App.initHelpers("slick");
	});
</script>


<!-- No js functions to initialize -->



<script>
var url = "<?php echo $module_sql_page; ?>"; /* SQL url for AJAX actions */
var module_db = "<?php echo $module_db; ?>"; /* Module database table name */
var table_module_field = $.parseJSON('<?php echo json_encode($table_module_field); ?>'); /* Config module to display */

</script>



<?php include("templates/".$configs["backend_template"]."/footer-login.php"); ?>