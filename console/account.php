<?php 
require_once("system/include.php");

/* configurations: Users */
$users = get_modules_data_by_id("6");
if (isset($users) && !empty($users)) {
	$title = translate("Account Settings");
	$module_page = "account.php";
	$module_page_link = backend_rewrite_url("account.php");
	$module_function_page = "system/".$users["modules_function_link"];
} else {
	$title = translate("Account Settings");
	$module_page = "account.php";
	$module_page_link = backend_rewrite_url("account.php");
	$module_function_page = "system/core/".$configs["version"]."/users.php";
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Users */
if (!authentication_session_users()) {
	authentication_deny();
}

if (isset($configs["api_session_credentials"]["Facebook"]["app_id"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_id"]) && isset($configs["api_session_credentials"]["Facebook"]["app_secret"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_secret"])) {
    authentication_connect_api_session("Facebook", $configs["api_session_credentials"]["Facebook"]);
}
if (isset($configs["api_session_credentials"]["Google"]["app_id"]) && !empty($configs["api_session_credentials"]["Google"]["app_id"]) && isset($configs["api_session_credentials"]["Google"]["app_secret"]) && !empty($configs["api_session_credentials"]["Google"]["app_secret"])) {
    authentication_connect_api_session("Google", $configs["api_session_credentials"]["Google"]);
}
if (isset($configs["api_session_credentials"]["Twitter"]["app_id"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_id"]) && isset($configs["api_session_credentials"]["Twitter"]["app_secret"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_secret"])) { 
	authentication_connect_api_session("Twitter", $configs["api_session_credentials"]["Twitter"]);
}
if (isset($configs["api_session_credentials"]["Line"]["app_id"]) && !empty($configs["api_session_credentials"]["Line"]["app_id"]) && isset($configs["api_session_credentials"]["Line"]["app_secret"]) && !empty($configs["api_session_credentials"]["Line"]["app_secret"])) { 
	authentication_connect_api_session("Line", $configs["api_session_credentials"]["Line"]);
}
if (isset($configs["api_session_credentials"]["Instagram"]["app_id"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_id"]) && isset($configs["api_session_credentials"]["Instagram"]["app_secret"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_secret"])) { 
	authentication_connect_api_session("Instagram", $configs["api_session_credentials"]["Instagram"]);
}
if (isset($configs["api_session_credentials"]["Microsoft"]["app_id"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_id"]) && isset($configs["api_session_credentials"]["Microsoft"]["app_secret"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_secret"])) { 
	authentication_connect_api_session("Microsoft", $configs["api_session_credentials"]["Microsoft"]);
}

require_once("templates/".$configs["backend_template"]."/header.php");
require_once("templates/".$configs["backend_template"]."/overlay.php");
require_once("templates/".$configs["backend_template"]."/sidebar.php");
require_once("templates/".$configs["backend_template"]."/navbar.php");
require_once("templates/".$configs["backend_template"]."/container-header.php");
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" title="Toggle Fullscreen" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="Toggle Show" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo $title; ?></h3></a>
			</div>
			<div class="block-content">

				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo $module_function_page; ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="users">
					<input type="hidden" id="users_id" name="users_id">
					<input type="hidden" id="users_action" name="users_action" value="">
					<input type="hidden" id="users_users_id" name="users_users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_users_username" name="users_users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_users_name" name="users_users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_users_last_name" name="users_users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_username"><?php echo translate("Username"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="text" id="users_username" name="users_username" value="" maxlength="100" placeholder="<?php echo translate("Username does not allow special characters and white spaces excluding ., -, _ and can not be duplicated"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_name"><?php echo translate("Name"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control" type="text" id="users_name" name="users_name" value="" placeholder="<?php echo translate("Name helps administrators identify user's identity"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_last_name"><?php echo translate("Last Name"); ?></label>
							<div class="col-md-12">
								<input class="form-control" type="text" id="users_last_name" name="users_last_name" value="" placeholder="<?php echo translate("Last name helps administrators identify user's identity"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_nick_name"><?php echo translate("Nickname"); ?></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="text" id="users_nick_name" name="users_nick_name" value="" maxlength="100" placeholder="<?php echo translate("Nickname helps administrators identify user's identity"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_email"><?php echo translate("Email"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="text" id="users_email" name="users_email" value="" maxlength="100" placeholder="<?php echo translate("Email will be used for notification"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="block block-bordered">
							<div class="block-header">
								<label for="users_image"><i class="si si-picture push-10-r"></i><?php echo translate("Image"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<div id="slim-form-users_image" class="col-md-12 col-md-8">
												<div id="slim-users_image" class="slim" data-ratio="640:639" data-size="640,640" data-jpeg-compression="<?php echo $users["modules_image_crop_quality"]; ?>" data-did-remove="remove_users_image">
													<input class="form-control users_image" type="file" id="users_image" name="users_image">
												</div>
												
												<div class="help-block"><?php echo translate(""); ?></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
                    <div class="row items-push">
						<div class="col-md-12" style="margin-top: 30px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>


				</form>
			</div>
		</div>
	</div>
</div>

<?php 
if ((isset($configs["api_session_credentials"]["Facebook"]["app_id"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_id"]) && isset($configs["api_session_credentials"]["Facebook"]["app_secret"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_secret"])) 
	|| (isset($configs["api_session_credentials"]["Google"]["app_id"]) && !empty($configs["api_session_credentials"]["Google"]["app_id"]) && isset($configs["api_session_credentials"]["Google"]["app_secret"]) && !empty($configs["api_session_credentials"]["Google"]["app_secret"])) 
	|| (isset($configs["api_session_credentials"]["Twitter"]["app_id"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_id"]) && isset($configs["api_session_credentials"]["Twitter"]["app_secret"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_secret"])) 
	|| (isset($configs["api_session_credentials"]["Line"]["app_id"]) && !empty($configs["api_session_credentials"]["Line"]["app_id"]) && isset($configs["api_session_credentials"]["Line"]["app_secret"]) && !empty($configs["api_session_credentials"]["Line"]["app_secret"]))
	|| (isset($configs["api_session_credentials"]["Instagram"]["app_id"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_id"]) && isset($configs["api_session_credentials"]["Instagram"]["app_secret"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_secret"])) 
	|| (isset($configs["api_session_credentials"]["Microsoft"]["app_id"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_id"]) && isset($configs["api_session_credentials"]["Microsoft"]["app_secret"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_secret"]))) { 
?>
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-linked-accounts">
			<div class="block-header">
				<a><h3 class="block-title" id="form-title-password"><?php echo translate("Linked Accounts"); ?></h3></a>
			</div>
			<div class="block-content">

				<form id="module-connect-api" class="form-horizontal" onsubmit="return false;">
					
					<?php 
					if (isset($configs["api_session_credentials"]["Facebook"]["app_id"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_id"]) && isset($configs["api_session_credentials"]["Facebook"]["app_secret"]) && !empty($configs["api_session_credentials"]["Facebook"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-facebook"><div class="sidebar-icon"><i class="fa fa-facebook-official" aria-hidden="true"></i></div><?php echo translate("Facebook"); ?></label>
									<div class="col-md-9">
										<?php
										$facebook_login_url = create_api_connect_link("Facebook", $configs["api_session_credentials"]["Facebook"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Facebook")) {
											$facebook_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-facebook" name="switch-connect-facebook" data-target="<?php echo $facebook_login_url;?>" type="checkbox" value="1" <?php echo $facebook_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<?php 
					if (isset($configs["api_session_credentials"]["Google"]["app_id"]) && !empty($configs["api_session_credentials"]["Google"]["app_id"]) && isset($configs["api_session_credentials"]["Google"]["app_secret"]) && !empty($configs["api_session_credentials"]["Google"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-google"><div class="sidebar-icon"><i class="fa fa-google" aria-hidden="true"></i></div><?php echo translate("Google"); ?></label>
									<div class="col-md-9">
										<?php
										$google_login_url = create_api_connect_link("Google", $configs["api_session_credentials"]["Google"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Google")) {
											$google_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-google" name="switch-connect-google" data-target="<?php echo $google_login_url;?>" type="checkbox" value="1" <?php echo $google_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<?php 
					if (isset($configs["api_session_credentials"]["Twitter"]["app_id"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_id"]) && isset($configs["api_session_credentials"]["Twitter"]["app_secret"]) && !empty($configs["api_session_credentials"]["Twitter"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-twitter"><div class="sidebar-icon"><i class="fa fa-twitter" aria-hidden="true"></i></div><?php echo translate("Twitter"); ?></label>
									<div class="col-md-9">
										<?php
										$twitter_login_url = create_api_connect_link("Twitter", $configs["backend_twitter_app_redirect_url"], $configs["api_session_credentials"]["Twitter"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Twitter")) {
											$twitter_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-twitter" name="switch-connect-twitter" data-target="<?php echo $twitter_login_url;?>" type="checkbox" value="1" <?php echo $twitter_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<?php 
					if (isset($configs["api_session_credentials"]["Line"]["app_id"]) && !empty($configs["api_session_credentials"]["Line"]["app_id"]) && isset($configs["api_session_credentials"]["Line"]["app_secret"]) && !empty($configs["api_session_credentials"]["Line"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-line"><div class="sidebar-icon"><img src="templates/<?php echo $configs["backend_template"]; ?>/images/icons/line-messenger.svg" style="width: 13px; height: auto;"/></div><?php echo translate("Line"); ?></label>
									<div class="col-md-9">
										<?php
										$line_login_url = create_api_connect_link("Line", $configs["api_session_credentials"]["Line"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Line")) {
											$line_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-line" name="switch-connect-line" data-target="<?php echo $line_login_url;?>" type="checkbox" value="1" <?php echo $line_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<?php 
					if (isset($configs["api_session_credentials"]["Instagram"]["app_id"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_id"]) && isset($configs["api_session_credentials"]["Instagram"]["app_secret"]) && !empty($configs["api_session_credentials"]["Instagram"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-instagram"><div class="sidebar-icon"><i class="fa fa-instagram" aria-hidden="true"></i></div><?php echo translate("Instagram"); ?></label>
									<div class="col-md-9">
										<?php
										$instagram_login_url = create_api_connect_link("Instagram", $configs["api_session_credentials"]["Instagram"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Instagram")) {
											$instagram_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-instagram" name="switch-connect-instagram" data-target="<?php echo $instagram_login_url;?>" type="checkbox" value="1" <?php echo $instagram_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<?php 
					if (isset($configs["api_session_credentials"]["Microsoft"]["app_id"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_id"]) && isset($configs["api_session_credentials"]["Microsoft"]["app_secret"]) && !empty($configs["api_session_credentials"]["Microsoft"]["app_secret"])) { 
					?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="col-md-3" for="button-connect-microsoft"><div class="sidebar-icon"><i class="fa fa-windows" aria-hidden="true"></i></div><?php echo translate("Microsoft"); ?></label>
									<div class="col-md-9">
										<?php
										$microsoft_login_url = create_api_connect_link("Microsoft", $configs["api_session_credentials"]["Microsoft"]);
										if (get_users_api_sessions_by_id($_SESSION["users_id"], "Microsoft")) {
											$microsoft_switch_checked = "checked";
										}
										?>
										<label class="css-input switch switch-square switch-lg switch-info">
											<input id="switch-connect-microsoft" name="switch-connect-microsoft" data-target="<?php echo $microsoft_login_url;?>" type="checkbox" value="1" <?php echo $microsoft_switch_checked; ?>><span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
					
					<div class="row items-push">
						<div class="col-md-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<?php
}
?>


<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content-password">
			<div class="block-header">
				<a><h3 class="block-title" id="form-title-password"><?php echo translate("Change Password"); ?></h3></a>
			</div>
			<div class="block-content">

				<form id="module-form-password" class="form-horizontal" onsubmit="return false;">
					<input type="hidden" id="password_users_id" name="password_users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="password_users_username" name="password_users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="password_users_name" name="password_users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="password_users_last_name" name="password_users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">

					<?php 
					if (!empty($_SESSION["users_password"]) && $_SESSION["users_password"] != "") {
                    ?>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_current_password"><?php echo translate("Current Password"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="password" id="users_current_password" name="users_current_password" value="" maxlength="100" placeholder="<?php echo translate("Current password for login"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>
					<?php
                    }
					?>
                    
                    <div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_password"><?php echo translate("Password"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="password" id="users_password" name="users_password" value="" maxlength="100" placeholder="<?php echo translate("Password for login"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>
                    
                    <div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="users_confirm_password"><?php echo translate("Confirm Password"); ?><span class="text-danger">*</span></label>
							<div class="col-md-12">
								<input class="form-control js-maxlength" type="password" id="users_confirm_password" name="users_confirm_password" value="" maxlength="100" placeholder="<?php echo translate("Type new password again"); ?>">
								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="row items-push">
						<div class="col-md-12" style="margin-top: 30px;">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-password-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>


				</form>
			</div>
		</div>
	</div>
</div>



<?php
include("templates/".$configs["backend_template"]."/container-footer.php");
?>
<!-- notification response function -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- end notification response function -->

<!-- Prompt confirmation -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- End Prompt confirmation -->

<!-- include styles (begin) -->
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/slim/slim.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" id="css-main" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include styles (end) -->

<!-- include JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="plugins/slim/slim.jquery.min.js"></script>
<!-- include JavaScript (end) -->

<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo $module_function_page; ?>";
	
$(document).ready(function() {
	
	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-content-password").css("visibility", "visible");
	$("#form-content-password").fadeTo("slow", 1);
	$("#form-content-password").removeClass("block-opt-hidden");
	$("#form-content-password").fadeIn("slow").slideDown("fast");
	$("#form-content-password").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_users_data_by_current_session_id"
		},
		success: function(response) {
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo strtoupper("Basic Information"); ?>");
				
				$("#users_username").val(response.values['users_username']);
				$("#users_name").val(response.values['users_name']);
				$("#users_last_name").val(response.values['users_last_name']);
				$("#users_nick_name").val(response.values['users_nick_name']);
				$("#users_email").val(response.values['users_email']);
				
				$("#slim-users_image").slim("destroy");
				if (response.values["users_image_path"] != "" && response.values["users_image_path"] != null) {
					$("#users_image_old_image").remove();
					$("#slim-users_image").prepend('<img id="users_image_old_image" src="' + response.values["users_image_path"] + '" alt=""/> <div class="help-block"><?php echo translate(""); ?></div>');
					$("#users_image_old").remove();
					$("#slim-form-users_image").append('<input id="users_image_old" name="users_image_old" class="users_image_old" type="hidden" /> <div class="help-block"><?php echo translate(""); ?></div>');
					$("#users_image_old").val(response.values["users_image"]);
				}
				$("#slim-users_image").slim({
					ratio: "640:639",
					size: "640,640",
					jpegCompression: <?php echo $users["modules_image_crop_quality"]; ?>,
					didRemove: function(data, ready) {
						$(".users_image_old").each(function(index) {
							if ($(this).val().indexOf(data.input.name) >= 0) {
								$(this).val("");
							}
						});
					},
					label: "<?php echo translate("Drop your image here"); ?>",
					buttonConfirmLabel: "<?php echo translate("Ok"); ?>",
					buttonCancelLabel: "<?php echo translate("Cancel"); ?>",
				});

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_users_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_users_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});
	
	$("#form-content-password").removeClass("block-opt-refresh");
	
	$("#switch-connect-facebook").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Facebook");
		}
	});
	
	$("#switch-connect-google").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Google");
		}
	});
	
	$("#switch-connect-twitter").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Twitter");
		}
	});
	
	$("#switch-connect-line").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Line");
		}
	});
	
	$("#switch-connect-instagram").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Instagram");
		}
	});
	
	$("#switch-connect-microsoft").change(function () {
		if ($(this).is(":checked") === true) {
			location.href = $(this).data("target");
		} else {
			disconnect_api_sessions("Microsoft");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

	$("#button-data-password-submit").click(function () {
		submit_users_data_password();
	});

});
	

function submit_users_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);
	
	var form_values   = {
		"users_username": $("#users_username").val(),
		"users_name": $("#users_name").val(),
		"users_last_name": $("#users_last_name").val(),
		"users_nick_name": $("#users_nick_name").val(),
		"users_email": $("#users_email").val(),	
		/*Assign image crop value users_image*/
		"users_image": $("#users_image").val().trim(),

		"users_users_id": $("#users_users_id").val(),
		"users_users_username": $("#users_users_username").val(),
		"users_users_name": $("#users_users_name").val(),
		"users_users_last_name": $("#users_users_last_name").val(),
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_users_personal_data",
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				$("#users_id").val(response.values["users_id"]);
				if (response.values["users_id"] !== undefined) {
					
					var form_data = new FormData(document.querySelector("#module-form"));
					form_data.append("method", "upload_users_files");
					form_data.append("users_id", response.values["users_id"]);
					
					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: form_data,
						processData: false,
						contentType: false,
						success: function(response_upload) {
							console.log(response_upload);
							show_users_modal_response(response.status, "<?php echo translate("Successfully updated data"); ?>");
							$("html, body").animate({
									scrollTop: "0px"
							}, "fast");
							$("#form-content").removeClass("block-opt-refresh");
							$("#button-data-submit").prop("disabled", false);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							show_users_modal_response(errorThrown, textStatus + ": " + errorThrown);
							$("#form-content").removeClass("block-opt-refresh");
							$("#button-data-submit").prop("disabled", false);
						}
					});

				} else {
					show_users_modal_response(response.status, response.message);
					$("#users_id").val(response.values.users_id);
					$("#form-content").removeClass("block-opt-refresh");
					$("#button-data-submit").prop("disabled", false);
				}
			} else if (response.status == false) {
				show_users_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
				$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_users_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
			$("#button-data-submit").prop("disabled", false);
		}
	});

}


function submit_users_data_password() {

	$("#form-content-password").addClass("block-opt-refresh");

    $("#button-data-password-submit").prop("disabled", true);
	<?php 
    if (!empty($_SESSION["users_password"]) && $_SESSION["users_password"] != "") {
    ?>
	if ($("#users_current_password").val() == "") {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Current Password") . "</strong> " . translate("is required"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", false);
	} else if ($("#users_password").val() == "") {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Password") . "</strong> " . translate("is required"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", false);
	} else if ($("#users_confirm_password").val() == "") {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Confirm Password") . "</strong> " . translate("is required"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", false);
	} else if ($("#users_password").val() == $("#users_confirm_password").val()) {
	
		var form_values   = {
			"users_current_password": $("#users_current_password").val(),
			"users_password": $("#users_password").val(),
			"users_confirm_password": $("#users_confirm_password").val(),
			
			"users_users_id": $("#password_users_id").val(),
			"users_users_username": $("#password_users_username").val(),
			"users_users_name": $("#password_users_name").val(),
			"users_users_last_name": $("#password_users_last_name").val(),
		}
	<?php
    } else {
	?>
	if ($("#users_password").val() == "") {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Password") . "</strong> " . translate("is required"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", false);
	} else if ($("#users_confirm_password").val() == "") {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Confirm Password") . "</strong> " . translate("is required"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", false);
	} else if ($("#users_password").val() == $("#users_confirm_password").val()) {
	
		var form_values   = {
			"users_current_password": $("#users_current_password").val(),
			"users_password": $("#users_password").val(),
			"users_confirm_password": $("#users_confirm_password").val(),
			
			"users_users_id": $("#password_users_id").val(),
			"users_users_username": $("#password_users_username").val(),
			"users_users_name": $("#password_users_name").val(),
			"users_users_last_name": $("#password_users_last_name").val(),
		}
	<?php
    }
	?>
	
		$.ajax({
			url: url,
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				method: "update_users_password_data",
				parameters: form_values
			},
			success: function(response) {
				show_users_modal_response(response.status, response.message);
				$("#form-content-password").removeClass("block-opt-refresh");
				document.getElementById('button-data-password-submit').disable = false;
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				show_users_modal_response(errorThrown, textStatus + ": " + errorThrown);
				$("#form-content-password").removeClass("block-opt-refresh");
				document.getElementById('button-data-password-submit').disable = false;
			}
		});
	
	} else {
		show_users_modal_response(false, "<?php echo "<strong>" . translate("Password") . "</strong> " . translate("is mismatched"); ?>");
		$("#form-content-password").removeClass("block-opt-refresh");
		$("#button-data-password-submit").prop("disabled", true);
	}

}

function connect_facebook () {
	
	$("#form-linked-accounts").addClass("block-opt-refresh");
	$("#switch-connect-facebook").prop("disabled", true);
	
	FB.login(function(response) {
		if (response.authResponse) {
			FB.api('/me?fields=<?php echo $configs["backend_facebook_app_scope"]; ?>', function(response_api) {
				
				var form_values   = {
					"api_name": "Facebook",
					"api_access_token": response.authResponse.accessToken,
					"api_details": response_api
				}

				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "connect_api_sessions",
						parameters: form_values
					},
					success: function(response_connect) {
						show_users_modal_response(response_connect.status, response_connect.message);
						$("#form-linked-accounts").removeClass("block-opt-refresh");
						$("#switch-connect-facebook").prop("checked", true);
						$("#switch-connect-facebook").prop("disabled", false);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						show_users_modal_response(errorThrown, textStatus + ": " + errorThrown);
						$("#form-linked-accounts").removeClass("block-opt-refresh");
						$("#switch-connect-facebook").prop("checked", false);
						$("#switch-connect-facebook").prop("disabled", false);
					}
				});
				
			});
		} else {
			show_users_modal_response(false, "<?php echo translate("User cancelled login or did not fully authorize."); ?>");
			$("#form-linked-accounts").removeClass("block-opt-refresh");
			$("#switch-connect-facebook").prop("checked", false);
			$("#switch-connect-facebook").prop("disabled", false);
		}
	}, {scope: "<?php echo $configs["backend_facebook_app_scope"]; ?>"});
	return false;
	
};
window.fbAsyncInit = function() {
	FB.init({
	  appId: '<?php echo $configs["backend_facebook_app_id"]; ?>',
	  cookie: true, // This is important, it's not enabled by default
	  version: 'v2.10'
	});
};
(function(d, s, id){
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function disconnect_api_sessions (api_name) {
	
	$("#form-linked-accounts").addClass("block-opt-refresh");
	if (api_name == "Facebook") {
		$("#switch-connect-facebook").prop("disabled", true);
	} else if (api_name == "Google") {
		$("#switch-connect-google").prop("disabled", true);
	} else if (api_name == "Twitter") {
		$("#switch-connect-twitter").prop("disabled", true);
	} else if (api_name == "Line") {
		$("#switch-connect-line").prop("disabled", true);
	} else if (api_name == "Instagram") {
		$("#switch-connect-instagram").prop("disabled", true);
	} else if (api_name == "Microsoft") {
		$("#switch-connect-microsoft").prop("disabled", true);
	}
	
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "disconnect_api_sessions",
			parameters: api_name
		},
		success: function(response_connect) {
			show_users_modal_response(response_connect.status, response_connect.message);
			$("#form-linked-accounts").removeClass("block-opt-refresh");
			
			if (api_name == "Facebook") {
				$("#switch-connect-facebook").prop("checked", false);
				$("#switch-connect-facebook").prop("disabled", false);
			} else if (api_name == "Google") {
				$("#switch-connect-google").prop("checked", false);
				$("#switch-connect-google").prop("disabled", false);
			} else if (api_name == "Twitter") {
				$("#switch-connect-twitter").prop("checked", false);
				$("#switch-connect-twitter").prop("disabled", false);
			} else if (api_name == "Line") {
				$("#switch-connect-line").prop("checked", false);
				$("#switch-connect-line").prop("disabled", false);
			} else if (api_name == "Instagram") {
				$("#switch-connect-instagram").prop("checked", false);
				$("#switch-connect-instagram").prop("disabled", false);
			} else if (api_name == "Microsoft") {
				$("#switch-connect-microsoft").prop("checked", false);
				$("#switch-connect-microsoft").prop("disabled", false);
			}

		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_users_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-linked-accounts").removeClass("block-opt-refresh");
			if (api_name == "Facebook") {
				$("#switch-connect-facebook").prop("checked", true);
				$("#switch-connect-facebook").prop("disabled", false);
			} else if (api_name == "Google") {
				$("#switch-connect-google").prop("checked", true);
				$("#switch-connect-google").prop("disabled", false);
			} else if (api_name == "Twitter") {
				$("#switch-connect-twitter").prop("checked", true);
				$("#switch-connect-twitter").prop("disabled", false);
			} else if (api_name == "Line") {
				$("#switch-connect-line").prop("checked", true);
				$("#switch-connect-line").prop("disabled", false);
			} else if (api_name == "Instagram") {
				$("#switch-connect-instagram").prop("checked", true);
				$("#switch-connect-instagram").prop("disabled", false);
			} else if (api_name == "Microsoft") {
				$("#switch-connect-microsoft").prop("checked", true);
				$("#switch-connect-microsoft").prop("disabled", false);
			}
		}
	});
	
}


function show_users_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}


function show_users_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> Yes</button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> No</button>');
	$("#modal-prompt").modal("show");
}
</script>

<!-- Form Validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>

<!-- additionnal method -->
<script>
jQuery.validator.addMethod("CheckNoSpeCharSoft", function(value, element) {
  regex=/^[a-zA-Z0-9_.\-]+$/;
  return this.optional( element ) || regex.test( value );
}, 'special characters and are not allowed');

</script>
<!-- end  additionnal method -->

<script>
/*
FORM VALIDATION GUIDES
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery('.js-validation-bootstrap').validate({
			submitHandler: function(form) {
				submit_users_data();
			},
			errorClass: 'help-block animated fadeInDown',
			errorElement: 'div',
			errorPlacement: function(error, e) {
				jQuery(e).parents('.form-group > div').append(error);
			},
			highlight: function(e) {
				jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
				jQuery(e).closest('.help-block').remove();
			},
			success: function(e) {
				jQuery(e).closest('.form-group').removeClass('has-error');
				jQuery(e).closest('.help-block').remove();
			},
			rules: {
				'users_username': {
					 required: true,
					 CheckNoSpeCharSoft: true,
				},
				'users_name': {
					 required: true,
				},
				'users_email': {
					 required: true,
					 email: true,
				}
			 },
			 messages: {
				'users_username': {
					 required: '<strong><?php echo translate("Username"); ?></strong> <?php echo translate("is required"); ?>',
					 CheckNoSpeCharSoft: '<strong><?php echo translate("Username"); ?></strong> <?php echo translate("does not allow special characters and white spaces excluding ., -, _"); ?>',
				},
				'users_name': {
					required: '<strong><?php echo translate("Name"); ?></strong> <?php echo translate("is required"); ?>',
				},
				'users_email': {
					 required: '<strong><?php echo translate("Email"); ?></strong> <?php echo translate("is required"); ?>',
					 email: '<strong><?php echo translate("Email"); ?></strong> <?php echo translate("is invalid email"); ?>',
				}
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once("templates/".$configs["backend_template"]."/footer.php");
?>