<?php
require_once "system/include.php";

/* configurations: Groups */
$groups = get_modules_data_by_id("7");
if (isset($groups) && !empty($groups)) {
    $title = translate($groups["modules_name"]);
    $module_page = $groups["modules_link"];
    $module_page_link = backend_rewrite_url($groups["modules_link"]);
    $module_function_page = htmlspecialchars("system/" . $groups["modules_function_link"]);
} else {
    $title = translate("Groups");
    $module_page = "groups.php";
    $module_page_link = backend_rewrite_url("groups.php");
    $module_function_page = htmlspecialchars("system/core/" . $configs["version"] . "/groups.php");
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Groups */
if (!authentication_session_users()) {
    authentication_deny();
}
if (!authentication_session_modules($module_page)) {
    authentication_permission_deny();
}

if (isset($groups["modules_datatable_field"]) && !empty($groups["modules_datatable_field"])) {
    $table_module_field = array();
    for ($i = 0; $i < count($groups["modules_datatable_field"]); $i++) {
        if ($groups["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
            $groups_modules_datatable_field_display = true;
        } else {
            $groups_modules_datatable_field_display = false;
        }
        $table_module_field[$groups["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $groups_modules_datatable_field_display;
    }
} else {
    $table_module_field = array(
        "groups_id" => true,
        "groups_name" => true,
        "modules_ids" => true,
        "groups_protected" => false,
        "groups_date_created" => true,
        "users_id" => false,
        "users_username" => false,
        "users_name" => true,
        "users_last_name" => false,
        "groups_activate" => true,
        "groups_actions" => true,
    );
}

require_once "templates/" . $configs["backend_template"] . "/header.php";
require_once "templates/" . $configs["backend_template"] . "/overlay.php";
require_once "templates/" . $configs["backend_template"] . "/sidebar.php";
require_once "templates/" . $configs["backend_template"] . "/navbar.php";
require_once "templates/" . $configs["backend_template"] . "/container-header.php";
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header bg-mama-green">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo htmlspecialchars($module_function_page); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="groups">
					<input type="hidden" id="groups_id" name="groups_id">
					<input type="hidden" id="groups_action" name="groups_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="groups_name"><?php echo translate("Name"); ?><span class="text-danger">*</span> <span id="groups_name_stringlength" class="text-success pull-right push-10-l">0/100</span></label>
							<div class="col-md-12">
								<input class="form-control groups_name" type="text" id="groups_name" name="groups_name" value="" placeholder="<?php echo translate("Name of group"); ?>" >

							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="modules_ids"><?php echo translate("Modules"); ?> <a href="<?php echo backend_rewrite_url(".php"); ?>" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>"><i class="si si-settings"></i></a></label>
							<div class="col-md-12">
								<select class="form-control modules_ids" id="modules_ids" name="modules_ids"  multiple>

									<?php
/* PHP variable for filter of dynamic selector "modules_ids"*/
/* Example:
$modules_ids_filters = array(
"user_id" => "",
"languages_short_name" => "",
);
 */
$modules_ids_filters = "";
/* PHP variable for extended command of dynamic selector "modules_ids"*/
/* Example:
$modules_ids_extended_command = array(
array(
"conjunction" => "AND",
"key" => "user_name",
"operator" => "LIKE",
"value" => "%Michael%",
),
array(
"conjunction" => "OR",
"key" => "user_name",
"operator" => "LIKE",
"value" => "%Ammy%",
),
)
 */
$modules_ids_extended_command = "";
?>
									<script>
									/* JavaScript variable for filter of dynamic selector "modules_ids" */
									var modules_ids_filters = $.parseJSON("<?php echo addslashes(json_encode($modules_ids_filters)); ?>");
									</script>
									<?php
$count_modules_ids = count_modules_ids_data_dynamic_list("", "", "", "1", $modules_ids_filters, $modules_ids_extended_command);
if ($count_modules_ids <= $configs["datatable_data_limit"]) {
    ?>
									<?php
$modules_ids = get_modules_ids_data_dynamic_list("", "", "", "1", $modules_ids_filters, $modules_ids_extended_command);
    for ($i = 0; $i < count($modules_ids); $i++) {
        ?>
											<option value="<?php echo htmlspecialchars($modules_ids[$i]['modules_id']); ?>">
												<?php echo htmlspecialchars($modules_ids[$i]['modules_name']) . " (ID: " . $modules_ids[$i]['modules_id'] . ")"; ?>
											</option>
									<?php
}
    ?>
									<?php
} else {
    ?>
									<?php
}
?>

								</select>
								<div id="modules_ids-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>

							</div>
						</div>
					</div>

					<?php
$groups_protected_default_value = unserialize('a:1:{i:0;i:1;}');
?>
					<script>
					/* JavaScript variable for dynamic default value "groups_protected" */
					var groups_protected_default_value = $.parseJSON("<?php echo addslashes(json_encode($groups_protected_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="groups_protected"><?php echo translate("Protected"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="groups_protected" name="groups_protected" class="groups_protected" type="checkbox" value="<?php echo $groups_protected_default_value[0]; ?>"><span></span> <?php echo translate(""); ?>
								</label>

							</div>
						</div>
					</div>

					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list"><i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span></a>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content"); ?></div>
                                        </div>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="groups_order"><?php echo translate("Order"); ?></label>
							<div class="col-md-12">
								<input class="form-control" type="number" id="groups_order" name="groups_order" value="1" >
							</div>
						</div>
					</div>
					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<div class="col-lg-10 col-md-10 col-sm-9 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="groups_activate" name="groups_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full">
			</div>
		</div>
	</div>
</div>

<!-- sort table (begin) -->
<div class="row">

    <div id="module-sort" class="col-lg-12">

		<div class="block block-themed" id="sort-content">
            <div class="block-header">
                <ul class="block-options">
					<li>
						<button type="button" class="btn-sort-close" title="<?php echo translate("Back to Data Table"); ?>" data-toggle="block-option"><i class="si si-list"></i> <?php echo translate("Table"); ?></button>
					</li>
                    <li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
                </ul>
                <h3 class="block-title"><?php echo translate("modules"); ?> <?php echo translate("Sort"); ?></h3>
            </div>

            <!-- Data Table -->
            <div class="block-content">

            	<ol class="sortable ui-sortable">
				</ol>

                <div class="row items-push">
					<div class="col-md-12 push-30-t push-30-b text-right">
						<button id="button-data-save-sort" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
					</div>
				</div>

            </div>

        </div>

	</div>

</div>
<!-- sort table (end) -->

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<li>
                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
                    </li>
                    <li>
                        <button class="btn-sort" type="button" title="<?php echo translate("Sort"); ?>"><i class="si si-shuffle" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Sort"); ?></span></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("Groups"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
				<div id="filter_parent" class="panel-group">
					<div class="panel panel-default">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#filter_parent" href="#filter">
							<div class="panel-heading">
								<span class="panel-title"><?php echo translate("Advanced Filter"); ?></span> <i class="fa fa-angle-down"></i>
							</div>
						</a>
						<div id="filter" class="panel-collapse collapse">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<form action="" method="get">
											<input type="hidden" value="<?php if (isset($_GET["search"]) && !empty($_GET["search"])) {echo $_GET["search"];}?>">
											<div class="row items-push">
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-4 push-5-t" for="published"><?php echo translate("Status"); ?></label>
														<div class="col-md-8">
															<?php
if (isset($_GET["published"])) {
    if ($_GET["published"] == "") {
        $status_all_selected = " selected";
        $status_published_selected = "";
        $status_unpublished_selected = "";
    } else if ($_GET["published"] == "1") {
        $status_all_selected = "";
        $status_published_selected = " selected";
        $status_unpublished_selected = "";
    } else if ($_GET["published"] == "0") {
        $status_all_selected = "";
        $status_published_selected = "";
        $status_unpublished_selected = " selected";
    } else {
        $status_all_selected = " selected";
        $status_published_selected = "";
        $status_unpublished_selected = "";
    }
} else {
    $status_all_selected = " selected";
    $status_published_selected = "";
    $status_unpublished_selected = "";
}
?>
															<select name="published" aria-controls="datatable" class="form-control">
																<option value="" <?php echo $status_all_selected; ?>><?php echo translate("All"); ?></option>
																<option value="" disabled>-</option>
																<option value="1" <?php echo $status_published_selected; ?>><?php echo translate("Published"); ?></option>
																<option value="0" <?php echo $status_unpublished_selected; ?>><?php echo translate("Unpublished"); ?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="from"><?php echo translate("From"); ?></label>
														<div class="col-md-9">
															<input class="form-control from" type="text" data-role="date" id="from" name="from" value="<?php if (isset($_GET["from"]) && !empty($_GET["from"])) {echo $_GET["from"];}?>">
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="to"><?php echo translate("To"); ?></label>
														<div class="col-md-9">
															<input class="form-control to" type="text" data-role="date" id="to" name="to" value="<?php if (isset($_GET["to"]) && !empty($_GET["to"])) {echo $_GET["to"];}?>">
														</div>
													</div>
												</div>
												<div class="col-md-3 text-right">
													<div class="form-group">
														<div class="col-xs-6">
															<button id="button-filter-submit" class="btn btn-sm btn-primary" type="submit" style="width: 100%;"><?php echo translate("Show"); ?></button>
														</div>
														<div class="col-xs-6">
															<button id="button-filter-reset" class="btn btn-sm btn-default" type="reset" onclick="location.href = '<?php echo $module_page_link; ?>'" style="width: 100%;"><?php echo translate("Reset"); ?></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            	<?php
$data_table_info = prepare_groups_table_defer($table_module_field);
echo create_groups_table($data_table_info["values"], $table_module_field);
?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->

<?php
include "templates/" . $configs["backend_template"] . "/container-footer.php";
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->

<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "<script src=\"plugins/bootstrap-datepicker/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js\"></script>";
}
?>
<script src="plugins/nestedsort/jquery.mjs.nestedSortable.js"></script>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo htmlspecialchars($module_function_page); ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;
</script>

<script>

function reset_groups_data() {

	$("#groups_id").val("");
	$("#groups_name").val("");
	$("#groups_protected").prop("checked", false);
	$("#modules_ids").each(function () {
        $(this).select2("val", "")
    });
	$("#groups_order").val(<?php echo $data_table_info["count_true"]; ?>);
	$("#groups_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#groups_action").val("");

}

function update_groups_data(target_id) {
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_ids_data_dynamic_list",
			parameters: modules_ids_filters
		},
		success: function(response) {
			var modules_ids = $("#modules_ids").val();
			$("#modules_ids").empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_ids").append("<option value=\"" + response.values[i]['modules_id'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_id'] + ")</option>");
				}
				$("#modules_ids").val(modules_ids);
			}
		}
	});
}

function submit_groups_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);

    if ($("#groups_id").val() == "") {
		var method = "create_groups_data";
	} else {
		var method = "update_groups_data";
	}

	if ($("#groups_activate").is(":checked") === true){
		var groups_activate = "1";
	} else {
		var groups_activate = "0";
	}
	if ($("#groups_protected").is(":checked") === true){
		var groups_protected = groups_protected_default_value[0];
	} else {
		var groups_protected = "";
	}







	var form_values   = {
		"groups_id": $("#groups_id").val().trim(),
		"groups_action": $("#groups_action").val().trim(),
		"groups_name": $("#groups_name").val(),
		"groups_protected": groups_protected,
		"modules_ids": $("#modules_ids").select2("val"),
		"groups_order": $("#groups_order").val().trim(),

		"users_id": $("#users_id").val().trim(),
		"users_username": $("#users_username").val().trim(),
		"users_name": $("#users_name").val().trim(),
		"users_last_name": $("#users_last_name").val().trim(),
		"groups_activate": groups_activate.trim(),
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				if (method == "create_groups_data") {
					create_groups_table_row(response.values);
					show_groups_modal_response(response.status, response.message);
					reset_groups_data();
					$("#form-content").slideUp("fast");
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");

					var limit = $("#datatable_length_selector").val();
					var page = get_url_param().page;
					if (page == null) {
						page = 1;
					}
					var search_text = $("#datatable_search").val();
					var published = get_url_param().published;
					var from = get_url_param().from;
					var to = get_url_param().to;
					if (limit != null) {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
							}
						}
					} else {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>");
							}
						}
					}
					
            		$("#button-data-submit").prop("disabled", false);
				} else if (method == "update_groups_data") {
					update_groups_table_row(response.values);
				    show_groups_modal_response(response.status, response.message);
					update_groups_data($("#groups_id").val());
					edit_groups_open($("#groups_id").val());
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
            		$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
            	$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function delete_groups_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_groups_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				delete_groups_table_row(target_id);
				if ($("#groups_id").val() == target_id) {
					reset_groups_data();
					form_groups_close();
				}
                show_groups_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus);
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function form_groups_close() {

	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}

}
function create_groups_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add") . " " . $title; ?>");

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

    var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_groups_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#groups_action").val("create");

}

function edit_groups_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_groups_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_groups_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit") . " " . $title; ?>");

				$("#groups_id").val(response.values["groups_id"]);
				$("#groups_name").val(response.values["groups_name"]);
				$("#groups_name_stringlength").empty().append($("#groups_name").val().length + "/100");
				$("#groups_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#groups_name").val().length < 240) {
					$("#groups_name_stringlength").addClass("text-success");
				} else {
					$("#groups_name_stringlength").addClass("text-danger");
				}
				if(response.values["groups_protected"] == groups_protected_default_value[0]){
					$("#groups_protected").prop("checked", true);
				} else {
					$("#groups_protected").prop("checked", false);
				}
				$("#modules_ids").select2("val", response.values["modules_ids"]);
				$("#groups_order").val(response.values["groups_order"]);
				if(response.values["groups_activate"] == "1" ){
					$("#groups_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#groups_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["groups_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

				$("#groups_action").val("edit");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&groups_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&groups_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&groups_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&groups_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&groups_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&groups_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_groups_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {

					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_groups_modal_prompt($(this).attr("data-content"), "revision_groups_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function copy_groups_open(target_id) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	reset_groups_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_groups_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy") . " " . $title; ?>");

				$("#groups_id").val("");
				$("#groups_name").val(response.values["groups_name"]);
				$("#groups_name_stringlength").empty().append($("#groups_name").val().length + "/100");
				$("#groups_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#groups_name").val().length < 240) {
					$("#groups_name_stringlength").addClass("text-success");
				} else {
					$("#groups_name_stringlength").addClass("text-danger");
				}
				if(response.values["groups_protected"] == groups_protected_default_value[0]){
					$("#groups_protected").prop("checked", true);
				} else {
					$("#groups_protected").prop("checked", false);
				}
				$("#modules_ids").select2("val", response.values["modules_ids"]);
				$("#groups_order").val(response.values["groups_order"]);
				if(response.values["groups_activate"] == "1" ){
					$("#groups_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#groups_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");

				$("#groups_action").val("copy");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&groups_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&groups_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&groups_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&groups_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&groups_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&groups_id=" + target_id);
						}
					}
				}
			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function view_groups_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_groups_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_groups_modal_response(response.status, response.html);

				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_groups_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_groups_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_groups_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit") . " " . $title; ?>");

				$("#groups_id").val(response.values["groups_id"]);
				$("#groups_name").val(response.values["groups_name"]);
				$("#groups_name_stringlength").empty().append($("#groups_name").val().length + "/100");
				$("#groups_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#groups_name").val().length < 240) {
					$("#groups_name_stringlength").addClass("text-success");
				} else {
					$("#groups_name_stringlength").addClass("text-danger");
				}
				if(response.values["groups_protected"] == groups_protected_default_value[0]){
					$("#groups_protected").prop("checked", true);
				} else {
					$("#groups_protected").prop("checked", false);
				}
				$("#modules_ids").select2("val", response.values["modules_ids"]);
				$("#groups_order").val(response.values["groups_order"]);
				if(response.values["groups_activate"] == "1" ){
					$("#groups_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#groups_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["groups_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_groups_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}

function sort_groups_open() {

	$("#sort-content").css("visibility", "visible");
	$("#sort-content").fadeTo("slow", 1);
	$("#sort-content").removeClass("block-opt-hidden");
	$("#sort-content").fadeIn("slow").slideDown("fast");
	$("#sort-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_sort_groups",
			table_module_field: table_module_field
		},
		success: function(response) {

			$("#module-table").slideUp("fast");
			$("#sort-content").slideDown("fast");
			$("#module-sort").find(".sortable").empty();
			$("#module-sort").find(".sortable").append(response.html);

			$("ol.sortable").nestedSortable({
				forcePlaceholderSize: true,
				handle: "div",
				helper:	"clone",
				items: "li",
				opacity: .6,
				placeholder: "placeholder",
				revert: 250,
				tabSize: 25,
				tolerance: "pointer",
				toleranceElement: "> div",
				maxLevels: 1,

				isTree: true,
				expandOnHover: 700,
				startCollapsed: true
			});

			$(".disclose").on("click", function() {
				$(this).closest("li").toggleClass("mjs-nestedSortable-collapsed").toggleClass("mjs-nestedSortable-expanded");
			});

			$("#sort-content").removeClass("block-opt-refresh");

		}
	});

	$("html, body").animate({
            scrollTop: $("#module-sort").offset().top - 160 + "px"
    }, "fast");

}

function save_sort_groups () {

	$("#sort-content").addClass("block-opt-refresh");

	$("#button-data-submit").prop("disabled", true);

	arraied = $("ol.sortable").nestedSortable("toArray", {startDepthCount: 0});

	$.ajax({
		type: "POST",
		url: url,
		data: {
			"method" : "sort_groups",
			"parameters" : arraied
		},
		dataType: "json",
		success: function(response) {
			if (response.status == true) {
				$("#module-sort").find(".sortable").empty();
				show_groups_modal_response(response.status, response.message);
				$("#sort-content").slideUp("fast");
				$("#module-table").slideDown("fast");
			} else {
				show_groups_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
			}
			$("#button-data-submit").prop("disabled", false);
			$("#sort-content").removeClass("block-opt-refresh");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_groups_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#sort-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		}
	});


}

function sort_groups_close() {

	$("#module-sort").find(".sortable").empty();
	$("#sort-content").fadeTo("fast", 0);
	$("#sort-content").slideUp("fast").fadeOut("slow");
	$("#module-table").slideDown("fast");

}

function create_groups_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: <?php if (isset($configs["page_limit"]) && !empty($configs["page_limit"])) {echo $configs["page_limit"];} else {echo "20";}?>,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_groups_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "" && search_text != null) {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "" && search_text != null) {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_groups_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_groups_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_groups_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			if ($("#datatable-" + data_table["groups_id"]).length > 0) {
				$("#datatable").find("#datatable-" + data_table["groups_id"]).remove();
			}
			$("#datatable-list").prepend(response.html);
			if (count_data_table < datatable_data_limit) {
				create_groups_table();
			} else {
				create_groups_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_groups_open", $(this).attr("data-target"));
				} else {
					edit_groups_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_groups_open", $(this).attr("data-target"));
				} else {
					copy_groups_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_groups_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_groups_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_groups_modal_prompt($(this).attr("data-content"), "delete_groups_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_groups_open($(this).attr("data-target"));
			});
		}
	});

}

function update_groups_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_groups_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			$("#datatable").find("#datatable-" + data_table["groups_id"]).empty();
			$("#datatable").find("#datatable-" + data_table["groups_id"]).append(response.html);
			if (count_data_table < datatable_data_limit) {
				create_groups_table();
			} else {
				create_groups_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_groups_open", $(this).attr("data-target"));
				} else {
					edit_groups_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_groups_open", $(this).attr("data-target"));
				} else {
					copy_groups_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_groups_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_groups_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_groups_modal_prompt($(this).attr("data-content"), "delete_groups_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_groups_open($(this).attr("data-target"));
			});
		}
	});

}

function delete_groups_table_row(field_id) {
	mainDataTable.fnDestroy();
	$("#datatable").find("#datatable-" + field_id).remove();
	if (count_data_table < datatable_data_limit) {
		create_groups_table();
	} else {
		create_groups_table_defer();
	}
}

function show_groups_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_groups_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {

	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_groups_open", $(this).attr("data-target"));
		} else {
			create_groups_open($(this).attr("data-target"));
		}
	});

	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_groups_open", $(this).attr("data-target"));
		} else {
			edit_groups_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_groups_open", $(this).attr("data-target"));
		} else {
			copy_groups_open($(this).attr("data-target"));
		}
	});

	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_groups_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
		} else {
			translate_groups_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});

	$(".btn-delete").click(function () {
		show_groups_modal_prompt($(this).attr("data-content"), "delete_groups_data", $(this).attr("data-target"));
	});

	$(".btn-view").click(function () {
		view_groups_open($(this).attr("data-target"));
	});

	$(".btn-form-close").click(function () {
		show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_groups_close", "");
	});

	$(".btn-sort").click(function () {
		sort_groups_open();
	});

	$(".btn-sort-close").click(function () {
		show_groups_modal_prompt("<?php echo translate("Are you sure you want to leave current sort") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "sort_groups_close", "");
	});

	$("#button-data-save-sort").click(function(){
		save_sort_groups();
	});

	$("#from").datepicker({
		<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "language: \"" . $configs["backend_language"] . "\",";
}
?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$("#to").datepicker({
		<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "language: \"" . $configs["backend_language"] . "\",";
}
?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});

	$("#groups_activate").click(function () {
		if ($("#groups_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});


	$("#groups_name").keyup(function(event){
		$("#groups_name_stringlength").empty().append($(this).val().length + "/100");
		$("#groups_name_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#groups_name_stringlength").addClass("text-success");
		} else {
			$("#groups_name_stringlength").addClass("text-danger");
		}
	});
	$("#modules_ids").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_ids_data_dynamic_list",
			filters: modules_ids_filters
		},
		success: function(response) {
			$("#modules_ids-check-loading").fadeOut("fast");
			$("#modules_ids").empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_ids").append("<option value=\"" + response.values[i]['modules_id'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_id'] + ")</option>");
				}
				$("#modules_ids").each(function () {
					$(this).select2("val", "")
				});
			}
		}
	});

	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_groups_table();
	} else {
		create_groups_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_groups_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_groups_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_groups_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().groups_id != null) {
			edit_groups_open(get_url_param().groups_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().groups_id != null) {
			copy_groups_open(get_url_param().groups_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().groups_id != null) {
			translate_groups_open(get_url_param().groups_id, get_url_param().groups_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().groups_id != null) {
			view_groups_open(get_url_param().groups_id);
		}
	}
	/* URL response (end) */


	if ($("#groups_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}

	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");

});
</script>
<!-- initialization: JavaScript (end) -->

<script>
/*
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");

jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_groups_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"groups_name": {
					required: true,
				},
				"modules_ids": {
					required: false,
				},
			 },
			 messages: {

				"groups_name": {
					required: "<strong><?php echo translate("Name"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_ids": {
					required: "<strong><?php echo translate("Modules"); ?></strong> <?php echo translate("is required"); ?>",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once "templates/" . $configs["backend_template"] . "/footer.php";
?>
