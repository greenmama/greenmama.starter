<?php 
require_once("system/include.php");

/* configurations: Abstract Spread Inputs */
$abstract_spreadinputs = get_modules_data_by_id("3");
if (isset($abstract_spreadinputs) && !empty($abstract_spreadinputs)) {
	$title = translate($abstract_spreadinputs["modules_name"]);
	$module_page = $abstract_spreadinputs["modules_link"];
	$module_page_link = backend_rewrite_url($abstract_spreadinputs["modules_link"]);
	$module_function_page = htmlspecialchars("system/".$abstract_spreadinputs["modules_function_link"]);
	require_once($module_function_page);
} else {
	$title = translate("Abstract Spread Inputs");
	$module_page = "abstract-spread-inputs.php";
	$module_page_link = backend_rewrite_url("abstract-spread-inputs.php");
	$module_function_page = htmlspecialchars("system/core/".$configs["version"]."/abstract-spread-inputs.php");
	require_once("system/core/".$configs["version"]."/abstract-spread-inputs.php");
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Abstract Spread Inputs */
if (!authentication_session_users()) {
	authentication_deny();
}
if (!authentication_session_modules($module_page)) {
	authentication_permission_deny();
}

if (isset($abstract_spreadinputs["modules_datatable_field"]) && !empty($abstract_spreadinputs["modules_datatable_field"])) {
	$table_module_field = array();
	for ($i = 0; $i < count($abstract_spreadinputs["modules_datatable_field"]); $i++) {
		if ($abstract_spreadinputs["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
			$abstract_spreadinputs_modules_datatable_field_display = true;
		} else {
			$abstract_spreadinputs_modules_datatable_field_display = false;
		}
		$table_module_field[$abstract_spreadinputs["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $abstract_spreadinputs_modules_datatable_field_display;
	}
} else {
	$table_module_field = array(
	"abstract_spreadinputs_id" => true,
	"abstract_spreadinputs_references" => true,
	"abstract_spreadinputs_label" => true,
	"abstract_spreadinputs_type" => true,
	"abstract_spreadinputs_varname" => false,
	"abstract_spreadinputs_placeholder" => false,
	"abstract_spreadinputs_help" => false,
	"abstract_spreadinputs_require" => false,
	"abstract_spreadinputs_readonly" => false,
	"abstract_spreadinputs_disable" => false,
	"abstract_spreadinputs_hidden" => false,
	"abstract_spreadinputs_validate_string_min" => false,
	"abstract_spreadinputs_validate_string_max" => false,
	"abstract_spreadinputs_validate_number_min" => false,
	"abstract_spreadinputs_validate_number_max" => false,
	"abstract_spreadinputs_validate_date_min" => false,
	"abstract_spreadinputs_validate_date_max" => false,
	"abstract_spreadinputs_validate_datetime_min" => false,
	"abstract_spreadinputs_validate_datetime_max" => false,
	"abstract_spreadinputs_validate_password_equal_to" => false,
	"abstract_spreadinputs_validate_email" => false,
	"abstract_spreadinputs_validate_password" => false,
	"abstract_spreadinputs_validate_website" => false,
	"abstract_spreadinputs_validate_no_space" => false,
	"abstract_spreadinputs_validate_no_specialchar_soft" => false,
	"abstract_spreadinputs_validate_no_specialchar_hard" => false,
	"abstract_spreadinputs_validate_upper" => false,
	"abstract_spreadinputs_validate_lower" => false,
	"abstract_spreadinputs_validate_number" => false,
	"abstract_spreadinputs_validate_digit" => false,
	"abstract_spreadinputs_validate_unique" => false,
	"abstract_spreadinputs_prefix" => false,
	"abstract_spreadinputs_suffix" => false,
	"abstract_spreadinputs_default_value_type" => false,
	"abstract_spreadinputs_default_value" => false,
	"abstract_spreadinputs_default_switch" => false,
	"abstract_spreadinputs_input_list" => false,
	"abstract_spreadinputs_input_list_static_value" => false,
	"abstract_spreadinputs_input_list_dynamic_module" => false,
	"abstract_spreadinputs_input_list_dynamic_id_column" => false,
	"abstract_spreadinputs_input_list_dynamic_value_column" => false,
	"abstract_spreadinputs_file_selector_type" => false,
	"abstract_spreadinputs_date_format" => false,
	"abstract_spreadinputs_color_format" => false,
	"abstract_spreadinputs_tags_format" => false,
	"abstract_spreadinputs_image_folder" => false,
	"abstract_spreadinputs_image_width" => false,
	"abstract_spreadinputs_image_height" => false,
	"abstract_spreadinputs_image_width_ratio" => false,
	"abstract_spreadinputs_image_height_ratio" => false,
	"abstract_spreadinputs_gridWidth" => false,
	"abstract_spreadinputs_alignment" => false,
	"abstract_spreadinputs_date_created" => true,
	"users_id" => false,
	"users_username" => false,
	"users_name" => true,
	"users_last_name" => false,
	"abstract_spreadinputs_activate" => true,
	"abstract_spreadinputs_actions" => true,
);
}

require_once("templates/".$configs["backend_template"]."/header.php");
require_once("templates/".$configs["backend_template"]."/overlay.php");
require_once("templates/".$configs["backend_template"]."/sidebar.php");
require_once("templates/".$configs["backend_template"]."/navbar.php");
require_once("templates/".$configs["backend_template"]."/container-header.php");
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header bg-mama-green">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo htmlspecialchars($module_function_page); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="abstract_spreadinputs">
					<input type="hidden" id="abstract_spreadinputs_id" name="abstract_spreadinputs_id">
					<input type="hidden" id="abstract_spreadinputs_action" name="abstract_spreadinputs_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_label"><?php echo translate("Spread Input Label"); ?><span class="text-danger">*</span> <span id="abstract_spreadinputs_label_stringlength" class="text-success pull-right push-10-l">0/50</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_label" type="text" id="abstract_spreadinputs_label" name="abstract_spreadinputs_label" value="" placeholder="<?php echo translate("Label of reference");?>" >
								
								<div class="help-block"><?php echo translate("Identify module"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_varname"><?php echo translate("Spread Input Variable Name"); ?><span id="abstract_spreadinputs_varname_stringlength" class="text-success pull-right push-10-l">0/50</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_varname" type="text" id="abstract_spreadinputs_varname" name="abstract_spreadinputs_varname" value="" placeholder="<?php echo translate("Variable name of reference");?>" >
								
								<div class="help-block"><?php echo translate("Leave this field blank to let system generate from label automatically"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_type"><?php echo translate("Spread Input Type"); ?><span class="text-danger">*</span> </label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_type" id="abstract_spreadinputs_type" name="abstract_spreadinputs_type" >
									<option value="input_Text">Text Box</option>
									<option value="textarea">Text Area</option>
									<option value="textEditor">Text Editor</option>
									<option value="input_Number">Number</option>
									<option value="input_Password">Password</option>
									<option value="select">Select Box</option>
									<option value="select_js">Select Box (Searchable)</option>
									<option value="multiSelect">Multiple Select Box</option>
									<option value="multiSelect_js_tags">Multiple Select Box (Searchable)</option>
									<option value="radio">Radio</option>
									<option value="radio-inline">Radio (Inline)</option>
									<option value="checkbox">Checkbox</option>
									<option value="checkbox-inline">Checkbox (Inline)</option>
									<option value="checkbox_radio">Checkbox (Like Switch))</option>
									<option value="switch">Switch</option>
									<option value="file_Input_Simple">File Uploader</option>
									<option value="Multiple_FileInputSimple">Multiple Files Uploader</option>
									<option value="file_selector">File Selector</option>
									<option value="image_upload">Image Uploader</option>
									<option value="input_Date">Date</option>
									<option value="input_DateTime">Date Time</option>
									<option value="input_Time">Time</option>
									<option value="input_Tags">Tags</option>
									<option value="input_Color">Color</option>
									<option value="range_slider">Range Slider</option>
									<option value="range_slider_double">Range Slider (Double)</option>
								</select>
								<div id="abstract_spreadinputs_type-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select type of reference"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_references"><?php echo translate("Reference"); ?><span class="text-danger">*</span>  <a href="<?php echo backend_rewrite_url("abstract-references.php"); ?>" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Reference"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Reference"); ?>"><i class="si si-settings"></i></a></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_references" id="abstract_spreadinputs_references" name="abstract_spreadinputs_references" >
								
									<?php
									/* PHP variable for filter of dynamic selector "abstract_spreadinputs_references"*/
									/* Example: 
									$abstract_spreadinputs_references_filters = array(
										"user_id" => "",
										"languages_short_name" => "",
									);
									*/
									$abstract_spreadinputs_references_filters = "";
									/* PHP variable for extended command of dynamic selector "abstract_spreadinputs_references"*/
									/* Example: 
									$abstract_spreadinputs_references_extended_command = array(
										array(
											"conjunction" => "AND",
											"key" => "user_name",
											"operator" => "LIKE",
											"value" => "%Michael%",
										),
										array(
											"conjunction" => "OR",
											"key" => "user_name",
											"operator" => "LIKE",
											"value" => "%Ammy%",
										),
									)
									*/
									$abstract_spreadinputs_references_extended_command = "";
									?>
									<script>
									/* JavaScript variable for filter of dynamic selector "abstract_spreadinputs_references" */
									var abstract_spreadinputs_references_filters = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_references_filters)); ?>");
									</script>
									<?php 
									$count_abstract_spreadinputs_references = count_abstract_spreadinputs_references_data_dynamic_list("", "", "", "1", $abstract_spreadinputs_references_filters, $abstract_spreadinputs_references_extended_command);
									if ($count_abstract_spreadinputs_references <= $configs["datatable_data_limit"]) {
									?>
									<option value=""><?php echo translate(""); ?></option>
									<option label="seperator" value="" disabled>-</option>
									<?php
									$abstract_spreadinputs_references = get_abstract_spreadinputs_references_data_dynamic_list("", "", "", "1", $abstract_spreadinputs_references_filters, $abstract_spreadinputs_references_extended_command);
									for($i = 0; $i < count($abstract_spreadinputs_references); $i++){
										?>
											<option value="<?php echo htmlspecialchars($abstract_spreadinputs_references[$i]['abstract_references_id']); ?>">
												<?php echo htmlspecialchars($abstract_spreadinputs_references[$i]['abstract_references_label'])." (ID: ".$abstract_spreadinputs_references[$i]['abstract_references_id'].")"; ?>
											</option>
									<?php
									}
									?>
									<?php
									} else {
									?>
										<option value=""><?php echo translate("Loading");?>...</option>
									<?php 
									}
									?>
									
								</select>
								<div id="abstract_spreadinputs_references-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select module to use this reference"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_placeholder"><?php echo translate("Spread Input Placeholder"); ?><span id="abstract_spreadinputs_placeholder_stringlength" class="text-success pull-right push-10-l">0/100</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_placeholder" type="text" id="abstract_spreadinputs_placeholder" name="abstract_spreadinputs_placeholder" value="" placeholder="<?php echo translate("Placeholder of input");?>" >
								
								<div class="help-block"><?php echo translate("Guide users how data should be filled"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_help"><?php echo translate("Spread Input Help"); ?><span id="abstract_spreadinputs_help_stringlength" class="text-success pull-right push-10-l">0/200</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_help" type="text" id="abstract_spreadinputs_help" name="abstract_spreadinputs_help" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Help appears under input area"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_require_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_require" */
					var abstract_spreadinputs_require_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_require_default_value)); ?>");
					</script>
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_require"><?php echo translate("Require"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_require" name="abstract_spreadinputs_require" class="abstract_spreadinputs_require" type="checkbox" value="<?php echo $abstract_spreadinputs_require_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Users need to fill data"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_readonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_readonly" */
					var abstract_spreadinputs_readonly_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_readonly_default_value)); ?>");
					</script>
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_readonly"><?php echo translate("Read-only"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_readonly" name="abstract_spreadinputs_readonly" class="abstract_spreadinputs_readonly" type="checkbox" value="<?php echo $abstract_spreadinputs_readonly_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Input is read-only"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_disable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_disable" */
					var abstract_spreadinputs_disable_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_disable_default_value)); ?>");
					</script>
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_disable"><?php echo translate("Disabled"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_disable" name="abstract_spreadinputs_disable" class="abstract_spreadinputs_disable" type="checkbox" value="<?php echo $abstract_spreadinputs_disable_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Input is disabled"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_hidden_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_hidden" */
					var abstract_spreadinputs_hidden_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_hidden_default_value)); ?>");
					</script>
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_hidden"><?php echo translate("Hidden"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_hidden" name="abstract_spreadinputs_hidden" class="abstract_spreadinputs_hidden" type="checkbox" value="<?php echo $abstract_spreadinputs_hidden_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Input is hidden"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_string_min"><?php echo translate("Validate Minimum Strings"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_string_min" type="number" id="abstract_spreadinputs_validate_string_min" name="abstract_spreadinputs_validate_string_min" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Minimum length of strings (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_string_max"><?php echo translate("Validate Maximum String"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_string_max" type="number" id="abstract_spreadinputs_validate_string_max" name="abstract_spreadinputs_validate_string_max" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Maximum length of strings (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_number_min"><?php echo translate("Validate Minimum Number"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_number_min" type="number" id="abstract_spreadinputs_validate_number_min" name="abstract_spreadinputs_validate_number_min" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Minimum value of number (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_number_max"><?php echo translate("Validate Maximum Number"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_number_max" type="number" id="abstract_spreadinputs_validate_number_max" name="abstract_spreadinputs_validate_number_max" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Maximum value of number (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_date_min"><?php echo translate("Validate Minimum Date"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_date_min" type="text" data-role="date" id="abstract_spreadinputs_validate_date_min" name="abstract_spreadinputs_validate_date_min" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Minimum date (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_date_max"><?php echo translate("Validate Maximum Date"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_date_max" type="text" data-role="date" id="abstract_spreadinputs_validate_date_max" name="abstract_spreadinputs_validate_date_max" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Maximum date (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_datetime_min"><?php echo translate("Validate Minimum Date Time"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_datetime_min" type="text" data-role="date" id="abstract_spreadinputs_validate_datetime_min" name="abstract_spreadinputs_validate_datetime_min" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Minimum date (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_datetime_max"><?php echo translate("Validate Maximum Date Time"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_datetime_max" type="text" data-role="date" id="abstract_spreadinputs_validate_datetime_max" name="abstract_spreadinputs_validate_datetime_max" value="" placeholder="<?php echo translate("Enable");?>" >
								
								<div class="help-block"><?php echo translate("Maximum date (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_password_equal_to"><?php echo translate("Password Equal To Element"); ?><span id="abstract_spreadinputs_validate_password_equal_to_stringlength" class="text-success pull-right push-10-l">0/0</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_validate_password_equal_to" type="text" id="abstract_spreadinputs_validate_password_equal_to" name="abstract_spreadinputs_validate_password_equal_to" value="" placeholder="<?php echo translate("Variable name of element to validate password");?>" >
								
								<div class="help-block"><?php echo translate("Validate matching password (Leave this blank to disable validation)"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_email_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_email" */
					var abstract_spreadinputs_validate_email_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_email_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_email"><?php echo translate("Validate Email"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_email" name="abstract_spreadinputs_validate_email" class="abstract_spreadinputs_validate_email" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_email_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is email"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_password_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_password" */
					var abstract_spreadinputs_validate_password_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_password_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_password"><?php echo translate("Validate Password"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_password" name="abstract_spreadinputs_validate_password" class="abstract_spreadinputs_validate_password" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_password_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value contains uppercase strings, lowercase strings, number and special characters"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_website_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_website" */
					var abstract_spreadinputs_validate_website_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_website_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_website"><?php echo translate("Validate Website (URL)"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_website" name="abstract_spreadinputs_validate_website" class="abstract_spreadinputs_validate_website" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_website_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is website URL"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_no_space_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_no_space" */
					var abstract_spreadinputs_validate_no_space_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_no_space_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_no_space"><?php echo translate("Validate Not Allow Spaces"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_no_space" name="abstract_spreadinputs_validate_no_space" class="abstract_spreadinputs_validate_no_space" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_no_space_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value has no spaces"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_no_specialchar_soft_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_no_specialchar_soft" */
					var abstract_spreadinputs_validate_no_specialchar_soft_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_no_specialchar_soft_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_no_specialchar_soft"><?php echo translate("Validate Not Allow Special Characters (Soft)"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_no_specialchar_soft" name="abstract_spreadinputs_validate_no_specialchar_soft" class="abstract_spreadinputs_validate_no_specialchar_soft" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_no_specialchar_soft_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value has no special characters ( _ , -  is acceptable)"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_no_specialchar_hard_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_no_specialchar_hard" */
					var abstract_spreadinputs_validate_no_specialchar_hard_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_no_specialchar_hard_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_no_specialchar_hard"><?php echo translate("Validate Not Allow Special Characters (Hard)"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_no_specialchar_hard" name="abstract_spreadinputs_validate_no_specialchar_hard" class="abstract_spreadinputs_validate_no_specialchar_hard" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_no_specialchar_hard_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value has no special characters"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_upper_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_upper" */
					var abstract_spreadinputs_validate_upper_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_upper_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_upper"><?php echo translate("Validate Uppercase Characters"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_upper" name="abstract_spreadinputs_validate_upper" class="abstract_spreadinputs_validate_upper" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_upper_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is uppercase characters"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_lower_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_lower" */
					var abstract_spreadinputs_validate_lower_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_lower_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_lower"><?php echo translate("Validate Lowercase Characters"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_lower" name="abstract_spreadinputs_validate_lower" class="abstract_spreadinputs_validate_lower" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_lower_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is lowercase characters"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_number_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_number" */
					var abstract_spreadinputs_validate_number_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_number_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_number"><?php echo translate("Validate Number"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_number" name="abstract_spreadinputs_validate_number" class="abstract_spreadinputs_validate_number" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_number_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is number"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_digit_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_digit" */
					var abstract_spreadinputs_validate_digit_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_digit_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_digit"><?php echo translate("Validate Digit"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_digit" name="abstract_spreadinputs_validate_digit" class="abstract_spreadinputs_validate_digit" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_digit_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is digit"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_validate_unique_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_validate_unique" */
					var abstract_spreadinputs_validate_unique_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_validate_unique_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_validate_unique"><?php echo translate("Validate Unique"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_validate_unique" name="abstract_spreadinputs_validate_unique" class="abstract_spreadinputs_validate_unique" type="checkbox" value="<?php echo $abstract_spreadinputs_validate_unique_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Validate if value is unique in module"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_prefix"><?php echo translate("Prefix"); ?><span id="abstract_spreadinputs_prefix_stringlength" class="text-success pull-right push-10-l">0/100</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_prefix" type="text" id="abstract_spreadinputs_prefix" name="abstract_spreadinputs_prefix" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Specify prefix strings of data"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_suffix"><?php echo translate("Suffix"); ?><span id="abstract_spreadinputs_suffix_stringlength" class="text-success pull-right push-10-l">0/100</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_suffix" type="text" id="abstract_spreadinputs_suffix" name="abstract_spreadinputs_suffix" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Specify suffix strings for data"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_default_value_type"><?php echo translate("Default Value Type"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_default_value_type" id="abstract_spreadinputs_default_value_type" name="abstract_spreadinputs_default_value_type" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="static">Static</option>
												<option value="dynamic">Dynamic</option>
								</select>
								<div id="abstract_spreadinputs_default_value_type-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select default value type"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_default_value"><?php echo translate("Default Values"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_default_value" type="text" id="abstract_spreadinputs_default_value" name="abstract_spreadinputs_default_value" value="<?php $abstract_spreadinputs_default_value_default_value[0]; ?>" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Specify default value"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_spreadinputs_default_switch_default_value = unserialize(stripslashes("a:1:{i:0;s:9:\\\"unchecked\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_spreadinputs_default_switch" */
					var abstract_spreadinputs_default_switch_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_default_switch_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_default_switch"><?php echo translate("Default Switch/Checkbox (Like Switch)"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_spreadinputs_default_switch" name="abstract_spreadinputs_default_switch" class="abstract_spreadinputs_default_switch" type="checkbox" value="<?php echo $abstract_spreadinputs_default_switch_default_value[0];?>"><span></span> <?php echo translate("Checked"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Specify default status of switch"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_input_list"><?php echo translate("List Type"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_input_list" id="abstract_spreadinputs_input_list" name="abstract_spreadinputs_input_list" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="static">Static</option>
												<option value="dynamic">Dynamic</option>
								</select>
								<div id="abstract_spreadinputs_input_list-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select list type"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_input_list_static_value"><?php echo translate("Static Value for List"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_input_list_static_value" type="text" id="abstract_spreadinputs_input_list_static_value" name="abstract_spreadinputs_input_list_static_value" value="<?php $abstract_spreadinputs_input_list_static_value_default_value[0]; ?>" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Specify default value"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_input_list_dynamic_module"><?php echo translate("Module for Dynamic List"); ?> <a href="<?php echo backend_rewrite_url("modules.php"); ?>" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Module for Dynamic List"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Module for Dynamic List"); ?>"><i class="si si-settings"></i></a></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_input_list_dynamic_module" id="abstract_spreadinputs_input_list_dynamic_module" name="abstract_spreadinputs_input_list_dynamic_module" >
								
									<?php
									/* PHP variable for filter of dynamic selector "abstract_spreadinputs_input_list_dynamic_module"*/
									/* Example: 
									$abstract_spreadinputs_input_list_dynamic_module_filters = array(
										"user_id" => "",
										"languages_short_name" => "",
									);
									*/
									$abstract_spreadinputs_input_list_dynamic_module_filters = "";
									/* PHP variable for extended command of dynamic selector "abstract_spreadinputs_input_list_dynamic_module"*/
									/* Example: 
									$abstract_spreadinputs_input_list_dynamic_module_extended_command = array(
										array(
											"conjunction" => "AND",
											"key" => "user_name",
											"operator" => "LIKE",
											"value" => "%Michael%",
										),
										array(
											"conjunction" => "OR",
											"key" => "user_name",
											"operator" => "LIKE",
											"value" => "%Ammy%",
										),
									)
									*/
									$abstract_spreadinputs_input_list_dynamic_module_extended_command = "";
									?>
									<script>
									/* JavaScript variable for filter of dynamic selector "abstract_spreadinputs_input_list_dynamic_module" */
									var abstract_spreadinputs_input_list_dynamic_module_filters = $.parseJSON("<?php echo addslashes(json_encode($abstract_spreadinputs_input_list_dynamic_module_filters)); ?>");
									</script>
									<?php 
									$count_abstract_spreadinputs_input_list_dynamic_module = count_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list("", "", "", "1", $abstract_spreadinputs_input_list_dynamic_module_filters, $abstract_spreadinputs_input_list_dynamic_module_extended_command);
									if ($count_abstract_spreadinputs_input_list_dynamic_module <= $configs["datatable_data_limit"]) {
									?>
									<option value=""><?php echo translate(""); ?></option>
									<option label="seperator" value="" disabled>-</option>
									<?php
									$abstract_spreadinputs_input_list_dynamic_module = get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list("", "", "", "1", $abstract_spreadinputs_input_list_dynamic_module_filters, $abstract_spreadinputs_input_list_dynamic_module_extended_command);
									for($i = 0; $i < count($abstract_spreadinputs_input_list_dynamic_module); $i++){
										?>
											<option value="<?php echo htmlspecialchars($abstract_spreadinputs_input_list_dynamic_module[$i]['modules_db_name']); ?>">
												<?php echo htmlspecialchars($abstract_spreadinputs_input_list_dynamic_module[$i]['modules_name'])." (ID: ".$abstract_spreadinputs_input_list_dynamic_module[$i]['modules_db_name'].")"; ?>
											</option>
									<?php
									}
									?>
									<?php
									} else {
									?>
										<option value=""><?php echo translate("Loading");?>...</option>
									<?php 
									}
									?>
									
								</select>
								<div id="abstract_spreadinputs_input_list_dynamic_module-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select module for dynamic list"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_input_list_dynamic_id_column"><?php echo translate("Module ID Column Name for Dynamic List"); ?> <a href="<?php echo backend_rewrite_url("modules.php"); ?>" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Module ID Column Name for Dynamic List"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Module ID Column Name for Dynamic List"); ?>"><i class="si si-settings"></i></a></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_input_list_dynamic_id_column" id="abstract_spreadinputs_input_list_dynamic_id_column" name="abstract_spreadinputs_input_list_dynamic_id_column" >

									<script>
									var abstract_spreadinputs_input_list_dynamic_id_column_filters = "";
									</script>

									<option value=""><?php echo translate(""); ?></option>
									
								</select>
								<div id="abstract_spreadinputs_input_list_dynamic_id_column-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Specify name of ID column for dynamic list"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_input_list_dynamic_value_column"><?php echo translate("Module Value Column Name for Dynamic List"); ?> <a href="<?php echo backend_rewrite_url("modules.php"); ?>" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Module Value Column Name for Dynamic List"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Module Value Column Name for Dynamic List"); ?>"><i class="si si-settings"></i></a></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_input_list_dynamic_value_column" id="abstract_spreadinputs_input_list_dynamic_value_column" name="abstract_spreadinputs_input_list_dynamic_value_column" >

									<script>
									var abstract_spreadinputs_input_list_dynamic_value_column_filters = "";
									</script>

									<option value=""><?php echo translate(""); ?></option>
																		
								</select>
								<div id="abstract_spreadinputs_input_list_dynamic_value_column-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Specify name of ID column for dynamic list"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_file_selector_type"><?php echo translate("File Selector Type"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_file_selector_type" id="abstract_spreadinputs_file_selector_type" name="abstract_spreadinputs_file_selector_type" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="all">All</option>
												<option value="image">Image</option>
												<option value="video">Video</option>
								</select>
								<div id="abstract_spreadinputs_file_selector_type-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select type of file selector"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_date_format"><?php echo translate("Date Format"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_date_format" id="abstract_spreadinputs_date_format" name="abstract_spreadinputs_date_format" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="yyyy-mm-dd">YYYY-MM-DD</option>
												<option value="YYYY-MM-DD HH:mm:ss">YYYY-MM-DD HH:MM:SS</option>
								</select>
								<div id="abstract_spreadinputs_date_format-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select date format"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_color_format"><?php echo translate("Color Format"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_color_format" id="abstract_spreadinputs_color_format" name="abstract_spreadinputs_color_format" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="hex">hex</option>
												<option value="RGBa">RGBa</option>
												<option value="RGB">RGB</option>
								</select>
								<div id="abstract_spreadinputs_color_format-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select color format"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_tags_format"><?php echo translate("Tags Format"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_tags_format" id="abstract_spreadinputs_tags_format" name="abstract_spreadinputs_tags_format" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="comma">Comma</option>
												<option value="serialized">Serialized</option>
								</select>
								<div id="abstract_spreadinputs_tags_format-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Select tags format"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_image_folder"><?php echo translate("Image/File Folder Name"); ?><span id="abstract_spreadinputs_image_folder_stringlength" class="text-success pull-right push-10-l">0/200</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_image_folder" type="text" id="abstract_spreadinputs_image_folder" name="abstract_spreadinputs_image_folder" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Title of image folder"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_image_width"><?php echo translate("Image Width"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_image_width" type="number" id="abstract_spreadinputs_image_width" name="abstract_spreadinputs_image_width" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Number of image width (pixel)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_image_height"><?php echo translate("Image Height"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_image_height" type="number" id="abstract_spreadinputs_image_height" name="abstract_spreadinputs_image_height" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Number of image height (pixel)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_image_width_ratio"><?php echo translate("Image Width Ratio"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_image_width_ratio" type="number" id="abstract_spreadinputs_image_width_ratio" name="abstract_spreadinputs_image_width_ratio" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Number of image width ratio"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_image_height_ratio"><?php echo translate("Image Height Ratio"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_image_height_ratio" type="number" id="abstract_spreadinputs_image_height_ratio" name="abstract_spreadinputs_image_height_ratio" value="" placeholder="<?php echo translate("");?>" >
								
								<div class="help-block"><?php echo translate("Number of image height ratio"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_gridWidth"><?php echo translate("Grid Width for Input"); ?></label>
							<div class="col-md-12">
								<input class="form-control abstract_spreadinputs_gridWidth" type="text" id="abstract_spreadinputs_gridWidth" name="abstract_spreadinputs_gridWidth" value="" placeholder="<?php echo translate("");?>" data-min="1" data-max="12" >
								
								<div class="help-block"><?php echo translate("Based on Bootstrap grid (1-12)"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_alignment"><?php echo translate("Alignment for Input"); ?></label>
							<div class="col-md-12">
								<select class="form-control abstract_spreadinputs_alignment" id="abstract_spreadinputs_alignment" name="abstract_spreadinputs_alignment" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="left">Left</option>
												<option value="center">Center</option>
												<option value="right">Right</option>
								</select>
								<div id="abstract_spreadinputs_alignment-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate(""); ?></div>
							</div>
						</div>
					</div>
					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list"><i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span></a>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content");?></div>
                                        </div>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
	
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_spreadinputs_order"><?php echo translate("Order"); ?></label>
							<div class="col-md-12">
								<input class="form-control" type="number" id="abstract_spreadinputs_order" name="abstract_spreadinputs_order" value="1" >
							</div>
						</div>
					</div>
					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<div class="col-lg-10 col-md-10 col-sm-9 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="abstract_spreadinputs_activate" name="abstract_spreadinputs_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full">
			</div>
		</div>
	</div>
</div>

<!-- sort table (begin) -->
<div class="row">

    <div id="module-sort" class="col-lg-12">
		
		<div class="block block-themed" id="sort-content">
            <div class="block-header">
                <ul class="block-options">
					<li>
						<button type="button" class="btn-sort-close" title="<?php echo translate("Back to Data Table"); ?>" data-toggle="block-option"><i class="si si-list"></i> <?php echo translate("Table"); ?></button>
					</li>
                    <li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
                </ul>
                <h3 class="block-title"><?php echo translate("modules"); ?> <?php echo translate("Sort"); ?></h3>
            </div>

            <!-- Data Table -->
            <div class="block-content">
			
            	<ol class="sortable ui-sortable">
				</ol>

                <div class="row items-push">
					<div class="col-md-12 push-30-t push-30-b text-right">
						<button id="button-data-save-sort" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
					</div>
				</div>
                
            </div>

        </div>
		
	</div>
	
</div>
<!-- sort table (end) -->

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<li>
                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
                    </li>
                    <li>
                        <button class="btn-sort" type="button" title="<?php echo translate("Sort"); ?>"><i class="si si-shuffle" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Sort"); ?></span></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("Abstract Spread Inputs"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
				<div id="filter_parent" class="panel-group">
					<div class="panel panel-default">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#filter_parent" href="#filter">
							<div class="panel-heading">
								<span class="panel-title"><?php echo translate("Advanced Filter"); ?></span> <i class="fa fa-angle-down"></i>
							</div>
						</a>
						<div id="filter" class="panel-collapse collapse">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<form action="" method="get">
											<input type="hidden" value="<?php if (isset($_GET["search"]) && !empty($_GET["search"])) { echo $_GET["search"]; } ?>">
											<div class="row items-push">
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-4 push-5-t" for="published"><?php echo translate("Status"); ?></label>
														<div class="col-md-8">
															<?php
															if (isset($_GET["published"])) {
																if ($_GET["published"] == "") {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "1") {
																	$status_all_selected = "";
																	$status_published_selected = " selected";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "0") {
																	$status_all_selected = "";
																	$status_published_selected = "";
																	$status_unpublished_selected = " selected";
																} else {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																}
															} else {
																$status_all_selected = " selected";
																$status_published_selected = "";
																$status_unpublished_selected = "";
															}
															?>
															<select name="published" aria-controls="datatable" class="form-control">
																<option value="" <?php echo $status_all_selected; ?>><?php echo translate("All");?></option>
																<option value="" disabled>-</option>
																<option value="1" <?php echo $status_published_selected; ?>><?php echo translate("Published");?></option>
																<option value="0" <?php echo $status_unpublished_selected; ?>><?php echo translate("Unpublished");?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="from"><?php echo translate("From"); ?></label>
														<div class="col-md-9">
															<input class="form-control from" type="text" data-role="date" id="from" name="from" value="<?php if (isset($_GET["from"]) && !empty($_GET["from"])) { echo $_GET["from"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="to"><?php echo translate("To"); ?></label>
														<div class="col-md-9">
															<input class="form-control to" type="text" data-role="date" id="to" name="to" value="<?php if (isset($_GET["to"]) && !empty($_GET["to"])) { echo $_GET["to"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3 text-right">
													<div class="form-group">
														<div class="col-xs-6">
															<button id="button-filter-submit" class="btn btn-sm btn-primary" type="submit" style="width: 100%;"><?php echo translate("Show"); ?></button>
														</div>
														<div class="col-xs-6">
															<button id="button-filter-reset" class="btn btn-sm btn-default" type="reset" onclick="location.href = '<?php echo $module_page_link; ?>'" style="width: 100%;"><?php echo translate("Reset"); ?></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            	<?php
            	$data_table_info = prepare_abstract_spreadinputs_table_defer($table_module_field);
                echo create_abstract_spreadinputs_table($data_table_info["values"], $table_module_field);
            	?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->

<?php
include("templates/".$configs["backend_template"]."/container-footer.php");
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="plugins/ion-rangeslider/css/ion.rangeSlider.min.css" rel="stylesheet">
<link href="plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->
	
<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
<?php 
if (file_exists("../../../plugins/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
	echo "<script src=\"plugins/bootstrap-datepicker/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js\"></script>";
}
?>
<script src="plugins/nestedsort/jquery.mjs.nestedSortable.js"></script>
<script src="plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<?php 
if (file_exists("../../../plugins/bootstrap-datetimepicker/locale/" . $configs["backend_language"] . ".js")) {
	echo "<script src=\"plugins/bootstrap-datetimepicker/locale/" . $configs["backend_language"] . ".js\"></script>";
}
?>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo htmlspecialchars($module_function_page); ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;
</script>

<script>

function reset_abstract_spreadinputs_data() {

	$("#abstract_spreadinputs_id").val("");
	$("#abstract_spreadinputs_label").val("");
	$("#abstract_spreadinputs_varname").val("");
	$("#abstract_spreadinputs_type").val($("#abstract_spreadinputs_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_references").val($("#abstract_spreadinputs_references option:first").val()).trigger("change");
	$("#abstract_spreadinputs_placeholder").val("");
	$("#abstract_spreadinputs_help").val("");
	$("#abstract_spreadinputs_require").prop("checked", false);
	$("#abstract_spreadinputs_readonly").prop("checked", false);
	$("#abstract_spreadinputs_disable").prop("checked", false);
	$("#abstract_spreadinputs_hidden").prop("checked", false);
	$("#abstract_spreadinputs_validate_string_min").val("");
	$("#abstract_spreadinputs_validate_string_max").val("");
	$("#abstract_spreadinputs_validate_number_min").val("");
	$("#abstract_spreadinputs_validate_number_max").val("");
	$("#abstract_spreadinputs_validate_date_min").val("");
	$("#abstract_spreadinputs_validate_date_max").val("");
	$("#abstract_spreadinputs_validate_datetime_min").val("");
	$("#abstract_spreadinputs_validate_datetime_max").val("");
	$("#abstract_spreadinputs_validate_password_equal_to").val("");
	$("#abstract_spreadinputs_validate_email").prop("checked", false);
	$("#abstract_spreadinputs_validate_password").prop("checked", false);
	$("#abstract_spreadinputs_validate_website").prop("checked", false);
	$("#abstract_spreadinputs_validate_no_space").prop("checked", false);
	$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", false);
	$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", false);
	$("#abstract_spreadinputs_validate_upper").prop("checked", false);
	$("#abstract_spreadinputs_validate_lower").prop("checked", false);
	$("#abstract_spreadinputs_validate_number").prop("checked", false);
	$("#abstract_spreadinputs_validate_digit").prop("checked", false);
	$("#abstract_spreadinputs_validate_unique").prop("checked", false);
	$("#abstract_spreadinputs_prefix").val("");
	$("#abstract_spreadinputs_suffix").val("");
	$("#abstract_spreadinputs_default_value_type").val($("#abstract_spreadinputs_default_value_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_default_value").tagsInputDestroy();
	$("#abstract_spreadinputs_default_value").val("");
	$("#abstract_spreadinputs_default_value").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "",
		removeWithBackspace: true
	});
	$("#abstract_spreadinputs_default_switch").prop("checked", false);
	$("#abstract_spreadinputs_input_list").val($("#abstract_spreadinputs_input_list option:first").val()).trigger("change");
	$("#abstract_spreadinputs_input_list_static_value").tagsInputDestroy();
	$("#abstract_spreadinputs_input_list_static_value").val("");
	$("#abstract_spreadinputs_input_list_static_value").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "",
		removeWithBackspace: true
	});
	$("#abstract_spreadinputs_input_list_dynamic_module").val($("#abstract_spreadinputs_input_list_dynamic_module option:first").val()).trigger("change");
	$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
	$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
	$("#abstract_spreadinputs_file_selector_type").val($("#abstract_spreadinputs_file_selector_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_date_format").val($("#abstract_spreadinputs_date_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_color_format").val($("#abstract_spreadinputs_color_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_tags_format").val($("#abstract_spreadinputs_tags_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_image_folder").val("");
	$("#abstract_spreadinputs_image_width").val("");
	$("#abstract_spreadinputs_image_height").val("");
	$("#abstract_spreadinputs_image_width_ratio").val("");
	$("#abstract_spreadinputs_image_height_ratio").val("");
	$("#abstract_spreadinputs_gridWidth").data("ionRangeSlider").update({
		from: 12
	});
	$("#abstract_spreadinputs_alignment").val($("#abstract_spreadinputs_alignment option:first").val()).trigger("change");
	$("#abstract_spreadinputs_order").val(<?php echo $data_table_info["count_true"]; ?>);
	$("#abstract_spreadinputs_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#abstract_spreadinputs_action").val("");

}

function update_abstract_spreadinputs_data(target_id) {
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_references_data_dynamic_list",
			parameters: abstract_spreadinputs_references_filters
		},
		success: function(response) {
			var abstract_spreadinputs_references = $("#abstract_spreadinputs_references").val();
			$("#abstract_spreadinputs_references").empty();
			$("#abstract_spreadinputs_references").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_references").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_references").append("<option value=\"" + response.values[i]['abstract_references_id'] + "\">" + response.values[i]['abstract_references_label'] + " (ID: " + response.values[i]['abstract_references_id'] + ")</option>");
				}
				$("#abstract_spreadinputs_references").val(abstract_spreadinputs_references);
			}
		}
	});
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list",
			parameters: abstract_spreadinputs_input_list_dynamic_module_filters
		},
		success: function(response) {
			var abstract_spreadinputs_input_list_dynamic_module = $("#abstract_spreadinputs_input_list_dynamic_module").val();
			$("#abstract_spreadinputs_input_list_dynamic_module").empty();
			$("#abstract_spreadinputs_input_list_dynamic_module").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_input_list_dynamic_module").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_input_list_dynamic_module").append("<option value=\"" + response.values[i]['modules_db_name'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_db_name'] + ")</option>");
				}
				$("#abstract_spreadinputs_input_list_dynamic_module").val(abstract_spreadinputs_input_list_dynamic_module);
			}
		}
	});
}

function submit_abstract_spreadinputs_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);

    if ($("#abstract_spreadinputs_id").val() == "") {
		var method = "create_abstract_spreadinputs_data";
	} else {
		var method = "update_abstract_spreadinputs_data";
	}

	if ($("#abstract_spreadinputs_activate").is(":checked") === true){
		var abstract_spreadinputs_activate = "1";
	} else {
		var abstract_spreadinputs_activate = "0";
	}
	if ($("#abstract_spreadinputs_require").is(":checked") === true){
		var abstract_spreadinputs_require = abstract_spreadinputs_require_default_value[0];
	} else {
		var abstract_spreadinputs_require = "";
	}
	if ($("#abstract_spreadinputs_readonly").is(":checked") === true){
		var abstract_spreadinputs_readonly = abstract_spreadinputs_readonly_default_value[0];
	} else {
		var abstract_spreadinputs_readonly = "";
	}
	if ($("#abstract_spreadinputs_disable").is(":checked") === true){
		var abstract_spreadinputs_disable = abstract_spreadinputs_disable_default_value[0];
	} else {
		var abstract_spreadinputs_disable = "";
	}
	if ($("#abstract_spreadinputs_hidden").is(":checked") === true){
		var abstract_spreadinputs_hidden = abstract_spreadinputs_hidden_default_value[0];
	} else {
		var abstract_spreadinputs_hidden = "";
	}
	if ($("#abstract_spreadinputs_validate_email").is(":checked") === true){
		var abstract_spreadinputs_validate_email = abstract_spreadinputs_validate_email_default_value[0];
	} else {
		var abstract_spreadinputs_validate_email = "";
	}
	if ($("#abstract_spreadinputs_validate_password").is(":checked") === true){
		var abstract_spreadinputs_validate_password = abstract_spreadinputs_validate_password_default_value[0];
	} else {
		var abstract_spreadinputs_validate_password = "";
	}
	if ($("#abstract_spreadinputs_validate_website").is(":checked") === true){
		var abstract_spreadinputs_validate_website = abstract_spreadinputs_validate_website_default_value[0];
	} else {
		var abstract_spreadinputs_validate_website = "";
	}
	if ($("#abstract_spreadinputs_validate_no_space").is(":checked") === true){
		var abstract_spreadinputs_validate_no_space = abstract_spreadinputs_validate_no_space_default_value[0];
	} else {
		var abstract_spreadinputs_validate_no_space = "";
	}
	if ($("#abstract_spreadinputs_validate_no_specialchar_soft").is(":checked") === true){
		var abstract_spreadinputs_validate_no_specialchar_soft = abstract_spreadinputs_validate_no_specialchar_soft_default_value[0];
	} else {
		var abstract_spreadinputs_validate_no_specialchar_soft = "";
	}
	if ($("#abstract_spreadinputs_validate_no_specialchar_hard").is(":checked") === true){
		var abstract_spreadinputs_validate_no_specialchar_hard = abstract_spreadinputs_validate_no_specialchar_hard_default_value[0];
	} else {
		var abstract_spreadinputs_validate_no_specialchar_hard = "";
	}
	if ($("#abstract_spreadinputs_validate_upper").is(":checked") === true){
		var abstract_spreadinputs_validate_upper = abstract_spreadinputs_validate_upper_default_value[0];
	} else {
		var abstract_spreadinputs_validate_upper = "";
	}
	if ($("#abstract_spreadinputs_validate_lower").is(":checked") === true){
		var abstract_spreadinputs_validate_lower = abstract_spreadinputs_validate_lower_default_value[0];
	} else {
		var abstract_spreadinputs_validate_lower = "";
	}
	if ($("#abstract_spreadinputs_validate_number").is(":checked") === true){
		var abstract_spreadinputs_validate_number = abstract_spreadinputs_validate_number_default_value[0];
	} else {
		var abstract_spreadinputs_validate_number = "";
	}
	if ($("#abstract_spreadinputs_validate_digit").is(":checked") === true){
		var abstract_spreadinputs_validate_digit = abstract_spreadinputs_validate_digit_default_value[0];
	} else {
		var abstract_spreadinputs_validate_digit = "";
	}
	if ($("#abstract_spreadinputs_validate_unique").is(":checked") === true){
		var abstract_spreadinputs_validate_unique = abstract_spreadinputs_validate_unique_default_value[0];
	} else {
		var abstract_spreadinputs_validate_unique = "";
	}
	if ($("#abstract_spreadinputs_default_switch").is(":checked") === true){
		var abstract_spreadinputs_default_switch = abstract_spreadinputs_default_switch_default_value[0];
	} else {
		var abstract_spreadinputs_default_switch = "";
	}
	
	
	
	
	
	

	var form_values   = {
		"abstract_spreadinputs_id": $("#abstract_spreadinputs_id").val().trim(),
		"abstract_spreadinputs_action": $("#abstract_spreadinputs_action").val().trim(),
		"abstract_spreadinputs_label": $("#abstract_spreadinputs_label").val(),
		"abstract_spreadinputs_varname": $("#abstract_spreadinputs_varname").val(),
		"abstract_spreadinputs_type": $("#abstract_spreadinputs_type").select2("val"),
		"abstract_spreadinputs_references": $("#abstract_spreadinputs_references").select2("val"),
		"abstract_spreadinputs_placeholder": $("#abstract_spreadinputs_placeholder").val(),
		"abstract_spreadinputs_help": $("#abstract_spreadinputs_help").val(),
		"abstract_spreadinputs_require": abstract_spreadinputs_require,
		"abstract_spreadinputs_readonly": abstract_spreadinputs_readonly,
		"abstract_spreadinputs_disable": abstract_spreadinputs_disable,
		"abstract_spreadinputs_hidden": abstract_spreadinputs_hidden,
		"abstract_spreadinputs_validate_string_min": $("#abstract_spreadinputs_validate_string_min").val(),
		"abstract_spreadinputs_validate_string_max": $("#abstract_spreadinputs_validate_string_max").val(),
		"abstract_spreadinputs_validate_number_min": $("#abstract_spreadinputs_validate_number_min").val(),
		"abstract_spreadinputs_validate_number_max": $("#abstract_spreadinputs_validate_number_max").val(),
		"abstract_spreadinputs_validate_date_min": $("#abstract_spreadinputs_validate_date_min").val(),
		"abstract_spreadinputs_validate_date_max": $("#abstract_spreadinputs_validate_date_max").val(),
		"abstract_spreadinputs_validate_datetime_min": $("#abstract_spreadinputs_validate_datetime_min").val(),
		"abstract_spreadinputs_validate_datetime_max": $("#abstract_spreadinputs_validate_datetime_max").val(),
		"abstract_spreadinputs_validate_password_equal_to": $("#abstract_spreadinputs_validate_password_equal_to").val(),
		"abstract_spreadinputs_validate_email": abstract_spreadinputs_validate_email,
		"abstract_spreadinputs_validate_password": abstract_spreadinputs_validate_password,
		"abstract_spreadinputs_validate_website": abstract_spreadinputs_validate_website,
		"abstract_spreadinputs_validate_no_space": abstract_spreadinputs_validate_no_space,
		"abstract_spreadinputs_validate_no_specialchar_soft": abstract_spreadinputs_validate_no_specialchar_soft,
		"abstract_spreadinputs_validate_no_specialchar_hard": abstract_spreadinputs_validate_no_specialchar_hard,
		"abstract_spreadinputs_validate_upper": abstract_spreadinputs_validate_upper,
		"abstract_spreadinputs_validate_lower": abstract_spreadinputs_validate_lower,
		"abstract_spreadinputs_validate_number": abstract_spreadinputs_validate_number,
		"abstract_spreadinputs_validate_digit": abstract_spreadinputs_validate_digit,
		"abstract_spreadinputs_validate_unique": abstract_spreadinputs_validate_unique,
		"abstract_spreadinputs_prefix": $("#abstract_spreadinputs_prefix").val(),
		"abstract_spreadinputs_suffix": $("#abstract_spreadinputs_suffix").val(),
		"abstract_spreadinputs_default_value_type": $("#abstract_spreadinputs_default_value_type").val(),
		"abstract_spreadinputs_default_value": $("#abstract_spreadinputs_default_value").val(),
		"abstract_spreadinputs_default_switch": abstract_spreadinputs_default_switch,
		"abstract_spreadinputs_input_list": $("#abstract_spreadinputs_input_list").val(),
		"abstract_spreadinputs_input_list_static_value": $("#abstract_spreadinputs_input_list_static_value").val(),
		"abstract_spreadinputs_input_list_dynamic_module": $("#abstract_spreadinputs_input_list_dynamic_module").select2("val"),
		"abstract_spreadinputs_input_list_dynamic_id_column": $("#abstract_spreadinputs_input_list_dynamic_id_column").select2("val"),
		"abstract_spreadinputs_input_list_dynamic_value_column": $("#abstract_spreadinputs_input_list_dynamic_value_column").select2("val"),
		"abstract_spreadinputs_file_selector_type": $("#abstract_spreadinputs_file_selector_type").val(),
		"abstract_spreadinputs_date_format": $("#abstract_spreadinputs_date_format").val(),
		"abstract_spreadinputs_color_format": $("#abstract_spreadinputs_color_format").val(),
		"abstract_spreadinputs_tags_format": $("#abstract_spreadinputs_tags_format").val(),
		"abstract_spreadinputs_image_folder": $("#abstract_spreadinputs_image_folder").val(),
		"abstract_spreadinputs_image_width": $("#abstract_spreadinputs_image_width").val(),
		"abstract_spreadinputs_image_height": $("#abstract_spreadinputs_image_height").val(),
		"abstract_spreadinputs_image_width_ratio": $("#abstract_spreadinputs_image_width_ratio").val(),
		"abstract_spreadinputs_image_height_ratio": $("#abstract_spreadinputs_image_height_ratio").val(),
		"abstract_spreadinputs_gridWidth": $("#abstract_spreadinputs_gridWidth").val(),
		"abstract_spreadinputs_alignment": $("#abstract_spreadinputs_alignment").val(),
		"abstract_spreadinputs_order": $("#abstract_spreadinputs_order").val().trim(),

		"users_id": $("#users_id").val().trim(),
		"users_username": $("#users_username").val().trim(),
		"users_name": $("#users_name").val().trim(),
		"users_last_name": $("#users_last_name").val().trim(),
		"abstract_spreadinputs_activate": abstract_spreadinputs_activate.trim(),
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				if (method == "create_abstract_spreadinputs_data") {
					create_abstract_spreadinputs_table_row(response.values);
					show_abstract_spreadinputs_modal_response(response.status, response.message);
					reset_abstract_spreadinputs_data();
					$("#form-content").slideUp("fast");
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
					var limit = $("#datatable_length_selector").val();
					var page = get_url_param().page;
					if (page == null) {
						page = 1;
					}
					var search_text = $("#datatable_search").val();
					var published = get_url_param().published;
					var from = get_url_param().from;
					var to = get_url_param().to;
					if (limit != null) {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
							}
						}
					} else {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>");
							}
						}
					}
            		$("#button-data-submit").prop("disabled", false);
				} else if (method == "update_abstract_spreadinputs_data") {
					update_abstract_spreadinputs_table_row(response.values);
				    show_abstract_spreadinputs_modal_response(response.status, response.message);
					update_abstract_spreadinputs_data($("#abstract_spreadinputs_id").val());
					edit_abstract_spreadinputs_open($("#abstract_spreadinputs_id").val());
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
            		$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
            	$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function delete_abstract_spreadinputs_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_abstract_spreadinputs_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				delete_abstract_spreadinputs_table_row(target_id);
				if ($("#abstract_spreadinputs_id").val() == target_id) {
					reset_abstract_spreadinputs_data();
					form_abstract_spreadinputs_close();
				}
                show_abstract_spreadinputs_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus);
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function form_abstract_spreadinputs_close() {
	
	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}
	
}
function create_abstract_spreadinputs_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add")." ".$title; ?>");

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_abstract_spreadinputs_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#abstract_spreadinputs_action").val("create");

}

function edit_abstract_spreadinputs_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_spreadinputs_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#abstract_spreadinputs_id").val(response.values["abstract_spreadinputs_id"]);
				$("#abstract_spreadinputs_label").val(response.values["abstract_spreadinputs_label"]);
				$("#abstract_spreadinputs_label_stringlength").empty().append($("#abstract_spreadinputs_label").val().length + "/50");
				$("#abstract_spreadinputs_label_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_label").val().length < 240) {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_varname").val(response.values["abstract_spreadinputs_varname"]);
				$("#abstract_spreadinputs_varname_stringlength").empty().append($("#abstract_spreadinputs_varname").val().length + "/50");
				$("#abstract_spreadinputs_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_varname").val().length < 240) {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_type").select2("val", response.values["abstract_spreadinputs_type"]);
				$("#abstract_spreadinputs_references").select2("val", response.values["abstract_spreadinputs_references"]);
				$("#abstract_spreadinputs_placeholder").val(response.values["abstract_spreadinputs_placeholder"]);
				$("#abstract_spreadinputs_placeholder_stringlength").empty().append($("#abstract_spreadinputs_placeholder").val().length + "/100");
				$("#abstract_spreadinputs_placeholder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_placeholder").val().length < 240) {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_help").val(response.values["abstract_spreadinputs_help"]);
				$("#abstract_spreadinputs_help_stringlength").empty().append($("#abstract_spreadinputs_help").val().length + "/200");
				$("#abstract_spreadinputs_help_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_help").val().length < 240) {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_require"] == abstract_spreadinputs_require_default_value[0]){
					$("#abstract_spreadinputs_require").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_require").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_readonly"] == abstract_spreadinputs_readonly_default_value[0]){
					$("#abstract_spreadinputs_readonly").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_readonly").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_disable"] == abstract_spreadinputs_disable_default_value[0]){
					$("#abstract_spreadinputs_disable").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_disable").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_hidden"] == abstract_spreadinputs_hidden_default_value[0]){
					$("#abstract_spreadinputs_hidden").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_hidden").prop("checked", false);
				}
				$("#abstract_spreadinputs_validate_string_min").val(response.values["abstract_spreadinputs_validate_string_min"]);
				$("#abstract_spreadinputs_validate_string_max").val(response.values["abstract_spreadinputs_validate_string_max"]);
				$("#abstract_spreadinputs_validate_number_min").val(response.values["abstract_spreadinputs_validate_number_min"]);
				$("#abstract_spreadinputs_validate_number_max").val(response.values["abstract_spreadinputs_validate_number_max"]);
				$("#abstract_spreadinputs_validate_date_min").val(response.values["abstract_spreadinputs_validate_date_min"]);
				$("#abstract_spreadinputs_validate_date_max").val(response.values["abstract_spreadinputs_validate_date_max"]);
				$("#abstract_spreadinputs_validate_datetime_min").val(response.values["abstract_spreadinputs_validate_datetime_min"]);
				$("#abstract_spreadinputs_validate_datetime_max").val(response.values["abstract_spreadinputs_validate_datetime_max"]);
				$("#abstract_spreadinputs_validate_password_equal_to").val(response.values["abstract_spreadinputs_validate_password_equal_to"]);
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").empty().append($("#abstract_spreadinputs_validate_password_equal_to").val().length + "/0");
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_validate_password_equal_to").val().length < 240) {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_validate_email"] == abstract_spreadinputs_validate_email_default_value[0]){
					$("#abstract_spreadinputs_validate_email").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_email").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_password"] == abstract_spreadinputs_validate_password_default_value[0]){
					$("#abstract_spreadinputs_validate_password").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_password").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_website"] == abstract_spreadinputs_validate_website_default_value[0]){
					$("#abstract_spreadinputs_validate_website").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_website").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_space"] == abstract_spreadinputs_validate_no_space_default_value[0]){
					$("#abstract_spreadinputs_validate_no_space").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_space").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_soft"] == abstract_spreadinputs_validate_no_specialchar_soft_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_hard"] == abstract_spreadinputs_validate_no_specialchar_hard_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_upper"] == abstract_spreadinputs_validate_upper_default_value[0]){
					$("#abstract_spreadinputs_validate_upper").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_upper").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_lower"] == abstract_spreadinputs_validate_lower_default_value[0]){
					$("#abstract_spreadinputs_validate_lower").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_lower").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_number"] == abstract_spreadinputs_validate_number_default_value[0]){
					$("#abstract_spreadinputs_validate_number").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_number").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_digit"] == abstract_spreadinputs_validate_digit_default_value[0]){
					$("#abstract_spreadinputs_validate_digit").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_digit").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_unique"] == abstract_spreadinputs_validate_unique_default_value[0]){
					$("#abstract_spreadinputs_validate_unique").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_unique").prop("checked", false);
				}
				$("#abstract_spreadinputs_prefix").val(response.values["abstract_spreadinputs_prefix"]);
				$("#abstract_spreadinputs_prefix_stringlength").empty().append($("#abstract_spreadinputs_prefix").val().length + "/100");
				$("#abstract_spreadinputs_prefix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_prefix").val().length < 240) {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_suffix").val(response.values["abstract_spreadinputs_suffix"]);
				$("#abstract_spreadinputs_suffix_stringlength").empty().append($("#abstract_spreadinputs_suffix").val().length + "/100");
				$("#abstract_spreadinputs_suffix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_suffix").val().length < 240) {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_default_value_type").val(response.values["abstract_spreadinputs_default_value_type"]);
				$("#abstract_spreadinputs_default_value").tagsInputDestroy();
				$("#abstract_spreadinputs_default_value").val(response.values["abstract_spreadinputs_default_value"]);
				$("#abstract_spreadinputs_default_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				if(response.values["abstract_spreadinputs_default_switch"] == abstract_spreadinputs_default_switch_default_value[0]){
					$("#abstract_spreadinputs_default_switch").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_default_switch").prop("checked", false);
				}
				$("#abstract_spreadinputs_input_list").val(response.values["abstract_spreadinputs_input_list"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInputDestroy();
				$("#abstract_spreadinputs_input_list_static_value").val(response.values["abstract_spreadinputs_input_list_static_value"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				$("#abstract_spreadinputs_input_list_dynamic_module").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_module"]);
				$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_id_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_id_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_value_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_value_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_file_selector_type").val(response.values["abstract_spreadinputs_file_selector_type"]);
				$("#abstract_spreadinputs_date_format").val(response.values["abstract_spreadinputs_date_format"]);
				$("#abstract_spreadinputs_color_format").val(response.values["abstract_spreadinputs_color_format"]);
				$("#abstract_spreadinputs_tags_format").val(response.values["abstract_spreadinputs_tags_format"]);
				$("#abstract_spreadinputs_image_folder").val(response.values["abstract_spreadinputs_image_folder"]);
				$("#abstract_spreadinputs_image_folder_stringlength").empty().append($("#abstract_spreadinputs_image_folder").val().length + "/200");
				$("#abstract_spreadinputs_image_folder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_image_folder").val().length < 240) {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_image_width").val(response.values["abstract_spreadinputs_image_width"]);
				$("#abstract_spreadinputs_image_height").val(response.values["abstract_spreadinputs_image_height"]);
				$("#abstract_spreadinputs_image_width_ratio").val(response.values["abstract_spreadinputs_image_width_ratio"]);
				$("#abstract_spreadinputs_image_height_ratio").val(response.values["abstract_spreadinputs_image_height_ratio"]);
				$("#abstract_spreadinputs_gridWidth").data("ionRangeSlider").update({
					from: response.values["abstract_spreadinputs_gridWidth"]
				});
				$("#abstract_spreadinputs_alignment").val(response.values["abstract_spreadinputs_alignment"]);
				$("#abstract_spreadinputs_order").val(response.values["abstract_spreadinputs_order"]);
				if(response.values["abstract_spreadinputs_activate"] == "1" ){
					$("#abstract_spreadinputs_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_spreadinputs_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["abstract_spreadinputs_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#abstract_spreadinputs_action").val("edit");
				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&abstract_spreadinputs_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_spreadinputs_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&abstract_spreadinputs_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&abstract_spreadinputs_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_spreadinputs_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&abstract_spreadinputs_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {
					
					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_abstract_spreadinputs_modal_prompt($(this).attr("data-content"), "revision_abstract_spreadinputs_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function copy_abstract_spreadinputs_open(target_id) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_spreadinputs_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy")." ".$title; ?>");

				$("#abstract_spreadinputs_id").val("");
				$("#abstract_spreadinputs_label").val(response.values["abstract_spreadinputs_label"]);
				$("#abstract_spreadinputs_label_stringlength").empty().append($("#abstract_spreadinputs_label").val().length + "/50");
				$("#abstract_spreadinputs_label_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_label").val().length < 240) {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_varname").val(response.values["abstract_spreadinputs_varname"]);
				$("#abstract_spreadinputs_varname_stringlength").empty().append($("#abstract_spreadinputs_varname").val().length + "/50");
				$("#abstract_spreadinputs_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_varname").val().length < 240) {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_type").select2("val", response.values["abstract_spreadinputs_type"]);
				$("#abstract_spreadinputs_references").select2("val", response.values["abstract_spreadinputs_references"]);
				$("#abstract_spreadinputs_placeholder").val(response.values["abstract_spreadinputs_placeholder"]);
				$("#abstract_spreadinputs_placeholder_stringlength").empty().append($("#abstract_spreadinputs_placeholder").val().length + "/100");
				$("#abstract_spreadinputs_placeholder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_placeholder").val().length < 240) {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_help").val(response.values["abstract_spreadinputs_help"]);
				$("#abstract_spreadinputs_help_stringlength").empty().append($("#abstract_spreadinputs_help").val().length + "/200");
				$("#abstract_spreadinputs_help_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_help").val().length < 240) {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_require"] == abstract_spreadinputs_require_default_value[0]){
					$("#abstract_spreadinputs_require").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_require").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_readonly"] == abstract_spreadinputs_readonly_default_value[0]){
					$("#abstract_spreadinputs_readonly").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_readonly").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_disable"] == abstract_spreadinputs_disable_default_value[0]){
					$("#abstract_spreadinputs_disable").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_disable").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_hidden"] == abstract_spreadinputs_hidden_default_value[0]){
					$("#abstract_spreadinputs_hidden").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_hidden").prop("checked", false);
				}
				$("#abstract_spreadinputs_validate_string_min").val(response.values["abstract_spreadinputs_validate_string_min"]);
				$("#abstract_spreadinputs_validate_string_max").val(response.values["abstract_spreadinputs_validate_string_max"]);
				$("#abstract_spreadinputs_validate_number_min").val(response.values["abstract_spreadinputs_validate_number_min"]);
				$("#abstract_spreadinputs_validate_number_max").val(response.values["abstract_spreadinputs_validate_number_max"]);
				$("#abstract_spreadinputs_validate_date_min").val(response.values["abstract_spreadinputs_validate_date_min"]);
				$("#abstract_spreadinputs_validate_date_max").val(response.values["abstract_spreadinputs_validate_date_max"]);
				$("#abstract_spreadinputs_validate_datetime_min").val(response.values["abstract_spreadinputs_validate_datetime_min"]);
				$("#abstract_spreadinputs_validate_datetime_max").val(response.values["abstract_spreadinputs_validate_datetime_max"]);
				$("#abstract_spreadinputs_validate_password_equal_to").val(response.values["abstract_spreadinputs_validate_password_equal_to"]);
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").empty().append($("#abstract_spreadinputs_validate_password_equal_to").val().length + "/0");
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_validate_password_equal_to").val().length < 240) {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_validate_email"] == abstract_spreadinputs_validate_email_default_value[0]){
					$("#abstract_spreadinputs_validate_email").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_email").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_password"] == abstract_spreadinputs_validate_password_default_value[0]){
					$("#abstract_spreadinputs_validate_password").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_password").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_website"] == abstract_spreadinputs_validate_website_default_value[0]){
					$("#abstract_spreadinputs_validate_website").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_website").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_space"] == abstract_spreadinputs_validate_no_space_default_value[0]){
					$("#abstract_spreadinputs_validate_no_space").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_space").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_soft"] == abstract_spreadinputs_validate_no_specialchar_soft_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_hard"] == abstract_spreadinputs_validate_no_specialchar_hard_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_upper"] == abstract_spreadinputs_validate_upper_default_value[0]){
					$("#abstract_spreadinputs_validate_upper").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_upper").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_lower"] == abstract_spreadinputs_validate_lower_default_value[0]){
					$("#abstract_spreadinputs_validate_lower").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_lower").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_number"] == abstract_spreadinputs_validate_number_default_value[0]){
					$("#abstract_spreadinputs_validate_number").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_number").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_digit"] == abstract_spreadinputs_validate_digit_default_value[0]){
					$("#abstract_spreadinputs_validate_digit").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_digit").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_unique"] == abstract_spreadinputs_validate_unique_default_value[0]){
					$("#abstract_spreadinputs_validate_unique").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_unique").prop("checked", false);
				}
				$("#abstract_spreadinputs_prefix").val(response.values["abstract_spreadinputs_prefix"]);
				$("#abstract_spreadinputs_prefix_stringlength").empty().append($("#abstract_spreadinputs_prefix").val().length + "/100");
				$("#abstract_spreadinputs_prefix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_prefix").val().length < 240) {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_suffix").val(response.values["abstract_spreadinputs_suffix"]);
				$("#abstract_spreadinputs_suffix_stringlength").empty().append($("#abstract_spreadinputs_suffix").val().length + "/100");
				$("#abstract_spreadinputs_suffix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_suffix").val().length < 240) {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_default_value_type").val(response.values["abstract_spreadinputs_default_value_type"]);
				$("#abstract_spreadinputs_default_value").tagsInputDestroy();
				$("#abstract_spreadinputs_default_value").val(response.values["abstract_spreadinputs_default_value"]);
				$("#abstract_spreadinputs_default_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				if(response.values["abstract_spreadinputs_default_switch"] == abstract_spreadinputs_default_switch_default_value[0]){
					$("#abstract_spreadinputs_default_switch").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_default_switch").prop("checked", false);
				}
				$("#abstract_spreadinputs_input_list").val(response.values["abstract_spreadinputs_input_list"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInputDestroy();
				$("#abstract_spreadinputs_input_list_static_value").val(response.values["abstract_spreadinputs_input_list_static_value"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				$("#abstract_spreadinputs_input_list_dynamic_module").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_module"]);
				$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_id_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_id_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_value_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_value_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_file_selector_type").val(response.values["abstract_spreadinputs_file_selector_type"]);
				$("#abstract_spreadinputs_date_format").val(response.values["abstract_spreadinputs_date_format"]);
				$("#abstract_spreadinputs_color_format").val(response.values["abstract_spreadinputs_color_format"]);
				$("#abstract_spreadinputs_tags_format").val(response.values["abstract_spreadinputs_tags_format"]);
				$("#abstract_spreadinputs_image_folder").val(response.values["abstract_spreadinputs_image_folder"]);
				$("#abstract_spreadinputs_image_folder_stringlength").empty().append($("#abstract_spreadinputs_image_folder").val().length + "/200");
				$("#abstract_spreadinputs_image_folder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_image_folder").val().length < 240) {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_image_width").val(response.values["abstract_spreadinputs_image_width"]);
				$("#abstract_spreadinputs_image_height").val(response.values["abstract_spreadinputs_image_height"]);
				$("#abstract_spreadinputs_image_width_ratio").val(response.values["abstract_spreadinputs_image_width_ratio"]);
				$("#abstract_spreadinputs_image_height_ratio").val(response.values["abstract_spreadinputs_image_height_ratio"]);
				$("#abstract_spreadinputs_gridWidth").data("ionRangeSlider").update({
					from: response.values["abstract_spreadinputs_gridWidth"]
				});
				$("#abstract_spreadinputs_alignment").val(response.values["abstract_spreadinputs_alignment"]);
				$("#abstract_spreadinputs_order").val(response.values["abstract_spreadinputs_order"]);
				if(response.values["abstract_spreadinputs_activate"] == "1" ){
					$("#abstract_spreadinputs_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_spreadinputs_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#abstract_spreadinputs_action").val("copy");
				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&abstract_spreadinputs_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_spreadinputs_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&abstract_spreadinputs_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&abstract_spreadinputs_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&abstract_spreadinputs_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&abstract_spreadinputs_id=" + target_id);
						}
					}
				}
			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function view_abstract_spreadinputs_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_abstract_spreadinputs_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_abstract_spreadinputs_modal_response(response.status, response.html);
				
				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_abstract_spreadinputs_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_spreadinputs_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#abstract_spreadinputs_id").val(response.values["abstract_spreadinputs_id"]);
				$("#abstract_spreadinputs_label").val(response.values["abstract_spreadinputs_label"]);
				$("#abstract_spreadinputs_label_stringlength").empty().append($("#abstract_spreadinputs_label").val().length + "/50");
				$("#abstract_spreadinputs_label_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_label").val().length < 240) {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_label_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_varname").val(response.values["abstract_spreadinputs_varname"]);
				$("#abstract_spreadinputs_varname_stringlength").empty().append($("#abstract_spreadinputs_varname").val().length + "/50");
				$("#abstract_spreadinputs_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_varname").val().length < 240) {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_type").select2("val", response.values["abstract_spreadinputs_type"]);
				$("#abstract_spreadinputs_references").select2("val", response.values["abstract_spreadinputs_references"]);
				$("#abstract_spreadinputs_placeholder").val(response.values["abstract_spreadinputs_placeholder"]);
				$("#abstract_spreadinputs_placeholder_stringlength").empty().append($("#abstract_spreadinputs_placeholder").val().length + "/100");
				$("#abstract_spreadinputs_placeholder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_placeholder").val().length < 240) {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_help").val(response.values["abstract_spreadinputs_help"]);
				$("#abstract_spreadinputs_help_stringlength").empty().append($("#abstract_spreadinputs_help").val().length + "/200");
				$("#abstract_spreadinputs_help_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_help").val().length < 240) {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_help_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_require"] == abstract_spreadinputs_require_default_value[0]){
					$("#abstract_spreadinputs_require").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_require").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_readonly"] == abstract_spreadinputs_readonly_default_value[0]){
					$("#abstract_spreadinputs_readonly").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_readonly").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_disable"] == abstract_spreadinputs_disable_default_value[0]){
					$("#abstract_spreadinputs_disable").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_disable").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_hidden"] == abstract_spreadinputs_hidden_default_value[0]){
					$("#abstract_spreadinputs_hidden").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_hidden").prop("checked", false);
				}
				$("#abstract_spreadinputs_validate_string_min").val(response.values["abstract_spreadinputs_validate_string_min"]);
				$("#abstract_spreadinputs_validate_string_max").val(response.values["abstract_spreadinputs_validate_string_max"]);
				$("#abstract_spreadinputs_validate_number_min").val(response.values["abstract_spreadinputs_validate_number_min"]);
				$("#abstract_spreadinputs_validate_number_max").val(response.values["abstract_spreadinputs_validate_number_max"]);
				$("#abstract_spreadinputs_validate_date_min").val(response.values["abstract_spreadinputs_validate_date_min"]);
				$("#abstract_spreadinputs_validate_date_max").val(response.values["abstract_spreadinputs_validate_date_max"]);
				$("#abstract_spreadinputs_validate_datetime_min").val(response.values["abstract_spreadinputs_validate_datetime_min"]);
				$("#abstract_spreadinputs_validate_datetime_max").val(response.values["abstract_spreadinputs_validate_datetime_max"]);
				$("#abstract_spreadinputs_validate_password_equal_to").val(response.values["abstract_spreadinputs_validate_password_equal_to"]);
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").empty().append($("#abstract_spreadinputs_validate_password_equal_to").val().length + "/0");
				$("#abstract_spreadinputs_validate_password_equal_to_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_validate_password_equal_to").val().length < 240) {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-danger");
				}
				if(response.values["abstract_spreadinputs_validate_email"] == abstract_spreadinputs_validate_email_default_value[0]){
					$("#abstract_spreadinputs_validate_email").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_email").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_password"] == abstract_spreadinputs_validate_password_default_value[0]){
					$("#abstract_spreadinputs_validate_password").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_password").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_website"] == abstract_spreadinputs_validate_website_default_value[0]){
					$("#abstract_spreadinputs_validate_website").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_website").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_space"] == abstract_spreadinputs_validate_no_space_default_value[0]){
					$("#abstract_spreadinputs_validate_no_space").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_space").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_soft"] == abstract_spreadinputs_validate_no_specialchar_soft_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_soft").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_no_specialchar_hard"] == abstract_spreadinputs_validate_no_specialchar_hard_default_value[0]){
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_no_specialchar_hard").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_upper"] == abstract_spreadinputs_validate_upper_default_value[0]){
					$("#abstract_spreadinputs_validate_upper").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_upper").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_lower"] == abstract_spreadinputs_validate_lower_default_value[0]){
					$("#abstract_spreadinputs_validate_lower").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_lower").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_number"] == abstract_spreadinputs_validate_number_default_value[0]){
					$("#abstract_spreadinputs_validate_number").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_number").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_digit"] == abstract_spreadinputs_validate_digit_default_value[0]){
					$("#abstract_spreadinputs_validate_digit").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_digit").prop("checked", false);
				}
				if(response.values["abstract_spreadinputs_validate_unique"] == abstract_spreadinputs_validate_unique_default_value[0]){
					$("#abstract_spreadinputs_validate_unique").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_validate_unique").prop("checked", false);
				}
				$("#abstract_spreadinputs_prefix").val(response.values["abstract_spreadinputs_prefix"]);
				$("#abstract_spreadinputs_prefix_stringlength").empty().append($("#abstract_spreadinputs_prefix").val().length + "/100");
				$("#abstract_spreadinputs_prefix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_prefix").val().length < 240) {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_prefix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_suffix").val(response.values["abstract_spreadinputs_suffix"]);
				$("#abstract_spreadinputs_suffix_stringlength").empty().append($("#abstract_spreadinputs_suffix").val().length + "/100");
				$("#abstract_spreadinputs_suffix_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_suffix").val().length < 240) {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_suffix_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_default_value_type").val(response.values["abstract_spreadinputs_default_value_type"]);
				$("#abstract_spreadinputs_default_value").tagsInputDestroy();
				$("#abstract_spreadinputs_default_value").val(response.values["abstract_spreadinputs_default_value"]);
				$("#abstract_spreadinputs_default_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				if(response.values["abstract_spreadinputs_default_switch"] == abstract_spreadinputs_default_switch_default_value[0]){
					$("#abstract_spreadinputs_default_switch").prop("checked", true);
				} else {
					$("#abstract_spreadinputs_default_switch").prop("checked", false);
				}
				$("#abstract_spreadinputs_input_list").val(response.values["abstract_spreadinputs_input_list"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInputDestroy();
				$("#abstract_spreadinputs_input_list_static_value").val(response.values["abstract_spreadinputs_input_list_static_value"]);
				$("#abstract_spreadinputs_input_list_static_value").tagsInput({
					height: "36px",
					width: "100%",
					defaultText: "",
					removeWithBackspace: true
				});
				$("#abstract_spreadinputs_input_list_dynamic_module").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_module"]);
				$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_id_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_id_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list",
						parameters: response.values["abstract_spreadinputs_input_list_dynamic_module"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									$("#abstract_spreadinputs_input_list_dynamic_value_column").select2("val", response.values["abstract_spreadinputs_input_list_dynamic_value_column"]);
								}
							}
						} else {
							$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
						}
					}
				});
				$("#abstract_spreadinputs_file_selector_type").val(response.values["abstract_spreadinputs_file_selector_type"]);
				$("#abstract_spreadinputs_date_format").val(response.values["abstract_spreadinputs_date_format"]);
				$("#abstract_spreadinputs_color_format").val(response.values["abstract_spreadinputs_color_format"]);
				$("#abstract_spreadinputs_tags_format").val(response.values["abstract_spreadinputs_tags_format"]);
				$("#abstract_spreadinputs_image_folder").val(response.values["abstract_spreadinputs_image_folder"]);
				$("#abstract_spreadinputs_image_folder_stringlength").empty().append($("#abstract_spreadinputs_image_folder").val().length + "/200");
				$("#abstract_spreadinputs_image_folder_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_spreadinputs_image_folder").val().length < 240) {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-success");
				} else {
					$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-danger");
				}
				$("#abstract_spreadinputs_image_width").val(response.values["abstract_spreadinputs_image_width"]);
				$("#abstract_spreadinputs_image_height").val(response.values["abstract_spreadinputs_image_height"]);
				$("#abstract_spreadinputs_image_width_ratio").val(response.values["abstract_spreadinputs_image_width_ratio"]);
				$("#abstract_spreadinputs_image_height_ratio").val(response.values["abstract_spreadinputs_image_height_ratio"]);
				$("#abstract_spreadinputs_gridWidth").data("ionRangeSlider").update({
					from: response.values["abstract_spreadinputs_gridWidth"]
				});
				$("#abstract_spreadinputs_alignment").val(response.values["abstract_spreadinputs_alignment"]);
				$("#abstract_spreadinputs_order").val(response.values["abstract_spreadinputs_order"]);
				if(response.values["abstract_spreadinputs_activate"] == "1" ){
					$("#abstract_spreadinputs_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_spreadinputs_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["abstract_spreadinputs_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}

function sort_abstract_spreadinputs_open() {

	$("#sort-content").css("visibility", "visible");
	$("#sort-content").fadeTo("slow", 1);
	$("#sort-content").removeClass("block-opt-hidden");
	$("#sort-content").fadeIn("slow").slideDown("fast");
	$("#sort-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_sort_abstract_spreadinputs",
			table_module_field: table_module_field
		},
		success: function(response) {
			
			$("#module-table").slideUp("fast");
			$("#sort-content").slideDown("fast");
			$("#module-sort").find(".sortable").empty();
			$("#module-sort").find(".sortable").append(response.html);
			
			$("ol.sortable").nestedSortable({
				forcePlaceholderSize: true,
				handle: "div",
				helper:	"clone",
				items: "li",
				opacity: .6,
				placeholder: "placeholder",
				revert: 250,
				tabSize: 25,
				tolerance: "pointer",
				toleranceElement: "> div",
				maxLevels: 1,

				isTree: true,
				expandOnHover: 700,
				startCollapsed: true
			});

			$(".disclose").on("click", function() {
				$(this).closest("li").toggleClass("mjs-nestedSortable-collapsed").toggleClass("mjs-nestedSortable-expanded");
			});
			
			$("#sort-content").removeClass("block-opt-refresh");
			
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-sort").offset().top - 160 + "px"
    }, "fast");

}
	
function save_sort_abstract_spreadinputs () {
	
	$("#sort-content").addClass("block-opt-refresh");
	
	$("#button-data-submit").prop("disabled", true);
	
	arraied = $("ol.sortable").nestedSortable("toArray", {startDepthCount: 0});
	
	$.ajax({
		type: "POST",
		url: url,
		data: {
			"method" : "sort_abstract_spreadinputs", 
			"parameters" : arraied 
		},
		dataType: "json", 
		success: function(response) {
			if (response.status == true) {
				$("#module-sort").find(".sortable").empty();
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				$("#sort-content").slideUp("fast");
				$("#module-table").slideDown("fast");
			} else {
				show_abstract_spreadinputs_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
			}
			$("#button-data-submit").prop("disabled", false);
			$("#sort-content").removeClass("block-opt-refresh");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_spreadinputs_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#sort-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		}
	});
	
	
}
	
function sort_abstract_spreadinputs_close() {
	
	$("#module-sort").find(".sortable").empty();
	$("#sort-content").fadeTo("fast", 0);
	$("#sort-content").slideUp("fast").fadeOut("slow");
	$("#module-table").slideDown("fast");

}

function create_abstract_spreadinputs_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: <?php if (isset($configs["page_limit"]) && !empty($configs["page_limit"])) { echo $configs["page_limit"]; } else { echo "20"; } ?>,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_abstract_spreadinputs_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $("#datatable_length_selector").val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "" && search_text != null) {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "" && search_text != null) {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_abstract_spreadinputs_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $("#datatable_length_selector").val();
			if (limit != null) {
				location.href = "?limit=" + limit;
			} else {
				location.href = "" + limit;
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_abstract_spreadinputs_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_abstract_spreadinputs_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			if ($("#datatable-" + data_table["abstract_spreadinputs_id"]).length > 0) {
				$("#datatable").find("#datatable-" + data_table["abstract_spreadinputs_id"]).remove();
			}
			$("#datatable-list").prepend(response.html);
			if (count_data_table < datatable_data_limit) {
				create_abstract_spreadinputs_table();
			} else {
				create_abstract_spreadinputs_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_spreadinputs_open", $(this).attr("data-target"));
				} else {
					edit_abstract_spreadinputs_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_spreadinputs_open", $(this).attr("data-target"));
				} else {
					copy_abstract_spreadinputs_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_spreadinputs_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_abstract_spreadinputs_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_abstract_spreadinputs_modal_prompt($(this).attr("data-content"), "delete_abstract_spreadinputs_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_abstract_spreadinputs_open($(this).attr("data-target"));
			});
		}
	});

}

function update_abstract_spreadinputs_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_abstract_spreadinputs_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			$("#datatable").find("#datatable-" + data_table["abstract_spreadinputs_id"]).empty();
			$("#datatable").find("#datatable-" + data_table["abstract_spreadinputs_id"]).append(response.html);
			if (count_data_table < datatable_data_limit) {
				create_abstract_spreadinputs_table();
			} else {
				create_abstract_spreadinputs_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_spreadinputs_open", $(this).attr("data-target"));
				} else {
					edit_abstract_spreadinputs_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_spreadinputs_open", $(this).attr("data-target"));
				} else {
					copy_abstract_spreadinputs_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_spreadinputs_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_abstract_spreadinputs_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_abstract_spreadinputs_modal_prompt($(this).attr("data-content"), "delete_abstract_spreadinputs_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_abstract_spreadinputs_open($(this).attr("data-target"));
			});
		}
	});

}

function delete_abstract_spreadinputs_table_row(field_id) {
	mainDataTable.fnDestroy();
	$("#datatable").find("#datatable-" + field_id).remove();
	if (count_data_table < datatable_data_limit) {
		create_abstract_spreadinputs_table();
	} else {
		create_abstract_spreadinputs_table_defer();
	}
}

function show_abstract_spreadinputs_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_abstract_spreadinputs_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {
	
	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_abstract_spreadinputs_open", $(this).attr("data-target"));
		} else {
			create_abstract_spreadinputs_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_spreadinputs_open", $(this).attr("data-target"));
		} else {
			edit_abstract_spreadinputs_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_spreadinputs_open", $(this).attr("data-target"));
		} else {
			copy_abstract_spreadinputs_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_spreadinputs_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
		} else {
			translate_abstract_spreadinputs_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});
	
	$(".btn-delete").click(function () {
		show_abstract_spreadinputs_modal_prompt($(this).attr("data-content"), "delete_abstract_spreadinputs_data", $(this).attr("data-target"));
	});
	
	$(".btn-view").click(function () {
		view_abstract_spreadinputs_open($(this).attr("data-target"));
	});
	
	$(".btn-form-close").click(function () {
		show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_abstract_spreadinputs_close", "");
	});
	
	$(".btn-sort").click(function () {
		sort_abstract_spreadinputs_open();
	});
	
	$(".btn-sort-close").click(function () {
		show_abstract_spreadinputs_modal_prompt("<?php echo translate("Are you sure you want to leave current sort") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "sort_abstract_spreadinputs_close", "");
	});
	
	$("#button-data-save-sort").click(function(){
		save_sort_abstract_spreadinputs();
	});
	
	$("#from").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});
	
	$("#to").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});
	
	$("#abstract_spreadinputs_activate").click(function () {
		if ($("#abstract_spreadinputs_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});
	
	
	$("#abstract_spreadinputs_label").keyup(function(event){
		$("#abstract_spreadinputs_label_stringlength").empty().append($(this).val().length + "/50");
		$("#abstract_spreadinputs_label_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_label_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_label_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_varname").keyup(function(event){
		$("#abstract_spreadinputs_varname_stringlength").empty().append($(this).val().length + "/50");
		$("#abstract_spreadinputs_varname_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_varname_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_varname_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_type").select2();
	$("#abstract_spreadinputs_type").val($("#abstract_spreadinputs_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_type-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_references").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_references_data_dynamic_list",
			filters: abstract_spreadinputs_references_filters
		},
		success: function(response) {
			$("#abstract_spreadinputs_references-check-loading").fadeOut("fast");
			$("#abstract_spreadinputs_references").empty();
			$("#abstract_spreadinputs_references").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_references").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_references").append("<option value=\"" + response.values[i]['abstract_references_id'] + "\">" + response.values[i]['abstract_references_label'] + " (ID: " + response.values[i]['abstract_references_id'] + ")</option>");
				}
				$("#abstract_spreadinputs_references").val($("#abstract_spreadinputs_references option:first").val()).trigger("change");
			}
		}
	});
	$("#abstract_spreadinputs_placeholder").keyup(function(event){
		$("#abstract_spreadinputs_placeholder_stringlength").empty().append($(this).val().length + "/100");
		$("#abstract_spreadinputs_placeholder_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_placeholder_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_help").keyup(function(event){
		$("#abstract_spreadinputs_help_stringlength").empty().append($(this).val().length + "/200");
		$("#abstract_spreadinputs_help_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_help_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_help_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_validate_date_min").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});
	
	$("#abstract_spreadinputs_validate_date_max").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});
	
	$("#abstract_spreadinputs_validate_datetime_min").datetimepicker({
	
		<?php 
		if (file_exists("../../../plugins/bootstrap-datetimepicker/locale/" . $configs["backend_language"] . ".js")) {
			echo "locale: \"" . $configs["backend_language"] . "\",";
		}
		?>
		showClose: true,
		showClear: true,
		collapse: true,
		dayViewHeaderFormat: "MMMM YYYY",
		format: "YYYY-MM-DD HH:mm:ss",
	});
	
	$("#abstract_spreadinputs_validate_datetime_max").datetimepicker({
	
		<?php 
		if (file_exists("../../../plugins/bootstrap-datetimepicker/locale/" . $configs["backend_language"] . ".js")) {
			echo "locale: \"" . $configs["backend_language"] . "\",";
		}
		?>
		showClose: true,
		showClear: true,
		collapse: true,
		dayViewHeaderFormat: "MMMM YYYY",
		format: "YYYY-MM-DD HH:mm:ss",
	});
	
	$("#abstract_spreadinputs_validate_password_equal_to").keyup(function(event){
		$("#abstract_spreadinputs_validate_password_equal_to_stringlength").empty().append($(this).val().length + "/0");
		$("#abstract_spreadinputs_validate_password_equal_to_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_validate_password_equal_to_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_prefix").keyup(function(event){
		$("#abstract_spreadinputs_prefix_stringlength").empty().append($(this).val().length + "/100");
		$("#abstract_spreadinputs_prefix_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_prefix_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_prefix_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_suffix").keyup(function(event){
		$("#abstract_spreadinputs_suffix_stringlength").empty().append($(this).val().length + "/100");
		$("#abstract_spreadinputs_suffix_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_suffix_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_suffix_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_default_value_type").val($("#abstract_spreadinputs_default_value_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_default_value_type-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_default_value").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "",
		removeWithBackspace: true
	});
	$("#abstract_spreadinputs_input_list").val($("#abstract_spreadinputs_input_list option:first").val()).trigger("change");
	$("#abstract_spreadinputs_input_list-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_input_list_static_value").tagsInput({
		height: "36px",
		width: "100%",
		defaultText: "",
		removeWithBackspace: true
	});
	$("#abstract_spreadinputs_input_list_dynamic_module").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list",
			filters: abstract_spreadinputs_input_list_dynamic_module_filters
		},
		success: function(response) {
			$("#abstract_spreadinputs_input_list_dynamic_module-check-loading").fadeOut("fast");
			$("#abstract_spreadinputs_input_list_dynamic_module").empty();
			$("#abstract_spreadinputs_input_list_dynamic_module").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_input_list_dynamic_module").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_input_list_dynamic_module").append("<option value=\"" + response.values[i]['modules_db_name'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_db_name'] + ")</option>");
				}
				$("#abstract_spreadinputs_input_list_dynamic_module").val($("#abstract_spreadinputs_input_list_dynamic_module option:first").val()).trigger("change");
			}
		}
	});
	$("#abstract_spreadinputs_input_list_dynamic_id_column").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list",
			filters: abstract_spreadinputs_input_list_dynamic_id_column_filters
		},
		success: function(response) {
			$("#abstract_spreadinputs_input_list_dynamic_id_column-check-loading").fadeOut("fast");
			$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
			$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option value=\"" + response.values[i]['modules_db_name'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_db_name'] + ")</option>");
				}
				$("#abstract_spreadinputs_input_list_dynamic_id_column").val($("#abstract_spreadinputs_input_list_dynamic_id_column option:first").val()).trigger("change");
			}
		}
	});
	$("#abstract_spreadinputs_input_list_dynamic_value_column").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list",
			filters: abstract_spreadinputs_input_list_dynamic_value_column_filters
		},
		success: function(response) {
			$("#abstract_spreadinputs_input_list_dynamic_value_column-check-loading").fadeOut("fast");
			$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
			$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option label=\"default\" value=\"\"><?php echo translate(""); ?></option>");
			$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option value=\"" + response.values[i]['modules_db_name'] + "\">" + response.values[i]['modules_name'] + " (ID: " + response.values[i]['modules_db_name'] + ")</option>");
				}
				$("#abstract_spreadinputs_input_list_dynamic_value_column").val($("#abstract_spreadinputs_input_list_dynamic_value_column option:first").val()).trigger("change");
			}
		}
	});
	$("#abstract_spreadinputs_file_selector_type").val($("#abstract_spreadinputs_file_selector_type option:first").val()).trigger("change");
	$("#abstract_spreadinputs_file_selector_type-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_date_format").val($("#abstract_spreadinputs_date_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_date_format-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_color_format").val($("#abstract_spreadinputs_color_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_color_format-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_tags_format").val($("#abstract_spreadinputs_tags_format option:first").val()).trigger("change");
	$("#abstract_spreadinputs_tags_format-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_image_folder").keyup(function(event){
		$("#abstract_spreadinputs_image_folder_stringlength").empty().append($(this).val().length + "/200");
		$("#abstract_spreadinputs_image_folder_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-success");
		} else {
			$("#abstract_spreadinputs_image_folder_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_spreadinputs_gridWidth").ionRangeSlider();
	$("#abstract_spreadinputs_alignment").val($("#abstract_spreadinputs_alignment option:first").val()).trigger("change");
	$("#abstract_spreadinputs_alignment-check-loading").fadeOut("fast");
	$("#abstract_spreadinputs_input_list_dynamic_module").change(function () {
		$.ajax({
			url: url,
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				method: "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list",
				parameters: $(this).val()
			},
			success: function(response_modules_pages_parent_link_field) {
				$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
				if (response_modules_pages_parent_link_field.values != null) {
					for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
						$("#abstract_spreadinputs_input_list_dynamic_id_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
						if (response_modules_pages_parent_link_field.values.length - i == 1) {
							$("#abstract_spreadinputs_input_list_dynamic_id_column").select2("val", "");
						}
					}
				} else {
					$("#abstract_spreadinputs_input_list_dynamic_id_column").empty();
				}
			}
		});
		$.ajax({
			url: url,
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				method: "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list",
				parameters: $(this).val()
			},
			success: function(response_modules_pages_parent_link_field) {
				$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
				if (response_modules_pages_parent_link_field.values != null) {
					for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
						$("#abstract_spreadinputs_input_list_dynamic_value_column").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
						if (response_modules_pages_parent_link_field.values.length - i == 1) {
							$("#abstract_spreadinputs_input_list_dynamic_value_column").select2("val", "");
						}
					}
				} else {
					$("#abstract_spreadinputs_input_list_dynamic_value_column").empty();
				}
			}
		});
	});
	
	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_abstract_spreadinputs_table();
	} else {
		create_abstract_spreadinputs_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_abstract_spreadinputs_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_abstract_spreadinputs_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_abstract_spreadinputs_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().abstract_spreadinputs_id != null) {
			edit_abstract_spreadinputs_open(get_url_param().abstract_spreadinputs_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().abstract_spreadinputs_id != null) {
			copy_abstract_spreadinputs_open(get_url_param().abstract_spreadinputs_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().abstract_spreadinputs_id != null) {
			translate_abstract_spreadinputs_open(get_url_param().abstract_spreadinputs_id, get_url_param().abstract_spreadinputs_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().abstract_spreadinputs_id != null) {
			view_abstract_spreadinputs_open(get_url_param().abstract_spreadinputs_id);
		}
	}
	/* URL response (end) */
	
	
	if ($("#abstract_spreadinputs_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}
	
	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");
	
});
</script>
<!-- initialization: JavaScript (end) -->

<script>
/* 
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");
        
jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_abstract_spreadinputs_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"abstract_spreadinputs_label": {
					required: true,
				},
				"abstract_spreadinputs_varname": {
					required: false,
					no_specialchar_hard: true,
				},
				"abstract_spreadinputs_type": {
					required: true,
				},
				"abstract_spreadinputs_references": {
					required: true,
				},
				"abstract_spreadinputs_placeholder": {
					required: false,
				},
				"abstract_spreadinputs_help": {
					required: false,
				},
				"abstract_spreadinputs_require": {
					required: false,
				},
				"abstract_spreadinputs_readonly": {
					required: false,
				},
				"abstract_spreadinputs_disable": {
					required: false,
				},
				"abstract_spreadinputs_hidden": {
					required: false,
				},
				"abstract_spreadinputs_validate_string_min": {
					required: false,
				},
				"abstract_spreadinputs_validate_string_max": {
					required: false,
				},
				"abstract_spreadinputs_validate_number_min": {
					required: false,
				},
				"abstract_spreadinputs_validate_number_max": {
					required: false,
				},
				"abstract_spreadinputs_validate_date_min": {
					required: false,
				},
				"abstract_spreadinputs_validate_date_max": {
					required: false,
				},
				"abstract_spreadinputs_validate_datetime_min": {
					required: false,
				},
				"abstract_spreadinputs_validate_datetime_max": {
					required: false,
				},
				"abstract_spreadinputs_validate_password_equal_to": {
					required: false,
				},
				"abstract_spreadinputs_validate_email": {
					required: false,
				},
				"abstract_spreadinputs_validate_password": {
					required: false,
				},
				"abstract_spreadinputs_validate_website": {
					required: false,
				},
				"abstract_spreadinputs_validate_no_space": {
					required: false,
				},
				"abstract_spreadinputs_validate_no_specialchar_soft": {
					required: false,
				},
				"abstract_spreadinputs_validate_no_specialchar_hard": {
					required: false,
				},
				"abstract_spreadinputs_validate_upper": {
					required: false,
				},
				"abstract_spreadinputs_validate_lower": {
					required: false,
				},
				"abstract_spreadinputs_validate_number": {
					required: false,
				},
				"abstract_spreadinputs_validate_digit": {
					required: false,
				},
				"abstract_spreadinputs_validate_unique": {
					required: false,
				},
				"abstract_spreadinputs_prefix": {
					required: false,
				},
				"abstract_spreadinputs_suffix": {
					required: false,
				},
				"abstract_spreadinputs_default_value_type": {
					required: false,
				},
				"abstract_spreadinputs_default_value": {
					required: false,
				},
				"abstract_spreadinputs_default_switch": {
					required: false,
				},
				"abstract_spreadinputs_input_list": {
					required: false,
				},
				"abstract_spreadinputs_input_list_static_value": {
					required: false,
				},
				"abstract_spreadinputs_input_list_dynamic_module": {
					required: false,
				},
				"abstract_spreadinputs_input_list_dynamic_id_column": {
					required: false,
				},
				"abstract_spreadinputs_input_list_dynamic_value_column": {
					required: false,
				},
				"abstract_spreadinputs_file_selector_type": {
					required: false,
				},
				"abstract_spreadinputs_date_format": {
					required: false,
				},
				"abstract_spreadinputs_color_format": {
					required: false,
				},
				"abstract_spreadinputs_tags_format": {
					required: false,
				},
				"abstract_spreadinputs_image_folder": {
					required: false,
				},
				"abstract_spreadinputs_image_width": {
					required: false,
				},
				"abstract_spreadinputs_image_height": {
					required: false,
				},
				"abstract_spreadinputs_image_width_ratio": {
					required: false,
				},
				"abstract_spreadinputs_image_height_ratio": {
					required: false,
				},
				"abstract_spreadinputs_gridWidth": {
					required: false,
				},
				"abstract_spreadinputs_alignment": {
					required: false,
				},
			 },
			 messages: {
				
				"abstract_spreadinputs_label": {
					required: "<strong><?php echo translate("Spread Input Label"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_varname": {
					required: "<strong><?php echo translate("Spread Input Variable Name"); ?></strong> <?php echo translate("is required"); ?>",
					no_specialchar_hard: "<strong><?php echo translate("Spread Input Variable Name"); ?></strong> <?php echo translate("does not allow special characters and white spaces"); ?>",
				},
				"abstract_spreadinputs_type": {
					required: "<strong><?php echo translate("Spread Input Type"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_references": {
					required: "<strong><?php echo translate("Reference"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_placeholder": {
					required: "<strong><?php echo translate("Spread Input Placeholder"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_help": {
					required: "<strong><?php echo translate("Spread Input Help"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_require": {
					required: "<strong><?php echo translate("Require"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_readonly": {
					required: "<strong><?php echo translate("Read-only"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_disable": {
					required: "<strong><?php echo translate("Disabled"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_hidden": {
					required: "<strong><?php echo translate("Hidden"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_string_min": {
					required: "<strong><?php echo translate("Validate Minimum Strings"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_string_max": {
					required: "<strong><?php echo translate("Validate Maximum String"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_number_min": {
					required: "<strong><?php echo translate("Validate Minimum Number"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_number_max": {
					required: "<strong><?php echo translate("Validate Maximum Number"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_date_min": {
					required: "<strong><?php echo translate("Validate Minimum Date"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_date_max": {
					required: "<strong><?php echo translate("Validate Maximum Date"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_datetime_min": {
					required: "<strong><?php echo translate("Validate Minimum Date Time"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_datetime_max": {
					required: "<strong><?php echo translate("Validate Maximum Date Time"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_password_equal_to": {
					required: "<strong><?php echo translate("Password Equal To Element"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_email": {
					required: "<strong><?php echo translate("Validate Email"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_password": {
					required: "<strong><?php echo translate("Validate Password"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_website": {
					required: "<strong><?php echo translate("Validate Website (URL)"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_no_space": {
					required: "<strong><?php echo translate("Validate Not Allow Spaces"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_no_specialchar_soft": {
					required: "<strong><?php echo translate("Validate Not Allow Special Characters (Soft)"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_no_specialchar_hard": {
					required: "<strong><?php echo translate("Validate Not Allow Special Characters (Hard)"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_upper": {
					required: "<strong><?php echo translate("Validate Uppercase Characters"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_lower": {
					required: "<strong><?php echo translate("Validate Lowercase Characters"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_number": {
					required: "<strong><?php echo translate("Validate Number"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_digit": {
					required: "<strong><?php echo translate("Validate Digit"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_validate_unique": {
					required: "<strong><?php echo translate("Validate Unique"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_prefix": {
					required: "<strong><?php echo translate("Prefix"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_suffix": {
					required: "<strong><?php echo translate("Suffix"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_default_value_type": {
					required: "<strong><?php echo translate("Default Value Type"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_default_value": {
					required: "<strong><?php echo translate("Default Values"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_default_switch": {
					required: "<strong><?php echo translate("Default Switch/Checkbox (Like Switch)"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_input_list": {
					required: "<strong><?php echo translate("List Type"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_input_list_static_value": {
					required: "<strong><?php echo translate("Static Value for List"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_input_list_dynamic_module": {
					required: "<strong><?php echo translate("Module for Dynamic List"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_input_list_dynamic_id_column": {
					required: "<strong><?php echo translate("Module ID Column Name for Dynamic List"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_input_list_dynamic_value_column": {
					required: "<strong><?php echo translate("Module Value Column Name for Dynamic List"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_file_selector_type": {
					required: "<strong><?php echo translate("File Selector Type"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_date_format": {
					required: "<strong><?php echo translate("Date Format"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_color_format": {
					required: "<strong><?php echo translate("Color Format"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_tags_format": {
					required: "<strong><?php echo translate("Tags Format"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_image_folder": {
					required: "<strong><?php echo translate("Image/File Folder Name"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_image_width": {
					required: "<strong><?php echo translate("Image Width"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_image_height": {
					required: "<strong><?php echo translate("Image Height"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_image_width_ratio": {
					required: "<strong><?php echo translate("Image Width Ratio"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_image_height_ratio": {
					required: "<strong><?php echo translate("Image Height Ratio"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_gridWidth": {
					required: "<strong><?php echo translate("Grid Width for Input"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_spreadinputs_alignment": {
					required: "<strong><?php echo translate("Alignment for Input"); ?></strong> <?php echo translate("is required"); ?>",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once("templates/".$configs["backend_template"]."/footer.php");
?>