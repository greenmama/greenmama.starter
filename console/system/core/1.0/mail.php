<?php 
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}

if (isset($method) && $method != "" 
	&& ($method == "send_mail")
) {
	
	/* include: configurations */
	require_once("../../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");

	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");

	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}

	if ($method == "send_mail"){
		send_mail($_POST["recipients"], $_POST["sender_name"], $_POST["sender_email"], $_POST["subject"], $_POST["body"], $_POST["ccs"], $_FILES['attachment'], $_POST["is_html"], $_POST["is_smtp"], $_POST["smtp_auth"], $_POST["smtp_username"], $_POST["smtp_password"]);
	}
	
}

function send_mail($recipients, $sender_name, $sender_email, $subject, $body, $ccs = "", $attachments = "", $is_html = true, $is_smtp = false,$smtp_auth = "", $smtp_username = "", $smtp_password = "") {

	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	$current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if (strpos($current_url, "/system") !== false && strpos($current_url, "/core") !== false && strpos($current_url, "/".$configs["version"]) !== false) {
		require_once("../../../plugins/phpmailer/PHPMailerAutoload.php");
	} else if (strpos($current_url, "/system") !== false) {
		require_once("../plugins/phpmailer/PHPMailerAutoload.php");
	} else {
		require_once("plugins/phpmailer/PHPMailerAutoload.php");
	}
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$recipients = escape_string($con, $recipients);
	$sender_name = escape_string($con, $sender_name);
	$sender_email = escape_string($con, $sender_email);
	$subject = escape_string($con, $subject);
	$ccs = escape_string($con, $ccs);
	$is_html = escape_string($con, $is_html);
	
	$parameters = array();
	$parameters["recipients"] = $recipients;
	$parameters["sender_name"] = $sender_name;
	$parameters["sender_email"] = $sender_email;
	$parameters["subject"] = $subject;
	$parameters["body"] = $body;
	$parameters["ccs"] = $ccs;
	$parameters["is_html"] = $is_html;
	$parameters["is_smtp"] = $is_smtp;
	$parameters["smtp_auth"] = $smtp_auth;
	$parameters["smtp_username"] = $smtp_username;
	$parameters["smtp_password"] = $smtp_password;
	
	if(!isset($parameters["recipients"]) or empty($parameters["recipients"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Recipient") . "</strong> " . translate("is required");
		$response["target"] = "#recipients";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["sender_email"]) or empty($parameters["sender_email"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Sender Email") . "</strong> " . translate("is required");
		$response["target"] = "#sender_email";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["subject"]) or empty($parameters["subject"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Subject") . "</strong> " . translate("is required");
		$response["target"] = "#subject";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["body"]) or empty($parameters["body"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Body") . "</strong> " . translate("is required");
		$response["target"] = "#body";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	$mail = new PHPMailer();
	
	try {

        if ($parameters["is_smtp"]) {
            $mail->isSMTP();
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            $mail->Debugoutput = '';
            //Set the hostname of the mail server
            $mail->Host = $configs["smtp_host"];
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->Port = $configs["smtp_port"];
            //Set the encryption system to use - ssl (deprecated) or tls
            $mail->SMTPSecure = $configs["smtp_secure"];
			//Whether to use SMTP authentication
			if (!isset($parameters["smtp_auth"]) || $parameters["smtp_auth"] == "") {
				if (!isset($configs["smtp_auth"]) || $configs["smtp_auth"] == "") {
					$parameters["smtp_auth"] = false;
				} else {
					$parameters["smtp_auth"] = $configs["smtp_auth"];
				}
			}
			$mail->SMTPAuth = $parameters["smtp_auth"];
			if ($parameters["smtp_auth"]) {
				//Username to use for SMTP authentication
				if (!isset($parameters["smtp_username"]) || $parameters["smtp_username"] == "") {
					if (!isset($configs["smtp_system_email_username"]) || $configs["smtp_system_email_username"] == "") {
						$parameters["smtp_username"] = "";
					} else {
						$parameters["smtp_username"] = $configs["smtp_system_email_username"];
					}
				}
				$mail->Username = $parameters["smtp_username"];
				unset($parameters["smtp_username"]);
				//Password to use for SMTP authentication
				if (!isset($parameters["smtp_password"]) || $parameters["smtp_password"] == "") {
					if (!isset($configs["smtp_system_email_password"]) || $configs["smtp_system_email_password"] == "") {
						$parameters["smtp_password"] = "";
					} else {
						$parameters["smtp_password"] = $configs["smtp_system_email_password"];
					}
				}
				$mail->Password = $parameters["smtp_password"];
				unset($parameters["smtp_password"]);
			}
			
			$mail->SMTPOptions = array(
				'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
				)
			);
		}

		$mail->CharSet = 'UTF-8';
		
		$mail->From      = $parameters["sender_email"];
		$mail->FromName  = $parameters["sender_name"];
		$mail->Subject   = $parameters["subject"];
		
		if (is_array($parameters["recipients"])) {
			for ($i = 0; $i < count($parameters["recipients"]); $i++) {
				$mail->AddAddress($parameters["recipients"][$i]);
			}
		} else {
			$mail->AddAddress($parameters["recipients"]);
		}
		
		if (isset($parameters["ccs"]) && !empty($parameters["ccs"])) {
			if (is_array($parameters["ccs"])) {
				for ($i = 0; $i < count($parameters["ccs"]); $i++) {
					$mail->AddCC($parameters["ccs"][$i]);
				}
			} else {
				$mail->AddCC($parameters["ccs"]);
			}
		}
		
		if (isset($attachments) && !empty($attachments)) {
	            if (isset($attachments) && $attachments['error'] == UPLOAD_ERR_OK) {
	                $mail->AddAttachment($attachments['tmp_name'], $attachments['name']);
	            }
		}
		
		if (isset($attachments) && !empty($attachments)) {
			if (is_array($attachments)) {
				for ($i = 0; $i < count($attachments); $i++) {
					$mail->AddAttachment($attachments[$i], basename($attachments[$i]));
				}
			} else {
				$mail->AddAttachment($attachments, basename($attachments));
			}
		}

		$body_original = $parameters["body"];
		$body = $body_original;

		if ($parameters["is_html"] == true || $parameters["is_html"] == "" || !isset($parameters["is_html"])) {
			$mail->IsHTML(true); 
		} else {
			$body = stripslashes(str_replace('\r\n', '
', $body));
			$body = preg_replace('/[\n\t]+/', '', $body);
		}

		$mail->Body = $body;
		
		$send_result = $mail->Send();

		$response['status'] = true;
		$response['message'] = translate($send_result);

		/* log */
		if ($configs["log"]) {
			$log = array(
				"log_name" => "send mail",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => $sessions,
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "frontend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "",
				"modules_record_target" => "",
				"modules_id" => "",
				"modules_name" => "",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}
		
	} catch (phpmailerException $e) {
		
	   $response['status'] = false;
	   $response['message'] = $e;
		
	} catch (Exception $e) {
		
		$response['status'] = false;
		$response['message'] = $e;
		
	}
	
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response['status'];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}
?>