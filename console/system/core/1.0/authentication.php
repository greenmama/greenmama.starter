<?php 
if (!isset($_SESSION)) {
	session_start();
} 

function authentication_session_users() {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	global $_COOKIE;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$_COOKIE = escape_string($con, $_COOKIE);
	
	ini_set('session.gc_maxlifetime', 7200); 
	if (!authentication_check_user($_SESSION['users_username'], $_SESSION['users_password'])) {
		
		if (isset($_COOKIE[generate_title_link_name($configs['site_name'])]) 
		  && !empty($_COOKIE[generate_title_link_name($configs['site_name'])])) {
			
			$cookies_array = explode(",", $_COOKIE[generate_title_link_name($configs['site_name'])]);
			
			$authentication_cookies = authentication_check_cookies($cookies_array[0], $cookies_array[1]);
			if (!$authentication_cookies || !isset($authentication_cookies) || empty($authentication_cookies)) {
				
				if ((isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"]) 
				     && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"]))
				  || (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"]))
				  || (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"]))
				  || (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"]))
				  || (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"]))
				  || (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"]))) {
					
					if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"]) 
				     && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"], "Facebook");
						$api_access_token = $users_api_sessions["api_access_token"];

							
						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Facebook"]);
						$_SESSION["session_id"] = $session_id;
						
						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					} else if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"], "Google");
						$api_access_token = $users_api_sessions["api_access_token"];
						
						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Google"]);
						$_SESSION["session_id"] = $session_id;

						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					} else if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"], "Twitter");
						$api_access_token = $users_api_sessions["api_access_token"];
							
						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Twitter"]);
						$_SESSION["session_id"] = $session_id;

						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					} else if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"], "Line");
						$api_access_token = $users_api_sessions["api_access_token"];
							
						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Line"]);
						$_SESSION["session_id"] = $session_id;

						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					} else if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"], "Instagram");
						$api_access_token = $users_api_sessions["api_access_token"];

						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Instagram"]);
						$_SESSION["session_id"] = $session_id;

						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					} else if (isset($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"]) 
				      && !empty($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"])) {
						
						$users_api_sessions = get_users_api_sessions_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"], "Microsoft");
						$api_access_token = $users_api_sessions["api_access_token"];
					
						$session_id = session_id();
						
						$_SESSION = get_users_data_by_id($_COOKIE[generate_title_link_name($configs['site_name'])."_Microsoft"]);
						$_SESSION["session_id"] = $session_id;

						$response["status"] = true;
						$response['message'] = translate("Passed authentication");
						
					}
					
				} else {
					
					$response["status"] = false;
					$response["message"] = translate("Failed authentication");
					
				}
				
			} else {
				
				$session_id = session_id();
				
				$_SESSION = get_users_data_by_id($cookies_array[0]);
				$_SESSION["session_id"] = $session_id;
				
				$response["status"] = true;
				$response['message'] = translate("Passed authentication");
				
			}
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Failed authentication");
			
		}
		
	} else {
		
		$response["status"] = true;
		$response['message'] = translate("Passed authentication");
		
	}
	
	/* update API sessions */ 
	if ($response["status"]) {
		// if (isset($_SESSION["users_api_sessions"]) && !empty($_SESSION["users_api_sessions"])) {
		// 	$users_api_sessions = $_SESSION["users_api_sessions"];
		// 	for ($i = 0; $i < count($users_api_sessions); $i++) {
		// 		$authentication_check_api_sessions = authentication_check_api_sessions($users_api_sessions[$i]["api_name"], $users_api_sessions[$i]["api_access_token"]);
		// 		if (!$authentication_check_api_sessions) {
		// 			if ($users_api_sessions[$i]["api_name"] == "Facebook") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Facebook", $configs["backend_facebook_app_redirect_url"], explode(",", $configs["backend_facebook_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			} else if ($users_api_sessions[$i]["api_name"] == "Google") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Google", $configs["backend_google_app_redirect_url"], explode(",", $configs["backend_google_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			} else if ($users_api_sessions[$i]["api_name"] == "Twitter") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Twitter", $configs["backend_twitter_app_redirect_url"], explode(",", $configs["backend_twitter_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			} else if ($users_api_sessions[$i]["api_name"] == "Line") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Line", $configs["backend_line_app_redirect_url"], str_replace(",", " ", $configs["backend_line_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			} else if ($users_api_sessions[$i]["api_name"] == "Instagram") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Instagram", $configs["backend_instagram_app_redirect_url"], explode(",", $configs["backend_instagram_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			} else if ($users_api_sessions[$i]["api_name"] == "Microsoft") {
		// 				$login_url = create_api_connect_link($_SESSION["users_id"], "Microsoft", $configs["backend_microsoft_app_redirect_url"], explode(",", $configs["backend_microsoft_app_scope"]));
		// 				header("Location: ". $login_url);
		// 			}
		// 		}
				
		// 	}
			
		// }
	}
	
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function authentication_connect_api_session ($api_name, $credential) {

	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	global $_COOKIE;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$_COOKIE = escape_string($con, $_COOKIE);
	$api_name = escape_string($con, $api_name);
	$credential = escape_string($con, $credential);

	/* check API sessions (begin) */ 

	if ($api_name == "Facebook") {
		/* check code for Facebook (begin) */

		if (isset($_GET["state"]) && isset($_SESSION["facebook_api_state"])
		&& $_SESSION["facebook_api_state"] == $_GET["state"]) {
			require_once("plugins/Facebook/autoload.php");

			$fb = new Facebook\Facebook($credential);
			
			$helper = $fb->getRedirectLoginHelper();

			try {
				$accessToken = $helper->getAccessToken();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				$response["api_error"] = translate('Graph returned an error: ' . $e->getMessage());
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				$response["api_error"] = translate('Facebook SDK returned an error: ' . $e->getMessage());
			}
			if (!$accessToken) {
				$helper_js = $fb->getJavaScriptHelper();
				try {
					$accessToken = $helper_js->getAccessToken();
				} catch(Facebook\Exceptions\FacebookResponseException $e) {
					$response["api_error"] = translate('Graph returned an error: ' . $e->getMessage());
				} catch(Facebook\Exceptions\FacebookSDKException $e) {
					$response["api_error"] = translate('Facebook SDK returned an error: ' . $e->getMessage());
				}
			}
			if ($accessToken) {
				try {
					$graph = $fb->get('/me?fields=id,first_name,last_name,picture,'.implode(",", $credential["scope"]), (string) $accessToken);
				} catch(Facebook\Exceptions\FacebookResponseException $e) {
					$response["api_error"] = translate('Graph returned an error: ' . $e->getMessage());
				} catch(Facebook\Exceptions\FacebookSDKException $e) {
					$response["api_error"] = translate('Facebook SDK returned an error: ' . $e->getMessage());
				}
				if (isset($graph)) {
					$response_profile = (array) json_decode($graph->getGraphUser());
					$response_profile["picture"] = $response_profile["picture"]->url;
				} else {
					$response_profile = array();
				}
				$new_api_sessions = array(
					"api_name" => "Facebook",
					"api_access_token" => $accessToken,
					"api_details" => $response_profile,
				);
				if ($response["status"] && isset($_SESSION["users_id"]) && !empty($_SESSION["users_id"])) {
					connect_api_sessions($new_api_sessions, $_SESSION["users_id"]);
				} else {
					if (!connect_api_sessions($new_api_sessions)) {
						$parameters = array(
							"users_username" => $response_profile["id"],
							"users_name" => $response_profile["first_name"],
							"users_last_name" => $response_profile["last_name"],
							"users_email" => $response_profile["email"],
							"groups_id" => "0",
							"users_level" => "6",
							"users_email_notify" => "1",
							"users_protected" => "0",
							"users_api_sessions" => serialize(array($new_api_sessions)),
							"users_activate" => "1",
						);
						$users_data = create_users_data($parameters);
						if ($users_data) {
							
							$patch_parameters = array(
								"users_id" => $users_data["users_id"], 
								"target_field" => "users_image", 
								"value" => "https://graph.facebook.com/".$response_profile["id"]."/picture?type=large"
							);
							patch_users_data($patch_parameters);
							
							$session_id = session_id();

							$_SESSION = get_users_data_by_id($users_data["users_id"]);
							
							$_SESSION["session_id"] = $session_id;
							setcookie(generate_title_link_name($configs['site_name'])."_".$new_api_sessions["api_name"], $_SESSION["users_id"], time() + (86400 * 90), "/");
							
							$response["status"] = true;
							$response['message'] = translate('Successfully logged in');
							
						} else {
							
							$response["status"] = true;
							$response['message'] = translate('Unable to login');
							
						}
						
					} else {
						
						$response["status"] = true;
						$response['message'] = translate('Successfully logged in');
						
					}
				}

			}
		}
		unset($_SESSION["facebook_api_state"]);
		/* check code for Facebook (end) */
	} else if ($api_name == "Line") {
		/* check code for Line (begin) */
		   echo "test";
		if (isset($_GET["state"]) && isset($_SESSION["line_api_state"]) 
		&& $_SESSION["line_api_state"] == $_GET["state"]) {
			$code = $_GET["code"];
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.line.me/oauth2/v2.1/token",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=".$code."&client_id=".$credential["app_id"]."&client_secret=".$credential["app_secret"]."&redirect_uri=".$credential["redirect_url"],
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: application/x-www-form-urlencoded"
				),
			));
			$response_authen = json_decode(curl_exec($curl));
			curl_close($curl);
			if ($response_authen && $response_authen->access_token) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://api.line.me/v2/profile",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						"Authorization: Bearer ".$response_authen->access_token
					),
				));
				$response_profile = (array) json_decode(curl_exec($curl));
				curl_close($curl);
				$new_api_sessions = array(
					"api_name" => "Line",
					"api_access_token" => $response_authen->access_token,
					"api_refresh_token" => $response_authen->refresh_token,
					"api_details" => $response_profile,
				);
				if ($response["status"] && isset($_SESSION["users_id"]) && !empty($_SESSION["users_id"])) {
					connect_api_sessions($new_api_sessions, $_SESSION["users_id"]);
				} else {
					if (!connect_api_sessions($new_api_sessions)) {
						$parameters = array(
							"users_username" => $response_profile["userId"],
							"users_name" => $response_profile["displayName"],
							"users_last_name" => "",
							"users_email" => "",
							"groups_id" => "0",
							"users_level" => "6",
							"users_email_notify" => "1",
							"users_protected" => "0",
							"users_api_sessions" => serialize(array($new_api_sessions)),
							"users_activate" => "1",
						);
						$users_data = create_users_data($parameters);
						if ($users_data) {
							
							$patch_parameters = array(
								"users_id" => $users_data["users_id"], 
								"target_field" => "users_image", 
								"value" => $response_profile["pictureUrl"]
							);
							patch_users_data($patch_parameters);

							$session_id = session_id();

							$_SESSION = get_users_data_by_id($users_data["users_id"]);
							
							$_SESSION["session_id"] = $session_id;

							$response["status"] = true;
							$response['message'] = translate('Successfully logged in');

						} else {

							$response["status"] = false;
							$response['message'] = translate('Unable to login');

						}

					} else {

						$response["status"] = true;
						$response['message'] = translate('Successfully logged in');

					}
				}
			}
		}
		unset($_SESSION["line_api_state"]);
		/* check code for Line (end) */
	}
	
		
	/* check API sessions (end) */ 

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function authentication_session_modules($module_page) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$module_page = escape_string($con, $module_page);
	
	ini_set('session.gc_maxlifetime', 7200); 
	if (!authentication_check_modules($_SESSION['users_id'], $module_page)) {
		
		$response["status"] = false;
		$response["message"] = translate("Permission denied");
		
	} else {
		
		$response["status"] = true;
		$response["message"] = translate("Permission accepted");
		
	}
	
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function authentication_check_user($username, $password_encrypted) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$username = escape_string($con, $username);
	$password_encrypted = escape_string($con, $password_encrypted);		

	$sql = "SELECT `users_id` 
	        FROM   `users` 
			WHERE  `users_username` = '".$username."' 
			  AND  `users_password` = '".$password_encrypted."'
			  AND  (`users_level` = 1 
			  		OR `users_level` = 2
			  		OR `users_level` = 3
			  		OR `users_level` = 4
					OR `users_level` = 5)
			  AND  `users_activate` = true 
			LIMIT  1";
	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);
	
	if ($result) {
		
		if ($num == 1) {
			
			$response["status"] = true;
			$response["message"] = translate("Valid user");
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Invalid user");
			
		}
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}
	
	mysqli_free_result($result);
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function authentication_check_cookies($users_id, $session_id) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$users_id = escape_string($con, $users_id);	
	$session_id = escape_string($con, $session_id);	
	
	$target_serialized = preg_replace("/a\:[0-9]+\:\{(s\:10\:\"session\_id\"\;[\w\:\"]+)\;\}/", "$1", serialize(array("session_id" => $session_id)));
	
	$sql = "SELECT `users_id` 
	        FROM `users` 
			WHERE `users_remember_login` LIKE '%".$target_serialized."%' 
			  AND `users_id` = '".$users_id."' 
			  AND `users_activate` = true 
			LIMIT  1";
	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);
	
	if ($result) {
		
		if ($num > 0) {
			
			$response["status"] = true;
			$response["message"] = translate("Valid session");
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Invalid session");
			
		}
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}
	
	mysqli_free_result($result);
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function authentication_check_modules($users_id, $module_page) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$users_id = escape_string($con, trim($users_id));
	$module_page = escape_string($con, trim($module_page));
	
	if ($_SESSION['users_level'] != '1') {
		
		$sql_modules = "
		SELECT `groups`.`modules_ids`
		FROM   `groups` 
		LEFT JOIN `users` ON `users`.`groups_id` = `groups`.`groups_id`
		WHERE  `users`.`users_id` = '".$users_id."' 
		LIMIT  1";
		$result_modules = mysqli_query($con, $sql_modules);
		$num_modules = mysqli_num_rows($result_modules);
		
		if ($result_modules) {
		
			if ($num_modules > 0) {

				$query_modules = mysqli_fetch_array($result_modules);
				$module_list = unserialize($query_modules["modules_ids"]);

				$sql = "SELECT `modules_id`  
						FROM   `modules` 
						WHERE  `modules_link` = '".$module_page."' 
						   AND `modules_activate` = true 
						LIMIT  1";
				$result = mysqli_query($con, $sql);
				$num = mysqli_num_rows($result);

				if ($result) {

					$query = mysqli_fetch_array($result);

					$module_id = $query['modules_id']; 

					if (count($module_list) > 0 && $module_list[0] != "") {
						
						if(in_array($module_id, $module_list)){
							
							$response["status"] = true;
							$response["message"] = translate("Found modules");
							
						} else { 
							
							$response["status"] = false;
							$response["message"] = translate("Not found");
							
						}
						
					} else {
						
						$response["status"] = false;
						$response["message"] = translate("Empty data");
						
					}

				} else {
					
					$response["status"] = false;
					$response["target"] = "modules";
					if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
						$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
					} else {
						$response["message"] = translate("Database encountered error");
					}
					
				}
				
				mysqli_free_result($result);

			} else {
				
				$response["status"] = false;
				$response["message"] = translate("Empty data");
				
			}
			
		} else {
			
			$response["status"] = false;
			$response["target"] = "groups,users";
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>groups</strong> or <strong>users</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
			
		}
		
		mysqli_free_result($result_modules);
		
	} else {
		
		$response["status"] = true;
		$response["message"] = translate("Super administrator found");
		
	}

	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function authentication_check_api_sessions($api_name, $credential, $api_access_token) {
	
	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$api_name = escape_string($con, $api_name);
	$api_access_token = escape_string($con, $api_access_token);
	$credential = escape_string($con, $credential);
	
	if ($api_name == "Facebook") {

		require_once("plugins/Facebook/autoload.php");

		$fb = new Facebook\Facebook($credential);

		$helper = $fb->getRedirectLoginHelper();

		if ($helper->getError()) {
			$response["facebook_status"] = false;
		} else {
			$response["facebook_status"] = false;
			$response["facebook_message"] = translate("Bad request");
		}

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();
		
		try {
			// Get the access token metadata from /debug_token
			$tokenMetadata = $oAuth2Client->debugToken($api_access_token);

			// Validation (these will throw FacebookSDKException's when they fail)
			$tokenMetadata->validateAppId($configs["facebook_app_id"]);
			$tokenMetadata->validateExpiration();

			date_default_timezone_set($configs["timezone"]);
			$expiresAt = (array)$tokenMetadata->getField('expires_at');
			$is_valid = (string)$tokenMetadata->getField('is_valid');
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$response["api_error"] = translate('Graph returned an error: ' . $e->getMessage());
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$response["api_error"] = translate('Facebook SDK returned an error: ' . $e->getMessage());
		}

		if (isset($expiresAt["date"]) && $expiresAt["date"] >= date("Y-m-d H:i:s") && isset($is_valid) && $is_valid == "true") {

			$response["status"] = true;
			$response["message"] = translate("Valid session");

		} else {

			$response["status"] = false;
			$response["message"] = translate("Invalid session");

		}

	} else if ($api_name == "Google") {



	} else if ($api_name == "Twitter") {



	} else if ($api_name == "Line") {
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.line.me/oauth2/v2.1/verify?access_token=".$api_access_token,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response_verify = json_decode(curl_exec($curl));
		curl_close($curl);
		
		if ($response_verify->expires_in > 0) {
			
			$response["status"] = true;
			$response["message"] = translate("Valid session");
			
		} else {
			
			$line_api_sessions = get_users_api_sessions_by_id($_SESSION["users_api_sessions"], "Line");
			
			if (isset($line_api_sessions["api_refresh_token"]) && !empty($line_api_sessions["api_refresh_token"])) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://api.line.me/oauth2/v2.1/token",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=".$line_api_sessions["api_refresh_token"]."&client_id=".$credential["app_id"]."&client_secret=".$credential["app_secret"]."&redirect_uri=".$credential["redirect_url"],
					CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded"
					),
				));
				$response_authen = json_decode(curl_exec($curl));
				curl_close($curl);
				
				if ($response_authen && $response_authen->access_token) {
					
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.line.me/v2/profile",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => array(
							"Authorization: Bearer ".$response_authen->access_token
						),
					));
					$response_profile = json_decode(curl_exec($curl));
					curl_close($curl);
					$new_api_sessions = array(
						"api_name" => "Line",
						"api_access_token" => $response_authen->access_token,
						"api_refresh_token" => $response_authen->refresh_token,
						"api_details" => (array) $response_profile,
					);
					connect_api_sessions($new_api_sessions);
					
					$response["status"] = true;
					$response["message"] = translate("Valid session");
					
				} else {
					
					$response["status"] = false;
					$response["message"] = translate("Empty access token");
					
				}
				
			} else {
				
				$response["status"] = false;
				$response["message"] = translate("Empty refresh token");
				
			}

		}

	} else if ($api_name == "Instagram") {



	} else if ($api_name == "Microsoft") {



	}

	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function authentication_get_users_id($username, $password_encrypted) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$username = escape_string($con, trim($username));
	$password_encrypted = escape_string($con, trim($password_encrypted));
	
	$sql = "SELECT `users_id` 
	        FROM   `users` 
			WHERE  `users_username` = '".$username."' 
			   AND `users_password` = '".$password_encrypted."'
			LIMIT  1";
	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);
	
	if ($result) {
		
		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$response["status"] = true;
			$response["values"] = $query["users_id"];
			$response["message"] = translate("Super administrator found");
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Empty data");
			
		}
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}

	mysqli_free_result($result);
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function authentication_update_recent($users_id, $remember_session = "") {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$users_id = escape_string($con, trim($users_id));
	$date_recent = escape_string($con, trim(date("Y-m-d H:i:s")));
	$ip_recent = escape_string($con, trim($_SERVER["REMOTE_ADDR"]));
	
	if (isset($remember_session) && $remember_session != "") {
		$extended_command = ", `users_remember_login` = '".serialize($remember_session)."'";
	}
	
	$sql = "UPDATE `users` 
	        SET    `users_date_recent` = '".$date_recent."', 
			       `users_ip_recent` = '".$ip_recent."'
				   ".$extended_command."
		    WHERE  `users_id` = '".$users_id."'";
	$result = mysqli_query($con,$sql);
	
	if ($result) {
		
		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		
	} else {
		
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		
	}
	
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function authentication_deny() {
	
	/* get global: configurations */
	global $configs;
	
	logout();
	
	header("Location: ".backend_rewrite_url("login.php")); 
	exit();
	
}

function authentication_permission_deny() {
	
	/* get global: configurations */
	global $configs;

	header("Location: ".backend_rewrite_url("403.php")); 
	exit();
	
}
?>