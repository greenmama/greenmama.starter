<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "login"
		|| $method == "logout"
		|| $method == "signup"
		|| $method == "connect_api_sessions"
		|| $method == "disconnect_api_sessions"
		|| $method == "check_api_sessions"
		|| $method == "get_users_data_by_id" 
		|| $method == "get_users_data_by_link" 
		|| $method == "get_users_data_by_current_session_id" 
		|| $method == "get_users_api_sessions_by_id"
		|| $method == "get_users_data_all" 
		|| $method == "count_users_data_all" 
		|| $method == "get_users_data_all_excluding" 
		|| $method == "count_users_data_all_excluding" 
		|| $method == "get_users_main_language_data_all" 
		|| $method == "count_users_main_language_data_all" 
		|| $method == "get_users_main_language_data_all_excluding" 
		|| $method == "count_users_main_language_data_all_excluding" 
		|| $method == "get_users_by_languages_id_data_all" 
		|| $method == "count_users_by_languages_id_data_all" 
		|| $method == "get_users_by_languages_short_name_data_all" 
		|| $method == "count_users_by_languages_short_name_data_all" 
		|| $method == "get_users_translation_data_all" 
		|| $method == "count_users_translation_data_all" 
		|| $method == "get_users_data_table_all" 
		|| $method == "count_users_data_table_all" 
		|| $method == "get_search_users_data_table_all" 
		|| $method == "count_search_users_data_table_all" 
		|| $method == "create_users_data" 
		|| $method == "update_users_data" 
		|| $method == "update_users_personal_data" 
		|| $method == "update_users_password_data" 
		|| $method == "patch_users_data" 
		|| $method == "delete_users_data" 
		|| $method == "create_users_table" 
		|| $method == "create_users_table_row" 
		|| $method == "update_users_table_row" 
		|| $method == "delete_users_table_row" 
		|| $method == "create_users_table_row" 
		|| $method == "update_users_table_row" 
		|| $method == "delete_users_table_row" 
		|| $method == "inform_users_table_row" 
		|| $method == "view_users_table_row" 
		|| $method == "check_users_title" 
		|| $method == "check_users_link" 
		|| $method == "check_users_meta_title" 
		|| $method == "check_users_meta_description" 
		|| $method == "generate_users_link" 
		|| $method == "get_users_edit_log_data" 
		|| $method == "count_users_edit_log_data" 
		|| $method == "get_users_edit_log_data_by_id" 
		|| $method == "count_users_edit_log_data_by_id"
		|| $method == "upload_users_files"
		
		|| $method == "get_users_data_all_by_groups_id" 
		|| $method == "count_users_data_all_by_groups_id" 
		|| $method == "get_groups_id_data_dynamic_list" 
		|| $method == "count_groups_id_data_dynamic_list" 
		|| $method == "get_groups_id_data_dynamic_list_by_id")
) {
	
	/* include: configurations */
	require_once("../../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	require_once("mail.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");


	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);
	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	if ($method == "login"){
		login($_POST);
	} else if ($method == "logout"){
		logout();
	} else if ($method == "signup"){
		signup($_POST, $_POST["api_parameters"]);
	} else if ($method == "connect_api_sessions"){
		connect_api_sessions($parameters);
	} else if ($method == "disconnect_api_sessions"){
		disconnect_api_sessions($parameters);
	} else if ($method == "check_api_sessions"){
		check_api_sessions($parameters);
		
	} else if ($method == "get_users_data_by_id"){
		get_users_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_users_data_by_current_session_id"){
		get_users_data_by_current_session_id();
	} else if ($method == "get_users_data_all"){
		get_users_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_users_api_sessions_by_id"){
		get_users_api_sessions_by_id($parameters, $_POST["api_name"]);
	} else if ($method == "count_users_data_all"){
		count_users_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_users_data_all_excluding"){
		get_users_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_users_data_all_excluding"){
		count_users_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_users_data_table_all"){
		get_users_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_users_data_table_all"){
		count_users_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_users_data_table_all"){
		get_search_users_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_users_data_table_all"){
		count_search_users_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_users_data"){
		create_users_data($parameters);
	} else if ($method == "update_users_data"){
		update_users_data($parameters);
	} else if ($method == "update_users_personal_data"){
		update_users_personal_data($parameters);
	} else if ($method == "update_users_password_data"){
		update_users_password_data($parameters);
	} else if ($method == "patch_users_data"){
		patch_users_data($parameters);
	} else if ($method == "delete_users_data"){
		delete_users_data($parameters);
	} else if ($method == "create_users_table"){
		create_users_table($parameters, $table_module_field); 
	} else if ($method == "create_users_table_row"){
		create_users_table_row($parameters, $table_module_field);
	} else if ($method == "update_users_table_row"){
		update_users_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_users_table_row"){
		inform_users_table_row($parameters, $table_module_field);
	} else if ($method == "view_users_table_row"){
		view_users_table_row($parameters, $table_module_field);
	} else if ($method == "get_users_edit_log_data"){
		get_users_edit_log_data($parameters);
	} else if ($method == "count_users_edit_log_data"){
		count_users_edit_log_data($parameters);
	} else if ($method == "get_users_edit_log_data_by_id"){
		get_users_edit_log_data_by_id($parameters);
	} else if ($method == "count_users_edit_log_data_by_id"){
		count_users_edit_log_data_by_id($parameters);
	} else if ($method == "upload_users_files"){
		upload_users_files($_POST);
	} else if ($method == "get_users_data_all_by_groups_id"){
		get_users_data_all_by_groups_id($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_users_data_all_by_groups_id"){
		count_users_data_all_by_groups_id($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_groups_id_data_dynamic_list"){
		get_groups_id_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_groups_id_data_dynamic_list"){
		count_groups_id_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_groups_id_data_dynamic_list_by_id"){
		get_groups_id_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	}
}

function login($parameters) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	global $_COOKIE;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$_COOKIE = escape_string($con, $_COOKIE);
	$parameters = escape_string($con, $parameters);
	
	if(!isset($parameters["username"]) || empty($parameters["username"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
		$response["target"] = "#username";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["password"]) || empty($parameters["password"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is required");
		$response["target"] = "#password";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(!isset($parameters["username"]) || empty($parameters["username"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
		$response["target"] = "#username";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if(isset($parameters[$parameters["form_name"]]) && !empty($parameters[$parameters["form_name"]])){

		$response["status"] = "violent";
		$response["message"] = translate("Unable to access due to inappropriate physical interaction detected");
		$response["target"] = "#".$parameters["form_name"];
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	if (authentication_check_user($parameters['username'], md5($parameters['password'].$configs["password_salt"]))){	

		$users_id = authentication_get_users_id($parameters['username'], md5($parameters['password'].$configs["password_salt"]));

		$users_data = get_users_data_by_id($users_id);
		$session_id = session_id();
		
		$_SESSION = $users_data;
		$_SESSION["session_id"] = $session_id;

		if($parameters['remember_login'] == 1) {

			setcookie(generate_title_link_name($configs['site_name']), $users_id.",".$session_id, time() + (86400 * 90), "/");

			$new_session = array(
				"session_id" => $session_id,
				"detail" => $_SERVER["HTTP_USER_AGENT"],
				"datetime" => gmdate("Y-m-d H:i:s"),
				"ip_address" => $_SERVER["REMOTE_ADDR"],
			);

				
			if (isset($users_data["users_remember_login"]) && !empty($users_data["users_remember_login"])) {

				$sessions = $users_data["users_remember_login"];
				$valid_session = 0;

				for($i = 0; $i < count($sessions); $i++) {
					if ($users_id.",".$sessions[$i]["session_id"] == $_COOKIE[generate_title_link_name($configs['site_name'])]) {
						$valid_session = $valid_session + 1;
					}
				}

				if ($valid_session <= 0) {
					array_push($sessions, $new_session);
				}

			} else {

				$sessions = array($new_session);

			}
			
			$_SESSION["users_remember_login"] = $sessions;

		} else {
			$sessions = "";
		}

		authentication_update_recent($users_id, $sessions);
		
		$response['status'] = true;
		$response['message'] = translate('Successfully logged in');

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "login",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => $sessions,
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $users_id,
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

	} else {
		
		$response['status'] = false;
		$response['message'] = translate("Account is invalid, please try again or contact administrator");
		
	}
	
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function logout() {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	global $_COOKIE;
	
	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	$sql_sessions = "
	SELECT `users_remember_login` 
	FROM `users` 
	WHERE `users_id` = '".$_SESSION['users_id']."'";
	$result_sessions = mysqli_query($con, $sql_sessions);
	
	if ($result_sessions) {
		
		$query_sessions = mysqli_fetch_array($result_sessions);
		
		if (isset($query_sessions["users_remember_login"]) && !empty($query_sessions["users_remember_login"])) {
			
			$sessions = unserialize($query_sessions["users_remember_login"]);
			
			for($i = 0; $i < count($sessions); $i++) {
				
				if ($_SESSION['users_id'].",".$sessions[$i]["session_id"] == $_COOKIE[generate_title_link_name($configs['site_name'])]) {
					
					unset($sessions[$i]);
					
				}
				
			}
			
		}
		
		unset($query_sessions);
		$query_sessions = array();
		
		mysqli_free_result($result_sessions);
		
	}

	$response['status'] = true;
	$response['message'] = translate("Successfully logged out");
	
	/* log */
	if ($configs["backend_log"]) {
		$log = array(
			"log_name" => "logout",
			"log_function" => __FUNCTION__,
			"log_violation" => "low",
			"log_content_hash" => $_SESSION,
			"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
			"log_type" => "backend",
			"log_ip" => $_SERVER["REMOTE_ADDR"],
			"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"log_date_created" => gmdate("Y-m-d H:i:s"),
			"modules_record_key" => "users_id",
			"modules_record_target" => $_SESSION['users_id'],
			"modules_id" => "5",
			"modules_name" => "Users",
			"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
			"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
			"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
			"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
		);
		create_log_data($log);
		unset($log);
		$log = array();
	}
	
	stop($con);
	
	$i = 0;
	$new_sessions = array();
	foreach($sessions as $key => $value) {
		$new_sessions[$i] = $value;
	}
	
	authentication_update_recent($_SESSION['users_id'], $new_sessions);

	session_unset();
	session_destroy();
	
	unset($_SESSION);
	
	setcookie(generate_title_link_name($configs['site_name']), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Facebook"), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Google"), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Twitter"), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Line"), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Instagram"), null, -1, '/');
	setcookie(generate_title_link_name($configs['site_name']."_Microsoft"), null, -1, '/');
	
	unset($_COOKIE);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
  
}

function signup($parameters){

	/* get global: configurations */
	global $configs;
	$users_configs = get_modules_data_by_id("6");

	/* get global: ajax function */
	global $method;
	

	/* configurations: image quality */
	if (isset($users_configs["modules_image_crop_quality"]) && $users_configs["modules_image_crop_quality"] != "") {
		$image_users_quality = $users_configs["modules_image_crop_quality"];
	} else {
		$image_users_quality = $configs["image_quality"];
	}

	/* configurations: thumbnail */
	if (isset($users_configs["modules_image_crop_thumbnail"]) && $users_configs["modules_image_crop_thumbnail"] != "") {
		$image_users_thumbnail = $users_configs["modules_image_crop_thumbnail"];
	} else {
		$image_users_thumbnail = $configs["image_thumbnail"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_aspectratio"]) && $users_configs["modules_image_crop_thumbnail_aspectratio"] != "") {
		$image_users_thumbnail_aspectratio = $users_configs["modules_image_crop_thumbnail_aspectratio"];
	} else {
		$image_users_thumbnail_aspectratio = $configs["image_thumbnail_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_quality"]) && $users_configs["modules_image_crop_thumbnail_quality"] != "") {
		$image_users_thumbnail_quality = $users_configs["modules_image_crop_thumbnail_quality"];
	} else {
		$image_users_thumbnail_quality = $configs["image_thumbnail_quality"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_width"]) && $users_configs["modules_image_crop_thumbnail_width"] != "") {
		$image_users_thumbnail_width = $users_configs["image_thumbnail_width"];
	} else {
		$image_users_thumbnail_width = $configs["image_thumbnail_width"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_height"]) && $users_configs["modules_image_crop_thumbnail_height"] != "") {
		$image_users_thumbnail_height = $users_configs["modules_image_crop_thumbnail_height"];
	} else {
		$image_users_thumbnail_height = $configs["image_thumbnail_height"];
	}

	/* configurations: large thumbnail */
	if (isset($users_configs["modules_image_crop_large"]) && $users_configs["modules_image_crop_large"] != "") {
		$image_users_large = $users_configs["modules_image_crop_large"];
	} else {
		$image_users_large = $configs["image_large"];
	}
	if (isset($users_configs["modules_image_crop_large_aspectratio"]) && $users_configs["modules_image_crop_large_aspectratio"] != "") {
		$image_users_large_aspectratio = $users_configs["modules_image_crop_large_aspectratio"];
	} else {
		$image_users_large_aspectratio = $configs["image_large_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_large_quality"]) && $users_configs["modules_image_crop_large_quality"] != "") {
		$image_users_large_quality = $users_configs["modules_image_crop_large_quality"];
	} else {
		$image_users_large_quality = $configs["image_large_quality"];
	}
	if (isset($users_configs["modules_image_crop_large_width"]) && $users_configs["modules_image_crop_large_width"] != "") {
		$image_users_large_width = $users_configs["modules_image_crop_large_width"];
	} else {
		$image_users_large_width = $configs["image_large_width"];
	}
	if (isset($users_configs["modules_image_crop_large_height"]) && $users_configs["modules_image_crop_large_height"] != "") {
		$image_users_large_height = $users_configs["modules_image_crop_large_height"];
	} else {
		$image_users_large_height = $configs["image_large_height"];
	}

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["users_activate"] == "1") {
	
		if(!isset($parameters["users_username"]) || empty($parameters["users_username"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["users_username"]) && $parameters["users_username"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["users_username"]) && $parameters["users_username"] != "") {
			$sql_check_unique_users_username = "
			SELECT `users_id` FROM `users` 
			WHERE `users_username` LIKE '".$parameters["users_username"]."' 
			
			LIMIT 1";
			$result_check_unique_users_username = mysqli_query($con, $sql_check_unique_users_username);
			$num_check_unique_users_username = mysqli_num_rows($result_check_unique_users_username);
			if ($num_check_unique_users_username > 0) {
				
				$response["status"] = false;
				$response["target"] = "#users_username";
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		
		if(!isset($parameters["users_password"]) || empty($parameters["users_password"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is required");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["users_confirm_password"]) || empty($parameters["users_confirm_password"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is required");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if($parameters["users_password"] != $parameters["users_confirm_password"]){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is mismatched");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)");
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/", $parameters["users_password"]) && $parameters["users_password"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("must contain uppercases, lowercases, digits and special characters");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(isset($parameters["users_email"]) && !empty($parameters["users_email"])){
			if (function_exists("mb_strlen")) {
				if(mb_strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			} else {
				if(strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $parameters["users_email"]) && $parameters["users_email"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is invalid email");
				$response["target"] = "#users_email";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
			if (isset($parameters["users_email"]) && $parameters["users_email"] != "") {
				$sql_check_unique_users_email = "
				SELECT `users_id` FROM `users` 
				WHERE `users_email` LIKE '".$parameters["users_email"]."' 

				LIMIT 1";
				$result_check_unique_users_email = mysqli_query($con, $sql_check_unique_users_email);
				$num_check_unique_users_email = mysqli_num_rows($result_check_unique_users_email);
				if ($num_check_unique_users_email > 0) {

					$response["status"] = false;
					$response["target"] = "#users_email";
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("can not be duplicated");
					$response["values"] = $parameters;
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
		}

	}

	/* initialize: default */
	$users_image = $parameters["users_image"];
	unset($parameters["users_image"]);
	$parameters["users_image"] = "";
	date_default_timezone_set($configs["timezone"]);
	$parameters["users_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: prepare data */
	$password = md5($parameters["users_password"].$configs["password_salt"]);
	$parameters["users_password"] = $password;

	/* database: insert to module "users" (begin) */
	$sql = "INSERT INTO `users` (
				`users_id`,
				`users_username`,
				`users_password`,
				`users_name`,
				`users_last_name`,
				`users_nick_name`,
				`users_email`,
				`users_api_sessions`,
				`groups_id`,
				`users_level`,
				`users_email_notify`,
				`users_protected`,
				`users_date_created`,
				`users_activate`)
			VALUES (
				NULL,
				'" . $parameters["users_username"] . "',
				'" . $password . "',
				'" . $parameters["users_name"] . "',
				'" . $parameters["users_last_name"] . "',
				'" . $parameters["users_nick_name"] . "',
				'" . $parameters["users_email"] . "',
				'',
				'0',
				'6',
				'1',
				'0',
				'" . $parameters["users_date_created"] . "',
				'1')";
	$result = mysqli_query($con, $sql);
	$parameters["users_id"] = mysqli_insert_id($con);
	/* database: insert to module "users" (end) */

	if ($result) {
		
		/* database: update to module "users" (begin) */
		$sql = "UPDATE `users`
				SET 
					   `users_users_username` = '" . $parameters["users_username"] . "',
					   `users_users_name` = '" . $parameters["users_name"] . "',
					   `users_users_last_name` = '" . $parameters["users_last_name"] . "',
					   `users_users_id` = '" . $parameters["users_users_id"] . "'
				WHERE  `users_id` = '" . $parameters["users_id"] . "'";
		$result = mysqli_query($con, $sql) or die(mysqli_error($con));
		/* database: update to module "users" (end) */
		
		if (!empty($parameters["users_name"])) {
			$mail_name = $parameters["users_name"];
		} else {
			$mail_name = $parameters["users_username"];
		}
		$mail_subject = translate("Welcome to")." ".$configs["site_name"];
		$mail_body = "<html><body><h4>".translate("Welcome").", ".$mail_name."</h4><br /><br />".translate("Your account was created at")." <strong>".$configs["site_name"]."</strong><br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
		send_mail($parameters["users_email"], $configs["site_name"], $configs["email"], $mail_subject, $mail_body);
		
		/* response: additional data */
		

		/* log (begin) */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "signup",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $parameters["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => $parameters["users_id"],
				"users_username" => $parameters["users_username"],
				"users_name" => $parameters["users_name"],
				"users_last_name" => $parameters["users_last_name"]
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function connect_api_sessions($api_sessions, $users_id = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$api_sessions = escape_string($con, $api_sessions);
	$users_id = escape_string($con, $users_id);
	
	if(!isset($api_sessions) || empty($api_sessions)){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("API sessions") . "</strong> " . translate("is required");
		$response["target"] = "#api_sessions";
		$response["values"] = $api_sessions;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	
	if (isset($users_id) && !empty($users_id)) {
		$sql_api_sessions = "
		SELECT `users_api_sessions` 
		FROM `users` 
		WHERE `users_id` = '".$users_id."'";
		$result_api_sessions = mysqli_query($con, $sql_api_sessions);

		if ($result_api_sessions) {

			$query_api_sessions = mysqli_fetch_array($result_api_sessions);

			if (isset($query_api_sessions["users_api_sessions"]) && !empty($query_api_sessions["users_api_sessions"])) {

				$current_api_sessions = unserialize($query_api_sessions["users_api_sessions"]);
				$valid_session = 0;

				for($i = 0; $i < count($current_api_sessions); $i++) {
					if ($current_api_sessions[$i]["api_name"] == $api_sessions["api_name"]) {
						$valid_session = $valid_session + 1;
						$current_api_sessions[$i] = $api_sessions;
					}
				}

				if ($valid_session <= 0) {
					array_push($current_api_sessions, $api_sessions);
				}
				
			} else {
				$current_api_sessions = array($api_sessions);

			}

			mysqli_free_result($result_api_sessions);
			
			$sql = "UPDATE `users` 
					SET    `users_api_sessions` = '".serialize($current_api_sessions)."'
					WHERE  `users_id` = '".$_SESSION["users_id"]."'";
			$result = mysqli_query($con,$sql);

			if ($result) {

				$_SESSION["users_api_sessions"] = $current_api_sessions;
				setcookie(generate_title_link_name($configs['site_name'])."_".$api_sessions["api_name"], $_SESSION["users_id"], time() + (86400 * 90), "/");
				
				authentication_update_recent($_SESSION["users_id"]);

				$response["status"] = true;
				$response["message"] = translate("Successfully connected");

			} else {

				$response["status"] = false;
				$response["target"] = "users";
				if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
					$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
				} else {
					$response["message"] = translate("Database encountered error");
				}

			}

		} else {
			
			$response["status"] = false;
			$response["target"] = "users";
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
			
		}
		
	} else {
		
		$target_serialized_name = preg_replace("/a\:[0-9]+\:\{(s\:8\:\"api\_name\"\;[\w\:\"]+)\;\}/", "$1", serialize(array("api_name" => $api_sessions["api_name"])));
		
		if ($api_sessions["api_name"] == "Facebook") {
			
			$target_serialized_id = preg_replace("/a\:[0-9]+\:\{(s\:2\:\"id\"\;[\w\:\"]+)\;\}/", "$1", serialize(array("id" => $api_sessions["api_details"]["id"])));
			
		} else if ($api_sessions["api_name"] == "Line") {
			
			$target_serialized_id = preg_replace("/a\:[0-9]+\:\{(s\:6\:\"userId\"\;[\w\:\"]+)\;\}/", "$1", serialize(array("userId" => $api_sessions["api_details"]["userId"])));
			
		}

		$sql_api_sessions = "
		SELECT `users_id`, `users_api_sessions` 
		FROM `users` 
		WHERE `users_api_sessions` LIKE '%".$target_serialized_name."%'
		  AND `users_api_sessions` LIKE '%".$target_serialized_id."%'";
		$result_api_sessions = mysqli_query($con, $sql_api_sessions);
		$num_api_sessions = mysqli_num_rows($result_api_sessions);
		
		if ($num_api_sessions > 0) {
			if ($result_api_sessions) {

				$query_api_sessions = mysqli_fetch_array($result_api_sessions);

				if (isset($query_api_sessions["users_api_sessions"]) && !empty($query_api_sessions["users_api_sessions"])) {

					$current_api_sessions = unserialize($query_api_sessions["users_api_sessions"]);
					$valid_session = 0;

					for($i = 0; $i < count($current_api_sessions); $i++) {
						if ($current_api_sessions[$i]["api_name"] == $api_sessions["api_name"]) {
							$valid_session = $valid_session + 1;
							$current_api_sessions[$i] = $api_sessions;
						}
					}

					if ($valid_session <= 0) {
						array_push($current_api_sessions, $api_sessions);
					}

				} else {

					$current_api_sessions = array($api_sessions);

				}

				mysqli_free_result($result_api_sessions);

				$sql = "UPDATE `users` 
						SET    `users_api_sessions` = '".serialize($current_api_sessions)."'
						WHERE  `users_id` = '".$query_api_sessions["users_id"]."'";
				$result = mysqli_query($con,$sql);

				if ($result) {

					$session_id = session_id();

					$_SESSION = get_users_data_by_id($query_api_sessions["users_id"]);
					$_SESSION["session_id"] = $session_id;
					$_SESSION["users_api_sessions"] = $current_api_sessions;
					setcookie(generate_title_link_name($configs['site_name'])."_".$api_sessions["api_name"], $_SESSION["users_id"], time() + (86400 * 90), "/");
					
					authentication_update_recent($_SESSION["users_id"]);

					$response["status"] = true;
					$response["message"] = translate("Successfully connected");

				} else {

					$response["status"] = false;
					$response["target"] = "users";
					if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
						$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
					} else {
						$response["message"] = translate("Database encountered error");
					}

				}

			} else {

				$response["status"] = false;
				$response["target"] = "users";
				if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
					$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
				} else {
					$response["message"] = translate("Database encountered error");
				}

			}
			
		} else {
			
			$response["status"] = false;
			$response["target"] = "users";
			$response["message"] = translate("Empty data");
			
		}

	}

	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function disconnect_api_sessions($api_name, $credential = "") {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$api_parameters = escape_string($con, $api_parameters);
	$api_name = escape_string($con, $api_name);
	
	if(!isset($api_name) || empty($api_name)){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("API name") . "</strong> " . translate("is required");
		$response["target"] = "#api_name";
		$response["values"] = $api_name;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	
	$sql_api_sessions = "
	SELECT `users_api_sessions` 
	FROM `users` 
	WHERE `users_id` = '".$_SESSION["users_id"]."'";
	$result_api_sessions = mysqli_query($con, $sql_api_sessions);

	if ($result_api_sessions) {

		$query_api_sessions = mysqli_fetch_array($result_api_sessions);

		if (isset($query_api_sessions["users_api_sessions"]) && !empty($query_api_sessions["users_api_sessions"])) {

			$api_sessions = unserialize($query_api_sessions["users_api_sessions"]);
			$valid_session = 0;

			for($i = 0; $i < count($api_sessions); $i++) {
				if ($api_sessions[$i]["api_name"] == $api_name) {
					
					unset($api_sessions[$i]);
					$valid_session = $valid_session + 1;
					
					
					if ($api_name == "Line") {
						
						$curl = curl_init();
						curl_setopt_array($curl, array(
							CURLOPT_URL => "https://api.line.me/oauth2/v2.1/revoke",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_POSTFIELDS => "access_token=".$code."&client_id=".$credential["app_id"]."&client_secret=".$credential["app_secret"],
							CURLOPT_HTTPHEADER => array(
								"cache-control: no-cache",
								"content-type: application/x-www-form-urlencoded"
							),
						));
						$response_authen = json_decode(curl_exec($curl));
						curl_close($curl);
						
					}
					
				}
			}

		}
		mysqli_free_result($result_api_sessions);
	}

	if ($valid_session) {
		
		$i = 0;
		$new_api_sessions = array();
		foreach($api_sessions as $key => $value) {
			$new_api_sessions[$i] = $value;
		}

		$sql = "UPDATE `users` 
				SET    `users_api_sessions` = '".serialize($new_api_sessions)."'
				WHERE  `users_id` = '".$_SESSION["users_id"]."'";
		$result = mysqli_query($con,$sql);

		if ($result) {

			$response["status"] = true;
			$response["message"] = translate("Successfully disconnected");

		} else {
			
			authentication_update_recent($_SESSION["users_id"]);

			$response["status"] = false;
			$response["target"] = "users";
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}

		}

	}
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function create_api_connect_link($api_name, $credential) {
	
	/* get global: configurations */
	global $configs;
	
	global $_SESSION;
	global $_COOKIE;
	
	/* get global: ajax function */
	global $method;
	
	
	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$_COOKIE = escape_string($con, $_COOKIE);
	$api_name = escape_string($con, $api_name);
	$credential = escape_string($con, $credential);
	
	if ($api_name == "Facebook") {
				
		require_once("plugins/Facebook/autoload.php");

		$fb = new Facebook\Facebook($credential);

		$helper = $fb->getRedirectLoginHelper();

		if ($helper->getError()) {
			$response["facebook_status"] = false;
			$response["facebook_message"] .= translate("Error").": ".translate($helper->getError())."\n";
			$response["facebook_message"] .= translate("Error Code").": ".translate($helper->getErrorCode())."\n";
			$response["facebook_message"] .= translate("Error Reason").": ".translate($helper->getErrorReason())."\n";
			$response["facebook_message"] .= translate("Error Description").": ".translate($helper->getErrorDescription())."\n";
		} else {
			$response["facebook_status"] = false;
			$response["facebook_message"] = translate("Bad request");
		}

		$loginUrl = $helper->getLoginUrl($credential["redirect_url"], $credential["scope"]);

		$loginUrl_parts = parse_url($loginUrl);
		parse_str($loginUrl_parts['query'], $get_parameters);
		$_SESSION["facebook_api_state"] = $get_parameters['state'];

		$response["status"] = true;
		$response["message"] = translate("Successfully created login URL");
		$response["values"] = $loginUrl;

	} else if ($api_name == "Google") {



	} else if ($api_name == "Twitter") {



	} else if ($api_name == "Line") {
		
		if (!isset($_SESSION["line_api_state"]) || empty($_SESSION["line_api_state"])) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$characters_length = strlen($characters);
			$state = '';
			for ($i = 0; $i < 10; $i++) {
				$state .= $characters[rand(0, $characters_length - 1)];
			}
			$nonce = '';
			for ($i = 0; $i < 9; $i++) {
				$nonce .= $characters[rand(0, $characters_length - 1)];
			}
			$_SESSION["line_api_state"] = $state;
		} else {
			$state = $_SESSION["line_api_state"];
		}
		
		$login_url = "https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=" . $credential["app_id"] . "&redirect_uri=" . urlencode($credential["redirect_url"]) . "&state=" . $state . "&scope=" . $credential["scope"] . "&nonce=".$nonce;

		$response["status"] = true;
		$response["message"] = translate("Successfully created login URL");
		$response["values"] = $login_url;

	} else if ($api_name == "Instagram") {

		

	} else if ($api_name == "Microsoft") {



	}

	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function upload_users_files ($parameters) {

	/* get global: configurations */
	global $configs;
	$users_configs = get_modules_data_by_id("6");

	/* get global: ajax function */
	global $method;

	global $_SESSION;

	
	$current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if (strpos($current_url, "system") !== false && strpos($current_url, "core") !== false && strpos($current_url, $configs["version"]) !== false) {
		require_once("../../../plugins/slim/slim.php");
		require_once("../../../plugins/slim/thumbnail.php");
	} else {
		require_once("../plugins/slim/slim.php");
		require_once("../plugins/slim/thumbnail.php");
	}
	
	/* configurations: image quality */
	if (isset($users_configs["modules_image_crop_quality"]) && $users_configs["modules_image_crop_quality"] != "") {
		$image_users_quality = $users_configs["modules_image_crop_quality"];
	} else {
		$image_users_quality = $configs["image_quality"];
	}

	/* configurations: thumbnail */
	if (isset($users_configs["modules_image_crop_thumbnail"]) && $users_configs["modules_image_crop_thumbnail"] != "") {
		$image_users_thumbnail = $users_configs["modules_image_crop_thumbnail"];
	} else {
		$image_users_thumbnail = $configs["image_thumbnail"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_aspectratio"]) && $users_configs["modules_image_crop_thumbnail_aspectratio"] != "") {
		$image_users_thumbnail_aspectratio = $users_configs["modules_image_crop_thumbnail_aspectratio"];
	} else {
		$image_users_thumbnail_aspectratio = $configs["image_thumbnail_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_quality"]) && $users_configs["modules_image_crop_thumbnail_quality"] != "") {
		$image_users_thumbnail_quality = $users_configs["modules_image_crop_thumbnail_quality"];
	} else {
		$image_users_thumbnail_quality = $configs["image_thumbnail_quality"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_width"]) && $users_configs["modules_image_crop_thumbnail_width"] != "") {
		$image_users_thumbnail_width = $users_configs["modules_image_crop_thumbnail_width"];
	} else {
		$image_users_thumbnail_width = $configs["image_thumbnail_width"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_height"]) && $users_configs["modules_image_crop_thumbnail_height"] != "") {
		$image_users_thumbnail_height = $users_configs["modules_image_crop_thumbnail_height"];
	} else {
		$image_users_thumbnail_height = $configs["image_thumbnail_height"];
	}

	/* configurations: large thumbnail */
	if (isset($users_configs["modules_image_crop_large"]) && $users_configs["modules_image_crop_large"] != "") {
		$image_users_large = $users_configs["modules_image_crop_large"];
	} else {
		$image_users_large = $configs["image_large"];
	}
	if (isset($users_configs["modules_image_crop_large_aspectratio"]) && $users_configs["modules_image_crop_large_aspectratio"] != "") {
		$image_users_large_aspectratio = $users_configs["modules_image_crop_large_aspectratio"];
	} else {
		$image_users_large_aspectratio = $configs["image_large_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_large_quality"]) && $users_configs["modules_image_crop_large_quality"] != "") {
		$image_users_large_quality = $users_configs["modules_image_crop_large_quality"];
	} else {
		$image_users_large_quality = $configs["image_large_quality"];
	}
	if (isset($users_configs["modules_image_crop_large_width"]) && $users_configs["modules_image_crop_large_width"] != "") {
		$image_users_large_width = $users_configs["modules_image_crop_large_width"];
	} else {
		$image_users_large_width = $configs["image_large_width"];
	}
	if (isset($users_configs["modules_image_crop_large_height"]) && $users_configs["modules_image_crop_large_height"] != "") {
		$image_users_large_height = $users_configs["modules_image_crop_large_height"];
	} else {
		$image_users_large_height = $configs["image_large_height"];
	}
	
    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	
	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	
	date_default_timezone_set($configs["timezone"]);
	$parameters["users_date_created"] = gmdate("Y-m-d H:i:s");
	
	/* database: get old data from module "users" (begin) */
	$sql = "SELECT *
	        FROM   `users`
			WHERE  `users_id` = '" . $parameters["users_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	if ($result) {
		$query = mysqli_fetch_array($result);
		$parameters["users_old"] = $query;
		mysqli_free_result($result);
	} else {
		$parameters["users_old"] = "";
	}
	/* database: get old data from module "users" (end) */
	
	/* initialize: user data */
	if (!isset($parameters["users_users_id"]) || $parameters["users_users_id"] == "") {
		$parameters["users_users_id"] = $_SESSION["users_id"];
		$parameters["users_users_username"] = $_SESSION["users_username"];
		$parameters["users_users_name"] = $_SESSION["users_name"];
		$parameters["users_users_last_name"] = $_SESSION["users_last_name"];
	} else {
		$parameters["users_users_id"] = 0;
		$parameters["users_users_username"] = "";
		$parameters["users_users_name"] = "";
		$parameters["users_users_last_name"] = "";
	}
	
		
	// Could be multiple slim croppers
	
	$users_image_image_upload = Slim::getImages("users_image");

	if ($users_image_image_upload) {

		$image = $users_image_image_upload[0];

		$files = array();

		$media_folder_save_path = '/media/';
		$media_folder_path = '../../../../media/';

		if (empty($media_folder_path)) {
			$media_folder_save_path = '/media/';
			$media_folder_path = '../../../../media/';
		}

		/* initialize: image paths for "users_image" */
		$absolutedir = dirname(__FILE__);
		$redirectdir = "/media/images/users/";
		$serverdir = "../../../../media/images/users/";
		$serverdir_full = $configs["website_url"] . "media/images/users/";
		$extension_default = pathinfo($image["output"]["name"], PATHINFO_EXTENSION);
		if(!file_exists($serverdir)) {
			mkdir($serverdir, 0777, true);
		}

		/* initialize: image title for "users_image" */
		if (isset($parameters["users_title"]) && !empty($parameters["users_title"])) {
			$name = $parameters["users_title"];
		} else {
			$name = str_replace(".".$extension_default, "", "users_image_".$image["output"]["name"]);
		}

		$filename_encrypted = generate_title_link_name($name);
		$filename = $filename_encrypted.strtolower(".".$extension_default);

		/* initialize: image ID for "users_image" */
		$id = preg_replace("/[^A-Za-z0-9\-]/", "", str_replace(" ", "", str_replace("-", " ", $name)));

		// save output data if set
		if (isset($image["output"]["data"])) {

			$data = $image["output"]["data"];

			// If you want to store the file in another directory pass the directory name as the third parameter.
			$file = Slim::saveFile($data, $filename, $serverdir);
			//createThumbnail("../../../../../image/creator/".$file["name"], "../../../../../image/creator/thumbnail/".$file["name"], 75, 75);

			// If you want to prevent Slim from adding a unique id to the file name add false as the fourth parameter.
			//$file = Slim::saveFile($data, $filename, "tmp/", false);
			//$output = Slim::saveFile($data, $filename);

			array_push($files, $file);

		}

		// save input data if set
		if (isset ($image["input"]["data"])) {

			$data = $image["input"]["data"];

			// If you want to store the file in another directory pass the directory name as the third parameter.
			$file = Slim::saveFile($data, $filename, $serverdir);
			//createThumbnail("../../../../../image/creator/".$file["name"], "../../../../../image/creator/thumbnail/".$file["name"], 75, 75);

			// If you want to prevent Slim from adding a unique id to the file name add false as the fourth parameter.
			//$file = Slim::saveFile($data, $filename, "tmp/", false);
			//$input = Slim::saveFile($data, $filename);

			array_push($files, $file);

		}

		$image_filenames = join(", ", array_map(function($file){ return $file["name"]; }, $files));
		$image_images = array_map(function($file) { return "<img src=\"" . $file["path"] . "\" alt=\"\"/>"; }, $files);

		/* database: update image path to module "users" for "users_image" (begin) */
		$sql = "UPDATE `users`
				SET    `users_image` = '".$redirectdir.$image_filenames."'
				WHERE  `users_id` = '".$parameters["users_id"]."'";
		$result = mysqli_query($con, $sql);
		/* database: update image path to module "users" for "users_image" (end) */

		$imgdata = $image["output"]["data"];

		/* thumbnail update handler for "users_image" (begin) */
		if ($image_users_thumbnail == true) {

			/* initialize: thumbnail paths for "users_image" */
			$redirectdir_thumbnail  = $redirectdir."thumbnail/";
			$serverdir_thumbnail	= $serverdir."thumbnail/";
			$image_thumbnail_old = str_replace($redirectdir, $redirectdir_thumbnail, $parameters["users_old"]["users_image"]);
			if(!file_exists($serverdir_thumbnail)) {
				mkdir($serverdir_thumbnail, 0777, true);
			}

			/* initialize: prepare thumbnail size for "users_image" */
			if (empty($image_users_thumbnail_width)) {
				$image_users_thumbnail_width = 200;
			}
			if (empty($image_users_thumbnail_height)) {
				$image_users_thumbnail_height = 200;
			}

			createThumbnail($serverdir.$image_filenames, $serverdir_thumbnail.$image_filenames, $image_users_thumbnail_width, $image_users_thumbnail_height, $image_users_thumbnail_aspectratio, $image_users_thumbnail_quality);

			/* remove old thumbnail file for "users_image" */
			if (file_exists($serverdir_thumbnail.$image_filenames) && !is_dir($serverdir_thumbnail.$image_filenames)){
				if ($image_thumbnail_old != "" && $image_thumbnail_old != $redirectdir_thumbnail.$image_filenames) {
					$image_thumbnail_path_old = "../..".$image_thumbnail_old;
					if (file_exists($image_thumbnail_path_old)){
						chmod($image_thumbnail_path_old, 0777);
						unlink($image_thumbnail_path_old);
					}
				}
			}

		}
		/* thumbnail update handler for "users_image" (end) */

		/* large thumbnail update handler for "users_image" (begin) */
		if ($image_users_large == true) {

			/* initialize: large thumbnail paths for "users_image" */
			$redirectdir_large  = $redirectdir."large/";
			$serverdir_large	= $serverdir."large/";
			$image_large_old = str_replace($redirectdir, $redirectdir_large, $parameters["users_old"]["users_image"]);
			if(!file_exists($serverdir_large)) {
				mkdir($serverdir_large, 0777, true);
			}

			/* initialize: prepare large thumbnail size for "users_image" */
			if (empty($image_users_large_width)) {
				$image_users_large_width = 400;
			}
			if (empty($image_users_large_height)) {
				$image_users_large_height = 400;
			}

			createThumbnail($serverdir.$image_filenames, $serverdir_large.$image_filenames, $image_users_large_width, $image_users_large_height, $image_users_large_aspectratio, $image_users_large_quality);

			/* remove old large thumbnail file for "users_image" */
			if (file_exists($serverdir_large.$image_filenames) && !is_dir($serverdir_large.$image_filenames)){
				if ($image_large_old != "" && $image_large_old != $redirectdir_large.$image_filenames) {
					$image_large_path_old = "../..".$image_large_old;
					if (file_exists($image_large_path_old)){
						chmod($image_large_path_old, 0777);
						unlink($image_large_path_old);
					}
				}
			}

		}
		/* large thumbnail update handler for "users_image" (end) */

		if ($result) {

			if ($_SESSION["users_id"] == $parameters["users_id"]) {
				$_SESSION["users_image"] = $redirectdir.$image_filenames;
			}
		
			/* remove old image file for "users_image" */
			if (file_exists($serverdir.$image_filenames) && !is_dir($serverdir.$image_filenames)){
				if ($parameters["users_old"]["users_image"] != "" && $image_large_old != $redirectdir.$image_filenames) {
					$users_image_path_old = "../..".$parameters["users_old"]["users_image"];
					if (file_exists($users_image_path_old)){
						chmod($users_image_path_old, 0777);
						unlink($users_image_path_old);
					}
				}
			}

			/* store image path to response for "users_image" */
			$users_image = $redirectdir.$image_filenames;

		} else {

			if (file_exists($serverdir.$image_filenames) && !is_dir($serverdir.$image_filenames)){
				chmod($serverdir.$image_filenames, 0777);
				unlink($serverdir.$image_filenames);
				if (file_exists($serverdir_thumbnail.$image_filenames)){
					chmod($serverdir_thumbnail.$image_filenames, 0777);
					unlink($serverdir_thumbnail.$image_filenames);
				}
				if (file_exists($serverdir_large.$image_filenames)){
					chmod($serverdir_large.$image_filenames, 0777);
					unlink($serverdir_large.$image_filenames);
				}
			}

		}

	} else {

		if ($parameters["users_image_old"] == "") {
	
			/* condition: "users_image" requirement (begin) */
			if ($parameters["users_old"]["users_image"] != "") {

				$redirectdir = "/media/images/users/";

				$sql = "
				UPDATE `users`
				SET    `users_image` = ''
				WHERE  `users_id` = '".$parameters["users_id"]."'";
				$result = mysqli_query($con, $sql);

				if ($result) {

					if ($_SESSION["users_id"] == $parameters["users_id"]) {
						$_SESSION["users_image"] = "";
					}
					if (file_exists("../..".$parameters["users_old"]["users_image"]) && !is_dir("../..".$parameters["users_old"]["users_image"])) {
						chmod("../..".$parameters["users_old"]["users_image"], 0777);
						unlink("../..".$parameters["users_old"]["users_image"]);
					}

					$redirectdir_thumbnail  = $redirectdir."thumbnail/";
					$serverdir_thumbnail	= $serverdir."thumbnail/";
					$image_thumbnail_old = str_replace($redirectdir, $redirectdir_thumbnail, $parameters["users_old"]["users_image"]);

					$redirectdir_large  = $redirectdir."large/";
					$serverdir_large	= $serverdir."large/";
					$image_large_old = str_replace($redirectdir, $redirectdir_large, $parameters["users_old"]["users_image"]);

					$image_thumbnail_path_old = "../..".$image_thumbnail_old;
					if (file_exists($image_thumbnail_path_old) && !is_dir($image_thumbnail_path_old)){
						chmod($image_thumbnail_path_old, 0777);
						unlink($image_thumbnail_path_old);
					}

					$image_large_path_old = "../..".$image_large_old;
					if (file_exists($image_large_path_old) && !is_dir($image_large_path_old)){
						chmod($image_large_path_old, 0777);
						unlink($image_large_path_old);
					}
					

				}

			}
			/* condition: "users_image" requirement (end) */
			

		}

	}
				
	/* upload handler for "users_image" (end) */

	$response["status"] = true;
	$response["message"] = translate("Successfully updated data");
	
	stop($con);
	
	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function get_users_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "users" */
	$sql = "
	SELECT *
	FROM `users`
	WHERE `users`.`users_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["users_remember_login"] = unserialize($query["users_remember_login"]);
			$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);
			$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
			$query["users_date_created_formatted"] = datetime_reformat($query["users_date_created"]);
			
			if (isset($query["users_image"]) && !empty($query["users_image"])) {
				if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
					$query["users_image_path"] = $query["users_image"];
					$query["users_image_thumbnail_path"] = $query["users_image"];
					$query["users_image_large_thumbnail_path"] = $query["users_image"];
				} else {
					$query["users_image_path"] = $configs["base_url"].$query["users_image"];
					$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
					$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
				}
				$file_check = curl_init($query["users_image_path"]);
				curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
				$file_check_response = curl_exec($file_check);
				$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
				curl_close($file_check);
				if ($file_status != 200) {
					$query["users_image"] = "";
					$query["users_image_path"] = "";
					$query["users_image_thumbnail_path"] = "";
					$query["users_image_large_thumbnail_path"] = "";
				}
			}
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI],
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "users_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_data_by_current_session_id(){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	

	/* database: get data by ID from module "users" */
	$sql = "
	SELECT *
	FROM `users`
	WHERE `users`.`users_id` = '" . $_SESSION["users_id"] . "'
	AND `users_activate` = '1'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["users_remember_login"] = unserialize($query["users_remember_login"]);
			$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);
			$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
			$query["users_date_created_formatted"] = datetime_reformat($query["users_date_created"]);
			
			if (isset($query["users_image"]) && !empty($query["users_image"])) {
				if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
					$query["users_image_path"] = $query["users_image"];
					$query["users_image_thumbnail_path"] = $query["users_image"];
					$query["users_image_large_thumbnail_path"] = $query["users_image"];
				} else {
					$query["users_image_path"] = $configs["base_url"].$query["users_image"];
					$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
					$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
				}
				$file_check = curl_init($query["users_image_path"]);
				curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
				$file_check_response = curl_exec($file_check);
				$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
				curl_close($file_check);
				if ($file_status != 200) {
					$query["users_image"] = "";
					$query["users_image_path"] = "";
					$query["users_image_thumbnail_path"] = "";
					$query["users_image_large_thumbnail_path"] = "";
				}
			}
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI],
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "users_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_api_sessions_by_id($target, $api_name){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$api_name = escape_string($con, $api_name);
	

	/* database: get data by ID from module "users" */
	$sql = "
	SELECT `users_api_sessions`
	FROM `users`
	WHERE `users`.`users_id` = '" . $target . "'
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$api_sessions_found = 0;
		
		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			
			$users_data_api_sessions = unserialize($query["users_api_sessions"]);
			for ($i = 0; $i < count($users_data_api_sessions); $i++) {
				if ($users_data_api_sessions[$i]["api_name"] == $api_name) {
					$query_users_api_sessions = $users_data_api_sessions[$i];
					$api_sessions_found = $api_sessions_found + 1;
					break;
				}
			}
			
			unset($query);
			$query = array();
			
		}
		
		if ($api_sessions_found > 0) {
			
			$response["status"] = true;
			$response["values"] = $query_users_api_sessions;
			$response["message"] = translate("Successfully get data");
			
		} else {
			
			$response["status"] = false;
			$response["message"] = translate("Empty data");
			
		}

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_data_all($start = 0, $limit = "", $sort_by = "users_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "users" */
	$sql = "
	SELECT *
	FROM   `users`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				
				$query["users_remember_login"] = unserialize($query["users_remember_login"]);
				$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);
				
				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
				if (isset($query["users_image"]) && !empty($query["users_image"])) {
					if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
						$query["users_image_path"] = $query["users_image"];
						$query["users_image_thumbnail_path"] = $query["users_image"];
						$query["users_image_large_thumbnail_path"] = $query["users_image"];
					} else {
						$query["users_image_path"] = $configs["base_url"].$query["users_image"];
						$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
						$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
					}
					$file_check = curl_init($query["users_image_path"]);
					curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
					$file_check_response = curl_exec($file_check);
					$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
					curl_close($file_check);
					if ($file_status != 200) {
						$query["users_image"] = "";
						$query["users_image_path"] = "";
						$query["users_image_thumbnail_path"] = "";
						$query["users_image_large_thumbnail_path"] = "";
					}
				}


				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "users" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "users_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "users" */
	$sql = "
	SELECT *
	FROM `users`
	WHERE `users_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				
				$query["users_remember_login"] = unserialize($query["users_remember_login"]);
				$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);

				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
				if (isset($query["users_image"]) && !empty($query["users_image"])) {
					if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
						$query["users_image_path"] = $query["users_image"];
						$query["users_image_thumbnail_path"] = $query["users_image"];
						$query["users_image_large_thumbnail_path"] = $query["users_image"];
					} else {
						$query["users_image_path"] = $configs["base_url"].$query["users_image"];
						$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
						$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
					}
					$file_check = curl_init($query["users_image_path"]);
					curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
					$file_check_response = curl_exec($file_check);
					$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
					curl_close($file_check);
					if ($file_status != 200) {
						$query["users_image"] = "";
						$query["users_image_path"] = "";
						$query["users_image_thumbnail_path"] = "";
						$query["users_image_large_thumbnail_path"] = "";
					}
				}
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "users_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "users" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `users`
	WHERE `users_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `users`
	WHERE `users_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "users_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "users" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				
				$query["users_remember_login"] = unserialize($query["users_remember_login"]);
				$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);
				
				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
				if (isset($query["users_image"]) && !empty($query["users_image"])) {
					if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
						$query["users_image_path"] = $query["users_image"];
						$query["users_image_thumbnail_path"] = $query["users_image"];
						$query["users_image_large_thumbnail_path"] = $query["users_image"];
					} else {
						$query["users_image_path"] = $configs["base_url"].$query["users_image"];
						$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
						$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
					}
					$file_check = curl_init($query["users_image_path"]);
					curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
					$file_check_response = curl_exec($file_check);
					$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
					curl_close($file_check);
					if ($file_status != 200) {
						$query["users_image"] = "";
						$query["users_image_path"] = "";
						$query["users_image_thumbnail_path"] = "";
						$query["users_image_large_thumbnail_path"] = "";
					}
				}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "users" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_users_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "users_id", $sort_direction = "desc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'users'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "users_actions" 
				&& $key != "users_date_created" 
				&& $key != "users_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "users" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				
				$query["users_remember_login"] = unserialize($query["users_remember_login"]);
				$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);
				
				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
				if (isset($query["users_image"]) && !empty($query["users_image"])) {
					if (strpos($query["users_image"], "http://") !== false || strpos($query["users_image"], "https://") !== false) {
						$query["users_image_path"] = $query["users_image"];
						$query["users_image_thumbnail_path"] = $query["users_image"];
						$query["users_image_large_thumbnail_path"] = $query["users_image"];
					} else {
						$query["users_image_path"] = $configs["base_url"].$query["users_image"];
						$query["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query["users_image"]);
						$query["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query["users_image"]);
					}
					$file_check = curl_init($query["users_image_path"]);
					curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
					$file_check_response = curl_exec($file_check);
					$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
					curl_close($file_check);
					if ($file_status != 200) {
						$query["users_image"] = "";
						$query["users_image_path"] = "";
						$query["users_image_thumbnail_path"] = "";
						$query["users_image_large_thumbnail_path"] = "";
					}
				}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_users_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `users_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `users_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'users'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "users_actions" 
				&& $key != "users_date_created" 
				&& $key != "users_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "users" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `users_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `users`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_users_data($parameters){

	/* get global: configurations */
	global $configs;
	$users_configs = get_modules_data_by_id("6");

	/* get global: ajax function */
	global $method;
	

	/* configurations: image quality */
	if (isset($users_configs["modules_image_crop_quality"]) && $users_configs["modules_image_crop_quality"] != "") {
		$image_users_quality = $users_configs["modules_image_crop_quality"];
	} else {
		$image_users_quality = $configs["image_quality"];
	}

	/* configurations: thumbnail */
	if (isset($users_configs["modules_image_crop_thumbnail"]) && $users_configs["modules_image_crop_thumbnail"] != "") {
		$image_users_thumbnail = $users_configs["modules_image_crop_thumbnail"];
	} else {
		$image_users_thumbnail = $configs["image_thumbnail"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_aspectratio"]) && $users_configs["modules_image_crop_thumbnail_aspectratio"] != "") {
		$image_users_thumbnail_aspectratio = $users_configs["modules_image_crop_thumbnail_aspectratio"];
	} else {
		$image_users_thumbnail_aspectratio = $configs["image_thumbnail_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_quality"]) && $users_configs["modules_image_crop_thumbnail_quality"] != "") {
		$image_users_thumbnail_quality = $users_configs["modules_image_crop_thumbnail_quality"];
	} else {
		$image_users_thumbnail_quality = $configs["image_thumbnail_quality"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_width"]) && $users_configs["modules_image_crop_thumbnail_width"] != "") {
		$image_users_thumbnail_width = $users_configs["modules_image_crop_thumbnail_width"];
	} else {
		$image_users_thumbnail_width = $configs["image_thumbnail_width"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_height"]) && $users_configs["modules_image_crop_thumbnail_height"] != "") {
		$image_users_thumbnail_height = $users_configs["modules_image_crop_thumbnail_height"];
	} else {
		$image_users_thumbnail_height = $configs["image_thumbnail_height"];
	}

	/* configurations: large thumbnail */
	if (isset($users_configs["modules_image_crop_large"]) && $users_configs["modules_image_crop_large"] != "") {
		$image_users_large = $users_configs["modules_image_crop_large"];
	} else {
		$image_users_large = $configs["image_large"];
	}
	if (isset($users_configs["modules_image_crop_large_aspectratio"]) && $users_configs["modules_image_crop_large_aspectratio"] != "") {
		$image_users_large_aspectratio = $users_configs["modules_image_crop_large_aspectratio"];
	} else {
		$image_users_large_aspectratio = $configs["image_large_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_large_quality"]) && $users_configs["modules_image_crop_large_quality"] != "") {
		$image_users_large_quality = $users_configs["modules_image_crop_large_quality"];
	} else {
		$image_users_large_quality = $configs["image_large_quality"];
	}
	if (isset($users_configs["modules_image_crop_large_width"]) && $users_configs["modules_image_crop_large_width"] != "") {
		$image_users_large_width = $users_configs["modules_image_crop_large_width"];
	} else {
		$image_users_large_width = $configs["image_large_width"];
	}
	if (isset($users_configs["modules_image_crop_large_height"]) && $users_configs["modules_image_crop_large_height"] != "") {
		$image_users_large_height = $users_configs["modules_image_crop_large_height"];
	} else {
		$image_users_large_height = $configs["image_large_height"];
	}

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["users_activate"] == "1") {
	
		if(!isset($parameters["users_username"]) || empty($parameters["users_username"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["users_username"]) && $parameters["users_username"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["users_username"]) && $parameters["users_username"] != "") {
			$sql_check_unique_users_username = "
			SELECT `users_id` FROM `users` 
			WHERE `users_username` LIKE '".$parameters["users_username"]."' 
			
			LIMIT 1";
			$result_check_unique_users_username = mysqli_query($con, $sql_check_unique_users_username);
			$num_check_unique_users_username = mysqli_num_rows($result_check_unique_users_username);
			if ($num_check_unique_users_username > 0) {
				
				$response["status"] = false;
				$response["target"] = "#users_username";
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["users_confirm_password"]) && !empty($parameters["users_confirm_password"])) {
			if(!isset($parameters["users_password"]) || empty($parameters["users_password"])){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is required");
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
			if($parameters["users_password"] != $parameters["users_confirm_password"]){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is mismatched");
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
			if (function_exists("mb_strlen")) {
				if(mb_strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
					$response["target"] = "#users_password";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			} else {
				if(strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
					$response["target"] = "#users_password";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
			if (function_exists("mb_strlen")) {
				if(mb_strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)");
					$response["target"] = "#users_password";
					$response["values"] = $parameters;
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			} else {
				if(strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
					$response["target"] = "#users_password";
					$response["values"] = $parameters;
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
			if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/", $parameters["users_password"]) && $parameters["users_password"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("must contain uppercases, lowercases, digits and special characters");
				$response["target"] = "#users_password";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#users_nick_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_nick_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(isset($parameters["users_email"]) && !empty($parameters["users_email"])){
			if (function_exists("mb_strlen")) {
				if(mb_strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			} else {
				if(strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $parameters["users_email"]) && $parameters["users_email"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is invalid email");
				$response["target"] = "#users_email";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
			if (isset($parameters["users_email"]) && $parameters["users_email"] != "") {
				$sql_check_unique_users_email = "
				SELECT `users_id` FROM `users` 
				WHERE `users_email` LIKE '".$parameters["users_email"]."' 

				LIMIT 1";
				$result_check_unique_users_email = mysqli_query($con, $sql_check_unique_users_email);
				$num_check_unique_users_email = mysqli_num_rows($result_check_unique_users_email);
				if ($num_check_unique_users_email > 0) {

					$response["status"] = false;
					$response["target"] = "#users_email";
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("can not be duplicated");
					$response["values"] = $parameters;
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["groups_id"]) > 100 && $parameters["groups_id"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Group") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#groups_id";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["groups_id"]) > 100 && $parameters["groups_id"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Group") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#groups_id";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["users_level"]) || empty($parameters["users_level"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is required");
			$response["target"] = "#users_level";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_level"]) > 100 && $parameters["users_level"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_level";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_level"]) > 100 && $parameters["users_level"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_level";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {

					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_email_notify";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_email_notify";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_protected"]) > 100 && $parameters["users_protected"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Protected") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_protected";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_protected"]) > 100 && $parameters["users_protected"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Protected") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_protected";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}

	}

	/* initialize: default */
	$users_image = $parameters["users_image"];
	unset($parameters["users_image"]);
	$parameters["users_image"] = "";
	date_default_timezone_set($configs["timezone"]);
	$parameters["users_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if (!isset($parameters["users_users_id"]) || $parameters["users_users_id"] == "") {
		$parameters["users_users_id"] = $_SESSION["users_id"];
		$parameters["users_users_username"] = $_SESSION["users_username"];
		$parameters["users_users_name"] = $_SESSION["users_name"];
		$parameters["users_users_last_name"] = $_SESSION["users_last_name"];
	} else {
		$parameters["users_users_id"] = 0;
		$parameters["users_users_username"] = "";
		$parameters["users_users_name"] = "";
		$parameters["users_users_last_name"] = "";
	}

	/* initialize: prepare data */
	if (!isset($parameters["users_api_sessions"]) || empty($parameters["users_api_sessions"])) {
		if (isset($parameters["users_password"]) && !empty($parameters["users_password"])
		   && isset($parameters["users_confirm_password"]) && !empty($parameters["users_confirm_password"])) {
			$password = md5($parameters["users_password"].$configs["password_salt"]);
			$parameters["users_password"] = $password;
		} else {
			$password_seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789!@#$%^&*()');
			shuffle($password_seed);
			$password_random = "";
			foreach (array_rand($password_seed, 10) as $k) $password_random .= $password_seed[$k];
			$parameters["users_password"] = $password_random;
			$password = md5($password_random.$configs["password_salt"]);
		}
	} else {
		$password = "";
	}

	/* database: insert to module "users" (begin) */
	$sql = "INSERT INTO `users` (
				`users_id`,
				`users_username`,
				`users_password`,
				`users_name`,
				`users_last_name`,
				`users_nick_name`,
				`users_email`,
				`users_api_sessions`,
				`groups_id`,
				`users_level`,
				`users_email_notify`,
				`users_protected`,
				`users_date_created`,
				`users_activate`,
				`users_users_id`,
				`users_users_username`,
				`users_users_name`,
				`users_users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["users_username"] . "',
				'" . $password . "',
				'" . $parameters["users_name"] . "',
				'" . $parameters["users_last_name"] . "',
				'" . $parameters["users_nick_name"] . "',
				'" . $parameters["users_email"] . "',
				'" . $parameters["users_api_sessions"] . "',
				'" . $parameters["groups_id"] . "',
				'" . $parameters["users_level"] . "',
				'" . $parameters["users_email_notify"] . "',
				'" . $parameters["users_protected"] . "',
				'" . $parameters["users_date_created"] . "',
				'" . $parameters["users_activate"] . "',
				'" . $parameters["users_users_id"] . "',
				'" . $parameters["users_users_username"] . "',
				'" . $parameters["users_users_name"] . "',
				'" . $parameters["users_users_last_name"] . "')";
	$result = mysqli_query($con, $sql);
	$parameters["users_id"] = mysqli_insert_id($con);
	/* database: insert to module "users" (end) */

	if ($result) {
		
		if (!isset($parameters["users_api_sessions"]) || empty($parameters["users_api_sessions"])) {
			if (!empty($parameters["users_name"])) {
				$mail_name = $parameters["users_name"];
			} else {
				$mail_name = $parameters["users_username"];
			}
			if (isset($parameters["users_password"]) && !empty($parameters["users_password"])
		   		&& isset($parameters["users_confirm_password"]) && !empty($parameters["users_confirm_password"])) {
				$mail_subject = translate("Welcome to")." ".$configs["site_name"];
				$mail_body = "<html><body><h4>".translate("Welcome").", ".$mail_name."</h4><br /><br />".translate("Your account was created at")." <strong>".$configs["site_name"]."</strong><br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
			} else {
				$mail_subject = translate("Welcome to")." ".$configs["site_name"];
				$mail_body = "<html><body><h4>".translate("Welcome").", ".$mail_name."</h4><br /><br />".translate("Your account was created at")." <strong>".$configs["site_name"]."</strong><br />".translate("Here is the information for your account")."<br /><br /><strong>".translate("Username")."</strong>: ".$parameters["users_username"]."<br /><strong>".translate("Password")."</strong>: ".$parameters["users_password"]."<br /><br />".translate("This password is generated by system and not secured")."<br />".translate("Please login and change your password as soon as possible")."<br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
			}
			send_mail($parameters["users_email"], $configs["site_name"], $configs["email"], $mail_subject, $mail_body);
		}
		/* response: additional data */
		

		/* log (begin) */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $parameters["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_users_data($parameters){

	/* get global: configurations */
	global $configs;
	$users_configs = get_modules_data_by_id("6");

	/* get global: ajax function */
	global $method;
	

	/* configurations: image quality */
	if (isset($users_configs["modules_image_crop_quality"]) && $users_configs["modules_image_crop_quality"] != "") {
		$image_users_quality = $users_configs["modules_image_crop_quality"];
	} else {
		$image_users_quality = $configs["image_quality"];
	}

	/* configurations: thumbnail */
	if (isset($users_configs["modules_image_crop_thumbnail"]) && $users_configs["modules_image_crop_thumbnail"] != "") {
		$image_users_thumbnail = $users_configs["modules_image_crop_thumbnail"];
	} else {
		$image_users_thumbnail = $configs["image_thumbnail"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_aspectratio"]) && $users_configs["modules_image_crop_thumbnail_aspectratio"] != "") {
		$image_users_thumbnail_aspectratio = $users_configs["modules_image_crop_thumbnail_aspectratio"];
	} else {
		$image_users_thumbnail_aspectratio = $configs["image_thumbnail_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_quality"]) && $users_configs["modules_image_crop_thumbnail_quality"] != "") {
		$image_users_thumbnail_quality = $users_configs["modules_image_crop_thumbnail_quality"];
	} else {
		$image_users_thumbnail_quality = $configs["image_thumbnail_quality"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_width"]) && $users_configs["modules_image_crop_thumbnail_width"] != "") {
		$image_users_thumbnail_width = $users_configs["modules_image_crop_thumbnail_width"];
	} else {
		$image_users_thumbnail_width = $configs["image_thumbnail_width"];
	}
	if (isset($users_configs["modules_image_crop_thumbnail_height"]) && $users_configs["modules_image_crop_thumbnail_height"] != "") {
		$image_users_thumbnail_height = $users_configs["modules_image_crop_thumbnail_height"];
	} else {
		$image_users_thumbnail_height = $configs["image_thumbnail_height"];
	}

	/* configurations: large thumbnail */
	if (isset($users_configs["modules_image_crop_large"]) && $users_configs["modules_image_crop_large"] != "") {
		$image_users_large = $users_configs["modules_image_crop_large"];
	} else {
		$image_users_large = $configs["image_large"];
	}
	if (isset($users_configs["modules_image_crop_large_aspectratio"]) && $users_configs["modules_image_crop_large_aspectratio"] != "") {
		$image_users_large_aspectratio = $users_configs["modules_image_crop_large_aspectratio"];
	} else {
		$image_users_large_aspectratio = $configs["image_large_aspectratio"];
	}
	if (isset($users_configs["modules_image_crop_large_quality"]) && $users_configs["modules_image_crop_large_quality"] != "") {
		$image_users_large_quality = $users_configs["modules_image_crop_large_quality"];
	} else {
		$image_users_large_quality = $configs["image_large_quality"];
	}
	if (isset($users_configs["modules_image_crop_large_width"]) && $users_configs["modules_image_crop_large_width"] != "") {
		$image_users_large_width = $users_configs["modules_image_crop_large_width"];
	} else {
		$image_users_large_width = $configs["image_large_width"];
	}
	if (isset($users_configs["modules_image_crop_large_height"]) && $users_configs["modules_image_crop_large_height"] != "") {
		$image_users_large_height = $users_configs["modules_image_crop_large_height"];
	} else {
		$image_users_large_height = $configs["image_large_height"];
	}

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["users_activate"] == "1") {
	
		if(!isset($parameters["users_username"]) || empty($parameters["users_username"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_username";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["users_username"]) && $parameters["users_username"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["users_username"]) && $parameters["users_username"] != "") {
			$sql_check_unique_users_username = "
			SELECT `users_id` FROM `users` 
			WHERE `users_username` LIKE '".$parameters["users_username"]."' 
			AND `users_id` != '".$parameters["users_id"]."'
			LIMIT 1";
			$result_check_unique_users_username = mysqli_query($con, $sql_check_unique_users_username);
			$num_check_unique_users_username = mysqli_num_rows($result_check_unique_users_username);
			if ($num_check_unique_users_username > 0) {
				
				$response["status"] = false;
				$response["target"] = "#users_username";
				$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_last_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#users_nick_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_nick_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(isset($parameters["users_email"]) && !empty($parameters["users_email"])){
			if (function_exists("mb_strlen")) {
				if(mb_strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			} else {
				if(strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

					$response["status"] = false;
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
					$response["target"] = "#users_email";
					$response["values"] = $parameters;
					unset($parameters);
					$parameters = array();
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $parameters["users_email"]) && $parameters["users_email"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is invalid email");
				$response["target"] = "#users_email";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
			if (isset($parameters["users_email"]) && $parameters["users_email"] != "") {
				$sql_check_unique_users_email = "
				SELECT `users_id` FROM `users` 
				WHERE `users_email` LIKE '".$parameters["users_email"]."' 
				AND `users_id` != '".$parameters["users_id"]."'
				LIMIT 1";
				$result_check_unique_users_email = mysqli_query($con, $sql_check_unique_users_email);
				$num_check_unique_users_email = mysqli_num_rows($result_check_unique_users_email);
				if ($num_check_unique_users_email > 0) {

					$response["status"] = false;
					$response["target"] = "#users_email";
					$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("can not be duplicated");
					$response["values"] = $parameters;
					if (isset($method) && $method == __FUNCTION__) {
						echo json_encode($response);
					} else {
						return $response["status"];
					}
					unset($response);
					$response = array();
					unset($method);
					exit();

				}
			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["groups_id"]) > 100 && $parameters["groups_id"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Group") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#groups_id";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["groups_id"]) > 100 && $parameters["groups_id"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Group") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#groups_id";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["users_level"]) || empty($parameters["users_level"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is required");
			$response["target"] = "#users_level";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_level"]) > 100 && $parameters["users_level"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_level";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_level"]) > 100 && $parameters["users_level"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Level") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_level";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_email_notify";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_email_notify";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_protected"]) > 100 && $parameters["users_protected"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Protected") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_protected";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_protected"]) > 100 && $parameters["users_protected"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Protected") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_protected";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}

	/* database: get old data from module "users" (begin) */
	$sql = "SELECT *
	        FROM   `users`
			WHERE  `users_id` = '" . $parameters["users_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["users_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "users" (end) */
	
	/* initialize: default */
	$users_image = $parameters["users_image"];
	unset($parameters["users_image"]);
	$parameters["users_image"] = "";
	
	$parameters["users_date_created"] = $query["users_date_created"];

	/* database: update to module "users" (begin) */
	$sql = "UPDATE `users`
			SET 
				   `users_username` = '" . $parameters["users_username"] . "',
				   `users_name` = '" . $parameters["users_name"] . "',
				   `users_last_name` = '" . $parameters["users_last_name"] . "',
				   `users_nick_name` = '" . $parameters["users_nick_name"] . "',
				   `users_email` = '" . $parameters["users_email"] . "',
				   `groups_id` = '" . $parameters["groups_id"] . "',
				   `users_level` = '" . $parameters["users_level"] . "',
				   `users_email_notify` = '" . $parameters["users_email_notify"] . "',
				   `users_protected` = '" . $parameters["users_protected"] . "',
				   `users_date_created` = '" . $parameters["users_date_created"] . "',
				   `users_activate` = '" . $parameters["users_activate"] . "'
			WHERE  `users_id` = '" . $parameters["users_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "users" (end) */

	if ($result) {
		
		if (!empty($parameters["users_name"])) {
			$mail_name = $parameters["users_name"];
		} else {
			$mail_name = $parameters["users_username"];
		}
		$mail_subject = translate("Your account details was changed at")." ".$configs["site_name"];
		$mail_body = "<html><body><h4>".translate("Hello").", ".$mail_name."</h4><br /><br />".translate("Your account detail was changed at")." <strong>".$configs["site_name"]."</strong><br />".translate("If you have any questions please contact administrator")."<br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
		send_mail($parameters["users_email"], $configs["site_name"], $configs["email"], $mail_subject, $mail_body);
		/* response: additional data */
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $parameters["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "users";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_users_personal_data($parameters){

	/* get global: configurations */
	global $configs;
	$users_configs = get_modules_data_by_id("6");

	/* get global: ajax function */
	global $method;
	

    $con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	if(!isset($parameters["users_username"]) || empty($parameters["users_username"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is required");
		$response["target"] = "#users_username";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_username"]) > 100 && $parameters["users_username"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_username";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/", $parameters["users_username"]) && $parameters["users_username"] != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("does not allow special characters and white spaces excluding ., -, _");
		$response["target"] = "#users_username";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (isset($parameters["users_username"]) && $parameters["users_username"] != "") {
		$sql_check_unique_users_username = "
		SELECT `users_id` FROM `users` 
		WHERE `users_username` LIKE '".$parameters["users_username"]."' 
		AND `users_id` != '".$_SESSION["users_id"]."'
		LIMIT 1";
		$result_check_unique_users_username = mysqli_query($con, $sql_check_unique_users_username);
		$num_check_unique_users_username = mysqli_num_rows($result_check_unique_users_username);
		if ($num_check_unique_users_username > 0) {
			
			$response["status"] = false;
			$response["target"] = "#users_username";
			$response["message"] = "<strong>" . translate("Username") . "</strong> " . translate("can not be duplicated");
			$response["values"] = $parameters;
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
			$response["target"] = "#users_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_name"]) > 100 && $parameters["users_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
			$response["target"] = "#users_last_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_last_name"]) > 100 && $parameters["users_last_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Last Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_last_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
			$response["target"] = "#users_nick_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_nick_name"]) > 50 && $parameters["users_nick_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Nickname") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_nick_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(isset($parameters["users_email"]) && !empty($parameters["users_email"])){
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#users_email";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["users_email"]) > 100 && $parameters["users_email"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#users_email";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $parameters["users_email"]) && $parameters["users_email"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("is invalid email");
			$response["target"] = "#users_email";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["users_email"]) && $parameters["users_email"] != "") {
			$sql_check_unique_users_email = "
			SELECT `users_id` FROM `users` 
			WHERE `users_email` LIKE '".$parameters["users_email"]."' 
			AND `users_id` != '".$_SESSION["users_id"]."'
			LIMIT 1";
			$result_check_unique_users_email = mysqli_query($con, $sql_check_unique_users_email);
			$num_check_unique_users_email = mysqli_num_rows($result_check_unique_users_email);
			if ($num_check_unique_users_email > 0) {

				$response["status"] = false;
				$response["target"] = "#users_email";
				$response["message"] = "<strong>" . translate("Email") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
			$response["target"] = "#users_email_notify";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_email_notify"]) > 100 && $parameters["users_email_notify"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Enable Email Notification") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_email_notify";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}

	/* database: get old data from module "users" (begin) */
	$sql = "SELECT *
	        FROM   `users`
			WHERE  `users_id` = '" . $_SESSION["users_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["users_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "users" (end) */
	
	/* initialize: default */
	$users_image = $parameters["users_image"];
	unset($parameters["users_image"]);
	$parameters["users_image"] = "";
	$parameters["users_id"] = $_SESSION["users_id"];
	
	$parameters["users_date_created"] = $query["users_date_created"];

	/* database: update to module "users" (begin) */
	$sql = "UPDATE `users`
			SET 
				   `users_username` = '" . $parameters["users_username"] . "',
				   `users_name` = '" . $parameters["users_name"] . "',
				   `users_last_name` = '" . $parameters["users_last_name"] . "',
				   `users_nick_name` = '" . $parameters["users_nick_name"] . "',
				   `users_email` = '" . $parameters["users_email"] . "',
				   `users_email_notify` = '" . $parameters["users_email_notify"] . "'
			WHERE  `users_id` = '" . $parameters["users_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "users" (end) */

	if ($result) {

		$_SESSION["users_username"] = $parameters["users_username"];
		$_SESSION["users_name"] = $parameters["users_name"];
		$_SESSION["users_last_name"] = $parameters["users_last_name"];
		$_SESSION["users_nick_name"] = $parameters["users_nick_name"];
		$_SESSION["users_email"] = $parameters["users_email"];
		$_SESSION["users_email_notify"] = $parameters["users_email_notify"];
		
		if (!empty($parameters["users_name"])) {
			$mail_name = $parameters["users_name"];
		} else {
			$mail_name = $parameters["users_username"];
		}
		$mail_subject = translate("Your account details was changed at")." ".$configs["site_name"];
		$mail_body = "<html><body><h4>".translate("Hello").", ".$mail_name."</h4><br /><br />".translate("Your account detail was changed at")." <strong>".$configs["site_name"]."</strong><br />".translate("If you have any questions please contact administrator")."<br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
		send_mail($parameters["users_email"], $configs["site_name"], $configs["email"], $mail_subject, $mail_body);
		/* response: additional data */
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $parameters["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "users";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_users_password_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if(isset($parameters["users_current_password"]) && !empty($parameters["users_current_password"])){

		$sql_check_users_current_password = "
		SELECT `users_id` FROM `users` 
		WHERE `users_password` = '".md5($parameters['users_current_password'].$configs["password_salt"])."' 
		AND `users_id` = '".$_SESSION["users_id"]."';";
		$result_check_users_current_password = mysqli_query($con, $sql_check_users_current_password);
		$num_check_users_current_password = mysqli_num_rows($result_check_users_current_password);
		if ($num_check_users_current_password <= 0) {

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Current Password") . "</strong> " . translate("is invalid");
			$response["target"] = "#users_current_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}

	}
	if(!isset($parameters["users_password"]) || empty($parameters["users_password"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is required");
		$response["target"] = "#users_password";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(!isset($parameters["users_confirm_password"]) || empty($parameters["users_confirm_password"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Confirm Password") . "</strong> " . translate("is required");
		$response["target"] = "#users_confirm_password";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if($parameters["users_password"] != $parameters["users_confirm_password"]){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is mismatched");
		$response["target"] = "#users_password";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_password"]) > 100 && $parameters["users_password"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)");
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["users_password"]) < 8 && $parameters["users_password"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("is shorter than minimum length of strings at") . " 8 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#users_password";
			$response["values"] = $parameters;
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/", $parameters["users_password"]) && $parameters["users_password"] != ""){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Password") . "</strong> " . translate("must contain uppercases, lowercases, digits and special characters");
		$response["target"] = "#users_password";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}

	/* database: get old data from module "users" (begin) */
	$sql = "SELECT *
	        FROM   `users`
			WHERE  `users_id` = '" . $_SESSION["users_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["users_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "users" (end) */
	
	/* initialize: prepare data */
	$password = md5($parameters["users_password"].$configs["password_salt"]);
	$parameters["users_password"] = $password;
	
	/* database: update to module "users" (begin) */
	$sql = "UPDATE `users`
			SET 
				   `users_password` = '" . $password . "'
			WHERE  `users_id` = '" . $_SESSION["users_id"] . "'";
	$result = mysqli_query($con, $sql) or die(mysqli_error($con));
	/* database: update to module "users" (end) */

	if ($result) {
		
		$_SESSION["users_password"] = $password;
		
		unset($parameters["users_password"]);
		unset($parameters["users_confirm_password"]);
		
		if (!empty($_SESSION["users_name"])) {
			$mail_name = $_SESSION["users_name"];
		} else {
			$mail_name = $_SESSION["users_username"];
		}
		$mail_subject = translate("Your passord was changed at")." ".$configs["site_name"];
		$mail_body = "<html><body><h4>".translate("Hello").", ".$mail_name."</h4><br /><br />".translate("Your account detail was changed at")." <strong>".$configs["site_name"]."</strong><br />".translate("If you have any questions please contact administrator")."<br /><br /><br /><br />".translate("Thanks").",<br /><a href='".$configs["base_url"]."' target='_blank'>".$configs["site_name"]."</a></body></html>";
		send_mail($_SESSION["users_email"], $configs["site_name"], $configs["email"], $mail_subject, $mail_body);
		/* response: additional data */
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $_SESSION["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "users";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_users_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "users" (begin) */
	$sql = "SELECT *
	        FROM   `users`
			WHERE  `users_id` = '" . $parameters["users_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["users_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "users" (end) */
	
	/* initialize: default */
	$parameters["users_date_created"] = $query["users_date_created"];

	/* database: update to module "users" (begin) */
	$sql = "UPDATE `users`
			SET    `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `users_id` = '" . $parameters["users_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "users" (end) */

	if ($result) {
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "users_id",
				"modules_record_target" => $parameters["users_id"],
				"modules_id" => "5",
				"modules_name" => "Users",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "users";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_users_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `users`
	WHERE `users_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["users_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "users" (begin) */
		$sql = "
		DELETE FROM `users`
		WHERE `users_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "users" (end) */

		if ($result) {
	
			$redirectdir = "/media/images/users/";
			$serverdir = "../../../../media/images/users/";
	
			/* image delete handler for "users_image" (begin) */
			if ($parameters["users_old"]["users_image"] != "") {
				$users_image_path = "../.." . $parameters["users_old"]["users_image"];
				if (file_exists($users_image_path)){
					chmod($users_image_path, 0777);
					unlink($users_image_path);
				}

				/* thumbnail delete handler for "users_image" (begin) */
				if ($image_users_thumbnail == true) {
					$image_old = $parameters["users_old"]["users_image"];
					$redirectdir = "/media/images/users/";
					$redirectdir_thumbnail = $redirectdir."thumbnail/";
					$image_thumbnail_old = str_replace($redirectdir, $redirectdir_thumbnail, $image_old);
					$image_thumbnail_old_path = "../..".$image_thumbnail_old;
					if (file_exists($image_thumbnail_old_path)){
						chmod($image_thumbnail_old_path, 0777);
						unlink($image_thumbnail_old_path);
					}
				}
				/* thumbnail delete handler for "users_image" (end) */

				/* large thumbnail delete handler for "users_image" (begin) */
				if ($image_users_large == true) {
					$image_old = $parameters["users_old"]["users_image"];
					$redirectdir = "/media/images/users/";
					$redirectdir_large  = $redirectdir."large/";
					$image_large_old = str_replace($redirectdir, $redirectdir_large, $image_old);
					$image_large_old_path = "../..".$image_large_old;
					if (file_exists($image_large_old_path)){
						chmod($image_large_old_path, 0777);
						unlink($image_large_old_path);
					}
				}
				/* large thumbnail delete handler for "users_image" (end) */
				
			}
			/* image delete handler for "users_image" (end) */
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "users_id",
					"modules_record_target" => $target,
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_users_upload_file($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* database: get old file data from module "users" (begin) */
	$sql = "SELECT `".$parameters["module_fieldname"]."` FROM `users`
	        WHERE  `users_id` = '" . $parameters["target_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	mysqli_free_result($result);
	/* database: get old file data from module "users" (begin) */
	
	/* delete file */
	if (!empty($query[$parameters["module_fieldname"]])) {
		$old_file_path = "../..".$query[$parameters["module_fieldname"]];
		if (file_exists($old_file_path)){
			chmod($old_file_path, 0777);
			unlink($old_file_path);
		}
	}

	/* database: delete upload file data from module "users" (begin) */
	$sql = "UPDATE `users`
	        SET    `".$parameters["module_fieldname"]."` = ''
			WHERE  `users_id` = '" . $parameters["target_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: delete upload file data from module "users" (end) */

	if ($result) {
		
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "delete file",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "users_id",
					"modules_record_target" => $parameters["target_id"],
					"modules_id" => "5",
					"modules_name" => "Users",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully delete file");
			$response["values"] = $parameters;
			$response["values"]["data_content"] = $new_file;

			unset($parameters);
			$parameters = array();
	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_users_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Username")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Last Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Nickname")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Email")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Group")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Level")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Enable Email Notification")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
			} else {
				$column_name = str_replace("users", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "users_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "users_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "users_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "users_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "users_username") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_username . '" data-toggle="tooltip" title="Username" data-original-title="Username">' . htmlspecialchars(strip_tags(translate("Username"))) . '</th>';
			} else if ($key == "users_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_name . '" data-toggle="tooltip" title="Name" data-original-title="Name">' . htmlspecialchars(strip_tags(translate("Name"))) . '</th>';
			} else if ($key == "users_last_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_last_name . '" data-toggle="tooltip" title="Last Name" data-original-title="Last Name">' . htmlspecialchars(strip_tags(translate("Last Name"))) . '</th>';
			} else if ($key == "users_nick_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_nick_name . '" data-toggle="tooltip" title="Nickname" data-original-title="Nickname">' . htmlspecialchars(strip_tags(translate("Nickname"))) . '</th>';
			} else if ($key == "users_email") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_email . '" data-toggle="tooltip" title="Email" data-original-title="Email">' . htmlspecialchars(strip_tags(translate("Email"))) . '</th>';
			} else if ($key == "groups_id") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . groups_id . '" data-toggle="tooltip" title="Group" data-original-title="Group">' . htmlspecialchars(strip_tags(translate("Group"))) . '</th>';
			} else if ($key == "users_level") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_level . '" data-toggle="tooltip" title="Level" data-original-title="Level">' . htmlspecialchars(strip_tags(translate("Level"))) . '</th>';
			} else if ($key == "users_image") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_image . '" data-toggle="tooltip" title="Image" data-original-title="Image">' . htmlspecialchars(strip_tags(translate("Image"))) . '</th>';
			} else if ($key == "users_email_notify") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_email_notify . '" data-toggle="tooltip" title="Enable Email Notification" data-original-title="Enable Email Notification">' . htmlspecialchars(strip_tags(translate("Enable Email Notification"))) . '</th>';
			} else if ($key == "users_protected") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . users_protected . '" data-toggle="tooltip" title="Protected" data-original-title="Protected">' . htmlspecialchars(strip_tags(translate("Protected"))) . '</th>';
			} else if ($key == "users_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User Created") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "users_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["users_id"].'" data-target="'.$data_table_row["users_id"].'">';

		$html .= inform_users_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_users_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "users_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "users_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "users_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "users_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "users_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "users_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_users_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_users_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_users_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_users_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}

		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_users_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_users_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_users_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_users_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}
		
	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_users_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_users_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_users_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_users_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["users_id"].'" data-target="'.$data_table["users_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_users_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_users_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_users_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "users_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "users_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "users_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "users_actions") {
				if ($key == "users_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "users_username") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_username"]));
				} else if ($key == "users_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_name"]));
				} else if ($key == "users_last_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_last_name"]));
				} else if ($key == "users_nick_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_nick_name"]));
				} else if ($key == "users_email") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_email"]));
				} else if ($key == "groups_id") {
					$td_align_class = "";
					$con = start();
					$sql_groups_id  = "
					SELECT `groups_id`, `groups_name`
					FROM   `groups`
					WHERE  `groups_id` = '".$data_table["groups_id"]."'
					LIMIT  1";
					$result_groups_id = mysqli_query($con, $sql_groups_id);
					$num_groups_id = mysqli_num_rows($result_groups_id);
					if ($num_groups_id > 0) {
						$query_groups_id = mysqli_fetch_array($result_groups_id);
						$module_link = false;
						foreach (array_keys($query_groups_id) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_groups_id['pages_link']).'" target="_blank">'.strip_tags($query_groups_id['groups_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_groups_id['groups_name']);
						}
					}
					mysqli_free_result($result_groups_id);
					stop($con);
				} else if ($key == "users_level") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["users_level"]));
				} else if ($key == "users_image") {
					$td_align_class = "text-center";
					$table_row_value = render_users_table_image($data_table["users_image"]);
				} else if ($key == "users_email_notify") {
					$td_align_class = ".text-center";
					$users_email_notify_default_value = unserialize("");
					if ($users_email_notify_default_value[0] != "" && isset($users_email_notify_default_value[0])) {
						if ($data_table["users_email_notify"] == $users_email_notify_default_value[0]) {
							if ($users_email_notify_default_value[0] == "1") {
								$users_email_notify_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$users_email_notify_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["users_email_notify"])) . '</span>';
							}
						} else {
							if ($users_email_notify_default_value[0] == "1") {
								$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["users_email_notify"] == "1") {
							$users_email_notify_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $users_email_notify_data_table_value;
				} else if ($key == "users_protected") {
					$td_align_class = ".text-center";
					$users_protected_default_value = unserialize("");
					if ($users_protected_default_value[0] != "" && isset($users_protected_default_value[0])) {
						if ($data_table["users_protected"] == $users_protected_default_value[0]) {
							if ($users_protected_default_value[0] == "1") {
								$users_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$users_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["users_protected"])) . '</span>';
							}
						} else {
							if ($users_protected_default_value[0] == "1") {
								$users_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$users_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["users_protected"] == "1") {
							$users_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$users_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $users_protected_data_table_value;
				} else if ($key == "users_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="'.backend_rewrite_url("users.php").'?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_users_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["users_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);

				$html .= '
		<td id="datatable-actions-'.$data_table["users_id"].'" class="text-center">
			<div class="btn-group">';

				$html .= '
				<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["users_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
				<i class="fa fa-eye"></i>
				</button>';

				if ($_SESSION["users_level"] != "5") {
			
					if (!isset($data_table["users_protected"]) || (isset($data_table["users_protected"]) && $data_table["users_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

					$html .= '
				<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["users_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
				<i class="fa fa-pencil"></i>
				</button>';
						
					}

					$html .= '
				<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["users_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
				<i class="fa fa-copy"></i>
				</button>';

					$html .= '
				<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["users_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_unassoc[1]])).'</strong>?">
				<i class="fa fa-times"></i>
				</button>';
					
				}

				$html .= '
			</div>
		</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_users_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_users_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Username")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Last Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Nickname")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Email")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Group")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Level")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Enable Email Notification")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Protected")));
			} else {
				$column_name = str_replace("users", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "users_actions") {
				if ($key == "users_id") {
					if (!empty($data_table["users_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_username") {
					if ($data_table["users_username"] != "") {
						$table_row_value = '<strong>' . translate("Username") . '</strong><br />' . $data_table["users_username"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_name") {
					if ($data_table["users_name"] != "") {
						$table_row_value = '<strong>' . translate("Name") . '</strong><br />' . $data_table["users_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					if ($data_table["users_last_name"] != "") {
						$table_row_value = '<strong>' . translate("Last Name") . '</strong><br />' . $data_table["users_last_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_nick_name") {
					if ($data_table["users_nick_name"] != "") {
						$table_row_value = '<strong>' . translate("Nickname") . '</strong><br />' . $data_table["users_nick_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_email") {
					if ($data_table["users_email"] != "") {
						$table_row_value = '<strong>' . translate("Email") . '</strong><br />' . $data_table["users_email"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "groups_id") {
					if ($data_table["groups_id"] != "") {
						$con = start();
						$sql_groups_id  = "
						SELECT `groups_id`, `groups_name`
						FROM   `groups`
						WHERE  `groups_id` = '".$data_table["groups_id"]."'
						LIMIT  1";
						$result_groups_id = mysqli_query($con, $sql_groups_id);
						$num_groups_id = mysqli_num_rows($result_groups_id);
						if ($num_groups_id > 0) {
							$query_groups_id = mysqli_fetch_array($result_groups_id);
							$module_link = false;
							foreach (array_keys($query_groups_id) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Group") . '</strong><br /><a href="'.rewrite_url($query_groups_id['pages_link']).'" target="_blank">'.strip_tags($query_groups_id['groups_name']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Group") . '</strong><br />'.strip_tags($query_groups_id['groups_name']).'<br /><br />';
							}
						}
						mysqli_free_result($result_groups_id);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_level") {
					if ($data_table["users_level"] != "") {
						$table_row_value = '<strong>' . translate("Level") . '</strong><br />' . $data_table["users_level"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_image") {
					if ($data_table["users_image"] != "") {
						$table_row_value = '<strong>' . translate("Image") . '</strong><br />'.render_users_view_image($data_table["users_image"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_email_notify") {
					if ($data_table["users_email_notify"] != "") {
						$users_email_notify_default_value = unserialize("");
						if ($users_email_notify_default_value[0] != "" && isset($users_email_notify_default_value[0])) {
							if ($data_table["users_email_notify"] == $users_email_notify_default_value[0]) {
								if ($users_email_notify_default_value[0] == "1") {
									$users_email_notify_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$users_email_notify_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["users_email_notify"])) . '</span>';
								}
							} else {
								if ($users_email_notify_default_value[0] == "1") {
									$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["users_email_notify"] == "1") {
								$users_email_notify_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$users_email_notify_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Enable Email Notification") . '</strong><br />' . $users_email_notify_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_protected") {
					if ($data_table["users_protected"] != "") {
						$users_protected_default_value = unserialize("");
						if ($users_protected_default_value[0] != "" && isset($users_protected_default_value[0])) {
							if ($data_table["users_protected"] == $users_protected_default_value[0]) {
								if ($users_protected_default_value[0] == "1") {
									$users_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$users_protected_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["users_protected"])) . '</span>';
								}
							} else {
								if ($users_protected_default_value[0] == "1") {
									$users_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$users_protected_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["users_protected"] == "1") {
								$users_protected_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$users_protected_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Protected") . '</strong><br />' . $users_protected_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "users_date_created") {
					if (!empty($data_table["users_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["users_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {

						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_users_id") {
					$i++;
				} else if ($key == "users_users_username") {
					$i++;
				} else if ($key == "users_users_name") {
					if (!empty($data_table["users_users_name"])) {
						$table_row_value = '<strong>' . translate("User Created") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
                    if (!empty($data_table[$key])) {
                        if (($key == "pages_link" || $module_link) && $i == 1) {
                            $table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
                        } else {
                            $table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_users_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
                    }
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_users_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';
	
	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_users_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_users_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_users_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($file_check,  CURLOPT_SSL_VERIFYPEER, false);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_users_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "users" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '6'
	AND (`log_function` = 'create_users_data' OR `log_function` = 'update_users_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				
				$query["users_remember_login"] = unserialize($query["users_remember_login"]);
				$query["users_api_sessions"] = unserialize($query["users_api_sessions"]);

				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	

	/* get global: ajax function */
	global $method;

    $con = start();


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "users" */
	$sql = "
	EXPLAIN SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '5'
	AND (`log_function` = 'create_users_data' OR `log_function` = 'update_users_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	mysqli_free_result($result);
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "users" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["users_image"]) && !empty($query_log_content_hash["users_image"])) {
			if (strpos($query_log_content_hash["users_image"], "http://") !== false || strpos($query_log_content_hash["users_image"], "https://") !== false) {
				$query_log_content_hash["users_image_path"] = $query_log_content_hash["users_image"];
			} else {
				$query_log_content_hash["users_image_path"] = $configs["base_url"].$query_log_content_hash["users_image"];
			}
			$query_log_content_hash["users_image_thumbnail_path"] = str_replace("users/", "users/thumbnail/", $configs["base_url"].$query_log_content_hash["users_image"]);
			$query_log_content_hash["users_image_large_thumbnail_path"] = str_replace("users/", "users/large/", $configs["base_url"].$query_log_content_hash["users_image"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	


    $con = start();

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "users" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_users_data_all_by_groups_id($target, $start = 0, $limit = "", $sort_by = "users_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `users_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `users`
	WHERE `groups_id` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["users_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["users_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_users_data_all_by_groups_id($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `users_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "users" */
	$sql = "
	EXPLAIN SELECT *
	FROM `users`
	WHERE `groups_id` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_id_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `users_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "groups" */
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["groups_id_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_id_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_groups_id_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `users_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "groups" */
	$sql = "
	EXPLAIN SELECT *
	FROM `groups`
	WHERE `groups_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$query = mysqli_fetch_array($result);
		$num = $query["rows"];

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_groups_id_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_all .= "AND `users_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "groups" */
	$sql = "
	SELECT *
	FROM `groups`
	WHERE `groups_id` = '".$target."'
	".$extra_sql_data_all."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["groups_id_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["groups_id_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "users";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>users</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

?>
