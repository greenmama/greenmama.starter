<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "get_log_data_by_id" 
		|| $method == "get_log_data_all" 
		|| $method == "count_log_data_all" 
		|| $method == "get_log_data_all_excluding" 
		|| $method == "count_log_data_all_excluding" 
		|| $method == "get_log_data_table_all" 
		|| $method == "count_log_data_table_all" 
		|| $method == "get_search_log_data_table_all" 
		|| $method == "count_search_log_data_table_all" 
		|| $method == "create_log_data" 
		|| $method == "update_log_data" 
		|| $method == "patch_log_data" 
		|| $method == "delete_log_data" 
		|| $method == "create_log_table" 
		|| $method == "create_log_table_row" 
		|| $method == "update_log_table_row" 
		|| $method == "delete_log_table_row" 
		|| $method == "create_log_table_row" 
		|| $method == "update_log_table_row" 
		|| $method == "inform_log_table_row" 
		|| $method == "view_log_table_row")
) {
	
	/* include: configurations */
	require_once("../../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	require_once("mail.php");
	
	/* include: functions - core */
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");
	
	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "get_log_data_by_id"){
		get_log_data_by_id($parameters);
	} else if ($method == "get_log_data_all"){
		get_log_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_log_data_all"){
		count_log_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_log_data_all_excluding"){
		get_log_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_log_data_all_excluding"){
		count_log_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_log_data_table_all"){
		get_log_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_log_data_table_all"){
		count_log_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_log_data_table_all"){
		get_search_log_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_log_data_table_all"){
		count_search_log_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_log_data"){
		create_log_data($parameters);
	} else if ($method == "update_log_data"){
		update_log_data($parameters);
	} else if ($method == "patch_log_data"){
		patch_log_data($parameters);
	} else if ($method == "delete_log_data"){
		delete_log_data($parameters);
	} else if ($method == "create_log_table"){
		create_log_table($parameters, $table_module_field); 
	} else if ($method == "create_log_table_row"){
		create_log_table_row($parameters, $table_module_field);
	} else if ($method == "update_log_table_row"){
		update_log_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_log_table_row"){
		inform_log_table_row($parameters, $table_module_field);
	} else if ($method == "view_log_table_row"){
		view_log_table_row($parameters, $table_module_field);
	}
}

function get_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	

	/* database: get data by ID from module "log" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `log`.`log_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
			$query["log_date_created_formatted"] = datetime_reformat($query["log_date_created"]);
			
		if (isset($query["log_content_hash"]) && !empty($query["log_content_hash"])) {
			$query["log_content_hash"] = str_replace("<br />", "\n", $query["log_content_hash"]);
		}
		if (isset($query["log_user_agent"]) && !empty($query["log_user_agent"])) {
			$query["log_user_agent"] = str_replace("<br />", "\n", $query["log_user_agent"]);
		}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_log_data_all($start = 0, $limit = "", $sort_by = "log_id", $sort_direction = "desc", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "log" */
	$sql = "
	SELECT *
	FROM   `log`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
		if (isset($query["log_content_hash"]) && !empty($query["log_content_hash"])) {
			$query["log_content_hash"] = str_replace("<br />", "\n", $query["log_content_hash"]);
		}
		if (isset($query["log_user_agent"]) && !empty($query["log_user_agent"])) {
			$query["log_user_agent"] = str_replace("<br />", "\n", $query["log_user_agent"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_log_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "log" */
	if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_log_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "log_id", $sort_direction = "desc", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "log" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
		if (isset($query["log_content_hash"]) && !empty($query["log_content_hash"])) {
			$query["log_content_hash"] = str_replace("<br />", "\n", $query["log_content_hash"]);
		}
		if (isset($query["log_user_agent"]) && !empty($query["log_user_agent"])) {
			$query["log_user_agent"] = str_replace("<br />", "\n", $query["log_user_agent"]);
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_log_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "log" */
	if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `log`
	WHERE `log_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_log_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "log_id", $sort_direction = "desc", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "log" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
		if (isset($query["log_content_hash"]) && !empty($query["log_content_hash"])) {
			$query["log_content_hash"] = str_replace("<br />", "\n", $query["log_content_hash"]);
		}
		if (isset($query["log_user_agent"]) && !empty($query["log_user_agent"])) {
			$query["log_user_agent"] = str_replace("<br />", "\n", $query["log_user_agent"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_log_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "log" */
	if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_log_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "log_id", $sort_direction = "desc", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'log'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "log_actions" 
				&& $key != "log_date_created" 
				&& $key != "log_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "log" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
		if (isset($query["log_content_hash"]) && !empty($query["log_content_hash"])) {
			$query["log_content_hash"] = str_replace("<br />", "\n", $query["log_content_hash"]);
		}
		if (isset($query["log_user_agent"]) && !empty($query["log_user_agent"])) {
			$query["log_user_agent"] = str_replace("<br />", "\n", $query["log_user_agent"]);
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_log_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'log'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "log_actions" 
				&& $key != "log_date_created" 
				&& $key != "log_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "log" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `log_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `log`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_log_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if(!isset($parameters["log_name"]) || empty($parameters["log_name"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is required");
		$response["target"] = "#log_name";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_name"]) > 200 && $parameters["log_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
			$response["target"] = "#log_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_name"]) > 200 && $parameters["log_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!isset($parameters["log_function"]) || empty($parameters["log_function"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is required");
		$response["target"] = "#log_function";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_function"]) > 150 && $parameters["log_function"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is longer than maximum length of strings at") . " 150 " . translate("character(s)");
			$response["target"] = "#log_function";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_function"]) > 150 && $parameters["log_function"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is longer than maximum length of strings at") . " 150 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_function";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!isset($parameters["log_violation"]) || empty($parameters["log_violation"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Violation level") . "</strong> " . translate("is required");
		$response["target"] = "#log_violation";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(!isset($parameters["log_type"]) || empty($parameters["log_type"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Type") . "</strong> " . translate("is required");
		$response["target"] = "#log_type";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_ip"]) > 15 && $parameters["log_ip"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("IP address") . "</strong> " . translate("is longer than maximum length of strings at") . " 15 " . translate("character(s)");
			$response["target"] = "#log_ip";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_ip"]) > 15 && $parameters["log_ip"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("IP address") . "</strong> " . translate("is longer than maximum length of strings at") . " 15 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_ip";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}

	/* initialize: default */
	date_default_timezone_set($configs["timezone"]);
	$parameters["log_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */

	/* database: insert to module "log" (begin) */
	$sql = "INSERT INTO `log` (
				`log_id`,
				`log_name`,
				`log_function`,
				`log_violation`,
				`log_content_hash`,
				`log_link`,
				`log_type`,
				`log_ip`,
				`log_user_agent`,
				`modules_id`,
				`modules_name`,
				`modules_record_target`,
				`modules_record_key`,
				`log_date_created`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["log_name"] . "',
				'" . $parameters["log_function"] . "',
				'" . $parameters["log_violation"] . "',
				'" . $parameters["log_content_hash"] . "',
				'" . $parameters["log_link"] . "',
				'" . $parameters["log_type"] . "',
				'" . $parameters["log_ip"] . "',
				'" . $parameters["log_user_agent"] . "',
				'" . $parameters["modules_id"] . "',
				'" . $parameters["modules_name"] . "',
				'" . $parameters["modules_record_target"] . "',
				'" . $parameters["modules_record_key"] . "',
				'" . $parameters["log_date_created"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "')";
	$result = mysqli_query($con, $sql);
	$parameters["log_id"] = mysqli_insert_id($con);
	/* database: insert to module "log" (end) */

	if ($result) {
		

		/* response: additional data */


		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_log_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if(!isset($parameters["log_name"]) || empty($parameters["log_name"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is required");
		$response["target"] = "#log_name";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_name"]) > 200 && $parameters["log_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
			$response["target"] = "#log_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_name"]) > 200 && $parameters["log_name"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Log name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!isset($parameters["log_function"]) || empty($parameters["log_function"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is required");
		$response["target"] = "#log_function";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_function"]) > 150 && $parameters["log_function"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is longer than maximum length of strings at") . " 150 " . translate("character(s)");
			$response["target"] = "#log_function";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_function"]) > 150 && $parameters["log_function"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Function") . "</strong> " . translate("is longer than maximum length of strings at") . " 150 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_function";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}
	if(!isset($parameters["log_violation"]) || empty($parameters["log_violation"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Violation level") . "</strong> " . translate("is required");
		$response["target"] = "#log_violation";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if(!isset($parameters["log_type"]) || empty($parameters["log_type"])){

		$response["status"] = false;
		$response["message"] = "<strong>" . translate("Type") . "</strong> " . translate("is required");
		$response["target"] = "#log_type";
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();

	}
	if (function_exists("mb_strlen")) {
		if(mb_strlen($parameters["log_ip"]) > 15 && $parameters["log_ip"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("IP address") . "</strong> " . translate("is longer than maximum length of strings at") . " 15 " . translate("character(s)");
			$response["target"] = "#log_ip";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	} else {
		if(strlen($parameters["log_ip"]) > 15 && $parameters["log_ip"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("IP address") . "</strong> " . translate("is longer than maximum length of strings at") . " 15 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
			$response["target"] = "#log_ip";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}

	/* database: get old data from module "log" (begin) */
	$sql = "SELECT *
	        FROM   `log`
			WHERE  `log_id` = '" . $parameters["log_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["log_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "log" (end) */
	
	/* initialize: default */
	
	$parameters["log_date_created"] = $query["log_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	/* database: update to module "log" (begin) */
	$sql = "UPDATE `log`
			SET 
				   `log_name` = '" . $parameters["log_name"] . "',
				   `log_function` = '" . $parameters["log_function"] . "',
				   `log_violation` = '" . $parameters["log_violation"] . "',
				   `log_content_hash` = '" . $parameters["log_content_hash"] . "',
				   `log_link` = '" . $parameters["log_link"] . "',
				   `log_type` = '" . $parameters["log_type"] . "',
				   `log_ip` = '" . $parameters["log_ip"] . "',
				   `log_user_agent` = '" . $parameters["log_user_agent"] . "',
				   `log_date_created` = '" . $parameters["log_date_created"] . "',
				   `modules_id` = '".$parameters["modules_id"]."',
				   `modules_name` = '".$parameters["modules_name"]."',
				   `modules_record_target` = '".$parameters["modules_record_target"]."',
				   `modules_record_key` = '".$parameters["modules_record_key"]."'
			WHERE  `log_id` = '" . $parameters["log_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "log" (end) */

	if ($result) {
		
		/* response: additional data */


		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "log";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_log_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "log" (begin) */
	$sql = "SELECT *
	        FROM   `log`
			WHERE  `log_id` = '" . $parameters["log_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["log_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "log" (end) */
	
	/* initialize: default */
	$parameters["log_date_created"] = $query["log_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "log" (begin) */
	$sql = "UPDATE `log`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `log_id` = '" . $parameters["log_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "log" (end) */

	if ($result) {
		

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "log";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_log_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["log_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "log" (begin) */
		$sql = "
		DELETE FROM `log`
		WHERE `log_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "log" (end) */

		if ($result) {

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_log_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Log name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Function")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module key")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Violation level")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Content hash")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "IP address")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "User agent")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module name")));
			} else {
				$column_name = str_replace("log", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3) {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4) {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5) {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "log_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "log_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_name . '" data-toggle="tooltip" title="Log name" data-original-title="Log name">' . htmlspecialchars(strip_tags(translate("Log name"))) . '</th>';
			} else if ($key == "log_function") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_function . '" data-toggle="tooltip" title="Function" data-original-title="Function">' . htmlspecialchars(strip_tags(translate("Function"))) . '</th>';
			} else if ($key == "log_violation") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_violation . '" data-toggle="tooltip" title="Violation level" data-original-title="Violation level">' . htmlspecialchars(strip_tags(translate("Violation level"))) . '</th>';
			} else if ($key == "log_content_hash") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_content_hash . '" data-toggle="tooltip" title="Content hash" data-original-title="Content hash">' . htmlspecialchars(strip_tags(translate("Content hash"))) . '</th>';
			} else if ($key == "log_link") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_link . '" data-toggle="tooltip" title="Link" data-original-title="Link">' . htmlspecialchars(strip_tags(translate("Link"))) . '</th>';
			} else if ($key == "log_type") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_type . '" data-toggle="tooltip" title="Type" data-original-title="Type">' . htmlspecialchars(strip_tags(translate("Type"))) . '</th>';
			} else if ($key == "log_ip") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_ip . '" data-toggle="tooltip" title="IP address" data-original-title="IP address">' . htmlspecialchars(strip_tags(translate("IP address"))) . '</th>';
			} else if ($key == "log_user_agent") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . log_user_agent . '" data-toggle="tooltip" title="User agent" data-original-title="User agent">' . htmlspecialchars(strip_tags(translate("User agent"))) . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "log_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["log_id"].'" data-target="'.$data_table_row["log_id"].'">';

		$html .= inform_log_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_log_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "log_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "log_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "log_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "log_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "log_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "log_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_log_data_table_all($table_module_field, "", "", "", "", $writers_filters, $extended_command);
	} else {
		$count_all_true = count_log_data_table_all($table_module_field, "", "", "", "", "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_log_data_table_all($search, $table_module_field, "", "", "", "", $writers_filters, $extended_command);
		} else {
			$count_all = count_search_log_data_table_all($search, $table_module_field, "", "", "", "", "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_log_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_log_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_log_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $writers_filters, $extended_command);
			} else {
				$data_table = get_log_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_log_data_table_all($table_module_field, "", "", "", "", $writers_filters, $extended_command);
		} else {
			$data_table = get_log_data_table_all($table_module_field, "", "", "", "", "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_log_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_log_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["log_id"].'" data-target="'.$data_table["log_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_log_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_log_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_log_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3) {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4) {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5) {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "log_actions") {
				if ($key == "log_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "log_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_name"]));
				} else if ($key == "log_function") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_function"]));
				} else if ($key == "log_violation") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_violation"]));
				} else if ($key == "log_content_hash") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_content_hash"]));
				} else if ($key == "log_link") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_link"]));
				} else if ($key == "log_type") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_type"]));
				} else if ($key == "log_ip") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_ip"]));
				} else if ($key == "log_user_agent") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["log_user_agent"]));
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="users.php?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_log_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["log_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "log_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}

				$html .= '
				<td id="datatable-actions-'.$data_table["log_id"].'" class="text-center">
					<div class="btn-group">';

				$html .= '
						<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["log_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
						<i class="fa fa-eye"></i>
						</button>';
				
				if ($_SESSION["users_level"] != "5") {

			
				}

				$html .= '
					</div>
				</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_log_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_log_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Log name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Function")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module key")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Violation level")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Content hash")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Link")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "IP address")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "User agent")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module name")));
			} else {
				$column_name = str_replace("log", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "log_actions") {
				if ($key == "log_id") {
					if (!empty($data_table["log_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_name") {
					if ($data_table["log_name"] != "") {
						$table_row_value = '<strong>' . translate("Log name") . '</strong><br />' . $data_table["log_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_function") {
					if ($data_table["log_function"] != "") {
						$table_row_value = '<strong>' . translate("Function") . '</strong><br />' . $data_table["log_function"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_violation") {
					if ($data_table["log_violation"] != "") {
						$table_row_value = '<strong>' . translate("Violation level") . '</strong><br />' . $data_table["log_violation"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_content_hash") {
					if ($data_table["log_content_hash"] != "") {
						$table_row_value = '<strong>' . translate("Content hash") . '</strong><br />' . $data_table["log_content_hash"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_link") {
					if ($data_table["log_link"] != "") {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br />' . $data_table["log_link"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_type") {
					if ($data_table["log_type"] != "") {
						$table_row_value = '<strong>' . translate("Type") . '</strong><br />' . $data_table["log_type"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_ip") {
					if ($data_table["log_ip"] != "") {
						$table_row_value = '<strong>' . translate("IP address") . '</strong><br />' . $data_table["log_ip"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_user_agent") {
					if ($data_table["log_user_agent"] != "") {
						$bname = 'Unknown';
						$platform = 'Unknown';
						$version= "";
						
						$u_agent = "";
						$u_agent = $data_table[$key];
						
						//First get the platform?
						if (preg_match('/linux/i', $u_agent)) {
							$platform = 'Linux';
						}
						elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
							$platform = 'Mac';
						}
						elseif (preg_match('/windows|win32/i', $u_agent)) {
							$platform = 'Windows';
						}
					
						// Next get the name of the useragent yes seperately and for good reason
						if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
						{ 
							$bname = 'Internet Explorer'; 
							$ub = "MSIE"; 
						} 
						elseif(preg_match('/Firefox/i',$u_agent)) 
						{ 
							$bname = 'Mozilla Firefox'; 
							$ub = "Firefox"; 
						} 
						elseif(preg_match('/Edge/i',$u_agent)) 
						{ 
							$bname = 'Microsoft Edge'; 
							$ub = "Edge"; 
						} 
						elseif(preg_match('/Chrome/i',$u_agent)) 
						{ 
							$bname = 'Google Chrome'; 
							$ub = "Chrome"; 
						} 
						elseif(preg_match('/Safari/i',$u_agent)) 
						{ 
							$bname = 'Apple Safari'; 
							$ub = "Safari"; 
						} 
						elseif(preg_match('/Opera/i',$u_agent)) 
						{ 
							$bname = 'Opera'; 
							$ub = "Opera"; 
						} 
						elseif(preg_match('/Netscape/i',$u_agent)) 
						{ 
							$bname = 'Netscape'; 
							$ub = "Netscape"; 
						} 
					
						// finally get the correct version number
						$known = array('Version', $ub, 'other');
						$pattern = '#(?<browser>' . join('|', $known) .
						')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
						if (!preg_match_all($pattern, $u_agent, $matches)) {
							// we have no matching number just continue
						}
					
						// see how many we have
						$i = count($matches['browser']);
						if ($i != 1) {
							//we will have two since we are not using 'other' argument yet
							//see if version is before or after the name
							if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
								$version= $matches['version'][0];
							}
							else {
								$version= $matches['version'][1];
							}
						}
						else {
							$version= $matches['version'][0];
						}
						
						if(preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$data_table[$key])||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($data_table[$key],0,4))) {
							$device = "Mobile";
						} else {
							$device = "Desktop";
						}
					
						// check if we have a number
						if ($version==null || $version=="") {$version="?";}
						
						$table_row_value = '<strong>' . translate("User Agent") . '</strong><br />'.$bname.' (Version '.$version.'), '.ucwords($platform).', '.$device.'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "log_date_created") {
					if (!empty($data_table["log_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["log_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="users.php?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_log_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_log_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_log_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_log_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_log_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_log_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "log" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '11'
	AND (`log_function` = 'create_log_data' OR `log_function` = 'update_log_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_log_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "log" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '11'
	AND (`log_function` = 'create_log_data' OR `log_function` = 'update_log_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_log_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "log" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["log_content_hash"]) && !empty($query_log_content_hash["log_content_hash"])) {
			$query_log_content_hash["log_content_hash"] = str_replace("<br />", "\n", $query_log_content_hash["log_content_hash"]);
		}
		if (isset($query_log_content_hash["log_user_agent"]) && !empty($query_log_content_hash["log_user_agent"])) {
			$query_log_content_hash["log_user_agent"] = str_replace("<br />", "\n", $query_log_content_hash["log_user_agent"]);
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_log_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "log" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "log";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>log</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}
?>