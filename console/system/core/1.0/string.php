<?php mb_internal_encoding('UTF-8'); 
function str_replace_first($from, $to, $subject) {
	
	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $call_function;
	
	$con = start();
	
	/* initialize: clean strings */
	$from = escape_string($con, $from);
	$to = escape_string($con, $to);
	$subject = escape_string($con, $subject);
	
    $from = '/'.preg_quote($from, '/').'/';
	
	$response["status"] = true;
	$response["values"] = preg_replace($from, $to, $subject, 1);
	
	if (isset($call_function) && $call_function == "str_replace_first") {
		echo json_encode($response);
		unset($response);
		$response = array();
		unset($call_function);
		$call_function = array();
	} else {
		return $response["values"];
		unset($response);
		$response = array();
		unset($call_function);
		$call_function = array();
	}
	
}

function test_integer_range($min,$max,$int){
	
	if(isset($int)){
		if ( ($int>=$min && $int<=$max) AND ctype_digit($int) ) return true;
		else return false;
	}
	else return false;
	}


function reduce_size($string,$size){
$data=trim(substr(strip_tags(stripslashes($string)),0,$size));
return $data;
}


function trimUltimePlus($chaine){
$chaine = trim($chaine);
$chaine = str_replace("\t", " ", $chaine);
$chaine = str_replace("\r", " ", $chaine);
$chaine = str_replace("\n", " ", $chaine);
$chaine = str_replace("-", " ", $chaine);
$chaine = str_replace(",", " ", $chaine);
$chaine = preg_replace("( +)", " ", $chaine);
$chaine = trim($chaine);
$chaine = preg_replace("(-+)", " ", $chaine);
$chaine = str_replace(" ","+", $chaine);
return $chaine;}


function trimUltimeTiret($chaine){
$chaine = trim($chaine);
$chaine = str_replace("\t", " ", $chaine);
$chaine = str_replace("\r", " ", $chaine);
$chaine = str_replace("\n", " ", $chaine);
$chaine = str_replace("-", " ", $chaine);
$chaine = str_replace(",", " ", $chaine);
$chaine = str_replace("%", " ", $chaine);
$chaine = preg_replace("( +)", " ", $chaine);
$chaine = trim($chaine);
$chaine = preg_replace("(-+)", " ", $chaine);
$chaine = str_replace(" ","-", $chaine);
return $chaine;}


//get the ip of the user
function get_IP() {
    if(isset($_SERVER)) {
        if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif(isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    } else {
        $ip = 'WW.XX.YY.ZZ';
    }
    return $ip;
}

function utf8_to_tis620($string) {
    $str = $string;
    $res = "";
	if (function_exists("mb_strlen")) {
		for ($i = 0; $i < mb_strlen($str); $i++) {
			if (ord($str[$i]) == 224) {
				$unicode = ord($str[$i + 2]) & 0x3F;
				$unicode |= (ord($str[$i + 1]) & 0x3F) << 6;
				$unicode |= (ord($str[$i]) & 0x0F) << 12;
				$res .= chr($unicode - 0x0E00 + 0xA0);
				$i += 2;
			} else {
				$res .= $str[$i];
			}
		}
	} else {
		for ($i = 0; $i < strlen($str); $i++) {
			if (ord($str[$i]) == 224) {
				$unicode = ord($str[$i + 2]) & 0x3F;
				$unicode |= (ord($str[$i + 1]) & 0x3F) << 6;
				$unicode |= (ord($str[$i]) & 0x0F) << 12;
				$res .= chr($unicode - 0x0E00 + 0xA0);
				$i += 2;
			} else {
				$res .= $str[$i];
			}
		}
	}
    return $res;
}

function substr_utf8($str, $start_p, $len_p) {
    
    $str_post = "";
	if (function_exists("mb_strlen")) {
		if (mb_strlen($str) > $len_p) {
			$str_post = "...";
		}
	} else {
		if (strlen(utf8_to_tis620($str)) > $len_p) {
			$str_post = "...";
		}
	}
    
    return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,' . $start_p . '}' . '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,' . $len_p . '}).*#s', '$1', $str) . $str_post;
}
?>