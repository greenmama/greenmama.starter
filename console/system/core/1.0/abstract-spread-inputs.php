<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "get_abstract_spreadinputs_data_by_id" 
		|| $method == "get_abstract_spreadinputs_data_all" 
		|| $method == "count_abstract_spreadinputs_data_all" 
		|| $method == "get_abstract_spreadinputs_data_all_excluding" 
		|| $method == "count_abstract_spreadinputs_data_all_excluding" 
		|| $method == "get_abstract_spreadinputs_data_table_all" 
		|| $method == "count_abstract_spreadinputs_data_table_all" 
		|| $method == "get_search_abstract_spreadinputs_data_table_all" 
		|| $method == "count_search_abstract_spreadinputs_data_table_all" 
		|| $method == "create_abstract_spreadinputs_data" 
		|| $method == "update_abstract_spreadinputs_data" 
		|| $method == "patch_abstract_spreadinputs_data" 
		|| $method == "delete_abstract_spreadinputs_data" 
		|| $method == "create_abstract_spreadinputs_table" 
		|| $method == "create_abstract_spreadinputs_table_row" 
		|| $method == "update_abstract_spreadinputs_table_row" 
		|| $method == "delete_abstract_spreadinputs_table_row" 
		|| $method == "create_abstract_spreadinputs_table_row" 
		|| $method == "update_abstract_spreadinputs_table_row" 
		|| $method == "inform_abstract_spreadinputs_table_row" 
		|| $method == "view_abstract_spreadinputs_table_row" 
		|| $method == "create_sort_abstract_spreadinputs" 
		|| $method == "sort_abstract_spreadinputs" 
		|| $method == "get_abstract_spreadinputs_edit_log_data" 
		|| $method == "count_abstract_spreadinputs_edit_log_data" 
		|| $method == "get_abstract_spreadinputs_edit_log_data_by_id" 
		|| $method == "count_abstract_spreadinputs_edit_log_data_by_id"
		
		|| $method == "get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references" 
		|| $method == "count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references" 
		|| $method == "get_abstract_spreadinputs_references_data_dynamic_list" 
		|| $method == "count_abstract_spreadinputs_references_data_dynamic_list" 
		|| $method == "get_abstract_spreadinputs_references_data_dynamic_list_by_id"
		
		|| $method == "get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module" 
		|| $method == "count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module" 
		|| $method == "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list" 
		|| $method == "count_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list" 
		|| $method == "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list_by_id"
		
		|| $method == "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list" 
		|| $method == "count_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list"
		 
		|| $method == "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list" 
		|| $method == "count_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list")
) {
	
	/* include: configurations */
	require_once("../../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");
	
	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "get_abstract_spreadinputs_data_by_id"){
		get_abstract_spreadinputs_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_abstract_spreadinputs_data_all"){
		get_abstract_spreadinputs_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_data_all"){
		count_abstract_spreadinputs_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_data_all_excluding"){
		get_abstract_spreadinputs_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_data_all_excluding"){
		count_abstract_spreadinputs_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_data_table_all"){
		get_abstract_spreadinputs_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_data_table_all"){
		count_abstract_spreadinputs_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_abstract_spreadinputs_data_table_all"){
		get_search_abstract_spreadinputs_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_abstract_spreadinputs_data_table_all"){
		count_search_abstract_spreadinputs_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_abstract_spreadinputs_data"){
		create_abstract_spreadinputs_data($parameters);
	} else if ($method == "update_abstract_spreadinputs_data"){
		update_abstract_spreadinputs_data($parameters);
	} else if ($method == "patch_abstract_spreadinputs_data"){
		patch_abstract_spreadinputs_data($parameters);
	} else if ($method == "delete_abstract_spreadinputs_data"){
		delete_abstract_spreadinputs_data($parameters);
	} else if ($method == "create_abstract_spreadinputs_table"){
		create_abstract_spreadinputs_table($parameters, $table_module_field); 
	} else if ($method == "create_abstract_spreadinputs_table_row"){
		create_abstract_spreadinputs_table_row($parameters, $table_module_field);
	} else if ($method == "update_abstract_spreadinputs_table_row"){
		update_abstract_spreadinputs_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_abstract_spreadinputs_table_row"){
		inform_abstract_spreadinputs_table_row($parameters, $table_module_field);
	} else if ($method == "view_abstract_spreadinputs_table_row"){
		view_abstract_spreadinputs_table_row($parameters, $table_module_field);
	} else if ($method == "create_sort_abstract_spreadinputs"){
		create_sort_abstract_spreadinputs($table_module_field);
	} else if ($method == "sort_abstract_spreadinputs"){
		sort_abstract_spreadinputs($parameters);
	} else if ($method == "get_abstract_spreadinputs_edit_log_data"){
		get_abstract_spreadinputs_edit_log_data($parameters);
	} else if ($method == "count_abstract_spreadinputs_edit_log_data"){
		count_abstract_spreadinputs_edit_log_data($parameters);
	} else if ($method == "get_abstract_spreadinputs_edit_log_data_by_id"){
		get_abstract_spreadinputs_edit_log_data_by_id($parameters);
	} else if ($method == "count_abstract_spreadinputs_edit_log_data_by_id"){
		count_abstract_spreadinputs_edit_log_data_by_id($parameters);
	} else if ($method == "get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references"){
		get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references"){
		count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_references_data_dynamic_list"){
		get_abstract_spreadinputs_references_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_references_data_dynamic_list"){
		count_abstract_spreadinputs_references_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_references_data_dynamic_list_by_id"){
		get_abstract_spreadinputs_references_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module"){
		get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module"){
		count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list"){
		get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list"){
		count_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list_by_id"){
		get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list"){
		get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list"){
		count_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list"){
		get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list"){
		count_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	}
}

function get_abstract_spreadinputs_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "abstract_spreadinputs" */
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs`.`abstract_spreadinputs_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));
			$query["abstract_spreadinputs_date_created_formatted"] = datetime_reformat($query["abstract_spreadinputs_date_created"]);
			
		if ($query["abstract_spreadinputs_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_min"] == "" || $query["abstract_spreadinputs_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_max"] == "" || $query["abstract_spreadinputs_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_min"] == "" || $query["abstract_spreadinputs_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_max"] == "" || $query["abstract_spreadinputs_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_default_value"]) && !empty($query["abstract_spreadinputs_default_value"])) {
		$query["abstract_spreadinputs_default_value"] = unserialize($query["abstract_spreadinputs_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_default_value"][$j]) && is_array($query["abstract_spreadinputs_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_default_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_default_value"][$j])) {
					$query["abstract_spreadinputs_default_value"][$j] = stripslashes($query["abstract_spreadinputs_default_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_default_value_json"] = json_encode($query["abstract_spreadinputs_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_input_list_static_value"]) && !empty($query["abstract_spreadinputs_input_list_static_value"])) {
		$query["abstract_spreadinputs_input_list_static_value"] = unserialize($query["abstract_spreadinputs_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_input_list_static_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j])) {
					$query["abstract_spreadinputs_input_list_static_value"][$j] = stripslashes($query["abstract_spreadinputs_input_list_static_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_image_width"] == "0") {
			$query["abstract_spreadinputs_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_image_height"] == "0") {
			$query["abstract_spreadinputs_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_image_height_ratio"] = "";
		}
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_spreadinputs_id",
					"modules_record_target" => $target,
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_data_all($start = 0, $limit = "", $sort_by = "abstract_spreadinputs_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "abstract_spreadinputs" */
	$sql = "
	SELECT *
	FROM   `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_min"] == "" || $query["abstract_spreadinputs_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_max"] == "" || $query["abstract_spreadinputs_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_min"] == "" || $query["abstract_spreadinputs_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_max"] == "" || $query["abstract_spreadinputs_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_default_value"]) && !empty($query["abstract_spreadinputs_default_value"])) {
		$query["abstract_spreadinputs_default_value"] = unserialize($query["abstract_spreadinputs_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_default_value"][$j]) && is_array($query["abstract_spreadinputs_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_default_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_default_value"][$j])) {
					$query["abstract_spreadinputs_default_value"][$j] = stripslashes($query["abstract_spreadinputs_default_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_default_value_json"] = json_encode($query["abstract_spreadinputs_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_input_list_static_value"]) && !empty($query["abstract_spreadinputs_input_list_static_value"])) {
		$query["abstract_spreadinputs_input_list_static_value"] = unserialize($query["abstract_spreadinputs_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_input_list_static_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j])) {
					$query["abstract_spreadinputs_input_list_static_value"][$j] = stripslashes($query["abstract_spreadinputs_input_list_static_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_image_width"] == "0") {
			$query["abstract_spreadinputs_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_image_height"] == "0") {
			$query["abstract_spreadinputs_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_image_height_ratio"] = "";
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "abstract_spreadinputs" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "abstract_spreadinputs_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "abstract_spreadinputs" */
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_min"] == "" || $query["abstract_spreadinputs_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_max"] == "" || $query["abstract_spreadinputs_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_min"] == "" || $query["abstract_spreadinputs_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_max"] == "" || $query["abstract_spreadinputs_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_default_value"]) && !empty($query["abstract_spreadinputs_default_value"])) {
		$query["abstract_spreadinputs_default_value"] = unserialize($query["abstract_spreadinputs_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_default_value"][$j]) && is_array($query["abstract_spreadinputs_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_default_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_default_value"][$j])) {
					$query["abstract_spreadinputs_default_value"][$j] = stripslashes($query["abstract_spreadinputs_default_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_default_value_json"] = json_encode($query["abstract_spreadinputs_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_input_list_static_value"]) && !empty($query["abstract_spreadinputs_input_list_static_value"])) {
		$query["abstract_spreadinputs_input_list_static_value"] = unserialize($query["abstract_spreadinputs_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_input_list_static_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j])) {
					$query["abstract_spreadinputs_input_list_static_value"][$j] = stripslashes($query["abstract_spreadinputs_input_list_static_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_image_width"] == "0") {
			$query["abstract_spreadinputs_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_image_height"] == "0") {
			$query["abstract_spreadinputs_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_image_height_ratio"] = "";
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_spreadinputs_id",
					"modules_record_target" => $target,
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "abstract_spreadinputs" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "abstract_spreadinputs_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "abstract_spreadinputs" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_min"] == "" || $query["abstract_spreadinputs_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_max"] == "" || $query["abstract_spreadinputs_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_min"] == "" || $query["abstract_spreadinputs_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_max"] == "" || $query["abstract_spreadinputs_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_default_value"]) && !empty($query["abstract_spreadinputs_default_value"])) {
		$query["abstract_spreadinputs_default_value"] = unserialize($query["abstract_spreadinputs_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_default_value"][$j]) && is_array($query["abstract_spreadinputs_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_default_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_default_value"][$j])) {
					$query["abstract_spreadinputs_default_value"][$j] = stripslashes($query["abstract_spreadinputs_default_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_default_value_json"] = json_encode($query["abstract_spreadinputs_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_input_list_static_value"]) && !empty($query["abstract_spreadinputs_input_list_static_value"])) {
		$query["abstract_spreadinputs_input_list_static_value"] = unserialize($query["abstract_spreadinputs_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_input_list_static_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j])) {
					$query["abstract_spreadinputs_input_list_static_value"][$j] = stripslashes($query["abstract_spreadinputs_input_list_static_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_image_width"] == "0") {
			$query["abstract_spreadinputs_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_image_height"] == "0") {
			$query["abstract_spreadinputs_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_image_height_ratio"] = "";
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "abstract_spreadinputs" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_abstract_spreadinputs_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "abstract_spreadinputs_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'abstract_spreadinputs'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "abstract_spreadinputs_actions" 
				&& $key != "abstract_spreadinputs_date_created" 
				&& $key != "abstract_spreadinputs_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "abstract_spreadinputs" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_min"] == "" || $query["abstract_spreadinputs_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_validate_date_max"] == "" || $query["abstract_spreadinputs_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_min"] == "" || $query["abstract_spreadinputs_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_validate_datetime_max"] == "" || $query["abstract_spreadinputs_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_default_value"]) && !empty($query["abstract_spreadinputs_default_value"])) {
		$query["abstract_spreadinputs_default_value"] = unserialize($query["abstract_spreadinputs_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_default_value"][$j]) && is_array($query["abstract_spreadinputs_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_default_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_default_value"][$j])) {
					$query["abstract_spreadinputs_default_value"][$j] = stripslashes($query["abstract_spreadinputs_default_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_default_value_json"] = json_encode($query["abstract_spreadinputs_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_input_list_static_value"]) && !empty($query["abstract_spreadinputs_input_list_static_value"])) {
		$query["abstract_spreadinputs_input_list_static_value"] = unserialize($query["abstract_spreadinputs_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_input_list_static_value"][$j][] = stripslashes($value);
				}
			} else {
				if (!empty($query["abstract_spreadinputs_input_list_static_value"][$j])) {
					$query["abstract_spreadinputs_input_list_static_value"][$j] = stripslashes($query["abstract_spreadinputs_input_list_static_value"][$j]);
				}
			}
		}
		$query["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_image_width"] == "0") {
			$query["abstract_spreadinputs_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_image_height"] == "0") {
			$query["abstract_spreadinputs_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_image_height_ratio"] = "";
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_abstract_spreadinputs_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_spreadinputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'abstract_spreadinputs'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "abstract_spreadinputs_actions" 
				&& $key != "abstract_spreadinputs_date_created" 
				&& $key != "abstract_spreadinputs_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "abstract_spreadinputs" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_spreadinputs_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_abstract_spreadinputs_data($parameters){

	/* get global: configurations */
	global $configs;
	$abstract_spreadinputs_configs = get_modules_data_by_id("3");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["abstract_spreadinputs_activate"] == "1") {
	
		if(!isset($parameters["abstract_spreadinputs_label"]) || empty($parameters["abstract_spreadinputs_label"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_label";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_label"]) > 50 && $parameters["abstract_spreadinputs_label"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_label";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_label"]) > 50 && $parameters["abstract_spreadinputs_label"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_label";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_varname"]) > 50 && $parameters["abstract_spreadinputs_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_varname"]) > 50 && $parameters["abstract_spreadinputs_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["abstract_spreadinputs_varname"]) && $parameters["abstract_spreadinputs_varname"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#abstract_spreadinputs_varname";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_spreadinputs_type"]) || empty($parameters["abstract_spreadinputs_type"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Type") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_type";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_spreadinputs_references"]) || empty($parameters["abstract_spreadinputs_references"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Reference") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_references";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_placeholder"]) > 100 && $parameters["abstract_spreadinputs_placeholder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Placeholder") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_placeholder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_placeholder"]) > 100 && $parameters["abstract_spreadinputs_placeholder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Placeholder") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_placeholder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_help"]) > 200 && $parameters["abstract_spreadinputs_help"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Help") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_help";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_help"]) > 200 && $parameters["abstract_spreadinputs_help"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Help") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_help";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_prefix"]) > 100 && $parameters["abstract_spreadinputs_prefix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Prefix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_prefix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_prefix"]) > 100 && $parameters["abstract_spreadinputs_prefix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Prefix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_prefix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_suffix"]) > 100 && $parameters["abstract_spreadinputs_suffix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Suffix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_suffix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_suffix"]) > 100 && $parameters["abstract_spreadinputs_suffix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Suffix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_suffix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_input_list_dynamic_id_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module ID Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_id_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_input_list_dynamic_id_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module ID Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_id_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_input_list_dynamic_value_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Value Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_value_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_input_list_dynamic_value_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Value Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_value_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_image_folder"]) > 200 && $parameters["abstract_spreadinputs_image_folder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Folder Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_image_folder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_image_folder"]) > 200 && $parameters["abstract_spreadinputs_image_folder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Folder Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_image_folder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_gridWidth"]) > 200 && $parameters["abstract_spreadinputs_gridWidth"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Grid Width for Input") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_gridWidth";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_gridWidth"]) > 200 && $parameters["abstract_spreadinputs_gridWidth"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Grid Width for Input") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_gridWidth";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}

	}

	/* initialize: default */
	$parameters["abstract_spreadinputs_default_value"] = trim(serialize(explode(",", $parameters["abstract_spreadinputs_default_value"])));
	$parameters["abstract_spreadinputs_input_list_static_value"] = trim(serialize(explode(",", $parameters["abstract_spreadinputs_input_list_static_value"])));
	date_default_timezone_set($configs["timezone"]);
	$parameters["abstract_spreadinputs_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */

	/* database: insert to module "abstract_spreadinputs" (begin) */
	$sql = "INSERT INTO `abstract_spreadinputs` (
				`abstract_spreadinputs_id`,
				`abstract_spreadinputs_label`,
				`abstract_spreadinputs_varname`,
				`abstract_spreadinputs_type`,
				`abstract_spreadinputs_references`,
				`abstract_spreadinputs_placeholder`,
				`abstract_spreadinputs_help`,
				`abstract_spreadinputs_require`,
				`abstract_spreadinputs_readonly`,
				`abstract_spreadinputs_disable`,
				`abstract_spreadinputs_hidden`,
				`abstract_spreadinputs_validate_string_min`,
				`abstract_spreadinputs_validate_string_max`,
				`abstract_spreadinputs_validate_number_min`,
				`abstract_spreadinputs_validate_number_max`,
				`abstract_spreadinputs_validate_date_min`,
				`abstract_spreadinputs_validate_date_max`,
				`abstract_spreadinputs_validate_datetime_min`,
				`abstract_spreadinputs_validate_datetime_max`,
				`abstract_spreadinputs_validate_password_equal_to`,
				`abstract_spreadinputs_validate_email`,
				`abstract_spreadinputs_validate_password`,
				`abstract_spreadinputs_validate_website`,
				`abstract_spreadinputs_validate_no_space`,
				`abstract_spreadinputs_validate_no_specialchar_soft`,
				`abstract_spreadinputs_validate_no_specialchar_hard`,
				`abstract_spreadinputs_validate_upper`,
				`abstract_spreadinputs_validate_lower`,
				`abstract_spreadinputs_validate_number`,
				`abstract_spreadinputs_validate_digit`,
				`abstract_spreadinputs_validate_unique`,
				`abstract_spreadinputs_prefix`,
				`abstract_spreadinputs_suffix`,
				`abstract_spreadinputs_default_value_type`,
				`abstract_spreadinputs_default_value`,
				`abstract_spreadinputs_default_switch`,
				`abstract_spreadinputs_input_list`,
				`abstract_spreadinputs_input_list_static_value`,
				`abstract_spreadinputs_input_list_dynamic_module`,
				`abstract_spreadinputs_input_list_dynamic_id_column`,
				`abstract_spreadinputs_input_list_dynamic_value_column`,
				`abstract_spreadinputs_file_selector_type`,
				`abstract_spreadinputs_date_format`,
				`abstract_spreadinputs_color_format`,
				`abstract_spreadinputs_tags_format`,
				`abstract_spreadinputs_image_folder`,
				`abstract_spreadinputs_image_width`,
				`abstract_spreadinputs_image_height`,
				`abstract_spreadinputs_image_width_ratio`,
				`abstract_spreadinputs_image_height_ratio`,
				`abstract_spreadinputs_gridWidth`,
				`abstract_spreadinputs_alignment`,
				`abstract_spreadinputs_order`,
				`abstract_spreadinputs_date_created`,
				`abstract_spreadinputs_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["abstract_spreadinputs_label"] . "',
				'" . $parameters["abstract_spreadinputs_varname"] . "',
				'" . $parameters["abstract_spreadinputs_type"] . "',
				'" . $parameters["abstract_spreadinputs_references"] . "',
				'" . $parameters["abstract_spreadinputs_placeholder"] . "',
				'" . $parameters["abstract_spreadinputs_help"] . "',
				'" . $parameters["abstract_spreadinputs_require"] . "',
				'" . $parameters["abstract_spreadinputs_readonly"] . "',
				'" . $parameters["abstract_spreadinputs_disable"] . "',
				'" . $parameters["abstract_spreadinputs_hidden"] . "',
				'" . $parameters["abstract_spreadinputs_validate_string_min"] . "',
				'" . $parameters["abstract_spreadinputs_validate_string_max"] . "',
				'" . $parameters["abstract_spreadinputs_validate_number_min"] . "',
				'" . $parameters["abstract_spreadinputs_validate_number_max"] . "',
				'" . $parameters["abstract_spreadinputs_validate_date_min"] . "',
				'" . $parameters["abstract_spreadinputs_validate_date_max"] . "',
				'" . datetime_revert($parameters["abstract_spreadinputs_validate_datetime_min"], $configs["timezone_difference"]) . "',
				'" . datetime_revert($parameters["abstract_spreadinputs_validate_datetime_max"], $configs["timezone_difference"]) . "',
				'" . $parameters["abstract_spreadinputs_validate_password_equal_to"] . "',
				'" . $parameters["abstract_spreadinputs_validate_email"] . "',
				'" . $parameters["abstract_spreadinputs_validate_password"] . "',
				'" . $parameters["abstract_spreadinputs_validate_website"] . "',
				'" . $parameters["abstract_spreadinputs_validate_no_space"] . "',
				'" . $parameters["abstract_spreadinputs_validate_no_specialchar_soft"] . "',
				'" . $parameters["abstract_spreadinputs_validate_no_specialchar_hard"] . "',
				'" . $parameters["abstract_spreadinputs_validate_upper"] . "',
				'" . $parameters["abstract_spreadinputs_validate_lower"] . "',
				'" . $parameters["abstract_spreadinputs_validate_number"] . "',
				'" . $parameters["abstract_spreadinputs_validate_digit"] . "',
				'" . $parameters["abstract_spreadinputs_validate_unique"] . "',
				'" . $parameters["abstract_spreadinputs_prefix"] . "',
				'" . $parameters["abstract_spreadinputs_suffix"] . "',
				'" . $parameters["abstract_spreadinputs_default_value_type"] . "',
				'" . $parameters["abstract_spreadinputs_default_value"] . "',
				'" . $parameters["abstract_spreadinputs_default_switch"] . "',
				'" . $parameters["abstract_spreadinputs_input_list"] . "',
				'" . $parameters["abstract_spreadinputs_input_list_static_value"] . "',
				'" . $parameters["abstract_spreadinputs_input_list_dynamic_module"] . "',
				'" . $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] . "',
				'" . $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] . "',
				'" . $parameters["abstract_spreadinputs_file_selector_type"] . "',
				'" . $parameters["abstract_spreadinputs_date_format"] . "',
				'" . $parameters["abstract_spreadinputs_color_format"] . "',
				'" . $parameters["abstract_spreadinputs_tags_format"] . "',
				'" . $parameters["abstract_spreadinputs_image_folder"] . "',
				'" . $parameters["abstract_spreadinputs_image_width"] . "',
				'" . $parameters["abstract_spreadinputs_image_height"] . "',
				'" . $parameters["abstract_spreadinputs_image_width_ratio"] . "',
				'" . $parameters["abstract_spreadinputs_image_height_ratio"] . "',
				'" . $parameters["abstract_spreadinputs_gridWidth"] . "',
				'" . $parameters["abstract_spreadinputs_alignment"] . "',
				'" . $parameters["abstract_spreadinputs_order"] . "',
				'" . $parameters["abstract_spreadinputs_date_created"] . "',
				'" . $parameters["abstract_spreadinputs_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "')";
	$result = mysqli_query($con, $sql);
	$parameters["abstract_spreadinputs_id"] = mysqli_insert_id($con);
	/* database: insert to module "abstract_spreadinputs" (end) */

	if ($result) {
		

		/* response: additional data */
		

		/* log (begin) */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_spreadinputs_id",
				"modules_record_target" => $parameters["abstract_spreadinputs_id"],
				"modules_id" => "3",
				"modules_name" => "Abstract Spread Inputs",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_abstract_spreadinputs_data($parameters){

	/* get global: configurations */
	global $configs;
	$abstract_spreadinputs_configs = get_modules_data_by_id("3");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["abstract_spreadinputs_activate"] == "1") {
	
		if(!isset($parameters["abstract_spreadinputs_label"]) || empty($parameters["abstract_spreadinputs_label"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_label";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_label"]) > 50 && $parameters["abstract_spreadinputs_label"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_label";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_label"]) > 50 && $parameters["abstract_spreadinputs_label"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Label") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_label";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_varname"]) > 50 && $parameters["abstract_spreadinputs_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_varname"]) > 50 && $parameters["abstract_spreadinputs_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 50 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["abstract_spreadinputs_varname"]) && $parameters["abstract_spreadinputs_varname"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Variable Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#abstract_spreadinputs_varname";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_spreadinputs_type"]) || empty($parameters["abstract_spreadinputs_type"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Spread Input Type") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_type";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_spreadinputs_references"]) || empty($parameters["abstract_spreadinputs_references"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Reference") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_spreadinputs_references";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_placeholder"]) > 100 && $parameters["abstract_spreadinputs_placeholder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Placeholder") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_placeholder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_placeholder"]) > 100 && $parameters["abstract_spreadinputs_placeholder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Placeholder") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_placeholder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_help"]) > 200 && $parameters["abstract_spreadinputs_help"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Help") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_help";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_help"]) > 200 && $parameters["abstract_spreadinputs_help"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Spread Input Help") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_help";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_prefix"]) > 100 && $parameters["abstract_spreadinputs_prefix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Prefix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_prefix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_prefix"]) > 100 && $parameters["abstract_spreadinputs_prefix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Prefix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_prefix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_suffix"]) > 100 && $parameters["abstract_spreadinputs_suffix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Suffix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_suffix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_suffix"]) > 100 && $parameters["abstract_spreadinputs_suffix"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Suffix") . "</strong> " . translate("is longer than maximum length of strings at") . " 100 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_suffix";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_input_list_dynamic_id_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module ID Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_id_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_input_list_dynamic_id_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module ID Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_id_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_input_list_dynamic_value_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Value Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_value_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_input_list_dynamic_value_column"]) > 200 && $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Value Column Name for Dynamic List") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_input_list_dynamic_value_column";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_image_folder"]) > 200 && $parameters["abstract_spreadinputs_image_folder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Folder Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_image_folder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_image_folder"]) > 200 && $parameters["abstract_spreadinputs_image_folder"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Image Folder Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_image_folder";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_spreadinputs_gridWidth"]) > 200 && $parameters["abstract_spreadinputs_gridWidth"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Grid Width for Input") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)");
				$response["target"] = "#abstract_spreadinputs_gridWidth";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_spreadinputs_gridWidth"]) > 200 && $parameters["abstract_spreadinputs_gridWidth"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Grid Width for Input") . "</strong> " . translate("is longer than maximum length of strings at") . " 200 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_spreadinputs_gridWidth";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
	}

	/* database: get old data from module "abstract_spreadinputs" (begin) */
	$sql = "SELECT *
	        FROM   `abstract_spreadinputs`
			WHERE  `abstract_spreadinputs_id` = '" . $parameters["abstract_spreadinputs_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_spreadinputs_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "abstract_spreadinputs" (end) */
	
	/* initialize: default */
	$parameters["abstract_spreadinputs_default_value"] = trim(serialize(explode(",", $parameters["abstract_spreadinputs_default_value"])));
	$parameters["abstract_spreadinputs_input_list_static_value"] = trim(serialize(explode(",", $parameters["abstract_spreadinputs_input_list_static_value"])));
	$parameters["abstract_spreadinputs_date_created"] = $query["abstract_spreadinputs_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	/* database: update to module "abstract_spreadinputs" (begin) */
	$sql = "UPDATE `abstract_spreadinputs`
			SET 
				   `abstract_spreadinputs_label` = '" . $parameters["abstract_spreadinputs_label"] . "',
				   `abstract_spreadinputs_varname` = '" . $parameters["abstract_spreadinputs_varname"] . "',
				   `abstract_spreadinputs_type` = '" . $parameters["abstract_spreadinputs_type"] . "',
				   `abstract_spreadinputs_references` = '" . $parameters["abstract_spreadinputs_references"] . "',
				   `abstract_spreadinputs_placeholder` = '" . $parameters["abstract_spreadinputs_placeholder"] . "',
				   `abstract_spreadinputs_help` = '" . $parameters["abstract_spreadinputs_help"] . "',
				   `abstract_spreadinputs_require` = '" . $parameters["abstract_spreadinputs_require"] . "',
				   `abstract_spreadinputs_readonly` = '" . $parameters["abstract_spreadinputs_readonly"] . "',
				   `abstract_spreadinputs_disable` = '" . $parameters["abstract_spreadinputs_disable"] . "',
				   `abstract_spreadinputs_hidden` = '" . $parameters["abstract_spreadinputs_hidden"] . "',
				   `abstract_spreadinputs_validate_string_min` = '" . $parameters["abstract_spreadinputs_validate_string_min"] . "',
				   `abstract_spreadinputs_validate_string_max` = '" . $parameters["abstract_spreadinputs_validate_string_max"] . "',
				   `abstract_spreadinputs_validate_number_min` = '" . $parameters["abstract_spreadinputs_validate_number_min"] . "',
				   `abstract_spreadinputs_validate_number_max` = '" . $parameters["abstract_spreadinputs_validate_number_max"] . "',
				   `abstract_spreadinputs_validate_date_min` = '" . $parameters["abstract_spreadinputs_validate_date_min"] . "',
				   `abstract_spreadinputs_validate_date_max` = '" . $parameters["abstract_spreadinputs_validate_date_max"] . "',
				   `abstract_spreadinputs_validate_datetime_min` = '".datetime_revert($parameters["abstract_spreadinputs_validate_datetime_min"], $configs["timezone_difference"])."',
				   `abstract_spreadinputs_validate_datetime_max` = '".datetime_revert($parameters["abstract_spreadinputs_validate_datetime_max"], $configs["timezone_difference"])."',
				   `abstract_spreadinputs_validate_password_equal_to` = '" . $parameters["abstract_spreadinputs_validate_password_equal_to"] . "',
				   `abstract_spreadinputs_validate_email` = '" . $parameters["abstract_spreadinputs_validate_email"] . "',
				   `abstract_spreadinputs_validate_password` = '" . $parameters["abstract_spreadinputs_validate_password"] . "',
				   `abstract_spreadinputs_validate_website` = '" . $parameters["abstract_spreadinputs_validate_website"] . "',
				   `abstract_spreadinputs_validate_no_space` = '" . $parameters["abstract_spreadinputs_validate_no_space"] . "',
				   `abstract_spreadinputs_validate_no_specialchar_soft` = '" . $parameters["abstract_spreadinputs_validate_no_specialchar_soft"] . "',
				   `abstract_spreadinputs_validate_no_specialchar_hard` = '" . $parameters["abstract_spreadinputs_validate_no_specialchar_hard"] . "',
				   `abstract_spreadinputs_validate_upper` = '" . $parameters["abstract_spreadinputs_validate_upper"] . "',
				   `abstract_spreadinputs_validate_lower` = '" . $parameters["abstract_spreadinputs_validate_lower"] . "',
				   `abstract_spreadinputs_validate_number` = '" . $parameters["abstract_spreadinputs_validate_number"] . "',
				   `abstract_spreadinputs_validate_digit` = '" . $parameters["abstract_spreadinputs_validate_digit"] . "',
				   `abstract_spreadinputs_validate_unique` = '" . $parameters["abstract_spreadinputs_validate_unique"] . "',
				   `abstract_spreadinputs_prefix` = '" . $parameters["abstract_spreadinputs_prefix"] . "',
				   `abstract_spreadinputs_suffix` = '" . $parameters["abstract_spreadinputs_suffix"] . "',
				   `abstract_spreadinputs_default_value_type` = '" . $parameters["abstract_spreadinputs_default_value_type"] . "',
				   `abstract_spreadinputs_default_value` = '" . $parameters["abstract_spreadinputs_default_value"] . "',
				   `abstract_spreadinputs_default_switch` = '" . $parameters["abstract_spreadinputs_default_switch"] . "',
				   `abstract_spreadinputs_input_list` = '" . $parameters["abstract_spreadinputs_input_list"] . "',
				   `abstract_spreadinputs_input_list_static_value` = '" . $parameters["abstract_spreadinputs_input_list_static_value"] . "',
				   `abstract_spreadinputs_input_list_dynamic_module` = '" . $parameters["abstract_spreadinputs_input_list_dynamic_module"] . "',
				   `abstract_spreadinputs_input_list_dynamic_id_column` = '" . $parameters["abstract_spreadinputs_input_list_dynamic_id_column"] . "',
				   `abstract_spreadinputs_input_list_dynamic_value_column` = '" . $parameters["abstract_spreadinputs_input_list_dynamic_value_column"] . "',
				   `abstract_spreadinputs_file_selector_type` = '" . $parameters["abstract_spreadinputs_file_selector_type"] . "',
				   `abstract_spreadinputs_date_format` = '" . $parameters["abstract_spreadinputs_date_format"] . "',
				   `abstract_spreadinputs_color_format` = '" . $parameters["abstract_spreadinputs_color_format"] . "',
				   `abstract_spreadinputs_tags_format` = '" . $parameters["abstract_spreadinputs_tags_format"] . "',
				   `abstract_spreadinputs_image_folder` = '" . $parameters["abstract_spreadinputs_image_folder"] . "',
				   `abstract_spreadinputs_image_width` = '" . $parameters["abstract_spreadinputs_image_width"] . "',
				   `abstract_spreadinputs_image_height` = '" . $parameters["abstract_spreadinputs_image_height"] . "',
				   `abstract_spreadinputs_image_width_ratio` = '" . $parameters["abstract_spreadinputs_image_width_ratio"] . "',
				   `abstract_spreadinputs_image_height_ratio` = '" . $parameters["abstract_spreadinputs_image_height_ratio"] . "',
				   `abstract_spreadinputs_gridWidth` = '" . $parameters["abstract_spreadinputs_gridWidth"] . "',
				   `abstract_spreadinputs_alignment` = '" . $parameters["abstract_spreadinputs_alignment"] . "',
				   `abstract_spreadinputs_order` = '" . $parameters["abstract_spreadinputs_order"] . "',
				   `abstract_spreadinputs_date_created` = '" . $parameters["abstract_spreadinputs_date_created"] . "',
				   `abstract_spreadinputs_activate` = '" . $parameters["abstract_spreadinputs_activate"] . "'
			WHERE  `abstract_spreadinputs_id` = '" . $parameters["abstract_spreadinputs_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "abstract_spreadinputs" (end) */

	if ($result) {
		
		/* response: additional data */
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_spreadinputs_id",
				"modules_record_target" => $parameters["abstract_spreadinputs_id"],
				"modules_id" => "3",
				"modules_name" => "Abstract Spread Inputs",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "abstract_spreadinputs";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_abstract_spreadinputs_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "abstract_spreadinputs" (begin) */
	$sql = "SELECT *
	        FROM   `abstract_spreadinputs`
			WHERE  `abstract_spreadinputs_id` = '" . $parameters["abstract_spreadinputs_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_spreadinputs_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "abstract_spreadinputs" (end) */
	
	/* initialize: default */
	$parameters["abstract_spreadinputs_date_created"] = $query["abstract_spreadinputs_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "abstract_spreadinputs" (begin) */
	$sql = "UPDATE `abstract_spreadinputs`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `abstract_spreadinputs_id` = '" . $parameters["abstract_spreadinputs_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "abstract_spreadinputs" (end) */

	if ($result) {
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_spreadinputs_id",
				"modules_record_target" => $parameters["abstract_spreadinputs_id"],
				"modules_id" => "3",
				"modules_name" => "Abstract Spread Inputs",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "abstract_spreadinputs";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_abstract_spreadinputs_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_spreadinputs_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "abstract_spreadinputs" (begin) */
		$sql = "
		DELETE FROM `abstract_spreadinputs`
		WHERE `abstract_spreadinputs_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "abstract_spreadinputs" (end) */

		if ($result) {
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_spreadinputs_id",
					"modules_record_target" => $target,
					"modules_id" => "3",
					"modules_name" => "Abstract Spread Inputs",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_sort_abstract_spreadinputs($table_module_field) {
	
	/* get global: configurations */
	global $configs;
	$abstract_spreadinputs_configs = get_modules_data_by_id("3");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);

	
	$table_module_field_unassoc = array_keys($table_module_field);
	$table_module_field_key = "abstract_spreadinputs_title";
	for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
		if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
			$table_module_field_key = $table_module_field_unassoc[$j];
			break;
		}
	}

	$data_table = get_abstract_spreadinputs_data_all("", "", "abstract_spreadinputs_order", "asc");
	
	
	$html = '';

	for($i = 0; $i < count($data_table); $i++){
		if ($data_table[$i]["abstract_spreadinputs_id"] > 0) {

			$table_row_value = render_abstract_spreadinputs_table_data($data_table[$i][$table_module_field_key]).'<br />('.$data_table[$i][$table_module_field_key].')';

			$html .= '<li id="list_'.$data_table[$i]["abstract_spreadinputs_id"].'" class="mjs-nestedSortable" style="display: list-item;">
						<div class="sort-item">
							<span>
								<div style="display: inline-table">
									<i class="si si-cursor-move"></i>
								</div>
								<div style="display: inline-table">
									'.$table_row_value.' ('.translate("ID").': '.$data_table[$i]["abstract_spreadinputs_id"].')
								</div>
							</span>
						</div>
					  </li>';

		}

	}
	
	$response["status"] = true;
	$response["message"] = translate("Successfully created sort");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function sort_abstract_spreadinputs($parameters) {
	
	/* get global: configurations */
	global $configs;
	$abstract_spreadinputs_configs = get_modules_data_by_id("3");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	
	$target_next = 1;
	
	for ($i = 0; $i <= count($parameters); $i++) {
		$target_item_id = "";
		for($s = 0; $s <= count($parameters); $s++) {
			if ($parameters[$s]["left"] == $target_next) {
				$target_item_id = $parameters[$s]["item_id"];
				$target_depth = $parameters[$s]["depth"];
				if ($target_depth == 1) {
					$target_next = $parameters[$s]["right"] + 1;
				} else {
					$target_next = $target_next + 1;
				}
				break;
			}
		}
		
		if ($target_depth != "0" && $target_depth == 1 && !empty($target_item_id)) {
			
			/* database: update to module "abstract_spreadinputs" (begin) */
			$sql = "UPDATE `abstract_spreadinputs`
					SET    `abstract_spreadinputs_order` = '" . $i . "'
					WHERE  `abstract_spreadinputs_id` = '" . $target_item_id . "'";
			$result = mysqli_query($con, $sql);
			/* database: update to module "abstract_spreadinputs" (end) */
			
		}
		
	}
		
	/* log */
	if ($configs["backend_log"]) {
		$log = array(
			"log_name" => "sort data",
			"log_function" => __FUNCTION__,
			"log_violation" => "normal",
			"log_content_hash" => serialize($parameters),
			"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
			"log_type" => "backend",
			"log_ip" => $_SERVER["REMOTE_ADDR"],
			"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"log_date_created" => gmdate("Y-m-d H:i:s"),
			"modules_record_key" => "",
			"modules_record_target" => "",
			"modules_id" => "3",
			"modules_name" => "Abstract Spread Inputs",
			"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
			"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
			"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
			"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
		);
		create_log_data($log);
		unset($log);
		$log = array();
	}
		
	$response["status"] = true;
	$response["message"] = translate("Successfully sorted data");
	$response["values"] = $parameters;
	unset($parameters);
	$parameters = array();
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function create_abstract_spreadinputs_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Label")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Variable Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Reference")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Placeholder")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Help")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Require")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Read-only")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Disabled")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Hidden")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Strings")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum String")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Date")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Date")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Date Time")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Date Time")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Password Equal To Element")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Email")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Password")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Website (URL)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Spaces")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Special Characters (Soft)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Special Characters (Hard)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Uppercase Characters")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Lowercase Characters")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Digit")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Unique")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Prefix")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Suffix")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Value Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Values")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Switch")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "List Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Static Value for List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module ID Column Name for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Value Column Name for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "File Selector Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Date Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Color Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Tags Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Folder Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Width")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Height")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Width Ratio")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Height Ratio")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Grid Width for Input")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Alignment for Input")));
			} else {
				$column_name = str_replace("abstract_spreadinputs", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "abstract_spreadinputs_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "abstract_spreadinputs_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "abstract_spreadinputs_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "abstract_spreadinputs_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "abstract_spreadinputs_label") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_label . '" data-toggle="tooltip" title="Spread Input Label" data-original-title="Spread Input Label">' . htmlspecialchars(strip_tags(translate("Spread Input Label"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_varname") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_varname . '" data-toggle="tooltip" title="Spread Input Variable Name" data-original-title="Spread Input Variable Name">' . htmlspecialchars(strip_tags(translate("Spread Input Variable Name"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_type") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_type . '" data-toggle="tooltip" title="Spread Input Type" data-original-title="Spread Input Type">' . htmlspecialchars(strip_tags(translate("Spread Input Type"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_references") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_references . '" data-toggle="tooltip" title="Reference" data-original-title="Reference">' . htmlspecialchars(strip_tags(translate("Reference"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_placeholder") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_placeholder . '" data-toggle="tooltip" title="Spread Input Placeholder" data-original-title="Spread Input Placeholder">' . htmlspecialchars(strip_tags(translate("Spread Input Placeholder"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_help") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_help . '" data-toggle="tooltip" title="Spread Input Help" data-original-title="Spread Input Help">' . htmlspecialchars(strip_tags(translate("Spread Input Help"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_require") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_require . '" data-toggle="tooltip" title="Require" data-original-title="Require">' . htmlspecialchars(strip_tags(translate("Require"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_readonly") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_readonly . '" data-toggle="tooltip" title="Read-only" data-original-title="Read-only">' . htmlspecialchars(strip_tags(translate("Read-only"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_disable") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_disable . '" data-toggle="tooltip" title="Disabled" data-original-title="Disabled">' . htmlspecialchars(strip_tags(translate("Disabled"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_hidden") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_hidden . '" data-toggle="tooltip" title="Hidden" data-original-title="Hidden">' . htmlspecialchars(strip_tags(translate("Hidden"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_string_min") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_string_min . '" data-toggle="tooltip" title="Validate Minimum Strings" data-original-title="Validate Minimum Strings">' . htmlspecialchars(strip_tags(translate("Validate Minimum Strings"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_string_max") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_string_max . '" data-toggle="tooltip" title="Validate Maximum String" data-original-title="Validate Maximum String">' . htmlspecialchars(strip_tags(translate("Validate Maximum String"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_number_min") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_number_min . '" data-toggle="tooltip" title="Validate Minimum Number" data-original-title="Validate Minimum Number">' . htmlspecialchars(strip_tags(translate("Validate Minimum Number"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_number_max") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_number_max . '" data-toggle="tooltip" title="Validate Maximum Number" data-original-title="Validate Maximum Number">' . htmlspecialchars(strip_tags(translate("Validate Maximum Number"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_date_min") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_date_min . '" data-toggle="tooltip" title="Validate Minimum Date" data-original-title="Validate Minimum Date">' . htmlspecialchars(strip_tags(translate("Validate Minimum Date"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_date_max") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_date_max . '" data-toggle="tooltip" title="Validate Maximum Date" data-original-title="Validate Maximum Date">' . htmlspecialchars(strip_tags(translate("Validate Maximum Date"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_datetime_min") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_datetime_min . '" data-toggle="tooltip" title="Validate Minimum Date Time" data-original-title="Validate Minimum Date Time">' . htmlspecialchars(strip_tags(translate("Validate Minimum Date Time"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_datetime_max") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_datetime_max . '" data-toggle="tooltip" title="Validate Maximum Date Time" data-original-title="Validate Maximum Date Time">' . htmlspecialchars(strip_tags(translate("Validate Maximum Date Time"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_password_equal_to") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_password_equal_to . '" data-toggle="tooltip" title="Password Equal To Element" data-original-title="Password Equal To Element">' . htmlspecialchars(strip_tags(translate("Password Equal To Element"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_email") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_email . '" data-toggle="tooltip" title="Validate Email" data-original-title="Validate Email">' . htmlspecialchars(strip_tags(translate("Validate Email"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_password") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_password . '" data-toggle="tooltip" title="Validate Password" data-original-title="Validate Password">' . htmlspecialchars(strip_tags(translate("Validate Password"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_website") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_website . '" data-toggle="tooltip" title="Validate Website (URL)" data-original-title="Validate Website (URL)">' . htmlspecialchars(strip_tags(translate("Validate Website (URL)"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_no_space") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_no_space . '" data-toggle="tooltip" title="Validate Not Allow Spaces" data-original-title="Validate Not Allow Spaces">' . htmlspecialchars(strip_tags(translate("Validate Not Allow Spaces"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_no_specialchar_soft") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_no_specialchar_soft . '" data-toggle="tooltip" title="Validate Not Allow Special Characters (Soft)" data-original-title="Validate Not Allow Special Characters (Soft)">' . htmlspecialchars(strip_tags(translate("Validate Not Allow Special Characters (Soft)"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_no_specialchar_hard") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_no_specialchar_hard . '" data-toggle="tooltip" title="Validate Not Allow Special Characters (Hard)" data-original-title="Validate Not Allow Special Characters (Hard)">' . htmlspecialchars(strip_tags(translate("Validate Not Allow Special Characters (Hard)"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_upper") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_upper . '" data-toggle="tooltip" title="Validate Uppercase Characters" data-original-title="Validate Uppercase Characters">' . htmlspecialchars(strip_tags(translate("Validate Uppercase Characters"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_lower") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_lower . '" data-toggle="tooltip" title="Validate Lowercase Characters" data-original-title="Validate Lowercase Characters">' . htmlspecialchars(strip_tags(translate("Validate Lowercase Characters"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_number") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_number . '" data-toggle="tooltip" title="Validate Number" data-original-title="Validate Number">' . htmlspecialchars(strip_tags(translate("Validate Number"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_digit") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_digit . '" data-toggle="tooltip" title="Validate Digit" data-original-title="Validate Digit">' . htmlspecialchars(strip_tags(translate("Validate Digit"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_validate_unique") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_validate_unique . '" data-toggle="tooltip" title="Validate Unique" data-original-title="Validate Unique">' . htmlspecialchars(strip_tags(translate("Validate Unique"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_prefix") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_prefix . '" data-toggle="tooltip" title="Prefix" data-original-title="Prefix">' . htmlspecialchars(strip_tags(translate("Prefix"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_suffix") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_suffix . '" data-toggle="tooltip" title="Suffix" data-original-title="Suffix">' . htmlspecialchars(strip_tags(translate("Suffix"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_default_value_type") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_default_value_type . '" data-toggle="tooltip" title="Default Value Type" data-original-title="Default Value Type">' . htmlspecialchars(strip_tags(translate("Default Value Type"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_default_value") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_default_value . '" data-toggle="tooltip" title="Default Values" data-original-title="Default Values">' . htmlspecialchars(strip_tags(translate("Default Values"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_default_switch") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_default_switch . '" data-toggle="tooltip" title="Default Switch" data-original-title="Default Switch">' . htmlspecialchars(strip_tags(translate("Default Switch"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_input_list") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_input_list . '" data-toggle="tooltip" title="List Type" data-original-title="List Type">' . htmlspecialchars(strip_tags(translate("List Type"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_input_list_static_value") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_input_list_static_value . '" data-toggle="tooltip" title="Static Value for List" data-original-title="Static Value for List">' . htmlspecialchars(strip_tags(translate("Static Value for List"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_input_list_dynamic_module") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_input_list_dynamic_module . '" data-toggle="tooltip" title="Module for Dynamic List" data-original-title="Module for Dynamic List">' . htmlspecialchars(strip_tags(translate("Module for Dynamic List"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_input_list_dynamic_id_column") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_input_list_dynamic_id_column . '" data-toggle="tooltip" title="Module ID Column Name for Dynamic List" data-original-title="Module ID Column Name for Dynamic List">' . htmlspecialchars(strip_tags(translate("Module ID Column Name for Dynamic List"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_input_list_dynamic_value_column") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_input_list_dynamic_value_column . '" data-toggle="tooltip" title="Module Value Column Name for Dynamic List" data-original-title="Module Value Column Name for Dynamic List">' . htmlspecialchars(strip_tags(translate("Module Value Column Name for Dynamic List"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_file_selector_type") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_file_selector_type . '" data-toggle="tooltip" title="File Selector Type" data-original-title="File Selector Type">' . htmlspecialchars(strip_tags(translate("File Selector Type"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_date_format") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_date_format . '" data-toggle="tooltip" title="Date Format" data-original-title="Date Format">' . htmlspecialchars(strip_tags(translate("Date Format"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_color_format") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_color_format . '" data-toggle="tooltip" title="Color Format" data-original-title="Color Format">' . htmlspecialchars(strip_tags(translate("Color Format"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_tags_format") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_tags_format . '" data-toggle="tooltip" title="Tags Format" data-original-title="Tags Format">' . htmlspecialchars(strip_tags(translate("Tags Format"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_image_folder") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_image_folder . '" data-toggle="tooltip" title="Image Folder Name" data-original-title="Image Folder Name">' . htmlspecialchars(strip_tags(translate("Image Folder Name"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_image_width") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_image_width . '" data-toggle="tooltip" title="Image Width" data-original-title="Image Width">' . htmlspecialchars(strip_tags(translate("Image Width"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_image_height") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_image_height . '" data-toggle="tooltip" title="Image Height" data-original-title="Image Height">' . htmlspecialchars(strip_tags(translate("Image Height"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_image_width_ratio") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_image_width_ratio . '" data-toggle="tooltip" title="Image Width Ratio" data-original-title="Image Width Ratio">' . htmlspecialchars(strip_tags(translate("Image Width Ratio"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_image_height_ratio") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_image_height_ratio . '" data-toggle="tooltip" title="Image Height Ratio" data-original-title="Image Height Ratio">' . htmlspecialchars(strip_tags(translate("Image Height Ratio"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_gridWidth") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_gridWidth . '" data-toggle="tooltip" title="Grid Width for Input" data-original-title="Grid Width for Input">' . htmlspecialchars(strip_tags(translate("Grid Width for Input"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_alignment") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_spreadinputs_alignment . '" data-toggle="tooltip" title="Alignment for Input" data-original-title="Alignment for Input">' . htmlspecialchars(strip_tags(translate("Alignment for Input"))) . '</th>';
			} else if ($key == "abstract_spreadinputs_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "abstract_spreadinputs_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["abstract_spreadinputs_id"].'" data-target="'.$data_table_row["abstract_spreadinputs_id"].'">';

		$html .= inform_abstract_spreadinputs_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_abstract_spreadinputs_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "abstract_spreadinputs_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "abstract_spreadinputs_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_spreadinputs_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "abstract_spreadinputs_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_spreadinputs_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_spreadinputs_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_abstract_spreadinputs_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_abstract_spreadinputs_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_abstract_spreadinputs_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_abstract_spreadinputs_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_abstract_spreadinputs_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_abstract_spreadinputs_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_abstract_spreadinputs_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_abstract_spreadinputs_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_abstract_spreadinputs_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_abstract_spreadinputs_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_abstract_spreadinputs_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_abstract_spreadinputs_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["abstract_spreadinputs_id"].'" data-target="'.$data_table["abstract_spreadinputs_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_abstract_spreadinputs_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_abstract_spreadinputs_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_abstract_spreadinputs_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "abstract_spreadinputs_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "abstract_spreadinputs_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "abstract_spreadinputs_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "abstract_spreadinputs_actions") {
				if ($key == "abstract_spreadinputs_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "abstract_spreadinputs_label") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_label"]));
				} else if ($key == "abstract_spreadinputs_varname") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_varname"]));
				} else if ($key == "abstract_spreadinputs_type") {
				$td_align_class = "";
				$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_type"]));
			} else if ($key == "abstract_spreadinputs_references") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_references  = "
					SELECT `abstract_references_id`, `abstract_references_label`
					FROM   `abstract_references`
					WHERE  `abstract_references_id` = '".$data_table["abstract_spreadinputs_references"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_references = mysqli_query($con, $sql_abstract_spreadinputs_references);
					$num_abstract_spreadinputs_references = mysqli_num_rows($result_abstract_spreadinputs_references);
					if ($num_abstract_spreadinputs_references > 0) {
						$query_abstract_spreadinputs_references = mysqli_fetch_array($result_abstract_spreadinputs_references);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_references) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_references['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references['abstract_references_label']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_references['abstract_references_label']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_references);
					stop($con);
				} else if ($key == "abstract_spreadinputs_references_modules") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_references_modules  = "
					SELECT `abstract_modules_id`, `abstract_modules_name`
					FROM   `abstract_modules`
					WHERE  `abstract_modules_id` = '".$data_table["abstract_spreadinputs_references_modules"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_references_modules = mysqli_query($con, $sql_abstract_spreadinputs_references_modules);
					$num_abstract_spreadinputs_references_modules = mysqli_num_rows($result_abstract_spreadinputs_references_modules);
					if ($num_abstract_spreadinputs_references_modules > 0) {
						$query_abstract_spreadinputs_references_modules = mysqli_fetch_array($result_abstract_spreadinputs_references_modules);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_references_modules) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_references_modules['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references_modules['abstract_modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_references_modules['abstract_modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_references_modules);
					stop($con);
				} else if ($key == "abstract_spreadinputs_references_label") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_label"]));
				} else if ($key == "abstract_spreadinputs_references_varname") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_varname"]));
				} else if ($key == "abstract_spreadinputs_references_placeholder") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_placeholder"]));
				} else if ($key == "abstract_spreadinputs_references_type") {
				$td_align_class = "";
				$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_type"]));
			} else if ($key == "abstract_spreadinputs_references_help") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_help"]));
				} else if ($key == "abstract_spreadinputs_references_require") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_require_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_require_default_value[0] != "" && isset($abstract_spreadinputs_references_require_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_require"] == $abstract_spreadinputs_references_require_default_value[0]) {
							if ($abstract_spreadinputs_references_require_default_value[0] == "1") {
								$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_require"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_require_default_value[0] == "1") {
								$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_require"] == "1") {
							$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_require_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_readonly") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_readonly_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_readonly_default_value[0] != "" && isset($abstract_spreadinputs_references_readonly_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_readonly"] == $abstract_spreadinputs_references_readonly_default_value[0]) {
							if ($abstract_spreadinputs_references_readonly_default_value[0] == "1") {
								$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_readonly"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_readonly_default_value[0] == "1") {
								$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_readonly"] == "1") {
							$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_readonly_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_disable") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_disable_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_disable_default_value[0] != "" && isset($abstract_spreadinputs_references_disable_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_disable"] == $abstract_spreadinputs_references_disable_default_value[0]) {
							if ($abstract_spreadinputs_references_disable_default_value[0] == "1") {
								$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_disable"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_disable_default_value[0] == "1") {
								$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_disable"] == "1") {
							$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_disable_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_hidden") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_hidden_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_hidden_default_value[0] != "" && isset($abstract_spreadinputs_references_hidden_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_hidden"] == $abstract_spreadinputs_references_hidden_default_value[0]) {
							if ($abstract_spreadinputs_references_hidden_default_value[0] == "1") {
								$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_hidden"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_hidden_default_value[0] == "1") {
								$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_hidden"] == "1") {
							$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_hidden_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_string_max") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_string_max"]));
				} else if ($key == "abstract_spreadinputs_references_validate_string_min") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_string_min"]));
				} else if ($key == "abstract_spreadinputs_references_validate_number_min") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_number_min"]));
				} else if ($key == "abstract_spreadinputs_references_validate_number_max") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_number_max"]));
				} else if ($key == "abstract_spreadinputs_references_validate_date_min") {
					$td_align_class = "";
					$table_row_value = date_short_reformat($data_table["abstract_spreadinputs_references_validate_date_min"]);
				} else if ($key == "abstract_spreadinputs_references_validate_date_max") {
					$td_align_class = "";
					$table_row_value = date_short_reformat($data_table["abstract_spreadinputs_references_validate_date_max"]);
				} else if ($key == "abstract_spreadinputs_references_validate_datetime_min") {
					$td_align_class = "";
					$table_row_value = datetime_short_reformat($data_table["abstract_spreadinputs_references_validate_datetime_min"]);
				} else if ($key == "abstract_spreadinputs_references_validate_datetime_max") {
					$td_align_class = "";
					$table_row_value = datetime_short_reformat($data_table["abstract_spreadinputs_references_validate_datetime_max"]);
				} else if ($key == "abstract_spreadinputs_references_validate_password_equal_to") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_password_equal_to"]));
				} else if ($key == "abstract_spreadinputs_references_validate_email") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_email_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_email_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_email_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_email"] == $abstract_spreadinputs_references_validate_email_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_email_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_email"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_email_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_email"] == "1") {
							$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_email_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_password") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_password_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_password_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_password_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_password"] == $abstract_spreadinputs_references_validate_password_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_password_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_password"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_password_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_password"] == "1") {
							$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_password_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_website") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_website_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_website_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_website_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_website"] == $abstract_spreadinputs_references_validate_website_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_website_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_website"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_website_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_website"] == "1") {
							$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_website_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_no_space") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_no_space_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_no_space_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_no_space_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_no_space"] == $abstract_spreadinputs_references_validate_no_space_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_no_space_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_no_space"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_no_space_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_no_space"] == "1") {
							$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_no_space_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_no_specialchar_soft") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_no_specialchar_soft_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_no_specialchar_soft_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_no_specialchar_soft_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_no_specialchar_soft"] == $abstract_spreadinputs_references_validate_no_specialchar_soft_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_no_specialchar_soft_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_no_specialchar_soft"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_no_specialchar_soft_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_no_specialchar_soft"] == "1") {
							$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_no_specialchar_soft_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_no_specialchar_hard") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_no_specialchar_hard_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_no_specialchar_hard_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_no_specialchar_hard_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_no_specialchar_hard"] == $abstract_spreadinputs_references_validate_no_specialchar_hard_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_no_specialchar_hard_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_no_specialchar_hard"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_no_specialchar_hard_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_no_specialchar_hard"] == "1") {
							$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_no_specialchar_hard_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_upper") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_upper_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_upper_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_upper_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_upper"] == $abstract_spreadinputs_references_validate_upper_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_upper_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_upper"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_upper_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_upper"] == "1") {
							$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_upper_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_lower") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_lower_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_lower_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_lower_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_lower"] == $abstract_spreadinputs_references_validate_lower_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_lower_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_lower"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_lower_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_lower"] == "1") {
							$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_lower_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_number") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_number_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_number_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_number_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_number"] == $abstract_spreadinputs_references_validate_number_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_number_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_number"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_number_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_number"] == "1") {
							$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_number_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_digit") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_digit_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_digit_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_digit_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_digit"] == $abstract_spreadinputs_references_validate_digit_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_digit_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_digit"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_digit_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_digit"] == "1") {
							$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_digit_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_validate_unique") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_validate_unique_default_value = unserialize(stripslashes("a:1:{i:0;s:2:\"no\";}"));
					if ($abstract_spreadinputs_references_validate_unique_default_value[0] != "" && isset($abstract_spreadinputs_references_validate_unique_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_validate_unique"] == $abstract_spreadinputs_references_validate_unique_default_value[0]) {
							if ($abstract_spreadinputs_references_validate_unique_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_validate_unique"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_validate_unique_default_value[0] == "1") {
								$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_validate_unique"] == "1") {
							$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_validate_unique_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_prefix") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_prefix"]));
				} else if ($key == "abstract_spreadinputs_references_suffix") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_suffix"]));
				} else if ($key == "abstract_spreadinputs_references_default_value_type") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_default_value_type"]));
				} else if ($key == "abstract_spreadinputs_references_default_value") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags(implode(",", $data_table["abstract_spreadinputs_references_default_value"])));
				} else if ($key == "abstract_spreadinputs_references_default_switch") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_references_default_switch_default_value = unserialize(stripslashes("a:1:{i:0;s:9:\"unchecked\";}"));
					if ($abstract_spreadinputs_references_default_switch_default_value[0] != "" && isset($abstract_spreadinputs_references_default_switch_default_value[0])) {
						if ($data_table["abstract_spreadinputs_references_default_switch"] == $abstract_spreadinputs_references_default_switch_default_value[0]) {
							if ($abstract_spreadinputs_references_default_switch_default_value[0] == "1") {
								$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_default_switch"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_references_default_switch_default_value[0] == "1") {
								$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_references_default_switch"] == "1") {
							$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_references_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_references_default_switch_data_table_value;
				} else if ($key == "abstract_spreadinputs_references_file_selector_type") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_file_selector_type"]));
				} else if ($key == "abstract_spreadinputs_references_input_list") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_input_list"]));
				} else if ($key == "abstract_spreadinputs_references_input_list_static_value") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags(implode(",", $data_table["abstract_spreadinputs_references_input_list_static_value"])));
				} else if ($key == "abstract_spreadinputs_references_input_list_dynamic_module") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_references_input_list_dynamic_module  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_references_input_list_dynamic_module"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_references_input_list_dynamic_module = mysqli_query($con, $sql_abstract_spreadinputs_references_input_list_dynamic_module);
					$num_abstract_spreadinputs_references_input_list_dynamic_module = mysqli_num_rows($result_abstract_spreadinputs_references_input_list_dynamic_module);
					if ($num_abstract_spreadinputs_references_input_list_dynamic_module > 0) {
						$query_abstract_spreadinputs_references_input_list_dynamic_module = mysqli_fetch_array($result_abstract_spreadinputs_references_input_list_dynamic_module);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_references_input_list_dynamic_module) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_references_input_list_dynamic_module['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_module['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_module['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_references_input_list_dynamic_module);
					stop($con);
				} else if ($key == "abstract_spreadinputs_references_input_list_dynamic_id_column") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_references_input_list_dynamic_id_column  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_references_input_list_dynamic_id_column"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_references_input_list_dynamic_id_column = mysqli_query($con, $sql_abstract_spreadinputs_references_input_list_dynamic_id_column);
					$num_abstract_spreadinputs_references_input_list_dynamic_id_column = mysqli_num_rows($result_abstract_spreadinputs_references_input_list_dynamic_id_column);
					if ($num_abstract_spreadinputs_references_input_list_dynamic_id_column > 0) {
						$query_abstract_spreadinputs_references_input_list_dynamic_id_column = mysqli_fetch_array($result_abstract_spreadinputs_references_input_list_dynamic_id_column);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_references_input_list_dynamic_id_column) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_references_input_list_dynamic_id_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_id_column['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_id_column['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_references_input_list_dynamic_id_column);
					stop($con);
				} else if ($key == "abstract_spreadinputs_references_input_list_dynamic_value_column") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_references_input_list_dynamic_value_column  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_references_input_list_dynamic_value_column"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_references_input_list_dynamic_value_column = mysqli_query($con, $sql_abstract_spreadinputs_references_input_list_dynamic_value_column);
					$num_abstract_spreadinputs_references_input_list_dynamic_value_column = mysqli_num_rows($result_abstract_spreadinputs_references_input_list_dynamic_value_column);
					if ($num_abstract_spreadinputs_references_input_list_dynamic_value_column > 0) {
						$query_abstract_spreadinputs_references_input_list_dynamic_value_column = mysqli_fetch_array($result_abstract_spreadinputs_references_input_list_dynamic_value_column);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_references_input_list_dynamic_value_column) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_references_input_list_dynamic_value_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_value_column['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_references_input_list_dynamic_value_column['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_references_input_list_dynamic_value_column);
					stop($con);
				} else if ($key == "abstract_spreadinputs_references_date_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_date_format"]));
				} else if ($key == "abstract_spreadinputs_references_color_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_color_format"]));
				} else if ($key == "abstract_spreadinputs_references_image_folder") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_image_folder"]));
				} else if ($key == "abstract_spreadinputs_references_image_width") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_image_width"]));
				} else if ($key == "abstract_spreadinputs_references_image_height") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_image_height"]));
				} else if ($key == "abstract_spreadinputs_references_image_width_ratio") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_image_width_ratio"]));
				} else if ($key == "abstract_spreadinputs_references_image_height_ratio") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_image_height_ratio"]));
				} else if ($key == "abstract_spreadinputs_references_tags_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_tags_format"]));
				} else if ($key == "abstract_spreadinputs_references_gridWidth") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_gridWidth"]));
				} else if ($key == "abstract_spreadinputs_references_alignment") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_references_alignment"]));
				} else if ($key == "abstract_spreadinputs_placeholder") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_placeholder"]));
				} else if ($key == "abstract_spreadinputs_help") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_help"]));
				} else if ($key == "abstract_spreadinputs_require") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_require_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_require_default_value[0] != "" && isset($abstract_spreadinputs_require_default_value[0])) {
						if ($data_table["abstract_spreadinputs_require"] == $abstract_spreadinputs_require_default_value[0]) {
							if ($abstract_spreadinputs_require_default_value[0] == "1") {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_require"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_require_default_value[0] == "1") {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_require"] == "1") {
							$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_require_data_table_value;
				} else if ($key == "abstract_spreadinputs_readonly") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_readonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_readonly_default_value[0] != "" && isset($abstract_spreadinputs_readonly_default_value[0])) {
						if ($data_table["abstract_spreadinputs_readonly"] == $abstract_spreadinputs_readonly_default_value[0]) {
							if ($abstract_spreadinputs_readonly_default_value[0] == "1") {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_readonly"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_readonly_default_value[0] == "1") {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_readonly"] == "1") {
							$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_readonly_data_table_value;
				} else if ($key == "abstract_spreadinputs_disable") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_disable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_disable_default_value[0] != "" && isset($abstract_spreadinputs_disable_default_value[0])) {
						if ($data_table["abstract_spreadinputs_disable"] == $abstract_spreadinputs_disable_default_value[0]) {
							if ($abstract_spreadinputs_disable_default_value[0] == "1") {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_disable"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_disable_default_value[0] == "1") {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_disable"] == "1") {
							$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_disable_data_table_value;
				} else if ($key == "abstract_spreadinputs_hidden") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_hidden_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_hidden_default_value[0] != "" && isset($abstract_spreadinputs_hidden_default_value[0])) {
						if ($data_table["abstract_spreadinputs_hidden"] == $abstract_spreadinputs_hidden_default_value[0]) {
							if ($abstract_spreadinputs_hidden_default_value[0] == "1") {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_hidden"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_hidden_default_value[0] == "1") {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_hidden"] == "1") {
							$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_hidden_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_string_min") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_string_min"]));
				} else if ($key == "abstract_spreadinputs_validate_string_max") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_string_max"]));
				} else if ($key == "abstract_spreadinputs_validate_number_min") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_number_min"]));
				} else if ($key == "abstract_spreadinputs_validate_number_max") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_number_max"]));
				} else if ($key == "abstract_spreadinputs_validate_date_min") {
					$td_align_class = "";
					$table_row_value = date_short_reformat($data_table["abstract_spreadinputs_validate_date_min"]);
				} else if ($key == "abstract_spreadinputs_validate_date_max") {
					$td_align_class = "";
					$table_row_value = date_short_reformat($data_table["abstract_spreadinputs_validate_date_max"]);
				} else if ($key == "abstract_spreadinputs_validate_datetime_min") {
					$td_align_class = "";
					$table_row_value = datetime_short_reformat($data_table["abstract_spreadinputs_validate_datetime_min"]);
				} else if ($key == "abstract_spreadinputs_validate_datetime_max") {
					$td_align_class = "";
					$table_row_value = datetime_short_reformat($data_table["abstract_spreadinputs_validate_datetime_max"]);
				} else if ($key == "abstract_spreadinputs_validate_password_equal_to") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_password_equal_to"]));
				} else if ($key == "abstract_spreadinputs_validate_email") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_email_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_email_default_value[0] != "" && isset($abstract_spreadinputs_validate_email_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_email"] == $abstract_spreadinputs_validate_email_default_value[0]) {
							if ($abstract_spreadinputs_validate_email_default_value[0] == "1") {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_email"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_email_default_value[0] == "1") {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_email"] == "1") {
							$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_email_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_password") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_password_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_password_default_value[0] != "" && isset($abstract_spreadinputs_validate_password_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_password"] == $abstract_spreadinputs_validate_password_default_value[0]) {
							if ($abstract_spreadinputs_validate_password_default_value[0] == "1") {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_password"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_password_default_value[0] == "1") {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_password"] == "1") {
							$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_password_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_website") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_website_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_website_default_value[0] != "" && isset($abstract_spreadinputs_validate_website_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_website"] == $abstract_spreadinputs_validate_website_default_value[0]) {
							if ($abstract_spreadinputs_validate_website_default_value[0] == "1") {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_website"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_website_default_value[0] == "1") {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_website"] == "1") {
							$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_website_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_no_space") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_no_space_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_no_space_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_space_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_no_space"] == $abstract_spreadinputs_validate_no_space_default_value[0]) {
							if ($abstract_spreadinputs_validate_no_space_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_space"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_no_space_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_no_space"] == "1") {
							$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_no_space_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_no_specialchar_soft") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_no_specialchar_soft_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_no_specialchar_soft"] == $abstract_spreadinputs_validate_no_specialchar_soft_default_value[0]) {
							if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_specialchar_soft"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_no_specialchar_soft"] == "1") {
							$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_no_specialchar_soft_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_no_specialchar_hard") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_no_specialchar_hard_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_no_specialchar_hard"] == $abstract_spreadinputs_validate_no_specialchar_hard_default_value[0]) {
							if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_specialchar_hard"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_no_specialchar_hard"] == "1") {
							$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_no_specialchar_hard_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_upper") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_upper_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_upper_default_value[0] != "" && isset($abstract_spreadinputs_validate_upper_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_upper"] == $abstract_spreadinputs_validate_upper_default_value[0]) {
							if ($abstract_spreadinputs_validate_upper_default_value[0] == "1") {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_upper"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_upper_default_value[0] == "1") {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_upper"] == "1") {
							$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_upper_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_lower") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_lower_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_lower_default_value[0] != "" && isset($abstract_spreadinputs_validate_lower_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_lower"] == $abstract_spreadinputs_validate_lower_default_value[0]) {
							if ($abstract_spreadinputs_validate_lower_default_value[0] == "1") {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_lower"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_lower_default_value[0] == "1") {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_lower"] == "1") {
							$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_lower_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_number") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_number_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_number_default_value[0] != "" && isset($abstract_spreadinputs_validate_number_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_number"] == $abstract_spreadinputs_validate_number_default_value[0]) {
							if ($abstract_spreadinputs_validate_number_default_value[0] == "1") {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_number"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_number_default_value[0] == "1") {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_number"] == "1") {
							$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_number_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_digit") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_digit_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_digit_default_value[0] != "" && isset($abstract_spreadinputs_validate_digit_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_digit"] == $abstract_spreadinputs_validate_digit_default_value[0]) {
							if ($abstract_spreadinputs_validate_digit_default_value[0] == "1") {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_digit"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_digit_default_value[0] == "1") {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_digit"] == "1") {
							$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_digit_data_table_value;
				} else if ($key == "abstract_spreadinputs_validate_unique") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_validate_unique_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_spreadinputs_validate_unique_default_value[0] != "" && isset($abstract_spreadinputs_validate_unique_default_value[0])) {
						if ($data_table["abstract_spreadinputs_validate_unique"] == $abstract_spreadinputs_validate_unique_default_value[0]) {
							if ($abstract_spreadinputs_validate_unique_default_value[0] == "1") {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_unique"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_validate_unique_default_value[0] == "1") {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_validate_unique"] == "1") {
							$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_validate_unique_data_table_value;
				} else if ($key == "abstract_spreadinputs_prefix") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_prefix"]));
				} else if ($key == "abstract_spreadinputs_suffix") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_suffix"]));
				} else if ($key == "abstract_spreadinputs_default_value_type") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_default_value_type"]));
				} else if ($key == "abstract_spreadinputs_default_value") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags(implode(",", $data_table["abstract_spreadinputs_default_value"])));
				} else if ($key == "abstract_spreadinputs_default_switch") {
					$td_align_class = ".text-center";
					$abstract_spreadinputs_default_switch_default_value = unserialize(stripslashes("a:1:{i:0;s:9:\\\"unchecked\\\";}"));
					if ($abstract_spreadinputs_default_switch_default_value[0] != "" && isset($abstract_spreadinputs_default_switch_default_value[0])) {
						if ($data_table["abstract_spreadinputs_default_switch"] == $abstract_spreadinputs_default_switch_default_value[0]) {
							if ($abstract_spreadinputs_default_switch_default_value[0] == "1") {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_default_switch"])) . '</span>';
							}
						} else {
							if ($abstract_spreadinputs_default_switch_default_value[0] == "1") {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_spreadinputs_default_switch"] == "1") {
							$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_spreadinputs_default_switch_data_table_value;
				} else if ($key == "abstract_spreadinputs_input_list") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_input_list"]));
				} else if ($key == "abstract_spreadinputs_input_list_static_value") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags(implode(",", $data_table["abstract_spreadinputs_input_list_static_value"])));
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_module") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_input_list_dynamic_module  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_module"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_input_list_dynamic_module = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_module);
					$num_abstract_spreadinputs_input_list_dynamic_module = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_module);
					if ($num_abstract_spreadinputs_input_list_dynamic_module > 0) {
						$query_abstract_spreadinputs_input_list_dynamic_module = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_module);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_module) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_module['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_module['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_input_list_dynamic_module['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_module);
					stop($con);
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_id_column") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_input_list_dynamic_id_column  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_id_column"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_id_column);
					$num_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_id_column);
					if ($num_abstract_spreadinputs_input_list_dynamic_id_column > 0) {
						$query_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_id_column);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_id_column) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_id_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_id_column['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_input_list_dynamic_id_column['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_id_column);
					stop($con);
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_value_column") {
					$td_align_class = "";
					$con = start();
					$sql_abstract_spreadinputs_input_list_dynamic_value_column  = "
					SELECT `modules_db_name`, `modules_name`
					FROM   `modules`
					WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_value_column"]."'
					LIMIT  1";
					$result_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_value_column);
					$num_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_value_column);
					if ($num_abstract_spreadinputs_input_list_dynamic_value_column > 0) {
						$query_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_value_column);
						$module_link = false;
						foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_value_column) as $pages_key) {
							if ($pages_key == "pages_link" && !empty($pages_key)) {
								$module_link = true;
							}
						}
						if ($module_link) {
							$table_row_value = '<a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_value_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_value_column['modules_name']).'</a>';
						} else {
							$table_row_value = strip_tags($query_abstract_spreadinputs_input_list_dynamic_value_column['modules_name']);
						}
					}
					mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_value_column);
					stop($con);
				} else if ($key == "abstract_spreadinputs_file_selector_type") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_file_selector_type"]));
				} else if ($key == "abstract_spreadinputs_date_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_date_format"]));
				} else if ($key == "abstract_spreadinputs_color_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_color_format"]));
				} else if ($key == "abstract_spreadinputs_tags_format") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_tags_format"]));
				} else if ($key == "abstract_spreadinputs_image_folder") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_image_folder"]));
				} else if ($key == "abstract_spreadinputs_image_width") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_image_width"]));
				} else if ($key == "abstract_spreadinputs_image_height") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_image_height"]));
				} else if ($key == "abstract_spreadinputs_image_width_ratio") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_image_width_ratio"]));
				} else if ($key == "abstract_spreadinputs_image_height_ratio") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_image_height_ratio"]));
				} else if ($key == "abstract_spreadinputs_gridWidth") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_gridWidth"]));
				} else if ($key == "abstract_spreadinputs_alignment") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_alignment"]));
				} else if ($key == "abstract_spreadinputs_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_short_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="'.backend_rewrite_url("users.php").'?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_abstract_spreadinputs_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="'.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["abstract_spreadinputs_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "abstract_spreadinputs_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}

				$html .= '
				<td id="datatable-actions-'.$data_table["abstract_spreadinputs_id"].'" class="text-center">
					<div class="btn-group">';

				$html .= '
						<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["abstract_spreadinputs_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
						<i class="fa fa-eye"></i>
						</button>';
				
				if ($_SESSION["users_level"] != "5") {

		
			
						if (!isset($data_table["abstract_spreadinputs_protected"]) || (isset($data_table["abstract_spreadinputs_protected"]) && $data_table["abstract_spreadinputs_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

						$html .= '
						<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["abstract_spreadinputs_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
						<i class="fa fa-pencil"></i>
						</button>';
						
						}

						$html .= '
						<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["abstract_spreadinputs_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
						<i class="fa fa-copy"></i>
						</button>';

						$html .= '
						<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["abstract_spreadinputs_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?">
						<i class="fa fa-times"></i>
						</button>';
			
				}

				$html .= '
					</div>
				</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_abstract_spreadinputs_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_abstract_spreadinputs_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Label")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Variable Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Reference")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Placeholder")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Spread Input Help")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Require")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Read-only")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Disabled")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Hidden")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Strings")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum String")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Date")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Date")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Minimum Date Time")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Maximum Date Time")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Password Equal To Element")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Email")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Password")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Website (URL)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Spaces")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Special Characters (Soft)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Not Allow Special Characters (Hard)")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Uppercase Characters")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Lowercase Characters")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Number")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Digit")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Validate Unique")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Prefix")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Suffix")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Value Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Values")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Default Switch")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "List Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Static Value for List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module ID Column Name for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Value Column Name for Dynamic List")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "File Selector Type")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Date Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Color Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Tags Format")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Folder Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Width")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Height")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Width Ratio")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Image Height Ratio")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Grid Width for Input")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Alignment for Input")));
			} else {
				$column_name = str_replace("abstract_spreadinputs", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "abstract_spreadinputs_actions") {
				if ($key == "abstract_spreadinputs_id") {
					if (!empty($data_table["abstract_spreadinputs_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_label") {
					if ($data_table["abstract_spreadinputs_label"] != "") {
						$table_row_value = '<strong>' . translate("Spread Input Label") . '</strong><br />' . $data_table["abstract_spreadinputs_label"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_varname") {
					if ($data_table["abstract_spreadinputs_varname"] != "") {
						$table_row_value = '<strong>' . translate("Spread Input Variable Name") . '</strong><br />' . $data_table["abstract_spreadinputs_varname"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_type") {
					if ($data_table["abstract_spreadinputs_type"] != "") {
						$table_row_value = '<strong>' . translate("Spread Input Type") . '</strong><br />' . $data_table["abstract_spreadinputs_type"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_references") {
					if ($data_table["abstract_spreadinputs_references"] != "") {
						$con = start();
						$sql_abstract_spreadinputs_references  = "
						SELECT `abstract_references_id`, `abstract_references_label`
						FROM   `abstract_references`
						WHERE  `abstract_references_id` = '".$data_table["abstract_spreadinputs_references"]."'
						LIMIT  1";
						$result_abstract_spreadinputs_references = mysqli_query($con, $sql_abstract_spreadinputs_references);
						$num_abstract_spreadinputs_references = mysqli_num_rows($result_abstract_spreadinputs_references);
						if ($num_abstract_spreadinputs_references > 0) {
							$query_abstract_spreadinputs_references = mysqli_fetch_array($result_abstract_spreadinputs_references);
							$module_link = false;
							foreach (array_keys($query_abstract_spreadinputs_references) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Reference") . '</strong><br /><a href="'.rewrite_url($query_abstract_spreadinputs_references['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_references['abstract_references_label']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Reference") . '</strong><br />'.strip_tags($query_abstract_spreadinputs_references['abstract_references_label']).'<br /><br />';
							}
						}
						mysqli_free_result($result_abstract_spreadinputs_references);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_placeholder") {
					if ($data_table["abstract_spreadinputs_placeholder"] != "") {
						$table_row_value = '<strong>' . translate("Spread Input Placeholder") . '</strong><br />' . $data_table["abstract_spreadinputs_placeholder"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_help") {
					if ($data_table["abstract_spreadinputs_help"] != "") {
						$table_row_value = '<strong>' . translate("Spread Input Help") . '</strong><br />' . $data_table["abstract_spreadinputs_help"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_require") {
					if ($data_table["abstract_spreadinputs_require"] != "") {
						$abstract_spreadinputs_require_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_require_default_value[0] != "" && isset($abstract_spreadinputs_require_default_value[0])) {
							if ($data_table["abstract_spreadinputs_require"] == $abstract_spreadinputs_require_default_value[0]) {
								if ($abstract_spreadinputs_require_default_value[0] == "1") {
									$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_require"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_require_default_value[0] == "1") {
									$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_require"] == "1") {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_require_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Require") . '</strong><br />' . $abstract_spreadinputs_require_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_readonly") {
					if ($data_table["abstract_spreadinputs_readonly"] != "") {
						$abstract_spreadinputs_readonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_readonly_default_value[0] != "" && isset($abstract_spreadinputs_readonly_default_value[0])) {
							if ($data_table["abstract_spreadinputs_readonly"] == $abstract_spreadinputs_readonly_default_value[0]) {
								if ($abstract_spreadinputs_readonly_default_value[0] == "1") {
									$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_readonly"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_readonly_default_value[0] == "1") {
									$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_readonly"] == "1") {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_readonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Read-only") . '</strong><br />' . $abstract_spreadinputs_readonly_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_disable") {
					if ($data_table["abstract_spreadinputs_disable"] != "") {
						$abstract_spreadinputs_disable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_disable_default_value[0] != "" && isset($abstract_spreadinputs_disable_default_value[0])) {
							if ($data_table["abstract_spreadinputs_disable"] == $abstract_spreadinputs_disable_default_value[0]) {
								if ($abstract_spreadinputs_disable_default_value[0] == "1") {
									$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_disable"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_disable_default_value[0] == "1") {
									$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_disable"] == "1") {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_disable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Disabled") . '</strong><br />' . $abstract_spreadinputs_disable_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_hidden") {
					if ($data_table["abstract_spreadinputs_hidden"] != "") {
						$abstract_spreadinputs_hidden_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_hidden_default_value[0] != "" && isset($abstract_spreadinputs_hidden_default_value[0])) {
							if ($data_table["abstract_spreadinputs_hidden"] == $abstract_spreadinputs_hidden_default_value[0]) {
								if ($abstract_spreadinputs_hidden_default_value[0] == "1") {
									$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_hidden"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_hidden_default_value[0] == "1") {
									$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_hidden"] == "1") {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_hidden_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Hidden") . '</strong><br />' . $abstract_spreadinputs_hidden_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_string_min") {
					if ($data_table["abstract_spreadinputs_validate_string_min"] != "") {
						$table_row_value = '<strong>' . translate("Validate Minimum Strings") . '</strong><br />' . $data_table["abstract_spreadinputs_validate_string_min"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_string_max") {
					if ($data_table["abstract_spreadinputs_validate_string_max"] != "") {
						$table_row_value = '<strong>' . translate("Validate Maximum String") . '</strong><br />' . $data_table["abstract_spreadinputs_validate_string_max"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_number_min") {
					if ($data_table["abstract_spreadinputs_validate_number_min"] != "") {
						$table_row_value = '<strong>' . translate("Validate Minimum Number") . '</strong><br />' . $data_table["abstract_spreadinputs_validate_number_min"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_number_max") {
					if ($data_table["abstract_spreadinputs_validate_number_max"] != "") {
						$table_row_value = '<strong>' . translate("Validate Maximum Number") . '</strong><br />' . $data_table["abstract_spreadinputs_validate_number_max"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_date_min") {
					if ($data_table["abstract_spreadinputs_validate_date_min"] != "") {
						$table_row_value = '<strong>' . translate("Validate Minimum Date") . '</strong><br />' . date_short_reformat($data_table["abstract_spreadinputs_validate_date_min"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_date_max") {
					if ($data_table["abstract_spreadinputs_validate_date_max"] != "") {
						$table_row_value = '<strong>' . translate("Validate Maximum Date") . '</strong><br />' . date_short_reformat($data_table["abstract_spreadinputs_validate_date_max"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_datetime_min") {
					if ($data_table["abstract_spreadinputs_validate_datetime_min"] != "") {
						$table_row_value = '<strong>' . translate("Validate Minimum Date Time") . '</strong><br />' . datetime_reformat($data_table["abstract_spreadinputs_validate_datetime_min"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_datetime_max") {
					if ($data_table["abstract_spreadinputs_validate_datetime_max"] != "") {
						$table_row_value = '<strong>' . translate("Validate Maximum Date Time") . '</strong><br />' . datetime_reformat($data_table["abstract_spreadinputs_validate_datetime_max"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_password_equal_to") {
					if ($data_table["abstract_spreadinputs_validate_password_equal_to"] != "") {
						$table_row_value = '<strong>' . translate("Password Equal To Element") . '</strong><br />' . $data_table["abstract_spreadinputs_validate_password_equal_to"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_email") {
					if ($data_table["abstract_spreadinputs_validate_email"] != "") {
						$abstract_spreadinputs_validate_email_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_email_default_value[0] != "" && isset($abstract_spreadinputs_validate_email_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_email"] == $abstract_spreadinputs_validate_email_default_value[0]) {
								if ($abstract_spreadinputs_validate_email_default_value[0] == "1") {
									$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_email"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_email_default_value[0] == "1") {
									$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_email"] == "1") {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_email_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Email") . '</strong><br />' . $abstract_spreadinputs_validate_email_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_password") {
					if ($data_table["abstract_spreadinputs_validate_password"] != "") {
						$abstract_spreadinputs_validate_password_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_password_default_value[0] != "" && isset($abstract_spreadinputs_validate_password_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_password"] == $abstract_spreadinputs_validate_password_default_value[0]) {
								if ($abstract_spreadinputs_validate_password_default_value[0] == "1") {
									$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_password"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_password_default_value[0] == "1") {
									$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_password"] == "1") {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_password_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Password") . '</strong><br />' . $abstract_spreadinputs_validate_password_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_website") {
					if ($data_table["abstract_spreadinputs_validate_website"] != "") {
						$abstract_spreadinputs_validate_website_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_website_default_value[0] != "" && isset($abstract_spreadinputs_validate_website_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_website"] == $abstract_spreadinputs_validate_website_default_value[0]) {
								if ($abstract_spreadinputs_validate_website_default_value[0] == "1") {
									$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_website"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_website_default_value[0] == "1") {
									$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_website"] == "1") {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_website_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Website (URL)") . '</strong><br />' . $abstract_spreadinputs_validate_website_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_no_space") {
					if ($data_table["abstract_spreadinputs_validate_no_space"] != "") {
						$abstract_spreadinputs_validate_no_space_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_no_space_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_space_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_no_space"] == $abstract_spreadinputs_validate_no_space_default_value[0]) {
								if ($abstract_spreadinputs_validate_no_space_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_space"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_no_space_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_no_space"] == "1") {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_space_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Not Allow Spaces") . '</strong><br />' . $abstract_spreadinputs_validate_no_space_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_no_specialchar_soft") {
					if ($data_table["abstract_spreadinputs_validate_no_specialchar_soft"] != "") {
						$abstract_spreadinputs_validate_no_specialchar_soft_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_no_specialchar_soft"] == $abstract_spreadinputs_validate_no_specialchar_soft_default_value[0]) {
								if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_specialchar_soft"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_no_specialchar_soft_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_no_specialchar_soft"] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_soft_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Not Allow Special Characters (Soft)") . '</strong><br />' . $abstract_spreadinputs_validate_no_specialchar_soft_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_no_specialchar_hard") {
					if ($data_table["abstract_spreadinputs_validate_no_specialchar_hard"] != "") {
						$abstract_spreadinputs_validate_no_specialchar_hard_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] != "" && isset($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_no_specialchar_hard"] == $abstract_spreadinputs_validate_no_specialchar_hard_default_value[0]) {
								if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_no_specialchar_hard"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_no_specialchar_hard_default_value[0] == "1") {
									$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_no_specialchar_hard"] == "1") {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_no_specialchar_hard_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Not Allow Special Characters (Hard)") . '</strong><br />' . $abstract_spreadinputs_validate_no_specialchar_hard_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_upper") {
					if ($data_table["abstract_spreadinputs_validate_upper"] != "") {
						$abstract_spreadinputs_validate_upper_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_upper_default_value[0] != "" && isset($abstract_spreadinputs_validate_upper_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_upper"] == $abstract_spreadinputs_validate_upper_default_value[0]) {
								if ($abstract_spreadinputs_validate_upper_default_value[0] == "1") {
									$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_upper"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_upper_default_value[0] == "1") {
									$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_upper"] == "1") {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_upper_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Uppercase Characters") . '</strong><br />' . $abstract_spreadinputs_validate_upper_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_lower") {
					if ($data_table["abstract_spreadinputs_validate_lower"] != "") {
						$abstract_spreadinputs_validate_lower_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_lower_default_value[0] != "" && isset($abstract_spreadinputs_validate_lower_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_lower"] == $abstract_spreadinputs_validate_lower_default_value[0]) {
								if ($abstract_spreadinputs_validate_lower_default_value[0] == "1") {
									$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_lower"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_lower_default_value[0] == "1") {
									$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_lower"] == "1") {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_lower_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Lowercase Characters") . '</strong><br />' . $abstract_spreadinputs_validate_lower_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_number") {
					if ($data_table["abstract_spreadinputs_validate_number"] != "") {
						$abstract_spreadinputs_validate_number_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_number_default_value[0] != "" && isset($abstract_spreadinputs_validate_number_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_number"] == $abstract_spreadinputs_validate_number_default_value[0]) {
								if ($abstract_spreadinputs_validate_number_default_value[0] == "1") {
									$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_number"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_number_default_value[0] == "1") {
									$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_number"] == "1") {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_number_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Number") . '</strong><br />' . $abstract_spreadinputs_validate_number_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_digit") {
					if ($data_table["abstract_spreadinputs_validate_digit"] != "") {
						$abstract_spreadinputs_validate_digit_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_digit_default_value[0] != "" && isset($abstract_spreadinputs_validate_digit_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_digit"] == $abstract_spreadinputs_validate_digit_default_value[0]) {
								if ($abstract_spreadinputs_validate_digit_default_value[0] == "1") {
									$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_digit"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_digit_default_value[0] == "1") {
									$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_digit"] == "1") {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_digit_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Digit") . '</strong><br />' . $abstract_spreadinputs_validate_digit_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_validate_unique") {
					if ($data_table["abstract_spreadinputs_validate_unique"] != "") {
						$abstract_spreadinputs_validate_unique_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_spreadinputs_validate_unique_default_value[0] != "" && isset($abstract_spreadinputs_validate_unique_default_value[0])) {
							if ($data_table["abstract_spreadinputs_validate_unique"] == $abstract_spreadinputs_validate_unique_default_value[0]) {
								if ($abstract_spreadinputs_validate_unique_default_value[0] == "1") {
									$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_validate_unique"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_validate_unique_default_value[0] == "1") {
									$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_validate_unique"] == "1") {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_validate_unique_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Validate Unique") . '</strong><br />' . $abstract_spreadinputs_validate_unique_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_prefix") {
					if ($data_table["abstract_spreadinputs_prefix"] != "") {
						$table_row_value = '<strong>' . translate("Prefix") . '</strong><br />' . $data_table["abstract_spreadinputs_prefix"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_suffix") {
					if ($data_table["abstract_spreadinputs_suffix"] != "") {
						$table_row_value = '<strong>' . translate("Suffix") . '</strong><br />' . $data_table["abstract_spreadinputs_suffix"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_default_value_type") {
					if ($data_table["abstract_spreadinputs_default_value_type"] != "") {
						$table_row_value = '<strong>' . translate("Default Value Type") . '</strong><br />' . $data_table["abstract_spreadinputs_default_value_type"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_default_value") {
					if ($data_table["abstract_spreadinputs_default_value"] != "") {
						$table_row_value = '<strong>' . translate("Default Values") . '</strong><br />' . implode(",", $data_table["abstract_spreadinputs_default_value"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_default_switch") {
					if ($data_table["abstract_spreadinputs_default_switch"] != "") {
						$abstract_spreadinputs_default_switch_default_value = unserialize(stripslashes("a:1:{i:0;s:9:\\\"unchecked\\\";}"));
						if ($abstract_spreadinputs_default_switch_default_value[0] != "" && isset($abstract_spreadinputs_default_switch_default_value[0])) {
							if ($data_table["abstract_spreadinputs_default_switch"] == $abstract_spreadinputs_default_switch_default_value[0]) {
								if ($abstract_spreadinputs_default_switch_default_value[0] == "1") {
									$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_spreadinputs_default_switch"])) . '</span>';
								}
							} else {
								if ($abstract_spreadinputs_default_switch_default_value[0] == "1") {
									$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_spreadinputs_default_switch"] == "1") {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_spreadinputs_default_switch_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Default Switch") . '</strong><br />' . $abstract_spreadinputs_default_switch_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_input_list") {
					if ($data_table["abstract_spreadinputs_input_list"] != "") {
						$table_row_value = '<strong>' . translate("List Type") . '</strong><br />' . $data_table["abstract_spreadinputs_input_list"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_input_list_static_value") {
					if ($data_table["abstract_spreadinputs_input_list_static_value"] != "") {
						$table_row_value = '<strong>' . translate("Static Value for List") . '</strong><br />' . implode(",", $data_table["abstract_spreadinputs_input_list_static_value"]) . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_module") {
					if ($data_table["abstract_spreadinputs_input_list_dynamic_module"] != "") {
						$con = start();
						$sql_abstract_spreadinputs_input_list_dynamic_module  = "
						SELECT `modules_db_name`, `modules_name`
						FROM   `modules`
						WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_module"]."'
						LIMIT  1";
						$result_abstract_spreadinputs_input_list_dynamic_module = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_module);
						$num_abstract_spreadinputs_input_list_dynamic_module = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_module);
						if ($num_abstract_spreadinputs_input_list_dynamic_module > 0) {
							$query_abstract_spreadinputs_input_list_dynamic_module = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_module);
							$module_link = false;
							foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_module) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Module for Dynamic List") . '</strong><br /><a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_module['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_module['modules_name']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Module for Dynamic List") . '</strong><br />'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_module['modules_name']).'<br /><br />';
							}
						}
						mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_module);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_id_column") {
					if ($data_table["abstract_spreadinputs_input_list_dynamic_id_column"] != "") {
						$con = start();
						$sql_abstract_spreadinputs_input_list_dynamic_id_column  = "
						SELECT `modules_db_name`, `modules_name`
						FROM   `modules`
						WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_id_column"]."'
						LIMIT  1";
						$result_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_id_column);
						$num_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_id_column);
						if ($num_abstract_spreadinputs_input_list_dynamic_id_column > 0) {
							$query_abstract_spreadinputs_input_list_dynamic_id_column = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_id_column);
							$module_link = false;
							foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_id_column) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Module ID Column Name for Dynamic List") . '</strong><br /><a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_id_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_id_column['modules_name']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Module ID Column Name for Dynamic List") . '</strong><br />'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_id_column['modules_name']).'<br /><br />';
							}
						}
						mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_id_column);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_input_list_dynamic_value_column") {
					if ($data_table["abstract_spreadinputs_input_list_dynamic_value_column"] != "") {
						$con = start();
						$sql_abstract_spreadinputs_input_list_dynamic_value_column  = "
						SELECT `modules_db_name`, `modules_name`
						FROM   `modules`
						WHERE  `modules_db_name` = '".$data_table["abstract_spreadinputs_input_list_dynamic_value_column"]."'
						LIMIT  1";
						$result_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_query($con, $sql_abstract_spreadinputs_input_list_dynamic_value_column);
						$num_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_num_rows($result_abstract_spreadinputs_input_list_dynamic_value_column);
						if ($num_abstract_spreadinputs_input_list_dynamic_value_column > 0) {
							$query_abstract_spreadinputs_input_list_dynamic_value_column = mysqli_fetch_array($result_abstract_spreadinputs_input_list_dynamic_value_column);
							$module_link = false;
							foreach (array_keys($query_abstract_spreadinputs_input_list_dynamic_value_column) as $pages_key) {
								if ($pages_key == "pages_link" && !empty($pages_key)) {
									$module_link = true;
								}
							}
							if ($module_link) {
								$table_row_value = '<strong>' . translate("Module Value Column Name for Dynamic List") . '</strong><br /><a href="'.rewrite_url($query_abstract_spreadinputs_input_list_dynamic_value_column['pages_link']).'" target="_blank">'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_value_column['modules_name']).'</a><br /><br />';
							} else {
								$table_row_value = '<strong>' . translate("Module Value Column Name for Dynamic List") . '</strong><br />'.strip_tags($query_abstract_spreadinputs_input_list_dynamic_value_column['modules_name']).'<br /><br />';
							}
						}
						mysqli_free_result($result_abstract_spreadinputs_input_list_dynamic_value_column);
						stop($con);
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_file_selector_type") {
					if ($data_table["abstract_spreadinputs_file_selector_type"] != "") {
						$table_row_value = '<strong>' . translate("File Selector Type") . '</strong><br />' . $data_table["abstract_spreadinputs_file_selector_type"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_date_format") {
					if ($data_table["abstract_spreadinputs_date_format"] != "") {
						$table_row_value = '<strong>' . translate("Date Format") . '</strong><br />' . $data_table["abstract_spreadinputs_date_format"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_color_format") {
					if ($data_table["abstract_spreadinputs_color_format"] != "") {
						$table_row_value = '<strong>' . translate("Color Format") . '</strong><br />' . $data_table["abstract_spreadinputs_color_format"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_tags_format") {
					if ($data_table["abstract_spreadinputs_tags_format"] != "") {
						$table_row_value = '<strong>' . translate("Tags Format") . '</strong><br />' . $data_table["abstract_spreadinputs_tags_format"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_image_folder") {
					if ($data_table["abstract_spreadinputs_image_folder"] != "") {
						$table_row_value = '<strong>' . translate("Image Folder Name") . '</strong><br />' . $data_table["abstract_spreadinputs_image_folder"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_image_width") {
					if ($data_table["abstract_spreadinputs_image_width"] != "") {
						$table_row_value = '<strong>' . translate("Image Width") . '</strong><br />' . $data_table["abstract_spreadinputs_image_width"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_image_height") {
					if ($data_table["abstract_spreadinputs_image_height"] != "") {
						$table_row_value = '<strong>' . translate("Image Height") . '</strong><br />' . $data_table["abstract_spreadinputs_image_height"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_image_width_ratio") {
					if ($data_table["abstract_spreadinputs_image_width_ratio"] != "") {
						$table_row_value = '<strong>' . translate("Image Width Ratio") . '</strong><br />' . $data_table["abstract_spreadinputs_image_width_ratio"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_image_height_ratio") {
					if ($data_table["abstract_spreadinputs_image_height_ratio"] != "") {
						$table_row_value = '<strong>' . translate("Image Height Ratio") . '</strong><br />' . $data_table["abstract_spreadinputs_image_height_ratio"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_gridWidth") {
					if ($data_table["abstract_spreadinputs_gridWidth"] != "") {
						$table_row_value = '<strong>' . translate("Grid Width for Input") . '</strong><br />' . $data_table["abstract_spreadinputs_gridWidth"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_alignment") {
					if ($data_table["abstract_spreadinputs_alignment"] != "") {
						$table_row_value = '<strong>' . translate("Alignment for Input") . '</strong><br />' . $data_table["abstract_spreadinputs_alignment"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_spreadinputs_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "abstract_spreadinputs_date_created") {
					if (!empty($data_table["abstract_spreadinputs_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["abstract_spreadinputs_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="'.backend_rewrite_url("users.php").'?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
						if (($key == "pages_link" || $module_link) && $i == 1) {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
						} else {
							$table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_abstract_spreadinputs_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
					}
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_abstract_spreadinputs_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_spreadinputs_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_spreadinputs_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_spreadinputs_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_abstract_spreadinputs_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "abstract_spreadinputs" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '3'
	AND (`log_function` = 'create_abstract_spreadinputs_data' OR `log_function` = 'update_abstract_spreadinputs_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spreadinputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_spreadinputs" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '3'
	AND (`log_function` = 'create_abstract_spreadinputs_data' OR `log_function` = 'update_abstract_spreadinputs_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_spreadinputs" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if ($query_log_content_hash["abstract_spreadinputs_validate_string_min"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_validate_string_min"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_string_max"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_validate_string_max"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_number_min"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_validate_number_min"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_number_max"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_validate_number_max"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_date_min"] == "0000-00-00") {
			$query_log_content_hash["abstract_spreadinputs_validate_date_min"] = "";
		} else {
			$query_log_content_hash["abstract_spreadinputs_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query_log_content_hash["abstract_spreadinputs_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_date_max"] == "0000-00-00") {
			$query_log_content_hash["abstract_spreadinputs_validate_date_max"] = "";
		} else {
			$query_log_content_hash["abstract_spreadinputs_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query_log_content_hash["abstract_spreadinputs_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_datetime_min"] == "0000-00-00 00:00:00") {
			$query_log_content_hash["abstract_spreadinputs_validate_datetime_min"] = "";
		} else {
			$query_log_content_hash["abstract_spreadinputs_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query_log_content_hash["abstract_spreadinputs_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query_log_content_hash["abstract_spreadinputs_validate_datetime_max"] == "0000-00-00 00:00:00") {
			$query_log_content_hash["abstract_spreadinputs_validate_datetime_max"] = "";
		} else {
			$query_log_content_hash["abstract_spreadinputs_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query_log_content_hash["abstract_spreadinputs_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query_log_content_hash["abstract_spreadinputs_default_value"]) && !empty($query_log_content_hash["abstract_spreadinputs_default_value"])) {
		
		for ($j = 0; $j < count($query_log_content_hash["abstract_spreadinputs_default_value"]); $j++) {
			if (!empty($query_log_content_hash["abstract_spreadinputs_default_value"][$j]) && is_array($query_log_content_hash["abstract_spreadinputs_default_value"][$j])) {
				foreach($query_log_content_hash["abstract_spreadinputs_default_value"][$j] as $key => $value) {
					$query_log_content_hash["abstract_spreadinputs_default_value"][$j][] = $value;
				}
			}
		}
		$query_log_content_hash["abstract_spreadinputs_default_value_json"] = json_encode($query_log_content_hash["abstract_spreadinputs_default_value"]);
	}
	if (isset($query_log_content_hash["abstract_spreadinputs_input_list_static_value"]) && !empty($query_log_content_hash["abstract_spreadinputs_input_list_static_value"])) {
		
		for ($j = 0; $j < count($query_log_content_hash["abstract_spreadinputs_input_list_static_value"]); $j++) {
			if (!empty($query_log_content_hash["abstract_spreadinputs_input_list_static_value"][$j]) && is_array($query_log_content_hash["abstract_spreadinputs_input_list_static_value"][$j])) {
				foreach($query_log_content_hash["abstract_spreadinputs_input_list_static_value"][$j] as $key => $value) {
					$query_log_content_hash["abstract_spreadinputs_input_list_static_value"][$j][] = $value;
				}
			}
		}
		$query_log_content_hash["abstract_spreadinputs_input_list_static_value_json"] = json_encode($query_log_content_hash["abstract_spreadinputs_input_list_static_value"]);
	}
		if ($query_log_content_hash["abstract_spreadinputs_image_width"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_image_width"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_image_height"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_image_height"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_image_width_ratio"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_image_width_ratio"] = "";
		}
		if ($query_log_content_hash["abstract_spreadinputs_image_height_ratio"] == "0") {
			$query_log_content_hash["abstract_spreadinputs_image_height_ratio"] = "";
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_spreadinputs" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references($target, $start = 0, $limit = "", $sort_by = "abstract_spread_inputs_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_references` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spread_inputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spread_inputs_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_references($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "abstract_spread_inputs" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_references` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_references` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_references_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `abstract_references_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "abstract_references" */
	$sql = "
	SELECT *
	FROM `abstract_references`
	WHERE `abstract_references_id` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["references_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["references_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_references_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_references_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_references_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_references_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_references_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_references_validate_date_min"] == "" || $query["abstract_spreadinputs_references_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_references_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_references_validate_date_max"] == "" || $query["abstract_spreadinputs_references_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_references_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_references_validate_datetime_min"] == "" || $query["abstract_spreadinputs_references_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_references_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_references_validate_datetime_max"] == "" || $query["abstract_spreadinputs_references_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_references_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_references_default_value"]) && !empty($query["abstract_spreadinputs_references_default_value"])) {
		$query["abstract_spreadinputs_references_default_value"] = unserialize($query["abstract_spreadinputs_references_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_references_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_references_default_value"][$j]) && is_array($query["abstract_spreadinputs_references_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_references_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_references_default_value"][$j][] = $value;
				}
			}
		}
		$query["abstract_spreadinputs_references_default_value_json"] = json_encode($query["abstract_spreadinputs_references_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_references_input_list_static_value"]) && !empty($query["abstract_spreadinputs_references_input_list_static_value"])) {
		$query["abstract_spreadinputs_references_input_list_static_value"] = unserialize($query["abstract_spreadinputs_references_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_references_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_references_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_references_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_references_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_references_input_list_static_value"][$j][] = $value;
				}
			}
		}
		$query["abstract_spreadinputs_references_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_references_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_references_image_width"] == "0") {
			$query["abstract_spreadinputs_references_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_height"] == "0") {
			$query["abstract_spreadinputs_references_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_references_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_references_image_height_ratio"] = "";
		}
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_references_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `abstract_references_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "abstract_references" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_references`
	WHERE `abstract_references_id` != '0'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_references`
	WHERE `abstract_references_id` != '0'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_references_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `abstract_references_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "abstract_references" */
	$sql = "
	SELECT *
	FROM `abstract_references`
	WHERE `abstract_references_id` = '".$target."'
	".$extra_sql_data_dynamic_list."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["references_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["references_date_created"], $configs["datetimezone"])));
		if ($query["abstract_spreadinputs_references_validate_string_max"] == "0") {
			$query["abstract_spreadinputs_references_validate_string_max"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_string_min"] == "0") {
			$query["abstract_spreadinputs_references_validate_string_min"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_number_min"] == "0") {
			$query["abstract_spreadinputs_references_validate_number_min"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_number_max"] == "0") {
			$query["abstract_spreadinputs_references_validate_number_max"] = "";
		}
		if ($query["abstract_spreadinputs_references_validate_date_min"] == "0000-00-00" || $query["abstract_spreadinputs_references_validate_date_min"] == "" || $query["abstract_spreadinputs_references_validate_date_min"] == NULL) {
			$query["abstract_spreadinputs_references_validate_date_min"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_date_min"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_date_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_date_max"] == "0000-00-00" || $query["abstract_spreadinputs_references_validate_date_max"] == "" || $query["abstract_spreadinputs_references_validate_date_max"] == NULL) {
			$query["abstract_spreadinputs_references_validate_date_max"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_date_max"] = date("Y-m-d", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_date_max"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_datetime_min"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_references_validate_datetime_min"] == "" || $query["abstract_spreadinputs_references_validate_datetime_min"] == NULL) {
			$query["abstract_spreadinputs_references_validate_datetime_min"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_datetime_min"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_datetime_min"], $configs["datetimezone"])));
		}
		if ($query["abstract_spreadinputs_references_validate_datetime_max"] == "0000-00-00 00:00:00" || $query["abstract_spreadinputs_references_validate_datetime_max"] == "" || $query["abstract_spreadinputs_references_validate_datetime_max"] == NULL) {
			$query["abstract_spreadinputs_references_validate_datetime_max"] = "";
		} else {
			$query["abstract_spreadinputs_references_validate_datetime_max"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spreadinputs_references_validate_datetime_max"], $configs["datetimezone"])));
		}
	if (isset($query["abstract_spreadinputs_references_default_value"]) && !empty($query["abstract_spreadinputs_references_default_value"])) {
		$query["abstract_spreadinputs_references_default_value"] = unserialize($query["abstract_spreadinputs_references_default_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_references_default_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_references_default_value"][$j]) && is_array($query["abstract_spreadinputs_references_default_value"][$j])) {
				foreach($query["abstract_spreadinputs_references_default_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_references_default_value"][$j][] = $value;
				}
			}
		}
		$query["abstract_spreadinputs_references_default_value_json"] = json_encode($query["abstract_spreadinputs_references_default_value"]);
	}
	if (isset($query["abstract_spreadinputs_references_input_list_static_value"]) && !empty($query["abstract_spreadinputs_references_input_list_static_value"])) {
		$query["abstract_spreadinputs_references_input_list_static_value"] = unserialize($query["abstract_spreadinputs_references_input_list_static_value"]);
		for ($j = 0; $j < count($query["abstract_spreadinputs_references_input_list_static_value"]); $j++) {
			if (!empty($query["abstract_spreadinputs_references_input_list_static_value"][$j]) && is_array($query["abstract_spreadinputs_references_input_list_static_value"][$j])) {
				foreach($query["abstract_spreadinputs_references_input_list_static_value"][$j] as $key => $value) {
					$query["abstract_spreadinputs_references_input_list_static_value"][$j][] = $value;
				}
			}
		}
		$query["abstract_spreadinputs_references_input_list_static_value_json"] = json_encode($query["abstract_spreadinputs_references_input_list_static_value"]);
	}
		if ($query["abstract_spreadinputs_references_image_width"] == "0") {
			$query["abstract_spreadinputs_references_image_width"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_height"] == "0") {
			$query["abstract_spreadinputs_references_image_height"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_width_ratio"] == "0") {
			$query["abstract_spreadinputs_references_image_width_ratio"] = "";
		}
		if ($query["abstract_spreadinputs_references_image_height_ratio"] == "0") {
			$query["abstract_spreadinputs_references_image_height_ratio"] = "";
		}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module($target, $start = 0, $limit = "", $sort_by = "abstract_spread_inputs_id", $sort_direction = "ASC", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}

	$sql = "
	SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_input_list_dynamic_module` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_spread_inputs_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_spread_inputs_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spread_inputs_data_all_by_abstract_spreadinputs_input_list_dynamic_module($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_dynamic_list .= "AND `abstract_spread_inputs_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "abstract_spread_inputs" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_input_list_dynamic_module` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_spread_inputs`
	WHERE `abstract_spreadinputs_input_list_dynamic_module` = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
	
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_db_name` != '0'
	".$extra_sql_data_dynamic_list;
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["input_list_dynamic_module_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["input_list_dynamic_module_date_created"], $configs["datetimezone"])));
				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_db_name` != '0'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `modules`
	WHERE `modules_db_name` != '0'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_input_list_dynamic_module_data_dynamic_list_by_id($target, $activate = "") {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	
	/* database: get data from module "modules" */
	$sql = "
	SELECT *
	FROM `modules`
	WHERE `modules_db_name` = '".$target."'
	".$extra_sql_data_dynamic_list."
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			
			$query = mysqli_fetch_array($result);

			$query["input_list_dynamic_module_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["input_list_dynamic_module_date_created"], $configs["datetimezone"])));

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $query;
			
			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spread_inputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spread_inputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				$query["COLUMN_NAME_HUMAN"] = trim(ucwords(str_replace("_", " ", str_replace($query["TABLE_NAME"], "", $query["COLUMN_NAME"]))));
				if ($query["COLUMN_NAME_HUMAN"] == "Id") {
					$query["COLUMN_NAME_HUMAN"] = "ID";
				}
				$queries[$i] = $query;
				$i++;
				$table_name = $query["TABLE_NAME"];
			}
			$action_field = array(
				"COLUMN_NAME" => $table_name."_actions",
				"TABLE_NAME" => $table_name,
				"COLUMN_NAME_HUMAN" => "Actions",
				0 => $table_name."_actions",
				1 => $table_name,
				2 => "Actions",
			);
			$queries[$i] = $action_field;

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			$response["target"] = $_POST["target"];
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_input_list_dynamic_id_column_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	";
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {
				$query["COLUMN_NAME_HUMAN"] = trim(ucwords(str_replace("_", " ", str_replace($query["TABLE_NAME"], "", $query["COLUMN_NAME"]))));
				if ($query["COLUMN_NAME_HUMAN"] == "Id") {
					$query["COLUMN_NAME_HUMAN"] = "ID";
				}
				$queries[$i] = $query;
				$i++;
				$table_name = $query["TABLE_NAME"];
			}
			$action_field = array(
				"COLUMN_NAME" => $table_name."_actions",
				"TABLE_NAME" => $table_name,
				"COLUMN_NAME_HUMAN" => "Actions",
				0 => $table_name."_actions",
				1 => $table_name,
				2 => "Actions",
			);
			$queries[$i] = $action_field;

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;
			$response["target"] = $_POST["target"];
			
			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_spreadinputs_input_list_dynamic_value_column_data_dynamic_list($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = "") {

	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$filters = escape_string($con, $filters);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);
	
	/* database: add extended command */
	$extra_sql_data_dynamic_list = "";
	if (isset($activate) && $activate != "") {
		$extra_sql_data_dynamic_list .= "AND `modules_activate` = '".$activate."' ";
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_dynamic_list .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_dynamic_list .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_dynamic_list .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_dynamic_list .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_dynamic_list .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_dynamic_list .= $sort_direction." ";
		} else {
			$extra_sql_data_dynamic_list .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_dynamic_list .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_dynamic_list .= "LIMIT ".$limit;
		}
	}
	
	/* database: get data from module "modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	} else {
	$sql = "
	EXPLAIN SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $target . "'
	".$extra_sql_data_dynamic_list;
	}
	$result = mysqli_query($con, $sql);
	
	if ($result) {
	
		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");
		
		unset($query);
		$query = array();
		
	} else {
	
		$response["status"] = false;
		$response["target"] = "abstract_spreadinputs";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_spreadinputs</strong>(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
	
	}
	
	mysqli_free_result($result);
	stop($con);

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

?>