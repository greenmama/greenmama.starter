<?php mb_internal_encoding("UTF-8");

function translate ($text) {
	
	global $configs;
	global $translations;
	
	if ($configs["backend_translate"]) {
		if (isset($translations) && isset($translations[$configs["backend_language"]])) {

			$translation_lower = array();
			foreach ($translations as $key => $value) {
				foreach ($value as $key_inside => $value_inside) {
					$translation_lower[$key][strtolower($key_inside)] = $value_inside;
				}
			}

			if (isset($translation_lower[$configs["backend_language"]][strtolower($text)]) && $translation_lower[$configs["backend_language"]][strtolower($text)] != "") {
				return $translation_lower[$configs["backend_language"]][strtolower($text)];
			} else {
				return $text;
			}

			unset($translation_lower);
			
		} else {
			
			return $text;
			
		}
	} else {
		return $text;
	}
	
}

function translate_value ($text) {
	
	global $configs;
	global $translations;
	
    if ($configs["translate_value"]) {
        if (isset($translations) && isset($translations[$configs["backend_language"]])) {
            $translation_lower = array();
            foreach ($translations as $key => $value) {
                foreach ($value as $key_inside => $value_inside) {
                    $translation_lower[$key][strtolower($key_inside)] = $value_inside;
                }
            }

            if (isset($translation_lower[$configs["backend_language"]][strtolower($text)]) && $translation_lower[$configs["backend_language"]][strtolower($text)] != "") {
                return $translation_lower[$configs["backend_language"]][strtolower($text)]." (".$text.")";
            } else {
                return $text;
            }

            unset($translation_lower);
        } else {
            return $text;
        }
    } else {
		return $text;
	}
	
}
?>