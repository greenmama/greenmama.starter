<?php
function get_image_thumbnail($image_path) {
	$image_file_path = basename($image_path);
	$image_directory_path = str_replace($image_file_path, "", $image_path);
	return $image_directory_path.'thumbnail/'.$image_file_path;
}

function get_image_large_thumbnail($image_path) {
	$image_file_path = basename($image_path);
	$image_directory_path = str_replace($image_file_path, "", $image_path);
	return $image_directory_path.'large/'.$image_file_path;
}
?>