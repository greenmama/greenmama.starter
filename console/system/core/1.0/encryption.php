<?php mb_internal_encoding('UTF-8'); 

function encrypt($data) {
	
	global $configs;
    $key = $configs["encrypt_key"];  // Clé de 8 caractères max
	
    $data = serialize($data);
    $td = mcrypt_module_open(MCRYPT_DES,"",MCRYPT_MODE_ECB,"");
    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td,$key,$iv);
    $data = base64_encode(mcrypt_generic($td, '!'.$data));
    mcrypt_generic_deinit($td);
    return $data;
}
 
function decrypt($data) {
    
	global $configs;
    $key = $configs["encrypt_key"];
	
    $td = mcrypt_module_open(MCRYPT_DES,"",MCRYPT_MODE_ECB,"");
    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td,$key,$iv);
    $data = mdecrypt_generic($td, base64_decode($data));
    mcrypt_generic_deinit($td);
 
    if (substr($data,0,1) != '!')
        return false;
 
    $data = substr($data,1,strlen($data)-1);
    return unserialize($data);
}

/* using to encrypt the name of the files from the backend */
function dtmd5_encrypt($data) {
	$random_data = '';
	for ($i = 0; $i<1; $i++) 
	{
		$random_data .= mt_rand(0,5000);
	}
	$datetime_data = date("Ymd_His");
	$encrypted_data = md5($datetime_data."_".$random_data);
	if(strlen($data) != mb_strlen($data, 'utf-8')) {
		
		//$key = "trnsl.1.1.20160713T104039Z.b5c63303953a9f1f.1796d8f7b6a8084afee0ede6ed4ef6d5656b1679";
		//$content_json = file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?key='.$key.'&text='.$data.'&lang=en');
		//$data_translation = json_decode($content_json, true);

		//if (!empty($data_translation['text'][0]) && isset($data_translation['text'][0])) {
			//if(strlen($data_translation['text'][0]) != mb_strlen($data_translation['text'][0], 'utf-8')) {
			//	$data_new = md5($data).'_'.$encrypted_data;
			//} else {
				$data_new = str_replace(" ", "_", $data).'_'.$encrypted_data;
			//}
		//} else {
			//$data_new = md5($data).'_'.$encrypted_data;
		//}
	} else {
		$data_new = str_replace(" ", "_", $data).'_'.$encrypted_data;
		//$data_new = str_replace(" ", "_", $data);
	}
	
	$data_new = trim($data_new);
	$data_new = preg_replace('!\s+!', ' ', $data_new);
	$data_new = str_replace(' ', '-', $data_new);
	$data_new = str_replace(array("^","!","@","#","%","^","&","*","(",")","+","=","~","{","}","[","]",";",":","\"","'","<",">",".","\,","\\","/",), "-", $data_new);
	$data_new = str_replace(array("?","\'","\""), "", $data_new);
	$data_new = preg_replace('/[-]+/', '-', $data_new);
	$data_new = str_replace(' ', '-', $data_new);
	$data_new = preg_replace('/[_]+/', '_', $data_new);
	$data_new = trim($data_new, '-');
	$data_new = trim($data_new, '_');
	$data_new = strtolower($data_new);
	
	return $data_new;
}

function create_guid() { 
    // The field names refer to RFC 4122 section 4.1.2
    return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
        mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
        mt_rand(0, 65535), // 16 bits for "time_mid"
        mt_rand(0, 4095),  // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
        bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
            // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
            // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
            // 8 bits for "clk_seq_low"
        mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node" 
    ); 
}
?>