<?php
function escape_string($con, $parameters) {
	if (isset($parameters) && !empty($parameters)) {
		if (is_array($parameters)) {
			foreach ($parameters as $key => $value) {
				if (!empty($value)) {
					$parameters[$key] = escape_string($con, $value);
				}
			}
		} else {
			if (strlen($parameters) > 5000){
			   $parameters = $parameters;
			} else {
			   $parameters = mysqli_real_escape_string($con, $parameters);
			}
		}
		return $parameters;
	}
}

function form_spam_protection () {
	$bait_seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
	shuffle($bait_seed);
	$bait_random = "";
	foreach (array_rand($bait_seed, 10) as $k) $bait_random .= $bait_seed[$k];
	echo '<input type="hidden" id="'.$bait_random.'" name="'.$bait_random.'"><input type="hidden" id="form-name" name="form_name" value="'.$bait_random.'" />';
	return $bait_random;
}
?>