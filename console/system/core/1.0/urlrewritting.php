<?php 
function rewrite_url($url){
	
	global $configs;
	global $con;

	if ($configs["url_rewritting"]) {
		
		if (strpos($url, "index.php?id=")) {
			$url_rewritten = str_replace("index.php?id=", "", $url);
		} else {
			$url_rewritten = $url;
		}
		if (strpos($url, "&") && strpos($url, "?") == false) {
			$url_rewritten = str_replace_first("&", "?", $url_rewritten);
		}

		if ($url_rewritten == "index.php/" || $url_rewritten == "index/") {
			$url_rewritten = "";
		} else {
			$url_rewritten = str_replace("index.php", "", $url_rewritten);
			$url_rewritten = str_replace("index.html", "", $url_rewritten);
			$url_rewritten = str_replace(".php", "", $url_rewritten);
			$url_rewritten = str_replace(".html", "", $url_rewritten);
		}
		
		if (strpos($url, "?id=")) {

			if (is_utf8($url_rewritten)) {
				if (function_exists('mb_strtolower')) {
					$url_rewritten = mb_strtolower($url_rewritten, 'UTF-8');
				}
				$url_rewritten = utf8_url_encode($url_rewritten, 999999999999);
			}

			$url_rewritten = str_replace("?id=", "", $url_rewritten);
			$url_rewritten = str_replace("&id=", "", $url_rewritten);
			
			$url_rewritten = str_replace("?page=", "/", $url_rewritten);
			$url_rewritten = str_replace("&page=", "/", $url_rewritten);
			
		} else {
			
			if (is_utf8($url_rewritten)) {
				if (function_exists('mb_strtolower')) {
					$url_rewritten = mb_strtolower($url_rewritten, 'UTF-8');
				}
				$url_rewritten = utf8_url_encode($url_rewritten, 999999999999);
			}
				
			$url_rewritten = str_replace("?id=", "", $url_rewritten);
			$url_rewritten = str_replace("&id=", "", $url_rewritten);
			
			$last_string_position = strlen($url_rewritten) - 1;
			if ($url_rewritten[$last_string_position] != "/" && strpos($url, "?page=") == false && strpos($url, "&page=") == false) {
				if ($url != "index.php") {
					$url_rewritten = $url_rewritten."/";
				} else {
					$url_rewritten = $url_rewritten;
				}
			} else {
				$url_rewritten = str_replace("?page=", "/", $url_rewritten);
				$url_rewritten = str_replace("&page=", "/", $url_rewritten);
			}

		}
		
		$con = start();
		
		$sql_pages = "
		SELECT * FROM `pages` 
		WHERE `pages_link` = '" . $url . "'
		LIMIT 1;";
		$result_pages = mysqli_query($con, $sql_pages);
		if ($result_pages) {
			$num_pages = mysqli_num_rows($result_pages);
			if ($num_pages > 0) {
				$pages = mysqli_fetch_array($result_pages);
				if (isset($pages["modules_id"]) && $pages["modules_id"] != "") {
					$sql_modules = "
					SELECT * FROM `modules` 
					WHERE `modules_id` = '" . $pages["modules_id"] . "'
					LIMIT 1;";
					$result_modules = mysqli_query($con, $sql_modules);
					if ($result_modules) {
						$num_modules = mysqli_num_rows($result_modules);
						if ($num_modules > 0) {
							$modules = mysqli_fetch_array($result_modules);
							if (isset($modules["modules_pages_parent_link_field"]) && $modules["modules_pages_parent_link_field"] != "") {
								if (isset($modules["modules_db_name"]) && $modules["modules_db_name"] != "" && isset($pages["modules_record_target"]) && $pages["modules_record_target"] != "") {
									$sql_parent = "
									SELECT * FROM `".$modules["modules_db_name"]."` 
									WHERE `".$modules["modules_key"]."_id` = '" . $pages["modules_record_target"] . "'
									LIMIT 1;";
									$result_parent = mysqli_query($con, $sql_parent);
									if ($result_parent) {
										$num_parent = mysqli_num_rows($result_parent);
										if ($num_parent > 0) {
											$query_parent = mysqli_fetch_array($result_parent);
											if (strpos(getcwd(), "/system")) {
												$path = $modules["modules_function_link"];
											} else if (strpos(getcwd(), "/system/core/".$configs["version"])) {
												$path = "../../../system/".$modules["modules_function_link"];
											} else {
												$path = "system/".$modules["modules_function_link"];
											}
											if (file_exists($path)) {
												if (!function_exists("get_" . $modules["modules_pages_parent_link_field"] . "_data_dynamic_list_by_id")) {
													require_once($path);
												}
												if (function_exists("get_" . $modules["modules_pages_parent_link_field"] . "_data_dynamic_list_by_id")) {	
													$dynamic_list = call_user_func("get_" . $modules["modules_pages_parent_link_field"] . "_data_dynamic_list_by_id", $query_parent[$modules["modules_pages_parent_link_field"]]);
													if (isset($dynamic_list["pages_link"]) && $dynamic_list["pages_link"] != "") {
														$url_rewritten = str_replace(".php", "", $dynamic_list["pages_link"])."/".$url_rewritten;
													} else {
														$url_rewritten = $query_parent[$modules["modules_pages_parent_link_field"]]."/".$url_rewritten;
													}
												} else {
													$url_rewritten = $query_parent[$modules["modules_pages_parent_link_field"]]."/".$url_rewritten;
												}
											} else {
												$url_rewritten = $query_parent[$modules["modules_pages_parent_link_field"]]."/".$url_rewritten;
											}
											unset($query_parent);
											$query_parent = array($query_parent);
										}
									}
									mysqli_free_result($result_parent);
								}
							}
							if ($modules["modules_individual_pages_parent_link"] == 1) {
								$sql_pages_individual = "
								SELECT * FROM `pages` 
								WHERE (`pages_id` = '" . $modules["pages_id"] . "' 
								  AND `languages_id` = '" . $pages["languages_id"] . "')
								  OR (`pages_translate` = '" . $modules["pages_id"] . "' 
								  AND `languages_id` = '" . $pages["languages_id"] . "')
								LIMIT 1;";
								$result_pages_individual = mysqli_query($con, $sql_pages_individual);
								if ($result_pages_individual) {
									$num_pages_individual = mysqli_num_rows($result_pages_individual);
									if ($num_pages_individual > 0) {
										$pages_individual = mysqli_fetch_array($result_pages_individual);
										$url_rewritten = str_replace(".php", "", $pages_individual["pages_link"])."/".$url_rewritten;
									}
									mysqli_free_result($result_pages_individual);
								}
							}
							unset($modules);
							$modules = array($modules);
						}
					}
					mysqli_free_result($result_modules);
				}
				unset($pages);
				$pages = array($pages);
			}
		}
		mysqli_free_result($result_pages);
		
		if (!isset($con) && !empty($con)) {
			stop($con);
		}
		
	} else {
		
		if (strpos($url, "?id=") == false) {
											 					 
			if (preg_match_all('/^[0-9]+/', $url)) {
				
				$url_rewritten = str_replace("?page=", "", $url);
				$url_rewritten = str_replace("&page=", "", $url_rewritten);
				$url_rewritten = preg_replace('/(^[0-9]+)/', "?page=$1", $url_rewritten);
				$url_rewritten = str_replace("&id=?", "?id=&", $url_rewritten);

			} else {
				
				if (strpos($url, "?")) {
					$url_rewritten = str_replace("?", "&", $url);
				} else {
					$url_rewritten = $url;
				}
				if (strpos($url, "index.php?id=") == false && strpos($url, "?id=") == false) {
					$url_rewritten = "index.php?id=".$url_rewritten;
				} else {
					$url_rewritten = $url_rewritten;
				}
				if ($url_rewritten == "index.php?id=index.php" || $url_rewritten == "index.php?id=index") {
					$url_rewritten = "./";
				} else {
					$url_rewritten = str_replace("index.php", "", $url_rewritten);
				}
				
				if (is_utf8($url_rewritten)) {
					if (function_exists('mb_strtolower')) {
						$url_rewritten = mb_strtolower($url_rewritten, 'UTF-8');
					}
					$url_rewritten = utf8_url_encode($url_rewritten, 999999999999);
				}

				$last_string_position = strlen($url_rewritten) - 1;
				if ($url_rewritten[$last_string_position] == "/") {
					$url_rewritten = rtrim($url_rewritten, "/");
				} else {
					$url_rewritten = preg_replace('/([\%a-zA-Z0-9\s\_\-\/]+)\/([0-9]+)/', "$1&page=$2", $url_rewritten);
				}

			}
			
		} else {
				
			$url_rewritten = $url;
			
			if (is_utf8($url_rewritten)) {
				if (function_exists('mb_strtolower')) {
					$url_rewritten = mb_strtolower($url_rewritten, 'UTF-8');
				}
				$url_rewritten = utf8_url_encode($url_rewritten, 999999999999);
			}
			
			$url_rewritten = preg_replace('/\/([0-9]+)/', "&page=$1", $url_rewritten);
			
		}
	}
	
	if (strpos($url_rewritten, $configs["website_url"]) == false) {
		$url_rewritten = $configs["website_url"].$url_rewritten;
	}

	return trim($url_rewritten);
	
}

function backend_rewrite_url($url){
	
	global $configs;
	
	if ($configs["url_rewritting"]) {
		$url_rewritten = str_replace(".php", "", $url);
	} else {
		$url_rewritten = $url;
	}
	
	return trim($url_rewritten);
	
}

function get_link_file($link){
	$link = str_replace("index.php?id=","",$link);
	$link = str_replace('.html',".php",$link);
	if (strpos($link,".php") <= 0) {
		$link = $link.".php";
	}
	return trim($link);
}

function generate_title_link($title) {

    return generate_title_link_name($title).".php";
	
}

function generate_title_link_name($title) {
	
    $title = strip_tags($title);
    // Preserve escaped octets.
    $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
    // Remove percent signs that are not part of an octet.
    $title = str_replace('%', '', $title);
    // Restore octets.
    $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

    if (is_utf8($title)) {
        if (function_exists('mb_strtolower')) {
            $title = mb_strtolower($title, 'UTF-8');
        }
        $title = utf8_url_encode($title, 1000);
    }

    $title = strtolower($title);
    $title = preg_replace('/&.+?;/', '', $title); // kill entities
    $title = str_replace('.', '-', $title);
    $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
    $title = preg_replace('/\s+/', '-', $title);
    $title = preg_replace('|-+|', '-', $title);
    $title = trim($title, '-');

    return urldecode($title);
	
}

function utf8_url_encode( $utf8_string, $length = 0 ) {
    $unicode = '';
    $values = array();
    $num_octets = 1;
    $unicode_length = 0;

    $string_length = strlen( $utf8_string );
    for ($i = 0; $i < $string_length; $i++ ) {

        $value = ord( $utf8_string[ $i ] );

        if ( $value < 128 ) {
            if ( $length && ( $unicode_length >= $length ) )
                break;
            $unicode .= chr($value);
            $unicode_length++;
        } else {
            if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

            $values[] = $value;

            if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
                break;
            if ( count( $values ) == $num_octets ) {
                if ($num_octets == 3) {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                    $unicode_length += 9;
                } else {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                    $unicode_length += 6;
                }

                $values = array();
                $num_octets = 1;
            }
        }
    }

    return $unicode;
}

function is_utf8($str) {
    $length = strlen($str);
    for ($i=0; $i < $length; $i++) {
        $c = ord($str[$i]);
        if ($c < 0x80) $n = 0; # 0bbbbbbb
        elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
        elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
        elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
        elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
        elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
        else return false; # Does not match any model
        for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
            if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                return false;
        }
    }
    return true;
}
?>