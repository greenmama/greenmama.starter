<?php
if (!isset($_SESSION)) {
	session_start();
}

/* get AJAX function */
if(isset($_POST["method"])){
	$method = $_POST["method"];
} else if (isset($_GET["method"])) {
	$method = $_GET["method"];
}
if (isset($method) && $method != "" 
	&& ($method == "generate_abstract_modules"
		|| $method == "regenerate_abstract_modules"
		|| $method == "get_abstract_modules_data_by_id" 
		|| $method == "get_abstract_modules_data_all" 
		|| $method == "count_abstract_modules_data_all" 
		|| $method == "get_abstract_modules_data_all_excluding" 
		|| $method == "count_abstract_modules_data_all_excluding" 
		|| $method == "get_abstract_modules_data_table_all" 
		|| $method == "count_abstract_modules_data_table_all" 
		|| $method == "get_search_abstract_modules_data_table_all" 
		|| $method == "count_search_abstract_modules_data_table_all" 
		|| $method == "create_abstract_modules_data" 
		|| $method == "update_abstract_modules_data" 
		|| $method == "patch_abstract_modules_data" 
		|| $method == "delete_abstract_modules_data" 
		|| $method == "create_abstract_modules_table" 
		|| $method == "create_abstract_modules_table_row" 
		|| $method == "update_abstract_modules_table_row" 
		|| $method == "delete_abstract_modules_table_row" 
		|| $method == "create_abstract_modules_table_row" 
		|| $method == "update_abstract_modules_table_row" 
		|| $method == "inform_abstract_modules_table_row" 
		|| $method == "view_abstract_modules_table_row" 
		|| $method == "create_sort_abstract_modules" 
		|| $method == "sort_abstract_modules" 
		|| $method == "get_abstract_modules_edit_log_data" 
		|| $method == "count_abstract_modules_edit_log_data" 
		|| $method == "get_abstract_modules_edit_log_data_by_id" 
		|| $method == "count_abstract_modules_edit_log_data_by_id")
) {
	
	/* include: configurations */
	require_once("../../../../config.php");
	require_once("../../translation.config.php");
	require_once("initial.php");
	
	/* include: functions - library */
	require_once("security.php");
	require_once("urlrewritting.php");
	require_once("datetime.php");
	require_once("encryption.php");
	require_once("string.php");
	require_once("translation.php");
	
	/* include: functions - core */
	require_once("log.php");
	require_once("api.php");
	require_once("authentication.php");
	require_once("modules.php");
	require_once("users.php");
	
	
	/* get parameters for AJAX function */
	if(isset($_POST["parameters"])){
		$parameters = $_POST["parameters"];
	}

	/* get table module field */
	if(isset($_POST["table_module_field"])){
		$table_module_field = $_POST["table_module_field"];
	}
	
	mb_internal_encoding($configs["encoding"]);
	date_default_timezone_set($configs["timezone"]);

	if ($method == "generate_abstract_modules"){
		generate_abstract_modules($parameters);
	} else if ($method == "regenerate_abstract_modules"){
		regenerate_abstract_modules($parameters);
	} else if ($method == "get_abstract_modules_data_by_id"){
		get_abstract_modules_data_by_id($parameters, $_POST["activate"]);
	} else if ($method == "get_abstract_modules_data_all"){
		get_abstract_modules_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_modules_data_all"){
		count_abstract_modules_data_all($_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_modules_data_all_excluding"){
		get_abstract_modules_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_modules_data_all_excluding"){
		count_abstract_modules_data_all_excluding($parameters, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_abstract_modules_data_table_all"){
		get_abstract_modules_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_abstract_modules_data_table_all"){
		count_abstract_modules_data_table_all($table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "get_search_abstract_modules_data_table_all"){
		get_search_abstract_modules_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "count_search_abstract_modules_data_table_all"){
		count_search_abstract_modules_data_table_all($parameters, $table_module_field, $_POST["start"], $_POST["limit"], $_POST["sort_by"], $_POST["sort_direction"], $_POST["activate"], $_POST["filters"], $_POST["extended_command"]);
	} else if ($method == "create_abstract_modules_data"){
		create_abstract_modules_data($parameters);
	} else if ($method == "update_abstract_modules_data"){
		update_abstract_modules_data($parameters);
	} else if ($method == "patch_abstract_modules_data"){
		patch_abstract_modules_data($parameters);
	} else if ($method == "delete_abstract_modules_data"){
		delete_abstract_modules_data($parameters);
	} else if ($method == "create_abstract_modules_table"){
		create_abstract_modules_table($parameters, $table_module_field); 
	} else if ($method == "create_abstract_modules_table_row"){
		create_abstract_modules_table_row($parameters, $table_module_field);
	} else if ($method == "update_abstract_modules_table_row"){
		update_abstract_modules_table_row($parameters, $table_module_field); 
	} else if ($method == "inform_abstract_modules_table_row"){
		inform_abstract_modules_table_row($parameters, $table_module_field);
	} else if ($method == "view_abstract_modules_table_row"){
		view_abstract_modules_table_row($parameters, $table_module_field);
	} else if ($method == "create_sort_abstract_modules"){
		create_sort_abstract_modules($table_module_field);
	} else if ($method == "sort_abstract_modules"){
		sort_abstract_modules($parameters);
	} else if ($method == "get_abstract_modules_edit_log_data"){
		get_abstract_modules_edit_log_data($parameters);
	} else if ($method == "count_abstract_modules_edit_log_data"){
		count_abstract_modules_edit_log_data($parameters);
	} else if ($method == "get_abstract_modules_edit_log_data_by_id"){
		get_abstract_modules_edit_log_data_by_id($parameters);
	} else if ($method == "count_abstract_modules_edit_log_data_by_id"){
		count_abstract_modules_edit_log_data_by_id($parameters);
	}
}

function generate_abstract_modules($target){
	
	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	$sql_modules_target = "
	SELECT `abstract_modules_name`, `abstract_modules_varname` FROM `abstract_modules`
	WHERE `abstract_modules_id` = '" . $target . "'
	AND `abstract_modules_activate` = '1'
	ORDER BY `abstract_modules_order` ASC";
	$result_modules_target = mysqli_query($con, $sql_modules_target);
	if ($result_modules_target) {
		$query_modules_target = mysqli_fetch_array($result_modules_target);
	}

	$sql_modules = "
	SELECT * FROM `abstract_modules`
	WHERE `abstract_modules_activate` = '1'
	ORDER BY `abstract_modules_order` ASC";
	$result_modules = mysqli_query($con, $sql_modules);
	if ($result_modules) {
		$i = 0;
		$queries_modules = array();
		while ($query_modules = mysqli_fetch_array($result_modules)) {
			$queries_modules[$i] = $query_modules;
			$i++;
		}
		$queries_modules_serialized = urlencode(serialize($queries_modules));
	}

	$sql_inputs = "
	SELECT * FROM `abstract_references`
	WHERE `abstract_references_activate` = '1'
	ORDER BY `abstract_references_order` ASC";
	$result_inputs = mysqli_query($con, $sql_inputs);
	if ($result_inputs) {
		$i = 0;
		$queries_inputs = array();
		while ($query_inputs = mysqli_fetch_array($result_inputs)) {
			$queries_inputs[$i] = $query_inputs;
			$i++;
		}
		$queries_inputs_serialized = urlencode(serialize($queries_inputs));
	}

	$sql_dynamic_inputs = "
	SELECT * FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_activate` = '1'
	ORDER BY `abstract_spreadinputs_order` ASC";
	$result_dynamic_inputs = mysqli_query($con, $sql_dynamic_inputs);
	if ($result_dynamic_inputs) {
		$i = 0;
		$queries_dynamic_inputs = array();
		while ($query_dynamic_inputs = mysqli_fetch_array($result_dynamic_inputs)) {
			$queries_dynamic_inputs[$i] = $query_dynamic_inputs;
			$i++;
		}
		$queries_dynamic_inputs_serialized = urlencode(serialize($queries_dynamic_inputs));
	}

	if (!isset($query_modules_target["abstract_modules_varname"]) || empty($query_modules_target["abstract_modules_varname"])) {
		$module_name = get_machinelang($query_modules_target["abstract_modules_name"]);
	} else {
		$module_name = $query_modules_target["abstract_modules_varname"];
	}
	if ($module_name == "configurations" 
		|| $module_name == "modules" 
		|| $module_name == "users" 
		|| $module_name == "groups" 
		|| $module_name == "languages" 
		|| $module_name == "pages" 
		|| $module_name == "media" 
		|| $module_name == "log" 
		|| $module_name == "api" 
		|| $module_name == "code" 
		|| $module_name == "agents" 
		|| $module_name == "notifications" 
		|| $module_name == "messages" 
		|| $module_name == "mail" 
		|| $module_name == "inquiry" 
		|| $module_name == "contacts" 
		|| $module_name == "calendar" 
		|| $module_name == "profile" 
		|| $module_name == "acquintants" 
		|| $module_name == "posts" 
		|| $module_name == "popup" 
		|| $module_name == "forms" 
		|| $module_name == "advertisements") {
		$module_name = $module_name . "_custom";
	}
	
	$sql_database_table_exists = "
	SELECT 1 FROM `" . $module_name . "`
	LIMIT 1";
	$result_database_table_exists = mysqli_query($con, $sql_database_table_exists);
	$result_database_table_exists_serialized = urlencode(serialize($result_database_table_exists));

	$sql_database_tables = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $module_name . "'";
	$result_database_table = mysqli_query($con, $sql_database_tables);
	if ($result_database_table) {
		$i = 0;
		$queries_database_tables = array();
		while ($query_database_tables = mysqli_fetch_assoc($result_database_table)) {
			$queries_database_tables[$i] = strtolower($query_database_tables['COLUMN_NAME']);
			$i++;
		}
		$queries_database_tables_serialized = urlencode(serialize($queries_database_tables));
	}

	$sql_old_module = "
	SELECT * FROM `modules`
	WHERE `modules_id` = '" . $modules_id . "'
	LIMIT 1";
	$result_old_module = mysqli_query($con, $sql_old_module);
	if ($result_old_module) {
		$num_old_module = mysqli_num_rows($result_old_module);
		if ($num_old_module > 0) {
			$module_old = mysqli_fetch_array($result_old_module);
			$module_old_serialized = urlencode(serialize($module_old));
		}
	}

	$sql_order = "
	EXPLAIN 
	SELECT * FROM `modules`";
	$result_order = mysqli_query($con, $sql_order);
	if ($result_order) {
		$modules_all = mysqli_fetch_array($result_order);
		$modules_order = $modules_all["rows"] + 1;
	}

	$configs_serialized = urlencode(serialize($configs));
	$database_access_serialized = urlencode(serialize($database_access));
		
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $configs["project_remote_url"]."/api/core/" . $configs["version"] . "/abstract/provide_abstract_modules_codes",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "target=" . $target . "&debug=true&project_api_key=".$configs["project_api_key"]."&project_api_secret=".$configs["project_api_secret"]."&modules_data=".$queries_modules_serialized."&inputs_data=".$queries_inputs_serialized."&dynamic_inputs_data=".$queries_dynamic_inputs_serialized."&result_database_table_exists_serialized=".$result_database_table_exists_serialized."&queries_database_tables_serialized=".$queries_database_tables_serialized."&module_old_serialized=".$module_old_serialized."&modules_order=".$modules_order."&configs_serialized=".$configs_serialized."&database_access_serialized=".$database_access_serialized."&template=default",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		),
	));
	$response_codes = json_decode(curl_exec($curl));
	curl_close($curl);

	if ($response_codes->status) {

		$error_database = 0;
		
		if (isset($response_codes->sql_clean_old_module) && !empty($response_codes->sql_clean_old_module)) {
			$result_clean_old_module = mysqli_query($con, $response_codes->sql_clean_old_module);
		}
		if (isset($response_codes->sql_module) && !empty($response_codes->sql_module)) {
			$result_module = mysqli_query($con, $response_codes->sql_module);
		}
		if (isset($response_codes->sql_module_drop) && !empty($response_codes->sql_module_drop)) {
			$result_module_drop = mysqli_query($con, $response_codes->sql_module_drop);
		}
		if (isset($response_codes->sql_clean_log) && !empty($response_codes->sql_clean_log)) {
			$result_clean_log = mysqli_query($con, $response_codes->sql_clean_log);
		}
		if (isset($response_codes->sql_modules) && !empty($response_codes->sql_modules)) {
			$result_modules = mysqli_query($con, $response_codes->sql_modules);
		}

		$error_files = 0;

		if (isset($response_codes->module_url) && !empty($response_codes->module_url)) {

			$module_file_path = "../../../" . $response_codes->module_url .".php";
			if (file_exists($module_file_path)) {
                if (chmod($module_file_path, 0777)) {
                    if (!unlink($module_file_path)) {
						$error_files = $error_files + 1;
					}
                } else {
					$error_files = $error_files + 1;
				}
			}
			$module_file = fopen($module_file_path, 'w+');
			fputs($module_file, $response_codes->module_file);
			fclose($module_file);
			if (file_exists($module_file_path)) {
				if (!chmod($module_file_path, 0777)) {
					$error_files = $error_files + 1;
				}
			} else {
				$error_files = $error_files + 1;
			}

			$module_system_file_path = "../../../../system/" . $response_codes->module_url .".php";
			if (file_exists($module_system_file_path)) {
				if (chmod($module_system_file_path, 0777)) {
					if (!unlink($module_system_file_path)) {
						$error_files = $error_files + 1;
					}
                } else {
					$error_files = $error_files + 1;
				}
			}
			$module_system_file = fopen($module_system_file_path, 'w+');
			fputs($module_system_file, $response_codes->module_function_file_frontend);
			fclose($module_system_file);
			if (file_exists($module_system_file_path)) {
				if (!chmod($module_system_file_path, 0777)) {
					$error_files = $error_files + 1;
				}
			}

			$module_backend_system_file_path = "../../../system/" . $response_codes->module_url .".php";
			if (file_exists($module_backend_system_file_path)) {
				if (chmod($module_backend_system_file_path, 0777)) {
                    if (!unlink($module_backend_system_file_path)) {
                        $error_files = $error_files + 1;
                    }
                } else {
					$error_files = $error_files + 1;
				}
			}
			$module_backend_system_file = fopen($module_backend_system_file_path, 'w+');
			fputs($module_backend_system_file, $response_codes->module_function_file_backend);
			fclose($module_backend_system_file);
			if (file_exists($module_backend_system_file_path)) {
				if (!chmod($module_backend_system_file_path, 0777)) {
					$error_files = $error_files + 1;
				}
			}

		} else {

			$error_files = $error_files + 1;

		}

		if ($error_database > 0 || $error_files > 0) {

			if ($error_database > 0 && $error_files > 0) {

				$response["status"] = false;
				$response["target"] = "abstract_modules";
				$response["message"] = translate("Database encountered error") . " " .  translate("and") . " " . translate("Unable to write file");

			} else if ($error_database > 0 && $error_files <= 0) {
				
				$response["status"] = false;
				$response["target"] = "abstract_modules";
				$response["message"] = translate("Database encountered error");

			} else if ($error_database <= 0 && $error_files > 0) {

				$response["status"] = false;
				$response["message"] = translate("Unable to write file");

			}

		} else {

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "generate module",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_modules_id",
					"modules_record_target" => $target,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully generated module") . " <strong>" . $query_modules_target["abstract_modules_name"] . "</strong>";

		}

	} else {

		$response["status"] = $response_codes->status;
		$response["message"] = translate($response_codes->message);

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function regenerate_abstract_modules($target){
	
	/* get global: configurations */
	global $configs;
	global $database_access;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	$sql_modules_target = "
	SELECT `abstract_modules_varname` FROM `abstract_modules`
	WHERE `abstract_modules_id` = '" . $target . "'
	AND `abstract_modules_activate` = '1'
	ORDER BY `abstract_modules_order` ASC";
	$result_modules_target = mysqli_query($con, $sql_modules_target);
	if ($result_modules_target) {
		$query_modules_target = mysqli_fetch_array($result_modules_target);
	}

	$sql_modules = "
	SELECT * FROM `abstract_modules`
	WHERE `abstract_modules_activate` = '1'
	ORDER BY `abstract_modules_order` ASC";
	$result_modules = mysqli_query($con, $sql_modules);
	if ($result_modules) {
		$i = 0;
		$queries_modules = array();
		while ($query_modules = mysqli_fetch_array($result_modules)) {
			$queries_modules[$i] = $query_modules;
			$i++;
		}
		$queries_modules_serialized = urlencode(serialize($queries_modules));
	}

	$sql_inputs = "
	SELECT * FROM `abstract_references`
	WHERE `abstract_references_activate` = '1'
	ORDER BY `abstract_references_order` ASC";
	$result_inputs = mysqli_query($con, $sql_inputs);
	if ($result_inputs) {
		$i = 0;
		$queries_inputs = array();
		while ($query_inputs = mysqli_fetch_array($result_inputs)) {
			$queries_inputs[$i] = $query_inputs;
			$i++;
		}
		$queries_inputs_serialized = urlencode(serialize($queries_inputs));
	}

	$sql_dynamic_inputs = "
	SELECT * FROM `abstract_spreadinputs`
	WHERE `abstract_spreadinputs_activate` = '1'
	ORDER BY `abstract_spreadinputs_order` ASC";
	$result_dynamic_inputs = mysqli_query($con, $sql_dynamic_inputs);
	if ($result_dynamic_inputs) {
		$i = 0;
		$queries_dynamic_inputs = array();
		while ($query_dynamic_inputs = mysqli_fetch_array($result_dynamic_inputs)) {
			$queries_dynamic_inputs[$i] = $query_dynamic_inputs;
			$i++;
		}
		$queries_dynamic_inputs_serialized = urlencode(serialize($queries_dynamic_inputs));
	}

	if (!isset($query_modules_target["abstract_modules_varname"]) || empty($query_modules_target["abstract_modules_varname"])) {
		$module_name = get_machinelang($query_modules_target["abstract_modules_name"]);
	} else {
		$module_name = $query_modules_target["abstract_modules_varname"];
	}
	if ($module_name == "configurations" 
		|| $module_name == "modules" 
		|| $module_name == "users" 
		|| $module_name == "groups" 
		|| $module_name == "languages" 
		|| $module_name == "pages" 
		|| $module_name == "media" 
		|| $module_name == "log" 
		|| $module_name == "api" 
		|| $module_name == "code" 
		|| $module_name == "agents" 
		|| $module_name == "notifications" 
		|| $module_name == "messages" 
		|| $module_name == "mail" 
		|| $module_name == "inquiry" 
		|| $module_name == "contacts" 
		|| $module_name == "calendar" 
		|| $module_name == "profile" 
		|| $module_name == "acquintants" 
		|| $module_name == "posts" 
		|| $module_name == "popup" 
		|| $module_name == "forms" 
		|| $module_name == "advertisements") {
		$module_name = $module_name . "_custom";
	}
	
	$sql_database_table_exists = "
	SELECT 1 FROM `" . $module_name . "`
	LIMIT 1";
	$result_database_table_exists = mysqli_query($con, $sql_database_table_exists);
	$result_database_table_exists_serialized = urlencode(serialize($result_database_table_exists));

	$sql_database_tables = "
	SELECT * from INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = '" . $database_access["name"] . "'
	AND TABLE_NAME = '" . $module_name . "'";
	$result_database_table = mysqli_query($con, $sql_database_tables);
	if ($result_database_table) {
		$i = 0;
		$queries_database_tables = array();
		while ($query_database_tables = mysqli_fetch_assoc($result_database_table)) {
			$queries_database_tables[$i] = strtolower($query_database_tables['COLUMN_NAME']);
			$i++;
		}
		$queries_database_tables_serialized = urlencode(serialize($queries_database_tables));
	}

	$sql_old_module = "
	SELECT * FROM `modules`
	WHERE `modules`.`modules_id` = '" . $modules_id . "'
	LIMIT 1";
	$result_old_module = mysqli_query($con, $sql_old_module);
	if ($result_old_module) {
		$num_old_module = mysqli_num_rows($result_old_module);
		if ($num_old_module > 0) {
			$module_old = mysqli_fetch_array($result_old_module);
			$module_old_serialized = urlencode(serialize($module_old));
		}
	}

	$sql_order = "
	EXPLAIN 
	SELECT * FROM `modules`";
	$result_order = mysqli_query($con, $sql_order);
	if ($result_order) {
		$modules_all = mysqli_fetch_array($result_order);
		$modules_order = $modules_all["rows"] + 1;
	}

	$configs_serialized = urlencode(serialize($configs));
	$database_access_serialized = urlencode(serialize($database_access));
		
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $configs["project_remote_url"]."/api/core/" . $configs["version"] . "/abstract/provide_abstract_modules_codes",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "target=" . $target . "&debug=false&project_api_key=".$configs["project_api_key"]."&project_api_secret=".$configs["project_api_secret"]."&modules_data=".$queries_modules_serialized."&inputs_data=".$queries_inputs_serialized."&dynamic_inputs_data=".$queries_dynamic_inputs_serialized."&result_database_table_exists_serialized=".$result_database_table_exists_serialized."&queries_database_tables_serialized=".$queries_database_tables_serialized."&module_old_serialized=".$module_old_serialized."&modules_order=".$modules_order."&configs_serialized=".$configs_serialized."&database_access_serialized=".$database_access_serialized."&template=default",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		),
	));
	$response_codes = json_decode(curl_exec($curl));
	curl_close($curl);

	if ($response_codes->status) {

		$error_database = 0;
		
		if (isset($response_codes->sql_clean_old_module) && !empty($response_codes->sql_clean_old_module)) {
			$result_clean_old_module = mysqli_query($con, $response_codes->sql_clean_old_module);
		}
		if (isset($response_codes->sql_module) && !empty($response_codes->sql_module)) {
			$result_module = mysqli_query($con, $response_codes->sql_module);
		}
		if (isset($response_codes->sql_module_drop) && !empty($response_codes->sql_module_drop)) {
			$result_module_drop = mysqli_query($con, $response_codes->sql_module_drop);
		}
		if (isset($response_codes->sql_clean_log) && !empty($response_codes->sql_clean_log)) {
			$result_clean_log = mysqli_query($con, $response_codes->sql_clean_log);
		}
		if (isset($response_codes->sql_modules) && !empty($response_codes->sql_modules)) {
			$result_modules = mysqli_query($con, $response_codes->sql_modules);
		}

		$error_files = 0;

		if (isset($response_codes->module_url) && !empty($response_codes->module_url)) {

			$module_file_path = "../../../" . $response_codes->module_url .".php";
			if (file_exists($module_file_path)) {
				// if (!chmod($module_file_path, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of backend module file") . " <strong>".$response_codes->module_url."</strong>";
				// }
			}
			$module_file = fopen($module_file_path, 'w+');
			fputs($module_file, $response_codes->module_file);
			fclose($module_file);
			if (file_exists($module_file_path)) {
				// if (!chmod($module_file_path, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of backend module file") . " <strong>".$response_codes->module_url."</strong>";
				// }
			}

			$module_file_backend_system_folder = "../../../../system/" . $response_codes->module_url .".php";
			if (file_exists($module_file_backend_system_folder)) {
				// if (!chmod($module_file_backend_system_folder, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of module function file") . " <strong>/system/".$response_codes->module_url."</strong>";
				// }
			}
			$module_file_function = fopen($module_file_backend_system_folder, 'w+');
			fputs($module_file_function, $response_codes->module_function_file_frontend);
			fclose($module_file_function);
			if (file_exists($module_file_backend_system_folder)) {
				// if (!chmod($module_file_backend_system_folder, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of module function file") . " <strong>/system/".$response_codes->module_url."</strong>";
				// }
			}

			$module_file_backend_backend_system_folder = "../../../system/" . $response_codes->module_url .".php";
			if (file_exists($module_file_backend_backend_system_folder)) {
				// if (!chmod($module_file_backend_backend_system_folder, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of backend module function file") . " <strong>/system/".$response_codes->module_url."</strong>";
				// }
			}
			$module_file_backend_function = fopen($module_file_backend_backend_system_folder, 'w+');
			fputs($module_file_backend_function, $response_codes->module_function_file_backend);
			fclose($module_file_backend_function);
			if (file_exists($module_file_backend_backend_system_folder)) {
				// if (!chmod($module_file_backend_backend_system_folder, 0777)) {
				// 	$response["status"] = false;
				// 	$response["message"] = translate("Unable to change permission of backend module function file") . " <strong>/system/".$response_codes->module_url."</strong>";
				// }
			}

		} else {

			$error_files = $error_files + 1;

		}

		if ($error_database > 0 || $error_files > 0) {

			if ($error_database > 0 && $error_files > 0) {

				$response["status"] = false;
				$response["target"] = "abstract_modules";
				$response["message"] = translate("Database encountered error") . " " .  translate("and") . " " . translate("Unable to write file");

			} else if ($error_database > 0 && $error_files <= 0) {
				
				$response["status"] = false;
				$response["target"] = "abstract_modules";
				$response["message"] = translate("Database encountered error");

			} else if ($error_database <= 0 && $error_files > 0) {

				$response["status"] = false;
				$response["message"] = translate("Unable to write file");

			}

		} else {

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "reset module",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_modules_id",
					"modules_record_target" => $target,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully reset module");

		}

	} else {

		$response["status"] = $response_codes->status;
		$response["message"] = translate($response_codes->message);

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_modules_data_by_id($target, $activate = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	

	/* database: get data by ID from module "abstract_modules" */
	$sql = "
	SELECT *
	FROM `abstract_modules`
	WHERE `abstract_modules`.`abstract_modules_id` = '" . $target . "'
	".$extra_sql_data_all."
    LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));
			$query["abstract_modules_date_created_formatted"] = datetime_reformat($query["abstract_modules_date_created"]);
			
		if (isset($query["abstract_modules_template"]) && !empty($query["abstract_modules_template"])) {
			$query["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query["abstract_modules_template"];
		}
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get data by ID",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_modules_id",
					"modules_record_target" => $target,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $query;
			$response["message"] = translate("Successfully get data");

			unset($query);
			$query = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_modules_data_all($start = 0, $limit = "", $sort_by = "abstract_modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}
	

	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: get data all from module "abstract_modules" */
	$sql = "
	SELECT *
	FROM   `abstract_modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));
		if (isset($query["abstract_modules_template"]) && !empty($query["abstract_modules_template"])) {
			$query["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query["abstract_modules_template"];
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["values"] = $queries;
			$response["message"] = translate("Successfully get all data");

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_modules_data_all($start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all from module "abstract_modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_modules_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "abstract_modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all excluding from module "abstract_modules" */
	$sql = "
	SELECT *
	FROM `abstract_modules`
	WHERE `abstract_modules_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));
		if (isset($query["abstract_modules_template"]) && !empty($query["abstract_modules_template"])) {
			$query["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query["abstract_modules_template"];
		}

				
				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data excluding",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => $target,
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_modules_id",
					"modules_record_target" => $target,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_name" => $_SESSION["users_name"]
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_modules_data_all_excluding($target, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);
	$filters = escape_string($con, $filters);
	$extended_command = escape_string($con, $extended_command);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all excluding from module "abstract_modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_modules`
	WHERE `abstract_modules_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_modules`
	WHERE `abstract_modules_id` != '" . $target . "'
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_modules_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "abstract_modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get data all for datatable from module "abstract_modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));
		if (isset($query["abstract_modules_template"]) && !empty($query["abstract_modules_template"])) {
			$query["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query["abstract_modules_template"];
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => "",
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_modules_data_table_all($table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}
	
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	/* database: count data all for datatable from module "abstract_modules" */
	if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
	$sql = "
	SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	} else {
	$sql = "
	EXPLAIN SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	}
	$result = mysqli_query($con, $sql);

	if ($result) {

		if ((isset($activate) && !empty($activate)) || (isset($filters) && !empty($filters)) || (isset($extended_command) && !empty($extended_command))) {
		$num = mysqli_num_rows($result);
		} else {
		$query = mysqli_fetch_array($result);
		if (isset($query["rows"]) && !empty($query["rows"])) {
			$num = $query["rows"];
		} else {
			$num = 0;
		}
		}

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_search_abstract_modules_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "abstract_modules_order", $sort_direction = "asc", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'abstract_modules'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "abstract_modules_actions" 
				&& $key != "abstract_modules_date_created" 
				&& $key != "abstract_modules_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: get search data all for datatable from module "abstract_modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));
		if (isset($query["abstract_modules_template"]) && !empty($query["abstract_modules_template"])) {
			$query["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query["abstract_modules_template"];
		}

				

				foreach ($query as $key){
					if (!in_array($key, $table_module_field)) {
						unset($key);
					}
				}

				
				

				$queries[$i] = $query;
				$i++;

			}

			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "get search all data for table",
					"log_function" => __FUNCTION__,
					"log_violation" => "low",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "",
					"modules_record_target" => $search,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_search_abstract_modules_data_table_all($search, $table_module_field, $start = 0, $limit = "", $sort_by = "", $sort_direction = "", $activate = "", $filters = "", $extended_command = ""){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$search = escape_string($con, $search);
	$table_module_field = escape_string($con, $table_module_field);
	$start = escape_string($con, $start);
	$limit = escape_string($con, $limit);
	$sort_by = escape_string($con, $sort_by);
	$sort_direction = escape_string($con, $sort_direction);

	/* database: add extended command */
	$extra_sql_data_all = "";
	if (isset($activate)) {
		if ($activate == "1") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '1' ";
		} else if ($activate == "0") {
			$extra_sql_data_all .= "AND `abstract_modules_activate` = '0' ";
		}
	}
	if (isset($filters) && $filters != "" && count($filters) > 0) {
		foreach ($filters as $key => $value) {
			if (isset($value)) {
				$extra_sql_data_all .= "AND `".$key."` = '".$value."' ";
			}
		}
	}
	if (isset($extended_command) && !empty($extended_command)) {
		$extra_sql_data_all .= " AND (";
		for($i = 0; $i < count($extended_command); $i++) {
			if (isset($extended_command[$i]["value"])) {
				$extra_sql_data_all .= $extended_command[$i]["conjunction"]." `".$extended_command[$i]["key"]."` ".$extended_command[$i]["operator"]." '".$extended_command[$i]["value"]."' ";
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (count($table_module_field) > 0) {
		$extra_sql_data_all .= "AND (";
		$i = 0;
		foreach ($table_module_field as $key => $value) {
			$sql_check_field_type = "
			SELECT DATA_TYPE 
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE  TABLE_NAME = 'abstract_modules'
			   AND COLUMN_NAME = '".$key."'
			LIMIT 1";
			$result_check_field_type = mysqli_query($con, $sql_check_field_type);
			$query_check_field_type = mysqli_fetch_array($result_check_field_type);
			if ($key != "abstract_modules_actions" 
				&& $key != "abstract_modules_date_created" 
				&& $key != "abstract_modules_activate"  
				&& $query_check_field_type["DATA_TYPE"] != "datetime"
				&& $value != "" && $key != "") {
				if ($i == 0) {
					$extra_sql_data_all .= "`" . $key . "` LIKE '%" . $search . "%' ";
				} else {
					$extra_sql_data_all .= "OR `" . $key . "` LIKE '%" . $search . "%' ";
				}
				$i = $i + 1;
			}
			unset($query_check_field_type);
			$query_check_field_type = array();
			if ($result_check_field_type) {
				mysqli_free_result($result_check_field_type);
			}
		}
		$extra_sql_data_all .= ") ";
	}
	if (isset($sort_by) && !empty($sort_by)) {
		$extra_sql_data_all .= "ORDER BY `".$sort_by."` ";
		if (isset($sort_direction) && !empty($sort_direction)) {
			$extra_sql_data_all .= $sort_direction." ";
		} else {
			$extra_sql_data_all .= "asc ";
		}
	}
	if (isset($limit) && $limit != "") {
		if (isset($start) && $start != "" && !empty($limit)) {
			$extra_sql_data_all .= "LIMIT ".$start.", ".$limit;
		} else {
			$extra_sql_data_all .= "LIMIT ".$limit;
		}
	}

	/* database: count search data all for datatable from module "abstract_modules" */
	if ($extra_sql_data_all != "" && !stristr($extra_sql_data_all, "WHERE")) {
		$extra_sql_data_all = "WHERE `abstract_modules_id` != '0' ".$extra_sql_data_all;
	}

	$sql = "
	SELECT *
	FROM `abstract_modules`
	".$extra_sql_data_all.";";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		$response["status"] = true;
		$response["values"] = $num;
		$response["message"] = translate("Successfully count all data");

		unset($query);
		$query = array();
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_abstract_modules_data($parameters){

	/* get global: configurations */
	global $configs;
	$abstract_modules_configs = get_modules_data_by_id("1");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["abstract_modules_activate"] == "1") {
	
		if(!isset($parameters["abstract_modules_name"]) || empty($parameters["abstract_modules_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_modules_name"]) > 80 && $parameters["abstract_modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)");
				$response["target"] = "#abstract_modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_modules_name"]) > 80 && $parameters["abstract_modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["abstract_modules_name"]) && $parameters["abstract_modules_name"] != "") {
			$sql_check_unique_abstract_modules_name = "
			SELECT `abstract_modules_id` FROM `abstract_modules` 
			WHERE `abstract_modules_name` LIKE '".$parameters["abstract_modules_name"]."' 
			
			LIMIT 1";
			$result_check_unique_abstract_modules_name = mysqli_query($con, $sql_check_unique_abstract_modules_name);
			$num_check_unique_abstract_modules_name = mysqli_num_rows($result_check_unique_abstract_modules_name);
			if ($num_check_unique_abstract_modules_name > 0) {
				
				$response["status"] = false;
				$response["target"] = "#abstract_modules_name";
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_modules_varname"]) > 80 && $parameters["abstract_modules_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)");
				$response["target"] = "#abstract_modules_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_modules_varname"]) > 80 && $parameters["abstract_modules_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_modules_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["abstract_modules_varname"]) && $parameters["abstract_modules_varname"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#abstract_modules_varname";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["abstract_modules_varname"]) && $parameters["abstract_modules_varname"] != "") {
			$sql_check_unique_abstract_modules_varname = "
			SELECT `abstract_modules_id` FROM `abstract_modules` 
			WHERE `abstract_modules_varname` LIKE '".$parameters["abstract_modules_varname"]."' 
			
			LIMIT 1";
			$result_check_unique_abstract_modules_varname = mysqli_query($con, $sql_check_unique_abstract_modules_varname);
			$num_check_unique_abstract_modules_varname = mysqli_num_rows($result_check_unique_abstract_modules_varname);
			if ($num_check_unique_abstract_modules_varname > 0) {
				
				$response["status"] = false;
				$response["target"] = "#abstract_modules_varname";
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["abstract_modules_form_method"]) || empty($parameters["abstract_modules_form_method"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Backend Form Method") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_form_method";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_modules_database_engine"]) || empty($parameters["abstract_modules_database_engine"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Engine") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_database_engine";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}

	}

	/* initialize: default */
	date_default_timezone_set($configs["timezone"]);
	$parameters["abstract_modules_date_created"] = gmdate("Y-m-d H:i:s");

	/* initialize: user data */
	if ($_SESSION["users_id"] != "") {
		$users_id = $_SESSION["users_id"];
		$users_username = $_SESSION["users_username"];
		$users_name = $_SESSION["users_name"];
		$users_last_name = $_SESSION["users_last_name"];
		$parameters["users_id"] = $users_id;
		$parameters["users_username"] = $users_username;
		$parameters["users_name"] = $users_name;
		$parameters["users_last_name"] = $users_last_name;
	} else {
		if (isset($parameters["users_id"]) && $parameters["users_id"] != "") {
			$users_id = $parameters["users_id"];
			$users_username = $parameters["users_username"];
			$users_name = $parameters["users_name"];
			$users_last_name = $parameters["users_last_name"];
		} else {
			$users_id = "";
			$users_username = "";
			$users_name = "";
			$users_last_name = "";
		}
	}

	/* initialize: prepare data */

	/* database: insert to module "abstract_modules" (begin) */
	$sql = "INSERT INTO `abstract_modules` (
				`abstract_modules_id`,
				`abstract_modules_name`,
				`abstract_modules_varname`,
				`abstract_modules_description`,
				`abstract_modules_form_method`,
				`abstract_modules_component_modules`,
				`abstract_modules_component_groups`,
				`abstract_modules_component_users`,
				`abstract_modules_component_languages`,
				`abstract_modules_component_pages`,
				`abstract_modules_component_media`,
				`abstract_modules_component_commerce`,
				`abstract_modules_database_engine`,
				`abstract_modules_data_sortable`,
				`abstract_modules_data_addable`,
				`abstract_modules_data_viewonly`,
				`abstract_modules_template`,
				`abstract_modules_icon`,
				`abstract_modules_category`,
				`abstract_modules_subject`,
				`abstract_modules_subject_icon`,
				`abstract_modules_order`,
				`abstract_modules_date_created`,
				`abstract_modules_activate`,
				`users_id`,
				`users_username`,
				`users_name`,
				`users_last_name`)
			VALUES (
				NULL,
				'" . $parameters["abstract_modules_name"] . "',
				'" . $parameters["abstract_modules_varname"] . "',
				'" . $parameters["abstract_modules_description"] . "',
				'" . $parameters["abstract_modules_form_method"] . "',
				'" . $parameters["abstract_modules_component_modules"] . "',
				'" . $parameters["abstract_modules_component_groups"] . "',
				'" . $parameters["abstract_modules_component_users"] . "',
				'" . $parameters["abstract_modules_component_languages"] . "',
				'" . $parameters["abstract_modules_component_pages"] . "',
				'" . $parameters["abstract_modules_component_media"] . "',
				'" . $parameters["abstract_modules_component_commerce"] . "',
				'" . $parameters["abstract_modules_database_engine"] . "',
				'" . $parameters["abstract_modules_data_sortable"] . "',
				'" . $parameters["abstract_modules_data_addable"] . "',
				'" . $parameters["abstract_modules_data_viewonly"] . "',
				'" . $parameters["abstract_modules_template"] . "',
				'" . $parameters["abstract_modules_icon"] . "',
				'" . $parameters["abstract_modules_category"] . "',
				'" . $parameters["abstract_modules_subject"] . "',
				'" . $parameters["abstract_modules_subject_icon"] . "',
				'" . $parameters["abstract_modules_order"] . "',
				'" . $parameters["abstract_modules_date_created"] . "',
				'" . $parameters["abstract_modules_activate"] . "',
				'" . $users_id . "',
				'" . $users_username . "',
				'" . $users_name . "',
				'" . $users_last_name . "')";
	$result = mysqli_query($con, $sql);
	$parameters["abstract_modules_id"] = mysqli_insert_id($con);
	/* database: insert to module "abstract_modules" (end) */

	if ($result) {
		

		/* response: additional data */
		

		/* log (begin) */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "create data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_modules_id",
				"modules_record_target" => $parameters["abstract_modules_id"],
				"modules_id" => "1",
				"modules_name" => "Abstract Modules",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully created data");
		$response["values"] = $parameters;

		unset($parameters);
		$parameters = array();

	} else {
		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function update_abstract_modules_data($parameters){

	/* get global: configurations */
	global $configs;
	$abstract_modules_configs = get_modules_data_by_id("1");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	if ($parameters["abstract_modules_activate"] == "1") {
	
		if(!isset($parameters["abstract_modules_name"]) || empty($parameters["abstract_modules_name"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_name";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_modules_name"]) > 80 && $parameters["abstract_modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)");
				$response["target"] = "#abstract_modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_modules_name"]) > 80 && $parameters["abstract_modules_name"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_modules_name";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (isset($parameters["abstract_modules_name"]) && $parameters["abstract_modules_name"] != "") {
			$sql_check_unique_abstract_modules_name = "
			SELECT `abstract_modules_id` FROM `abstract_modules` 
			WHERE `abstract_modules_name` LIKE '".$parameters["abstract_modules_name"]."' 
			AND `abstract_modules_id` != '".$parameters["abstract_modules_id"]."'
			LIMIT 1";
			$result_check_unique_abstract_modules_name = mysqli_query($con, $sql_check_unique_abstract_modules_name);
			$num_check_unique_abstract_modules_name = mysqli_num_rows($result_check_unique_abstract_modules_name);
			if ($num_check_unique_abstract_modules_name > 0) {
				
				$response["status"] = false;
				$response["target"] = "#abstract_modules_name";
				$response["message"] = "<strong>" . translate("Module Name") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if (function_exists("mb_strlen")) {
			if(mb_strlen($parameters["abstract_modules_varname"]) > 80 && $parameters["abstract_modules_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)");
				$response["target"] = "#abstract_modules_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		} else {
			if(strlen($parameters["abstract_modules_varname"]) > 80 && $parameters["abstract_modules_varname"] != ""){

				$response["status"] = false;
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("is longer than maximum length of strings at") . " 80 " . translate("character(s)") . "<br />(" .  translate("Please install and enable php_mbstring.dll extension in PHP server for accuaracy of using unicode language") . ")";
				$response["target"] = "#abstract_modules_varname";
				$response["values"] = $parameters;
				unset($parameters);
				$parameters = array();
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!preg_match("/[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\._\-]+$/", $parameters["abstract_modules_varname"]) && $parameters["abstract_modules_varname"] != ""){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("does not allow special characters and white spaces");
			$response["target"] = "#abstract_modules_varname";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if (isset($parameters["abstract_modules_varname"]) && $parameters["abstract_modules_varname"] != "") {
			$sql_check_unique_abstract_modules_varname = "
			SELECT `abstract_modules_id` FROM `abstract_modules` 
			WHERE `abstract_modules_varname` LIKE '".$parameters["abstract_modules_varname"]."' 
			AND `abstract_modules_id` != '".$parameters["abstract_modules_id"]."'
			LIMIT 1";
			$result_check_unique_abstract_modules_varname = mysqli_query($con, $sql_check_unique_abstract_modules_varname);
			$num_check_unique_abstract_modules_varname = mysqli_num_rows($result_check_unique_abstract_modules_varname);
			if ($num_check_unique_abstract_modules_varname > 0) {
				
				$response["status"] = false;
				$response["target"] = "#abstract_modules_varname";
				$response["message"] = "<strong>" . translate("Module Variable Name") . "</strong> " . translate("can not be duplicated");
				$response["values"] = $parameters;
				if (isset($method) && $method == __FUNCTION__) {
					echo json_encode($response);
				} else {
					return $response["status"];
				}
				unset($response);
				$response = array();
				unset($method);
				exit();

			}
		}
		if(!isset($parameters["abstract_modules_form_method"]) || empty($parameters["abstract_modules_form_method"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Backend Form Method") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_form_method";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
		if(!isset($parameters["abstract_modules_database_engine"]) || empty($parameters["abstract_modules_database_engine"])){

			$response["status"] = false;
			$response["message"] = "<strong>" . translate("Database Engine") . "</strong> " . translate("is required");
			$response["target"] = "#abstract_modules_database_engine";
			$response["values"] = $parameters;
			unset($parameters);
			$parameters = array();
			if (isset($method) && $method == __FUNCTION__) {
				echo json_encode($response);
			} else {
				return $response["status"];
			}
			unset($response);
			$response = array();
			unset($method);
			exit();

		}
	}

	/* database: get old data from module "abstract_modules" (begin) */
	$sql = "SELECT *
	        FROM   `abstract_modules`
			WHERE  `abstract_modules_id` = '" . $parameters["abstract_modules_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_modules_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "abstract_modules" (end) */
	
	/* initialize: default */
	
	$parameters["abstract_modules_date_created"] = $query["abstract_modules_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];
	/* database: update to module "abstract_modules" (begin) */
	$sql = "UPDATE `abstract_modules`
			SET 
				   `abstract_modules_name` = '" . $parameters["abstract_modules_name"] . "',
				   `abstract_modules_varname` = '" . $parameters["abstract_modules_varname"] . "',
				   `abstract_modules_description` = '" . $parameters["abstract_modules_description"] . "',
				   `abstract_modules_form_method` = '" . $parameters["abstract_modules_form_method"] . "',
				   `abstract_modules_component_modules` = '" . $parameters["abstract_modules_component_modules"] . "',
				   `abstract_modules_component_groups` = '" . $parameters["abstract_modules_component_groups"] . "',
				   `abstract_modules_component_users` = '" . $parameters["abstract_modules_component_users"] . "',
				   `abstract_modules_component_languages` = '" . $parameters["abstract_modules_component_languages"] . "',
				   `abstract_modules_component_pages` = '" . $parameters["abstract_modules_component_pages"] . "',
				   `abstract_modules_component_media` = '" . $parameters["abstract_modules_component_media"] . "',
				   `abstract_modules_component_commerce` = '" . $parameters["abstract_modules_component_commerce"] . "',
				   `abstract_modules_database_engine` = '" . $parameters["abstract_modules_database_engine"] . "',
				   `abstract_modules_data_sortable` = '" . $parameters["abstract_modules_data_sortable"] . "',
				   `abstract_modules_data_addable` = '" . $parameters["abstract_modules_data_addable"] . "',
				   `abstract_modules_data_viewonly` = '" . $parameters["abstract_modules_data_viewonly"] . "',
				   `abstract_modules_template` = '" . $parameters["abstract_modules_template"] . "',
				   `abstract_modules_icon` = '" . $parameters["abstract_modules_icon"] . "',
				   `abstract_modules_category` = '" . $parameters["abstract_modules_category"] . "',
				   `abstract_modules_subject` = '" . $parameters["abstract_modules_subject"] . "',
				   `abstract_modules_subject_icon` = '" . $parameters["abstract_modules_subject_icon"] . "',
				   `abstract_modules_order` = '" . $parameters["abstract_modules_order"] . "',
				   `abstract_modules_date_created` = '" . $parameters["abstract_modules_date_created"] . "',
				   `abstract_modules_activate` = '" . $parameters["abstract_modules_activate"] . "'
			WHERE  `abstract_modules_id` = '" . $parameters["abstract_modules_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "abstract_modules" (end) */

	if ($result) {
		
		/* response: additional data */
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_modules_id",
				"modules_record_target" => $parameters["abstract_modules_id"],
				"modules_id" => "1",
				"modules_name" => "Abstract Modules",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {
	

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "abstract_modules";
		$response["values"] = $parameters;

	}
	
	/* pages handler (end) */

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function patch_abstract_modules_data($parameters){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);

	/* validate data */
	
	/* database: get old data from module "abstract_modules" (begin) */
	$sql = "SELECT *
	        FROM   `abstract_modules`
			WHERE  `abstract_modules_id` = '" . $parameters["abstract_modules_id"] . "'
			LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_modules_old"] = $query;
	mysqli_free_result($result);
	/* database: get old data from module "abstract_modules" (end) */
	
	/* initialize: default */
	$parameters["abstract_modules_date_created"] = $query["abstract_modules_date_created"];
	$parameters["users_id"] = $query["users_id"];
	$parameters["users_username"] = $query["users_username"];
	$parameters["users_name"] = $query["users_name"];
	$parameters["users_last_name"] = $query["users_last_name"];

	/* database: update to module "abstract_modules" (begin) */
	$sql = "UPDATE `abstract_modules`
			SET `" . $parameters["target_field"] . "` = '" . $parameters["value"] . "'
			WHERE  `abstract_modules_id` = '" . $parameters["abstract_modules_id"] . "'";
	$result = mysqli_query($con, $sql);
	/* database: update to module "abstract_modules" (end) */

	if ($result) {
		

		/* log */
		if ($configs["backend_log"]) {
			$log = array(
				"log_name" => "update data",
				"log_function" => __FUNCTION__,
				"log_violation" => "normal",
				"log_content_hash" => serialize($parameters),
				"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
				"log_type" => "backend",
				"log_ip" => $_SERVER["REMOTE_ADDR"],
				"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
				"log_date_created" => gmdate("Y-m-d H:i:s"),
				"modules_record_key" => "abstract_modules_id",
				"modules_record_target" => $parameters["abstract_modules_id"],
				"modules_id" => "1",
				"modules_name" => "Abstract Modules",
				"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
				"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
				"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
				"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
			);
			create_log_data($log);
			unset($log);
			$log = array();
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully updated data");
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["target"] = "abstract_modules";
		$response["values"] = $parameters;

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function delete_abstract_modules_data($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* initialize: old data */
	$sql = "
	SELECT *
	FROM `abstract_modules`
	WHERE `abstract_modules_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_array($result);
	$parameters["abstract_modules_old"] = $query;
	mysqli_free_result($result);
	unset($query);
	$query = array();

	if ($result) {
	
		/* database: delete from module "abstract_modules" (begin) */
		$sql = "
		DELETE FROM `abstract_modules`
		WHERE `abstract_modules_id` = '" . $target . "'";
		$result = mysqli_query($con, $sql);
		/* database: delete from module "abstract_modules" (end) */

		if ($result) {
			/* log */
			if ($configs["backend_log"]) {
				$log = array(
					"log_name" => "delete data",
					"log_function" => __FUNCTION__,
					"log_violation" => "risk",
					"log_content_hash" => "",
					"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
					"log_type" => "backend",
					"log_ip" => $_SERVER["REMOTE_ADDR"],
					"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
					"log_date_created" => gmdate("Y-m-d H:i:s"),
					"modules_record_key" => "abstract_modules_id",
					"modules_record_target" => $target,
					"modules_id" => "1",
					"modules_name" => "Abstract Modules",
					"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
					"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
					"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
					"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
				);
				create_log_data($log);
				unset($log);
				$log = array();
			}

			$response["status"] = true;
			$response["message"] = translate("Successfully deleted data");
		    $response["values"] = $parameters;

			unset($parameters);
			$parameters = array();

		} else {

			$response["status"] = false;
			if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
				$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
			} else {
				$response["message"] = translate("Database encountered error");
			}
		    $response["values"] = $parameters;
			unset($parameters);
			$parameters = array();

		}

	} else {

		$response["status"] = false;
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}
		$response["values"] = $parameters;
		unset($parameters);
		$parameters = array();

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function create_sort_abstract_modules($table_module_field) {
	
	/* get global: configurations */
	global $configs;
	$abstract_modules_configs = get_modules_data_by_id("1");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$table_module_field = escape_string($con, $table_module_field);

	
	$table_module_field_unassoc = array_keys($table_module_field);
	$table_module_field_key = "abstract_modules_title";
	for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
		if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
			$table_module_field_key = $table_module_field_unassoc[$j];
			break;
		}
	}

	$data_table = get_abstract_modules_data_all("", "", "abstract_modules_order", "asc");
	
	
	$html = '';

	for($i = 0; $i < count($data_table); $i++){
		if ($data_table[$i]["abstract_modules_id"] > 0) {

			$table_row_value = render_abstract_modules_table_data($data_table[$i][$table_module_field_key]).'<br />('.$data_table[$i][$table_module_field_key].')';

			$html .= '<li id="list_'.$data_table[$i]["abstract_modules_id"].'" class="mjs-nestedSortable" style="display: list-item;">
						<div class="sort-item">
							<span>
								<div style="display: inline-table">
									<i class="si si-cursor-move"></i>
								</div>
								<div style="display: inline-table">
									'.$table_row_value.' ('.translate("ID").': '.$data_table[$i]["abstract_modules_id"].')
								</div>
							</span>
						</div>
					  </li>';

		}

	}
	
	$response["status"] = true;
	$response["message"] = translate("Successfully created sort");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function sort_abstract_modules($parameters) {
	
	/* get global: configurations */
	global $configs;
	$abstract_modules_configs = get_modules_data_by_id("1");

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$parameters = escape_string($con, $parameters);
	
	$target_next = 1;
	
	for ($i = 0; $i <= count($parameters); $i++) {
		$target_item_id = "";
		for($s = 0; $s <= count($parameters); $s++) {
			if ($parameters[$s]["left"] == $target_next) {
				$target_item_id = $parameters[$s]["item_id"];
				$target_depth = $parameters[$s]["depth"];
				if ($target_depth == 1) {
					$target_next = $parameters[$s]["right"] + 1;
				} else {
					$target_next = $target_next + 1;
				}
				break;
			}
		}
		
		if ($target_depth != "0" && $target_depth == 1 && !empty($target_item_id)) {
			
			/* database: update to module "abstract_modules" (begin) */
			$sql = "UPDATE `abstract_modules`
					SET    `abstract_modules_order` = '" . $i . "'
					WHERE  `abstract_modules_id` = '" . $target_item_id . "'";
			$result = mysqli_query($con, $sql);
			/* database: update to module "abstract_modules" (end) */
			
		}
		
	}
		
	/* log */
	if ($configs["backend_log"]) {
		$log = array(
			"log_name" => "sort data",
			"log_function" => __FUNCTION__,
			"log_violation" => "normal",
			"log_content_hash" => serialize($parameters),
			"log_link" => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
			"log_type" => "backend",
			"log_ip" => $_SERVER["REMOTE_ADDR"],
			"log_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"log_date_created" => gmdate("Y-m-d H:i:s"),
			"modules_record_key" => "",
			"modules_record_target" => "",
			"modules_id" => "1",
			"modules_name" => "Abstract Modules",
			"users_id" => isset($_SESSION["users_id"]) ? $_SESSION["users_id"] : "",
			"users_username" => isset($_SESSION["users_username"]) ? $_SESSION["users_username"] : "",
			"users_name" => isset($_SESSION["users_name"]) ? $_SESSION["users_name"] : "",
			"users_last_name" => isset($_SESSION["users_last_name"]) ? $_SESSION["users_last_name"] : ""
		);
		create_log_data($log);
		unset($log);
		$log = array();
	}
		
	$response["status"] = true;
	$response["message"] = translate("Successfully sorted data");
	$response["values"] = $parameters;
	unset($parameters);
	$parameters = array();
	
	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["status"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function create_abstract_modules_table($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html_open = '<table id="datatable" class="table table-bordered table-striped js-dataTable-full"><thead><tr>';
	$table_module_field_count = count($table_module_field);
	$i = 0;
	$table_module_field_true_count = 0;

	foreach ($table_module_field as $key => $value) {
	
		if ($value == "true" || $value === true) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Variable Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Description")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Backend Form Method")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Modules Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Groups Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Users Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Languages Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Pages Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Media Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Commerce Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Database Engine")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Sortable Data")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Addable Data")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Data is View Only")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Front-end Inner Pages Default Template")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Icon")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Category")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Subject")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Subject Icon")));
			} else {
				$column_name = str_replace("abstract_modules", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($i >= 2) {
				$th_hidden_xs_class = "hidden-xs";
			} else {
				$th_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "abstract_modules_activate") {
				$th_hidden_sm_class = "hidden-sm";
			} else {
				$th_hidden_sm_class = "";

			}

			if ($i >= 4 && $key != "abstract_modules_activate") {
				$th_hidden_md_class = "hidden-md";
			} else {
				$th_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "abstract_modules_activate") {
				$th_hidden_lg_class = "hidden-lg";
			} else {
				$th_hidden_lg_class = "";
			}
			
			
			if ($key == "abstract_modules_id") {
				$html_open .= '<th class="text-center hidden-xs hidden-sm" data-target="'.$key.'">' . translate("ID") . '</th>';
			} else if ($key == "abstract_modules_name") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_name . '" data-toggle="tooltip" title="Module Name" data-original-title="Module Name">' . htmlspecialchars(strip_tags(translate("Module Name"))) . '</th>';
			} else if ($key == "abstract_modules_varname") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_varname . '" data-toggle="tooltip" title="Module Variable Name" data-original-title="Module Variable Name">' . htmlspecialchars(strip_tags(translate("Module Variable Name"))) . '</th>';
			} else if ($key == "abstract_modules_description") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_description . '" data-toggle="tooltip" title="Module Description" data-original-title="Module Description">' . htmlspecialchars(strip_tags(translate("Module Description"))) . '</th>';
			} else if ($key == "abstract_modules_form_method") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_form_method . '" data-toggle="tooltip" title="Backend Form Method" data-original-title="Backend Form Method">' . htmlspecialchars(strip_tags(translate("Backend Form Method"))) . '</th>';
			} else if ($key == "abstract_modules_component_modules") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_modules . '" data-toggle="tooltip" title="Modules Component" data-original-title="Modules Component">' . htmlspecialchars(strip_tags(translate("Modules Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_groups") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_groups . '" data-toggle="tooltip" title="Groups Component" data-original-title="Groups Component">' . htmlspecialchars(strip_tags(translate("Groups Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_users") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_users . '" data-toggle="tooltip" title="Users Component" data-original-title="Users Component">' . htmlspecialchars(strip_tags(translate("Users Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_languages") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_languages . '" data-toggle="tooltip" title="Languages Component" data-original-title="Languages Component">' . htmlspecialchars(strip_tags(translate("Languages Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_pages") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_pages . '" data-toggle="tooltip" title="Pages Component" data-original-title="Pages Component">' . htmlspecialchars(strip_tags(translate("Pages Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_media") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_media . '" data-toggle="tooltip" title="Media Component" data-original-title="Media Component">' . htmlspecialchars(strip_tags(translate("Media Component"))) . '</th>';
			} else if ($key == "abstract_modules_component_commerce") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_component_commerce . '" data-toggle="tooltip" title="Commerce Component" data-original-title="Commerce Component">' . htmlspecialchars(strip_tags(translate("Commerce Component"))) . '</th>';
			} else if ($key == "abstract_modules_database_engine") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_database_engine . '" data-toggle="tooltip" title="Database Engine" data-original-title="Database Engine">' . htmlspecialchars(strip_tags(translate("Database Engine"))) . '</th>';
			} else if ($key == "abstract_modules_data_sortable") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_data_sortable . '" data-toggle="tooltip" title="Sortable Data" data-original-title="Sortable Data">' . htmlspecialchars(strip_tags(translate("Sortable Data"))) . '</th>';
			} else if ($key == "abstract_modules_data_addable") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_data_addable . '" data-toggle="tooltip" title="Addable Data" data-original-title="Addable Data">' . htmlspecialchars(strip_tags(translate("Addable Data"))) . '</th>';
			} else if ($key == "abstract_modules_data_viewonly") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_data_viewonly . '" data-toggle="tooltip" title="Data is View Only" data-original-title="Data is View Only">' . htmlspecialchars(strip_tags(translate("Data is View Only"))) . '</th>';
			} else if ($key == "abstract_modules_template") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_template . '" data-toggle="tooltip" title="Front-end Inner Pages Default Template" data-original-title="Front-end Inner Pages Default Template">' . htmlspecialchars(strip_tags(translate("Front-end Inner Pages Default Template"))) . '</th>';
			} else if ($key == "abstract_modules_icon") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_icon . '" data-toggle="tooltip" title="Module Icon" data-original-title="Module Icon">' . htmlspecialchars(strip_tags(translate("Module Icon"))) . '</th>';
			} else if ($key == "abstract_modules_category") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_category . '" data-toggle="tooltip" title="Module Category" data-original-title="Module Category">' . htmlspecialchars(strip_tags(translate("Module Category"))) . '</th>';
			} else if ($key == "abstract_modules_subject") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_subject . '" data-toggle="tooltip" title="Module Subject" data-original-title="Module Subject">' . htmlspecialchars(strip_tags(translate("Module Subject"))) . '</th>';
			} else if ($key == "abstract_modules_subject_icon") {
                $html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="' . abstract_modules_subject_icon . '" data-toggle="tooltip" title="Module Subject Icon" data-original-title="Module Subject Icon">' . htmlspecialchars(strip_tags(translate("Module Subject Icon"))) . '</th>';
			} else if ($key == "abstract_modules_activate") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Status") . '</th>';
			} else if ($key == "users_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("User") . '</th>';
			} else if ($key == "modules_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Module") . '</th>';
			} else if ($key == "languages_id") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language ID") . '</th>';
			} else if ($key == "languages_short_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language") . '</th>';
			} else if ($key == "languages_name") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Language (Full)") . '</th>';
			} else if ($key == "pages_link") {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate("Link") . '</th>';
			} else if ($key == "abstract_modules_actions") {
				$html_open .= '<th class="text-center" data-target="'.$key.'">' . translate("Actions") . '</th>';
			} else {
				$html_open .= '<th class="text-center '.$th_hidden_xs_class.' '.$th_hidden_sm_class.' '.$th_hidden_md_class.' '.$th_hidden_lg_class.'" data-target="'.$key.'">' . translate($column_name) . '</th>';
			}
			$table_module_field_true_count = $table_module_field_true_count + 1;
			$i++;
		}

	}

	$html_open .= '</tr></thead><tbody id="datatable-list">';

	for($i=0;$i<count($data_table);$i++){

		$data_table_row = $data_table[$i];

		$html .= '<tr id="datatable-'.$data_table_row["abstract_modules_id"].'" data-target="'.$data_table_row["abstract_modules_id"].'">';

		$html .= inform_abstract_modules_table_row($data_table_row, $table_module_field);

		$html .= '</tr>';

		unset($data_table_row);
		$data_table_row = array();

    }
	$html_close = '</tbody></table>';

	$html = $html_open.$html.$html_close;

	$response["status"] = true;
	$response["message"] = translate("Successfully created table");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function prepare_abstract_modules_table_defer($table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	$page_current = escape_string($con, trim($_GET["page"]));
	if (!isset($page_current) || empty($page_current)) {
		$page_current = 1;
	}
	$page_limit = escape_string($con, trim($_GET["limit"]));
	if (!isset($page_limit) || empty($page_limit)) {
		$page_limit = $configs["datatable_page_limit"];
	}
	$sort_by = escape_string($con, trim($_GET["sortby"]));
	if (!isset($sort_by) || empty($sort_by)) {
		$sort_by = "abstract_modules_id";
	}
	$sort_direction = escape_string($con, trim($_GET["sortdirection"]));
	if (!isset($sort_direction) || empty($sort_direction)) {
		if ($sort_by == "abstract_modules_id") {
			$sort_direction = "desc";
		} else {
			$sort_direction = "asc";
		}
	}
	if ($_GET["published"] == "1") {
		$activate = 1;
	} else if ($_GET["published"] == "0") {
		$activate = 0;
	} else {
		$activate = "";
	}
	$filter_date_from = escape_string($con, trim($_GET["from"]));
	if (!isset($filter_date_from) || empty($filter_date_from)) {
		$filter_date_from = "";
	}
	$filter_date_to = escape_string($con, trim($_GET["to"]));
	if (!isset($filter_date_to) || empty($filter_date_to)) {
		$filter_date_to = "";
	}
	if (isset($filter_date_from) && !empty($filter_date_from)
		&& isset($filter_date_to) && !empty($filter_date_to)) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_modules_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
			array(
				"conjunction" => "AND",
				"key" => "abstract_modules_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else if (isset($filter_date_from) && !empty($filter_date_from)
		&& (!isset($filter_date_to) || empty($filter_date_to))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_modules_date_created",
				"operator" => ">=",
				"value" => $filter_date_from,
			),
		);
	} else if (isset($filter_date_to) && !empty($filter_date_to)
		&& (!isset($filter_date_from) || empty($filter_date_from))) {
		$extended_command = array(
			array(
				"conjunction" => "",
				"key" => "abstract_modules_date_created",
				"operator" => "<=",
				"value" => $filter_date_to,
			),
		);
	} else {
		$extended_command = "";
	}
	$search = escape_string($con, trim($_GET["search"]));
	if ($_SESSION["users_level"] == "4") {
		$writers_filters = array(
			"users_id" => $_SESSION["users_id"]
		);
		$count_all_true = count_abstract_modules_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
	} else {
		$count_all_true = count_abstract_modules_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
	}
	if ($search != "") {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$count_all = count_search_abstract_modules_data_table_all($search, $table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$count_all = count_search_abstract_modules_data_table_all($search, $table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	} else {
		$count_all = $count_all_true;
	}

	if ($count_all_true > $configs["datatable_data_limit"]) {

		$data_table_all_start = ($page_current - 1)*$page_limit;
		if ((($page_current - 1)*$page_limit) + 1 <= $count_all) {
			$data_table_all_start_real = (($page_current - 1)*$page_limit) + 1;
			if ($data_table_all_start + $page_limit <= $count_all) {
				$data_table_all_end_real = $data_table_all_start + $page_limit;
			} else {
				$data_table_all_end_real = $count_all;
			}
		} else {
			$data_table_all_start_real = "";
			$data_table_all_end_real = "";
		}
		$page_limit = $page_limit;
		$pages = ceil($count_all / $page_limit);

		if ($search != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction)) {
				$search_parameter = "?search=".$search;
			} else {
				$search_parameter = "&search=".$search;
			}
		}

		if ($filter_date_from != "" || $filter_date_to != "" || $activate != "") {
			if (empty($page_limit) && empty($page_current) && empty($sort_by) && empty($sort_direction) && empty($search)) {
				$filter_parameter = "?published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			} else {
				$filter_parameter = "&published=".$activate."from=".$filter_date_from."&to=".$filter_date_to;
			}
		}

		if ($page_current <= 1) {
			$pagination .= '<li class=\"paginate_button previous disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>';
		} else {
			$page_prev = $page_current - 1;
			$pagination .= '<li class=\"paginate_button previous\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_previous\"><a href=\"?page='.$page_prev.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-left\"></i></a></li>';
		}

		for ($i = 0; $i < $pages; $i++) {
			$i_prev = $i;
			$i_next = $i + 2;
			$i_real = $i + 1;
			if ($i_real == $page_current) {
				$pagination .= '<li class=\"paginate_button active\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			} else {
				$pagination .= '<li class=\"paginate_button\" aria-controls=\"datatable\" tabindex=\"0\"><a href=\"?page='.$i_real.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\">'.$i_real.'</a></li>';
			}
		}

		if ($page_current >= $pages) {
			$pagination .= '<li class=\"paginate_button next disabled\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>';
		} else {
			$page_next = $page_current + 1;
			$pagination .= '<li class=\"paginate_button next\" aria-controls=\"datatable\" tabindex=\"0\" id=\"datatable_next\"><a href=\"?page='.$page_next.'&limit='.$page_limit.$search_parameter.$filter_parameter.'\"><i class=\"fa fa-angle-right\"></i></a></li>';
		}


		if ($search != "") {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_search_abstract_modules_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_search_abstract_modules_data_table_all($search, $table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		} else {
			if ($_SESSION["users_level"] == "4") {
				$writers_filters = array(
					"users_id" => $_SESSION["users_id"]
				);
				$data_table = get_abstract_modules_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, $writers_filters, $extended_command);
			} else {
				$data_table = get_abstract_modules_data_table_all($table_module_field, $data_table_all_start, $page_limit, $sort_by, $sort_direction, $activate, "", $extended_command);
			}
		}

	} else {
		if ($_SESSION["users_level"] == "4") {
			$writers_filters = array(
				"users_id" => $_SESSION["users_id"]
			);
			$data_table = get_abstract_modules_data_table_all($table_module_field, "", "", "", "", $activate, $writers_filters, $extended_command);
		} else {
			$data_table = get_abstract_modules_data_table_all($table_module_field, "", "", "", "", $activate, "", $extended_command);
		}
	}

	if (isset($con)) {
		stop($con);
	}

	if (empty($page_limit)) {
		if ($configs["datatable_page_limit"] == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($configs["datatable_page_limit"] == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	} else {
		if ($page_limit == 5) {
			$page_limit_default_5_selected = "selected";
		} else if ($page_limit == 10) {
			$page_limit_default_10_selected = "selected";
		} else if ($page_limit == 15) {
			$page_limit_default_15_selected = "selected";
		} else if ($page_limit == 20) {
			$page_limit_default_20_selected = "selected";
		} else if ($page_limit == 50) {
			$page_limit_default_50_selected = "selected";
		} else if ($page_limit == 100) {
			$page_limit_default_100_selected = "selected";
		} else {
			$page_limit_default_20_selected = "selected";
		}
	}

	$top_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_length\" id=\"datatable_length\"><label><select id=\"datatable_length_selector\" name=\"datatable_length\" aria-controls=\"datatable\" class=\"form-control\"><option value=\"5\" '.$page_limit_default_5_selected.'>5</option><option value=\"10\" '.$page_limit_default_10_selected.'>10</option><option value=\"15\" '.$page_limit_default_15_selected.'>15</option><option value=\"20\" '.$page_limit_default_20_selected.'>20</option><option value=\"50\" '.$page_limit_default_50_selected.'>50</option><option value=\"100\" '.$page_limit_default_100_selected.'>100</option></select></label></div></div><div class=\"col-sm-6\"><div id=\"datatable_filter\" class=\"dataTables_filter\"><label>Search:<input id=\"datatable_search\" type=\"search\" class=\"form-control\" placeholder=\"\" aria-controls=\"datatable\" style=\"width: auto;\" value=\"'.$search.'\"></label></div></div></div>';

	$foot_panel = '<div class=\"row\"><div class=\"col-sm-6\"><div class=\"dataTables_info\" id=\"datatable_info\" role=\"status\" aria-live=\"polite\">Showing <strong>'.$data_table_all_start_real.'</strong>-<strong>'.$data_table_all_end_real.'</strong> of <strong>'.$count_all.'</strong></div></div><div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"datatable_paginate\"><ul class=\"pagination\">'.$pagination.'</ul></div></div></div>';

	$sort_symbol_down = '<i class=\"fa fa-angle-down\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol_up = '<i class=\"fa fa-angle-up\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .75;\"></i>';
	$sort_symbol = '<i class=\"fa fa-sort\" aria-hidden=\"true\" style=\"position: absolute; margin-top: 2px; margin-left: 20px; display: inline-block; opacity: .25;\"></i>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["values"] = $data_table;
	$response["count"] = $count_all;
	$response["count_true"] = $count_all_true;
	$response["pagination"] = $pagination;
	$response["sort_symbol_down"] = $sort_symbol_down;
	$response["sort_symbol_up"] = $sort_symbol_up;
	$response["sort_symbol"] = $sort_symbol;
	$response["top_panel"] = $top_panel;
	$response["foot_panel"] = $foot_panel;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function create_abstract_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$html = inform_abstract_modules_table_row($data_table, $table_module_field);
	$html = '<tr id="datatable-'.$data_table["abstract_modules_id"].'" data-target="'.$data_table["abstract_modules_id"].'">'.$html.'</tr>';

	$response["status"] = true;
	$response["message"] = translate("Successfully created table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function update_abstract_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $html = inform_abstract_modules_table_row($data_table, $table_module_field);

	$response["status"] = true;
	$response["message"] = translate("Successfully updated table row");
	$response["html"] = $html;

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();
}

function inform_abstract_modules_table_row($data_table, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $module_link = false;
	foreach (array_keys($data_table) as $pages_key) {
		if ($pages_key == "pages_link" && !empty($pages_key)) {
			$module_link = true;
		}
	}

	$i = 0;
	foreach ($table_module_field as $key => $value) {

		$table_row_value_key = "";
		$table_row_value = "";
		$td_align_class = "";
		$td_weight_class = "";

		if ($value == "true" || $value === true) {

			if ($i >= 2) {
				$td_hidden_xs_class = "hidden-xs";
			} else {
				$td_hidden_xs_class = "";
			}

			if ($i >= 3 && $key != "abstract_modules_activate") {
				$td_hidden_sm_class = "hidden-sm";
			} else {
				$td_hidden_sm_class = "";
			}

			if ($i >= 4 && $key != "abstract_modules_activate") {
				$td_hidden_md_class = "hidden-md";
			} else {
				$td_hidden_md_class = "";
			}

			if ($i >= 5 && $key != "abstract_modules_activate") {
				$td_hidden_lg_class = "hidden-lg";
			} else {
				$td_hidden_lg_class = "";
			}

			if ($key != "abstract_modules_actions") {
				if ($key == "abstract_modules_id") {
					$td_hidden_xs_class = "hidden-xs";
					$td_hidden_sm_class = "hidden-sm";
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "abstract_modules_name") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_name"]));
				} else if ($key == "abstract_modules_varname") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_varname"]));
				} else if ($key == "abstract_modules_description") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_description"]));
				} else if ($key == "abstract_modules_form_method") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_form_method"]));
				} else if ($key == "abstract_modules_component_modules") {
					$td_align_class = ".text-center";
					$abstract_modules_component_modules_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_modules_default_value[0] != "" && isset($abstract_modules_component_modules_default_value[0])) {
						if ($data_table["abstract_modules_component_modules"] == $abstract_modules_component_modules_default_value[0]) {
							if ($abstract_modules_component_modules_default_value[0] == "1") {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_modules"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_modules_default_value[0] == "1") {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_modules"] == "1") {
							$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_modules_data_table_value;
				} else if ($key == "abstract_modules_component_groups") {
					$td_align_class = ".text-center";
					$abstract_modules_component_groups_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_groups_default_value[0] != "" && isset($abstract_modules_component_groups_default_value[0])) {
						if ($data_table["abstract_modules_component_groups"] == $abstract_modules_component_groups_default_value[0]) {
							if ($abstract_modules_component_groups_default_value[0] == "1") {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_groups"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_groups_default_value[0] == "1") {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_groups"] == "1") {
							$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_groups_data_table_value;
				} else if ($key == "abstract_modules_component_users") {
					$td_align_class = ".text-center";
					$abstract_modules_component_users_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_users_default_value[0] != "" && isset($abstract_modules_component_users_default_value[0])) {
						if ($data_table["abstract_modules_component_users"] == $abstract_modules_component_users_default_value[0]) {
							if ($abstract_modules_component_users_default_value[0] == "1") {
								$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_users"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_users_default_value[0] == "1") {
								$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_users"] == "1") {
							$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_users_data_table_value;
				} else if ($key == "abstract_modules_component_languages") {
					$td_align_class = ".text-center";
					$abstract_modules_component_languages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_languages_default_value[0] != "" && isset($abstract_modules_component_languages_default_value[0])) {
						if ($data_table["abstract_modules_component_languages"] == $abstract_modules_component_languages_default_value[0]) {
							if ($abstract_modules_component_languages_default_value[0] == "1") {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_languages"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_languages_default_value[0] == "1") {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_languages"] == "1") {
							$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_languages_data_table_value;
				} else if ($key == "abstract_modules_component_pages") {
					$td_align_class = ".text-center";
					$abstract_modules_component_pages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_pages_default_value[0] != "" && isset($abstract_modules_component_pages_default_value[0])) {
						if ($data_table["abstract_modules_component_pages"] == $abstract_modules_component_pages_default_value[0]) {
							if ($abstract_modules_component_pages_default_value[0] == "1") {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_pages"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_pages_default_value[0] == "1") {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_pages"] == "1") {
							$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_pages_data_table_value;
				} else if ($key == "abstract_modules_component_media") {
					$td_align_class = ".text-center";
					$abstract_modules_component_media_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_media_default_value[0] != "" && isset($abstract_modules_component_media_default_value[0])) {
						if ($data_table["abstract_modules_component_media"] == $abstract_modules_component_media_default_value[0]) {
							if ($abstract_modules_component_media_default_value[0] == "1") {
								$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_media"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_media_default_value[0] == "1") {
								$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_media"] == "1") {
							$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_media_data_table_value;
				} else if ($key == "abstract_modules_component_commerce") {
					$td_align_class = ".text-center";
					$abstract_modules_component_commerce_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_component_commerce_default_value[0] != "" && isset($abstract_modules_component_commerce_default_value[0])) {
						if ($data_table["abstract_modules_component_commerce"] == $abstract_modules_component_commerce_default_value[0]) {
							if ($abstract_modules_component_commerce_default_value[0] == "1") {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_commerce"])) . '</span>';
							}
						} else {
							if ($abstract_modules_component_commerce_default_value[0] == "1") {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_component_commerce"] == "1") {
							$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_component_commerce_data_table_value;
				} else if ($key == "abstract_modules_database_engine") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_database_engine"]));
				} else if ($key == "abstract_modules_data_sortable") {
					$td_align_class = ".text-center";
					$abstract_modules_data_sortable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_data_sortable_default_value[0] != "" && isset($abstract_modules_data_sortable_default_value[0])) {
						if ($data_table["abstract_modules_data_sortable"] == $abstract_modules_data_sortable_default_value[0]) {
							if ($abstract_modules_data_sortable_default_value[0] == "1") {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_sortable"])) . '</span>';
							}
						} else {
							if ($abstract_modules_data_sortable_default_value[0] == "1") {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_data_sortable"] == "1") {
							$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_data_sortable_data_table_value;
				} else if ($key == "abstract_modules_data_addable") {
					$td_align_class = ".text-center";
					$abstract_modules_data_addable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_data_addable_default_value[0] != "" && isset($abstract_modules_data_addable_default_value[0])) {
						if ($data_table["abstract_modules_data_addable"] == $abstract_modules_data_addable_default_value[0]) {
							if ($abstract_modules_data_addable_default_value[0] == "1") {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_addable"])) . '</span>';
							}
						} else {
							if ($abstract_modules_data_addable_default_value[0] == "1") {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_data_addable"] == "1") {
							$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_data_addable_data_table_value;
				} else if ($key == "abstract_modules_data_viewonly") {
					$td_align_class = ".text-center";
					$abstract_modules_data_viewonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					if ($abstract_modules_data_viewonly_default_value[0] != "" && isset($abstract_modules_data_viewonly_default_value[0])) {
						if ($data_table["abstract_modules_data_viewonly"] == $abstract_modules_data_viewonly_default_value[0]) {
							if ($abstract_modules_data_viewonly_default_value[0] == "1") {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_viewonly"])) . '</span>';
							}
						} else {
							if ($abstract_modules_data_viewonly_default_value[0] == "1") {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							} else {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
							}
						}
					} else {
						if ($data_table["abstract_modules_data_viewonly"] == "1") {
							$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
						} else {
							$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
						}
					}
					$table_row_value = $abstract_modules_data_viewonly_data_table_value;
				} else if ($key == "abstract_modules_template") {
					$td_align_class = "text-center";
					$table_row_value = render_abstract_modules_table_template_file($data_table["abstract_modules_template"]);
				} else if ($key == "abstract_modules_icon") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_icon"]));
				} else if ($key == "abstract_modules_category") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_category"]));
				} else if ($key == "abstract_modules_subject") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_subject"]));
				} else if ($key == "abstract_modules_subject_icon") {
					$td_align_class = "";
					$table_row_value = htmlspecialchars(strip_tags($data_table["abstract_modules_subject_icon"]));
				} else if ($key == "abstract_modules_activate") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					if ($data_table[$key] == "1") {
						$table_row_value = '<span class="label label-success">Published</span>';

					} else {
						$table_row_value = '<span class="label label-danger">Unpublished</span>';
					}
				} else if ($key == "languages_short_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = $data_table[$key];
				} else if ($key == "users_name") {
					$td_weight_class = "";
					$td_align_class = "text-center";
					$table_row_value = '<a href="'.backend_rewrite_url("users.php").'?action=view&users_id='.$data_table["users_id"].'" target="_blank">'.$data_table[$key].'</a>';
				} else {
					if (($key == "pages_link" || $module_link) && $i == 1) {
						$td_align_class = "";
						$table_row_value = '<a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a>';
					} else {
						$td_align_class = "text-center";
						$table_row_value = render_abstract_modules_table_data($data_table[$key]);
					}
				}
				if ($i == 1) {
					$td_weight_class = "font-w600";
				} else {
					$td_weight_class = "";
				}
				
				$html .= '
				<td class="td-abstract '.$td_align_class.' '.$td_hidden_xs_class.' '.$td_hidden_sm_class.' '.$td_hidden_md_class.' '.$td_hidden_lg_class.' '.$td_weight_class.'" id="datatable-'.$key.'-'.$data_table["abstract_modules_id"].'">'.$table_row_value.'</td>';
				$i++;
				
			} else {
			
				$table_module_field_unassoc = array_keys($table_module_field);
				$table_module_field_key = "abstract_modules_title";
				for ($j = 1; $j < count($table_module_field_unassoc); $j++) {
					if ($table_module_field[$table_module_field_unassoc[$j]] == "true" || $table_module_field[$table_module_field_unassoc[$j]] === true) {
						$table_module_field_key = $table_module_field_unassoc[$j];
						break;
					}
				}

				$html .= '
				<td id="datatable-actions-'.$data_table["abstract_modules_id"].'" class="td-abstract text-center">
					<div class="btn-group">';

				$html .= '
						<button class="btn btn-xs btn-default btn-view" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="tooltip" title="'. translate("View") . '">
						<i class="fa fa-eye"></i>
						</button>';
				
				if ($_SESSION["users_level"] != "5") {

					$html .= '
					<button class="btn btn-xs btn-default btn-generate" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="tooltip" title="'. translate("Generate") . '" data-content="'. translate("Are you sure you want to generate") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?<br />('. translate("Customed codes in last generated might be lost") . ')">
					<i class="si si-control-play"></i>
					</button>';

					$html .= '
					<button class="btn btn-xs btn-default btn-regenerate" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="tooltip" title="'. translate("Reset") . '" data-content="'. translate("Are you sure you want to reset") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?<br />('. translate("Customed codes in last generated and all data inside this module will be wiped") . ')">
					<i class="si si-reload"></i>
					</button>';
			
						if (!isset($data_table["abstract_modules_protected"]) || (isset($data_table["abstract_modules_protected"]) && $data_table["abstract_modules_protected"] != "1") || $_SESSION["users_level"] == "1" || $_SESSION["users_level"] == "2") {

						$html .= '
						<button class="btn btn-xs btn-default btn-edit" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="tooltip" title="'. translate("Edit") . '">
						<i class="fa fa-pencil"></i>
						</button>';
						
						}

						$html .= '
						<button class="btn btn-xs btn-default btn-copy" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="tooltip" title="'. translate("Copy") . '">
						<i class="fa fa-copy"></i>
						</button>';

						$html .= '
						<button class="btn btn-xs btn-default btn-delete" type="button" data-target="'.$data_table["abstract_modules_id"].'" data-toggle="modal" title="'. translate("Delete") . '" data-content="'. translate("Delete") . ' <strong>'.htmlspecialchars(strip_tags($data_table[$table_module_field_key])).'</strong>?">
						<i class="fa fa-times"></i>
						</button>';
			
				}

				$html .= '
					</div>
				</td>';
			
			}
			
		}
		
	}
	

    $response["status"] = true;
    $response["message"] = translate("Successfully inform table field");
    $response["html"] = $html;

    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function view_abstract_modules_table_row($target, $table_module_field) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

	$data_table = get_abstract_modules_data_by_id($target);

	if (isset($data_table) && $data_table != "" && $data_table != false) {

		$module_link = false;
		foreach (array_keys($data_table) as $pages_key) {
			if ($pages_key == "pages_link" && !empty($pages_key)) {
				$module_link = true;
			}
		}

		$i = 0;

		if (isset($data_table["languages_short_name"]) && !empty($data_table["languages_short_name"])) {
			$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
		} else {
			$flag = '';
		}

		foreach ($table_module_field as $key => $value) {
		
			if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Variable Name")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Description")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Backend Form Method")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Modules Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Groups Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Users Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Languages Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Pages Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Media Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Commerce Component")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Database Engine")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Sortable Data")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Addable Data")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Data is View Only")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Front-end Inner Pages Default Template")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Icon")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Category")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Subject")));
			} else if ($key == "") {
				$column_name = ucwords(trim(str_replace("_", " ", "Module Subject Icon")));
			} else {
				$column_name = str_replace("abstract_modules", "", $key);
				$column_name = ucwords(trim(str_replace("_", " ", $column_name)));
			}
			if ($key != "abstract_modules_actions") {
				if ($key == "abstract_modules_id") {
					if (!empty($data_table["abstract_modules_id"])) {
						$table_row_value = '<strong>' . translate("ID") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_name") {
					if ($data_table["abstract_modules_name"] != "") {
						$table_row_value = '<strong>' . translate("Module Name") . '</strong><br />' . $data_table["abstract_modules_name"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_varname") {
					if ($data_table["abstract_modules_varname"] != "") {
						$table_row_value = '<strong>' . translate("Module Variable Name") . '</strong><br />' . $data_table["abstract_modules_varname"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_description") {
					if ($data_table["abstract_modules_description"] != "") {
						$table_row_value = '<strong>' . translate("Module Description") . '</strong><br />' . $data_table["abstract_modules_description"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_form_method") {
					if ($data_table["abstract_modules_form_method"] != "") {
						$table_row_value = '<strong>' . translate("Backend Form Method") . '</strong><br />' . $data_table["abstract_modules_form_method"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_modules") {
					if ($data_table["abstract_modules_component_modules"] != "") {
						$abstract_modules_component_modules_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_modules_default_value[0] != "" && isset($abstract_modules_component_modules_default_value[0])) {
							if ($data_table["abstract_modules_component_modules"] == $abstract_modules_component_modules_default_value[0]) {
								if ($abstract_modules_component_modules_default_value[0] == "1") {
									$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_modules"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_modules_default_value[0] == "1") {
									$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_modules"] == "1") {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_modules_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Modules Component") . '</strong><br />' . $abstract_modules_component_modules_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_groups") {
					if ($data_table["abstract_modules_component_groups"] != "") {
						$abstract_modules_component_groups_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_groups_default_value[0] != "" && isset($abstract_modules_component_groups_default_value[0])) {
							if ($data_table["abstract_modules_component_groups"] == $abstract_modules_component_groups_default_value[0]) {
								if ($abstract_modules_component_groups_default_value[0] == "1") {
									$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_groups"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_groups_default_value[0] == "1") {
									$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_groups"] == "1") {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_groups_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Groups Component") . '</strong><br />' . $abstract_modules_component_groups_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_users") {
					if ($data_table["abstract_modules_component_users"] != "") {
						$abstract_modules_component_users_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_users_default_value[0] != "" && isset($abstract_modules_component_users_default_value[0])) {
							if ($data_table["abstract_modules_component_users"] == $abstract_modules_component_users_default_value[0]) {
								if ($abstract_modules_component_users_default_value[0] == "1") {
									$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_users"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_users_default_value[0] == "1") {
									$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_users"] == "1") {
								$abstract_modules_component_users_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_users_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Users Component") . '</strong><br />' . $abstract_modules_component_users_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_languages") {
					if ($data_table["abstract_modules_component_languages"] != "") {
						$abstract_modules_component_languages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_languages_default_value[0] != "" && isset($abstract_modules_component_languages_default_value[0])) {
							if ($data_table["abstract_modules_component_languages"] == $abstract_modules_component_languages_default_value[0]) {
								if ($abstract_modules_component_languages_default_value[0] == "1") {
									$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_languages"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_languages_default_value[0] == "1") {
									$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_languages"] == "1") {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_languages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Languages Component") . '</strong><br />' . $abstract_modules_component_languages_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_pages") {
					if ($data_table["abstract_modules_component_pages"] != "") {
						$abstract_modules_component_pages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_pages_default_value[0] != "" && isset($abstract_modules_component_pages_default_value[0])) {
							if ($data_table["abstract_modules_component_pages"] == $abstract_modules_component_pages_default_value[0]) {
								if ($abstract_modules_component_pages_default_value[0] == "1") {
									$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_pages"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_pages_default_value[0] == "1") {
									$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_pages"] == "1") {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_pages_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Pages Component") . '</strong><br />' . $abstract_modules_component_pages_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_media") {
					if ($data_table["abstract_modules_component_media"] != "") {
						$abstract_modules_component_media_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_media_default_value[0] != "" && isset($abstract_modules_component_media_default_value[0])) {
							if ($data_table["abstract_modules_component_media"] == $abstract_modules_component_media_default_value[0]) {
								if ($abstract_modules_component_media_default_value[0] == "1") {
									$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_media"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_media_default_value[0] == "1") {
									$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_media"] == "1") {
								$abstract_modules_component_media_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_media_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Media Component") . '</strong><br />' . $abstract_modules_component_media_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_component_commerce") {
					if ($data_table["abstract_modules_component_commerce"] != "") {
						$abstract_modules_component_commerce_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_component_commerce_default_value[0] != "" && isset($abstract_modules_component_commerce_default_value[0])) {
							if ($data_table["abstract_modules_component_commerce"] == $abstract_modules_component_commerce_default_value[0]) {
								if ($abstract_modules_component_commerce_default_value[0] == "1") {
									$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_component_commerce"])) . '</span>';
								}
							} else {
								if ($abstract_modules_component_commerce_default_value[0] == "1") {
									$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_component_commerce"] == "1") {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_component_commerce_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Commerce Component") . '</strong><br />' . $abstract_modules_component_commerce_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_database_engine") {
					if ($data_table["abstract_modules_database_engine"] != "") {
						$table_row_value = '<strong>' . translate("Database Engine") . '</strong><br />' . $data_table["abstract_modules_database_engine"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_data_sortable") {
					if ($data_table["abstract_modules_data_sortable"] != "") {
						$abstract_modules_data_sortable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_data_sortable_default_value[0] != "" && isset($abstract_modules_data_sortable_default_value[0])) {
							if ($data_table["abstract_modules_data_sortable"] == $abstract_modules_data_sortable_default_value[0]) {
								if ($abstract_modules_data_sortable_default_value[0] == "1") {
									$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_sortable"])) . '</span>';
								}
							} else {
								if ($abstract_modules_data_sortable_default_value[0] == "1") {
									$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_data_sortable"] == "1") {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_sortable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Sortable Data") . '</strong><br />' . $abstract_modules_data_sortable_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_data_addable") {
					if ($data_table["abstract_modules_data_addable"] != "") {
						$abstract_modules_data_addable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_data_addable_default_value[0] != "" && isset($abstract_modules_data_addable_default_value[0])) {
							if ($data_table["abstract_modules_data_addable"] == $abstract_modules_data_addable_default_value[0]) {
								if ($abstract_modules_data_addable_default_value[0] == "1") {
									$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_addable"])) . '</span>';
								}
							} else {
								if ($abstract_modules_data_addable_default_value[0] == "1") {
									$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_data_addable"] == "1") {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_addable_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Addable Data") . '</strong><br />' . $abstract_modules_data_addable_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_data_viewonly") {
					if ($data_table["abstract_modules_data_viewonly"] != "") {
						$abstract_modules_data_viewonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
						if ($abstract_modules_data_viewonly_default_value[0] != "" && isset($abstract_modules_data_viewonly_default_value[0])) {
							if ($data_table["abstract_modules_data_viewonly"] == $abstract_modules_data_viewonly_default_value[0]) {
								if ($abstract_modules_data_viewonly_default_value[0] == "1") {
									$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
								} else {
									$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . htmlspecialchars(strip_tags($data_table["abstract_modules_data_viewonly"])) . '</span>';
								}
							} else {
								if ($abstract_modules_data_viewonly_default_value[0] == "1") {
									$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
								} else {
									$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("Not set") . '</span>';
								}
							}
						} else {
							if ($data_table["abstract_modules_data_viewonly"] == "1") {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-success">' . translate("Yes") . '</span>';
							} else {
								$abstract_modules_data_viewonly_data_table_value = '<span class="label label-danger">' . translate("No") . '</span>';
							}
						}
						$table_row_value = '<strong>' . translate("Data is View Only") . '</strong><br />' . $abstract_modules_data_viewonly_data_table_value . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_template") {
					if ($data_table["abstract_modules_template"] != "") {
						$table_row_value = '<strong>' . translate("Front-end Inner Pages Default Template") . '</strong><br />'.render_abstract_modules_view_template_file($data_table["abstract_modules_template"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_icon") {
					if ($data_table["abstract_modules_icon"] != "") {
						$table_row_value = '<strong>' . translate("Module Icon") . '</strong><br />' . $data_table["abstract_modules_icon"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_category") {
					if ($data_table["abstract_modules_category"] != "") {
						$table_row_value = '<strong>' . translate("Module Category") . '</strong><br />' . $data_table["abstract_modules_category"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_subject") {
					if ($data_table["abstract_modules_subject"] != "") {
						$table_row_value = '<strong>' . translate("Module Subject") . '</strong><br />' . $data_table["abstract_modules_subject"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_subject_icon") {
					if ($data_table["abstract_modules_subject_icon"] != "") {
						$table_row_value = '<strong>' . translate("Module Subject Icon") . '</strong><br />' . $data_table["abstract_modules_subject_icon"] . '<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "abstract_modules_activate") {
					if ($data_table[$key] == "1") {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-success">' . translate("Published") . '</span><br /><br />';
					} else {
						$table_row_value = '<strong>' . translate("Status") . '</strong><br /><span class="label label-danger">' . translate("Unpublished") . '</span><br /><br />';
					}
					$html .= $table_row_value;
					$i++;
				} else if ($key == "abstract_modules_date_created") {
					if (!empty($data_table["abstract_modules_date_created"])) {
						$table_row_value = '<strong>' . translate("Date Created") . '</strong><br />'.datetime_short_reformat($data_table["abstract_modules_date_created"]).'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_id") {
					$i++;
				} else if ($key == "languages_short_name") {
					if (!empty($data_table["languages_short_name"])) {
						$flag = '<img src="templates/'.$configs["backend_template"].'/images/languages/'.strtolower($data_table["languages_short_name"]).'.gif" alt="'.$data_table["languages_short_name"].'" title="'.$data_table["languages_short_name"].'" > ';
						$table_row_value = '<strong>' . translate("Language") . '</strong><br />'.$flag.$data_table["languages_name"].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "languages_name") {
					$i++;
				} else if ($key == "users_id") {
					$i++;
				} else if ($key == "users_username") {
					$i++;
				} else if ($key == "users_name") {
					if (!empty($data_table["users_name"])) {
						$table_row_value = '<strong>' . translate("User") . '</strong><br /><a href="'.backend_rewrite_url("users.php").'?action=view&users_id='.$data_table["users_id"].'">'.$data_table[$key].'</a><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "users_last_name") {
					$i++;
				} else if ($key == "pages_link") {
					if (!empty($data_table["pages_link"])) {
						$table_row_value = '<strong>' . translate("Link") . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(urldecode(rewrite_url(strip_tags($data_table[$key])))).'</a ><br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_title") {
					if (!empty($data_table["pages_meta_title"])) {
						$table_row_value = '<strong>' . translate("Meta Title") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_description") {
					if (!empty($data_table["pages_meta_description"])) {
						$table_row_value = '<strong>' . translate("Meta Description") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else if ($key == "pages_meta_keyword") {
					if (!empty($data_table["pages_meta_keyword"])) {
						$table_row_value = '<strong>' . translate("Meta Keywords") . '</strong><br />'.$data_table[$key].'<br /><br />';
						$html .= $table_row_value;
						$i++;
					}
				} else {
					if (!empty($data_table[$key])) {
                        if (($key == "pages_link" || $module_link) && $i == 1) {
                            $table_row_value = '<strong>' . translate($column_name) . '</strong><br /><a href="'.rewrite_url($data_table["pages_link"]).'" target="_blank">'.htmlspecialchars(strip_tags($data_table[$key])).'</a><br /><br />';
                        } else {
                            $table_row_value = '<strong>' . translate($column_name) . '</strong><br />'.render_abstract_modules_view_data($data_table[$key]).'<br /><br />';
						}
						$html .= $table_row_value;
						$i++;
                    }
				}
			}
		}

		$response["status"] = true;
		$response["message"] = translate("Successfully view table field");
		$response["html"] = $html;

	} else {

		$response["status"] = false;
		$response["message"] = translate("Empty data");
		$response["html"] = $html;

	}


    if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["html"];
	}
	unset($html);
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function render_abstract_modules_table_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_modules_table_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$filimagee;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 200px; max-height: 100px;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_modules_table_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="group" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 200px; max-height: 100px;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="max-width: 200px; max-height: 100px;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_modules_view_template_file($file) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	
	if (function_exists("mb_strpos")) {
		if (mb_strpos($file, "http://") === false && mb_strpos($file, "https://") === false) {
			if (mb_strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $image;
		}
	} else {
		if (strpos($file, "http://") === false && strpos($file, "https://") === false) {
			if (strpos($configs["base_url"].$file, "/templates") === false) {
				$file_path = $configs["base_url"]."/templates/".$configs["template"]."/".$file;
			} else {
				$file_path = $configs["base_url"].$file;
			}
		} else {
			$file_path = $file;
		}
	}
	$path_parts = pathinfo($file_path);
	$rendered_data = '<a href="' . $file_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';

	$file_check = curl_init($file_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status == 404) {
		$rendered_data = '<a href="'.$file_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_modules_view_image($image) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (function_exists("mb_strpos")) {
		if (mb_strpos($image, "http://") === false && mb_strpos($image, "https://") === false) {
			if (mb_strpos($configs["base_url"].$image, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	} else {
		if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
			if (strpos($configs["base_url"].$data, "/media") === false) {
				$image_path = $configs["base_url"]."/media/user/files/".$image;
			} else {
				$image_path = $configs["base_url"].$image;
			}
		} else {
			$image_path = $image;
		}
	}
	$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$image_path.'"><img src="'.$image_path.'" alt="'.$image.'" style="max-width: 100%; max-height: 100%;" /></a>';

	$file_check = curl_init($image_path);
	curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
	$file_check_response = curl_exec($file_check);
	$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
	curl_close($file_check);
	if ($file_status != 200) {
		$rendered_data = '<a href="'.$image_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function render_abstract_modules_view_data($data) {

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;

	if (empty($data)) {
		$rendered_data = '';
	} else if (@unserialize($data) !== false) {
		$td_align_class = "";
		unset($table_row_value_arrays);
		$table_row_value_arrays = unserialize($data);
		if (!empty($table_row_value_arrays[0])) {
			$rendered_data = htmlspecialchars(implode(", ", $table_row_value_arrays));
		}
	} else if (is_array($data)) {
		$rendered_data = htmlspecialchars(implode(", ", $data));
	} else if (DateTime::createFromFormat("Y-m-d H:i:s", $data) !== FALSE) {
		$rendered_data = datetime_short_reformat($data);
	} else if (DateTime::createFromFormat("Y-m-d", $data) !== FALSE) {
		$rendered_data = date_short_reformat($data);
	} else if (preg_match("/(\.jpg|\.jpeg|\.JPG|\.JPEG|\.png|\.PNG|\.gif|\.GIF|\.webp)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<a class="datatable-image-popup" rel="view" href="'.$data_path.'"><img src="'.$data_path.'" alt="'.$data.'" style="max-width: 100%; max-height: 100%;" /></a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mpg|\.mpeg|\.MPG|\.MPEG|\.mov|\.MOV|\.mp4|\.MP4|\.webm|\.WEBM|\.avi|\.AVI|\.wmv|\.WMV|\.flv|\.FLV|\.m4v|\.M4V)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<video controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></video>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mp3|\.MP3|\.m4a|\.M4A|\.ac3|\.AC3|\.aiff|\.AIFF|\.mid|\.MID|\.ogg|\.OGG|\.wav|\.WAV|\.wma|\.WMA)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$rendered_data = '<audio controls style="width: 100%; max-width: 100%;"><source src="'.$data_path.'" /></audio>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.txt|\.log|\.dat|\.inf|\.info|\.md)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-text-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.doc|\.docx|\.rtf|\.page)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-word-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.xls|\.xlsx|\.csv)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-excel-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.ppt|\.pptx|\.key)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-powerpoint-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.pdf)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-pdf-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.zip|\.rar|\.iso|\.tar|\.gz|\.lz|\.7z|\.cab)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-archive-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.exe|\.dmg|\.apk|\.cmd)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-terminal"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
	} else if (preg_match("/(\.html|\.htm|\.xhtml|\.sql|\.xml|\.php|\.css|\.scss|\.js|\.ts|\.htaccess|\.ade|\.adp|\.sln|\.suo)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-code-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.mdb|\.accdb|\.odb|\.bin)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-database"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else if (preg_match("/(\.psd|\.svg|\.ai)$/", $data)) {
		$data_path = "";
		if (function_exists("mb_strpos")) {
			if (mb_strpos($data, "http://") === false && mb_strpos($data, "https://") === false) {
				if (mb_strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}

			} else {
				$data_path = $data;
			}
		} else {
			if (strpos($data, "http://") === false && strpos($data, "https://") === false) {
				if (strpos($configs["base_url"].$data, "/media") === false) {
					$data_path = $configs["base_url"]."/media/user/files/".$data;
				} else {
					$data_path = $configs["base_url"].$data;
				}
			} else {
				$data_path = $data;
			}
		}
		$path_parts = pathinfo($data_path);
		$rendered_data = '<a href="' . $data_path . '" target="_blank"><i class="fa fa-file-image-o"></i> '.htmlspecialchars(strip_tags($path_parts["basename"])).'</a>';
		$file_check = curl_init($data_path);
		curl_setopt($file_check,  CURLOPT_RETURNTRANSFER, TRUE);
		$file_check_response = curl_exec($file_check);
		$file_status = curl_getinfo($file_check, CURLINFO_HTTP_CODE);
		curl_close($file_check);
		if ($file_status != 200) {
			$rendered_data = '<a href="'.$data_path.'" target="_blank" title="'.translate("File is broken").'"><i class="fa fa-chain-broken"></i></a>';
		}
	} else {
		$rendered_data = htmlspecialchars(strip_tags($path_parts["basename"]));
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo $rendered_data;
	} else {
		return $rendered_data;
	}
	unset($rendered_data);
	unset($method);
	exit();

}

function get_abstract_modules_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get edit log data from module "abstract_modules" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '1'
	AND (`log_function` = 'create_abstract_modules_data' OR `log_function` = 'update_abstract_modules_data')
	AND `log_type` = 'backend'
	ORDER BY `log_date_created` DESC";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {
			$i = 0;
			$queries = array();
			while($query = mysqli_fetch_array($result)) {

				$query["abstract_modules_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["abstract_modules_date_created"], $configs["datetimezone"])));

				$query["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
				$query["log_date_created_formatted"] = datetime_reformat_plussecond($query["log_date_created"]);

				$queries[$i] = $query;
				$i++;

			}

			$response["status"] = true;
			$response["message"] = translate("Successfully get all data");
			$response["values"] = $queries;

			unset($queries);
			$queries = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");

		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_modules_edit_log_data($target){

	/* get global: configurations */
	global $configs;
	
	/* get global: ajax function */
	global $method;


    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_modules" */
	$sql = "
	SELECT *
	FROM `log`
	WHERE `modules_record_target` = '" . $target . "'
	AND `modules_id` = '1'
	AND (`log_function` = 'create_abstract_modules_data' OR `log_function` = 'update_abstract_modules_data')
	AND `log_type` = 'backend'";
	$result = mysqli_query($con, $sql);

	if ($result) {
		
		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_abstract_modules_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_modules" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);

		if ($num > 0) {

			$query = mysqli_fetch_array($result);
			$query_log_content_hash = unserialize($query["log_content_hash"]);
			
		if (isset($query_log_content_hash["abstract_modules_template"]) && !empty($query_log_content_hash["abstract_modules_template"])) {
			$query_log_content_hash["abstract_modules_template_path"] = $configs["base_url"]."/media/user/".$query_log_content_hash["abstract_modules_template"];
		}

			$response["values"] = $query_log_content_hash;
			$response["status"] = true;
			$response["message"] = translate("Successfully get data by id");

			unset($query);
			$query = array();
			unset($query_log_content_hash);
			$query_log_content_hash = array();

		} else {

			$response["status"] = false;
			$response["message"] = translate("Empty data");
		}
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function count_abstract_modules_edit_log_data_by_id($target){

	/* get global: configurations */
	global $configs;

	/* get global: ajax function */
	global $method;
	

    $con = start();
	

	/* check remote usage permssion */
	if (!check_api_source()) {
		$response["status"] = false;
		$response["message"] = "<strong>" . __FUNCTION__ . "</strong> " . translate("is permission denied");
		$response["target"] = __FUNCTION__;
		$response["values"] = $parameters;
		if (isset($method) && $method == __FUNCTION__) {
			echo json_encode($response);
		} else {
			return $response["status"];
		}
		unset($response);
		$response = array();
		unset($method);
		exit();
	}


	/* initialize: clean strings */
	$method = escape_string($con, $method);
	$_SESSION = escape_string($con, $_SESSION);
	$target = escape_string($con, $target);

	/* database: get data from module "abstract_modules" */
    $sql = "
	SELECT *
	FROM `log`
	WHERE `log_id` = '" . $target . "'
	LIMIT 1";
	$result = mysqli_query($con, $sql);

	if ($result) {

		$num = mysqli_num_rows($result);
		
		$response["status"] = true;
		$response["message"] = translate("Successfully count all data");
		$response["values"] = $num;
		mysqli_free_result($result);

	} else {

		$response["status"] = false;
		$response["target"] = "abstract_modules";
		if (isset($_SESSION["users_level"]) && $_SESSION["users_level"] == "1") {
			$response["message"] = translate("Database encountered error at table")." <strong>abstract_modules</strong><br />(".mysqli_error($con).")";
		} else {
			$response["message"] = translate("Database encountered error");
		}

	}

	if (isset($con)) {
		stop($con);
	}

	if (isset($method) && $method == __FUNCTION__) {
		echo json_encode($response);
	} else {
		return $response["values"];
	}
	unset($response);
	$response = array();
	unset($method);
	exit();

}

function get_machinelang($strings) {

	$url = generate_title_link($strings);
    $url_pure = str_replace(".php", "", $url);
	$machinelang = str_replace("-", "_", $url_pure);

    return $machinelang;

}
?>