<?php
mb_internal_encoding('UTF-8');
/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

function dateDifference($date_1, $date_2, $differenceFormat = '%a' ) {
	
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);
	
}

function count_log_date_created_by_page_load() {

	global $configs;
	
	$con = start();

	$time = escape_string($con, $time);
	$duration = escape_string($con, $duration);

	$sql_total = "
	SELECT DISTINCT DATE(`log_date_created`)
	FROM  `log`
	WHERE  `log_function` = 'page_load'";

	$result_total = mysqli_query($con, $sql_total);
	$num_total = mysqli_num_rows($result_total);
	
	stop($con);
	
	return $num_total;

}

function count_log_by_page_load($time = "", $duration = "") {

	global $configs;
	
	$con = start();

	$extra_sql_data_all = "";
	if (isset($time) && $time != "" && isset($duration) && $duration != "") {
		$extra_sql_data_all .= "AND `log_date_created` > DATE_SUB(NOW(), INTERVAL '".$time."' ".$duration.") ";
	}

	$sql_total = "
	SELECT `log_link` ,  `log_date_created`
	FROM  `log`
	WHERE  `log_function` = 'page_load'
	".$extra_sql_data_all ;

	$result_total = mysqli_query($con, $sql_total);
	$num_total = mysqli_num_rows($result_total);
	
	stop($con);
	
	return $num_total;
	
}

function get_start_date() {
	
	global $configs;
	
	$con = start();

	$sql = "	SELECT DISTINCT `log_link` , `log_date_created`
				FROM `log`
				WHERE `log_function` = 'page_load'
				ORDER BY `log_date_created` ASC
				LIMIT 0 , 1";

	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_assoc($result);
	
	stop($con);
	
	return $query["log_date_created"];
	
}


function count_from_module($module_name="") {
	
	global $configs;
	
	$con = start();

	$module_name = escape_string($con, $module_name);

	$find_module = "";
	if (isset($module_name) && $module_name != "") {
		$find_module .= "WHERE `modules_name` = '".$module_name."' ";
	}

	$module_id = "	SELECT `modules_db_name` FROM `modules`
					".$find_module;

	$module_db = "";
	$result = mysqli_query($con, $module_id);
	$name = mysqli_fetch_assoc($result);
	if ($name != "") {
		$module_db .= $name["modules_db_name"];
	} else {
		$module_db .= $module_name;
	}

	$sql = "EXPLAIN SELECT *
			FROM  `".strtolower($module_db)."`";

	$result = mysqli_query($con, $sql);
	$query = mysqli_fetch_assoc($result);
	
	stop($con);
	
	return $query["rows"];
	
}

function count_agent() {
	
	global $configs;
	global $method;
	
	$con = start();

	$sql = "SELECT `log_user_agent`
			FROM  `log`
			WHERE  `log_function` = 'page_load'";

	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);

	$count_mobile = 0;
    $count_desktop = 0;

	while($data = mysqli_fetch_array($result)) {
		$u_agent = $data["log_user_agent"];

		if (preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge|maemo|midp|mmp|opera m(ob|in)i|palm(os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$u_agent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac(|\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt|kwc\-|kyo(c|k)|le(no|xi)|lg(g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-||o|v)|zz)|mt(50|p1|v)|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v)|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-|)|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($u_agent,0,4)) )
		{
			$device = "Desktop";
		} else {
			$device = "Mobile";
		}

		if ($device == "Mobile") {
			$count_mobile+=1;
		} elseif ($device == "Desktop") {
			$count_desktop+=1;
		}
	}
	
	stop($con);
	
	$response["count_desktop"] = $count_desktop;
	$response["count_mobile"] = $count_mobile;

	if ($method == "count_agent") {
		echo json_encode($response);
	} else {
		return $response;
	}
	unset($response);
	$response = array();
	unset($method);
	exit();
	
}

function count_monthly_visit($start_date="2017-01-01",$month="1") {
	
	global $configs;
	
	$con = start();

	$start_date = escape_string($con, $start_date);
	$month = escape_string($con, $month);

	$extra_sql = "";
	if (isset($start_date) && $start_date != "" && isset($month) && $month != "") {
		$extra_sql .= "	AND `log_date_created` < DATE_SUB('".$start_date."', INTERVAL -1 YEAR)
						AND `log_date_created` >= '".$start_date."'
						AND MONTH(`log_date_created`) = '".$month."'";
	}

	$sql = "SELECT DISTINCT YEAR(`log_date_created`) AS year, MONTH(`log_date_created`) AS month, COUNT(`log_link`) AS count, `log_date_created`
			FROM `log`
			WHERE `log_function` = 'page_load'
			".$extra_sql."
			GROUP BY month";

	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);

	if ($num < 1) {

		$queries = array();
		$queries["year"] = 0;
		$queries["month"] = 0;
		$queries["count"] = 0;
		$queries["log_date_created"] = 0;

		return $queries;

	} else {

		$i = 0;
		$queries = array();
		while($data = mysqli_fetch_array($result)) {
			$queries["year"] = $data["year"];
			$queries["month"] = $data["month"];
			$queries["count"] = $data["count"];
			$queries["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($data["log_date_created"], $configs["datetimezone"])));;
			$i++;
		}

		return $queries;

	}
	
	stop($con);

}

function top_page_traffic() {
	
	global $configs;
	
	$con = start();

	$sql = "SELECT `log_link`, modules_record_target as name, COUNT(`log_link`) as visit
			FROM `log`
			WHERE `log_function` = 'page_load'
			GROUP BY `log_link`
			ORDER BY visit DESC
			LIMIT 0,10";

	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);

	$i = 0;
	$queries = array();
    while($data = mysqli_fetch_assoc($result)) {
    	$queries[$i] = $data;
		$i++;
    }
	
	stop($con);
	
	return $queries;
	
}

function users() {

	global $configs;
	
	$con = start();

	$sql = "SELECT update_time.`users_name`, update_time.`log_date_created`, check_recent.all_content
			FROM (
			    SELECT `users_name`, MAX(`log_date_created`) AS recent, COUNT(`log_id`) AS all_content
			    FROM `log`
			    WHERE `log_name` = 'create data'
				AND `log_type` = 'backend'
			    GROUP BY `users_username` ASC
			) AS check_recent
			INNER JOIN `log` AS update_time
			ON update_time.`users_name` = check_recent.`users_name`
			AND update_time.`log_date_created` = check_recent.recent
			GROUP BY `users_username` ASC";

	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);

	$i = 0;
	$queries = array();
    while($data = mysqli_fetch_assoc($result)) {
    	$queries[$i] = $data;
		$i++;
    }
	return $queries;
	
	stop($con);
	
}

function user_date($time="") {

	global $configs;
	
	$con = start();

	$time = escape_string($con, $time);

	$extra_sql_data_all = "";
	if (isset($time) && $time != "" && strtolower($time) == "today") {
		$extra_sql_data_all .= "AND `log_date_created` > DATE_SUB(NOW(), INTERVAL DATE_FORMAT(NOW(), '%H:%i:%s') HOUR_SECOND) ";
	} elseif (isset($time) && $time != "" && strtolower($time) == "week") {
		$extra_sql_data_all .= "AND `log_date_created` > DATE_SUB(NOW(), INTERVAL DATE_FORMAT(NOW(), '%w %H:%i:%s') DAY_SECOND) ";
	} elseif (isset($time) && $time != "" && strtolower($time) == "month") {
		$extra_sql_data_all .= "AND `log_date_created` > DATE_FORMAT('".date('Y-m-01')." ".date("00:00:00")."', '%Y-%m-%d %H:%i:%s') ";
	}

	$sql = "SELECT update_time.`users_name`, update_time.`log_date_created`, check_recent.all_content
			FROM (
			    SELECT `users_name`, MAX(`log_date_created`) AS recent, COUNT(`log_id`) AS all_content
			    FROM `log`
			    WHERE `log_name` = 'create data'
				AND `users_username` != 'creator'
				AND `users_username` != ''
				".$extra_sql_data_all."
				GROUP BY `users_username` ASC
			) AS check_recent
			INNER JOIN `log` AS update_time
			ON update_time.`users_name` = check_recent.`users_name`
			AND update_time.`log_date_created` = check_recent.recent
			GROUP BY `users_username` ASC";

	$result = mysqli_query($con, $sql);
	$num = mysqli_num_rows($result);

	if ($num < 1) {

		$queries = array();
		$queries["log_id"] = 0;
		$queries["log_ip"] = 0;
		$queries["log_date_created"] = 0;
		$queries["users_username"] = 0;
		$queries["count"] = 0;

		return $queries;

	} else {

		$i = 0;
		$queries = array();
	    while($data = mysqli_fetch_assoc($result)) {
	    	$queries[$i] = $data;
			// $queries[$i]["log_date_created"] = date("Y-m-d H:i:s", strtotime(datetime_convert($query["log_date_created"], $configs["datetimezone"])));
			$i++;
	    }

		return $queries;

	}
	
	stop($con);

}
?>
