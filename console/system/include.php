<?php session_start(); ?>
<?php 
error_reporting(~E_NOTICE);

/* configurations */
include('../config.php');
include('translation.config.php');
include('core/'.$configs['version'].'/initial.php');
include('core/'.$configs['version'].'/filemanager.config.php'); 

/* functions - library */
include('core/'.$configs['version'].'/string.php');
include('core/'.$configs['version'].'/datetime.php');
include('core/'.$configs['version'].'/urlrewritting.php');
include('core/'.$configs['version'].'/translation.php');

/* functions - core */
include('core/'.$configs['version'].'/security.php');
include('core/'.$configs['version'].'/authentication.php');
include('core/'.$configs['version'].'/template.php');
include('core/'.$configs['version'].'/path.php');
include('core/'.$configs['version'].'/modules.php');
include('core/'.$configs['version'].'/groups.php');
include('core/'.$configs['version'].'/users.php');
include('core/'.$configs['version'].'/languages.php');
include('core/'.$configs['version'].'/pages.php');
include('core/'.$configs['version'].'/media.php');
include('core/'.$configs['version'].'/log.php');
include('core/'.$configs['version'].'/api.php');
include('core/'.$configs['version'].'/mail.php');

/* factsheets */
include('core/'.$configs['version'].'/factsheet.php');
?>