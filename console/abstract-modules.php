<?php 
require_once("system/include.php");

/* configurations: Abstract Modules */
$abstract_modules = get_modules_data_by_id("1");
if (isset($abstract_modules) && !empty($abstract_modules)) {
	$title = translate($abstract_modules["modules_name"]);
	$module_page = $abstract_modules["modules_link"];
	$module_page_link = backend_rewrite_url($abstract_modules["modules_link"]);
	$module_function_page = htmlspecialchars("system/".$abstract_modules["modules_function_link"]);
	require_once($module_function_page);
} else {
	$title = translate("Abstract Modules");
	$module_page = "abstract-modules.php";
	$module_page_link = backend_rewrite_url("abstract-modules.php");
	$module_function_page = htmlspecialchars("system/core/".$configs["version"]."/abstract-modules.php");
	require_once("system/core/".$configs["version"]."/abstract-modules.php");
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Abstract Modules */
if (!authentication_session_users()) {
	authentication_deny();
}
if (!authentication_session_modules($module_page)) {
	authentication_permission_deny();
}

if (isset($abstract_modules["modules_datatable_field"]) && !empty($abstract_modules["modules_datatable_field"])) {
	$table_module_field = array();
	for ($i = 0; $i < count($abstract_modules["modules_datatable_field"]); $i++) {
		if ($abstract_modules["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
			$abstract_modules_modules_datatable_field_display = true;
		} else {
			$abstract_modules_modules_datatable_field_display = false;
		}
		$table_module_field[$abstract_modules["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $abstract_modules_modules_datatable_field_display;
	}
} else {
	$table_module_field = array(
	"abstract_modules_id" => true,
	"abstract_modules_name" => true,
	"abstract_modules_description" => true,
	"abstract_modules_varname" => false,
	"abstract_modules_form_method" => false,
	"abstract_modules_component_modules" => false,
	"abstract_modules_component_groups" => false,
	"abstract_modules_component_users" => false,
	"abstract_modules_component_languages" => false,
	"abstract_modules_component_pages" => false,
	"abstract_modules_component_media" => false,
	"abstract_modules_component_commerce" => false,
	"abstract_modules_database_engine" => false,
	"abstract_modules_data_sortable" => false,
	"abstract_modules_data_addable" => false,
	"abstract_modules_data_viewonly" => false,
	"abstract_modules_template" => false,
	"abstract_modules_icon" => false,
	"abstract_modules_category" => false,
	"abstract_modules_subject" => false,
	"abstract_modules_subject_icon" => false,
	"abstract_modules_date_created" => true,
	"users_id" => false,
	"users_username" => false,
	"users_name" => true,
	"users_last_name" => false,
	"abstract_modules_activate" => true,
	"abstract_modules_actions" => true,
);
}

require_once("templates/".$configs["backend_template"]."/header.php");
require_once("templates/".$configs["backend_template"]."/overlay.php");
require_once("templates/".$configs["backend_template"]."/sidebar.php");
require_once("templates/".$configs["backend_template"]."/navbar.php");
require_once("templates/".$configs["backend_template"]."/container-header.php");
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header bg-mama-green">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo htmlspecialchars($module_function_page); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="abstract_modules">
					<input type="hidden" id="abstract_modules_id" name="abstract_modules_id">
					<input type="hidden" id="abstract_modules_action" name="abstract_modules_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_name"><?php echo translate("Module Name"); ?><span class="text-danger">*</span> <span id="abstract_modules_name_stringlength" class="text-success pull-right push-10-l">0/80</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_modules_name" type="text" id="abstract_modules_name" name="abstract_modules_name" value="" placeholder="<?php echo translate("Name of module");?>" >
								
								<div class="help-block"><?php echo translate("Identify module"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_varname"><?php echo translate("Module Variable Name"); ?><span id="abstract_modules_varname_stringlength" class="text-success pull-right push-10-l">0/80</span></label>
							<div class="col-md-12">
								<input class="form-control abstract_modules_varname" type="text" id="abstract_modules_varname" name="abstract_modules_varname" value="" placeholder="<?php echo translate("Variable name of module ");?>" >
								
								<div class="help-block"><?php echo translate("Leave this field blank to let system generate from name automatically"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_description"><?php echo translate("Module Description"); ?></label>
							<div class="col-md-12">
								<textarea class="form-control abstract_modules_description" id="abstract_modules_description" name="abstract_modules_description" ><?php $abstract_modules_description_default_value[0]; ?></textarea>
								
								<div class="help-block"><?php echo translate("Describe what module is about"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_form_method_default_value = unserialize(stripslashes("a:1:{i:0;s:4:\\\"post\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_form_method" */
					var abstract_modules_form_method_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_form_method_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_form_method"><?php echo translate("Backend Form Method"); ?><span class="text-danger">*</span> </label>
							<div class="col-md-12">
								<select class="form-control abstract_modules_form_method" id="abstract_modules_form_method" name="abstract_modules_form_method" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="get">get</option>
												<option value="post">post</option>
								</select>
								<div id="abstract_modules_form_method-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Method type for form submission"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_modules_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_modules" */
					var abstract_modules_component_modules_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_modules_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_modules"><?php echo translate("Modules Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_modules" name="abstract_modules_component_modules" class="abstract_modules_component_modules" type="checkbox" value="<?php echo $abstract_modules_component_modules_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Modules will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_groups_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_groups" */
					var abstract_modules_component_groups_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_groups_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_groups"><?php echo translate("Groups Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_groups" name="abstract_modules_component_groups" class="abstract_modules_component_groups" type="checkbox" value="<?php echo $abstract_modules_component_groups_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Groups will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_users_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_users" */
					var abstract_modules_component_users_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_users_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_users"><?php echo translate("Users Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_users" name="abstract_modules_component_users" class="abstract_modules_component_users" type="checkbox" value="<?php echo $abstract_modules_component_users_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Modules will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_languages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}"));
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_languages" */
					var abstract_modules_component_languages_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_languages_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_languages"><?php echo translate("Languages Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_languages" name="abstract_modules_component_languages" class="abstract_modules_component_languages" type="checkbox" value="<?php echo $abstract_modules_component_languages_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Languages will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_pages_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_pages" */
					var abstract_modules_component_pages_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_pages_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_pages"><?php echo translate("Pages Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_pages" name="abstract_modules_component_pages" class="abstract_modules_component_pages" type="checkbox" value="<?php echo $abstract_modules_component_pages_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Pages will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_media_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_media" */
					var abstract_modules_component_media_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_media_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_media"><?php echo translate("Media Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_media" name="abstract_modules_component_media" class="abstract_modules_component_media" type="checkbox" value="<?php echo $abstract_modules_component_media_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Media will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_component_commerce_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_component_commerce" */
					var abstract_modules_component_commerce_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_component_commerce_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_component_commerce"><?php echo translate("Commerce Component"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_component_commerce" name="abstract_modules_component_commerce" class="abstract_modules_component_commerce" type="checkbox" value="<?php echo $abstract_modules_component_commerce_default_value[0];?>"><span></span> <?php echo translate("Enable"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Component of Commerce will be integrated"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_database_engine_default_value = unserialize(stripslashes("a:1:{i:0;s:6:\\\"InnoDB\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_database_engine" */
					var abstract_modules_database_engine_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_database_engine_default_value)); ?>");
					</script>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_database_engine"><?php echo translate("Database Engine"); ?><span class="text-danger">*</span> </label>
							<div class="col-md-12">
								<select class="form-control abstract_modules_database_engine" id="abstract_modules_database_engine" name="abstract_modules_database_engine" >
								
									<option value=""><?php echo translate(""); ?></option>
									<option value="" disabled>-</option>
												<option value="InnoDB">InnoDB</option>
												<option value="MyISAM">MyISAM</option>
								</select>
								<div id="abstract_modules_database_engine-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
								
								<div class="help-block"><?php echo translate("Engine of SQL database"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_data_sortable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_data_sortable" */
					var abstract_modules_data_sortable_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_data_sortable_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_data_sortable"><?php echo translate("Sortable Data"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_data_sortable" name="abstract_modules_data_sortable" class="abstract_modules_data_sortable" type="checkbox" value="<?php echo $abstract_modules_data_sortable_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Backend users can sort contents"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_data_addable_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_data_addable" */
					var abstract_modules_data_addable_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_data_addable_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_data_addable"><?php echo translate("Addable Data"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_data_addable" name="abstract_modules_data_addable" class="abstract_modules_data_addable" type="checkbox" value="<?php echo $abstract_modules_data_addable_default_value[0];?>" checked ><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Backend users can add contents"); ?></div>
							</div>
						</div>
					</div>
					
					<?php 
					$abstract_modules_data_viewonly_default_value = unserialize(stripslashes("a:1:{i:0;s:3:\\\"yes\\\";}")); 
					?>
					<script>
					/* JavaScript variable for dynamic default value "abstract_modules_data_viewonly" */
					var abstract_modules_data_viewonly_default_value = $.parseJSON("<?php echo addslashes(json_encode($abstract_modules_data_viewonly_default_value)); ?>");
					</script>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_data_viewonly"><?php echo translate("Data is View Only"); ?></label>
							<div class="col-md-12">
								<label class="css-input switch switch-lg switch-success">
									<input id="abstract_modules_data_viewonly" name="abstract_modules_data_viewonly" class="abstract_modules_data_viewonly" type="checkbox" value="<?php echo $abstract_modules_data_viewonly_default_value[0];?>"><span></span> <?php echo translate("Yes"); ?>
								</label>
								
								<div class="help-block"><?php echo translate("Backend users can only view contents"); ?></div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_template"><?php echo translate("Front-end Inner Pages Default Template"); ?></label>
							<div class="col-md-5">
								<input class="form-control abstract_modules_template" type="text" id="abstract_modules_template" name="abstract_modules_template" value="" placeholder="<?php echo translate("Template of inner pages created by module");?>" >
								
								<div class="help-block"><?php echo translate("Selected front-end template will be activated to inner pages created by module which was enabled pages component"); ?></div>
							</div>
							<div class="col-md-5">
								<a href="plugins/filemanager/dialog-template.php?type=2&amp;field_id=modules_pages_template&amp;relative_url=1" id="abstract_modules_template-fileselector-btn" class="btn" type="button" style="padding: 0">
								<i class="si si-folder-alt fa-2x"></i> <?php echo translate("Select"); ?>
								</a>
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
            				<div class="col-md-12">
								<div id="icon_list_parent" class="panel-group">
									<div class="panel panel-default">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#icon_list_parent" href="#icon_list">
											<div class="panel-heading">
												<span class="panel-title"><?php echo translate("Icon"); ?></span> <i class="fa fa-angle-down"></i>
											</div>
										</a>

                    					<div id="icon_list" class="panel-collapse collapse">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-12">
														<div class="form-group">
															<div class="col-md-12">
																<div class="row items-push">
																	<div class="col-md-12" style="margin-bottom: 0;">
																		<label class="css-input css-radio css-radio-default push-10-r">
																			<input type="radio" id="abstract_modules_icon_" name="abstract_modules_icon" class="abstract_modules_icon" checked  value=""><span></span> <?php echo translate("None"); ?>
																		</label>
																	</div>
																</div>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siactionredoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-action-redo'></i>"><span></span> <i class='si si-action-redo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siactionundoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-action-undo'></i>"><span></span> <i class='si si-action-undo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siarrowdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-arrow-down'></i>"><span></span> <i class='si si-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siarrowlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-arrow-left'></i>"><span></span> <i class='si si-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siarrowrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-arrow-right'></i>"><span></span> <i class='si si-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siarrowupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-arrow-up'></i>"><span></span> <i class='si si-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sianchori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-anchor'></i>"><span></span> <i class='si si-anchor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibadgei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-badge'></i>"><span></span> <i class='si si-badge'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bag'></i>"><span></span> <i class='si si-bag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibani" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-ban'></i>"><span></span> <i class='si si-ban'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibarcharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bar-chart'></i>"><span></span> <i class='si si-bar-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibasketi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-basket'></i>"><span></span> <i class='si si-basket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibasketloadedi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-basket-loaded'></i>"><span></span> <i class='si si-basket-loaded'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibelli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bell'></i>"><span></span> <i class='si si-bell'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibookopeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-book-open'></i>"><span></span> <i class='si si-book-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibriefcasei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-briefcase'></i>"><span></span> <i class='si si-briefcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibubblei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bubble'></i>"><span></span> <i class='si si-bubble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibubblesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bubbles'></i>"><span></span> <i class='si si-bubbles'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sibulbi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-bulb'></i>"><span></span> <i class='si si-bulb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicalculatori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-calculator'></i>"><span></span> <i class='si si-calculator'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicalendari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-calendar'></i>"><span></span> <i class='si si-calendar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicallendi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-call-end'></i>"><span></span> <i class='si si-call-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicallini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-call-in'></i>"><span></span> <i class='si si-call-in'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicallouti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-call-out'></i>"><span></span> <i class='si si-call-out'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicamcorderi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-camcorder'></i>"><span></span> <i class='si si-camcorder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicamerai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-camera'></i>"><span></span> <i class='si si-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sichecki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-check'></i>"><span></span> <i class='si si-check'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sichemistryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-chemistry'></i>"><span></span> <i class='si si-chemistry'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siclocki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-clock'></i>"><span></span> <i class='si si-clock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siclouddownloadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-cloud-download'></i>"><span></span> <i class='si si-cloud-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siclouduploadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-cloud-upload'></i>"><span></span> <i class='si si-cloud-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicompassi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-compass'></i>"><span></span> <i class='si si-compass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolendi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-end'></i>"><span></span> <i class='si si-control-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolforwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-forward'></i>"><span></span> <i class='si si-control-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolplayi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-play'></i>"><span></span> <i class='si si-control-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolpausei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-pause'></i>"><span></span> <i class='si si-control-pause'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolrewindi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-rewind'></i>"><span></span> <i class='si si-control-rewind'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicontrolstarti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-control-start'></i>"><span></span> <i class='si si-control-start'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicreditcardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-credit-card'></i>"><span></span> <i class='si si-credit-card'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicropi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-crop'></i>"><span></span> <i class='si si-crop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-cup'></i>"><span></span> <i class='si si-cup'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicursori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-cursor'></i>"><span></span> <i class='si si-cursor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sicursormovei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-cursor-move'></i>"><span></span> <i class='si si-cursor-move'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidiamondi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-diamond'></i>"><span></span> <i class='si si-diamond'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidirectioni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-direction'></i>"><span></span> <i class='si si-direction'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidirectionsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-directions'></i>"><span></span> <i class='si si-directions'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidisci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-disc'></i>"><span></span> <i class='si si-disc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidislikei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-dislike'></i>"><span></span> <i class='si si-dislike'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidoci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-doc'></i>"><span></span> <i class='si si-doc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidocsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-docs'></i>"><span></span> <i class='si si-docs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidraweri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-drawer'></i>"><span></span> <i class='si si-drawer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sidropi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-drop'></i>"><span></span> <i class='si si-drop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siearphonesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-earphones'></i>"><span></span> <i class='si si-earphones'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siearphonesalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-earphones-alt'></i>"><span></span> <i class='si si-earphones-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siemoticonsmilei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-emoticon-smile'></i>"><span></span> <i class='si si-emoticon-smile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sienergyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-energy'></i>"><span></span> <i class='si si-energy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sienvelopei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-envelope'></i>"><span></span> <i class='si si-envelope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sienvelopeletteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-envelope-letter'></i>"><span></span> <i class='si si-envelope-letter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sienvelopeopeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-envelope-open'></i>"><span></span> <i class='si si-envelope-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siequalizeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-equalizer'></i>"><span></span> <i class='si si-equalizer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sieyei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-eye'></i>"><span></span> <i class='si si-eye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sieyeglassesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-eyeglasses'></i>"><span></span> <i class='si si-eyeglasses'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sifeedi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-feed'></i>"><span></span> <i class='si si-feed'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sifilmi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-film'></i>"><span></span> <i class='si si-film'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sifirei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-fire'></i>"><span></span> <i class='si si-fire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siflagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-flag'></i>"><span></span> <i class='si si-flag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sifolderi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-folder'></i>"><span></span> <i class='si si-folder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sifolderalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-folder-alt'></i>"><span></span> <i class='si si-folder-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siframei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-frame'></i>"><span></span> <i class='si si-frame'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sigamecontrolleri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-game-controller'></i>"><span></span> <i class='si si-game-controller'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sighosti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-ghost'></i>"><span></span> <i class='si si-ghost'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siglobei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-globe'></i>"><span></span> <i class='si si-globe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siglobealti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-globe-alt'></i>"><span></span> <i class='si si-globe-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sigraduationi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-graduation'></i>"><span></span> <i class='si si-graduation'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sigraphi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-graph'></i>"><span></span> <i class='si si-graph'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sigridi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-grid'></i>"><span></span> <i class='si si-grid'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sihandbagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-handbag'></i>"><span></span> <i class='si si-handbag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sihearti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-heart'></i>"><span></span> <i class='si si-heart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sihomei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-home'></i>"><span></span> <i class='si si-home'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sihourglassi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-hourglass'></i>"><span></span> <i class='si si-hourglass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siinfoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-info'></i>"><span></span> <i class='si si-info'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sikeyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-key'></i>"><span></span> <i class='si si-key'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silayersi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-layers'></i>"><span></span> <i class='si si-layers'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silikei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-like'></i>"><span></span> <i class='si si-like'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silinki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-link'></i>"><span></span> <i class='si si-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silisti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-list'></i>"><span></span> <i class='si si-list'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silocki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-lock'></i>"><span></span> <i class='si si-lock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silockopeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-lock-open'></i>"><span></span> <i class='si si-lock-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silogini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-login'></i>"><span></span> <i class='si si-login'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-silogouti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-logout'></i>"><span></span> <i class='si si-logout'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siloopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-loop'></i>"><span></span> <i class='si si-loop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simagicwandi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-magic-wand'></i>"><span></span> <i class='si si-magic-wand'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simagneti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-magnet'></i>"><span></span> <i class='si si-magnet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simagnifieri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-magnifier'></i>"><span></span> <i class='si si-magnifier'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simagnifieraddi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-magnifier-add'></i>"><span></span> <i class='si si-magnifier-add'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simagnifierremovei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-magnifier-remove'></i>"><span></span> <i class='si si-magnifier-remove'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simapi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-map'></i>"><span></span> <i class='si si-map'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simicrophonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-microphone'></i>"><span></span> <i class='si si-microphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simousei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-mouse'></i>"><span></span> <i class='si si-mouse'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simoustachei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-moustache'></i>"><span></span> <i class='si si-moustache'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simusictonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-music-tone'></i>"><span></span> <i class='si si-music-tone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-simusictonealti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-music-tone-alt'></i>"><span></span> <i class='si si-music-tone-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sinotei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-note'></i>"><span></span> <i class='si si-note'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sinotebooki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-notebook'></i>"><span></span> <i class='si si-notebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipaperclipi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-paper-clip'></i>"><span></span> <i class='si si-paper-clip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipaperplanei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-paper-plane'></i>"><span></span> <i class='si si-paper-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipencili" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-pencil'></i>"><span></span> <i class='si si-pencil'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipicturei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-picture'></i>"><span></span> <i class='si si-picture'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipiecharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-pie-chart'></i>"><span></span> <i class='si si-pie-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-pin'></i>"><span></span> <i class='si si-pin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siplanei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-plane'></i>"><span></span> <i class='si si-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siplaylisti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-playlist'></i>"><span></span> <i class='si si-playlist'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-plus'></i>"><span></span> <i class='si si-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipointeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-pointer'></i>"><span></span> <i class='si si-pointer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipoweri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-power'></i>"><span></span> <i class='si si-power'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipresenti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-present'></i>"><span></span> <i class='si si-present'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siprinteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-printer'></i>"><span></span> <i class='si si-printer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sipuzzlei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-puzzle'></i>"><span></span> <i class='si si-puzzle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siquestioni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-question'></i>"><span></span> <i class='si si-question'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sirefreshi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-refresh'></i>"><span></span> <i class='si si-refresh'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sireloadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-reload'></i>"><span></span> <i class='si si-reload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sirocketi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-rocket'></i>"><span></span> <i class='si si-rocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siscreendesktopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-screen-desktop'></i>"><span></span> <i class='si si-screen-desktop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siscreensmartphonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-screen-smartphone'></i>"><span></span> <i class='si si-screen-smartphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siscreentableti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-screen-tablet'></i>"><span></span> <i class='si si-screen-tablet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisettingsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-settings'></i>"><span></span> <i class='si si-settings'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisharei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-share'></i>"><span></span> <i class='si si-share'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisharealti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-share-alt'></i>"><span></span> <i class='si si-share-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sishieldi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-shield'></i>"><span></span> <i class='si si-shield'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sishufflei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-shuffle'></i>"><span></span> <i class='si si-shuffle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisizeactuali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-size-actual'></i>"><span></span> <i class='si si-size-actual'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisizefullscreeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-size-fullscreen'></i>"><span></span> <i class='si si-size-fullscreen'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialdribbblei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-dribbble'></i>"><span></span> <i class='si si-social-dribbble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialdropboxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-dropbox'></i>"><span></span> <i class='si si-social-dropbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialfacebooki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-facebook'></i>"><span></span> <i class='si si-social-facebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialtumblri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-tumblr'></i>"><span></span> <i class='si si-social-tumblr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialtwitteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-twitter'></i>"><span></span> <i class='si si-social-twitter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisocialyoutubei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-social-youtube'></i>"><span></span> <i class='si si-social-youtube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sispeechi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-speech'></i>"><span></span> <i class='si si-speech'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sispeedometeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-speedometer'></i>"><span></span> <i class='si si-speedometer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sistari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-star'></i>"><span></span> <i class='si si-star'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisupporti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-support'></i>"><span></span> <i class='si si-support'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisymbolfemalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-symbol-female'></i>"><span></span> <i class='si si-symbol-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sisymbolmalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-symbol-male'></i>"><span></span> <i class='si si-symbol-male'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sitagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-tag'></i>"><span></span> <i class='si si-tag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sitargeti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-target'></i>"><span></span> <i class='si si-target'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sitrashi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-trash'></i>"><span></span> <i class='si si-trash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sitrophyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-trophy'></i>"><span></span> <i class='si si-trophy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siumbrellai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-umbrella'></i>"><span></span> <i class='si si-umbrella'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siuseri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-user'></i>"><span></span> <i class='si si-user'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siuserfemalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-user-female'></i>"><span></span> <i class='si si-user-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siuserfollowi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-user-follow'></i>"><span></span> <i class='si si-user-follow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siuserfollowingi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-user-following'></i>"><span></span> <i class='si si-user-following'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siuserunfollowi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-user-unfollow'></i>"><span></span> <i class='si si-user-unfollow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siusersi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-users'></i>"><span></span> <i class='si si-users'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sivectori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-vector'></i>"><span></span> <i class='si si-vector'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sivolume1i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-volume-1'></i>"><span></span> <i class='si si-volume-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sivolume2i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-volume-2'></i>"><span></span> <i class='si si-volume-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-sivolumeoffi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-volume-off'></i>"><span></span> <i class='si si-volume-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siwalleti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-wallet'></i>"><span></span> <i class='si si-wallet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classsi-siwrenchi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='si si-wrench'></i>"><span></span> <i class='si si-wrench'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fa500pxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-500px'></i>"><span></span> <i class='fa fa-500px'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faadjusti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-adjust'></i>"><span></span> <i class='fa fa-adjust'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faadni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-adn'></i>"><span></span> <i class='fa fa-adn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faaligncenteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-align-center'></i>"><span></span> <i class='fa fa-align-center'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faalignjustifyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-align-justify'></i>"><span></span> <i class='fa fa-align-justify'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faalignlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-align-left'></i>"><span></span> <i class='fa fa-align-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faalignrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-align-right'></i>"><span></span> <i class='fa fa-align-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faamazoni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-amazon'></i>"><span></span> <i class='fa fa-amazon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faambulancei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ambulance'></i>"><span></span> <i class='fa fa-ambulance'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faanchori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-anchor'></i>"><span></span> <i class='fa fa-anchor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faandroidi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-android'></i>"><span></span> <i class='fa fa-android'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangellisti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angellist'></i>"><span></span> <i class='fa fa-angellist'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangledoubledowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-double-down'></i>"><span></span> <i class='fa fa-angle-double-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangledoublelefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-double-left'></i>"><span></span> <i class='fa fa-angle-double-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangledoublerighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-double-right'></i>"><span></span> <i class='fa fa-angle-double-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangledoubleupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-double-up'></i>"><span></span> <i class='fa fa-angle-double-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangledowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-down'></i>"><span></span> <i class='fa fa-angle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faanglelefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-left'></i>"><span></span> <i class='fa fa-angle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faanglerighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-right'></i>"><span></span> <i class='fa fa-angle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faangleupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-angle-up'></i>"><span></span> <i class='fa fa-angle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faapplei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-apple'></i>"><span></span> <i class='fa fa-apple'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarchivei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-archive'></i>"><span></span> <i class='fa fa-archive'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faareacharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-area-chart'></i>"><span></span> <i class='fa fa-area-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircledowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-down'></i>"><span></span> <i class='fa fa-arrow-circle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcirclelefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-left'></i>"><span></span> <i class='fa fa-arrow-circle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcirclerighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-right'></i>"><span></span> <i class='fa fa-arrow-circle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircleupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-up'></i>"><span></span> <i class='fa fa-arrow-circle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircleodowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-o-down'></i>"><span></span> <i class='fa fa-arrow-circle-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircleolefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-o-left'></i>"><span></span> <i class='fa fa-arrow-circle-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircleorighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-o-right'></i>"><span></span> <i class='fa fa-arrow-circle-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowcircleoupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-circle-o-up'></i>"><span></span> <i class='fa fa-arrow-circle-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-down'></i>"><span></span> <i class='fa fa-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-left'></i>"><span></span> <i class='fa fa-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-right'></i>"><span></span> <i class='fa fa-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrow-up'></i>"><span></span> <i class='fa fa-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowsalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrows-alt'></i>"><span></span> <i class='fa fa-arrows-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowshi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrows-h'></i>"><span></span> <i class='fa fa-arrows-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowsvi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrows-v'></i>"><span></span> <i class='fa fa-arrows-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faarrowsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-arrows'></i>"><span></span> <i class='fa fa-arrows'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faasteriski" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-asterisk'></i>"><span></span> <i class='fa fa-asterisk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faati" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-at'></i>"><span></span> <i class='fa fa-at'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faautomobilei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-automobile'></i>"><span></span> <i class='fa fa-automobile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabackwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-backward'></i>"><span></span> <i class='fa fa-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabalancescalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-balance-scale'></i>"><span></span> <i class='fa fa-balance-scale'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabani" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ban'></i>"><span></span> <i class='fa fa-ban'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabanki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bank'></i>"><span></span> <i class='fa fa-bank'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabarcharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bar-chart'></i>"><span></span> <i class='fa fa-bar-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabarchartoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bar-chart-o'></i>"><span></span> <i class='fa fa-bar-chart-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabarcodei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-barcode'></i>"><span></span> <i class='fa fa-barcode'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabarsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bars'></i>"><span></span> <i class='fa fa-bars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabattery0i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-battery-0'></i>"><span></span> <i class='fa fa-battery-0'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabattery1i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-battery-1'></i>"><span></span> <i class='fa fa-battery-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabattery2i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-battery-2'></i>"><span></span> <i class='fa fa-battery-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabattery3i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-battery-3'></i>"><span></span> <i class='fa fa-battery-3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabattery4i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-battery-4'></i>"><span></span> <i class='fa fa-battery-4'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabedi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bed'></i>"><span></span> <i class='fa fa-bed'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabeeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-beer'></i>"><span></span> <i class='fa fa-beer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabehancei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-behance'></i>"><span></span> <i class='fa fa-behance'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabehancesquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-behance-square'></i>"><span></span> <i class='fa fa-behance-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabelli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bell'></i>"><span></span> <i class='fa fa-bell'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabelloi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bell-o'></i>"><span></span> <i class='fa fa-bell-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabellslashi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bell-slash'></i>"><span></span> <i class='fa fa-bell-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabellslashoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bell-slash-o'></i>"><span></span> <i class='fa fa-bell-slash-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabicyclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bicycle'></i>"><span></span> <i class='fa fa-bicycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabinocularsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-binoculars'></i>"><span></span> <i class='fa fa-binoculars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabirthdaycakei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-birthday-cake'></i>"><span></span> <i class='fa fa-birthday-cake'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabitbucketi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bitbucket'></i>"><span></span> <i class='fa fa-bitbucket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabitbucketsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bitbucket-square'></i>"><span></span> <i class='fa fa-bitbucket-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabitcoini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bitcoin'></i>"><span></span> <i class='fa fa-bitcoin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fablacktiei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-black-tie'></i>"><span></span> <i class='fa fa-black-tie'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faboldi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bold'></i>"><span></span> <i class='fa fa-bold'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabolti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bolt'></i>"><span></span> <i class='fa fa-bolt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabombi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bomb'></i>"><span></span> <i class='fa fa-bomb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabooki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-book'></i>"><span></span> <i class='fa fa-book'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabookmarki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bookmark'></i>"><span></span> <i class='fa fa-bookmark'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabookmarkoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bookmark-o'></i>"><span></span> <i class='fa fa-bookmark-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabriefcasei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-briefcase'></i>"><span></span> <i class='fa fa-briefcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabugi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bug'></i>"><span></span> <i class='fa fa-bug'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabuildingi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-building'></i>"><span></span> <i class='fa fa-building'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabuildingoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-building-o'></i>"><span></span> <i class='fa fa-building-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabullhorni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bullhorn'></i>"><span></span> <i class='fa fa-bullhorn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabullseyei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bullseye'></i>"><span></span> <i class='fa fa-bullseye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-bus'></i>"><span></span> <i class='fa fa-bus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fabuyselladsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-buysellads'></i>"><span></span> <i class='fa fa-buysellads'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facabi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cab'></i>"><span></span> <i class='fa fa-cab'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalculatori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calculator'></i>"><span></span> <i class='fa fa-calculator'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar'></i>"><span></span> <i class='fa fa-calendar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendarcheckoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar-check-o'></i>"><span></span> <i class='fa fa-calendar-check-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendarminusoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar-minus-o'></i>"><span></span> <i class='fa fa-calendar-minus-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendarplusoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar-plus-o'></i>"><span></span> <i class='fa fa-calendar-plus-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendartimesoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar-times-o'></i>"><span></span> <i class='fa fa-calendar-times-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facalendaroi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-calendar-o'></i>"><span></span> <i class='fa fa-calendar-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facamerai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-camera'></i>"><span></span> <i class='fa fa-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facameraretroi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-camera-retro'></i>"><span></span> <i class='fa fa-camera-retro'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-car'></i>"><span></span> <i class='fa fa-car'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-down'></i>"><span></span> <i class='fa fa-caret-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-left'></i>"><span></span> <i class='fa fa-caret-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-right'></i>"><span></span> <i class='fa fa-caret-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-up'></i>"><span></span> <i class='fa fa-caret-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretsquareodowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-square-o-down'></i>"><span></span> <i class='fa fa-caret-square-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretsquareolefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-square-o-left'></i>"><span></span> <i class='fa fa-caret-square-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretsquareorighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-square-o-right'></i>"><span></span> <i class='fa fa-caret-square-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facaretsquareoupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-caret-square-o-up'></i>"><span></span> <i class='fa fa-caret-square-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facartarrowdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cart-arrow-down'></i>"><span></span> <i class='fa fa-cart-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facartplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cart-plus'></i>"><span></span> <i class='fa fa-cart-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc'></i>"><span></span> <i class='fa fa-cc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccamexi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-amex'></i>"><span></span> <i class='fa fa-cc-amex'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccdinersclubi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-diners-club'></i>"><span></span> <i class='fa fa-cc-diners-club'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccdiscoveri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-discover'></i>"><span></span> <i class='fa fa-cc-discover'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccjcbi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-jcb'></i>"><span></span> <i class='fa fa-cc-jcb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccmastercardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-mastercard'></i>"><span></span> <i class='fa fa-cc-mastercard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccpaypali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-paypal'></i>"><span></span> <i class='fa fa-cc-paypal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccstripei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-stripe'></i>"><span></span> <i class='fa fa-cc-stripe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faccvisai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cc-visa'></i>"><span></span> <i class='fa fa-cc-visa'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facertificatei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-certificate'></i>"><span></span> <i class='fa fa-certificate'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachecki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-check'></i>"><span></span> <i class='fa fa-check'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facheckcirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-check-circle'></i>"><span></span> <i class='fa fa-check-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facheckcircleoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-check-circle-o'></i>"><span></span> <i class='fa fa-check-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachecksquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-check-square'></i>"><span></span> <i class='fa fa-check-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachecksquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-check-square-o'></i>"><span></span> <i class='fa fa-check-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevroncircledowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-circle-down'></i>"><span></span> <i class='fa fa-chevron-circle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevroncirclelefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-circle-left'></i>"><span></span> <i class='fa fa-chevron-circle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevroncirclerighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-circle-right'></i>"><span></span> <i class='fa fa-chevron-circle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevroncircleupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-circle-up'></i>"><span></span> <i class='fa fa-chevron-circle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevrondowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-down'></i>"><span></span> <i class='fa fa-chevron-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevronlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-left'></i>"><span></span> <i class='fa fa-chevron-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevronrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-right'></i>"><span></span> <i class='fa fa-chevron-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachevronupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chevron-up'></i>"><span></span> <i class='fa fa-chevron-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachildi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-child'></i>"><span></span> <i class='fa fa-child'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fachromei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-chrome'></i>"><span></span> <i class='fa fa-chrome'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-clone'></i>"><span></span> <i class='fa fa-clone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-circle'></i>"><span></span> <i class='fa fa-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facircleoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-circle-o'></i>"><span></span> <i class='fa fa-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facircleonotchi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-circle-o-notch'></i>"><span></span> <i class='fa fa-circle-o-notch'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facirclethini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-circle-thin'></i>"><span></span> <i class='fa fa-circle-thin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclipboardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-clipboard'></i>"><span></span> <i class='fa fa-clipboard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclockoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-clock-o'></i>"><span></span> <i class='fa fa-clock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclosei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-close'></i>"><span></span> <i class='fa fa-close'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facloudi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cloud'></i>"><span></span> <i class='fa fa-cloud'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclouddownloadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cloud-download'></i>"><span></span> <i class='fa fa-cloud-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faclouduploadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cloud-upload'></i>"><span></span> <i class='fa fa-cloud-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facnyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cny'></i>"><span></span> <i class='fa fa-cny'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facodei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-code'></i>"><span></span> <i class='fa fa-code'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facodeforki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-code-fork'></i>"><span></span> <i class='fa fa-code-fork'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facodepeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-codepen'></i>"><span></span> <i class='fa fa-codepen'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facoffeei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-coffee'></i>"><span></span> <i class='fa fa-coffee'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facolumnsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-columns'></i>"><span></span> <i class='fa fa-columns'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommenti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-comment'></i>"><span></span> <i class='fa fa-comment'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommentoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-comment-o'></i>"><span></span> <i class='fa fa-comment-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommentsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-comments'></i>"><span></span> <i class='fa fa-comments'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommentsoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-comments-o'></i>"><span></span> <i class='fa fa-comments-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommentingi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-commenting'></i>"><span></span> <i class='fa fa-commenting'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facommentingoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-commenting-o'></i>"><span></span> <i class='fa fa-commenting-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facompassi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-compass'></i>"><span></span> <i class='fa fa-compass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facompressi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-compress'></i>"><span></span> <i class='fa fa-compress'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faconnectdevelopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-connectdevelop'></i>"><span></span> <i class='fa fa-connectdevelop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facontaoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-contao'></i>"><span></span> <i class='fa fa-contao'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facopyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-copy'></i>"><span></span> <i class='fa fa-copy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facopyrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-copyright'></i>"><span></span> <i class='fa fa-copyright'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facreativecommonsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-creative-commons'></i>"><span></span> <i class='fa fa-creative-commons'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facreditcardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-credit-card'></i>"><span></span> <i class='fa fa-credit-card'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facropi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-crop'></i>"><span></span> <i class='fa fa-crop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facrosshairsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-crosshairs'></i>"><span></span> <i class='fa fa-crosshairs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facss3i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-css3'></i>"><span></span> <i class='fa fa-css3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facubei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cube'></i>"><span></span> <i class='fa fa-cube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facubesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cubes'></i>"><span></span> <i class='fa fa-cubes'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facuti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cut'></i>"><span></span> <i class='fa fa-cut'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-facutleryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-cutlery'></i>"><span></span> <i class='fa fa-cutlery'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadashboardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dashboard'></i>"><span></span> <i class='fa fa-dashboard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadashcubei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dashcube'></i>"><span></span> <i class='fa fa-dashcube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadatabasei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-database'></i>"><span></span> <i class='fa fa-database'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadedenti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dedent'></i>"><span></span> <i class='fa fa-dedent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadeliciousi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-delicious'></i>"><span></span> <i class='fa fa-delicious'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadesktopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-desktop'></i>"><span></span> <i class='fa fa-desktop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadeviantarti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-deviantart'></i>"><span></span> <i class='fa fa-deviantart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadiamondi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-diamond'></i>"><span></span> <i class='fa fa-diamond'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadiggi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-digg'></i>"><span></span> <i class='fa fa-digg'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadollari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dollar'></i>"><span></span> <i class='fa fa-dollar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadotcircleoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dot-circle-o'></i>"><span></span> <i class='fa fa-dot-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadownloadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-download'></i>"><span></span> <i class='fa fa-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadribbblei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dribbble'></i>"><span></span> <i class='fa fa-dribbble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadropboxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-dropbox'></i>"><span></span> <i class='fa fa-dropbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fadrupali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-drupal'></i>"><span></span> <i class='fa fa-drupal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faediti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-edit'></i>"><span></span> <i class='fa fa-edit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faejecti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-eject'></i>"><span></span> <i class='fa fa-eject'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faellipsishi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ellipsis-h'></i>"><span></span> <i class='fa fa-ellipsis-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faellipsisvi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ellipsis-v'></i>"><span></span> <i class='fa fa-ellipsis-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faempirei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-empire'></i>"><span></span> <i class='fa fa-empire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faenvelopei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-envelope'></i>"><span></span> <i class='fa fa-envelope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faenvelopeoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-envelope-o'></i>"><span></span> <i class='fa fa-envelope-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faenvelopesquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-envelope-square'></i>"><span></span> <i class='fa fa-envelope-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faeraseri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-eraser'></i>"><span></span> <i class='fa fa-eraser'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faeuroi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-euro'></i>"><span></span> <i class='fa fa-euro'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexchangei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-exchange'></i>"><span></span> <i class='fa fa-exchange'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexclamationi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-exclamation'></i>"><span></span> <i class='fa fa-exclamation'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexclamationcirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-exclamation-circle'></i>"><span></span> <i class='fa fa-exclamation-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexclamationtrianglei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-exclamation-triangle'></i>"><span></span> <i class='fa fa-exclamation-triangle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexpandi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-expand'></i>"><span></span> <i class='fa fa-expand'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexpeditedssli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-expeditedssl'></i>"><span></span> <i class='fa fa-expeditedssl'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexternallinki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-external-link'></i>"><span></span> <i class='fa fa-external-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faexternallinksquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-external-link-square'></i>"><span></span> <i class='fa fa-external-link-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faeyei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-eye'></i>"><span></span> <i class='fa fa-eye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faeyeslashi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-eye-slash'></i>"><span></span> <i class='fa fa-eye-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faeyedropperi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-eyedropper'></i>"><span></span> <i class='fa fa-eyedropper'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafacebooki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-facebook'></i>"><span></span> <i class='fa fa-facebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafacebookofficiali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-facebook-official'></i>"><span></span> <i class='fa fa-facebook-official'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafacebooksquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-facebook-square'></i>"><span></span> <i class='fa fa-facebook-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafastbackwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fast-backward'></i>"><span></span> <i class='fa fa-fast-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafastforwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fast-forward'></i>"><span></span> <i class='fa fa-fast-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafaxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fax'></i>"><span></span> <i class='fa fa-fax'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafemalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-female'></i>"><span></span> <i class='fa fa-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafighterjeti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fighter-jet'></i>"><span></span> <i class='fa fa-fighter-jet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file'></i>"><span></span> <i class='fa fa-file'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafileoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-o'></i>"><span></span> <i class='fa fa-file-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilearchiveoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-archive-o'></i>"><span></span> <i class='fa fa-file-archive-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafileaudiooi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-audio-o'></i>"><span></span> <i class='fa fa-file-audio-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilecodeoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-code-o'></i>"><span></span> <i class='fa fa-file-code-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafileexceloi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-excel-o'></i>"><span></span> <i class='fa fa-file-excel-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafileimageoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-image-o'></i>"><span></span> <i class='fa fa-file-image-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilemovieoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-movie-o'></i>"><span></span> <i class='fa fa-file-movie-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilepdfoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-pdf-o'></i>"><span></span> <i class='fa fa-file-pdf-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilephotooi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-photo-o'></i>"><span></span> <i class='fa fa-file-photo-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilepictureoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-picture-o'></i>"><span></span> <i class='fa fa-file-picture-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilepowerpointoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-powerpoint-o'></i>"><span></span> <i class='fa fa-file-powerpoint-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilesoundoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-sound-o'></i>"><span></span> <i class='fa fa-file-sound-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafiletexti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-text'></i>"><span></span> <i class='fa fa-file-text'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafiletextoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-text-o'></i>"><span></span> <i class='fa fa-file-text-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilevideooi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-video-o'></i>"><span></span> <i class='fa fa-file-video-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilewordoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-word-o'></i>"><span></span> <i class='fa fa-file-word-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilezipoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-file-zip-o'></i>"><span></span> <i class='fa fa-file-zip-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilesoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-files-o'></i>"><span></span> <i class='fa fa-files-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilmi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-film'></i>"><span></span> <i class='fa fa-film'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafilteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-filter'></i>"><span></span> <i class='fa fa-filter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafirei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fire'></i>"><span></span> <i class='fa fa-fire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafireextinguisheri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fire-extinguisher'></i>"><span></span> <i class='fa fa-fire-extinguisher'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafirefoxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-firefox'></i>"><span></span> <i class='fa fa-firefox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faflagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-flag'></i>"><span></span> <i class='fa fa-flag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faflagcheckeredi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-flag-checkered'></i>"><span></span> <i class='fa fa-flag-checkered'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faflagoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-flag-o'></i>"><span></span> <i class='fa fa-flag-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faflaski" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-flask'></i>"><span></span> <i class='fa fa-flask'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faflickri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-flickr'></i>"><span></span> <i class='fa fa-flickr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafloppyoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-floppy-o'></i>"><span></span> <i class='fa fa-floppy-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafolderi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-folder'></i>"><span></span> <i class='fa fa-folder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafolderoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-folder-o'></i>"><span></span> <i class='fa fa-folder-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafolderopeni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-folder-open'></i>"><span></span> <i class='fa fa-folder-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafolderopenoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-folder-open-o'></i>"><span></span> <i class='fa fa-folder-open-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafonti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-font'></i>"><span></span> <i class='fa fa-font'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafonticonsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fonticons'></i>"><span></span> <i class='fa fa-fonticons'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faforumbeei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-forumbee'></i>"><span></span> <i class='fa fa-forumbee'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faforwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-forward'></i>"><span></span> <i class='fa fa-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafoursquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-foursquare'></i>"><span></span> <i class='fa fa-foursquare'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafrownoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-frown-o'></i>"><span></span> <i class='fa fa-frown-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafutboloi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-futbol-o'></i>"><span></span> <i class='fa fa-futbol-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagamepadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gamepad'></i>"><span></span> <i class='fa fa-gamepad'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagaveli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gavel'></i>"><span></span> <i class='fa fa-gavel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagbpi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gbp'></i>"><span></span> <i class='fa fa-gbp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ge'></i>"><span></span> <i class='fa fa-ge'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fageari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gear'></i>"><span></span> <i class='fa fa-gear'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagearsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gears'></i>"><span></span> <i class='fa fa-gears'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagetpocketi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-get-pocket'></i>"><span></span> <i class='fa fa-get-pocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faggi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gg'></i>"><span></span> <i class='fa fa-gg'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faggcirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gg-circle'></i>"><span></span> <i class='fa fa-gg-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagifti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gift'></i>"><span></span> <i class='fa fa-gift'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagiti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-git'></i>"><span></span> <i class='fa fa-git'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagitsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-git-square'></i>"><span></span> <i class='fa fa-git-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagithubi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-github'></i>"><span></span> <i class='fa fa-github'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagithubalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-github-alt'></i>"><span></span> <i class='fa fa-github-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagithubsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-github-square'></i>"><span></span> <i class='fa fa-github-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagittipi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gittip'></i>"><span></span> <i class='fa fa-gittip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faglassi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-glass'></i>"><span></span> <i class='fa fa-glass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faglobei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-globe'></i>"><span></span> <i class='fa fa-globe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagooglei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-google'></i>"><span></span> <i class='fa fa-google'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagoogleplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-google-plus'></i>"><span></span> <i class='fa fa-google-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagoogleplussquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-google-plus-square'></i>"><span></span> <i class='fa fa-google-plus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagooglewalleti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-google-wallet'></i>"><span></span> <i class='fa fa-google-wallet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagraduationcapi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-graduation-cap'></i>"><span></span> <i class='fa fa-graduation-cap'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagratipayi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-gratipay'></i>"><span></span> <i class='fa fa-gratipay'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fagroupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-group'></i>"><span></span> <i class='fa fa-group'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-h-square'></i>"><span></span> <i class='fa fa-h-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahackernewsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hacker-news'></i>"><span></span> <i class='fa fa-hacker-news'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandgraboi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-grab-o'></i>"><span></span> <i class='fa fa-hand-grab-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandlizardoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-lizard-o'></i>"><span></span> <i class='fa fa-hand-lizard-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandpaperoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-paper-o'></i>"><span></span> <i class='fa fa-hand-paper-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandpeaceoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-peace-o'></i>"><span></span> <i class='fa fa-hand-peace-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandpointeroi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-pointer-o'></i>"><span></span> <i class='fa fa-hand-pointer-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandrockoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-rock-o'></i>"><span></span> <i class='fa fa-hand-rock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandscissorsoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-scissors-o'></i>"><span></span> <i class='fa fa-hand-scissors-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandspockoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-spock-o'></i>"><span></span> <i class='fa fa-hand-spock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandstopoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-stop-o'></i>"><span></span> <i class='fa fa-hand-stop-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandodowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-o-down'></i>"><span></span> <i class='fa fa-hand-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandolefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-o-left'></i>"><span></span> <i class='fa fa-hand-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandorighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-o-right'></i>"><span></span> <i class='fa fa-hand-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahandoupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hand-o-up'></i>"><span></span> <i class='fa fa-hand-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahddoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hdd-o'></i>"><span></span> <i class='fa fa-hdd-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faheaderi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-header'></i>"><span></span> <i class='fa fa-header'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faheadphonesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-headphones'></i>"><span></span> <i class='fa fa-headphones'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahearti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-heart'></i>"><span></span> <i class='fa fa-heart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faheartoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-heart-o'></i>"><span></span> <i class='fa fa-heart-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faheartbeati" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-heartbeat'></i>"><span></span> <i class='fa fa-heartbeat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahistoryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-history'></i>"><span></span> <i class='fa fa-history'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahomei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-home'></i>"><span></span> <i class='fa fa-home'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahospitaloi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hospital-o'></i>"><span></span> <i class='fa fa-hospital-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahoteli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hotel'></i>"><span></span> <i class='fa fa-hotel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglassi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass'></i>"><span></span> <i class='fa fa-hourglass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglass1i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass-1'></i>"><span></span> <i class='fa fa-hourglass-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglass2i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass-2'></i>"><span></span> <i class='fa fa-hourglass-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglass3i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass-3'></i>"><span></span> <i class='fa fa-hourglass-3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglassendi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass-end'></i>"><span></span> <i class='fa fa-hourglass-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahourglassoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-hourglass-o'></i>"><span></span> <i class='fa fa-hourglass-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahouzzi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-houzz'></i>"><span></span> <i class='fa fa-houzz'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fahtml5i" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-html5'></i>"><span></span> <i class='fa fa-html5'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faicursori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-i-cursor'></i>"><span></span> <i class='fa fa-i-cursor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-failsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ils'></i>"><span></span> <i class='fa fa-ils'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faimagei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-image'></i>"><span></span> <i class='fa fa-image'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainboxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-inbox'></i>"><span></span> <i class='fa fa-inbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faindenti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-indent'></i>"><span></span> <i class='fa fa-indent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faindustryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-industry'></i>"><span></span> <i class='fa fa-industry'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainfoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-info'></i>"><span></span> <i class='fa fa-info'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainfocirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-info-circle'></i>"><span></span> <i class='fa fa-info-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-inr'></i>"><span></span> <i class='fa fa-inr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainstagrami" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-instagram'></i>"><span></span> <i class='fa fa-instagram'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainstitutioni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-institution'></i>"><span></span> <i class='fa fa-institution'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fainternetexploreri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-internet-explorer'></i>"><span></span> <i class='fa fa-internet-explorer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faioxhosti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ioxhost'></i>"><span></span> <i class='fa fa-ioxhost'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faitalici" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-italic'></i>"><span></span> <i class='fa fa-italic'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fajoomlai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-joomla'></i>"><span></span> <i class='fa fa-joomla'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fajpyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-jpy'></i>"><span></span> <i class='fa fa-jpy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fajsfiddlei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-jsfiddle'></i>"><span></span> <i class='fa fa-jsfiddle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fakeyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-key'></i>"><span></span> <i class='fa fa-key'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fakeyboardoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-keyboard-o'></i>"><span></span> <i class='fa fa-keyboard-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fakrwi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-krw'></i>"><span></span> <i class='fa fa-krw'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falanguagei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-language'></i>"><span></span> <i class='fa fa-language'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falaptopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-laptop'></i>"><span></span> <i class='fa fa-laptop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falegali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-legal'></i>"><span></span> <i class='fa fa-legal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falemonoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-lemon-o'></i>"><span></span> <i class='fa fa-lemon-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faleveldowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-level-down'></i>"><span></span> <i class='fa fa-level-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falevelupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-level-up'></i>"><span></span> <i class='fa fa-level-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faliferingi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-life-ring'></i>"><span></span> <i class='fa fa-life-ring'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falightbulboi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-lightbulb-o'></i>"><span></span> <i class='fa fa-lightbulb-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falinecharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-line-chart'></i>"><span></span> <i class='fa fa-line-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falinki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-link'></i>"><span></span> <i class='fa fa-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falinkedini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-linkedin'></i>"><span></span> <i class='fa fa-linkedin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falinkedinsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-linkedin-square'></i>"><span></span> <i class='fa fa-linkedin-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falinuxi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-linux'></i>"><span></span> <i class='fa fa-linux'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falistalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-list-alt'></i>"><span></span> <i class='fa fa-list-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falistoli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-list-ol'></i>"><span></span> <i class='fa fa-list-ol'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falistuli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-list-ul'></i>"><span></span> <i class='fa fa-list-ul'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falocationarrowi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-location-arrow'></i>"><span></span> <i class='fa fa-location-arrow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falocki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-lock'></i>"><span></span> <i class='fa fa-lock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falongarrowdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-long-arrow-down'></i>"><span></span> <i class='fa fa-long-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falongarrowlefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-long-arrow-left'></i>"><span></span> <i class='fa fa-long-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falongarrowrighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-long-arrow-right'></i>"><span></span> <i class='fa fa-long-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-falongarrowupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-long-arrow-up'></i>"><span></span> <i class='fa fa-long-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famagici" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-magic'></i>"><span></span> <i class='fa fa-magic'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famagneti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-magnet'></i>"><span></span> <i class='fa fa-magnet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famalei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-male'></i>"><span></span> <i class='fa fa-male'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famapi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-map'></i>"><span></span> <i class='fa fa-map'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famapoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-map-o'></i>"><span></span> <i class='fa fa-map-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famapmarkeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-map-marker'></i>"><span></span> <i class='fa fa-map-marker'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famappini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-map-pin'></i>"><span></span> <i class='fa fa-map-pin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famapsignsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-map-signs'></i>"><span></span> <i class='fa fa-map-signs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famarsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mars'></i>"><span></span> <i class='fa fa-mars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famarsdoublei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mars-double'></i>"><span></span> <i class='fa fa-mars-double'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famarsstrokei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mars-stroke'></i>"><span></span> <i class='fa fa-mars-stroke'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famarsstrokehi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mars-stroke-h'></i>"><span></span> <i class='fa fa-mars-stroke-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famarsstrokevi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mars-stroke-v'></i>"><span></span> <i class='fa fa-mars-stroke-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famaxcdni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-maxcdn'></i>"><span></span> <i class='fa fa-maxcdn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fameanpathi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-meanpath'></i>"><span></span> <i class='fa fa-meanpath'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famediumi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-medium'></i>"><span></span> <i class='fa fa-medium'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famedkiti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-medkit'></i>"><span></span> <i class='fa fa-medkit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famehoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-meh-o'></i>"><span></span> <i class='fa fa-meh-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famercuryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mercury'></i>"><span></span> <i class='fa fa-mercury'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famicrophonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-microphone'></i>"><span></span> <i class='fa fa-microphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famicrophoneslashi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-microphone-slash'></i>"><span></span> <i class='fa fa-microphone-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faminusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-minus'></i>"><span></span> <i class='fa fa-minus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faminuscirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-minus-circle'></i>"><span></span> <i class='fa fa-minus-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faminussquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-minus-square'></i>"><span></span> <i class='fa fa-minus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faminussquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-minus-square-o'></i>"><span></span> <i class='fa fa-minus-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famobilei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mobile'></i>"><span></span> <i class='fa fa-mobile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famoneyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-money'></i>"><span></span> <i class='fa fa-money'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famoonoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-moon-o'></i>"><span></span> <i class='fa fa-moon-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famortarboardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mortar-board'></i>"><span></span> <i class='fa fa-mortar-board'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famotorcyclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-motorcycle'></i>"><span></span> <i class='fa fa-motorcycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famousepointeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-mouse-pointer'></i>"><span></span> <i class='fa fa-mouse-pointer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-famusici" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-music'></i>"><span></span> <i class='fa fa-music'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fanaviconi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-navicon'></i>"><span></span> <i class='fa fa-navicon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faneuteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-neuter'></i>"><span></span> <i class='fa fa-neuter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fanewspaperoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-newspaper-o'></i>"><span></span> <i class='fa fa-newspaper-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faobjectgroupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-object-group'></i>"><span></span> <i class='fa fa-object-group'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faobjectungroupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-object-ungroup'></i>"><span></span> <i class='fa fa-object-ungroup'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faodnoklassnikii" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-odnoklassniki'></i>"><span></span> <i class='fa fa-odnoklassniki'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faodnoklassnikisquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-odnoklassniki-square'></i>"><span></span> <i class='fa fa-odnoklassniki-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faopencarti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-opencart'></i>"><span></span> <i class='fa fa-opencart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faopenidi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-openid'></i>"><span></span> <i class='fa fa-openid'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faoperai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-opera'></i>"><span></span> <i class='fa fa-opera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faoptinmonsteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-optin-monster'></i>"><span></span> <i class='fa fa-optin-monster'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faoutdenti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-outdent'></i>"><span></span> <i class='fa fa-outdent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapagelinesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pagelines'></i>"><span></span> <i class='fa fa-pagelines'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapaintbrushi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paint-brush'></i>"><span></span> <i class='fa fa-paint-brush'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapaperclipi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paperclip'></i>"><span></span> <i class='fa fa-paperclip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faparagraphi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paragraph'></i>"><span></span> <i class='fa fa-paragraph'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapastei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paste'></i>"><span></span> <i class='fa fa-paste'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapausei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pause'></i>"><span></span> <i class='fa fa-pause'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapawi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paw'></i>"><span></span> <i class='fa fa-paw'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapaypali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-paypal'></i>"><span></span> <i class='fa fa-paypal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapencili" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pencil'></i>"><span></span> <i class='fa fa-pencil'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapencilsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pencil-square'></i>"><span></span> <i class='fa fa-pencil-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapencilsquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pencil-square-o'></i>"><span></span> <i class='fa fa-pencil-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faphonei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-phone'></i>"><span></span> <i class='fa fa-phone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faphonesquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-phone-square'></i>"><span></span> <i class='fa fa-phone-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faphotoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-photo'></i>"><span></span> <i class='fa fa-photo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapictureoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-picture-o'></i>"><span></span> <i class='fa fa-picture-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapiecharti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pie-chart'></i>"><span></span> <i class='fa fa-pie-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapiedpiperi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pied-piper'></i>"><span></span> <i class='fa fa-pied-piper'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapiedpiperalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pied-piper-alt'></i>"><span></span> <i class='fa fa-pied-piper-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapinteresti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pinterest'></i>"><span></span> <i class='fa fa-pinterest'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapinterestpi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pinterest-p'></i>"><span></span> <i class='fa fa-pinterest-p'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapinterestsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-pinterest-square'></i>"><span></span> <i class='fa fa-pinterest-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplanei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plane'></i>"><span></span> <i class='fa fa-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplayi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-play'></i>"><span></span> <i class='fa fa-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplaycirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-play-circle'></i>"><span></span> <i class='fa fa-play-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplaycircleoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-play-circle-o'></i>"><span></span> <i class='fa fa-play-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplugi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plug'></i>"><span></span> <i class='fa fa-plug'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plus'></i>"><span></span> <i class='fa fa-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapluscirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plus-circle'></i>"><span></span> <i class='fa fa-plus-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplussquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plus-square'></i>"><span></span> <i class='fa fa-plus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faplussquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-plus-square-o'></i>"><span></span> <i class='fa fa-plus-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapoweroffi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-power-off'></i>"><span></span> <i class='fa fa-power-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faprinti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-print'></i>"><span></span> <i class='fa fa-print'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fapuzzlepiecei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-puzzle-piece'></i>"><span></span> <i class='fa fa-puzzle-piece'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faqqi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-qq'></i>"><span></span> <i class='fa fa-qq'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faqrcodei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-qrcode'></i>"><span></span> <i class='fa fa-qrcode'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faquestioni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-question'></i>"><span></span> <i class='fa fa-question'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faquestioncirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-question-circle'></i>"><span></span> <i class='fa fa-question-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faquotelefti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-quote-left'></i>"><span></span> <i class='fa fa-quote-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faquoterighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-quote-right'></i>"><span></span> <i class='fa fa-quote-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farandomi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-random'></i>"><span></span> <i class='fa fa-random'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farebeli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-rebel'></i>"><span></span> <i class='fa fa-rebel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farecyclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-recycle'></i>"><span></span> <i class='fa fa-recycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faredditi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-reddit'></i>"><span></span> <i class='fa fa-reddit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faredditsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-reddit-square'></i>"><span></span> <i class='fa fa-reddit-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farefreshi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-refresh'></i>"><span></span> <i class='fa fa-refresh'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faregisteredi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-registered'></i>"><span></span> <i class='fa fa-registered'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faremovei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-remove'></i>"><span></span> <i class='fa fa-remove'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farenreni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-renren'></i>"><span></span> <i class='fa fa-renren'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farepeati" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-repeat'></i>"><span></span> <i class='fa fa-repeat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fareplyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-reply'></i>"><span></span> <i class='fa fa-reply'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fareplyalli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-reply-all'></i>"><span></span> <i class='fa fa-reply-all'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faretweeti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-retweet'></i>"><span></span> <i class='fa fa-retweet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farmbi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-rmb'></i>"><span></span> <i class='fa fa-rmb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faroadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-road'></i>"><span></span> <i class='fa fa-road'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-farocketi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-rocket'></i>"><span></span> <i class='fa fa-rocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faroublei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-rouble'></i>"><span></span> <i class='fa fa-rouble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasafarii" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-safari'></i>"><span></span> <i class='fa fa-safari'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasavei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-save'></i>"><span></span> <i class='fa fa-save'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fascissorsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-scissors'></i>"><span></span> <i class='fa fa-scissors'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasearchi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-search'></i>"><span></span> <i class='fa fa-search'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasearchminusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-search-minus'></i>"><span></span> <i class='fa fa-search-minus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasearchplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-search-plus'></i>"><span></span> <i class='fa fa-search-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasellsyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sellsy'></i>"><span></span> <i class='fa fa-sellsy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasendi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-send'></i>"><span></span> <i class='fa fa-send'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasendoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-send-o'></i>"><span></span> <i class='fa fa-send-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faserveri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-server'></i>"><span></span> <i class='fa fa-server'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasharei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-share'></i>"><span></span> <i class='fa fa-share'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasharesquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-share-square'></i>"><span></span> <i class='fa fa-share-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasharesquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-share-square-o'></i>"><span></span> <i class='fa fa-share-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasharealti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-share-alt'></i>"><span></span> <i class='fa fa-share-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasharealtsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-share-alt-square'></i>"><span></span> <i class='fa fa-share-alt-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fashekeli" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-shekel'></i>"><span></span> <i class='fa fa-shekel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fashieldi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-shield'></i>"><span></span> <i class='fa fa-shield'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fashipi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-ship'></i>"><span></span> <i class='fa fa-ship'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fashirtsinbulki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-shirtsinbulk'></i>"><span></span> <i class='fa fa-shirtsinbulk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fashoppingcarti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-shopping-cart'></i>"><span></span> <i class='fa fa-shopping-cart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasignini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sign-in'></i>"><span></span> <i class='fa fa-sign-in'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasignouti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sign-out'></i>"><span></span> <i class='fa fa-sign-out'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasignali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-signal'></i>"><span></span> <i class='fa fa-signal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasimplybuilti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-simplybuilt'></i>"><span></span> <i class='fa fa-simplybuilt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasitemapi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sitemap'></i>"><span></span> <i class='fa fa-sitemap'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faskyatlasi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-skyatlas'></i>"><span></span> <i class='fa fa-skyatlas'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faskypei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-skype'></i>"><span></span> <i class='fa fa-skype'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faslacki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-slack'></i>"><span></span> <i class='fa fa-slack'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faslidersi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sliders'></i>"><span></span> <i class='fa fa-sliders'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faslidesharei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-slideshare'></i>"><span></span> <i class='fa fa-slideshare'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasmileoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-smile-o'></i>"><span></span> <i class='fa fa-smile-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasoccerballoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-soccer-ball-o'></i>"><span></span> <i class='fa fa-soccer-ball-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasorti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort'></i>"><span></span> <i class='fa fa-sort'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortalphaasci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-alpha-asc'></i>"><span></span> <i class='fa fa-sort-alpha-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortalphadesci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-alpha-desc'></i>"><span></span> <i class='fa fa-sort-alpha-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortamountasci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-amount-asc'></i>"><span></span> <i class='fa fa-sort-amount-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortamountdesci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-amount-desc'></i>"><span></span> <i class='fa fa-sort-amount-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortnumericasci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-numeric-asc'></i>"><span></span> <i class='fa fa-sort-numeric-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasortnumericdesci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sort-numeric-desc'></i>"><span></span> <i class='fa fa-sort-numeric-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasoundcloudi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-soundcloud'></i>"><span></span> <i class='fa fa-soundcloud'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faspaceshuttlei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-space-shuttle'></i>"><span></span> <i class='fa fa-space-shuttle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faspinneri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-spinner'></i>"><span></span> <i class='fa fa-spinner'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faspooni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-spoon'></i>"><span></span> <i class='fa fa-spoon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faspotifyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-spotify'></i>"><span></span> <i class='fa fa-spotify'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-square'></i>"><span></span> <i class='fa fa-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasquareoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-square-o'></i>"><span></span> <i class='fa fa-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastackexchangei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stack-exchange'></i>"><span></span> <i class='fa fa-stack-exchange'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastackoverflowi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stack-overflow'></i>"><span></span> <i class='fa fa-stack-overflow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastari" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-star'></i>"><span></span> <i class='fa fa-star'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastarhalfi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-star-half'></i>"><span></span> <i class='fa fa-star-half'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastarhalfoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-star-half-o'></i>"><span></span> <i class='fa fa-star-half-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastaroi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-star-o'></i>"><span></span> <i class='fa fa-star-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasteami" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-steam'></i>"><span></span> <i class='fa fa-steam'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasteamsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-steam-square'></i>"><span></span> <i class='fa fa-steam-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastepbackwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-step-backward'></i>"><span></span> <i class='fa fa-step-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastepforwardi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-step-forward'></i>"><span></span> <i class='fa fa-step-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastethoscopei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stethoscope'></i>"><span></span> <i class='fa fa-stethoscope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastickynotei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sticky-note'></i>"><span></span> <i class='fa fa-sticky-note'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastickynoteoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sticky-note-o'></i>"><span></span> <i class='fa fa-sticky-note-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastopi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stop'></i>"><span></span> <i class='fa fa-stop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastreetviewi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-street-view'></i>"><span></span> <i class='fa fa-street-view'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastrikethroughi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-strikethrough'></i>"><span></span> <i class='fa fa-strikethrough'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastumbleuponi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stumbleupon'></i>"><span></span> <i class='fa fa-stumbleupon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fastumbleuponcirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-stumbleupon-circle'></i>"><span></span> <i class='fa fa-stumbleupon-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasubscripti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-subscript'></i>"><span></span> <i class='fa fa-subscript'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasubwayi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-subway'></i>"><span></span> <i class='fa fa-subway'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasuitcasei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-suitcase'></i>"><span></span> <i class='fa fa-suitcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasunoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-sun-o'></i>"><span></span> <i class='fa fa-sun-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fasuperscripti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-superscript'></i>"><span></span> <i class='fa fa-superscript'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatablei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-table'></i>"><span></span> <i class='fa fa-table'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatableti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tablet'></i>"><span></span> <i class='fa fa-tablet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatachometeri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tachometer'></i>"><span></span> <i class='fa fa-tachometer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatagi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tag'></i>"><span></span> <i class='fa fa-tag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatagsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tags'></i>"><span></span> <i class='fa fa-tags'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatasksi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tasks'></i>"><span></span> <i class='fa fa-tasks'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fataxii" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-taxi'></i>"><span></span> <i class='fa fa-taxi'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatelevisioni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-television'></i>"><span></span> <i class='fa fa-television'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatencentweiboi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tencent-weibo'></i>"><span></span> <i class='fa fa-tencent-weibo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faterminali" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-terminal'></i>"><span></span> <i class='fa fa-terminal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatextheighti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-text-height'></i>"><span></span> <i class='fa fa-text-height'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatextwidthi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-text-width'></i>"><span></span> <i class='fa fa-text-width'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-th'></i>"><span></span> <i class='fa fa-th'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathlargei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-th-large'></i>"><span></span> <i class='fa fa-th-large'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathumbtacki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-thumb-tack'></i>"><span></span> <i class='fa fa-thumb-tack'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathumbsdowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-thumbs-down'></i>"><span></span> <i class='fa fa-thumbs-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathumbsupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-thumbs-up'></i>"><span></span> <i class='fa fa-thumbs-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathumbsodowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-thumbs-o-down'></i>"><span></span> <i class='fa fa-thumbs-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fathumbsoupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-thumbs-o-up'></i>"><span></span> <i class='fa fa-thumbs-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatimesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-times'></i>"><span></span> <i class='fa fa-times'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatimescirclei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-times-circle'></i>"><span></span> <i class='fa fa-times-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatimescircleoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-times-circle-o'></i>"><span></span> <i class='fa fa-times-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatinti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tint'></i>"><span></span> <i class='fa fa-tint'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatoggleoffi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-toggle-off'></i>"><span></span> <i class='fa fa-toggle-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatoggleoni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-toggle-on'></i>"><span></span> <i class='fa fa-toggle-on'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrademarki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-trademark'></i>"><span></span> <i class='fa fa-trademark'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatraini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-train'></i>"><span></span> <i class='fa fa-train'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatransgenderi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-transgender'></i>"><span></span> <i class='fa fa-transgender'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatransgenderalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-transgender-alt'></i>"><span></span> <i class='fa fa-transgender-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrashi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-trash'></i>"><span></span> <i class='fa fa-trash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrashoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-trash-o'></i>"><span></span> <i class='fa fa-trash-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatreei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tree'></i>"><span></span> <i class='fa fa-tree'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrelloi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-trello'></i>"><span></span> <i class='fa fa-trello'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatripadvisori" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tripadvisor'></i>"><span></span> <i class='fa fa-tripadvisor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrophyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-trophy'></i>"><span></span> <i class='fa fa-trophy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatrucki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-truck'></i>"><span></span> <i class='fa fa-truck'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatryi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-try'></i>"><span></span> <i class='fa fa-try'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fattyi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tty'></i>"><span></span> <i class='fa fa-tty'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatumblri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tumblr'></i>"><span></span> <i class='fa fa-tumblr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatumblrsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-tumblr-square'></i>"><span></span> <i class='fa fa-tumblr-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatwitchi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-twitch'></i>"><span></span> <i class='fa fa-twitch'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatwitteri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-twitter'></i>"><span></span> <i class='fa fa-twitter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fatwittersquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-twitter-square'></i>"><span></span> <i class='fa fa-twitter-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favenusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-venus'></i>"><span></span> <i class='fa fa-venus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favenusdoublei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-venus-double'></i>"><span></span> <i class='fa fa-venus-double'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favenusmarsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-venus-mars'></i>"><span></span> <i class='fa fa-venus-mars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faviacoini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-viacoin'></i>"><span></span> <i class='fa fa-viacoin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favideocamerai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-video-camera'></i>"><span></span> <i class='fa fa-video-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favimeoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-vimeo'></i>"><span></span> <i class='fa fa-vimeo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favimeosquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-vimeo-square'></i>"><span></span> <i class='fa fa-vimeo-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favinei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-vine'></i>"><span></span> <i class='fa fa-vine'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-vk'></i>"><span></span> <i class='fa fa-vk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favolumedowni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-volume-down'></i>"><span></span> <i class='fa fa-volume-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favolumeoffi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-volume-off'></i>"><span></span> <i class='fa fa-volume-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-favolumeupi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-volume-up'></i>"><span></span> <i class='fa fa-volume-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faumbrellai" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-umbrella'></i>"><span></span> <i class='fa fa-umbrella'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faunderlinei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-underline'></i>"><span></span> <i class='fa fa-underline'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faundoi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-undo'></i>"><span></span> <i class='fa fa-undo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fauniversityi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-university'></i>"><span></span> <i class='fa fa-university'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faunlinki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-unlink'></i>"><span></span> <i class='fa fa-unlink'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faunlocki" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-unlock'></i>"><span></span> <i class='fa fa-unlock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faunlockalti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-unlock-alt'></i>"><span></span> <i class='fa fa-unlock-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fauploadi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-upload'></i>"><span></span> <i class='fa fa-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fauseri" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-user'></i>"><span></span> <i class='fa fa-user'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fausermdi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-user-md'></i>"><span></span> <i class='fa fa-user-md'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fauserplusi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-user-plus'></i>"><span></span> <i class='fa fa-user-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fausersecreti" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-user-secret'></i>"><span></span> <i class='fa fa-user-secret'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fausertimesi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-user-times'></i>"><span></span> <i class='fa fa-user-times'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fausersi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-users'></i>"><span></span> <i class='fa fa-users'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawechati" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wechat'></i>"><span></span> <i class='fa fa-wechat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faweiboi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-weibo'></i>"><span></span> <i class='fa fa-weibo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faweixini" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-weixin'></i>"><span></span> <i class='fa fa-weixin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawhatsappi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-whatsapp'></i>"><span></span> <i class='fa fa-whatsapp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawheelchairi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wheelchair'></i>"><span></span> <i class='fa fa-wheelchair'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawifii" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wifi'></i>"><span></span> <i class='fa fa-wifi'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawikipediawi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wikipedia-w'></i>"><span></span> <i class='fa fa-wikipedia-w'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawindowsi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-windows'></i>"><span></span> <i class='fa fa-windows'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawoni" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-won'></i>"><span></span> <i class='fa fa-won'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawordpressi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wordpress'></i>"><span></span> <i class='fa fa-wordpress'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fawrenchi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-wrench'></i>"><span></span> <i class='fa fa-wrench'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faxingi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-xing'></i>"><span></span> <i class='fa fa-xing'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faxingsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-xing-square'></i>"><span></span> <i class='fa fa-xing-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fayahooi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-yahoo'></i>"><span></span> <i class='fa fa-yahoo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fafayci" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-fa-yc'></i>"><span></span> <i class='fa fa-fa-yc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-faycsquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-yc-square'></i>"><span></span> <i class='fa fa-yc-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fayelpi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-yelp'></i>"><span></span> <i class='fa fa-yelp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fayoutubei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-youtube'></i>"><span></span> <i class='fa fa-youtube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fayoutubeplayi" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-youtube-play'></i>"><span></span> <i class='fa fa-youtube-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="abstract_modules_icon_i-classfa-fayoutubesquarei" name="abstract_modules_icon" class="abstract_modules_icon"  value="<i class='fa fa-youtube-square'></i>"><span></span> <i class='fa fa-youtube-square'></i>
																</label>
																<div id="abstract_modules_icon-check-loading"><?php echo translate("Loading"); ?>...</div>
																<div class="help-block"><?php echo translate("Make menu of module friendly with icon in backend mode"); ?></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-list push-5-r"></i> <?php echo translate("Backend Menu"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-12" for="abstract_modules_category"><?php echo translate("Category"); ?></label>
											<div class="col-md-12">
												<input class="form-control abstract_modules_category" type="text" id="abstract_modules_category" name="abstract_modules_category" value="" placeholder="<?php echo translate("Category of module");?>" >

												<div class="help-block"><?php echo translate("Categorize module to make it more easier to find in side menu in backend mode"); ?></div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-12" for="abstract_modules_subject"><?php echo translate("Subject"); ?></label>
											<div class="col-md-12">
												<input class="form-control abstract_modules_subject" type="text" id="abstract_modules_subject" name="abstract_modules_subject" value="" placeholder="<?php echo translate("Subject of module");?>" >

												<div class="help-block"><?php echo translate("Put menu of module inside a subject menu (Parent menu) in backend mode"); ?></div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12">
												<div id="subject_icon_list_parent" class="panel-group">
													<div class="panel panel-default">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#subject_icon_list_parent" href="#subject_icon_list">
															<div class="panel-heading">
																<span class="panel-title"><?php echo translate("Subject Icon"); ?></span> <i class="fa fa-angle-down"></i>
															</div>
														</a>

														<div id="subject_icon_list" class="panel-collapse collapse">
															<div class="row">
																<div class="col-md-12">

																	<div class="col-md-12">
																		<div class="form-group">
																			<div class="col-md-12">

																					<div class="row items-push">
																						<div class="col-md-12" style="margin-bottom: 0;">
																							<label class="css-input css-radio css-radio-default push-10-r">
																								<input type="radio" id="abstract_modules_subject_icon_" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon" checked  value=""><span></span> <?php echo translate("None"); ?>
																							</label>
																						</div>
																					</div>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siactionredoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-action-redo'></i>"><span></span> <i class='si si-action-redo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siactionundoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-action-undo'></i>"><span></span> <i class='si si-action-undo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siarrowdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-arrow-down'></i>"><span></span> <i class='si si-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siarrowlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-arrow-left'></i>"><span></span> <i class='si si-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siarrowrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-arrow-right'></i>"><span></span> <i class='si si-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siarrowupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-arrow-up'></i>"><span></span> <i class='si si-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sianchori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-anchor'></i>"><span></span> <i class='si si-anchor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibadgei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-badge'></i>"><span></span> <i class='si si-badge'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bag'></i>"><span></span> <i class='si si-bag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibani" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-ban'></i>"><span></span> <i class='si si-ban'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibarcharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bar-chart'></i>"><span></span> <i class='si si-bar-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibasketi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-basket'></i>"><span></span> <i class='si si-basket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibasketloadedi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-basket-loaded'></i>"><span></span> <i class='si si-basket-loaded'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibelli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bell'></i>"><span></span> <i class='si si-bell'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibookopeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-book-open'></i>"><span></span> <i class='si si-book-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibriefcasei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-briefcase'></i>"><span></span> <i class='si si-briefcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibubblei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bubble'></i>"><span></span> <i class='si si-bubble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibubblesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bubbles'></i>"><span></span> <i class='si si-bubbles'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sibulbi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-bulb'></i>"><span></span> <i class='si si-bulb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicalculatori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-calculator'></i>"><span></span> <i class='si si-calculator'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicalendari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-calendar'></i>"><span></span> <i class='si si-calendar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicallendi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-call-end'></i>"><span></span> <i class='si si-call-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicallini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-call-in'></i>"><span></span> <i class='si si-call-in'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicallouti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-call-out'></i>"><span></span> <i class='si si-call-out'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicamcorderi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-camcorder'></i>"><span></span> <i class='si si-camcorder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicamerai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-camera'></i>"><span></span> <i class='si si-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sichecki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-check'></i>"><span></span> <i class='si si-check'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sichemistryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-chemistry'></i>"><span></span> <i class='si si-chemistry'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siclocki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-clock'></i>"><span></span> <i class='si si-clock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siclouddownloadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-cloud-download'></i>"><span></span> <i class='si si-cloud-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siclouduploadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-cloud-upload'></i>"><span></span> <i class='si si-cloud-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicompassi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-compass'></i>"><span></span> <i class='si si-compass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolendi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-end'></i>"><span></span> <i class='si si-control-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolforwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-forward'></i>"><span></span> <i class='si si-control-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolplayi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-play'></i>"><span></span> <i class='si si-control-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolpausei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-pause'></i>"><span></span> <i class='si si-control-pause'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolrewindi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-rewind'></i>"><span></span> <i class='si si-control-rewind'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicontrolstarti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-control-start'></i>"><span></span> <i class='si si-control-start'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicreditcardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-credit-card'></i>"><span></span> <i class='si si-credit-card'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicropi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-crop'></i>"><span></span> <i class='si si-crop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-cup'></i>"><span></span> <i class='si si-cup'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicursori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-cursor'></i>"><span></span> <i class='si si-cursor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sicursormovei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-cursor-move'></i>"><span></span> <i class='si si-cursor-move'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidiamondi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-diamond'></i>"><span></span> <i class='si si-diamond'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidirectioni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-direction'></i>"><span></span> <i class='si si-direction'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidirectionsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-directions'></i>"><span></span> <i class='si si-directions'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidisci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-disc'></i>"><span></span> <i class='si si-disc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidislikei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-dislike'></i>"><span></span> <i class='si si-dislike'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidoci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-doc'></i>"><span></span> <i class='si si-doc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidocsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-docs'></i>"><span></span> <i class='si si-docs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidraweri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-drawer'></i>"><span></span> <i class='si si-drawer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sidropi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-drop'></i>"><span></span> <i class='si si-drop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siearphonesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-earphones'></i>"><span></span> <i class='si si-earphones'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siearphonesalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-earphones-alt'></i>"><span></span> <i class='si si-earphones-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siemoticonsmilei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-emoticon-smile'></i>"><span></span> <i class='si si-emoticon-smile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sienergyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-energy'></i>"><span></span> <i class='si si-energy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sienvelopei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-envelope'></i>"><span></span> <i class='si si-envelope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sienvelopeletteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-envelope-letter'></i>"><span></span> <i class='si si-envelope-letter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sienvelopeopeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-envelope-open'></i>"><span></span> <i class='si si-envelope-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siequalizeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-equalizer'></i>"><span></span> <i class='si si-equalizer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sieyei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-eye'></i>"><span></span> <i class='si si-eye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sieyeglassesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-eyeglasses'></i>"><span></span> <i class='si si-eyeglasses'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sifeedi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-feed'></i>"><span></span> <i class='si si-feed'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sifilmi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-film'></i>"><span></span> <i class='si si-film'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sifirei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-fire'></i>"><span></span> <i class='si si-fire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siflagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-flag'></i>"><span></span> <i class='si si-flag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sifolderi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-folder'></i>"><span></span> <i class='si si-folder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sifolderalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-folder-alt'></i>"><span></span> <i class='si si-folder-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siframei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-frame'></i>"><span></span> <i class='si si-frame'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sigamecontrolleri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-game-controller'></i>"><span></span> <i class='si si-game-controller'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sighosti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-ghost'></i>"><span></span> <i class='si si-ghost'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siglobei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-globe'></i>"><span></span> <i class='si si-globe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siglobealti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-globe-alt'></i>"><span></span> <i class='si si-globe-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sigraduationi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-graduation'></i>"><span></span> <i class='si si-graduation'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sigraphi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-graph'></i>"><span></span> <i class='si si-graph'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sigridi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-grid'></i>"><span></span> <i class='si si-grid'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sihandbagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-handbag'></i>"><span></span> <i class='si si-handbag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sihearti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-heart'></i>"><span></span> <i class='si si-heart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sihomei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-home'></i>"><span></span> <i class='si si-home'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sihourglassi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-hourglass'></i>"><span></span> <i class='si si-hourglass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siinfoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-info'></i>"><span></span> <i class='si si-info'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sikeyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-key'></i>"><span></span> <i class='si si-key'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silayersi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-layers'></i>"><span></span> <i class='si si-layers'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silikei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-like'></i>"><span></span> <i class='si si-like'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silinki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-link'></i>"><span></span> <i class='si si-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silisti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-list'></i>"><span></span> <i class='si si-list'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silocki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-lock'></i>"><span></span> <i class='si si-lock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silockopeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-lock-open'></i>"><span></span> <i class='si si-lock-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silogini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-login'></i>"><span></span> <i class='si si-login'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-silogouti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-logout'></i>"><span></span> <i class='si si-logout'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siloopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-loop'></i>"><span></span> <i class='si si-loop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simagicwandi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-magic-wand'></i>"><span></span> <i class='si si-magic-wand'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simagneti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-magnet'></i>"><span></span> <i class='si si-magnet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simagnifieri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-magnifier'></i>"><span></span> <i class='si si-magnifier'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simagnifieraddi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-magnifier-add'></i>"><span></span> <i class='si si-magnifier-add'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simagnifierremovei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-magnifier-remove'></i>"><span></span> <i class='si si-magnifier-remove'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simapi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-map'></i>"><span></span> <i class='si si-map'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simicrophonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-microphone'></i>"><span></span> <i class='si si-microphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simousei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-mouse'></i>"><span></span> <i class='si si-mouse'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simoustachei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-moustache'></i>"><span></span> <i class='si si-moustache'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simusictonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-music-tone'></i>"><span></span> <i class='si si-music-tone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-simusictonealti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-music-tone-alt'></i>"><span></span> <i class='si si-music-tone-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sinotei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-note'></i>"><span></span> <i class='si si-note'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sinotebooki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-notebook'></i>"><span></span> <i class='si si-notebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipaperclipi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-paper-clip'></i>"><span></span> <i class='si si-paper-clip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipaperplanei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-paper-plane'></i>"><span></span> <i class='si si-paper-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipencili" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-pencil'></i>"><span></span> <i class='si si-pencil'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipicturei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-picture'></i>"><span></span> <i class='si si-picture'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipiecharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-pie-chart'></i>"><span></span> <i class='si si-pie-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-pin'></i>"><span></span> <i class='si si-pin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siplanei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-plane'></i>"><span></span> <i class='si si-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siplaylisti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-playlist'></i>"><span></span> <i class='si si-playlist'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-plus'></i>"><span></span> <i class='si si-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipointeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-pointer'></i>"><span></span> <i class='si si-pointer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipoweri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-power'></i>"><span></span> <i class='si si-power'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipresenti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-present'></i>"><span></span> <i class='si si-present'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siprinteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-printer'></i>"><span></span> <i class='si si-printer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sipuzzlei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-puzzle'></i>"><span></span> <i class='si si-puzzle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siquestioni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-question'></i>"><span></span> <i class='si si-question'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sirefreshi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-refresh'></i>"><span></span> <i class='si si-refresh'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sireloadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-reload'></i>"><span></span> <i class='si si-reload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sirocketi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-rocket'></i>"><span></span> <i class='si si-rocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siscreendesktopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-screen-desktop'></i>"><span></span> <i class='si si-screen-desktop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siscreensmartphonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-screen-smartphone'></i>"><span></span> <i class='si si-screen-smartphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siscreentableti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-screen-tablet'></i>"><span></span> <i class='si si-screen-tablet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisettingsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-settings'></i>"><span></span> <i class='si si-settings'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisharei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-share'></i>"><span></span> <i class='si si-share'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisharealti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-share-alt'></i>"><span></span> <i class='si si-share-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sishieldi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-shield'></i>"><span></span> <i class='si si-shield'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sishufflei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-shuffle'></i>"><span></span> <i class='si si-shuffle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisizeactuali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-size-actual'></i>"><span></span> <i class='si si-size-actual'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisizefullscreeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-size-fullscreen'></i>"><span></span> <i class='si si-size-fullscreen'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialdribbblei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-dribbble'></i>"><span></span> <i class='si si-social-dribbble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialdropboxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-dropbox'></i>"><span></span> <i class='si si-social-dropbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialfacebooki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-facebook'></i>"><span></span> <i class='si si-social-facebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialtumblri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-tumblr'></i>"><span></span> <i class='si si-social-tumblr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialtwitteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-twitter'></i>"><span></span> <i class='si si-social-twitter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisocialyoutubei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-social-youtube'></i>"><span></span> <i class='si si-social-youtube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sispeechi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-speech'></i>"><span></span> <i class='si si-speech'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sispeedometeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-speedometer'></i>"><span></span> <i class='si si-speedometer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sistari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-star'></i>"><span></span> <i class='si si-star'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisupporti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-support'></i>"><span></span> <i class='si si-support'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisymbolfemalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-symbol-female'></i>"><span></span> <i class='si si-symbol-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sisymbolmalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-symbol-male'></i>"><span></span> <i class='si si-symbol-male'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sitagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-tag'></i>"><span></span> <i class='si si-tag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sitargeti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-target'></i>"><span></span> <i class='si si-target'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sitrashi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-trash'></i>"><span></span> <i class='si si-trash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sitrophyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-trophy'></i>"><span></span> <i class='si si-trophy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siumbrellai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-umbrella'></i>"><span></span> <i class='si si-umbrella'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siuseri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-user'></i>"><span></span> <i class='si si-user'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siuserfemalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-user-female'></i>"><span></span> <i class='si si-user-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siuserfollowi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-user-follow'></i>"><span></span> <i class='si si-user-follow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siuserfollowingi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-user-following'></i>"><span></span> <i class='si si-user-following'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siuserunfollowi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-user-unfollow'></i>"><span></span> <i class='si si-user-unfollow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siusersi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-users'></i>"><span></span> <i class='si si-users'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sivectori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-vector'></i>"><span></span> <i class='si si-vector'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sivolume1i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-volume-1'></i>"><span></span> <i class='si si-volume-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sivolume2i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-volume-2'></i>"><span></span> <i class='si si-volume-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-sivolumeoffi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-volume-off'></i>"><span></span> <i class='si si-volume-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siwalleti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-wallet'></i>"><span></span> <i class='si si-wallet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classsi-siwrenchi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='si si-wrench'></i>"><span></span> <i class='si si-wrench'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fa500pxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-500px'></i>"><span></span> <i class='fa fa-500px'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faadjusti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-adjust'></i>"><span></span> <i class='fa fa-adjust'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faadni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-adn'></i>"><span></span> <i class='fa fa-adn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faaligncenteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-align-center'></i>"><span></span> <i class='fa fa-align-center'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faalignjustifyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-align-justify'></i>"><span></span> <i class='fa fa-align-justify'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faalignlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-align-left'></i>"><span></span> <i class='fa fa-align-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faalignrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-align-right'></i>"><span></span> <i class='fa fa-align-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faamazoni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-amazon'></i>"><span></span> <i class='fa fa-amazon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faambulancei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ambulance'></i>"><span></span> <i class='fa fa-ambulance'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faanchori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-anchor'></i>"><span></span> <i class='fa fa-anchor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faandroidi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-android'></i>"><span></span> <i class='fa fa-android'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangellisti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angellist'></i>"><span></span> <i class='fa fa-angellist'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangledoubledowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-double-down'></i>"><span></span> <i class='fa fa-angle-double-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangledoublelefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-double-left'></i>"><span></span> <i class='fa fa-angle-double-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangledoublerighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-double-right'></i>"><span></span> <i class='fa fa-angle-double-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangledoubleupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-double-up'></i>"><span></span> <i class='fa fa-angle-double-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangledowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-down'></i>"><span></span> <i class='fa fa-angle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faanglelefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-left'></i>"><span></span> <i class='fa fa-angle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faanglerighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-right'></i>"><span></span> <i class='fa fa-angle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faangleupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-angle-up'></i>"><span></span> <i class='fa fa-angle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faapplei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-apple'></i>"><span></span> <i class='fa fa-apple'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarchivei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-archive'></i>"><span></span> <i class='fa fa-archive'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faareacharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-area-chart'></i>"><span></span> <i class='fa fa-area-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircledowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-down'></i>"><span></span> <i class='fa fa-arrow-circle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcirclelefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-left'></i>"><span></span> <i class='fa fa-arrow-circle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcirclerighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-right'></i>"><span></span> <i class='fa fa-arrow-circle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircleupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-up'></i>"><span></span> <i class='fa fa-arrow-circle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircleodowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-down'></i>"><span></span> <i class='fa fa-arrow-circle-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircleolefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-left'></i>"><span></span> <i class='fa fa-arrow-circle-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircleorighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-right'></i>"><span></span> <i class='fa fa-arrow-circle-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowcircleoupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-up'></i>"><span></span> <i class='fa fa-arrow-circle-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-down'></i>"><span></span> <i class='fa fa-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-left'></i>"><span></span> <i class='fa fa-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-right'></i>"><span></span> <i class='fa fa-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrow-up'></i>"><span></span> <i class='fa fa-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowsalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrows-alt'></i>"><span></span> <i class='fa fa-arrows-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowshi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrows-h'></i>"><span></span> <i class='fa fa-arrows-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowsvi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrows-v'></i>"><span></span> <i class='fa fa-arrows-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faarrowsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-arrows'></i>"><span></span> <i class='fa fa-arrows'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faasteriski" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-asterisk'></i>"><span></span> <i class='fa fa-asterisk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faati" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-at'></i>"><span></span> <i class='fa fa-at'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faautomobilei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-automobile'></i>"><span></span> <i class='fa fa-automobile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabackwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-backward'></i>"><span></span> <i class='fa fa-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabalancescalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-balance-scale'></i>"><span></span> <i class='fa fa-balance-scale'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabani" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ban'></i>"><span></span> <i class='fa fa-ban'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabanki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bank'></i>"><span></span> <i class='fa fa-bank'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabarcharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bar-chart'></i>"><span></span> <i class='fa fa-bar-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabarchartoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bar-chart-o'></i>"><span></span> <i class='fa fa-bar-chart-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabarcodei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-barcode'></i>"><span></span> <i class='fa fa-barcode'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabarsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bars'></i>"><span></span> <i class='fa fa-bars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabattery0i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-battery-0'></i>"><span></span> <i class='fa fa-battery-0'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabattery1i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-battery-1'></i>"><span></span> <i class='fa fa-battery-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabattery2i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-battery-2'></i>"><span></span> <i class='fa fa-battery-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabattery3i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-battery-3'></i>"><span></span> <i class='fa fa-battery-3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabattery4i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-battery-4'></i>"><span></span> <i class='fa fa-battery-4'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabedi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bed'></i>"><span></span> <i class='fa fa-bed'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabeeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-beer'></i>"><span></span> <i class='fa fa-beer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabehancei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-behance'></i>"><span></span> <i class='fa fa-behance'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabehancesquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-behance-square'></i>"><span></span> <i class='fa fa-behance-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabelli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bell'></i>"><span></span> <i class='fa fa-bell'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabelloi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bell-o'></i>"><span></span> <i class='fa fa-bell-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabellslashi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bell-slash'></i>"><span></span> <i class='fa fa-bell-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabellslashoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bell-slash-o'></i>"><span></span> <i class='fa fa-bell-slash-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabicyclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bicycle'></i>"><span></span> <i class='fa fa-bicycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabinocularsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-binoculars'></i>"><span></span> <i class='fa fa-binoculars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabirthdaycakei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-birthday-cake'></i>"><span></span> <i class='fa fa-birthday-cake'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabitbucketi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bitbucket'></i>"><span></span> <i class='fa fa-bitbucket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabitbucketsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bitbucket-square'></i>"><span></span> <i class='fa fa-bitbucket-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabitcoini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bitcoin'></i>"><span></span> <i class='fa fa-bitcoin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fablacktiei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-black-tie'></i>"><span></span> <i class='fa fa-black-tie'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faboldi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bold'></i>"><span></span> <i class='fa fa-bold'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabolti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bolt'></i>"><span></span> <i class='fa fa-bolt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabombi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bomb'></i>"><span></span> <i class='fa fa-bomb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabooki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-book'></i>"><span></span> <i class='fa fa-book'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabookmarki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bookmark'></i>"><span></span> <i class='fa fa-bookmark'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabookmarkoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bookmark-o'></i>"><span></span> <i class='fa fa-bookmark-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabriefcasei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-briefcase'></i>"><span></span> <i class='fa fa-briefcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabugi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bug'></i>"><span></span> <i class='fa fa-bug'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabuildingi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-building'></i>"><span></span> <i class='fa fa-building'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabuildingoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-building-o'></i>"><span></span> <i class='fa fa-building-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabullhorni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bullhorn'></i>"><span></span> <i class='fa fa-bullhorn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabullseyei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bullseye'></i>"><span></span> <i class='fa fa-bullseye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-bus'></i>"><span></span> <i class='fa fa-bus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fabuyselladsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-buysellads'></i>"><span></span> <i class='fa fa-buysellads'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facabi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cab'></i>"><span></span> <i class='fa fa-cab'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalculatori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calculator'></i>"><span></span> <i class='fa fa-calculator'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar'></i>"><span></span> <i class='fa fa-calendar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendarcheckoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar-check-o'></i>"><span></span> <i class='fa fa-calendar-check-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendarminusoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar-minus-o'></i>"><span></span> <i class='fa fa-calendar-minus-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendarplusoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar-plus-o'></i>"><span></span> <i class='fa fa-calendar-plus-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendartimesoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar-times-o'></i>"><span></span> <i class='fa fa-calendar-times-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facalendaroi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-calendar-o'></i>"><span></span> <i class='fa fa-calendar-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facamerai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-camera'></i>"><span></span> <i class='fa fa-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facameraretroi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-camera-retro'></i>"><span></span> <i class='fa fa-camera-retro'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-car'></i>"><span></span> <i class='fa fa-car'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-down'></i>"><span></span> <i class='fa fa-caret-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-left'></i>"><span></span> <i class='fa fa-caret-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-right'></i>"><span></span> <i class='fa fa-caret-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-up'></i>"><span></span> <i class='fa fa-caret-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretsquareodowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-square-o-down'></i>"><span></span> <i class='fa fa-caret-square-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretsquareolefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-square-o-left'></i>"><span></span> <i class='fa fa-caret-square-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretsquareorighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-square-o-right'></i>"><span></span> <i class='fa fa-caret-square-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facaretsquareoupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-caret-square-o-up'></i>"><span></span> <i class='fa fa-caret-square-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facartarrowdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cart-arrow-down'></i>"><span></span> <i class='fa fa-cart-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facartplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cart-plus'></i>"><span></span> <i class='fa fa-cart-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc'></i>"><span></span> <i class='fa fa-cc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccamexi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-amex'></i>"><span></span> <i class='fa fa-cc-amex'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccdinersclubi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-diners-club'></i>"><span></span> <i class='fa fa-cc-diners-club'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccdiscoveri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-discover'></i>"><span></span> <i class='fa fa-cc-discover'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccjcbi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-jcb'></i>"><span></span> <i class='fa fa-cc-jcb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccmastercardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-mastercard'></i>"><span></span> <i class='fa fa-cc-mastercard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccpaypali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-paypal'></i>"><span></span> <i class='fa fa-cc-paypal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccstripei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-stripe'></i>"><span></span> <i class='fa fa-cc-stripe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faccvisai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cc-visa'></i>"><span></span> <i class='fa fa-cc-visa'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facertificatei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-certificate'></i>"><span></span> <i class='fa fa-certificate'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachecki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-check'></i>"><span></span> <i class='fa fa-check'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facheckcirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-check-circle'></i>"><span></span> <i class='fa fa-check-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facheckcircleoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-check-circle-o'></i>"><span></span> <i class='fa fa-check-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachecksquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-check-square'></i>"><span></span> <i class='fa fa-check-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachecksquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-check-square-o'></i>"><span></span> <i class='fa fa-check-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevroncircledowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-circle-down'></i>"><span></span> <i class='fa fa-chevron-circle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevroncirclelefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-circle-left'></i>"><span></span> <i class='fa fa-chevron-circle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevroncirclerighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-circle-right'></i>"><span></span> <i class='fa fa-chevron-circle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevroncircleupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-circle-up'></i>"><span></span> <i class='fa fa-chevron-circle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevrondowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-down'></i>"><span></span> <i class='fa fa-chevron-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevronlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-left'></i>"><span></span> <i class='fa fa-chevron-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevronrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-right'></i>"><span></span> <i class='fa fa-chevron-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachevronupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chevron-up'></i>"><span></span> <i class='fa fa-chevron-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachildi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-child'></i>"><span></span> <i class='fa fa-child'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fachromei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-chrome'></i>"><span></span> <i class='fa fa-chrome'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-clone'></i>"><span></span> <i class='fa fa-clone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-circle'></i>"><span></span> <i class='fa fa-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facircleoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-circle-o'></i>"><span></span> <i class='fa fa-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facircleonotchi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-circle-o-notch'></i>"><span></span> <i class='fa fa-circle-o-notch'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facirclethini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-circle-thin'></i>"><span></span> <i class='fa fa-circle-thin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclipboardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-clipboard'></i>"><span></span> <i class='fa fa-clipboard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclockoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-clock-o'></i>"><span></span> <i class='fa fa-clock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclosei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-close'></i>"><span></span> <i class='fa fa-close'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facloudi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cloud'></i>"><span></span> <i class='fa fa-cloud'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclouddownloadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cloud-download'></i>"><span></span> <i class='fa fa-cloud-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faclouduploadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cloud-upload'></i>"><span></span> <i class='fa fa-cloud-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facnyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cny'></i>"><span></span> <i class='fa fa-cny'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facodei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-code'></i>"><span></span> <i class='fa fa-code'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facodeforki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-code-fork'></i>"><span></span> <i class='fa fa-code-fork'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facodepeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-codepen'></i>"><span></span> <i class='fa fa-codepen'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facoffeei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-coffee'></i>"><span></span> <i class='fa fa-coffee'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facolumnsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-columns'></i>"><span></span> <i class='fa fa-columns'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommenti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-comment'></i>"><span></span> <i class='fa fa-comment'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommentoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-comment-o'></i>"><span></span> <i class='fa fa-comment-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommentsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-comments'></i>"><span></span> <i class='fa fa-comments'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommentsoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-comments-o'></i>"><span></span> <i class='fa fa-comments-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommentingi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-commenting'></i>"><span></span> <i class='fa fa-commenting'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facommentingoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-commenting-o'></i>"><span></span> <i class='fa fa-commenting-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facompassi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-compass'></i>"><span></span> <i class='fa fa-compass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facompressi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-compress'></i>"><span></span> <i class='fa fa-compress'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faconnectdevelopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-connectdevelop'></i>"><span></span> <i class='fa fa-connectdevelop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facontaoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-contao'></i>"><span></span> <i class='fa fa-contao'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facopyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-copy'></i>"><span></span> <i class='fa fa-copy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facopyrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-copyright'></i>"><span></span> <i class='fa fa-copyright'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facreativecommonsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-creative-commons'></i>"><span></span> <i class='fa fa-creative-commons'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facreditcardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-credit-card'></i>"><span></span> <i class='fa fa-credit-card'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facropi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-crop'></i>"><span></span> <i class='fa fa-crop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facrosshairsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-crosshairs'></i>"><span></span> <i class='fa fa-crosshairs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facss3i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-css3'></i>"><span></span> <i class='fa fa-css3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facubei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cube'></i>"><span></span> <i class='fa fa-cube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facubesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cubes'></i>"><span></span> <i class='fa fa-cubes'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facuti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cut'></i>"><span></span> <i class='fa fa-cut'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-facutleryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-cutlery'></i>"><span></span> <i class='fa fa-cutlery'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadashboardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dashboard'></i>"><span></span> <i class='fa fa-dashboard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadashcubei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dashcube'></i>"><span></span> <i class='fa fa-dashcube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadatabasei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-database'></i>"><span></span> <i class='fa fa-database'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadedenti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dedent'></i>"><span></span> <i class='fa fa-dedent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadeliciousi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-delicious'></i>"><span></span> <i class='fa fa-delicious'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadesktopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-desktop'></i>"><span></span> <i class='fa fa-desktop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadeviantarti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-deviantart'></i>"><span></span> <i class='fa fa-deviantart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadiamondi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-diamond'></i>"><span></span> <i class='fa fa-diamond'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadiggi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-digg'></i>"><span></span> <i class='fa fa-digg'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadollari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dollar'></i>"><span></span> <i class='fa fa-dollar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadotcircleoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dot-circle-o'></i>"><span></span> <i class='fa fa-dot-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadownloadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-download'></i>"><span></span> <i class='fa fa-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadribbblei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dribbble'></i>"><span></span> <i class='fa fa-dribbble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadropboxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-dropbox'></i>"><span></span> <i class='fa fa-dropbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fadrupali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-drupal'></i>"><span></span> <i class='fa fa-drupal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faediti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-edit'></i>"><span></span> <i class='fa fa-edit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faejecti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-eject'></i>"><span></span> <i class='fa fa-eject'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faellipsishi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ellipsis-h'></i>"><span></span> <i class='fa fa-ellipsis-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faellipsisvi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ellipsis-v'></i>"><span></span> <i class='fa fa-ellipsis-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faempirei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-empire'></i>"><span></span> <i class='fa fa-empire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faenvelopei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-envelope'></i>"><span></span> <i class='fa fa-envelope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faenvelopeoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-envelope-o'></i>"><span></span> <i class='fa fa-envelope-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faenvelopesquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-envelope-square'></i>"><span></span> <i class='fa fa-envelope-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faeraseri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-eraser'></i>"><span></span> <i class='fa fa-eraser'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faeuroi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-euro'></i>"><span></span> <i class='fa fa-euro'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexchangei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-exchange'></i>"><span></span> <i class='fa fa-exchange'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexclamationi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-exclamation'></i>"><span></span> <i class='fa fa-exclamation'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexclamationcirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-exclamation-circle'></i>"><span></span> <i class='fa fa-exclamation-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexclamationtrianglei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-exclamation-triangle'></i>"><span></span> <i class='fa fa-exclamation-triangle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexpandi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-expand'></i>"><span></span> <i class='fa fa-expand'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexpeditedssli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-expeditedssl'></i>"><span></span> <i class='fa fa-expeditedssl'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexternallinki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-external-link'></i>"><span></span> <i class='fa fa-external-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faexternallinksquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-external-link-square'></i>"><span></span> <i class='fa fa-external-link-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faeyei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-eye'></i>"><span></span> <i class='fa fa-eye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faeyeslashi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-eye-slash'></i>"><span></span> <i class='fa fa-eye-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faeyedropperi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-eyedropper'></i>"><span></span> <i class='fa fa-eyedropper'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafacebooki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-facebook'></i>"><span></span> <i class='fa fa-facebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafacebookofficiali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-facebook-official'></i>"><span></span> <i class='fa fa-facebook-official'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafacebooksquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-facebook-square'></i>"><span></span> <i class='fa fa-facebook-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafastbackwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fast-backward'></i>"><span></span> <i class='fa fa-fast-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafastforwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fast-forward'></i>"><span></span> <i class='fa fa-fast-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafaxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fax'></i>"><span></span> <i class='fa fa-fax'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafemalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-female'></i>"><span></span> <i class='fa fa-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafighterjeti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fighter-jet'></i>"><span></span> <i class='fa fa-fighter-jet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file'></i>"><span></span> <i class='fa fa-file'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafileoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-o'></i>"><span></span> <i class='fa fa-file-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilearchiveoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-archive-o'></i>"><span></span> <i class='fa fa-file-archive-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafileaudiooi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-audio-o'></i>"><span></span> <i class='fa fa-file-audio-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilecodeoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-code-o'></i>"><span></span> <i class='fa fa-file-code-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafileexceloi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-excel-o'></i>"><span></span> <i class='fa fa-file-excel-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafileimageoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-image-o'></i>"><span></span> <i class='fa fa-file-image-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilemovieoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-movie-o'></i>"><span></span> <i class='fa fa-file-movie-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilepdfoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-pdf-o'></i>"><span></span> <i class='fa fa-file-pdf-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilephotooi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-photo-o'></i>"><span></span> <i class='fa fa-file-photo-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilepictureoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-picture-o'></i>"><span></span> <i class='fa fa-file-picture-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilepowerpointoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-powerpoint-o'></i>"><span></span> <i class='fa fa-file-powerpoint-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilesoundoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-sound-o'></i>"><span></span> <i class='fa fa-file-sound-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafiletexti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-text'></i>"><span></span> <i class='fa fa-file-text'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafiletextoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-text-o'></i>"><span></span> <i class='fa fa-file-text-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilevideooi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-video-o'></i>"><span></span> <i class='fa fa-file-video-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilewordoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-word-o'></i>"><span></span> <i class='fa fa-file-word-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilezipoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-file-zip-o'></i>"><span></span> <i class='fa fa-file-zip-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilesoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-files-o'></i>"><span></span> <i class='fa fa-files-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilmi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-film'></i>"><span></span> <i class='fa fa-film'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafilteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-filter'></i>"><span></span> <i class='fa fa-filter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafirei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fire'></i>"><span></span> <i class='fa fa-fire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafireextinguisheri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fire-extinguisher'></i>"><span></span> <i class='fa fa-fire-extinguisher'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafirefoxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-firefox'></i>"><span></span> <i class='fa fa-firefox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faflagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-flag'></i>"><span></span> <i class='fa fa-flag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faflagcheckeredi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-flag-checkered'></i>"><span></span> <i class='fa fa-flag-checkered'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faflagoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-flag-o'></i>"><span></span> <i class='fa fa-flag-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faflaski" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-flask'></i>"><span></span> <i class='fa fa-flask'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faflickri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-flickr'></i>"><span></span> <i class='fa fa-flickr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafloppyoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-floppy-o'></i>"><span></span> <i class='fa fa-floppy-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafolderi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-folder'></i>"><span></span> <i class='fa fa-folder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafolderoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-folder-o'></i>"><span></span> <i class='fa fa-folder-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafolderopeni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-folder-open'></i>"><span></span> <i class='fa fa-folder-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafolderopenoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-folder-open-o'></i>"><span></span> <i class='fa fa-folder-open-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafonti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-font'></i>"><span></span> <i class='fa fa-font'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafonticonsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fonticons'></i>"><span></span> <i class='fa fa-fonticons'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faforumbeei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-forumbee'></i>"><span></span> <i class='fa fa-forumbee'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faforwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-forward'></i>"><span></span> <i class='fa fa-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafoursquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-foursquare'></i>"><span></span> <i class='fa fa-foursquare'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafrownoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-frown-o'></i>"><span></span> <i class='fa fa-frown-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafutboloi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-futbol-o'></i>"><span></span> <i class='fa fa-futbol-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagamepadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gamepad'></i>"><span></span> <i class='fa fa-gamepad'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagaveli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gavel'></i>"><span></span> <i class='fa fa-gavel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagbpi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gbp'></i>"><span></span> <i class='fa fa-gbp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ge'></i>"><span></span> <i class='fa fa-ge'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fageari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gear'></i>"><span></span> <i class='fa fa-gear'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagearsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gears'></i>"><span></span> <i class='fa fa-gears'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagetpocketi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-get-pocket'></i>"><span></span> <i class='fa fa-get-pocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faggi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gg'></i>"><span></span> <i class='fa fa-gg'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faggcirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gg-circle'></i>"><span></span> <i class='fa fa-gg-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagifti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gift'></i>"><span></span> <i class='fa fa-gift'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagiti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-git'></i>"><span></span> <i class='fa fa-git'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagitsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-git-square'></i>"><span></span> <i class='fa fa-git-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagithubi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-github'></i>"><span></span> <i class='fa fa-github'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagithubalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-github-alt'></i>"><span></span> <i class='fa fa-github-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagithubsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-github-square'></i>"><span></span> <i class='fa fa-github-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagittipi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gittip'></i>"><span></span> <i class='fa fa-gittip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faglassi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-glass'></i>"><span></span> <i class='fa fa-glass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faglobei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-globe'></i>"><span></span> <i class='fa fa-globe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagooglei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-google'></i>"><span></span> <i class='fa fa-google'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagoogleplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-google-plus'></i>"><span></span> <i class='fa fa-google-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagoogleplussquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-google-plus-square'></i>"><span></span> <i class='fa fa-google-plus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagooglewalleti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-google-wallet'></i>"><span></span> <i class='fa fa-google-wallet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagraduationcapi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-graduation-cap'></i>"><span></span> <i class='fa fa-graduation-cap'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagratipayi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-gratipay'></i>"><span></span> <i class='fa fa-gratipay'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fagroupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-group'></i>"><span></span> <i class='fa fa-group'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-h-square'></i>"><span></span> <i class='fa fa-h-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahackernewsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hacker-news'></i>"><span></span> <i class='fa fa-hacker-news'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandgraboi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-grab-o'></i>"><span></span> <i class='fa fa-hand-grab-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandlizardoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-lizard-o'></i>"><span></span> <i class='fa fa-hand-lizard-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandpaperoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-paper-o'></i>"><span></span> <i class='fa fa-hand-paper-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandpeaceoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-peace-o'></i>"><span></span> <i class='fa fa-hand-peace-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandpointeroi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-pointer-o'></i>"><span></span> <i class='fa fa-hand-pointer-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandrockoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-rock-o'></i>"><span></span> <i class='fa fa-hand-rock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandscissorsoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-scissors-o'></i>"><span></span> <i class='fa fa-hand-scissors-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandspockoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-spock-o'></i>"><span></span> <i class='fa fa-hand-spock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandstopoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-stop-o'></i>"><span></span> <i class='fa fa-hand-stop-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandodowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-o-down'></i>"><span></span> <i class='fa fa-hand-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandolefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-o-left'></i>"><span></span> <i class='fa fa-hand-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandorighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-o-right'></i>"><span></span> <i class='fa fa-hand-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahandoupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hand-o-up'></i>"><span></span> <i class='fa fa-hand-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahddoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hdd-o'></i>"><span></span> <i class='fa fa-hdd-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faheaderi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-header'></i>"><span></span> <i class='fa fa-header'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faheadphonesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-headphones'></i>"><span></span> <i class='fa fa-headphones'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahearti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-heart'></i>"><span></span> <i class='fa fa-heart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faheartoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-heart-o'></i>"><span></span> <i class='fa fa-heart-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faheartbeati" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-heartbeat'></i>"><span></span> <i class='fa fa-heartbeat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahistoryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-history'></i>"><span></span> <i class='fa fa-history'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahomei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-home'></i>"><span></span> <i class='fa fa-home'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahospitaloi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hospital-o'></i>"><span></span> <i class='fa fa-hospital-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahoteli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hotel'></i>"><span></span> <i class='fa fa-hotel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglassi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass'></i>"><span></span> <i class='fa fa-hourglass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglass1i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass-1'></i>"><span></span> <i class='fa fa-hourglass-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglass2i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass-2'></i>"><span></span> <i class='fa fa-hourglass-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglass3i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass-3'></i>"><span></span> <i class='fa fa-hourglass-3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglassendi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass-end'></i>"><span></span> <i class='fa fa-hourglass-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahourglassoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-hourglass-o'></i>"><span></span> <i class='fa fa-hourglass-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahouzzi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-houzz'></i>"><span></span> <i class='fa fa-houzz'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fahtml5i" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-html5'></i>"><span></span> <i class='fa fa-html5'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faicursori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-i-cursor'></i>"><span></span> <i class='fa fa-i-cursor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-failsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ils'></i>"><span></span> <i class='fa fa-ils'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faimagei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-image'></i>"><span></span> <i class='fa fa-image'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainboxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-inbox'></i>"><span></span> <i class='fa fa-inbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faindenti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-indent'></i>"><span></span> <i class='fa fa-indent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faindustryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-industry'></i>"><span></span> <i class='fa fa-industry'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainfoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-info'></i>"><span></span> <i class='fa fa-info'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainfocirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-info-circle'></i>"><span></span> <i class='fa fa-info-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-inr'></i>"><span></span> <i class='fa fa-inr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainstagrami" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-instagram'></i>"><span></span> <i class='fa fa-instagram'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainstitutioni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-institution'></i>"><span></span> <i class='fa fa-institution'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fainternetexploreri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-internet-explorer'></i>"><span></span> <i class='fa fa-internet-explorer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faioxhosti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ioxhost'></i>"><span></span> <i class='fa fa-ioxhost'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faitalici" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-italic'></i>"><span></span> <i class='fa fa-italic'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fajoomlai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-joomla'></i>"><span></span> <i class='fa fa-joomla'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fajpyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-jpy'></i>"><span></span> <i class='fa fa-jpy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fajsfiddlei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-jsfiddle'></i>"><span></span> <i class='fa fa-jsfiddle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fakeyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-key'></i>"><span></span> <i class='fa fa-key'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fakeyboardoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-keyboard-o'></i>"><span></span> <i class='fa fa-keyboard-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fakrwi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-krw'></i>"><span></span> <i class='fa fa-krw'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falanguagei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-language'></i>"><span></span> <i class='fa fa-language'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falaptopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-laptop'></i>"><span></span> <i class='fa fa-laptop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falegali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-legal'></i>"><span></span> <i class='fa fa-legal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falemonoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-lemon-o'></i>"><span></span> <i class='fa fa-lemon-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faleveldowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-level-down'></i>"><span></span> <i class='fa fa-level-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falevelupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-level-up'></i>"><span></span> <i class='fa fa-level-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faliferingi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-life-ring'></i>"><span></span> <i class='fa fa-life-ring'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falightbulboi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-lightbulb-o'></i>"><span></span> <i class='fa fa-lightbulb-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falinecharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-line-chart'></i>"><span></span> <i class='fa fa-line-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falinki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-link'></i>"><span></span> <i class='fa fa-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falinkedini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-linkedin'></i>"><span></span> <i class='fa fa-linkedin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falinkedinsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-linkedin-square'></i>"><span></span> <i class='fa fa-linkedin-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falinuxi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-linux'></i>"><span></span> <i class='fa fa-linux'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falistalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-list-alt'></i>"><span></span> <i class='fa fa-list-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falistoli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-list-ol'></i>"><span></span> <i class='fa fa-list-ol'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falistuli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-list-ul'></i>"><span></span> <i class='fa fa-list-ul'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falocationarrowi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-location-arrow'></i>"><span></span> <i class='fa fa-location-arrow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falocki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-lock'></i>"><span></span> <i class='fa fa-lock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falongarrowdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-long-arrow-down'></i>"><span></span> <i class='fa fa-long-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falongarrowlefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-long-arrow-left'></i>"><span></span> <i class='fa fa-long-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falongarrowrighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-long-arrow-right'></i>"><span></span> <i class='fa fa-long-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-falongarrowupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-long-arrow-up'></i>"><span></span> <i class='fa fa-long-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famagici" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-magic'></i>"><span></span> <i class='fa fa-magic'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famagneti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-magnet'></i>"><span></span> <i class='fa fa-magnet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famalei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-male'></i>"><span></span> <i class='fa fa-male'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famapi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-map'></i>"><span></span> <i class='fa fa-map'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famapoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-map-o'></i>"><span></span> <i class='fa fa-map-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famapmarkeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-map-marker'></i>"><span></span> <i class='fa fa-map-marker'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famappini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-map-pin'></i>"><span></span> <i class='fa fa-map-pin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famapsignsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-map-signs'></i>"><span></span> <i class='fa fa-map-signs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famarsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mars'></i>"><span></span> <i class='fa fa-mars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famarsdoublei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mars-double'></i>"><span></span> <i class='fa fa-mars-double'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famarsstrokei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mars-stroke'></i>"><span></span> <i class='fa fa-mars-stroke'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famarsstrokehi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mars-stroke-h'></i>"><span></span> <i class='fa fa-mars-stroke-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famarsstrokevi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mars-stroke-v'></i>"><span></span> <i class='fa fa-mars-stroke-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famaxcdni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-maxcdn'></i>"><span></span> <i class='fa fa-maxcdn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fameanpathi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-meanpath'></i>"><span></span> <i class='fa fa-meanpath'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famediumi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-medium'></i>"><span></span> <i class='fa fa-medium'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famedkiti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-medkit'></i>"><span></span> <i class='fa fa-medkit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famehoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-meh-o'></i>"><span></span> <i class='fa fa-meh-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famercuryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mercury'></i>"><span></span> <i class='fa fa-mercury'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famicrophonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-microphone'></i>"><span></span> <i class='fa fa-microphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famicrophoneslashi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-microphone-slash'></i>"><span></span> <i class='fa fa-microphone-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faminusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-minus'></i>"><span></span> <i class='fa fa-minus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faminuscirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-minus-circle'></i>"><span></span> <i class='fa fa-minus-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faminussquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-minus-square'></i>"><span></span> <i class='fa fa-minus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faminussquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-minus-square-o'></i>"><span></span> <i class='fa fa-minus-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famobilei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mobile'></i>"><span></span> <i class='fa fa-mobile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famoneyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-money'></i>"><span></span> <i class='fa fa-money'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famoonoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-moon-o'></i>"><span></span> <i class='fa fa-moon-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famortarboardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mortar-board'></i>"><span></span> <i class='fa fa-mortar-board'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famotorcyclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-motorcycle'></i>"><span></span> <i class='fa fa-motorcycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famousepointeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-mouse-pointer'></i>"><span></span> <i class='fa fa-mouse-pointer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-famusici" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-music'></i>"><span></span> <i class='fa fa-music'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fanaviconi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-navicon'></i>"><span></span> <i class='fa fa-navicon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faneuteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-neuter'></i>"><span></span> <i class='fa fa-neuter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fanewspaperoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-newspaper-o'></i>"><span></span> <i class='fa fa-newspaper-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faobjectgroupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-object-group'></i>"><span></span> <i class='fa fa-object-group'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faobjectungroupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-object-ungroup'></i>"><span></span> <i class='fa fa-object-ungroup'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faodnoklassnikii" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-odnoklassniki'></i>"><span></span> <i class='fa fa-odnoklassniki'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faodnoklassnikisquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-odnoklassniki-square'></i>"><span></span> <i class='fa fa-odnoklassniki-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faopencarti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-opencart'></i>"><span></span> <i class='fa fa-opencart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faopenidi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-openid'></i>"><span></span> <i class='fa fa-openid'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faoperai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-opera'></i>"><span></span> <i class='fa fa-opera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faoptinmonsteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-optin-monster'></i>"><span></span> <i class='fa fa-optin-monster'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faoutdenti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-outdent'></i>"><span></span> <i class='fa fa-outdent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapagelinesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pagelines'></i>"><span></span> <i class='fa fa-pagelines'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapaintbrushi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paint-brush'></i>"><span></span> <i class='fa fa-paint-brush'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapaperclipi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paperclip'></i>"><span></span> <i class='fa fa-paperclip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faparagraphi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paragraph'></i>"><span></span> <i class='fa fa-paragraph'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapastei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paste'></i>"><span></span> <i class='fa fa-paste'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapausei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pause'></i>"><span></span> <i class='fa fa-pause'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapawi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paw'></i>"><span></span> <i class='fa fa-paw'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapaypali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-paypal'></i>"><span></span> <i class='fa fa-paypal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapencili" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pencil'></i>"><span></span> <i class='fa fa-pencil'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapencilsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pencil-square'></i>"><span></span> <i class='fa fa-pencil-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapencilsquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pencil-square-o'></i>"><span></span> <i class='fa fa-pencil-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faphonei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-phone'></i>"><span></span> <i class='fa fa-phone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faphonesquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-phone-square'></i>"><span></span> <i class='fa fa-phone-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faphotoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-photo'></i>"><span></span> <i class='fa fa-photo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapictureoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-picture-o'></i>"><span></span> <i class='fa fa-picture-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapiecharti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pie-chart'></i>"><span></span> <i class='fa fa-pie-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapiedpiperi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pied-piper'></i>"><span></span> <i class='fa fa-pied-piper'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapiedpiperalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pied-piper-alt'></i>"><span></span> <i class='fa fa-pied-piper-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapinteresti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pinterest'></i>"><span></span> <i class='fa fa-pinterest'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapinterestpi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pinterest-p'></i>"><span></span> <i class='fa fa-pinterest-p'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapinterestsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-pinterest-square'></i>"><span></span> <i class='fa fa-pinterest-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplanei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plane'></i>"><span></span> <i class='fa fa-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplayi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-play'></i>"><span></span> <i class='fa fa-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplaycirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-play-circle'></i>"><span></span> <i class='fa fa-play-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplaycircleoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-play-circle-o'></i>"><span></span> <i class='fa fa-play-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplugi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plug'></i>"><span></span> <i class='fa fa-plug'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plus'></i>"><span></span> <i class='fa fa-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapluscirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plus-circle'></i>"><span></span> <i class='fa fa-plus-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplussquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plus-square'></i>"><span></span> <i class='fa fa-plus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faplussquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-plus-square-o'></i>"><span></span> <i class='fa fa-plus-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapoweroffi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-power-off'></i>"><span></span> <i class='fa fa-power-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faprinti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-print'></i>"><span></span> <i class='fa fa-print'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fapuzzlepiecei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-puzzle-piece'></i>"><span></span> <i class='fa fa-puzzle-piece'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faqqi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-qq'></i>"><span></span> <i class='fa fa-qq'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faqrcodei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-qrcode'></i>"><span></span> <i class='fa fa-qrcode'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faquestioni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-question'></i>"><span></span> <i class='fa fa-question'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faquestioncirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-question-circle'></i>"><span></span> <i class='fa fa-question-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faquotelefti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-quote-left'></i>"><span></span> <i class='fa fa-quote-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faquoterighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-quote-right'></i>"><span></span> <i class='fa fa-quote-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farandomi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-random'></i>"><span></span> <i class='fa fa-random'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farebeli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-rebel'></i>"><span></span> <i class='fa fa-rebel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farecyclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-recycle'></i>"><span></span> <i class='fa fa-recycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faredditi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-reddit'></i>"><span></span> <i class='fa fa-reddit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faredditsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-reddit-square'></i>"><span></span> <i class='fa fa-reddit-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farefreshi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-refresh'></i>"><span></span> <i class='fa fa-refresh'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faregisteredi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-registered'></i>"><span></span> <i class='fa fa-registered'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faremovei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-remove'></i>"><span></span> <i class='fa fa-remove'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farenreni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-renren'></i>"><span></span> <i class='fa fa-renren'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farepeati" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-repeat'></i>"><span></span> <i class='fa fa-repeat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fareplyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-reply'></i>"><span></span> <i class='fa fa-reply'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fareplyalli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-reply-all'></i>"><span></span> <i class='fa fa-reply-all'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faretweeti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-retweet'></i>"><span></span> <i class='fa fa-retweet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farmbi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-rmb'></i>"><span></span> <i class='fa fa-rmb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faroadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-road'></i>"><span></span> <i class='fa fa-road'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-farocketi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-rocket'></i>"><span></span> <i class='fa fa-rocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faroublei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-rouble'></i>"><span></span> <i class='fa fa-rouble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasafarii" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-safari'></i>"><span></span> <i class='fa fa-safari'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasavei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-save'></i>"><span></span> <i class='fa fa-save'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fascissorsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-scissors'></i>"><span></span> <i class='fa fa-scissors'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasearchi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-search'></i>"><span></span> <i class='fa fa-search'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasearchminusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-search-minus'></i>"><span></span> <i class='fa fa-search-minus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasearchplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-search-plus'></i>"><span></span> <i class='fa fa-search-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasellsyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sellsy'></i>"><span></span> <i class='fa fa-sellsy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasendi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-send'></i>"><span></span> <i class='fa fa-send'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasendoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-send-o'></i>"><span></span> <i class='fa fa-send-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faserveri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-server'></i>"><span></span> <i class='fa fa-server'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasharei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-share'></i>"><span></span> <i class='fa fa-share'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasharesquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-share-square'></i>"><span></span> <i class='fa fa-share-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasharesquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-share-square-o'></i>"><span></span> <i class='fa fa-share-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasharealti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-share-alt'></i>"><span></span> <i class='fa fa-share-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasharealtsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-share-alt-square'></i>"><span></span> <i class='fa fa-share-alt-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fashekeli" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-shekel'></i>"><span></span> <i class='fa fa-shekel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fashieldi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-shield'></i>"><span></span> <i class='fa fa-shield'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fashipi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-ship'></i>"><span></span> <i class='fa fa-ship'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fashirtsinbulki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-shirtsinbulk'></i>"><span></span> <i class='fa fa-shirtsinbulk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fashoppingcarti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-shopping-cart'></i>"><span></span> <i class='fa fa-shopping-cart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasignini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sign-in'></i>"><span></span> <i class='fa fa-sign-in'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasignouti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sign-out'></i>"><span></span> <i class='fa fa-sign-out'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasignali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-signal'></i>"><span></span> <i class='fa fa-signal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasimplybuilti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-simplybuilt'></i>"><span></span> <i class='fa fa-simplybuilt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasitemapi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sitemap'></i>"><span></span> <i class='fa fa-sitemap'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faskyatlasi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-skyatlas'></i>"><span></span> <i class='fa fa-skyatlas'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faskypei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-skype'></i>"><span></span> <i class='fa fa-skype'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faslacki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-slack'></i>"><span></span> <i class='fa fa-slack'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faslidersi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sliders'></i>"><span></span> <i class='fa fa-sliders'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faslidesharei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-slideshare'></i>"><span></span> <i class='fa fa-slideshare'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasmileoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-smile-o'></i>"><span></span> <i class='fa fa-smile-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasoccerballoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-soccer-ball-o'></i>"><span></span> <i class='fa fa-soccer-ball-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasorti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort'></i>"><span></span> <i class='fa fa-sort'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortalphaasci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-alpha-asc'></i>"><span></span> <i class='fa fa-sort-alpha-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortalphadesci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-alpha-desc'></i>"><span></span> <i class='fa fa-sort-alpha-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortamountasci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-amount-asc'></i>"><span></span> <i class='fa fa-sort-amount-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortamountdesci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-amount-desc'></i>"><span></span> <i class='fa fa-sort-amount-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortnumericasci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-numeric-asc'></i>"><span></span> <i class='fa fa-sort-numeric-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasortnumericdesci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sort-numeric-desc'></i>"><span></span> <i class='fa fa-sort-numeric-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasoundcloudi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-soundcloud'></i>"><span></span> <i class='fa fa-soundcloud'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faspaceshuttlei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-space-shuttle'></i>"><span></span> <i class='fa fa-space-shuttle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faspinneri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-spinner'></i>"><span></span> <i class='fa fa-spinner'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faspooni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-spoon'></i>"><span></span> <i class='fa fa-spoon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faspotifyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-spotify'></i>"><span></span> <i class='fa fa-spotify'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-square'></i>"><span></span> <i class='fa fa-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasquareoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-square-o'></i>"><span></span> <i class='fa fa-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastackexchangei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stack-exchange'></i>"><span></span> <i class='fa fa-stack-exchange'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastackoverflowi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stack-overflow'></i>"><span></span> <i class='fa fa-stack-overflow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastari" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-star'></i>"><span></span> <i class='fa fa-star'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastarhalfi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-star-half'></i>"><span></span> <i class='fa fa-star-half'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastarhalfoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-star-half-o'></i>"><span></span> <i class='fa fa-star-half-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastaroi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-star-o'></i>"><span></span> <i class='fa fa-star-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasteami" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-steam'></i>"><span></span> <i class='fa fa-steam'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasteamsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-steam-square'></i>"><span></span> <i class='fa fa-steam-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastepbackwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-step-backward'></i>"><span></span> <i class='fa fa-step-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastepforwardi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-step-forward'></i>"><span></span> <i class='fa fa-step-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastethoscopei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stethoscope'></i>"><span></span> <i class='fa fa-stethoscope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastickynotei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sticky-note'></i>"><span></span> <i class='fa fa-sticky-note'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastickynoteoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sticky-note-o'></i>"><span></span> <i class='fa fa-sticky-note-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastopi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stop'></i>"><span></span> <i class='fa fa-stop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastreetviewi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-street-view'></i>"><span></span> <i class='fa fa-street-view'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastrikethroughi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-strikethrough'></i>"><span></span> <i class='fa fa-strikethrough'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastumbleuponi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stumbleupon'></i>"><span></span> <i class='fa fa-stumbleupon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fastumbleuponcirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-stumbleupon-circle'></i>"><span></span> <i class='fa fa-stumbleupon-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasubscripti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-subscript'></i>"><span></span> <i class='fa fa-subscript'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasubwayi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-subway'></i>"><span></span> <i class='fa fa-subway'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasuitcasei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-suitcase'></i>"><span></span> <i class='fa fa-suitcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasunoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-sun-o'></i>"><span></span> <i class='fa fa-sun-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fasuperscripti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-superscript'></i>"><span></span> <i class='fa fa-superscript'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatablei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-table'></i>"><span></span> <i class='fa fa-table'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatableti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tablet'></i>"><span></span> <i class='fa fa-tablet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatachometeri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tachometer'></i>"><span></span> <i class='fa fa-tachometer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatagi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tag'></i>"><span></span> <i class='fa fa-tag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatagsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tags'></i>"><span></span> <i class='fa fa-tags'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatasksi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tasks'></i>"><span></span> <i class='fa fa-tasks'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fataxii" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-taxi'></i>"><span></span> <i class='fa fa-taxi'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatelevisioni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-television'></i>"><span></span> <i class='fa fa-television'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatencentweiboi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tencent-weibo'></i>"><span></span> <i class='fa fa-tencent-weibo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faterminali" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-terminal'></i>"><span></span> <i class='fa fa-terminal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatextheighti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-text-height'></i>"><span></span> <i class='fa fa-text-height'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatextwidthi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-text-width'></i>"><span></span> <i class='fa fa-text-width'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-th'></i>"><span></span> <i class='fa fa-th'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathlargei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-th-large'></i>"><span></span> <i class='fa fa-th-large'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathumbtacki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-thumb-tack'></i>"><span></span> <i class='fa fa-thumb-tack'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathumbsdowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-thumbs-down'></i>"><span></span> <i class='fa fa-thumbs-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathumbsupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-thumbs-up'></i>"><span></span> <i class='fa fa-thumbs-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathumbsodowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-thumbs-o-down'></i>"><span></span> <i class='fa fa-thumbs-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fathumbsoupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-thumbs-o-up'></i>"><span></span> <i class='fa fa-thumbs-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatimesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-times'></i>"><span></span> <i class='fa fa-times'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatimescirclei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-times-circle'></i>"><span></span> <i class='fa fa-times-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatimescircleoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-times-circle-o'></i>"><span></span> <i class='fa fa-times-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatinti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tint'></i>"><span></span> <i class='fa fa-tint'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatoggleoffi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-toggle-off'></i>"><span></span> <i class='fa fa-toggle-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatoggleoni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-toggle-on'></i>"><span></span> <i class='fa fa-toggle-on'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrademarki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-trademark'></i>"><span></span> <i class='fa fa-trademark'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatraini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-train'></i>"><span></span> <i class='fa fa-train'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatransgenderi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-transgender'></i>"><span></span> <i class='fa fa-transgender'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatransgenderalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-transgender-alt'></i>"><span></span> <i class='fa fa-transgender-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrashi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-trash'></i>"><span></span> <i class='fa fa-trash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrashoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-trash-o'></i>"><span></span> <i class='fa fa-trash-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatreei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tree'></i>"><span></span> <i class='fa fa-tree'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrelloi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-trello'></i>"><span></span> <i class='fa fa-trello'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatripadvisori" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tripadvisor'></i>"><span></span> <i class='fa fa-tripadvisor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrophyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-trophy'></i>"><span></span> <i class='fa fa-trophy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatrucki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-truck'></i>"><span></span> <i class='fa fa-truck'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatryi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-try'></i>"><span></span> <i class='fa fa-try'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fattyi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tty'></i>"><span></span> <i class='fa fa-tty'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatumblri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tumblr'></i>"><span></span> <i class='fa fa-tumblr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatumblrsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-tumblr-square'></i>"><span></span> <i class='fa fa-tumblr-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatwitchi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-twitch'></i>"><span></span> <i class='fa fa-twitch'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatwitteri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-twitter'></i>"><span></span> <i class='fa fa-twitter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fatwittersquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-twitter-square'></i>"><span></span> <i class='fa fa-twitter-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favenusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-venus'></i>"><span></span> <i class='fa fa-venus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favenusdoublei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-venus-double'></i>"><span></span> <i class='fa fa-venus-double'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favenusmarsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-venus-mars'></i>"><span></span> <i class='fa fa-venus-mars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faviacoini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-viacoin'></i>"><span></span> <i class='fa fa-viacoin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favideocamerai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-video-camera'></i>"><span></span> <i class='fa fa-video-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favimeoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-vimeo'></i>"><span></span> <i class='fa fa-vimeo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favimeosquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-vimeo-square'></i>"><span></span> <i class='fa fa-vimeo-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favinei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-vine'></i>"><span></span> <i class='fa fa-vine'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-vk'></i>"><span></span> <i class='fa fa-vk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favolumedowni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-volume-down'></i>"><span></span> <i class='fa fa-volume-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favolumeoffi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-volume-off'></i>"><span></span> <i class='fa fa-volume-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-favolumeupi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-volume-up'></i>"><span></span> <i class='fa fa-volume-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faumbrellai" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-umbrella'></i>"><span></span> <i class='fa fa-umbrella'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faunderlinei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-underline'></i>"><span></span> <i class='fa fa-underline'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faundoi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-undo'></i>"><span></span> <i class='fa fa-undo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fauniversityi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-university'></i>"><span></span> <i class='fa fa-university'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faunlinki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-unlink'></i>"><span></span> <i class='fa fa-unlink'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faunlocki" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-unlock'></i>"><span></span> <i class='fa fa-unlock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faunlockalti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-unlock-alt'></i>"><span></span> <i class='fa fa-unlock-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fauploadi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-upload'></i>"><span></span> <i class='fa fa-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fauseri" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-user'></i>"><span></span> <i class='fa fa-user'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fausermdi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-user-md'></i>"><span></span> <i class='fa fa-user-md'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fauserplusi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-user-plus'></i>"><span></span> <i class='fa fa-user-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fausersecreti" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-user-secret'></i>"><span></span> <i class='fa fa-user-secret'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fausertimesi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-user-times'></i>"><span></span> <i class='fa fa-user-times'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fausersi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-users'></i>"><span></span> <i class='fa fa-users'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawechati" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wechat'></i>"><span></span> <i class='fa fa-wechat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faweiboi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-weibo'></i>"><span></span> <i class='fa fa-weibo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faweixini" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-weixin'></i>"><span></span> <i class='fa fa-weixin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawhatsappi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-whatsapp'></i>"><span></span> <i class='fa fa-whatsapp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawheelchairi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wheelchair'></i>"><span></span> <i class='fa fa-wheelchair'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawifii" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wifi'></i>"><span></span> <i class='fa fa-wifi'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawikipediawi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wikipedia-w'></i>"><span></span> <i class='fa fa-wikipedia-w'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawindowsi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-windows'></i>"><span></span> <i class='fa fa-windows'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawoni" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-won'></i>"><span></span> <i class='fa fa-won'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawordpressi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wordpress'></i>"><span></span> <i class='fa fa-wordpress'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fawrenchi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-wrench'></i>"><span></span> <i class='fa fa-wrench'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faxingi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-xing'></i>"><span></span> <i class='fa fa-xing'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faxingsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-xing-square'></i>"><span></span> <i class='fa fa-xing-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fayahooi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-yahoo'></i>"><span></span> <i class='fa fa-yahoo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fafayci" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-fa-yc'></i>"><span></span> <i class='fa fa-fa-yc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-faycsquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-yc-square'></i>"><span></span> <i class='fa fa-yc-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fayelpi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-yelp'></i>"><span></span> <i class='fa fa-yelp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fayoutubei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-youtube'></i>"><span></span> <i class='fa fa-youtube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fayoutubeplayi" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-youtube-play'></i>"><span></span> <i class='fa fa-youtube-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="abstract_modules_subject_icon_i-classfa-fayoutubesquarei" name="abstract_modules_subject_icon" class="abstract_modules_subject_icon"  value="<i class='fa fa-youtube-square'></i>"><span></span> <i class='fa fa-youtube-square'></i>
																					</label>
																					<div id="abstract_modules_subject_icon-check-loading"><?php echo translate("Loading"); ?>...</div>

																				<div class="help-block"><?php echo translate("Make menu of subject friendly with icon in backend mode"); ?></div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
									  </div>

									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list"><i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span></a>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content");?></div>
                                        </div>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
	
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="abstract_modules_order"><?php echo translate("Order"); ?></label>
							<div class="col-md-12">
								<input class="form-control" type="number" id="abstract_modules_order" name="abstract_modules_order" value="1" >
							</div>
						</div>
					</div>
					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<div class="col-lg-10 col-md-10 col-sm-9 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="abstract_modules_activate" name="abstract_modules_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full">
			</div>
		</div>
	</div>
</div>

<!-- sort table (begin) -->
<div class="row">

    <div id="module-sort" class="col-lg-12">
		
		<div class="block block-themed" id="sort-content">
            <div class="block-header">
                <ul class="block-options">
					<li>
						<button type="button" class="btn-sort-close" title="<?php echo translate("Back to Data Table"); ?>" data-toggle="block-option"><i class="si si-list"></i> <?php echo translate("Table"); ?></button>
					</li>
                    <li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
                </ul>
                <h3 class="block-title"><?php echo translate("modules"); ?> <?php echo translate("Sort"); ?></h3>
            </div>

            <!-- Data Table -->
            <div class="block-content">
			
            	<ol class="sortable ui-sortable">
				</ol>

                <div class="row items-push">
					<div class="col-md-12 push-30-t push-30-b text-right">
						<button id="button-data-save-sort" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
					</div>
				</div>
                
            </div>

        </div>
		
	</div>
	
</div>
<!-- sort table (end) -->

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<li>
                        <button class="btn-batch-generate" type="button" title="<?php echo translate("Batch Generate"); ?>" data-content="<?php echo translate("Are you sure you want to batch generate all")." <strong>".translate("Modules Abstract")."</strong>?<br />(".translate("Customed codes in last generated might be lost").")"; ?>" data-original-title="Generate"><i class="si si-control-play" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Batch Generate"); ?></span></button>
                    </li>
					<li>
                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
                    </li>
                    <li>
                        <button class="btn-sort" type="button" title="<?php echo translate("Sort"); ?>"><i class="si si-shuffle" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Sort"); ?></span></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("Abstract Modules"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
				<div id="filter_parent" class="panel-group">
					<div class="panel panel-default">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#filter_parent" href="#filter">
							<div class="panel-heading">
								<span class="panel-title"><?php echo translate("Advanced Filter"); ?></span> <i class="fa fa-angle-down"></i>
							</div>
						</a>
						<div id="filter" class="panel-collapse collapse">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<form action="" method="get">
											<input type="hidden" value="<?php if (isset($_GET["search"]) && !empty($_GET["search"])) { echo $_GET["search"]; } ?>">
											<div class="row items-push">
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-4 push-5-t" for="published"><?php echo translate("Status"); ?></label>
														<div class="col-md-8">
															<?php
															if (isset($_GET["published"])) {
																if ($_GET["published"] == "") {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "1") {
																	$status_all_selected = "";
																	$status_published_selected = " selected";
																	$status_unpublished_selected = "";
																} else if ($_GET["published"] == "0") {
																	$status_all_selected = "";
																	$status_published_selected = "";
																	$status_unpublished_selected = " selected";
																} else {
																	$status_all_selected = " selected";
																	$status_published_selected = "";
																	$status_unpublished_selected = "";
																}
															} else {
																$status_all_selected = " selected";
																$status_published_selected = "";
																$status_unpublished_selected = "";
															}
															?>
															<select name="published" aria-controls="datatable" class="form-control">
																<option value="" <?php echo $status_all_selected; ?>><?php echo translate("All");?></option>
																<option value="" disabled>-</option>
																<option value="1" <?php echo $status_published_selected; ?>><?php echo translate("Published");?></option>
																<option value="0" <?php echo $status_unpublished_selected; ?>><?php echo translate("Unpublished");?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="from"><?php echo translate("From"); ?></label>
														<div class="col-md-9">
															<input class="form-control from" type="text" data-role="date" id="from" name="from" value="<?php if (isset($_GET["from"]) && !empty($_GET["from"])) { echo $_GET["from"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="to"><?php echo translate("To"); ?></label>
														<div class="col-md-9">
															<input class="form-control to" type="text" data-role="date" id="to" name="to" value="<?php if (isset($_GET["to"]) && !empty($_GET["to"])) { echo $_GET["to"]; } ?>">
														</div>
													</div>
												</div>
												<div class="col-md-3 text-right">
													<div class="form-group">
														<div class="col-xs-6">
															<button id="button-filter-submit" class="btn btn-sm btn-primary" type="submit" style="width: 100%;"><?php echo translate("Show"); ?></button>
														</div>
														<div class="col-xs-6">
															<button id="button-filter-reset" class="btn btn-sm btn-default" type="reset" onclick="location.href = '<?php echo $module_page_link; ?>'" style="width: 100%;"><?php echo translate("Reset"); ?></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            	<?php
            	$data_table_info = prepare_abstract_modules_table_defer($table_module_field);
                echo create_abstract_modules_table($data_table_info["values"], $table_module_field);
            	?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->

<?php
include("templates/".$configs["backend_template"]."/container-footer.php");
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->
	
<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php 
if (file_exists("../../../plugins/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
	echo "<script src=\"plugins/bootstrap-datepicker/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js\"></script>";
}
?>
<script src="plugins/nestedsort/jquery.mjs.nestedSortable.js"></script>
<script src="plugins/tinymce-jqerry/jquery.tinymce.min.js"></script>
<script src="plugins/tinymce-jqerry/tinymce.min.js"></script>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo htmlspecialchars($module_function_page); ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;

var abstract_modules_data_all = $.parseJSON("<?php echo addslashes(json_encode(get_abstract_modules_data_all("", "", "", "", 1))); ?>");
</script>

<script>

function reset_abstract_modules_data() {

	$("#abstract_modules_id").val("");
	$("#abstract_modules_name").val("");
	$("#abstract_modules_varname").val("");
	$("#abstract_modules_description").val("");
	$("#abstract_modules_form_method").val(abstract_modules_form_method_default_value[0]);
	$("#abstract_modules_component_modules").prop("checked", false);
	$("#abstract_modules_component_groups").prop("checked", false);
	$("#abstract_modules_component_users").prop("checked", false);
	$("#abstract_modules_component_languages").prop("checked", false);
	$("#abstract_modules_component_pages").prop("checked", false);
	$("#abstract_modules_component_media").prop("checked", false);
	$("#abstract_modules_component_commerce").prop("checked", false);
	$("#abstract_modules_database_engine").val(abstract_modules_database_engine_default_value[0]);
	$("#abstract_modules_data_sortable").prop("checked", false);
	$("#abstract_modules_data_addable").prop("checked", true);
	$("#abstract_modules_data_viewonly").prop("checked", false);
	$("#abstract_modules_template").val("");
	$(".abstract_modules_icon").first().prop('checked', true);
	$("#abstract_modules_category").val("");
	$("#abstract_modules_subject").val("");
	$(".abstract_modules_subject_icon").first().prop('checked', true);
	$("#abstract_modules_order").val(<?php echo $data_table_info["count_true"]; ?>);
	$("#abstract_modules_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#abstract_modules_action").val("");

}

function update_abstract_modules_data(target_id) {
}

function submit_abstract_modules_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);

    if ($("#abstract_modules_id").val() == "") {
		var method = "create_abstract_modules_data";
	} else {
		var method = "update_abstract_modules_data";
	}

	if ($("#abstract_modules_activate").is(":checked") === true){
		var abstract_modules_activate = "1";
	} else {
		var abstract_modules_activate = "0";
	}
	if ($("#abstract_modules_component_modules").is(":checked") === true){
		var abstract_modules_component_modules = abstract_modules_component_modules_default_value[0];
	} else {
		var abstract_modules_component_modules = "";
	}
	if ($("#abstract_modules_component_groups").is(":checked") === true){
		var abstract_modules_component_groups = abstract_modules_component_groups_default_value[0];
	} else {
		var abstract_modules_component_groups = "";
	}
	if ($("#abstract_modules_component_users").is(":checked") === true){
		var abstract_modules_component_users = abstract_modules_component_users_default_value[0];
	} else {
		var abstract_modules_component_users = "";
	}
	if ($("#abstract_modules_component_languages").is(":checked") === true){
		var abstract_modules_component_languages = abstract_modules_component_languages_default_value[0];
	} else {
		var abstract_modules_component_languages = "";
	}
	if ($("#abstract_modules_component_pages").is(":checked") === true){
		var abstract_modules_component_pages = abstract_modules_component_pages_default_value[0];
	} else {
		var abstract_modules_component_pages = "";
	}
	if ($("#abstract_modules_component_media").is(":checked") === true){
		var abstract_modules_component_media = abstract_modules_component_media_default_value[0];
	} else {
		var abstract_modules_component_media = "";
	}
	if ($("#abstract_modules_component_commerce").is(":checked") === true){
		var abstract_modules_component_commerce = abstract_modules_component_commerce_default_value[0];
	} else {
		var abstract_modules_component_commerce = "";
	}
	if ($("#abstract_modules_data_sortable").is(":checked") === true){
		var abstract_modules_data_sortable = abstract_modules_data_sortable_default_value[0];
	} else {
		var abstract_modules_data_sortable = "";
	}
	if ($("#abstract_modules_data_addable").is(":checked") === true){
		var abstract_modules_data_addable = abstract_modules_data_addable_default_value[0];
	} else {
		var abstract_modules_data_addable = "";
	}
	if ($("#abstract_modules_data_viewonly").is(":checked") === true){
		var abstract_modules_data_viewonly = abstract_modules_data_viewonly_default_value[0];
	} else {
		var abstract_modules_data_viewonly = "";
	}
	var abstract_modules_icon = "";
	$(".abstract_modules_icon").each(function (i) {
		if ($(this).is(":checked")) {
			abstract_modules_icon = $(this).val();
		}
	});
	var abstract_modules_subject_icon = "";
	$(".abstract_modules_subject_icon").each(function (i) {
		if ($(this).is(":checked")) {
			abstract_modules_subject_icon = $(this).val();
		}
	});
	
	
	
	
	
	

	var form_values   = {
		"abstract_modules_id": $("#abstract_modules_id").val().trim(),
		"abstract_modules_action": $("#abstract_modules_action").val().trim(),
		"abstract_modules_name": $("#abstract_modules_name").val(),
		"abstract_modules_varname": $("#abstract_modules_varname").val(),
		"abstract_modules_description": $("#abstract_modules_description").val().trim(),
		"abstract_modules_form_method": $("#abstract_modules_form_method").val(),
		"abstract_modules_component_modules": abstract_modules_component_modules,
		"abstract_modules_component_groups": abstract_modules_component_groups,
		"abstract_modules_component_users": abstract_modules_component_users,
		"abstract_modules_component_languages": abstract_modules_component_languages,
		"abstract_modules_component_pages": abstract_modules_component_pages,
		"abstract_modules_component_media": abstract_modules_component_media,
		"abstract_modules_component_commerce": abstract_modules_component_commerce,
		"abstract_modules_database_engine": $("#abstract_modules_database_engine").val(),
		"abstract_modules_data_sortable": abstract_modules_data_sortable,
		"abstract_modules_data_addable": abstract_modules_data_addable,
		"abstract_modules_data_viewonly": abstract_modules_data_viewonly,
		"abstract_modules_template": $("#abstract_modules_template").val(),
		"abstract_modules_icon": abstract_modules_icon,
		"abstract_modules_category": $("#abstract_modules_category").val(),
		"abstract_modules_subject": $("#abstract_modules_subject").val(),
		"abstract_modules_subject_icon": abstract_modules_subject_icon,
		"abstract_modules_order": $("#abstract_modules_order").val().trim(),

		"users_id": $("#users_id").val().trim(),
		"users_username": $("#users_username").val().trim(),
		"users_name": $("#users_name").val().trim(),
		"users_last_name": $("#users_last_name").val().trim(),
		"abstract_modules_activate": abstract_modules_activate.trim(),
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				if (method == "create_abstract_modules_data") {
					create_abstract_modules_table_row(response.values);
					show_abstract_modules_modal_response(response.status, response.message);
					reset_abstract_modules_data();
					$("#form-content").slideUp("fast");
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
					var limit = $("#datatable_length_selector").val();
					var page = get_url_param().page;
					if (page == null) {
						page = 1;
					}
					var search_text = $("#datatable_search").val();
					var published = get_url_param().published;
					var from = get_url_param().from;
					var to = get_url_param().to;
					if (limit != null) {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
							}
						}
					} else {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>");
							}
						}
					}
            		$("#button-data-submit").prop("disabled", false);
				} else if (method == "update_abstract_modules_data") {
					update_abstract_modules_table_row(response.values);
				    show_abstract_modules_modal_response(response.status, response.message);
					update_abstract_modules_data($("#abstract_modules_id").val());
					edit_abstract_modules_open($("#abstract_modules_id").val());
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
            		$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
            	$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function delete_abstract_modules_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_abstract_modules_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				delete_abstract_modules_table_row(target_id);
				if ($("#abstract_modules_id").val() == target_id) {
					reset_abstract_modules_data();
					form_abstract_modules_close();
				}
                show_abstract_modules_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function form_abstract_modules_close() {
	
	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}
	
}
function create_abstract_modules_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add")." ".$title; ?>");

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_abstract_modules_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#abstract_modules_action").val("create");

}

function edit_abstract_modules_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_modules_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_modules_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#abstract_modules_id").val(response.values["abstract_modules_id"]);
				$("#abstract_modules_name").val(response.values["abstract_modules_name"]);
				$("#abstract_modules_name_stringlength").empty().append($("#abstract_modules_name").val().length + "/80");
				$("#abstract_modules_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_name").val().length < 240) {
					$("#abstract_modules_name_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_name_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_varname").val(response.values["abstract_modules_varname"]);
				$("#abstract_modules_varname_stringlength").empty().append($("#abstract_modules_varname").val().length + "/80");
				$("#abstract_modules_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_varname").val().length < 240) {
					$("#abstract_modules_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_description").val(response.values["abstract_modules_description"]);
				$("#abstract_modules_form_method").val(response.values["abstract_modules_form_method"]);
				if(response.values["abstract_modules_component_modules"] == abstract_modules_component_modules_default_value[0]){
					$("#abstract_modules_component_modules").prop("checked", true);
				} else {
					$("#abstract_modules_component_modules").prop("checked", false);
				}
				if(response.values["abstract_modules_component_groups"] == abstract_modules_component_groups_default_value[0]){
					$("#abstract_modules_component_groups").prop("checked", true);
				} else {
					$("#abstract_modules_component_groups").prop("checked", false);
				}
				if(response.values["abstract_modules_component_users"] == abstract_modules_component_users_default_value[0]){
					$("#abstract_modules_component_users").prop("checked", true);
				} else {
					$("#abstract_modules_component_users").prop("checked", false);
				}
				if(response.values["abstract_modules_component_languages"] == abstract_modules_component_languages_default_value[0]){
					$("#abstract_modules_component_languages").prop("checked", true);
				} else {
					$("#abstract_modules_component_languages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_pages"] == abstract_modules_component_pages_default_value[0]){
					$("#abstract_modules_component_pages").prop("checked", true);
				} else {
					$("#abstract_modules_component_pages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_media"] == abstract_modules_component_media_default_value[0]){
					$("#abstract_modules_component_media").prop("checked", true);
				} else {
					$("#abstract_modules_component_media").prop("checked", false);
				}
				if(response.values["abstract_modules_component_commerce"] == abstract_modules_component_commerce_default_value[0]){
					$("#abstract_modules_component_commerce").prop("checked", true);
				} else {
					$("#abstract_modules_component_commerce").prop("checked", false);
				}
				$("#abstract_modules_database_engine").val(response.values["abstract_modules_database_engine"]);
				if(response.values["abstract_modules_data_sortable"] == abstract_modules_data_sortable_default_value[0]){
					$("#abstract_modules_data_sortable").prop("checked", true);
				} else {
					$("#abstract_modules_data_sortable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_addable"] == abstract_modules_data_addable_default_value[0]){
					$("#abstract_modules_data_addable").prop("checked", true);
				} else {
					$("#abstract_modules_data_addable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_viewonly"] == abstract_modules_data_viewonly_default_value[0]){
					$("#abstract_modules_data_viewonly").prop("checked", true);
				} else {
					$("#abstract_modules_data_viewonly").prop("checked", false);
				}
				$("#abstract_modules_template").val(response.values["abstract_modules_template"]);
				$(".abstract_modules_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_category").val(response.values["abstract_modules_category"]);
				$("#abstract_modules_subject").val(response.values["abstract_modules_subject"]);
				$(".abstract_modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_order").val(response.values["abstract_modules_order"]);
				if(response.values["abstract_modules_activate"] == "1" ){
					$("#abstract_modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["abstract_modules_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#abstract_modules_action").val("edit");
				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&abstract_modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&abstract_modules_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&abstract_modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&abstract_modules_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_modules_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {
					
					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_abstract_modules_modal_prompt($(this).attr("data-content"), "revision_abstract_modules_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function copy_abstract_modules_open(target_id) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");
	
	$("#form-footer").empty();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_modules_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_modules_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy")." ".$title; ?>");

				$("#abstract_modules_id").val("");
				$("#abstract_modules_name").val(response.values["abstract_modules_name"]);
				$("#abstract_modules_name_stringlength").empty().append($("#abstract_modules_name").val().length + "/80");
				$("#abstract_modules_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_name").val().length < 240) {
					$("#abstract_modules_name_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_name_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_varname").val(response.values["abstract_modules_varname"]);
				$("#abstract_modules_varname_stringlength").empty().append($("#abstract_modules_varname").val().length + "/80");
				$("#abstract_modules_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_varname").val().length < 240) {
					$("#abstract_modules_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_description").val(response.values["abstract_modules_description"]);
				$("#abstract_modules_form_method").val(response.values["abstract_modules_form_method"]);
				if(response.values["abstract_modules_component_modules"] == abstract_modules_component_modules_default_value[0]){
					$("#abstract_modules_component_modules").prop("checked", true);
				} else {
					$("#abstract_modules_component_modules").prop("checked", false);
				}
				if(response.values["abstract_modules_component_groups"] == abstract_modules_component_groups_default_value[0]){
					$("#abstract_modules_component_groups").prop("checked", true);
				} else {
					$("#abstract_modules_component_groups").prop("checked", false);
				}
				if(response.values["abstract_modules_component_users"] == abstract_modules_component_users_default_value[0]){
					$("#abstract_modules_component_users").prop("checked", true);
				} else {
					$("#abstract_modules_component_users").prop("checked", false);
				}
				if(response.values["abstract_modules_component_languages"] == abstract_modules_component_languages_default_value[0]){
					$("#abstract_modules_component_languages").prop("checked", true);
				} else {
					$("#abstract_modules_component_languages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_pages"] == abstract_modules_component_pages_default_value[0]){
					$("#abstract_modules_component_pages").prop("checked", true);
				} else {
					$("#abstract_modules_component_pages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_media"] == abstract_modules_component_media_default_value[0]){
					$("#abstract_modules_component_media").prop("checked", true);
				} else {
					$("#abstract_modules_component_media").prop("checked", false);
				}
				if(response.values["abstract_modules_component_commerce"] == abstract_modules_component_commerce_default_value[0]){
					$("#abstract_modules_component_commerce").prop("checked", true);
				} else {
					$("#abstract_modules_component_commerce").prop("checked", false);
				}
				$("#abstract_modules_database_engine").val(response.values["abstract_modules_database_engine"]);
				if(response.values["abstract_modules_data_sortable"] == abstract_modules_data_sortable_default_value[0]){
					$("#abstract_modules_data_sortable").prop("checked", true);
				} else {
					$("#abstract_modules_data_sortable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_addable"] == abstract_modules_data_addable_default_value[0]){
					$("#abstract_modules_data_addable").prop("checked", true);
				} else {
					$("#abstract_modules_data_addable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_viewonly"] == abstract_modules_data_viewonly_default_value[0]){
					$("#abstract_modules_data_viewonly").prop("checked", true);
				} else {
					$("#abstract_modules_data_viewonly").prop("checked", false);
				}
				$("#abstract_modules_template").val(response.values["abstract_modules_template"]);
				$(".abstract_modules_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_category").val(response.values["abstract_modules_category"]);
				$("#abstract_modules_subject").val(response.values["abstract_modules_subject"]);
				$(".abstract_modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_order").val(response.values["abstract_modules_order"]);
				if(response.values["abstract_modules_activate"] == "1" ){
					$("#abstract_modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");
				
				$("#abstract_modules_action").val("copy");
				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&abstract_modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&abstract_modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&abstract_modules_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&abstract_modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&abstract_modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&abstract_modules_id=" + target_id);
						}
					}
				}
			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
	
}

function view_abstract_modules_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_abstract_modules_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_abstract_modules_modal_response(response.status, response.html);
				
				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function batch_generate_abstract_modules() {

	if (abstract_modules_data_all != null){

		var errors = 0;

		$("#module-table-block").addClass("block-opt-refresh");

		for (var i = 0; i < abstract_modules_data_all.length; i++) {

			$.ajax({
				url: url,
				type: "POST",
				cache: false,
				dataType: "json",
				data: {
					method: "generate_abstract_modules",
					parameters: abstract_modules_data_all[i]["abstract_modules_id"]
				},
				success: function(response) {
					if (response.status == false) {
						errors = errors + 1;
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					errors = errors + 1;
				}
			});
		}

		if (errors <= 0) {
			show_abstract_modules_modal_response(true, "<?php echo translate("Batch generating all") . " <strong>" . translate("Modules Abstracts") . "</strong> " . translate("are now in queue"); ?>");
		} else {
			show_abstract_modules_modal_response(false, "<strong>" + errors + " <?php echo translate("Modules Abstracts") . "</strong> " . translate("is unsuccessfully batch generated"); ?>");
		}

		$("#module-table-block").removeClass("block-opt-refresh");

	}

	

}


function generate_abstract_modules(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "generate_abstract_modules",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				show_abstract_modules_modal_response(response.status, response.message);
				
				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function regenerate_abstract_modules(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "regenerate_abstract_modules",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				show_abstract_modules_modal_response(response.status, response.message);
				
				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_abstract_modules_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_abstract_modules_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {
		
			reset_abstract_modules_data();
		
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#abstract_modules_id").val(response.values["abstract_modules_id"]);
				$("#abstract_modules_name").val(response.values["abstract_modules_name"]);
				$("#abstract_modules_name_stringlength").empty().append($("#abstract_modules_name").val().length + "/80");
				$("#abstract_modules_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_name").val().length < 240) {
					$("#abstract_modules_name_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_name_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_varname").val(response.values["abstract_modules_varname"]);
				$("#abstract_modules_varname_stringlength").empty().append($("#abstract_modules_varname").val().length + "/80");
				$("#abstract_modules_varname_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#abstract_modules_varname").val().length < 240) {
					$("#abstract_modules_varname_stringlength").addClass("text-success");
				} else {
					$("#abstract_modules_varname_stringlength").addClass("text-danger");
				}
				$("#abstract_modules_description").val(response.values["abstract_modules_description"]);
				$("#abstract_modules_form_method").val(response.values["abstract_modules_form_method"]);
				if(response.values["abstract_modules_component_modules"] == abstract_modules_component_modules_default_value[0]){
					$("#abstract_modules_component_modules").prop("checked", true);
				} else {
					$("#abstract_modules_component_modules").prop("checked", false);
				}
				if(response.values["abstract_modules_component_groups"] == abstract_modules_component_groups_default_value[0]){
					$("#abstract_modules_component_groups").prop("checked", true);
				} else {
					$("#abstract_modules_component_groups").prop("checked", false);
				}
				if(response.values["abstract_modules_component_users"] == abstract_modules_component_users_default_value[0]){
					$("#abstract_modules_component_users").prop("checked", true);
				} else {
					$("#abstract_modules_component_users").prop("checked", false);
				}
				if(response.values["abstract_modules_component_languages"] == abstract_modules_component_languages_default_value[0]){
					$("#abstract_modules_component_languages").prop("checked", true);
				} else {
					$("#abstract_modules_component_languages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_pages"] == abstract_modules_component_pages_default_value[0]){
					$("#abstract_modules_component_pages").prop("checked", true);
				} else {
					$("#abstract_modules_component_pages").prop("checked", false);
				}
				if(response.values["abstract_modules_component_media"] == abstract_modules_component_media_default_value[0]){
					$("#abstract_modules_component_media").prop("checked", true);
				} else {
					$("#abstract_modules_component_media").prop("checked", false);
				}
				if(response.values["abstract_modules_component_commerce"] == abstract_modules_component_commerce_default_value[0]){
					$("#abstract_modules_component_commerce").prop("checked", true);
				} else {
					$("#abstract_modules_component_commerce").prop("checked", false);
				}
				$("#abstract_modules_database_engine").val(response.values["abstract_modules_database_engine"]);
				if(response.values["abstract_modules_data_sortable"] == abstract_modules_data_sortable_default_value[0]){
					$("#abstract_modules_data_sortable").prop("checked", true);
				} else {
					$("#abstract_modules_data_sortable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_addable"] == abstract_modules_data_addable_default_value[0]){
					$("#abstract_modules_data_addable").prop("checked", true);
				} else {
					$("#abstract_modules_data_addable").prop("checked", false);
				}
				if(response.values["abstract_modules_data_viewonly"] == abstract_modules_data_viewonly_default_value[0]){
					$("#abstract_modules_data_viewonly").prop("checked", true);
				} else {
					$("#abstract_modules_data_viewonly").prop("checked", false);
				}
				$("#abstract_modules_template").val(response.values["abstract_modules_template"]);
				$(".abstract_modules_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_category").val(response.values["abstract_modules_category"]);
				$("#abstract_modules_subject").val(response.values["abstract_modules_subject"]);
				$(".abstract_modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['abstract_modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#abstract_modules_order").val(response.values["abstract_modules_order"]);
				if(response.values["abstract_modules_activate"] == "1" ){
					$("#abstract_modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#abstract_modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}
				
				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["abstract_modules_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_abstract_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}

function sort_abstract_modules_open() {

	$("#sort-content").css("visibility", "visible");
	$("#sort-content").fadeTo("slow", 1);
	$("#sort-content").removeClass("block-opt-hidden");
	$("#sort-content").fadeIn("slow").slideDown("fast");
	$("#sort-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_sort_abstract_modules",
			table_module_field: table_module_field
		},
		success: function(response) {
			
			$("#module-table").slideUp("fast");
			$("#sort-content").slideDown("fast");
			$("#module-sort").find(".sortable").empty();
			$("#module-sort").find(".sortable").append(response.html);
			
			$("ol.sortable").nestedSortable({
				forcePlaceholderSize: true,
				handle: "div",
				helper:	"clone",
				items: "li",
				opacity: .6,
				placeholder: "placeholder",
				revert: 250,
				tabSize: 25,
				tolerance: "pointer",
				toleranceElement: "> div",
				maxLevels: 1,

				isTree: true,
				expandOnHover: 700,
				startCollapsed: true
			});

			$(".disclose").on("click", function() {
				$(this).closest("li").toggleClass("mjs-nestedSortable-collapsed").toggleClass("mjs-nestedSortable-expanded");
			});
			
			$("#sort-content").removeClass("block-opt-refresh");
			
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-sort").offset().top - 160 + "px"
    }, "fast");

}
	
function save_sort_abstract_modules () {
	
	$("#sort-content").addClass("block-opt-refresh");
	
	$("#button-data-submit").prop("disabled", true);
	
	arraied = $("ol.sortable").nestedSortable("toArray", {startDepthCount: 0});
	
	$.ajax({
		type: "POST",
		url: url,
		data: {
			"method" : "sort_abstract_modules", 
			"parameters" : arraied 
		},
		dataType: "json", 
		success: function(response) {
			if (response.status == true) {
				$("#module-sort").find(".sortable").empty();
				show_abstract_modules_modal_response(response.status, response.message);
				$("#sort-content").slideUp("fast");
				$("#module-table").slideDown("fast");
			} else {
				show_abstract_modules_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
			}
			$("#button-data-submit").prop("disabled", false);
			$("#sort-content").removeClass("block-opt-refresh");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_abstract_modules_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#sort-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		}
	});
	
	
}
	
function sort_abstract_modules_close() {
	
	$("#module-sort").find(".sortable").empty();
	$("#sort-content").fadeTo("fast", 0);
	$("#sort-content").slideUp("fast").fadeOut("slow");
	$("#module-table").slideDown("fast");

}

function create_abstract_modules_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: <?php if (isset($configs["page_limit"]) && !empty($configs["page_limit"])) { echo $configs["page_limit"]; } else { echo "20"; } ?>,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_abstract_modules_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $("#datatable_length_selector").val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "" && search_text != null) {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "" && search_text != null) {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_abstract_modules_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $("#datatable_length_selector").val();
			if (limit != null) {
				location.href = "?limit=" + limit;
			} else {
				location.href = "" + limit;
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_abstract_modules_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_abstract_modules_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			if ($("#datatable-" + data_table["abstract_modules_id"]).length > 0) {
				$("#datatable").find("#datatable-" + data_table["abstract_modules_id"]).remove();
			}
			$("#datatable-list").prepend(response.html);
			if (count_data_table < datatable_data_limit) {
				create_abstract_modules_table();
			} else {
				create_abstract_modules_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_modules_open", $(this).attr("data-target"));
				} else {
					edit_abstract_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_modules_open", $(this).attr("data-target"));
				} else {
					copy_abstract_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_modules_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_abstract_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "delete_abstract_modules_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_abstract_modules_open($(this).attr("data-target"));
			});
			$(".btn-generate").unbind();
			$(".btn-generate").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "generate_abstract_modules", $(this).attr("data-target"));
			});
			$(".btn-regenerate").unbind();
			$(".btn-regenerate").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "regenerate_abstract_modules", $(this).attr("data-target"));
			});
		}
	});

}

function update_abstract_modules_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_abstract_modules_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			$("#datatable").find("#datatable-" + data_table["abstract_modules_id"]).empty();
			$("#datatable").find("#datatable-" + data_table["abstract_modules_id"]).append(response.html);
			if (count_data_table < datatable_data_limit) {
				create_abstract_modules_table();
			} else {
				create_abstract_modules_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_modules_open", $(this).attr("data-target"));
				} else {
					edit_abstract_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_modules_open", $(this).attr("data-target"));
				} else {
					copy_abstract_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_modules_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_abstract_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "delete_abstract_modules_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_abstract_modules_open($(this).attr("data-target"));
			});
			$(".btn-generate").unbind();
			$(".btn-generate").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "generate_abstract_modules", $(this).attr("data-target"));
			});
			$(".btn-regenerate").unbind();
			$(".btn-regenerate").click(function () {
				show_abstract_modules_modal_prompt($(this).attr("data-content"), "regenerate_abstract_modules", $(this).attr("data-target"));
			});
		}
	});

}

function delete_abstract_modules_table_row(field_id) {
	mainDataTable.fnDestroy();
	$("#datatable").find("#datatable-" + field_id).remove();
	if (count_data_table < datatable_data_limit) {
		create_abstract_modules_table();
	} else {
		create_abstract_modules_table_defer();
	}
}

function show_abstract_modules_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_abstract_modules_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {
	
	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_abstract_modules_open", $(this).attr("data-target"));
		} else {
			create_abstract_modules_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_abstract_modules_open", $(this).attr("data-target"));
		} else {
			edit_abstract_modules_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_abstract_modules_open", $(this).attr("data-target"));
		} else {
			copy_abstract_modules_open($(this).attr("data-target"));
		}
	});
	
	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_abstract_modules_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
		} else {
			translate_abstract_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});
	
	$(".btn-delete").click(function () {
		show_abstract_modules_modal_prompt($(this).attr("data-content"), "delete_abstract_modules_data", $(this).attr("data-target"));
	});
	
	$(".btn-view").click(function () {
		view_abstract_modules_open($(this).attr("data-target"));
	});

	$(".btn-batch-generate").click(function () {
		show_abstract_modules_modal_prompt($(this).attr("data-content"), "batch_generate_abstract_modules", "");
	});

	$(".btn-generate").click(function () {
		show_abstract_modules_modal_prompt($(this).attr("data-content"), "generate_abstract_modules", $(this).attr("data-target"));
	});

	$(".btn-regenerate").click(function () {
		show_abstract_modules_modal_prompt($(this).attr("data-content"), "regenerate_abstract_modules", $(this).attr("data-target"));
	});
	
	$(".btn-form-close").click(function () {
		show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_abstract_modules_close", "");
	});
	
	$(".btn-sort").click(function () {
		sort_abstract_modules_open();
	});
	
	$(".btn-sort-close").click(function () {
		show_abstract_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current sort") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "sort_abstract_modules_close", "");
	});
	
	$("#button-data-save-sort").click(function(){
		save_sort_abstract_modules();
	});
	
	$("#from").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});
	
	$("#to").datepicker({
		<?php 
		if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
			echo "language: \"" . $configs["backend_language"] . "\",";
		}
		?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});
	
	$("#abstract_modules_activate").click(function () {
		if ($("#abstract_modules_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});
	
	
	$("#abstract_modules_name").keyup(function(event){
		$("#abstract_modules_name_stringlength").empty().append($(this).val().length + "/80");
		$("#abstract_modules_name_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_modules_name_stringlength").addClass("text-success");
		} else {
			$("#abstract_modules_name_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_modules_varname").keyup(function(event){
		$("#abstract_modules_varname_stringlength").empty().append($(this).val().length + "/80");
		$("#abstract_modules_varname_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#abstract_modules_varname_stringlength").addClass("text-success");
		} else {
			$("#abstract_modules_varname_stringlength").addClass("text-danger");
		}
	});
	$("#abstract_modules_description").tinymce({

		setup: function (editor) {
			editor.on("change", function (e) {
				var content = this.getContent({
					/* Get only text not included HTML tags */
					/* format: "text" */
				});
				var element = this.getElement();
				/* var container = this.getContainer(); */
				/* var container_area_container = this.getContentAreaContainer(); */
				var options = {}
				validate_abstract_modules_description_tinymce(element, content, options);
			})
		},
		theme: "modern",
		skin: "greenmama", width: "100%",height: 150,
		convert_urls : true,
		relative_urls : true,
		/* autoresize : true, */

		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor color responsivefilemanager code"],

		toolbar1: "responsivefilemanager | undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor  | forecolor backcolor color | print preview | code",

		image_advtab: true ,
		external_filemanager_path: "plugins/filemanager/",
		filemanager_title: "File Manager" ,
		filemanager_access_key: "hflsHFSj45fjg-dgfMMh",
		external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}

	});
	$("#abstract_modules_form_method").val(abstract_modules_form_method_default_value[0]);
	$("#abstract_modules_form_method-check-loading").fadeOut("fast");
	$("#abstract_modules_database_engine").val(abstract_modules_database_engine_default_value[0]);
	$("#abstract_modules_database_engine-check-loading").fadeOut("fast");
	$("#abstract_modules_template-fileselector-btn").fancybox({
	  "width": 880,
	  "height": 900,
	  "minHeight": 500,
	  "type": "iframe",
	  "autoScale": "auto",
	  "padding": 0
	});
	$(".abstract_modules_icon").first().prop('checked', true);
	$("#abstract_modules_icon-check-loading").fadeOut("fast");
	$(".abstract_modules_subject_icon").first().prop('checked', true);
	$("#abstract_modules_subject_icon-check-loading").fadeOut("fast");
	
	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_abstract_modules_table();
	} else {
		create_abstract_modules_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_abstract_modules_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_abstract_modules_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_abstract_modules_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().abstract_modules_id != null) {
			edit_abstract_modules_open(get_url_param().abstract_modules_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().abstract_modules_id != null) {
			copy_abstract_modules_open(get_url_param().abstract_modules_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().abstract_modules_id != null) {
			translate_abstract_modules_open(get_url_param().abstract_modules_id, get_url_param().abstract_modules_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().abstract_modules_id != null) {
			view_abstract_modules_open(get_url_param().abstract_modules_id);
		}
	}
	/* URL response (end) */
	
	
	if ($("#abstract_modules_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}
	
	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");
	
});
</script>
<!-- initialization: JavaScript (end) -->

<script>
function validate_abstract_modules_description_tinymce (element, content, options){

    if (content == "" || content == null) {
		var error_text = "<?php echo translate("Module Description") . " " . translate("is required"); ?>";
    } else if (content.length <= options.min_value && content.length <= options.max_value) {
		var error_text = "<?php echo translate("Module Description") . " " . translate("is shorter than minimum length of strings"); ?> " + options.min_value;
    } else if (content.length >= options.max_value) {
		if ($("#" + element.id).parent().find(".bootstrap-maxlength").length <= 0) {
        	$("#" + element.id).parent().append("<span class=\"bootstrap-maxlength label-danger label\" style=\"display: block; left: 50%; position: absolute; white-sapce: nowrap; z-index: 1099;\">" + content.length + "/" + options.max_value + "<span>");
		} else {
			$("#" + element.id).parent().find(".bootstrap-maxlength").empty();
			$("#" + element.id).parent().find(".bootstrap-maxlength").append(content.length + "/" + options.max_value);
		}
    } else {
		$("#" + element.id).parent().find(".bootstrap-maxlength").remove();
    }


    if (error_text != "" && error_text != null) {
      if ($("#" + element.id).parent().find(".help-block").length <= 0) {
          $("#" + element.id).closest(".form-group > div").append("<div class=\"help-block animated fadeInDown\">" + error_text + "</div>");
          $("#" + element.id).closest(".form-group").addClass("has-error");
      } else {
          $("#" + element.id).parent().find(".help-block").empty();
          $("#" + element.id).parent().find(".help-block").append(error_text);
      }

    } else {
      $("#" + element.id).parent().find(".help-block").remove();
      $("#" + element.id).closest(".form-group").removeClass("has-error");
    }
	
}
/* 
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");
        
jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");
        
jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_abstract_modules_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"abstract_modules_name": {
					required: true,
				},
				"abstract_modules_varname": {
					required: false,
					no_specialchar_hard: true,
				},
				"abstract_modules_description": {
					required: false,
				},
				"abstract_modules_form_method": {
					required: true,
				},
				"abstract_modules_component_modules": {
					required: false,
				},
				"abstract_modules_component_groups": {
					required: false,
				},
				"abstract_modules_component_users": {
					required: false,
				},
				"abstract_modules_component_languages": {
					required: false,
				},
				"abstract_modules_component_pages": {
					required: false,
				},
				"abstract_modules_component_media": {
					required: false,
				},
				"abstract_modules_component_commerce": {
					required: false,
				},
				"abstract_modules_database_engine": {
					required: true,
				},
				"abstract_modules_data_sortable": {
					required: false,
				},
				"abstract_modules_data_addable": {
					required: false,
				},
				"abstract_modules_data_viewonly": {
					required: false,
				},
				"abstract_modules_template": {
					required: false,
				},
				"abstract_modules_icon": {
					required: false,
				},
				"abstract_modules_category": {
					required: false,
				},
				"abstract_modules_subject": {
					required: false,
				},
				"abstract_modules_subject_icon": {
					required: false,
				},
			 },
			 messages: {
				
				"abstract_modules_name": {
					required: "<strong><?php echo translate("Module Name"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_varname": {
					required: "<strong><?php echo translate("Module Variable Name"); ?></strong> <?php echo translate("is required"); ?>",
					no_specialchar_hard: "<strong><?php echo translate("Module Variable Name"); ?></strong> <?php echo translate("does not allow special characters and white spaces"); ?>",
				},
				"abstract_modules_description": {
					required: "<strong><?php echo translate("Module Description"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_form_method": {
					required: "<strong><?php echo translate("Backend Form Method"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_modules": {
					required: "<strong><?php echo translate("Modules Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_groups": {
					required: "<strong><?php echo translate("Groups Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_users": {
					required: "<strong><?php echo translate("Users Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_languages": {
					required: "<strong><?php echo translate("Languages Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_pages": {
					required: "<strong><?php echo translate("Pages Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_media": {
					required: "<strong><?php echo translate("Media Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_component_commerce": {
					required: "<strong><?php echo translate("Commerce Component"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_database_engine": {
					required: "<strong><?php echo translate("Database Engine"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_data_sortable": {
					required: "<strong><?php echo translate("Sortable Data"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_data_addable": {
					required: "<strong><?php echo translate("Addable Data"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_data_viewonly": {
					required: "<strong><?php echo translate("Data is View Only"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_template": {
					required: "<strong><?php echo translate("Front-end Inner Pages Default Template"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_icon": {
					required: "<strong><?php echo translate("Module Icon"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_category": {
					required: "<strong><?php echo translate("Module Category"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_subject": {
					required: "<strong><?php echo translate("Module Subject"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"abstract_modules_subject_icon": {
					required: "<strong><?php echo translate("Module Subject Icon"); ?></strong> <?php echo translate("is required"); ?>",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once("templates/".$configs["backend_template"]."/footer.php");
?>