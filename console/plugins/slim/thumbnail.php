<?php
// Link image type to correct image loader and saver
// - makes it easier to add additional types later on
// - makes the function easier to read

/**
 * @param $src - a valid file location
 * @param $dest - a valid file target
 * @param $targetWidth - desired output width
 * @param $targetHeight - desired output height or null
 */
function createThumbnail($src, $dest, $targetWidth, $targetHeight = null, $aspectratio = true, $quality = 100) {

    $image_handlers = array(
        "IMAGETYPE_JPEG" => array(
            'load' => 'imagecreatefromjpeg',
            'save' => 'imagejpeg',
            'quality' => 100
        ),
        "IMAGETYPE_PNG" => array(
            'load' => 'imagecreatefrompng',
            'save' => 'imagepng',
            'quality' => 0
        ),
        "IMAGETYPE_GIF" => array(
            'load' => 'imagecreatefromgif',
            'save' => 'imagegif'
        )
    );    

    // 1. Load the image from the given $src
    // - see if the file actually exists
    // - check if it's of a valid image type
    // - load the image resource

    // get the type of the image
    // we need the type to determine the correct loader
    $info   = getimagesize($src);
    if ($info["mime"] == "image/png") {
        $type = "IMAGETYPE_PNG";
    } else if ($info["mime"] == "image/gif") {
        $type = "IMAGETYPE_GIF";
    } else if ($info["mime"] == "image/jpeg") {
        $type = "IMAGETYPE_JPEG";
    }

    // if no valid type or no handler found -> exit
    if (!isset($type) || !isset($image_handlers[$type])) {
        return null;
    }
    
    // load the image with the correct loader
    $image = call_user_func($image_handlers[$type]['load'], $src);


    // no image found at supplied location -> exit
    if (!isset($image)) {
        return null;
    }


    // 2. Create a thumbnail and resize the loaded $image
    // - get the image dimensions
    // - define the output size appropriately
    // - create a thumbnail based on that size
    // - set alpha transparency for GIFs and PNGs
    // - draw the final thumbnail

    // get original image width and height
    $width = imagesx($image);
    $height = imagesy($image);

    // maintain aspect ratio when no height set
    if ($targetHeight == null) {

        // get width to height ratio
        $ratio = $width / $height;

        // if is portrait
        // use ratio to scale height to fit in square
        if ($width > $height) {
            $targetHeight = floor($targetWidth / $ratio);
        }
        // if is landscape
        // use ratio to scale width to fit in square
        else {
            $targetHeight = $targetWidth;
            $targetWidth = floor($targetWidth * $ratio);
        }
    }

    // create duplicate image based on calculated target size
    $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);
    
    // set transparency options for GIFs and PNGs
    if ($type == "IMAGETYPE_GIF" || $type == "IMAGETYPE_PNG") {

        // make image transparent
        imagecolortransparent(
            $thumbnail,
            imagecolorallocate($thumbnail, 0, 0, 0)
        );
		$quality = "";

        // additional settings for PNGs
        if ($type == "IMAGETYPE_PNG") {
            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
			$quality = 0;
        }
    }
    
    // copy entire source image to duplicate image and resize
	if ($aspectratio == true) {
		if ($width/$height > $targetWidth/$targetHeight) {
			$targetWidth_temp = round($width*($targetHeight/$height));
			$targetHeight_temp = $targetHeight;
			$targetWidth_temp_re = round($targetWidth*($height/$targetHeight));
			$targetHeight_temp_re = $height;
			$image_Crop_thumbnail_x = round(($width - $targetWidth_temp_re)/2);
			$image_Crop_thumbnail_y = 0;
		} else if ($width/$height < $targetWidth/$targetHeight) {
			$targetWidth_temp = $targetWidth;
			$targetHeight_temp = round($height*($targetWidth/$width));
			$targetWidth_temp_re = $width;
			$targetHeight_temp_re = round($targetHeight*($width/$targetWidth));
			$image_Crop_thumbnail_x = 0;
			$image_Crop_thumbnail_y = round(($height - $targetHeight_temp_re)/2);
		} else {
			$targetWidth_temp = $targetWidth;
			$targetHeight_temp = $targetHeight;
			$image_Crop_thumbnail_x = 0;
			$image_Crop_thumbnail_y = 0;
		}
		imagecopyresampled(
			$thumbnail, 
			$image, 0, 0, $image_Crop_thumbnail_x, $image_Crop_thumbnail_y, 
			$targetWidth_temp, $targetHeight_temp, 
			$width, $height
		);
	} else {
		imagecopyresampled(
			$thumbnail,
			$image,
			0, 0, 0, 0,
			$targetWidth, $targetHeight,
			$width, $height
		);
	}

    // 3. Save the $thumbnail to disk
    // - call the correct save method
    // - set the correct quality level

    // save the duplicate version of the image to disk
    return call_user_func(
        $image_handlers[$type]['save'],
        $thumbnail,
        $dest,
        $quality
    );
}
?>