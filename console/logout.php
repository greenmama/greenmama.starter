<?php 
include("system/include.php");

if (logout()) {
	header("Location: ".$configs["base_url"]."/".$configs["backend_url"]."/");
} else {
	header("Location: ".$configs["base_url"]."/");
}
?>