<?php
require_once("system/include.php");

/* configurations: Modules */
$modules = get_modules_data_by_id("5");
if (isset($modules) && !empty($modules)) {
	$title = translate($modules["modules_name"]);
	$module_page = $modules["modules_link"];
	$module_function_page = "system/".$modules["modules_function_link"];
} else {
	$title = translate("Modules");
	$module_page = "modules.php";
	$module_function_page = "system/core/".$configs["version"]."/modules.php";
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Modules */
if (!authentication_session_users()) {
	authentication_deny();
}
if (!authentication_session_modules($module_page)) {
	authentication_permission_deny();
}

if (isset($modules["modules_datatable_field"]) && !empty($modules["modules_datatable_field"])) {
	$table_module_field = array();
	for ($i = 0; $i < count($modules["modules_datatable_field"]); $i++) {
		if ($modules["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
			$modules_modules_datatable_field_display = true;
		} else {
			$modules_modules_datatable_field_display = false;
		}
		$table_module_field[$modules["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $modules_modules_datatable_field_display;
	}
} else {
	$table_module_field = array(
	"modules_id" => true,
	"modules_name" => true,
	"modules_description" => false,
	"modules_icon" => false,
	"modules_category" => false,
	"modules_subject" => false,
	"modules_subject_icon" => false,
	"modules_link" => false,
	"modules_function_link" => false,
	"modules_key" => false,
	"modules_db_name" => false,
	"modules_protected" => false,
	"modules_individual_pages_parent_link" => false,
	"modules_pages_parent_link_field" => false,
	"modules_pages_template" => false,
	"modules_pages_template_settings" => false,
	"modules_image_crop_quality" => false,
	"modules_image_crop_thumbnail" => false,
	"modules_image_crop_thumbnail_aspectratio" => false,
	"modules_image_crop_thumbnail_quality" => false,
	"modules_image_crop_thumbnail_width" => false,
	"modules_image_crop_thumbnail_height" => false,
	"modules_image_crop_large" => false,
	"modules_image_crop_large_aspectratio" => false,
	"modules_image_crop_large_quality" => false,
	"modules_image_crop_large_width" => false,
	"modules_image_crop_large_height" => false,
	"modules_datatable_field" => false,
	"modules_date_created" => true,
	"users_id" => false,
	"users_username" => false,
	"users_name" => true,
	"users_last_name" => false,
	"modules_activate" => true,
	"pages_id" => false,
	"pages_link" => false,
	"modules_actions" => true,
);
}

require_once("templates/".$configs["backend_template"]."/header.php");
require_once("templates/".$configs["backend_template"]."/overlay.php");
require_once("templates/".$configs["backend_template"]."/sidebar.php");
require_once("templates/".$configs["backend_template"]."/navbar.php");
require_once("templates/".$configs["backend_template"]."/container-header.php");
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo $module_function_page; ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="modules">
					<input type="hidden" id="modules_id" name="modules_id">
					<input type="hidden" id="modules_action" name="modules_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="modules_name"><?php echo translate("Name"); ?><span class="text-danger">*</span> </label>
							<div class="col-md-12">
								<input class="form-control modules_name" type="text" id="modules_name" name="modules_name" value="" placeholder="<?php echo translate("Name of module");?>" >

								<div class="help-block"><?php echo translate("Identify module"); ?></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="modules_description"><?php echo translate("Description"); ?></label>
							<div class="col-md-12">
								<textarea class="form-control modules_description" id="modules_description" name="modules_description"  placeholder="<?php echo translate("Description of module");?>" ><?php $modules_description_default_value[0]; ?></textarea>
								<div class="help-block"><?php echo translate("Describe what module is about"); ?></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
            				<div class="col-md-12">
								<div id="icon_list_parent" class="panel-group">
									<div class="panel panel-default">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#icon_list_parent" href="#icon_list">
											<div class="panel-heading">
												<span class="panel-title"><?php echo translate("Icon"); ?></span> <i class="fa fa-angle-down"></i>
											</div>
										</a>

                    					<div id="icon_list" class="panel-collapse collapse">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-12">
														<div class="form-group">
															<div class="col-md-12">
																<div class="row items-push">
																	<div class="col-md-12" style="margin-bottom: 0;">
																		<label class="css-input css-radio css-radio-default push-10-r">
																			<input type="radio" id="modules_icon_" name="modules_icon" class="modules_icon" checked  value=""><span></span> <?php echo translate("None"); ?>
																		</label>
																	</div>
																</div>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siactionredoi" name="modules_icon" class="modules_icon"  value="<i class='si si-action-redo'></i>"><span></span> <i class='si si-action-redo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siactionundoi" name="modules_icon" class="modules_icon"  value="<i class='si si-action-undo'></i>"><span></span> <i class='si si-action-undo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siarrowdowni" name="modules_icon" class="modules_icon"  value="<i class='si si-arrow-down'></i>"><span></span> <i class='si si-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siarrowlefti" name="modules_icon" class="modules_icon"  value="<i class='si si-arrow-left'></i>"><span></span> <i class='si si-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siarrowrighti" name="modules_icon" class="modules_icon"  value="<i class='si si-arrow-right'></i>"><span></span> <i class='si si-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siarrowupi" name="modules_icon" class="modules_icon"  value="<i class='si si-arrow-up'></i>"><span></span> <i class='si si-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sianchori" name="modules_icon" class="modules_icon"  value="<i class='si si-anchor'></i>"><span></span> <i class='si si-anchor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibadgei" name="modules_icon" class="modules_icon"  value="<i class='si si-badge'></i>"><span></span> <i class='si si-badge'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibagi" name="modules_icon" class="modules_icon"  value="<i class='si si-bag'></i>"><span></span> <i class='si si-bag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibani" name="modules_icon" class="modules_icon"  value="<i class='si si-ban'></i>"><span></span> <i class='si si-ban'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibarcharti" name="modules_icon" class="modules_icon"  value="<i class='si si-bar-chart'></i>"><span></span> <i class='si si-bar-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibasketi" name="modules_icon" class="modules_icon"  value="<i class='si si-basket'></i>"><span></span> <i class='si si-basket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibasketloadedi" name="modules_icon" class="modules_icon"  value="<i class='si si-basket-loaded'></i>"><span></span> <i class='si si-basket-loaded'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibelli" name="modules_icon" class="modules_icon"  value="<i class='si si-bell'></i>"><span></span> <i class='si si-bell'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibookopeni" name="modules_icon" class="modules_icon"  value="<i class='si si-book-open'></i>"><span></span> <i class='si si-book-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibriefcasei" name="modules_icon" class="modules_icon"  value="<i class='si si-briefcase'></i>"><span></span> <i class='si si-briefcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibubblei" name="modules_icon" class="modules_icon"  value="<i class='si si-bubble'></i>"><span></span> <i class='si si-bubble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibubblesi" name="modules_icon" class="modules_icon"  value="<i class='si si-bubbles'></i>"><span></span> <i class='si si-bubbles'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sibulbi" name="modules_icon" class="modules_icon"  value="<i class='si si-bulb'></i>"><span></span> <i class='si si-bulb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicalculatori" name="modules_icon" class="modules_icon"  value="<i class='si si-calculator'></i>"><span></span> <i class='si si-calculator'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicalendari" name="modules_icon" class="modules_icon"  value="<i class='si si-calendar'></i>"><span></span> <i class='si si-calendar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicallendi" name="modules_icon" class="modules_icon"  value="<i class='si si-call-end'></i>"><span></span> <i class='si si-call-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicallini" name="modules_icon" class="modules_icon"  value="<i class='si si-call-in'></i>"><span></span> <i class='si si-call-in'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicallouti" name="modules_icon" class="modules_icon"  value="<i class='si si-call-out'></i>"><span></span> <i class='si si-call-out'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicamcorderi" name="modules_icon" class="modules_icon"  value="<i class='si si-camcorder'></i>"><span></span> <i class='si si-camcorder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicamerai" name="modules_icon" class="modules_icon"  value="<i class='si si-camera'></i>"><span></span> <i class='si si-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sichecki" name="modules_icon" class="modules_icon"  value="<i class='si si-check'></i>"><span></span> <i class='si si-check'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sichemistryi" name="modules_icon" class="modules_icon"  value="<i class='si si-chemistry'></i>"><span></span> <i class='si si-chemistry'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siclocki" name="modules_icon" class="modules_icon"  value="<i class='si si-clock'></i>"><span></span> <i class='si si-clock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siclouddownloadi" name="modules_icon" class="modules_icon"  value="<i class='si si-cloud-download'></i>"><span></span> <i class='si si-cloud-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siclouduploadi" name="modules_icon" class="modules_icon"  value="<i class='si si-cloud-upload'></i>"><span></span> <i class='si si-cloud-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicompassi" name="modules_icon" class="modules_icon"  value="<i class='si si-compass'></i>"><span></span> <i class='si si-compass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolendi" name="modules_icon" class="modules_icon"  value="<i class='si si-control-end'></i>"><span></span> <i class='si si-control-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolforwardi" name="modules_icon" class="modules_icon"  value="<i class='si si-control-forward'></i>"><span></span> <i class='si si-control-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolplayi" name="modules_icon" class="modules_icon"  value="<i class='si si-control-play'></i>"><span></span> <i class='si si-control-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolpausei" name="modules_icon" class="modules_icon"  value="<i class='si si-control-pause'></i>"><span></span> <i class='si si-control-pause'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolrewindi" name="modules_icon" class="modules_icon"  value="<i class='si si-control-rewind'></i>"><span></span> <i class='si si-control-rewind'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicontrolstarti" name="modules_icon" class="modules_icon"  value="<i class='si si-control-start'></i>"><span></span> <i class='si si-control-start'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicreditcardi" name="modules_icon" class="modules_icon"  value="<i class='si si-credit-card'></i>"><span></span> <i class='si si-credit-card'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicropi" name="modules_icon" class="modules_icon"  value="<i class='si si-crop'></i>"><span></span> <i class='si si-crop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicupi" name="modules_icon" class="modules_icon"  value="<i class='si si-cup'></i>"><span></span> <i class='si si-cup'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicursori" name="modules_icon" class="modules_icon"  value="<i class='si si-cursor'></i>"><span></span> <i class='si si-cursor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sicursormovei" name="modules_icon" class="modules_icon"  value="<i class='si si-cursor-move'></i>"><span></span> <i class='si si-cursor-move'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidiamondi" name="modules_icon" class="modules_icon"  value="<i class='si si-diamond'></i>"><span></span> <i class='si si-diamond'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidirectioni" name="modules_icon" class="modules_icon"  value="<i class='si si-direction'></i>"><span></span> <i class='si si-direction'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidirectionsi" name="modules_icon" class="modules_icon"  value="<i class='si si-directions'></i>"><span></span> <i class='si si-directions'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidisci" name="modules_icon" class="modules_icon"  value="<i class='si si-disc'></i>"><span></span> <i class='si si-disc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidislikei" name="modules_icon" class="modules_icon"  value="<i class='si si-dislike'></i>"><span></span> <i class='si si-dislike'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidoci" name="modules_icon" class="modules_icon"  value="<i class='si si-doc'></i>"><span></span> <i class='si si-doc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidocsi" name="modules_icon" class="modules_icon"  value="<i class='si si-docs'></i>"><span></span> <i class='si si-docs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidraweri" name="modules_icon" class="modules_icon"  value="<i class='si si-drawer'></i>"><span></span> <i class='si si-drawer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sidropi" name="modules_icon" class="modules_icon"  value="<i class='si si-drop'></i>"><span></span> <i class='si si-drop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siearphonesi" name="modules_icon" class="modules_icon"  value="<i class='si si-earphones'></i>"><span></span> <i class='si si-earphones'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siearphonesalti" name="modules_icon" class="modules_icon"  value="<i class='si si-earphones-alt'></i>"><span></span> <i class='si si-earphones-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siemoticonsmilei" name="modules_icon" class="modules_icon"  value="<i class='si si-emoticon-smile'></i>"><span></span> <i class='si si-emoticon-smile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sienergyi" name="modules_icon" class="modules_icon"  value="<i class='si si-energy'></i>"><span></span> <i class='si si-energy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sienvelopei" name="modules_icon" class="modules_icon"  value="<i class='si si-envelope'></i>"><span></span> <i class='si si-envelope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sienvelopeletteri" name="modules_icon" class="modules_icon"  value="<i class='si si-envelope-letter'></i>"><span></span> <i class='si si-envelope-letter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sienvelopeopeni" name="modules_icon" class="modules_icon"  value="<i class='si si-envelope-open'></i>"><span></span> <i class='si si-envelope-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siequalizeri" name="modules_icon" class="modules_icon"  value="<i class='si si-equalizer'></i>"><span></span> <i class='si si-equalizer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sieyei" name="modules_icon" class="modules_icon"  value="<i class='si si-eye'></i>"><span></span> <i class='si si-eye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sieyeglassesi" name="modules_icon" class="modules_icon"  value="<i class='si si-eyeglasses'></i>"><span></span> <i class='si si-eyeglasses'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sifeedi" name="modules_icon" class="modules_icon"  value="<i class='si si-feed'></i>"><span></span> <i class='si si-feed'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sifilmi" name="modules_icon" class="modules_icon"  value="<i class='si si-film'></i>"><span></span> <i class='si si-film'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sifirei" name="modules_icon" class="modules_icon"  value="<i class='si si-fire'></i>"><span></span> <i class='si si-fire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siflagi" name="modules_icon" class="modules_icon"  value="<i class='si si-flag'></i>"><span></span> <i class='si si-flag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sifolderi" name="modules_icon" class="modules_icon"  value="<i class='si si-folder'></i>"><span></span> <i class='si si-folder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sifolderalti" name="modules_icon" class="modules_icon"  value="<i class='si si-folder-alt'></i>"><span></span> <i class='si si-folder-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siframei" name="modules_icon" class="modules_icon"  value="<i class='si si-frame'></i>"><span></span> <i class='si si-frame'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sigamecontrolleri" name="modules_icon" class="modules_icon"  value="<i class='si si-game-controller'></i>"><span></span> <i class='si si-game-controller'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sighosti" name="modules_icon" class="modules_icon"  value="<i class='si si-ghost'></i>"><span></span> <i class='si si-ghost'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siglobei" name="modules_icon" class="modules_icon"  value="<i class='si si-globe'></i>"><span></span> <i class='si si-globe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siglobealti" name="modules_icon" class="modules_icon"  value="<i class='si si-globe-alt'></i>"><span></span> <i class='si si-globe-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sigraduationi" name="modules_icon" class="modules_icon"  value="<i class='si si-graduation'></i>"><span></span> <i class='si si-graduation'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sigraphi" name="modules_icon" class="modules_icon"  value="<i class='si si-graph'></i>"><span></span> <i class='si si-graph'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sigridi" name="modules_icon" class="modules_icon"  value="<i class='si si-grid'></i>"><span></span> <i class='si si-grid'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sihandbagi" name="modules_icon" class="modules_icon"  value="<i class='si si-handbag'></i>"><span></span> <i class='si si-handbag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sihearti" name="modules_icon" class="modules_icon"  value="<i class='si si-heart'></i>"><span></span> <i class='si si-heart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sihomei" name="modules_icon" class="modules_icon"  value="<i class='si si-home'></i>"><span></span> <i class='si si-home'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sihourglassi" name="modules_icon" class="modules_icon"  value="<i class='si si-hourglass'></i>"><span></span> <i class='si si-hourglass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siinfoi" name="modules_icon" class="modules_icon"  value="<i class='si si-info'></i>"><span></span> <i class='si si-info'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sikeyi" name="modules_icon" class="modules_icon"  value="<i class='si si-key'></i>"><span></span> <i class='si si-key'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silayersi" name="modules_icon" class="modules_icon"  value="<i class='si si-layers'></i>"><span></span> <i class='si si-layers'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silikei" name="modules_icon" class="modules_icon"  value="<i class='si si-like'></i>"><span></span> <i class='si si-like'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silinki" name="modules_icon" class="modules_icon"  value="<i class='si si-link'></i>"><span></span> <i class='si si-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silisti" name="modules_icon" class="modules_icon"  value="<i class='si si-list'></i>"><span></span> <i class='si si-list'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silocki" name="modules_icon" class="modules_icon"  value="<i class='si si-lock'></i>"><span></span> <i class='si si-lock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silockopeni" name="modules_icon" class="modules_icon"  value="<i class='si si-lock-open'></i>"><span></span> <i class='si si-lock-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silogini" name="modules_icon" class="modules_icon"  value="<i class='si si-login'></i>"><span></span> <i class='si si-login'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-silogouti" name="modules_icon" class="modules_icon"  value="<i class='si si-logout'></i>"><span></span> <i class='si si-logout'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siloopi" name="modules_icon" class="modules_icon"  value="<i class='si si-loop'></i>"><span></span> <i class='si si-loop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simagicwandi" name="modules_icon" class="modules_icon"  value="<i class='si si-magic-wand'></i>"><span></span> <i class='si si-magic-wand'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simagneti" name="modules_icon" class="modules_icon"  value="<i class='si si-magnet'></i>"><span></span> <i class='si si-magnet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simagnifieri" name="modules_icon" class="modules_icon"  value="<i class='si si-magnifier'></i>"><span></span> <i class='si si-magnifier'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simagnifieraddi" name="modules_icon" class="modules_icon"  value="<i class='si si-magnifier-add'></i>"><span></span> <i class='si si-magnifier-add'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simagnifierremovei" name="modules_icon" class="modules_icon"  value="<i class='si si-magnifier-remove'></i>"><span></span> <i class='si si-magnifier-remove'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simapi" name="modules_icon" class="modules_icon"  value="<i class='si si-map'></i>"><span></span> <i class='si si-map'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simicrophonei" name="modules_icon" class="modules_icon"  value="<i class='si si-microphone'></i>"><span></span> <i class='si si-microphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simousei" name="modules_icon" class="modules_icon"  value="<i class='si si-mouse'></i>"><span></span> <i class='si si-mouse'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simoustachei" name="modules_icon" class="modules_icon"  value="<i class='si si-moustache'></i>"><span></span> <i class='si si-moustache'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simusictonei" name="modules_icon" class="modules_icon"  value="<i class='si si-music-tone'></i>"><span></span> <i class='si si-music-tone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-simusictonealti" name="modules_icon" class="modules_icon"  value="<i class='si si-music-tone-alt'></i>"><span></span> <i class='si si-music-tone-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sinotei" name="modules_icon" class="modules_icon"  value="<i class='si si-note'></i>"><span></span> <i class='si si-note'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sinotebooki" name="modules_icon" class="modules_icon"  value="<i class='si si-notebook'></i>"><span></span> <i class='si si-notebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipaperclipi" name="modules_icon" class="modules_icon"  value="<i class='si si-paper-clip'></i>"><span></span> <i class='si si-paper-clip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipaperplanei" name="modules_icon" class="modules_icon"  value="<i class='si si-paper-plane'></i>"><span></span> <i class='si si-paper-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipencili" name="modules_icon" class="modules_icon"  value="<i class='si si-pencil'></i>"><span></span> <i class='si si-pencil'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipicturei" name="modules_icon" class="modules_icon"  value="<i class='si si-picture'></i>"><span></span> <i class='si si-picture'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipiecharti" name="modules_icon" class="modules_icon"  value="<i class='si si-pie-chart'></i>"><span></span> <i class='si si-pie-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipini" name="modules_icon" class="modules_icon"  value="<i class='si si-pin'></i>"><span></span> <i class='si si-pin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siplanei" name="modules_icon" class="modules_icon"  value="<i class='si si-plane'></i>"><span></span> <i class='si si-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siplaylisti" name="modules_icon" class="modules_icon"  value="<i class='si si-playlist'></i>"><span></span> <i class='si si-playlist'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siplusi" name="modules_icon" class="modules_icon"  value="<i class='si si-plus'></i>"><span></span> <i class='si si-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipointeri" name="modules_icon" class="modules_icon"  value="<i class='si si-pointer'></i>"><span></span> <i class='si si-pointer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipoweri" name="modules_icon" class="modules_icon"  value="<i class='si si-power'></i>"><span></span> <i class='si si-power'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipresenti" name="modules_icon" class="modules_icon"  value="<i class='si si-present'></i>"><span></span> <i class='si si-present'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siprinteri" name="modules_icon" class="modules_icon"  value="<i class='si si-printer'></i>"><span></span> <i class='si si-printer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sipuzzlei" name="modules_icon" class="modules_icon"  value="<i class='si si-puzzle'></i>"><span></span> <i class='si si-puzzle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siquestioni" name="modules_icon" class="modules_icon"  value="<i class='si si-question'></i>"><span></span> <i class='si si-question'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sirefreshi" name="modules_icon" class="modules_icon"  value="<i class='si si-refresh'></i>"><span></span> <i class='si si-refresh'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sireloadi" name="modules_icon" class="modules_icon"  value="<i class='si si-reload'></i>"><span></span> <i class='si si-reload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sirocketi" name="modules_icon" class="modules_icon"  value="<i class='si si-rocket'></i>"><span></span> <i class='si si-rocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siscreendesktopi" name="modules_icon" class="modules_icon"  value="<i class='si si-screen-desktop'></i>"><span></span> <i class='si si-screen-desktop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siscreensmartphonei" name="modules_icon" class="modules_icon"  value="<i class='si si-screen-smartphone'></i>"><span></span> <i class='si si-screen-smartphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siscreentableti" name="modules_icon" class="modules_icon"  value="<i class='si si-screen-tablet'></i>"><span></span> <i class='si si-screen-tablet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisettingsi" name="modules_icon" class="modules_icon"  value="<i class='si si-settings'></i>"><span></span> <i class='si si-settings'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisharei" name="modules_icon" class="modules_icon"  value="<i class='si si-share'></i>"><span></span> <i class='si si-share'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisharealti" name="modules_icon" class="modules_icon"  value="<i class='si si-share-alt'></i>"><span></span> <i class='si si-share-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sishieldi" name="modules_icon" class="modules_icon"  value="<i class='si si-shield'></i>"><span></span> <i class='si si-shield'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sishufflei" name="modules_icon" class="modules_icon"  value="<i class='si si-shuffle'></i>"><span></span> <i class='si si-shuffle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisizeactuali" name="modules_icon" class="modules_icon"  value="<i class='si si-size-actual'></i>"><span></span> <i class='si si-size-actual'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisizefullscreeni" name="modules_icon" class="modules_icon"  value="<i class='si si-size-fullscreen'></i>"><span></span> <i class='si si-size-fullscreen'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialdribbblei" name="modules_icon" class="modules_icon"  value="<i class='si si-social-dribbble'></i>"><span></span> <i class='si si-social-dribbble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialdropboxi" name="modules_icon" class="modules_icon"  value="<i class='si si-social-dropbox'></i>"><span></span> <i class='si si-social-dropbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialfacebooki" name="modules_icon" class="modules_icon"  value="<i class='si si-social-facebook'></i>"><span></span> <i class='si si-social-facebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialtumblri" name="modules_icon" class="modules_icon"  value="<i class='si si-social-tumblr'></i>"><span></span> <i class='si si-social-tumblr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialtwitteri" name="modules_icon" class="modules_icon"  value="<i class='si si-social-twitter'></i>"><span></span> <i class='si si-social-twitter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisocialyoutubei" name="modules_icon" class="modules_icon"  value="<i class='si si-social-youtube'></i>"><span></span> <i class='si si-social-youtube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sispeechi" name="modules_icon" class="modules_icon"  value="<i class='si si-speech'></i>"><span></span> <i class='si si-speech'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sispeedometeri" name="modules_icon" class="modules_icon"  value="<i class='si si-speedometer'></i>"><span></span> <i class='si si-speedometer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sistari" name="modules_icon" class="modules_icon"  value="<i class='si si-star'></i>"><span></span> <i class='si si-star'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisupporti" name="modules_icon" class="modules_icon"  value="<i class='si si-support'></i>"><span></span> <i class='si si-support'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisymbolfemalei" name="modules_icon" class="modules_icon"  value="<i class='si si-symbol-female'></i>"><span></span> <i class='si si-symbol-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sisymbolmalei" name="modules_icon" class="modules_icon"  value="<i class='si si-symbol-male'></i>"><span></span> <i class='si si-symbol-male'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sitagi" name="modules_icon" class="modules_icon"  value="<i class='si si-tag'></i>"><span></span> <i class='si si-tag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sitargeti" name="modules_icon" class="modules_icon"  value="<i class='si si-target'></i>"><span></span> <i class='si si-target'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sitrashi" name="modules_icon" class="modules_icon"  value="<i class='si si-trash'></i>"><span></span> <i class='si si-trash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sitrophyi" name="modules_icon" class="modules_icon"  value="<i class='si si-trophy'></i>"><span></span> <i class='si si-trophy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siumbrellai" name="modules_icon" class="modules_icon"  value="<i class='si si-umbrella'></i>"><span></span> <i class='si si-umbrella'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siuseri" name="modules_icon" class="modules_icon"  value="<i class='si si-user'></i>"><span></span> <i class='si si-user'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siuserfemalei" name="modules_icon" class="modules_icon"  value="<i class='si si-user-female'></i>"><span></span> <i class='si si-user-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siuserfollowi" name="modules_icon" class="modules_icon"  value="<i class='si si-user-follow'></i>"><span></span> <i class='si si-user-follow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siuserfollowingi" name="modules_icon" class="modules_icon"  value="<i class='si si-user-following'></i>"><span></span> <i class='si si-user-following'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siuserunfollowi" name="modules_icon" class="modules_icon"  value="<i class='si si-user-unfollow'></i>"><span></span> <i class='si si-user-unfollow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siusersi" name="modules_icon" class="modules_icon"  value="<i class='si si-users'></i>"><span></span> <i class='si si-users'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sivectori" name="modules_icon" class="modules_icon"  value="<i class='si si-vector'></i>"><span></span> <i class='si si-vector'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sivolume1i" name="modules_icon" class="modules_icon"  value="<i class='si si-volume-1'></i>"><span></span> <i class='si si-volume-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sivolume2i" name="modules_icon" class="modules_icon"  value="<i class='si si-volume-2'></i>"><span></span> <i class='si si-volume-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-sivolumeoffi" name="modules_icon" class="modules_icon"  value="<i class='si si-volume-off'></i>"><span></span> <i class='si si-volume-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siwalleti" name="modules_icon" class="modules_icon"  value="<i class='si si-wallet'></i>"><span></span> <i class='si si-wallet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classsi-siwrenchi" name="modules_icon" class="modules_icon"  value="<i class='si si-wrench'></i>"><span></span> <i class='si si-wrench'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fa500pxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-500px'></i>"><span></span> <i class='fa fa-500px'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faadjusti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-adjust'></i>"><span></span> <i class='fa fa-adjust'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faadni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-adn'></i>"><span></span> <i class='fa fa-adn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faaligncenteri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-align-center'></i>"><span></span> <i class='fa fa-align-center'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faalignjustifyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-align-justify'></i>"><span></span> <i class='fa fa-align-justify'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faalignlefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-align-left'></i>"><span></span> <i class='fa fa-align-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faalignrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-align-right'></i>"><span></span> <i class='fa fa-align-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faamazoni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-amazon'></i>"><span></span> <i class='fa fa-amazon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faambulancei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ambulance'></i>"><span></span> <i class='fa fa-ambulance'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faanchori" name="modules_icon" class="modules_icon"  value="<i class='fa fa-anchor'></i>"><span></span> <i class='fa fa-anchor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faandroidi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-android'></i>"><span></span> <i class='fa fa-android'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangellisti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angellist'></i>"><span></span> <i class='fa fa-angellist'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangledoubledowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-double-down'></i>"><span></span> <i class='fa fa-angle-double-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangledoublelefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-double-left'></i>"><span></span> <i class='fa fa-angle-double-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangledoublerighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-double-right'></i>"><span></span> <i class='fa fa-angle-double-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangledoubleupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-double-up'></i>"><span></span> <i class='fa fa-angle-double-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangledowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-down'></i>"><span></span> <i class='fa fa-angle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faanglelefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-left'></i>"><span></span> <i class='fa fa-angle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faanglerighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-right'></i>"><span></span> <i class='fa fa-angle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faangleupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-angle-up'></i>"><span></span> <i class='fa fa-angle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faapplei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-apple'></i>"><span></span> <i class='fa fa-apple'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarchivei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-archive'></i>"><span></span> <i class='fa fa-archive'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faareacharti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-area-chart'></i>"><span></span> <i class='fa fa-area-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircledowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-down'></i>"><span></span> <i class='fa fa-arrow-circle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcirclelefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-left'></i>"><span></span> <i class='fa fa-arrow-circle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcirclerighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-right'></i>"><span></span> <i class='fa fa-arrow-circle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircleupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-up'></i>"><span></span> <i class='fa fa-arrow-circle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircleodowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-o-down'></i>"><span></span> <i class='fa fa-arrow-circle-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircleolefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-o-left'></i>"><span></span> <i class='fa fa-arrow-circle-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircleorighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-o-right'></i>"><span></span> <i class='fa fa-arrow-circle-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowcircleoupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-circle-o-up'></i>"><span></span> <i class='fa fa-arrow-circle-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowdowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-down'></i>"><span></span> <i class='fa fa-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowlefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-left'></i>"><span></span> <i class='fa fa-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-right'></i>"><span></span> <i class='fa fa-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrow-up'></i>"><span></span> <i class='fa fa-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowsalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrows-alt'></i>"><span></span> <i class='fa fa-arrows-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowshi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrows-h'></i>"><span></span> <i class='fa fa-arrows-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowsvi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrows-v'></i>"><span></span> <i class='fa fa-arrows-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faarrowsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-arrows'></i>"><span></span> <i class='fa fa-arrows'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faasteriski" name="modules_icon" class="modules_icon"  value="<i class='fa fa-asterisk'></i>"><span></span> <i class='fa fa-asterisk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faati" name="modules_icon" class="modules_icon"  value="<i class='fa fa-at'></i>"><span></span> <i class='fa fa-at'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faautomobilei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-automobile'></i>"><span></span> <i class='fa fa-automobile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabackwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-backward'></i>"><span></span> <i class='fa fa-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabalancescalei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-balance-scale'></i>"><span></span> <i class='fa fa-balance-scale'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabani" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ban'></i>"><span></span> <i class='fa fa-ban'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabanki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bank'></i>"><span></span> <i class='fa fa-bank'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabarcharti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bar-chart'></i>"><span></span> <i class='fa fa-bar-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabarchartoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bar-chart-o'></i>"><span></span> <i class='fa fa-bar-chart-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabarcodei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-barcode'></i>"><span></span> <i class='fa fa-barcode'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabarsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bars'></i>"><span></span> <i class='fa fa-bars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabattery0i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-battery-0'></i>"><span></span> <i class='fa fa-battery-0'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabattery1i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-battery-1'></i>"><span></span> <i class='fa fa-battery-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabattery2i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-battery-2'></i>"><span></span> <i class='fa fa-battery-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabattery3i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-battery-3'></i>"><span></span> <i class='fa fa-battery-3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabattery4i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-battery-4'></i>"><span></span> <i class='fa fa-battery-4'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabedi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bed'></i>"><span></span> <i class='fa fa-bed'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabeeri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-beer'></i>"><span></span> <i class='fa fa-beer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabehancei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-behance'></i>"><span></span> <i class='fa fa-behance'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabehancesquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-behance-square'></i>"><span></span> <i class='fa fa-behance-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabelli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bell'></i>"><span></span> <i class='fa fa-bell'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabelloi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bell-o'></i>"><span></span> <i class='fa fa-bell-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabellslashi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bell-slash'></i>"><span></span> <i class='fa fa-bell-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabellslashoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bell-slash-o'></i>"><span></span> <i class='fa fa-bell-slash-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabicyclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bicycle'></i>"><span></span> <i class='fa fa-bicycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabinocularsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-binoculars'></i>"><span></span> <i class='fa fa-binoculars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabirthdaycakei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-birthday-cake'></i>"><span></span> <i class='fa fa-birthday-cake'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabitbucketi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bitbucket'></i>"><span></span> <i class='fa fa-bitbucket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabitbucketsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bitbucket-square'></i>"><span></span> <i class='fa fa-bitbucket-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabitcoini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bitcoin'></i>"><span></span> <i class='fa fa-bitcoin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fablacktiei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-black-tie'></i>"><span></span> <i class='fa fa-black-tie'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faboldi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bold'></i>"><span></span> <i class='fa fa-bold'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabolti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bolt'></i>"><span></span> <i class='fa fa-bolt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabombi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bomb'></i>"><span></span> <i class='fa fa-bomb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabooki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-book'></i>"><span></span> <i class='fa fa-book'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabookmarki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bookmark'></i>"><span></span> <i class='fa fa-bookmark'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabookmarkoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bookmark-o'></i>"><span></span> <i class='fa fa-bookmark-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabriefcasei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-briefcase'></i>"><span></span> <i class='fa fa-briefcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabugi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bug'></i>"><span></span> <i class='fa fa-bug'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabuildingi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-building'></i>"><span></span> <i class='fa fa-building'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabuildingoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-building-o'></i>"><span></span> <i class='fa fa-building-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabullhorni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bullhorn'></i>"><span></span> <i class='fa fa-bullhorn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabullseyei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bullseye'></i>"><span></span> <i class='fa fa-bullseye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-bus'></i>"><span></span> <i class='fa fa-bus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fabuyselladsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-buysellads'></i>"><span></span> <i class='fa fa-buysellads'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facabi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cab'></i>"><span></span> <i class='fa fa-cab'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalculatori" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calculator'></i>"><span></span> <i class='fa fa-calculator'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendari" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar'></i>"><span></span> <i class='fa fa-calendar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendarcheckoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar-check-o'></i>"><span></span> <i class='fa fa-calendar-check-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendarminusoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar-minus-o'></i>"><span></span> <i class='fa fa-calendar-minus-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendarplusoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar-plus-o'></i>"><span></span> <i class='fa fa-calendar-plus-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendartimesoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar-times-o'></i>"><span></span> <i class='fa fa-calendar-times-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facalendaroi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-calendar-o'></i>"><span></span> <i class='fa fa-calendar-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facamerai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-camera'></i>"><span></span> <i class='fa fa-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facameraretroi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-camera-retro'></i>"><span></span> <i class='fa fa-camera-retro'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facari" name="modules_icon" class="modules_icon"  value="<i class='fa fa-car'></i>"><span></span> <i class='fa fa-car'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretdowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-down'></i>"><span></span> <i class='fa fa-caret-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretlefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-left'></i>"><span></span> <i class='fa fa-caret-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-right'></i>"><span></span> <i class='fa fa-caret-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-up'></i>"><span></span> <i class='fa fa-caret-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretsquareodowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-square-o-down'></i>"><span></span> <i class='fa fa-caret-square-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretsquareolefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-square-o-left'></i>"><span></span> <i class='fa fa-caret-square-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretsquareorighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-square-o-right'></i>"><span></span> <i class='fa fa-caret-square-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facaretsquareoupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-caret-square-o-up'></i>"><span></span> <i class='fa fa-caret-square-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facartarrowdowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cart-arrow-down'></i>"><span></span> <i class='fa fa-cart-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facartplusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cart-plus'></i>"><span></span> <i class='fa fa-cart-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc'></i>"><span></span> <i class='fa fa-cc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccamexi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-amex'></i>"><span></span> <i class='fa fa-cc-amex'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccdinersclubi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-diners-club'></i>"><span></span> <i class='fa fa-cc-diners-club'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccdiscoveri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-discover'></i>"><span></span> <i class='fa fa-cc-discover'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccjcbi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-jcb'></i>"><span></span> <i class='fa fa-cc-jcb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccmastercardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-mastercard'></i>"><span></span> <i class='fa fa-cc-mastercard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccpaypali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-paypal'></i>"><span></span> <i class='fa fa-cc-paypal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccstripei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-stripe'></i>"><span></span> <i class='fa fa-cc-stripe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faccvisai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cc-visa'></i>"><span></span> <i class='fa fa-cc-visa'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facertificatei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-certificate'></i>"><span></span> <i class='fa fa-certificate'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachecki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-check'></i>"><span></span> <i class='fa fa-check'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facheckcirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-check-circle'></i>"><span></span> <i class='fa fa-check-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facheckcircleoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-check-circle-o'></i>"><span></span> <i class='fa fa-check-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachecksquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-check-square'></i>"><span></span> <i class='fa fa-check-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachecksquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-check-square-o'></i>"><span></span> <i class='fa fa-check-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevroncircledowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-circle-down'></i>"><span></span> <i class='fa fa-chevron-circle-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevroncirclelefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-circle-left'></i>"><span></span> <i class='fa fa-chevron-circle-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevroncirclerighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-circle-right'></i>"><span></span> <i class='fa fa-chevron-circle-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevroncircleupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-circle-up'></i>"><span></span> <i class='fa fa-chevron-circle-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevrondowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-down'></i>"><span></span> <i class='fa fa-chevron-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevronlefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-left'></i>"><span></span> <i class='fa fa-chevron-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevronrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-right'></i>"><span></span> <i class='fa fa-chevron-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachevronupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chevron-up'></i>"><span></span> <i class='fa fa-chevron-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachildi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-child'></i>"><span></span> <i class='fa fa-child'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fachromei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-chrome'></i>"><span></span> <i class='fa fa-chrome'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclonei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-clone'></i>"><span></span> <i class='fa fa-clone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-circle'></i>"><span></span> <i class='fa fa-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facircleoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-circle-o'></i>"><span></span> <i class='fa fa-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facircleonotchi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-circle-o-notch'></i>"><span></span> <i class='fa fa-circle-o-notch'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facirclethini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-circle-thin'></i>"><span></span> <i class='fa fa-circle-thin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclipboardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-clipboard'></i>"><span></span> <i class='fa fa-clipboard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclockoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-clock-o'></i>"><span></span> <i class='fa fa-clock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclosei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-close'></i>"><span></span> <i class='fa fa-close'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facloudi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cloud'></i>"><span></span> <i class='fa fa-cloud'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclouddownloadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cloud-download'></i>"><span></span> <i class='fa fa-cloud-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faclouduploadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cloud-upload'></i>"><span></span> <i class='fa fa-cloud-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facnyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cny'></i>"><span></span> <i class='fa fa-cny'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facodei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-code'></i>"><span></span> <i class='fa fa-code'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facodeforki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-code-fork'></i>"><span></span> <i class='fa fa-code-fork'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facodepeni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-codepen'></i>"><span></span> <i class='fa fa-codepen'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facoffeei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-coffee'></i>"><span></span> <i class='fa fa-coffee'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facolumnsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-columns'></i>"><span></span> <i class='fa fa-columns'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommenti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-comment'></i>"><span></span> <i class='fa fa-comment'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommentoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-comment-o'></i>"><span></span> <i class='fa fa-comment-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommentsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-comments'></i>"><span></span> <i class='fa fa-comments'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommentsoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-comments-o'></i>"><span></span> <i class='fa fa-comments-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommentingi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-commenting'></i>"><span></span> <i class='fa fa-commenting'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facommentingoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-commenting-o'></i>"><span></span> <i class='fa fa-commenting-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facompassi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-compass'></i>"><span></span> <i class='fa fa-compass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facompressi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-compress'></i>"><span></span> <i class='fa fa-compress'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faconnectdevelopi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-connectdevelop'></i>"><span></span> <i class='fa fa-connectdevelop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facontaoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-contao'></i>"><span></span> <i class='fa fa-contao'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facopyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-copy'></i>"><span></span> <i class='fa fa-copy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facopyrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-copyright'></i>"><span></span> <i class='fa fa-copyright'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facreativecommonsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-creative-commons'></i>"><span></span> <i class='fa fa-creative-commons'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facreditcardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-credit-card'></i>"><span></span> <i class='fa fa-credit-card'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facropi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-crop'></i>"><span></span> <i class='fa fa-crop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facrosshairsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-crosshairs'></i>"><span></span> <i class='fa fa-crosshairs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facss3i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-css3'></i>"><span></span> <i class='fa fa-css3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facubei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cube'></i>"><span></span> <i class='fa fa-cube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facubesi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cubes'></i>"><span></span> <i class='fa fa-cubes'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facuti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cut'></i>"><span></span> <i class='fa fa-cut'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-facutleryi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-cutlery'></i>"><span></span> <i class='fa fa-cutlery'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadashboardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dashboard'></i>"><span></span> <i class='fa fa-dashboard'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadashcubei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dashcube'></i>"><span></span> <i class='fa fa-dashcube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadatabasei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-database'></i>"><span></span> <i class='fa fa-database'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadedenti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dedent'></i>"><span></span> <i class='fa fa-dedent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadeliciousi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-delicious'></i>"><span></span> <i class='fa fa-delicious'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadesktopi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-desktop'></i>"><span></span> <i class='fa fa-desktop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadeviantarti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-deviantart'></i>"><span></span> <i class='fa fa-deviantart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadiamondi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-diamond'></i>"><span></span> <i class='fa fa-diamond'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadiggi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-digg'></i>"><span></span> <i class='fa fa-digg'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadollari" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dollar'></i>"><span></span> <i class='fa fa-dollar'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadotcircleoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dot-circle-o'></i>"><span></span> <i class='fa fa-dot-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadownloadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-download'></i>"><span></span> <i class='fa fa-download'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadribbblei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dribbble'></i>"><span></span> <i class='fa fa-dribbble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadropboxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-dropbox'></i>"><span></span> <i class='fa fa-dropbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fadrupali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-drupal'></i>"><span></span> <i class='fa fa-drupal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faediti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-edit'></i>"><span></span> <i class='fa fa-edit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faejecti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-eject'></i>"><span></span> <i class='fa fa-eject'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faellipsishi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ellipsis-h'></i>"><span></span> <i class='fa fa-ellipsis-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faellipsisvi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ellipsis-v'></i>"><span></span> <i class='fa fa-ellipsis-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faempirei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-empire'></i>"><span></span> <i class='fa fa-empire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faenvelopei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-envelope'></i>"><span></span> <i class='fa fa-envelope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faenvelopeoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-envelope-o'></i>"><span></span> <i class='fa fa-envelope-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faenvelopesquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-envelope-square'></i>"><span></span> <i class='fa fa-envelope-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faeraseri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-eraser'></i>"><span></span> <i class='fa fa-eraser'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faeuroi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-euro'></i>"><span></span> <i class='fa fa-euro'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexchangei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-exchange'></i>"><span></span> <i class='fa fa-exchange'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexclamationi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-exclamation'></i>"><span></span> <i class='fa fa-exclamation'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexclamationcirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-exclamation-circle'></i>"><span></span> <i class='fa fa-exclamation-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexclamationtrianglei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-exclamation-triangle'></i>"><span></span> <i class='fa fa-exclamation-triangle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexpandi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-expand'></i>"><span></span> <i class='fa fa-expand'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexpeditedssli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-expeditedssl'></i>"><span></span> <i class='fa fa-expeditedssl'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexternallinki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-external-link'></i>"><span></span> <i class='fa fa-external-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faexternallinksquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-external-link-square'></i>"><span></span> <i class='fa fa-external-link-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faeyei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-eye'></i>"><span></span> <i class='fa fa-eye'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faeyeslashi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-eye-slash'></i>"><span></span> <i class='fa fa-eye-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faeyedropperi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-eyedropper'></i>"><span></span> <i class='fa fa-eyedropper'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafacebooki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-facebook'></i>"><span></span> <i class='fa fa-facebook'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafacebookofficiali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-facebook-official'></i>"><span></span> <i class='fa fa-facebook-official'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafacebooksquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-facebook-square'></i>"><span></span> <i class='fa fa-facebook-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafastbackwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fast-backward'></i>"><span></span> <i class='fa fa-fast-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafastforwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fast-forward'></i>"><span></span> <i class='fa fa-fast-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafaxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fax'></i>"><span></span> <i class='fa fa-fax'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafemalei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-female'></i>"><span></span> <i class='fa fa-female'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafighterjeti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fighter-jet'></i>"><span></span> <i class='fa fa-fighter-jet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file'></i>"><span></span> <i class='fa fa-file'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafileoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-o'></i>"><span></span> <i class='fa fa-file-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilearchiveoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-archive-o'></i>"><span></span> <i class='fa fa-file-archive-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafileaudiooi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-audio-o'></i>"><span></span> <i class='fa fa-file-audio-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilecodeoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-code-o'></i>"><span></span> <i class='fa fa-file-code-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafileexceloi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-excel-o'></i>"><span></span> <i class='fa fa-file-excel-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafileimageoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-image-o'></i>"><span></span> <i class='fa fa-file-image-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilemovieoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-movie-o'></i>"><span></span> <i class='fa fa-file-movie-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilepdfoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-pdf-o'></i>"><span></span> <i class='fa fa-file-pdf-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilephotooi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-photo-o'></i>"><span></span> <i class='fa fa-file-photo-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilepictureoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-picture-o'></i>"><span></span> <i class='fa fa-file-picture-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilepowerpointoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-powerpoint-o'></i>"><span></span> <i class='fa fa-file-powerpoint-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilesoundoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-sound-o'></i>"><span></span> <i class='fa fa-file-sound-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafiletexti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-text'></i>"><span></span> <i class='fa fa-file-text'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafiletextoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-text-o'></i>"><span></span> <i class='fa fa-file-text-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilevideooi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-video-o'></i>"><span></span> <i class='fa fa-file-video-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilewordoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-word-o'></i>"><span></span> <i class='fa fa-file-word-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilezipoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-file-zip-o'></i>"><span></span> <i class='fa fa-file-zip-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilesoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-files-o'></i>"><span></span> <i class='fa fa-files-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilmi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-film'></i>"><span></span> <i class='fa fa-film'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafilteri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-filter'></i>"><span></span> <i class='fa fa-filter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafirei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fire'></i>"><span></span> <i class='fa fa-fire'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafireextinguisheri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fire-extinguisher'></i>"><span></span> <i class='fa fa-fire-extinguisher'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafirefoxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-firefox'></i>"><span></span> <i class='fa fa-firefox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faflagi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-flag'></i>"><span></span> <i class='fa fa-flag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faflagcheckeredi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-flag-checkered'></i>"><span></span> <i class='fa fa-flag-checkered'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faflagoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-flag-o'></i>"><span></span> <i class='fa fa-flag-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faflaski" name="modules_icon" class="modules_icon"  value="<i class='fa fa-flask'></i>"><span></span> <i class='fa fa-flask'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faflickri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-flickr'></i>"><span></span> <i class='fa fa-flickr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafloppyoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-floppy-o'></i>"><span></span> <i class='fa fa-floppy-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafolderi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-folder'></i>"><span></span> <i class='fa fa-folder'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafolderoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-folder-o'></i>"><span></span> <i class='fa fa-folder-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafolderopeni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-folder-open'></i>"><span></span> <i class='fa fa-folder-open'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafolderopenoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-folder-open-o'></i>"><span></span> <i class='fa fa-folder-open-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafonti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-font'></i>"><span></span> <i class='fa fa-font'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafonticonsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fonticons'></i>"><span></span> <i class='fa fa-fonticons'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faforumbeei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-forumbee'></i>"><span></span> <i class='fa fa-forumbee'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faforwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-forward'></i>"><span></span> <i class='fa fa-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafoursquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-foursquare'></i>"><span></span> <i class='fa fa-foursquare'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafrownoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-frown-o'></i>"><span></span> <i class='fa fa-frown-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafutboloi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-futbol-o'></i>"><span></span> <i class='fa fa-futbol-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagamepadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gamepad'></i>"><span></span> <i class='fa fa-gamepad'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagaveli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gavel'></i>"><span></span> <i class='fa fa-gavel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagbpi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gbp'></i>"><span></span> <i class='fa fa-gbp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ge'></i>"><span></span> <i class='fa fa-ge'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fageari" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gear'></i>"><span></span> <i class='fa fa-gear'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagearsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gears'></i>"><span></span> <i class='fa fa-gears'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagetpocketi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-get-pocket'></i>"><span></span> <i class='fa fa-get-pocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faggi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gg'></i>"><span></span> <i class='fa fa-gg'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faggcirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gg-circle'></i>"><span></span> <i class='fa fa-gg-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagifti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gift'></i>"><span></span> <i class='fa fa-gift'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagiti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-git'></i>"><span></span> <i class='fa fa-git'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagitsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-git-square'></i>"><span></span> <i class='fa fa-git-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagithubi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-github'></i>"><span></span> <i class='fa fa-github'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagithubalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-github-alt'></i>"><span></span> <i class='fa fa-github-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagithubsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-github-square'></i>"><span></span> <i class='fa fa-github-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagittipi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gittip'></i>"><span></span> <i class='fa fa-gittip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faglassi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-glass'></i>"><span></span> <i class='fa fa-glass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faglobei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-globe'></i>"><span></span> <i class='fa fa-globe'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagooglei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-google'></i>"><span></span> <i class='fa fa-google'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagoogleplusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-google-plus'></i>"><span></span> <i class='fa fa-google-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagoogleplussquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-google-plus-square'></i>"><span></span> <i class='fa fa-google-plus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagooglewalleti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-google-wallet'></i>"><span></span> <i class='fa fa-google-wallet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagraduationcapi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-graduation-cap'></i>"><span></span> <i class='fa fa-graduation-cap'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagratipayi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-gratipay'></i>"><span></span> <i class='fa fa-gratipay'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fagroupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-group'></i>"><span></span> <i class='fa fa-group'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-h-square'></i>"><span></span> <i class='fa fa-h-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahackernewsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hacker-news'></i>"><span></span> <i class='fa fa-hacker-news'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandgraboi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-grab-o'></i>"><span></span> <i class='fa fa-hand-grab-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandlizardoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-lizard-o'></i>"><span></span> <i class='fa fa-hand-lizard-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandpaperoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-paper-o'></i>"><span></span> <i class='fa fa-hand-paper-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandpeaceoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-peace-o'></i>"><span></span> <i class='fa fa-hand-peace-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandpointeroi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-pointer-o'></i>"><span></span> <i class='fa fa-hand-pointer-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandrockoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-rock-o'></i>"><span></span> <i class='fa fa-hand-rock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandscissorsoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-scissors-o'></i>"><span></span> <i class='fa fa-hand-scissors-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandspockoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-spock-o'></i>"><span></span> <i class='fa fa-hand-spock-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandstopoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-stop-o'></i>"><span></span> <i class='fa fa-hand-stop-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandodowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-o-down'></i>"><span></span> <i class='fa fa-hand-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandolefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-o-left'></i>"><span></span> <i class='fa fa-hand-o-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandorighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-o-right'></i>"><span></span> <i class='fa fa-hand-o-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahandoupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hand-o-up'></i>"><span></span> <i class='fa fa-hand-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahddoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hdd-o'></i>"><span></span> <i class='fa fa-hdd-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faheaderi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-header'></i>"><span></span> <i class='fa fa-header'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faheadphonesi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-headphones'></i>"><span></span> <i class='fa fa-headphones'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahearti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-heart'></i>"><span></span> <i class='fa fa-heart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faheartoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-heart-o'></i>"><span></span> <i class='fa fa-heart-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faheartbeati" name="modules_icon" class="modules_icon"  value="<i class='fa fa-heartbeat'></i>"><span></span> <i class='fa fa-heartbeat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahistoryi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-history'></i>"><span></span> <i class='fa fa-history'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahomei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-home'></i>"><span></span> <i class='fa fa-home'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahospitaloi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hospital-o'></i>"><span></span> <i class='fa fa-hospital-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahoteli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hotel'></i>"><span></span> <i class='fa fa-hotel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglassi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass'></i>"><span></span> <i class='fa fa-hourglass'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglass1i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass-1'></i>"><span></span> <i class='fa fa-hourglass-1'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglass2i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass-2'></i>"><span></span> <i class='fa fa-hourglass-2'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglass3i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass-3'></i>"><span></span> <i class='fa fa-hourglass-3'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglassendi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass-end'></i>"><span></span> <i class='fa fa-hourglass-end'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahourglassoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-hourglass-o'></i>"><span></span> <i class='fa fa-hourglass-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahouzzi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-houzz'></i>"><span></span> <i class='fa fa-houzz'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fahtml5i" name="modules_icon" class="modules_icon"  value="<i class='fa fa-html5'></i>"><span></span> <i class='fa fa-html5'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faicursori" name="modules_icon" class="modules_icon"  value="<i class='fa fa-i-cursor'></i>"><span></span> <i class='fa fa-i-cursor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-failsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ils'></i>"><span></span> <i class='fa fa-ils'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faimagei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-image'></i>"><span></span> <i class='fa fa-image'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainboxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-inbox'></i>"><span></span> <i class='fa fa-inbox'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faindenti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-indent'></i>"><span></span> <i class='fa fa-indent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faindustryi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-industry'></i>"><span></span> <i class='fa fa-industry'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainfoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-info'></i>"><span></span> <i class='fa fa-info'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainfocirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-info-circle'></i>"><span></span> <i class='fa fa-info-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-inr'></i>"><span></span> <i class='fa fa-inr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainstagrami" name="modules_icon" class="modules_icon"  value="<i class='fa fa-instagram'></i>"><span></span> <i class='fa fa-instagram'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainstitutioni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-institution'></i>"><span></span> <i class='fa fa-institution'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fainternetexploreri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-internet-explorer'></i>"><span></span> <i class='fa fa-internet-explorer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faioxhosti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ioxhost'></i>"><span></span> <i class='fa fa-ioxhost'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faitalici" name="modules_icon" class="modules_icon"  value="<i class='fa fa-italic'></i>"><span></span> <i class='fa fa-italic'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fajoomlai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-joomla'></i>"><span></span> <i class='fa fa-joomla'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fajpyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-jpy'></i>"><span></span> <i class='fa fa-jpy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fajsfiddlei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-jsfiddle'></i>"><span></span> <i class='fa fa-jsfiddle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fakeyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-key'></i>"><span></span> <i class='fa fa-key'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fakeyboardoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-keyboard-o'></i>"><span></span> <i class='fa fa-keyboard-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fakrwi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-krw'></i>"><span></span> <i class='fa fa-krw'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falanguagei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-language'></i>"><span></span> <i class='fa fa-language'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falaptopi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-laptop'></i>"><span></span> <i class='fa fa-laptop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falegali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-legal'></i>"><span></span> <i class='fa fa-legal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falemonoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-lemon-o'></i>"><span></span> <i class='fa fa-lemon-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faleveldowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-level-down'></i>"><span></span> <i class='fa fa-level-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falevelupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-level-up'></i>"><span></span> <i class='fa fa-level-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faliferingi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-life-ring'></i>"><span></span> <i class='fa fa-life-ring'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falightbulboi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-lightbulb-o'></i>"><span></span> <i class='fa fa-lightbulb-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falinecharti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-line-chart'></i>"><span></span> <i class='fa fa-line-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falinki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-link'></i>"><span></span> <i class='fa fa-link'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falinkedini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-linkedin'></i>"><span></span> <i class='fa fa-linkedin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falinkedinsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-linkedin-square'></i>"><span></span> <i class='fa fa-linkedin-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falinuxi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-linux'></i>"><span></span> <i class='fa fa-linux'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falistalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-list-alt'></i>"><span></span> <i class='fa fa-list-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falistoli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-list-ol'></i>"><span></span> <i class='fa fa-list-ol'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falistuli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-list-ul'></i>"><span></span> <i class='fa fa-list-ul'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falocationarrowi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-location-arrow'></i>"><span></span> <i class='fa fa-location-arrow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falocki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-lock'></i>"><span></span> <i class='fa fa-lock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falongarrowdowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-long-arrow-down'></i>"><span></span> <i class='fa fa-long-arrow-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falongarrowlefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-long-arrow-left'></i>"><span></span> <i class='fa fa-long-arrow-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falongarrowrighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-long-arrow-right'></i>"><span></span> <i class='fa fa-long-arrow-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-falongarrowupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-long-arrow-up'></i>"><span></span> <i class='fa fa-long-arrow-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famagici" name="modules_icon" class="modules_icon"  value="<i class='fa fa-magic'></i>"><span></span> <i class='fa fa-magic'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famagneti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-magnet'></i>"><span></span> <i class='fa fa-magnet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famalei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-male'></i>"><span></span> <i class='fa fa-male'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famapi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-map'></i>"><span></span> <i class='fa fa-map'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famapoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-map-o'></i>"><span></span> <i class='fa fa-map-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famapmarkeri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-map-marker'></i>"><span></span> <i class='fa fa-map-marker'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famappini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-map-pin'></i>"><span></span> <i class='fa fa-map-pin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famapsignsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-map-signs'></i>"><span></span> <i class='fa fa-map-signs'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famarsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mars'></i>"><span></span> <i class='fa fa-mars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famarsdoublei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mars-double'></i>"><span></span> <i class='fa fa-mars-double'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famarsstrokei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mars-stroke'></i>"><span></span> <i class='fa fa-mars-stroke'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famarsstrokehi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mars-stroke-h'></i>"><span></span> <i class='fa fa-mars-stroke-h'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famarsstrokevi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mars-stroke-v'></i>"><span></span> <i class='fa fa-mars-stroke-v'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famaxcdni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-maxcdn'></i>"><span></span> <i class='fa fa-maxcdn'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fameanpathi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-meanpath'></i>"><span></span> <i class='fa fa-meanpath'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famediumi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-medium'></i>"><span></span> <i class='fa fa-medium'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famedkiti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-medkit'></i>"><span></span> <i class='fa fa-medkit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famehoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-meh-o'></i>"><span></span> <i class='fa fa-meh-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famercuryi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mercury'></i>"><span></span> <i class='fa fa-mercury'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famicrophonei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-microphone'></i>"><span></span> <i class='fa fa-microphone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famicrophoneslashi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-microphone-slash'></i>"><span></span> <i class='fa fa-microphone-slash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faminusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-minus'></i>"><span></span> <i class='fa fa-minus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faminuscirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-minus-circle'></i>"><span></span> <i class='fa fa-minus-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faminussquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-minus-square'></i>"><span></span> <i class='fa fa-minus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faminussquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-minus-square-o'></i>"><span></span> <i class='fa fa-minus-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famobilei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mobile'></i>"><span></span> <i class='fa fa-mobile'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famoneyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-money'></i>"><span></span> <i class='fa fa-money'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famoonoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-moon-o'></i>"><span></span> <i class='fa fa-moon-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famortarboardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mortar-board'></i>"><span></span> <i class='fa fa-mortar-board'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famotorcyclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-motorcycle'></i>"><span></span> <i class='fa fa-motorcycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famousepointeri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-mouse-pointer'></i>"><span></span> <i class='fa fa-mouse-pointer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-famusici" name="modules_icon" class="modules_icon"  value="<i class='fa fa-music'></i>"><span></span> <i class='fa fa-music'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fanaviconi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-navicon'></i>"><span></span> <i class='fa fa-navicon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faneuteri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-neuter'></i>"><span></span> <i class='fa fa-neuter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fanewspaperoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-newspaper-o'></i>"><span></span> <i class='fa fa-newspaper-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faobjectgroupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-object-group'></i>"><span></span> <i class='fa fa-object-group'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faobjectungroupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-object-ungroup'></i>"><span></span> <i class='fa fa-object-ungroup'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faodnoklassnikii" name="modules_icon" class="modules_icon"  value="<i class='fa fa-odnoklassniki'></i>"><span></span> <i class='fa fa-odnoklassniki'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faodnoklassnikisquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-odnoklassniki-square'></i>"><span></span> <i class='fa fa-odnoklassniki-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faopencarti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-opencart'></i>"><span></span> <i class='fa fa-opencart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faopenidi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-openid'></i>"><span></span> <i class='fa fa-openid'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faoperai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-opera'></i>"><span></span> <i class='fa fa-opera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faoptinmonsteri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-optin-monster'></i>"><span></span> <i class='fa fa-optin-monster'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faoutdenti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-outdent'></i>"><span></span> <i class='fa fa-outdent'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapagelinesi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pagelines'></i>"><span></span> <i class='fa fa-pagelines'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapaintbrushi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paint-brush'></i>"><span></span> <i class='fa fa-paint-brush'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapaperclipi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paperclip'></i>"><span></span> <i class='fa fa-paperclip'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faparagraphi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paragraph'></i>"><span></span> <i class='fa fa-paragraph'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapastei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paste'></i>"><span></span> <i class='fa fa-paste'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapausei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pause'></i>"><span></span> <i class='fa fa-pause'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapawi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paw'></i>"><span></span> <i class='fa fa-paw'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapaypali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-paypal'></i>"><span></span> <i class='fa fa-paypal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapencili" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pencil'></i>"><span></span> <i class='fa fa-pencil'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapencilsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pencil-square'></i>"><span></span> <i class='fa fa-pencil-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapencilsquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pencil-square-o'></i>"><span></span> <i class='fa fa-pencil-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faphonei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-phone'></i>"><span></span> <i class='fa fa-phone'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faphonesquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-phone-square'></i>"><span></span> <i class='fa fa-phone-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faphotoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-photo'></i>"><span></span> <i class='fa fa-photo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapictureoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-picture-o'></i>"><span></span> <i class='fa fa-picture-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapiecharti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pie-chart'></i>"><span></span> <i class='fa fa-pie-chart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapiedpiperi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pied-piper'></i>"><span></span> <i class='fa fa-pied-piper'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapiedpiperalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pied-piper-alt'></i>"><span></span> <i class='fa fa-pied-piper-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapinteresti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pinterest'></i>"><span></span> <i class='fa fa-pinterest'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapinterestpi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pinterest-p'></i>"><span></span> <i class='fa fa-pinterest-p'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapinterestsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-pinterest-square'></i>"><span></span> <i class='fa fa-pinterest-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplanei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plane'></i>"><span></span> <i class='fa fa-plane'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplayi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-play'></i>"><span></span> <i class='fa fa-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplaycirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-play-circle'></i>"><span></span> <i class='fa fa-play-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplaycircleoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-play-circle-o'></i>"><span></span> <i class='fa fa-play-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplugi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plug'></i>"><span></span> <i class='fa fa-plug'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plus'></i>"><span></span> <i class='fa fa-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapluscirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plus-circle'></i>"><span></span> <i class='fa fa-plus-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplussquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plus-square'></i>"><span></span> <i class='fa fa-plus-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faplussquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-plus-square-o'></i>"><span></span> <i class='fa fa-plus-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapoweroffi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-power-off'></i>"><span></span> <i class='fa fa-power-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faprinti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-print'></i>"><span></span> <i class='fa fa-print'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fapuzzlepiecei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-puzzle-piece'></i>"><span></span> <i class='fa fa-puzzle-piece'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faqqi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-qq'></i>"><span></span> <i class='fa fa-qq'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faqrcodei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-qrcode'></i>"><span></span> <i class='fa fa-qrcode'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faquestioni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-question'></i>"><span></span> <i class='fa fa-question'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faquestioncirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-question-circle'></i>"><span></span> <i class='fa fa-question-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faquotelefti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-quote-left'></i>"><span></span> <i class='fa fa-quote-left'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faquoterighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-quote-right'></i>"><span></span> <i class='fa fa-quote-right'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farandomi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-random'></i>"><span></span> <i class='fa fa-random'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farebeli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-rebel'></i>"><span></span> <i class='fa fa-rebel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farecyclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-recycle'></i>"><span></span> <i class='fa fa-recycle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faredditi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-reddit'></i>"><span></span> <i class='fa fa-reddit'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faredditsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-reddit-square'></i>"><span></span> <i class='fa fa-reddit-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farefreshi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-refresh'></i>"><span></span> <i class='fa fa-refresh'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faregisteredi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-registered'></i>"><span></span> <i class='fa fa-registered'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faremovei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-remove'></i>"><span></span> <i class='fa fa-remove'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farenreni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-renren'></i>"><span></span> <i class='fa fa-renren'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farepeati" name="modules_icon" class="modules_icon"  value="<i class='fa fa-repeat'></i>"><span></span> <i class='fa fa-repeat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fareplyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-reply'></i>"><span></span> <i class='fa fa-reply'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fareplyalli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-reply-all'></i>"><span></span> <i class='fa fa-reply-all'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faretweeti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-retweet'></i>"><span></span> <i class='fa fa-retweet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farmbi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-rmb'></i>"><span></span> <i class='fa fa-rmb'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faroadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-road'></i>"><span></span> <i class='fa fa-road'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-farocketi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-rocket'></i>"><span></span> <i class='fa fa-rocket'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faroublei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-rouble'></i>"><span></span> <i class='fa fa-rouble'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasafarii" name="modules_icon" class="modules_icon"  value="<i class='fa fa-safari'></i>"><span></span> <i class='fa fa-safari'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasavei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-save'></i>"><span></span> <i class='fa fa-save'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fascissorsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-scissors'></i>"><span></span> <i class='fa fa-scissors'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasearchi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-search'></i>"><span></span> <i class='fa fa-search'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasearchminusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-search-minus'></i>"><span></span> <i class='fa fa-search-minus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasearchplusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-search-plus'></i>"><span></span> <i class='fa fa-search-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasellsyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sellsy'></i>"><span></span> <i class='fa fa-sellsy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasendi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-send'></i>"><span></span> <i class='fa fa-send'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasendoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-send-o'></i>"><span></span> <i class='fa fa-send-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faserveri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-server'></i>"><span></span> <i class='fa fa-server'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasharei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-share'></i>"><span></span> <i class='fa fa-share'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasharesquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-share-square'></i>"><span></span> <i class='fa fa-share-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasharesquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-share-square-o'></i>"><span></span> <i class='fa fa-share-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasharealti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-share-alt'></i>"><span></span> <i class='fa fa-share-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasharealtsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-share-alt-square'></i>"><span></span> <i class='fa fa-share-alt-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fashekeli" name="modules_icon" class="modules_icon"  value="<i class='fa fa-shekel'></i>"><span></span> <i class='fa fa-shekel'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fashieldi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-shield'></i>"><span></span> <i class='fa fa-shield'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fashipi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-ship'></i>"><span></span> <i class='fa fa-ship'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fashirtsinbulki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-shirtsinbulk'></i>"><span></span> <i class='fa fa-shirtsinbulk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fashoppingcarti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-shopping-cart'></i>"><span></span> <i class='fa fa-shopping-cart'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasignini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sign-in'></i>"><span></span> <i class='fa fa-sign-in'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasignouti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sign-out'></i>"><span></span> <i class='fa fa-sign-out'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasignali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-signal'></i>"><span></span> <i class='fa fa-signal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasimplybuilti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-simplybuilt'></i>"><span></span> <i class='fa fa-simplybuilt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasitemapi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sitemap'></i>"><span></span> <i class='fa fa-sitemap'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faskyatlasi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-skyatlas'></i>"><span></span> <i class='fa fa-skyatlas'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faskypei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-skype'></i>"><span></span> <i class='fa fa-skype'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faslacki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-slack'></i>"><span></span> <i class='fa fa-slack'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faslidersi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sliders'></i>"><span></span> <i class='fa fa-sliders'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faslidesharei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-slideshare'></i>"><span></span> <i class='fa fa-slideshare'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasmileoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-smile-o'></i>"><span></span> <i class='fa fa-smile-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasoccerballoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-soccer-ball-o'></i>"><span></span> <i class='fa fa-soccer-ball-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasorti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort'></i>"><span></span> <i class='fa fa-sort'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortalphaasci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-alpha-asc'></i>"><span></span> <i class='fa fa-sort-alpha-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortalphadesci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-alpha-desc'></i>"><span></span> <i class='fa fa-sort-alpha-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortamountasci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-amount-asc'></i>"><span></span> <i class='fa fa-sort-amount-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortamountdesci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-amount-desc'></i>"><span></span> <i class='fa fa-sort-amount-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortnumericasci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-numeric-asc'></i>"><span></span> <i class='fa fa-sort-numeric-asc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasortnumericdesci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sort-numeric-desc'></i>"><span></span> <i class='fa fa-sort-numeric-desc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasoundcloudi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-soundcloud'></i>"><span></span> <i class='fa fa-soundcloud'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faspaceshuttlei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-space-shuttle'></i>"><span></span> <i class='fa fa-space-shuttle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faspinneri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-spinner'></i>"><span></span> <i class='fa fa-spinner'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faspooni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-spoon'></i>"><span></span> <i class='fa fa-spoon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faspotifyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-spotify'></i>"><span></span> <i class='fa fa-spotify'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-square'></i>"><span></span> <i class='fa fa-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasquareoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-square-o'></i>"><span></span> <i class='fa fa-square-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastackexchangei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stack-exchange'></i>"><span></span> <i class='fa fa-stack-exchange'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastackoverflowi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stack-overflow'></i>"><span></span> <i class='fa fa-stack-overflow'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastari" name="modules_icon" class="modules_icon"  value="<i class='fa fa-star'></i>"><span></span> <i class='fa fa-star'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastarhalfi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-star-half'></i>"><span></span> <i class='fa fa-star-half'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastarhalfoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-star-half-o'></i>"><span></span> <i class='fa fa-star-half-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastaroi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-star-o'></i>"><span></span> <i class='fa fa-star-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasteami" name="modules_icon" class="modules_icon"  value="<i class='fa fa-steam'></i>"><span></span> <i class='fa fa-steam'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasteamsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-steam-square'></i>"><span></span> <i class='fa fa-steam-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastepbackwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-step-backward'></i>"><span></span> <i class='fa fa-step-backward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastepforwardi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-step-forward'></i>"><span></span> <i class='fa fa-step-forward'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastethoscopei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stethoscope'></i>"><span></span> <i class='fa fa-stethoscope'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastickynotei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sticky-note'></i>"><span></span> <i class='fa fa-sticky-note'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastickynoteoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sticky-note-o'></i>"><span></span> <i class='fa fa-sticky-note-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastopi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stop'></i>"><span></span> <i class='fa fa-stop'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastreetviewi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-street-view'></i>"><span></span> <i class='fa fa-street-view'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastrikethroughi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-strikethrough'></i>"><span></span> <i class='fa fa-strikethrough'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastumbleuponi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stumbleupon'></i>"><span></span> <i class='fa fa-stumbleupon'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fastumbleuponcirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-stumbleupon-circle'></i>"><span></span> <i class='fa fa-stumbleupon-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasubscripti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-subscript'></i>"><span></span> <i class='fa fa-subscript'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasubwayi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-subway'></i>"><span></span> <i class='fa fa-subway'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasuitcasei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-suitcase'></i>"><span></span> <i class='fa fa-suitcase'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasunoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-sun-o'></i>"><span></span> <i class='fa fa-sun-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fasuperscripti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-superscript'></i>"><span></span> <i class='fa fa-superscript'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatablei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-table'></i>"><span></span> <i class='fa fa-table'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatableti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tablet'></i>"><span></span> <i class='fa fa-tablet'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatachometeri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tachometer'></i>"><span></span> <i class='fa fa-tachometer'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatagi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tag'></i>"><span></span> <i class='fa fa-tag'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatagsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tags'></i>"><span></span> <i class='fa fa-tags'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatasksi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tasks'></i>"><span></span> <i class='fa fa-tasks'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fataxii" name="modules_icon" class="modules_icon"  value="<i class='fa fa-taxi'></i>"><span></span> <i class='fa fa-taxi'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatelevisioni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-television'></i>"><span></span> <i class='fa fa-television'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatencentweiboi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tencent-weibo'></i>"><span></span> <i class='fa fa-tencent-weibo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faterminali" name="modules_icon" class="modules_icon"  value="<i class='fa fa-terminal'></i>"><span></span> <i class='fa fa-terminal'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatextheighti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-text-height'></i>"><span></span> <i class='fa fa-text-height'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatextwidthi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-text-width'></i>"><span></span> <i class='fa fa-text-width'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-th'></i>"><span></span> <i class='fa fa-th'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathlargei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-th-large'></i>"><span></span> <i class='fa fa-th-large'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathumbtacki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-thumb-tack'></i>"><span></span> <i class='fa fa-thumb-tack'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathumbsdowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-thumbs-down'></i>"><span></span> <i class='fa fa-thumbs-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathumbsupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-thumbs-up'></i>"><span></span> <i class='fa fa-thumbs-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathumbsodowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-thumbs-o-down'></i>"><span></span> <i class='fa fa-thumbs-o-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fathumbsoupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-thumbs-o-up'></i>"><span></span> <i class='fa fa-thumbs-o-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatimesi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-times'></i>"><span></span> <i class='fa fa-times'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatimescirclei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-times-circle'></i>"><span></span> <i class='fa fa-times-circle'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatimescircleoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-times-circle-o'></i>"><span></span> <i class='fa fa-times-circle-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatinti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tint'></i>"><span></span> <i class='fa fa-tint'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatoggleoffi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-toggle-off'></i>"><span></span> <i class='fa fa-toggle-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatoggleoni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-toggle-on'></i>"><span></span> <i class='fa fa-toggle-on'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrademarki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-trademark'></i>"><span></span> <i class='fa fa-trademark'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatraini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-train'></i>"><span></span> <i class='fa fa-train'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatransgenderi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-transgender'></i>"><span></span> <i class='fa fa-transgender'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatransgenderalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-transgender-alt'></i>"><span></span> <i class='fa fa-transgender-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrashi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-trash'></i>"><span></span> <i class='fa fa-trash'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrashoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-trash-o'></i>"><span></span> <i class='fa fa-trash-o'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatreei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tree'></i>"><span></span> <i class='fa fa-tree'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrelloi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-trello'></i>"><span></span> <i class='fa fa-trello'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatripadvisori" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tripadvisor'></i>"><span></span> <i class='fa fa-tripadvisor'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrophyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-trophy'></i>"><span></span> <i class='fa fa-trophy'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatrucki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-truck'></i>"><span></span> <i class='fa fa-truck'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatryi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-try'></i>"><span></span> <i class='fa fa-try'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fattyi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tty'></i>"><span></span> <i class='fa fa-tty'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatumblri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tumblr'></i>"><span></span> <i class='fa fa-tumblr'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatumblrsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-tumblr-square'></i>"><span></span> <i class='fa fa-tumblr-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatwitchi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-twitch'></i>"><span></span> <i class='fa fa-twitch'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatwitteri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-twitter'></i>"><span></span> <i class='fa fa-twitter'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fatwittersquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-twitter-square'></i>"><span></span> <i class='fa fa-twitter-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favenusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-venus'></i>"><span></span> <i class='fa fa-venus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favenusdoublei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-venus-double'></i>"><span></span> <i class='fa fa-venus-double'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favenusmarsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-venus-mars'></i>"><span></span> <i class='fa fa-venus-mars'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faviacoini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-viacoin'></i>"><span></span> <i class='fa fa-viacoin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favideocamerai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-video-camera'></i>"><span></span> <i class='fa fa-video-camera'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favimeoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-vimeo'></i>"><span></span> <i class='fa fa-vimeo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favimeosquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-vimeo-square'></i>"><span></span> <i class='fa fa-vimeo-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favinei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-vine'></i>"><span></span> <i class='fa fa-vine'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-vk'></i>"><span></span> <i class='fa fa-vk'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favolumedowni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-volume-down'></i>"><span></span> <i class='fa fa-volume-down'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favolumeoffi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-volume-off'></i>"><span></span> <i class='fa fa-volume-off'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-favolumeupi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-volume-up'></i>"><span></span> <i class='fa fa-volume-up'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faumbrellai" name="modules_icon" class="modules_icon"  value="<i class='fa fa-umbrella'></i>"><span></span> <i class='fa fa-umbrella'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faunderlinei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-underline'></i>"><span></span> <i class='fa fa-underline'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faundoi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-undo'></i>"><span></span> <i class='fa fa-undo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fauniversityi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-university'></i>"><span></span> <i class='fa fa-university'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faunlinki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-unlink'></i>"><span></span> <i class='fa fa-unlink'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faunlocki" name="modules_icon" class="modules_icon"  value="<i class='fa fa-unlock'></i>"><span></span> <i class='fa fa-unlock'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faunlockalti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-unlock-alt'></i>"><span></span> <i class='fa fa-unlock-alt'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fauploadi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-upload'></i>"><span></span> <i class='fa fa-upload'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fauseri" name="modules_icon" class="modules_icon"  value="<i class='fa fa-user'></i>"><span></span> <i class='fa fa-user'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fausermdi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-user-md'></i>"><span></span> <i class='fa fa-user-md'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fauserplusi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-user-plus'></i>"><span></span> <i class='fa fa-user-plus'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fausersecreti" name="modules_icon" class="modules_icon"  value="<i class='fa fa-user-secret'></i>"><span></span> <i class='fa fa-user-secret'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fausertimesi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-user-times'></i>"><span></span> <i class='fa fa-user-times'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fausersi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-users'></i>"><span></span> <i class='fa fa-users'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawechati" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wechat'></i>"><span></span> <i class='fa fa-wechat'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faweiboi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-weibo'></i>"><span></span> <i class='fa fa-weibo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faweixini" name="modules_icon" class="modules_icon"  value="<i class='fa fa-weixin'></i>"><span></span> <i class='fa fa-weixin'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawhatsappi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-whatsapp'></i>"><span></span> <i class='fa fa-whatsapp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawheelchairi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wheelchair'></i>"><span></span> <i class='fa fa-wheelchair'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawifii" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wifi'></i>"><span></span> <i class='fa fa-wifi'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawikipediawi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wikipedia-w'></i>"><span></span> <i class='fa fa-wikipedia-w'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawindowsi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-windows'></i>"><span></span> <i class='fa fa-windows'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawoni" name="modules_icon" class="modules_icon"  value="<i class='fa fa-won'></i>"><span></span> <i class='fa fa-won'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawordpressi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wordpress'></i>"><span></span> <i class='fa fa-wordpress'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fawrenchi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-wrench'></i>"><span></span> <i class='fa fa-wrench'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faxingi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-xing'></i>"><span></span> <i class='fa fa-xing'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faxingsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-xing-square'></i>"><span></span> <i class='fa fa-xing-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fayahooi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-yahoo'></i>"><span></span> <i class='fa fa-yahoo'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fafayci" name="modules_icon" class="modules_icon"  value="<i class='fa fa-fa-yc'></i>"><span></span> <i class='fa fa-fa-yc'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-faycsquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-yc-square'></i>"><span></span> <i class='fa fa-yc-square'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fayelpi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-yelp'></i>"><span></span> <i class='fa fa-yelp'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fayoutubei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-youtube'></i>"><span></span> <i class='fa fa-youtube'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fayoutubeplayi" name="modules_icon" class="modules_icon"  value="<i class='fa fa-youtube-play'></i>"><span></span> <i class='fa fa-youtube-play'></i>
																</label>
																<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																<input type="radio" id="modules_icon_i-classfa-fayoutubesquarei" name="modules_icon" class="modules_icon"  value="<i class='fa fa-youtube-square'></i>"><span></span> <i class='fa fa-youtube-square'></i>
																</label>
																<div id="modules_icon-check-loading"><?php echo translate("Loading"); ?>...</div>
																<div class="help-block"><?php echo translate("Make menu of module friendly with icon in backend mode"); ?></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-settings push-5-r"></i> <?php echo translate("Configurations"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_link"><?php echo translate("Backend Page"); ?><span class="text-danger">*</span> </label>
												<div class="col-md-5">
													<input class="form-control modules_link" type="text" id="modules_link" name="modules_link" value="" placeholder="<?php echo translate("File of module");?>" >

													<div class="help-block"><?php echo translate("Location of module in backend mode"); ?></div>
												</div>
												<div class="col-md-5">
													<a href="plugins/filemanager/dialog-backend.php?type=2&amp;field_id=modules_link&amp;relative_url=1" id="modules_link-fileselector-btn" class="btn" style="padding: 0">
													<i class="si si-folder-alt fa-2x"></i> <?php echo translate("Select"); ?>
													</a>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_function_link"><?php echo translate("Function Link"); ?></label>
												<div class="col-md-5">
													<input class="form-control modules_function_link" type="text" id="modules_function_link" name="modules_function_link" value="" placeholder="<?php echo translate("Function link of module");?>" >

													<div class="help-block"><?php echo translate("Link for function file function file of module"); ?></div>
												</div>
												<div class="col-md-5">
													<a href="plugins/filemanager/dialog-system.php?type=2&amp;field_id=modules_function_link&amp;relative_url=1" id="modules_function_link-fileselector-btn" class="btn" style="padding: 0">
													<i class="si si-folder-alt fa-2x"></i> <?php echo translate("Select"); ?>
													</a>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_key"><?php echo translate("Key"); ?><span class="text-danger">*</span> </label>
												<div class="col-md-12">
													<input class="form-control modules_key" type="text" id="modules_key" name="modules_key" value="" placeholder="<?php echo translate("Key of module");?>" >

													<div class="help-block"><?php echo translate("Unique phrase which will be used as prefix for files of module in backend mode"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_db_name"><?php echo translate("Database Table Name"); ?><span class="text-danger">*</span> </label>
												<div class="col-md-12">
													<input class="form-control modules_db_name" type="text" id="modules_db_name" name="modules_db_name" value="" placeholder="<?php echo translate("Database table name of module");?>" >

													<div class="help-block"><?php echo translate("Unigue table name in database of module "); ?></div>
												</div>
											</div>
										</div>

										<?php
										$modules_protected_default_value = unserialize('a:1:{i:0;i:1;}');
										?>
										<script>
										/* JavaScript variable for dynamic default value "modules_protected" */
										var modules_protected_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_protected_default_value)); ?>");
										</script>
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_protected"><?php echo translate("Protected"); ?></label>
												<div class="col-md-12">
													<label class="css-input switch switch-lg switch-success">
														<input id="modules_protected" name="modules_protected" class="modules_protected" type="checkbox" value="<?php echo $modules_protected_default_value[0];?>" ><span></span> <?php echo translate(""); ?>
													</label>
													<div class="help-block"><?php echo translate("Protect module from access in backend mode when user is not Super Administrator"); ?></div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
				  	</div>

					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-list push-5-r"></i> <?php echo translate("Backend Menu"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-12" for="modules_category"><?php echo translate("Category"); ?></label>
											<div class="col-md-12">
												<input class="form-control modules_category" type="text" id="modules_category" name="modules_category" value="" placeholder="<?php echo translate("Category of module");?>" >

												<div class="help-block"><?php echo translate("Categorize module to make it more easier to find in side menu in backend mode"); ?></div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-12" for="modules_subject"><?php echo translate("Subject"); ?></label>
											<div class="col-md-12">
												<input class="form-control modules_subject" type="text" id="modules_subject" name="modules_subject" value="" placeholder="<?php echo translate("Subject of module");?>" >

												<div class="help-block"><?php echo translate("Put menu of module inside a subject menu (Parent menu) in backend mode"); ?></div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12">
												<div id="subject_icon_list_parent" class="panel-group">
													<div class="panel panel-default">
														<a class="accordion-toggle" data-toggle="collapse" data-parent="#subject_icon_list_parent" href="#subject_icon_list">
															<div class="panel-heading">
																<span class="panel-title"><?php echo translate("Subject Icon"); ?></span> <i class="fa fa-angle-down"></i>
															</div>
														</a>

														<div id="subject_icon_list" class="panel-collapse collapse">
															<div class="row">
																<div class="col-md-12">

																	<div class="col-md-12">
																		<div class="form-group">
																			<div class="col-md-12">

																					<div class="row items-push">
																						<div class="col-md-12" style="margin-bottom: 0;">
																							<label class="css-input css-radio css-radio-default push-10-r">
																								<input type="radio" id="modules_subject_icon_" name="modules_subject_icon" class="modules_subject_icon" checked  value=""><span></span> <?php echo translate("None"); ?>
																							</label>
																						</div>
																					</div>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siactionredoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-action-redo'></i>"><span></span> <i class='si si-action-redo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siactionundoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-action-undo'></i>"><span></span> <i class='si si-action-undo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siarrowdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-arrow-down'></i>"><span></span> <i class='si si-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siarrowlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-arrow-left'></i>"><span></span> <i class='si si-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siarrowrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-arrow-right'></i>"><span></span> <i class='si si-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siarrowupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-arrow-up'></i>"><span></span> <i class='si si-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sianchori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-anchor'></i>"><span></span> <i class='si si-anchor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibadgei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-badge'></i>"><span></span> <i class='si si-badge'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bag'></i>"><span></span> <i class='si si-bag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibani" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-ban'></i>"><span></span> <i class='si si-ban'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibarcharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bar-chart'></i>"><span></span> <i class='si si-bar-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibasketi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-basket'></i>"><span></span> <i class='si si-basket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibasketloadedi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-basket-loaded'></i>"><span></span> <i class='si si-basket-loaded'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibelli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bell'></i>"><span></span> <i class='si si-bell'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibookopeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-book-open'></i>"><span></span> <i class='si si-book-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibriefcasei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-briefcase'></i>"><span></span> <i class='si si-briefcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibubblei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bubble'></i>"><span></span> <i class='si si-bubble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibubblesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bubbles'></i>"><span></span> <i class='si si-bubbles'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sibulbi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-bulb'></i>"><span></span> <i class='si si-bulb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicalculatori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-calculator'></i>"><span></span> <i class='si si-calculator'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicalendari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-calendar'></i>"><span></span> <i class='si si-calendar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicallendi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-call-end'></i>"><span></span> <i class='si si-call-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicallini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-call-in'></i>"><span></span> <i class='si si-call-in'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicallouti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-call-out'></i>"><span></span> <i class='si si-call-out'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicamcorderi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-camcorder'></i>"><span></span> <i class='si si-camcorder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicamerai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-camera'></i>"><span></span> <i class='si si-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sichecki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-check'></i>"><span></span> <i class='si si-check'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sichemistryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-chemistry'></i>"><span></span> <i class='si si-chemistry'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siclocki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-clock'></i>"><span></span> <i class='si si-clock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siclouddownloadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-cloud-download'></i>"><span></span> <i class='si si-cloud-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siclouduploadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-cloud-upload'></i>"><span></span> <i class='si si-cloud-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicompassi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-compass'></i>"><span></span> <i class='si si-compass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolendi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-end'></i>"><span></span> <i class='si si-control-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolforwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-forward'></i>"><span></span> <i class='si si-control-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolplayi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-play'></i>"><span></span> <i class='si si-control-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolpausei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-pause'></i>"><span></span> <i class='si si-control-pause'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolrewindi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-rewind'></i>"><span></span> <i class='si si-control-rewind'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicontrolstarti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-control-start'></i>"><span></span> <i class='si si-control-start'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicreditcardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-credit-card'></i>"><span></span> <i class='si si-credit-card'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicropi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-crop'></i>"><span></span> <i class='si si-crop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-cup'></i>"><span></span> <i class='si si-cup'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicursori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-cursor'></i>"><span></span> <i class='si si-cursor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sicursormovei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-cursor-move'></i>"><span></span> <i class='si si-cursor-move'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidiamondi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-diamond'></i>"><span></span> <i class='si si-diamond'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidirectioni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-direction'></i>"><span></span> <i class='si si-direction'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidirectionsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-directions'></i>"><span></span> <i class='si si-directions'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidisci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-disc'></i>"><span></span> <i class='si si-disc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidislikei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-dislike'></i>"><span></span> <i class='si si-dislike'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidoci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-doc'></i>"><span></span> <i class='si si-doc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidocsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-docs'></i>"><span></span> <i class='si si-docs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidraweri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-drawer'></i>"><span></span> <i class='si si-drawer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sidropi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-drop'></i>"><span></span> <i class='si si-drop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siearphonesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-earphones'></i>"><span></span> <i class='si si-earphones'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siearphonesalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-earphones-alt'></i>"><span></span> <i class='si si-earphones-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siemoticonsmilei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-emoticon-smile'></i>"><span></span> <i class='si si-emoticon-smile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sienergyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-energy'></i>"><span></span> <i class='si si-energy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sienvelopei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-envelope'></i>"><span></span> <i class='si si-envelope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sienvelopeletteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-envelope-letter'></i>"><span></span> <i class='si si-envelope-letter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sienvelopeopeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-envelope-open'></i>"><span></span> <i class='si si-envelope-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siequalizeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-equalizer'></i>"><span></span> <i class='si si-equalizer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sieyei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-eye'></i>"><span></span> <i class='si si-eye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sieyeglassesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-eyeglasses'></i>"><span></span> <i class='si si-eyeglasses'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sifeedi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-feed'></i>"><span></span> <i class='si si-feed'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sifilmi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-film'></i>"><span></span> <i class='si si-film'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sifirei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-fire'></i>"><span></span> <i class='si si-fire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siflagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-flag'></i>"><span></span> <i class='si si-flag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sifolderi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-folder'></i>"><span></span> <i class='si si-folder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sifolderalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-folder-alt'></i>"><span></span> <i class='si si-folder-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siframei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-frame'></i>"><span></span> <i class='si si-frame'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sigamecontrolleri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-game-controller'></i>"><span></span> <i class='si si-game-controller'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sighosti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-ghost'></i>"><span></span> <i class='si si-ghost'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siglobei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-globe'></i>"><span></span> <i class='si si-globe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siglobealti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-globe-alt'></i>"><span></span> <i class='si si-globe-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sigraduationi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-graduation'></i>"><span></span> <i class='si si-graduation'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sigraphi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-graph'></i>"><span></span> <i class='si si-graph'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sigridi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-grid'></i>"><span></span> <i class='si si-grid'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sihandbagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-handbag'></i>"><span></span> <i class='si si-handbag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sihearti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-heart'></i>"><span></span> <i class='si si-heart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sihomei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-home'></i>"><span></span> <i class='si si-home'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sihourglassi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-hourglass'></i>"><span></span> <i class='si si-hourglass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siinfoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-info'></i>"><span></span> <i class='si si-info'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sikeyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-key'></i>"><span></span> <i class='si si-key'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silayersi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-layers'></i>"><span></span> <i class='si si-layers'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silikei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-like'></i>"><span></span> <i class='si si-like'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silinki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-link'></i>"><span></span> <i class='si si-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silisti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-list'></i>"><span></span> <i class='si si-list'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silocki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-lock'></i>"><span></span> <i class='si si-lock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silockopeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-lock-open'></i>"><span></span> <i class='si si-lock-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silogini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-login'></i>"><span></span> <i class='si si-login'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-silogouti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-logout'></i>"><span></span> <i class='si si-logout'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siloopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-loop'></i>"><span></span> <i class='si si-loop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simagicwandi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-magic-wand'></i>"><span></span> <i class='si si-magic-wand'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simagneti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-magnet'></i>"><span></span> <i class='si si-magnet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simagnifieri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-magnifier'></i>"><span></span> <i class='si si-magnifier'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simagnifieraddi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-magnifier-add'></i>"><span></span> <i class='si si-magnifier-add'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simagnifierremovei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-magnifier-remove'></i>"><span></span> <i class='si si-magnifier-remove'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simapi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-map'></i>"><span></span> <i class='si si-map'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simicrophonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-microphone'></i>"><span></span> <i class='si si-microphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simousei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-mouse'></i>"><span></span> <i class='si si-mouse'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simoustachei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-moustache'></i>"><span></span> <i class='si si-moustache'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simusictonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-music-tone'></i>"><span></span> <i class='si si-music-tone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-simusictonealti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-music-tone-alt'></i>"><span></span> <i class='si si-music-tone-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sinotei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-note'></i>"><span></span> <i class='si si-note'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sinotebooki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-notebook'></i>"><span></span> <i class='si si-notebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipaperclipi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-paper-clip'></i>"><span></span> <i class='si si-paper-clip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipaperplanei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-paper-plane'></i>"><span></span> <i class='si si-paper-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipencili" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-pencil'></i>"><span></span> <i class='si si-pencil'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipicturei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-picture'></i>"><span></span> <i class='si si-picture'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipiecharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-pie-chart'></i>"><span></span> <i class='si si-pie-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-pin'></i>"><span></span> <i class='si si-pin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siplanei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-plane'></i>"><span></span> <i class='si si-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siplaylisti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-playlist'></i>"><span></span> <i class='si si-playlist'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-plus'></i>"><span></span> <i class='si si-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipointeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-pointer'></i>"><span></span> <i class='si si-pointer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipoweri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-power'></i>"><span></span> <i class='si si-power'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipresenti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-present'></i>"><span></span> <i class='si si-present'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siprinteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-printer'></i>"><span></span> <i class='si si-printer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sipuzzlei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-puzzle'></i>"><span></span> <i class='si si-puzzle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siquestioni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-question'></i>"><span></span> <i class='si si-question'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sirefreshi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-refresh'></i>"><span></span> <i class='si si-refresh'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sireloadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-reload'></i>"><span></span> <i class='si si-reload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sirocketi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-rocket'></i>"><span></span> <i class='si si-rocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siscreendesktopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-screen-desktop'></i>"><span></span> <i class='si si-screen-desktop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siscreensmartphonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-screen-smartphone'></i>"><span></span> <i class='si si-screen-smartphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siscreentableti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-screen-tablet'></i>"><span></span> <i class='si si-screen-tablet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisettingsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-settings'></i>"><span></span> <i class='si si-settings'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisharei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-share'></i>"><span></span> <i class='si si-share'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisharealti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-share-alt'></i>"><span></span> <i class='si si-share-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sishieldi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-shield'></i>"><span></span> <i class='si si-shield'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sishufflei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-shuffle'></i>"><span></span> <i class='si si-shuffle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisizeactuali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-size-actual'></i>"><span></span> <i class='si si-size-actual'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisizefullscreeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-size-fullscreen'></i>"><span></span> <i class='si si-size-fullscreen'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialdribbblei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-dribbble'></i>"><span></span> <i class='si si-social-dribbble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialdropboxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-dropbox'></i>"><span></span> <i class='si si-social-dropbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialfacebooki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-facebook'></i>"><span></span> <i class='si si-social-facebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialtumblri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-tumblr'></i>"><span></span> <i class='si si-social-tumblr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialtwitteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-twitter'></i>"><span></span> <i class='si si-social-twitter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisocialyoutubei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-social-youtube'></i>"><span></span> <i class='si si-social-youtube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sispeechi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-speech'></i>"><span></span> <i class='si si-speech'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sispeedometeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-speedometer'></i>"><span></span> <i class='si si-speedometer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sistari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-star'></i>"><span></span> <i class='si si-star'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisupporti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-support'></i>"><span></span> <i class='si si-support'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisymbolfemalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-symbol-female'></i>"><span></span> <i class='si si-symbol-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sisymbolmalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-symbol-male'></i>"><span></span> <i class='si si-symbol-male'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sitagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-tag'></i>"><span></span> <i class='si si-tag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sitargeti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-target'></i>"><span></span> <i class='si si-target'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sitrashi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-trash'></i>"><span></span> <i class='si si-trash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sitrophyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-trophy'></i>"><span></span> <i class='si si-trophy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siumbrellai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-umbrella'></i>"><span></span> <i class='si si-umbrella'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siuseri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-user'></i>"><span></span> <i class='si si-user'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siuserfemalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-user-female'></i>"><span></span> <i class='si si-user-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siuserfollowi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-user-follow'></i>"><span></span> <i class='si si-user-follow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siuserfollowingi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-user-following'></i>"><span></span> <i class='si si-user-following'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siuserunfollowi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-user-unfollow'></i>"><span></span> <i class='si si-user-unfollow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siusersi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-users'></i>"><span></span> <i class='si si-users'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sivectori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-vector'></i>"><span></span> <i class='si si-vector'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sivolume1i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-volume-1'></i>"><span></span> <i class='si si-volume-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sivolume2i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-volume-2'></i>"><span></span> <i class='si si-volume-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-sivolumeoffi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-volume-off'></i>"><span></span> <i class='si si-volume-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siwalleti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-wallet'></i>"><span></span> <i class='si si-wallet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classsi-siwrenchi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='si si-wrench'></i>"><span></span> <i class='si si-wrench'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fa500pxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-500px'></i>"><span></span> <i class='fa fa-500px'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faadjusti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-adjust'></i>"><span></span> <i class='fa fa-adjust'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faadni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-adn'></i>"><span></span> <i class='fa fa-adn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faaligncenteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-align-center'></i>"><span></span> <i class='fa fa-align-center'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faalignjustifyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-align-justify'></i>"><span></span> <i class='fa fa-align-justify'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faalignlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-align-left'></i>"><span></span> <i class='fa fa-align-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faalignrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-align-right'></i>"><span></span> <i class='fa fa-align-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faamazoni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-amazon'></i>"><span></span> <i class='fa fa-amazon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faambulancei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ambulance'></i>"><span></span> <i class='fa fa-ambulance'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faanchori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-anchor'></i>"><span></span> <i class='fa fa-anchor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faandroidi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-android'></i>"><span></span> <i class='fa fa-android'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangellisti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angellist'></i>"><span></span> <i class='fa fa-angellist'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangledoubledowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-double-down'></i>"><span></span> <i class='fa fa-angle-double-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangledoublelefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-double-left'></i>"><span></span> <i class='fa fa-angle-double-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangledoublerighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-double-right'></i>"><span></span> <i class='fa fa-angle-double-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangledoubleupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-double-up'></i>"><span></span> <i class='fa fa-angle-double-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangledowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-down'></i>"><span></span> <i class='fa fa-angle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faanglelefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-left'></i>"><span></span> <i class='fa fa-angle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faanglerighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-right'></i>"><span></span> <i class='fa fa-angle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faangleupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-angle-up'></i>"><span></span> <i class='fa fa-angle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faapplei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-apple'></i>"><span></span> <i class='fa fa-apple'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarchivei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-archive'></i>"><span></span> <i class='fa fa-archive'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faareacharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-area-chart'></i>"><span></span> <i class='fa fa-area-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircledowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-down'></i>"><span></span> <i class='fa fa-arrow-circle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcirclelefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-left'></i>"><span></span> <i class='fa fa-arrow-circle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcirclerighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-right'></i>"><span></span> <i class='fa fa-arrow-circle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircleupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-up'></i>"><span></span> <i class='fa fa-arrow-circle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircleodowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-down'></i>"><span></span> <i class='fa fa-arrow-circle-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircleolefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-left'></i>"><span></span> <i class='fa fa-arrow-circle-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircleorighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-right'></i>"><span></span> <i class='fa fa-arrow-circle-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowcircleoupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-circle-o-up'></i>"><span></span> <i class='fa fa-arrow-circle-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-down'></i>"><span></span> <i class='fa fa-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-left'></i>"><span></span> <i class='fa fa-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-right'></i>"><span></span> <i class='fa fa-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrow-up'></i>"><span></span> <i class='fa fa-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowsalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrows-alt'></i>"><span></span> <i class='fa fa-arrows-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowshi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrows-h'></i>"><span></span> <i class='fa fa-arrows-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowsvi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrows-v'></i>"><span></span> <i class='fa fa-arrows-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faarrowsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-arrows'></i>"><span></span> <i class='fa fa-arrows'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faasteriski" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-asterisk'></i>"><span></span> <i class='fa fa-asterisk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faati" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-at'></i>"><span></span> <i class='fa fa-at'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faautomobilei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-automobile'></i>"><span></span> <i class='fa fa-automobile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabackwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-backward'></i>"><span></span> <i class='fa fa-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabalancescalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-balance-scale'></i>"><span></span> <i class='fa fa-balance-scale'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabani" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ban'></i>"><span></span> <i class='fa fa-ban'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabanki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bank'></i>"><span></span> <i class='fa fa-bank'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabarcharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bar-chart'></i>"><span></span> <i class='fa fa-bar-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabarchartoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bar-chart-o'></i>"><span></span> <i class='fa fa-bar-chart-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabarcodei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-barcode'></i>"><span></span> <i class='fa fa-barcode'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabarsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bars'></i>"><span></span> <i class='fa fa-bars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabattery0i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-battery-0'></i>"><span></span> <i class='fa fa-battery-0'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabattery1i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-battery-1'></i>"><span></span> <i class='fa fa-battery-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabattery2i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-battery-2'></i>"><span></span> <i class='fa fa-battery-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabattery3i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-battery-3'></i>"><span></span> <i class='fa fa-battery-3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabattery4i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-battery-4'></i>"><span></span> <i class='fa fa-battery-4'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabedi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bed'></i>"><span></span> <i class='fa fa-bed'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabeeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-beer'></i>"><span></span> <i class='fa fa-beer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabehancei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-behance'></i>"><span></span> <i class='fa fa-behance'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabehancesquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-behance-square'></i>"><span></span> <i class='fa fa-behance-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabelli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bell'></i>"><span></span> <i class='fa fa-bell'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabelloi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bell-o'></i>"><span></span> <i class='fa fa-bell-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabellslashi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bell-slash'></i>"><span></span> <i class='fa fa-bell-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabellslashoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bell-slash-o'></i>"><span></span> <i class='fa fa-bell-slash-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabicyclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bicycle'></i>"><span></span> <i class='fa fa-bicycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabinocularsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-binoculars'></i>"><span></span> <i class='fa fa-binoculars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabirthdaycakei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-birthday-cake'></i>"><span></span> <i class='fa fa-birthday-cake'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabitbucketi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bitbucket'></i>"><span></span> <i class='fa fa-bitbucket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabitbucketsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bitbucket-square'></i>"><span></span> <i class='fa fa-bitbucket-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabitcoini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bitcoin'></i>"><span></span> <i class='fa fa-bitcoin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fablacktiei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-black-tie'></i>"><span></span> <i class='fa fa-black-tie'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faboldi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bold'></i>"><span></span> <i class='fa fa-bold'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabolti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bolt'></i>"><span></span> <i class='fa fa-bolt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabombi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bomb'></i>"><span></span> <i class='fa fa-bomb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabooki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-book'></i>"><span></span> <i class='fa fa-book'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabookmarki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bookmark'></i>"><span></span> <i class='fa fa-bookmark'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabookmarkoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bookmark-o'></i>"><span></span> <i class='fa fa-bookmark-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabriefcasei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-briefcase'></i>"><span></span> <i class='fa fa-briefcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabugi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bug'></i>"><span></span> <i class='fa fa-bug'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabuildingi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-building'></i>"><span></span> <i class='fa fa-building'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabuildingoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-building-o'></i>"><span></span> <i class='fa fa-building-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabullhorni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bullhorn'></i>"><span></span> <i class='fa fa-bullhorn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabullseyei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bullseye'></i>"><span></span> <i class='fa fa-bullseye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-bus'></i>"><span></span> <i class='fa fa-bus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fabuyselladsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-buysellads'></i>"><span></span> <i class='fa fa-buysellads'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facabi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cab'></i>"><span></span> <i class='fa fa-cab'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalculatori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calculator'></i>"><span></span> <i class='fa fa-calculator'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar'></i>"><span></span> <i class='fa fa-calendar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendarcheckoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar-check-o'></i>"><span></span> <i class='fa fa-calendar-check-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendarminusoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar-minus-o'></i>"><span></span> <i class='fa fa-calendar-minus-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendarplusoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar-plus-o'></i>"><span></span> <i class='fa fa-calendar-plus-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendartimesoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar-times-o'></i>"><span></span> <i class='fa fa-calendar-times-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facalendaroi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-calendar-o'></i>"><span></span> <i class='fa fa-calendar-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facamerai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-camera'></i>"><span></span> <i class='fa fa-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facameraretroi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-camera-retro'></i>"><span></span> <i class='fa fa-camera-retro'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-car'></i>"><span></span> <i class='fa fa-car'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-down'></i>"><span></span> <i class='fa fa-caret-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-left'></i>"><span></span> <i class='fa fa-caret-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-right'></i>"><span></span> <i class='fa fa-caret-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-up'></i>"><span></span> <i class='fa fa-caret-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretsquareodowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-square-o-down'></i>"><span></span> <i class='fa fa-caret-square-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretsquareolefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-square-o-left'></i>"><span></span> <i class='fa fa-caret-square-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretsquareorighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-square-o-right'></i>"><span></span> <i class='fa fa-caret-square-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facaretsquareoupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-caret-square-o-up'></i>"><span></span> <i class='fa fa-caret-square-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facartarrowdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cart-arrow-down'></i>"><span></span> <i class='fa fa-cart-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facartplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cart-plus'></i>"><span></span> <i class='fa fa-cart-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc'></i>"><span></span> <i class='fa fa-cc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccamexi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-amex'></i>"><span></span> <i class='fa fa-cc-amex'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccdinersclubi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-diners-club'></i>"><span></span> <i class='fa fa-cc-diners-club'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccdiscoveri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-discover'></i>"><span></span> <i class='fa fa-cc-discover'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccjcbi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-jcb'></i>"><span></span> <i class='fa fa-cc-jcb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccmastercardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-mastercard'></i>"><span></span> <i class='fa fa-cc-mastercard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccpaypali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-paypal'></i>"><span></span> <i class='fa fa-cc-paypal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccstripei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-stripe'></i>"><span></span> <i class='fa fa-cc-stripe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faccvisai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cc-visa'></i>"><span></span> <i class='fa fa-cc-visa'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facertificatei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-certificate'></i>"><span></span> <i class='fa fa-certificate'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachecki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-check'></i>"><span></span> <i class='fa fa-check'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facheckcirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-check-circle'></i>"><span></span> <i class='fa fa-check-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facheckcircleoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-check-circle-o'></i>"><span></span> <i class='fa fa-check-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachecksquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-check-square'></i>"><span></span> <i class='fa fa-check-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachecksquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-check-square-o'></i>"><span></span> <i class='fa fa-check-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevroncircledowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-circle-down'></i>"><span></span> <i class='fa fa-chevron-circle-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevroncirclelefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-circle-left'></i>"><span></span> <i class='fa fa-chevron-circle-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevroncirclerighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-circle-right'></i>"><span></span> <i class='fa fa-chevron-circle-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevroncircleupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-circle-up'></i>"><span></span> <i class='fa fa-chevron-circle-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevrondowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-down'></i>"><span></span> <i class='fa fa-chevron-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevronlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-left'></i>"><span></span> <i class='fa fa-chevron-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevronrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-right'></i>"><span></span> <i class='fa fa-chevron-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachevronupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chevron-up'></i>"><span></span> <i class='fa fa-chevron-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachildi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-child'></i>"><span></span> <i class='fa fa-child'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fachromei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-chrome'></i>"><span></span> <i class='fa fa-chrome'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-clone'></i>"><span></span> <i class='fa fa-clone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-circle'></i>"><span></span> <i class='fa fa-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facircleoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-circle-o'></i>"><span></span> <i class='fa fa-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facircleonotchi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-circle-o-notch'></i>"><span></span> <i class='fa fa-circle-o-notch'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facirclethini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-circle-thin'></i>"><span></span> <i class='fa fa-circle-thin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclipboardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-clipboard'></i>"><span></span> <i class='fa fa-clipboard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclockoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-clock-o'></i>"><span></span> <i class='fa fa-clock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclosei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-close'></i>"><span></span> <i class='fa fa-close'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facloudi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cloud'></i>"><span></span> <i class='fa fa-cloud'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclouddownloadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cloud-download'></i>"><span></span> <i class='fa fa-cloud-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faclouduploadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cloud-upload'></i>"><span></span> <i class='fa fa-cloud-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facnyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cny'></i>"><span></span> <i class='fa fa-cny'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facodei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-code'></i>"><span></span> <i class='fa fa-code'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facodeforki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-code-fork'></i>"><span></span> <i class='fa fa-code-fork'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facodepeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-codepen'></i>"><span></span> <i class='fa fa-codepen'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facoffeei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-coffee'></i>"><span></span> <i class='fa fa-coffee'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facolumnsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-columns'></i>"><span></span> <i class='fa fa-columns'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommenti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-comment'></i>"><span></span> <i class='fa fa-comment'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommentoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-comment-o'></i>"><span></span> <i class='fa fa-comment-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommentsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-comments'></i>"><span></span> <i class='fa fa-comments'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommentsoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-comments-o'></i>"><span></span> <i class='fa fa-comments-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommentingi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-commenting'></i>"><span></span> <i class='fa fa-commenting'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facommentingoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-commenting-o'></i>"><span></span> <i class='fa fa-commenting-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facompassi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-compass'></i>"><span></span> <i class='fa fa-compass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facompressi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-compress'></i>"><span></span> <i class='fa fa-compress'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faconnectdevelopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-connectdevelop'></i>"><span></span> <i class='fa fa-connectdevelop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facontaoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-contao'></i>"><span></span> <i class='fa fa-contao'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facopyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-copy'></i>"><span></span> <i class='fa fa-copy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facopyrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-copyright'></i>"><span></span> <i class='fa fa-copyright'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facreativecommonsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-creative-commons'></i>"><span></span> <i class='fa fa-creative-commons'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facreditcardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-credit-card'></i>"><span></span> <i class='fa fa-credit-card'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facropi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-crop'></i>"><span></span> <i class='fa fa-crop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facrosshairsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-crosshairs'></i>"><span></span> <i class='fa fa-crosshairs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facss3i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-css3'></i>"><span></span> <i class='fa fa-css3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facubei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cube'></i>"><span></span> <i class='fa fa-cube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facubesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cubes'></i>"><span></span> <i class='fa fa-cubes'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facuti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cut'></i>"><span></span> <i class='fa fa-cut'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-facutleryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-cutlery'></i>"><span></span> <i class='fa fa-cutlery'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadashboardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dashboard'></i>"><span></span> <i class='fa fa-dashboard'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadashcubei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dashcube'></i>"><span></span> <i class='fa fa-dashcube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadatabasei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-database'></i>"><span></span> <i class='fa fa-database'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadedenti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dedent'></i>"><span></span> <i class='fa fa-dedent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadeliciousi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-delicious'></i>"><span></span> <i class='fa fa-delicious'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadesktopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-desktop'></i>"><span></span> <i class='fa fa-desktop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadeviantarti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-deviantart'></i>"><span></span> <i class='fa fa-deviantart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadiamondi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-diamond'></i>"><span></span> <i class='fa fa-diamond'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadiggi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-digg'></i>"><span></span> <i class='fa fa-digg'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadollari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dollar'></i>"><span></span> <i class='fa fa-dollar'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadotcircleoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dot-circle-o'></i>"><span></span> <i class='fa fa-dot-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadownloadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-download'></i>"><span></span> <i class='fa fa-download'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadribbblei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dribbble'></i>"><span></span> <i class='fa fa-dribbble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadropboxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-dropbox'></i>"><span></span> <i class='fa fa-dropbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fadrupali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-drupal'></i>"><span></span> <i class='fa fa-drupal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faediti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-edit'></i>"><span></span> <i class='fa fa-edit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faejecti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-eject'></i>"><span></span> <i class='fa fa-eject'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faellipsishi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ellipsis-h'></i>"><span></span> <i class='fa fa-ellipsis-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faellipsisvi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ellipsis-v'></i>"><span></span> <i class='fa fa-ellipsis-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faempirei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-empire'></i>"><span></span> <i class='fa fa-empire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faenvelopei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-envelope'></i>"><span></span> <i class='fa fa-envelope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faenvelopeoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-envelope-o'></i>"><span></span> <i class='fa fa-envelope-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faenvelopesquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-envelope-square'></i>"><span></span> <i class='fa fa-envelope-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faeraseri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-eraser'></i>"><span></span> <i class='fa fa-eraser'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faeuroi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-euro'></i>"><span></span> <i class='fa fa-euro'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexchangei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-exchange'></i>"><span></span> <i class='fa fa-exchange'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexclamationi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-exclamation'></i>"><span></span> <i class='fa fa-exclamation'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexclamationcirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-exclamation-circle'></i>"><span></span> <i class='fa fa-exclamation-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexclamationtrianglei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-exclamation-triangle'></i>"><span></span> <i class='fa fa-exclamation-triangle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexpandi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-expand'></i>"><span></span> <i class='fa fa-expand'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexpeditedssli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-expeditedssl'></i>"><span></span> <i class='fa fa-expeditedssl'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexternallinki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-external-link'></i>"><span></span> <i class='fa fa-external-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faexternallinksquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-external-link-square'></i>"><span></span> <i class='fa fa-external-link-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faeyei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-eye'></i>"><span></span> <i class='fa fa-eye'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faeyeslashi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-eye-slash'></i>"><span></span> <i class='fa fa-eye-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faeyedropperi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-eyedropper'></i>"><span></span> <i class='fa fa-eyedropper'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafacebooki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-facebook'></i>"><span></span> <i class='fa fa-facebook'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafacebookofficiali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-facebook-official'></i>"><span></span> <i class='fa fa-facebook-official'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafacebooksquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-facebook-square'></i>"><span></span> <i class='fa fa-facebook-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafastbackwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fast-backward'></i>"><span></span> <i class='fa fa-fast-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafastforwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fast-forward'></i>"><span></span> <i class='fa fa-fast-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafaxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fax'></i>"><span></span> <i class='fa fa-fax'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafemalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-female'></i>"><span></span> <i class='fa fa-female'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafighterjeti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fighter-jet'></i>"><span></span> <i class='fa fa-fighter-jet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file'></i>"><span></span> <i class='fa fa-file'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafileoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-o'></i>"><span></span> <i class='fa fa-file-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilearchiveoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-archive-o'></i>"><span></span> <i class='fa fa-file-archive-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafileaudiooi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-audio-o'></i>"><span></span> <i class='fa fa-file-audio-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilecodeoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-code-o'></i>"><span></span> <i class='fa fa-file-code-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafileexceloi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-excel-o'></i>"><span></span> <i class='fa fa-file-excel-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafileimageoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-image-o'></i>"><span></span> <i class='fa fa-file-image-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilemovieoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-movie-o'></i>"><span></span> <i class='fa fa-file-movie-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilepdfoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-pdf-o'></i>"><span></span> <i class='fa fa-file-pdf-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilephotooi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-photo-o'></i>"><span></span> <i class='fa fa-file-photo-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilepictureoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-picture-o'></i>"><span></span> <i class='fa fa-file-picture-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilepowerpointoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-powerpoint-o'></i>"><span></span> <i class='fa fa-file-powerpoint-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilesoundoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-sound-o'></i>"><span></span> <i class='fa fa-file-sound-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafiletexti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-text'></i>"><span></span> <i class='fa fa-file-text'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafiletextoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-text-o'></i>"><span></span> <i class='fa fa-file-text-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilevideooi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-video-o'></i>"><span></span> <i class='fa fa-file-video-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilewordoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-word-o'></i>"><span></span> <i class='fa fa-file-word-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilezipoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-file-zip-o'></i>"><span></span> <i class='fa fa-file-zip-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilesoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-files-o'></i>"><span></span> <i class='fa fa-files-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilmi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-film'></i>"><span></span> <i class='fa fa-film'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafilteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-filter'></i>"><span></span> <i class='fa fa-filter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafirei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fire'></i>"><span></span> <i class='fa fa-fire'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafireextinguisheri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fire-extinguisher'></i>"><span></span> <i class='fa fa-fire-extinguisher'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafirefoxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-firefox'></i>"><span></span> <i class='fa fa-firefox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faflagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-flag'></i>"><span></span> <i class='fa fa-flag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faflagcheckeredi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-flag-checkered'></i>"><span></span> <i class='fa fa-flag-checkered'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faflagoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-flag-o'></i>"><span></span> <i class='fa fa-flag-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faflaski" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-flask'></i>"><span></span> <i class='fa fa-flask'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faflickri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-flickr'></i>"><span></span> <i class='fa fa-flickr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafloppyoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-floppy-o'></i>"><span></span> <i class='fa fa-floppy-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafolderi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-folder'></i>"><span></span> <i class='fa fa-folder'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafolderoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-folder-o'></i>"><span></span> <i class='fa fa-folder-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafolderopeni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-folder-open'></i>"><span></span> <i class='fa fa-folder-open'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafolderopenoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-folder-open-o'></i>"><span></span> <i class='fa fa-folder-open-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafonti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-font'></i>"><span></span> <i class='fa fa-font'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafonticonsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fonticons'></i>"><span></span> <i class='fa fa-fonticons'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faforumbeei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-forumbee'></i>"><span></span> <i class='fa fa-forumbee'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faforwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-forward'></i>"><span></span> <i class='fa fa-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafoursquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-foursquare'></i>"><span></span> <i class='fa fa-foursquare'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafrownoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-frown-o'></i>"><span></span> <i class='fa fa-frown-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafutboloi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-futbol-o'></i>"><span></span> <i class='fa fa-futbol-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagamepadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gamepad'></i>"><span></span> <i class='fa fa-gamepad'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagaveli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gavel'></i>"><span></span> <i class='fa fa-gavel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagbpi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gbp'></i>"><span></span> <i class='fa fa-gbp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ge'></i>"><span></span> <i class='fa fa-ge'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fageari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gear'></i>"><span></span> <i class='fa fa-gear'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagearsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gears'></i>"><span></span> <i class='fa fa-gears'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagetpocketi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-get-pocket'></i>"><span></span> <i class='fa fa-get-pocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faggi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gg'></i>"><span></span> <i class='fa fa-gg'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faggcirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gg-circle'></i>"><span></span> <i class='fa fa-gg-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagifti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gift'></i>"><span></span> <i class='fa fa-gift'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagiti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-git'></i>"><span></span> <i class='fa fa-git'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagitsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-git-square'></i>"><span></span> <i class='fa fa-git-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagithubi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-github'></i>"><span></span> <i class='fa fa-github'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagithubalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-github-alt'></i>"><span></span> <i class='fa fa-github-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagithubsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-github-square'></i>"><span></span> <i class='fa fa-github-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagittipi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gittip'></i>"><span></span> <i class='fa fa-gittip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faglassi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-glass'></i>"><span></span> <i class='fa fa-glass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faglobei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-globe'></i>"><span></span> <i class='fa fa-globe'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagooglei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-google'></i>"><span></span> <i class='fa fa-google'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagoogleplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-google-plus'></i>"><span></span> <i class='fa fa-google-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagoogleplussquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-google-plus-square'></i>"><span></span> <i class='fa fa-google-plus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagooglewalleti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-google-wallet'></i>"><span></span> <i class='fa fa-google-wallet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagraduationcapi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-graduation-cap'></i>"><span></span> <i class='fa fa-graduation-cap'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagratipayi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-gratipay'></i>"><span></span> <i class='fa fa-gratipay'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fagroupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-group'></i>"><span></span> <i class='fa fa-group'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-h-square'></i>"><span></span> <i class='fa fa-h-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahackernewsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hacker-news'></i>"><span></span> <i class='fa fa-hacker-news'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandgraboi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-grab-o'></i>"><span></span> <i class='fa fa-hand-grab-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandlizardoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-lizard-o'></i>"><span></span> <i class='fa fa-hand-lizard-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandpaperoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-paper-o'></i>"><span></span> <i class='fa fa-hand-paper-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandpeaceoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-peace-o'></i>"><span></span> <i class='fa fa-hand-peace-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandpointeroi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-pointer-o'></i>"><span></span> <i class='fa fa-hand-pointer-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandrockoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-rock-o'></i>"><span></span> <i class='fa fa-hand-rock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandscissorsoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-scissors-o'></i>"><span></span> <i class='fa fa-hand-scissors-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandspockoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-spock-o'></i>"><span></span> <i class='fa fa-hand-spock-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandstopoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-stop-o'></i>"><span></span> <i class='fa fa-hand-stop-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandodowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-o-down'></i>"><span></span> <i class='fa fa-hand-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandolefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-o-left'></i>"><span></span> <i class='fa fa-hand-o-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandorighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-o-right'></i>"><span></span> <i class='fa fa-hand-o-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahandoupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hand-o-up'></i>"><span></span> <i class='fa fa-hand-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahddoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hdd-o'></i>"><span></span> <i class='fa fa-hdd-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faheaderi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-header'></i>"><span></span> <i class='fa fa-header'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faheadphonesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-headphones'></i>"><span></span> <i class='fa fa-headphones'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahearti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-heart'></i>"><span></span> <i class='fa fa-heart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faheartoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-heart-o'></i>"><span></span> <i class='fa fa-heart-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faheartbeati" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-heartbeat'></i>"><span></span> <i class='fa fa-heartbeat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahistoryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-history'></i>"><span></span> <i class='fa fa-history'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahomei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-home'></i>"><span></span> <i class='fa fa-home'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahospitaloi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hospital-o'></i>"><span></span> <i class='fa fa-hospital-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahoteli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hotel'></i>"><span></span> <i class='fa fa-hotel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglassi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass'></i>"><span></span> <i class='fa fa-hourglass'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglass1i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass-1'></i>"><span></span> <i class='fa fa-hourglass-1'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglass2i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass-2'></i>"><span></span> <i class='fa fa-hourglass-2'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglass3i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass-3'></i>"><span></span> <i class='fa fa-hourglass-3'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglassendi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass-end'></i>"><span></span> <i class='fa fa-hourglass-end'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahourglassoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-hourglass-o'></i>"><span></span> <i class='fa fa-hourglass-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahouzzi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-houzz'></i>"><span></span> <i class='fa fa-houzz'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fahtml5i" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-html5'></i>"><span></span> <i class='fa fa-html5'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faicursori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-i-cursor'></i>"><span></span> <i class='fa fa-i-cursor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-failsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ils'></i>"><span></span> <i class='fa fa-ils'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faimagei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-image'></i>"><span></span> <i class='fa fa-image'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainboxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-inbox'></i>"><span></span> <i class='fa fa-inbox'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faindenti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-indent'></i>"><span></span> <i class='fa fa-indent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faindustryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-industry'></i>"><span></span> <i class='fa fa-industry'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainfoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-info'></i>"><span></span> <i class='fa fa-info'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainfocirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-info-circle'></i>"><span></span> <i class='fa fa-info-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-inr'></i>"><span></span> <i class='fa fa-inr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainstagrami" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-instagram'></i>"><span></span> <i class='fa fa-instagram'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainstitutioni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-institution'></i>"><span></span> <i class='fa fa-institution'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fainternetexploreri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-internet-explorer'></i>"><span></span> <i class='fa fa-internet-explorer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faioxhosti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ioxhost'></i>"><span></span> <i class='fa fa-ioxhost'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faitalici" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-italic'></i>"><span></span> <i class='fa fa-italic'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fajoomlai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-joomla'></i>"><span></span> <i class='fa fa-joomla'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fajpyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-jpy'></i>"><span></span> <i class='fa fa-jpy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fajsfiddlei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-jsfiddle'></i>"><span></span> <i class='fa fa-jsfiddle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fakeyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-key'></i>"><span></span> <i class='fa fa-key'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fakeyboardoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-keyboard-o'></i>"><span></span> <i class='fa fa-keyboard-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fakrwi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-krw'></i>"><span></span> <i class='fa fa-krw'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falanguagei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-language'></i>"><span></span> <i class='fa fa-language'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falaptopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-laptop'></i>"><span></span> <i class='fa fa-laptop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falegali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-legal'></i>"><span></span> <i class='fa fa-legal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falemonoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-lemon-o'></i>"><span></span> <i class='fa fa-lemon-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faleveldowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-level-down'></i>"><span></span> <i class='fa fa-level-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falevelupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-level-up'></i>"><span></span> <i class='fa fa-level-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faliferingi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-life-ring'></i>"><span></span> <i class='fa fa-life-ring'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falightbulboi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-lightbulb-o'></i>"><span></span> <i class='fa fa-lightbulb-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falinecharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-line-chart'></i>"><span></span> <i class='fa fa-line-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falinki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-link'></i>"><span></span> <i class='fa fa-link'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falinkedini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-linkedin'></i>"><span></span> <i class='fa fa-linkedin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falinkedinsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-linkedin-square'></i>"><span></span> <i class='fa fa-linkedin-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falinuxi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-linux'></i>"><span></span> <i class='fa fa-linux'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falistalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-list-alt'></i>"><span></span> <i class='fa fa-list-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falistoli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-list-ol'></i>"><span></span> <i class='fa fa-list-ol'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falistuli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-list-ul'></i>"><span></span> <i class='fa fa-list-ul'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falocationarrowi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-location-arrow'></i>"><span></span> <i class='fa fa-location-arrow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falocki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-lock'></i>"><span></span> <i class='fa fa-lock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falongarrowdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-long-arrow-down'></i>"><span></span> <i class='fa fa-long-arrow-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falongarrowlefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-long-arrow-left'></i>"><span></span> <i class='fa fa-long-arrow-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falongarrowrighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-long-arrow-right'></i>"><span></span> <i class='fa fa-long-arrow-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-falongarrowupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-long-arrow-up'></i>"><span></span> <i class='fa fa-long-arrow-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famagici" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-magic'></i>"><span></span> <i class='fa fa-magic'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famagneti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-magnet'></i>"><span></span> <i class='fa fa-magnet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famalei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-male'></i>"><span></span> <i class='fa fa-male'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famapi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-map'></i>"><span></span> <i class='fa fa-map'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famapoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-map-o'></i>"><span></span> <i class='fa fa-map-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famapmarkeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-map-marker'></i>"><span></span> <i class='fa fa-map-marker'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famappini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-map-pin'></i>"><span></span> <i class='fa fa-map-pin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famapsignsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-map-signs'></i>"><span></span> <i class='fa fa-map-signs'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famarsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mars'></i>"><span></span> <i class='fa fa-mars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famarsdoublei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mars-double'></i>"><span></span> <i class='fa fa-mars-double'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famarsstrokei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mars-stroke'></i>"><span></span> <i class='fa fa-mars-stroke'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famarsstrokehi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mars-stroke-h'></i>"><span></span> <i class='fa fa-mars-stroke-h'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famarsstrokevi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mars-stroke-v'></i>"><span></span> <i class='fa fa-mars-stroke-v'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famaxcdni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-maxcdn'></i>"><span></span> <i class='fa fa-maxcdn'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fameanpathi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-meanpath'></i>"><span></span> <i class='fa fa-meanpath'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famediumi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-medium'></i>"><span></span> <i class='fa fa-medium'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famedkiti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-medkit'></i>"><span></span> <i class='fa fa-medkit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famehoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-meh-o'></i>"><span></span> <i class='fa fa-meh-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famercuryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mercury'></i>"><span></span> <i class='fa fa-mercury'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famicrophonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-microphone'></i>"><span></span> <i class='fa fa-microphone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famicrophoneslashi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-microphone-slash'></i>"><span></span> <i class='fa fa-microphone-slash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faminusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-minus'></i>"><span></span> <i class='fa fa-minus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faminuscirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-minus-circle'></i>"><span></span> <i class='fa fa-minus-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faminussquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-minus-square'></i>"><span></span> <i class='fa fa-minus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faminussquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-minus-square-o'></i>"><span></span> <i class='fa fa-minus-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famobilei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mobile'></i>"><span></span> <i class='fa fa-mobile'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famoneyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-money'></i>"><span></span> <i class='fa fa-money'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famoonoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-moon-o'></i>"><span></span> <i class='fa fa-moon-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famortarboardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mortar-board'></i>"><span></span> <i class='fa fa-mortar-board'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famotorcyclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-motorcycle'></i>"><span></span> <i class='fa fa-motorcycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famousepointeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-mouse-pointer'></i>"><span></span> <i class='fa fa-mouse-pointer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-famusici" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-music'></i>"><span></span> <i class='fa fa-music'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fanaviconi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-navicon'></i>"><span></span> <i class='fa fa-navicon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faneuteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-neuter'></i>"><span></span> <i class='fa fa-neuter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fanewspaperoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-newspaper-o'></i>"><span></span> <i class='fa fa-newspaper-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faobjectgroupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-object-group'></i>"><span></span> <i class='fa fa-object-group'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faobjectungroupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-object-ungroup'></i>"><span></span> <i class='fa fa-object-ungroup'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faodnoklassnikii" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-odnoklassniki'></i>"><span></span> <i class='fa fa-odnoklassniki'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faodnoklassnikisquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-odnoklassniki-square'></i>"><span></span> <i class='fa fa-odnoklassniki-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faopencarti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-opencart'></i>"><span></span> <i class='fa fa-opencart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faopenidi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-openid'></i>"><span></span> <i class='fa fa-openid'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faoperai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-opera'></i>"><span></span> <i class='fa fa-opera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faoptinmonsteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-optin-monster'></i>"><span></span> <i class='fa fa-optin-monster'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faoutdenti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-outdent'></i>"><span></span> <i class='fa fa-outdent'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapagelinesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pagelines'></i>"><span></span> <i class='fa fa-pagelines'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapaintbrushi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paint-brush'></i>"><span></span> <i class='fa fa-paint-brush'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapaperclipi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paperclip'></i>"><span></span> <i class='fa fa-paperclip'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faparagraphi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paragraph'></i>"><span></span> <i class='fa fa-paragraph'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapastei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paste'></i>"><span></span> <i class='fa fa-paste'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapausei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pause'></i>"><span></span> <i class='fa fa-pause'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapawi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paw'></i>"><span></span> <i class='fa fa-paw'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapaypali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-paypal'></i>"><span></span> <i class='fa fa-paypal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapencili" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pencil'></i>"><span></span> <i class='fa fa-pencil'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapencilsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pencil-square'></i>"><span></span> <i class='fa fa-pencil-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapencilsquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pencil-square-o'></i>"><span></span> <i class='fa fa-pencil-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faphonei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-phone'></i>"><span></span> <i class='fa fa-phone'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faphonesquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-phone-square'></i>"><span></span> <i class='fa fa-phone-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faphotoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-photo'></i>"><span></span> <i class='fa fa-photo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapictureoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-picture-o'></i>"><span></span> <i class='fa fa-picture-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapiecharti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pie-chart'></i>"><span></span> <i class='fa fa-pie-chart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapiedpiperi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pied-piper'></i>"><span></span> <i class='fa fa-pied-piper'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapiedpiperalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pied-piper-alt'></i>"><span></span> <i class='fa fa-pied-piper-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapinteresti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pinterest'></i>"><span></span> <i class='fa fa-pinterest'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapinterestpi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pinterest-p'></i>"><span></span> <i class='fa fa-pinterest-p'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapinterestsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-pinterest-square'></i>"><span></span> <i class='fa fa-pinterest-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplanei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plane'></i>"><span></span> <i class='fa fa-plane'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplayi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-play'></i>"><span></span> <i class='fa fa-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplaycirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-play-circle'></i>"><span></span> <i class='fa fa-play-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplaycircleoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-play-circle-o'></i>"><span></span> <i class='fa fa-play-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplugi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plug'></i>"><span></span> <i class='fa fa-plug'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plus'></i>"><span></span> <i class='fa fa-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapluscirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plus-circle'></i>"><span></span> <i class='fa fa-plus-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplussquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plus-square'></i>"><span></span> <i class='fa fa-plus-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faplussquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-plus-square-o'></i>"><span></span> <i class='fa fa-plus-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapoweroffi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-power-off'></i>"><span></span> <i class='fa fa-power-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faprinti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-print'></i>"><span></span> <i class='fa fa-print'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fapuzzlepiecei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-puzzle-piece'></i>"><span></span> <i class='fa fa-puzzle-piece'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faqqi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-qq'></i>"><span></span> <i class='fa fa-qq'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faqrcodei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-qrcode'></i>"><span></span> <i class='fa fa-qrcode'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faquestioni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-question'></i>"><span></span> <i class='fa fa-question'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faquestioncirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-question-circle'></i>"><span></span> <i class='fa fa-question-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faquotelefti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-quote-left'></i>"><span></span> <i class='fa fa-quote-left'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faquoterighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-quote-right'></i>"><span></span> <i class='fa fa-quote-right'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farandomi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-random'></i>"><span></span> <i class='fa fa-random'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farebeli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-rebel'></i>"><span></span> <i class='fa fa-rebel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farecyclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-recycle'></i>"><span></span> <i class='fa fa-recycle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faredditi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-reddit'></i>"><span></span> <i class='fa fa-reddit'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faredditsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-reddit-square'></i>"><span></span> <i class='fa fa-reddit-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farefreshi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-refresh'></i>"><span></span> <i class='fa fa-refresh'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faregisteredi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-registered'></i>"><span></span> <i class='fa fa-registered'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faremovei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-remove'></i>"><span></span> <i class='fa fa-remove'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farenreni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-renren'></i>"><span></span> <i class='fa fa-renren'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farepeati" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-repeat'></i>"><span></span> <i class='fa fa-repeat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fareplyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-reply'></i>"><span></span> <i class='fa fa-reply'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fareplyalli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-reply-all'></i>"><span></span> <i class='fa fa-reply-all'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faretweeti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-retweet'></i>"><span></span> <i class='fa fa-retweet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farmbi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-rmb'></i>"><span></span> <i class='fa fa-rmb'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faroadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-road'></i>"><span></span> <i class='fa fa-road'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-farocketi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-rocket'></i>"><span></span> <i class='fa fa-rocket'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faroublei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-rouble'></i>"><span></span> <i class='fa fa-rouble'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasafarii" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-safari'></i>"><span></span> <i class='fa fa-safari'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasavei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-save'></i>"><span></span> <i class='fa fa-save'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fascissorsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-scissors'></i>"><span></span> <i class='fa fa-scissors'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasearchi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-search'></i>"><span></span> <i class='fa fa-search'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasearchminusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-search-minus'></i>"><span></span> <i class='fa fa-search-minus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasearchplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-search-plus'></i>"><span></span> <i class='fa fa-search-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasellsyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sellsy'></i>"><span></span> <i class='fa fa-sellsy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasendi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-send'></i>"><span></span> <i class='fa fa-send'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasendoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-send-o'></i>"><span></span> <i class='fa fa-send-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faserveri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-server'></i>"><span></span> <i class='fa fa-server'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasharei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-share'></i>"><span></span> <i class='fa fa-share'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasharesquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-share-square'></i>"><span></span> <i class='fa fa-share-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasharesquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-share-square-o'></i>"><span></span> <i class='fa fa-share-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasharealti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-share-alt'></i>"><span></span> <i class='fa fa-share-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasharealtsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-share-alt-square'></i>"><span></span> <i class='fa fa-share-alt-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fashekeli" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-shekel'></i>"><span></span> <i class='fa fa-shekel'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fashieldi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-shield'></i>"><span></span> <i class='fa fa-shield'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fashipi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-ship'></i>"><span></span> <i class='fa fa-ship'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fashirtsinbulki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-shirtsinbulk'></i>"><span></span> <i class='fa fa-shirtsinbulk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fashoppingcarti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-shopping-cart'></i>"><span></span> <i class='fa fa-shopping-cart'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasignini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sign-in'></i>"><span></span> <i class='fa fa-sign-in'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasignouti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sign-out'></i>"><span></span> <i class='fa fa-sign-out'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasignali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-signal'></i>"><span></span> <i class='fa fa-signal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasimplybuilti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-simplybuilt'></i>"><span></span> <i class='fa fa-simplybuilt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasitemapi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sitemap'></i>"><span></span> <i class='fa fa-sitemap'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faskyatlasi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-skyatlas'></i>"><span></span> <i class='fa fa-skyatlas'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faskypei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-skype'></i>"><span></span> <i class='fa fa-skype'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faslacki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-slack'></i>"><span></span> <i class='fa fa-slack'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faslidersi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sliders'></i>"><span></span> <i class='fa fa-sliders'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faslidesharei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-slideshare'></i>"><span></span> <i class='fa fa-slideshare'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasmileoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-smile-o'></i>"><span></span> <i class='fa fa-smile-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasoccerballoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-soccer-ball-o'></i>"><span></span> <i class='fa fa-soccer-ball-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasorti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort'></i>"><span></span> <i class='fa fa-sort'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortalphaasci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-alpha-asc'></i>"><span></span> <i class='fa fa-sort-alpha-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortalphadesci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-alpha-desc'></i>"><span></span> <i class='fa fa-sort-alpha-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortamountasci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-amount-asc'></i>"><span></span> <i class='fa fa-sort-amount-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortamountdesci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-amount-desc'></i>"><span></span> <i class='fa fa-sort-amount-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortnumericasci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-numeric-asc'></i>"><span></span> <i class='fa fa-sort-numeric-asc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasortnumericdesci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sort-numeric-desc'></i>"><span></span> <i class='fa fa-sort-numeric-desc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasoundcloudi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-soundcloud'></i>"><span></span> <i class='fa fa-soundcloud'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faspaceshuttlei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-space-shuttle'></i>"><span></span> <i class='fa fa-space-shuttle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faspinneri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-spinner'></i>"><span></span> <i class='fa fa-spinner'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faspooni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-spoon'></i>"><span></span> <i class='fa fa-spoon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faspotifyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-spotify'></i>"><span></span> <i class='fa fa-spotify'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-square'></i>"><span></span> <i class='fa fa-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasquareoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-square-o'></i>"><span></span> <i class='fa fa-square-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastackexchangei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stack-exchange'></i>"><span></span> <i class='fa fa-stack-exchange'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastackoverflowi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stack-overflow'></i>"><span></span> <i class='fa fa-stack-overflow'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastari" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-star'></i>"><span></span> <i class='fa fa-star'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastarhalfi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-star-half'></i>"><span></span> <i class='fa fa-star-half'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastarhalfoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-star-half-o'></i>"><span></span> <i class='fa fa-star-half-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastaroi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-star-o'></i>"><span></span> <i class='fa fa-star-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasteami" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-steam'></i>"><span></span> <i class='fa fa-steam'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasteamsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-steam-square'></i>"><span></span> <i class='fa fa-steam-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastepbackwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-step-backward'></i>"><span></span> <i class='fa fa-step-backward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastepforwardi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-step-forward'></i>"><span></span> <i class='fa fa-step-forward'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastethoscopei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stethoscope'></i>"><span></span> <i class='fa fa-stethoscope'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastickynotei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sticky-note'></i>"><span></span> <i class='fa fa-sticky-note'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastickynoteoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sticky-note-o'></i>"><span></span> <i class='fa fa-sticky-note-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastopi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stop'></i>"><span></span> <i class='fa fa-stop'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastreetviewi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-street-view'></i>"><span></span> <i class='fa fa-street-view'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastrikethroughi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-strikethrough'></i>"><span></span> <i class='fa fa-strikethrough'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastumbleuponi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stumbleupon'></i>"><span></span> <i class='fa fa-stumbleupon'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fastumbleuponcirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-stumbleupon-circle'></i>"><span></span> <i class='fa fa-stumbleupon-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasubscripti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-subscript'></i>"><span></span> <i class='fa fa-subscript'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasubwayi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-subway'></i>"><span></span> <i class='fa fa-subway'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasuitcasei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-suitcase'></i>"><span></span> <i class='fa fa-suitcase'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasunoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-sun-o'></i>"><span></span> <i class='fa fa-sun-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fasuperscripti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-superscript'></i>"><span></span> <i class='fa fa-superscript'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatablei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-table'></i>"><span></span> <i class='fa fa-table'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatableti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tablet'></i>"><span></span> <i class='fa fa-tablet'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatachometeri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tachometer'></i>"><span></span> <i class='fa fa-tachometer'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatagi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tag'></i>"><span></span> <i class='fa fa-tag'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatagsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tags'></i>"><span></span> <i class='fa fa-tags'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatasksi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tasks'></i>"><span></span> <i class='fa fa-tasks'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fataxii" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-taxi'></i>"><span></span> <i class='fa fa-taxi'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatelevisioni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-television'></i>"><span></span> <i class='fa fa-television'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatencentweiboi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tencent-weibo'></i>"><span></span> <i class='fa fa-tencent-weibo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faterminali" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-terminal'></i>"><span></span> <i class='fa fa-terminal'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatextheighti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-text-height'></i>"><span></span> <i class='fa fa-text-height'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatextwidthi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-text-width'></i>"><span></span> <i class='fa fa-text-width'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-th'></i>"><span></span> <i class='fa fa-th'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathlargei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-th-large'></i>"><span></span> <i class='fa fa-th-large'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathumbtacki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-thumb-tack'></i>"><span></span> <i class='fa fa-thumb-tack'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathumbsdowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-thumbs-down'></i>"><span></span> <i class='fa fa-thumbs-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathumbsupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-thumbs-up'></i>"><span></span> <i class='fa fa-thumbs-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathumbsodowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-thumbs-o-down'></i>"><span></span> <i class='fa fa-thumbs-o-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fathumbsoupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-thumbs-o-up'></i>"><span></span> <i class='fa fa-thumbs-o-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatimesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-times'></i>"><span></span> <i class='fa fa-times'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatimescirclei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-times-circle'></i>"><span></span> <i class='fa fa-times-circle'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatimescircleoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-times-circle-o'></i>"><span></span> <i class='fa fa-times-circle-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatinti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tint'></i>"><span></span> <i class='fa fa-tint'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatoggleoffi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-toggle-off'></i>"><span></span> <i class='fa fa-toggle-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatoggleoni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-toggle-on'></i>"><span></span> <i class='fa fa-toggle-on'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrademarki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-trademark'></i>"><span></span> <i class='fa fa-trademark'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatraini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-train'></i>"><span></span> <i class='fa fa-train'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatransgenderi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-transgender'></i>"><span></span> <i class='fa fa-transgender'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatransgenderalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-transgender-alt'></i>"><span></span> <i class='fa fa-transgender-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrashi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-trash'></i>"><span></span> <i class='fa fa-trash'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrashoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-trash-o'></i>"><span></span> <i class='fa fa-trash-o'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatreei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tree'></i>"><span></span> <i class='fa fa-tree'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrelloi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-trello'></i>"><span></span> <i class='fa fa-trello'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatripadvisori" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tripadvisor'></i>"><span></span> <i class='fa fa-tripadvisor'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrophyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-trophy'></i>"><span></span> <i class='fa fa-trophy'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatrucki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-truck'></i>"><span></span> <i class='fa fa-truck'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatryi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-try'></i>"><span></span> <i class='fa fa-try'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fattyi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tty'></i>"><span></span> <i class='fa fa-tty'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatumblri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tumblr'></i>"><span></span> <i class='fa fa-tumblr'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatumblrsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-tumblr-square'></i>"><span></span> <i class='fa fa-tumblr-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatwitchi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-twitch'></i>"><span></span> <i class='fa fa-twitch'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatwitteri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-twitter'></i>"><span></span> <i class='fa fa-twitter'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fatwittersquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-twitter-square'></i>"><span></span> <i class='fa fa-twitter-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favenusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-venus'></i>"><span></span> <i class='fa fa-venus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favenusdoublei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-venus-double'></i>"><span></span> <i class='fa fa-venus-double'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favenusmarsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-venus-mars'></i>"><span></span> <i class='fa fa-venus-mars'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faviacoini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-viacoin'></i>"><span></span> <i class='fa fa-viacoin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favideocamerai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-video-camera'></i>"><span></span> <i class='fa fa-video-camera'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favimeoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-vimeo'></i>"><span></span> <i class='fa fa-vimeo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favimeosquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-vimeo-square'></i>"><span></span> <i class='fa fa-vimeo-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favinei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-vine'></i>"><span></span> <i class='fa fa-vine'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-vk'></i>"><span></span> <i class='fa fa-vk'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favolumedowni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-volume-down'></i>"><span></span> <i class='fa fa-volume-down'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favolumeoffi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-volume-off'></i>"><span></span> <i class='fa fa-volume-off'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-favolumeupi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-volume-up'></i>"><span></span> <i class='fa fa-volume-up'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faumbrellai" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-umbrella'></i>"><span></span> <i class='fa fa-umbrella'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faunderlinei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-underline'></i>"><span></span> <i class='fa fa-underline'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faundoi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-undo'></i>"><span></span> <i class='fa fa-undo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fauniversityi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-university'></i>"><span></span> <i class='fa fa-university'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faunlinki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-unlink'></i>"><span></span> <i class='fa fa-unlink'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faunlocki" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-unlock'></i>"><span></span> <i class='fa fa-unlock'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faunlockalti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-unlock-alt'></i>"><span></span> <i class='fa fa-unlock-alt'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fauploadi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-upload'></i>"><span></span> <i class='fa fa-upload'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fauseri" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-user'></i>"><span></span> <i class='fa fa-user'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fausermdi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-user-md'></i>"><span></span> <i class='fa fa-user-md'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fauserplusi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-user-plus'></i>"><span></span> <i class='fa fa-user-plus'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fausersecreti" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-user-secret'></i>"><span></span> <i class='fa fa-user-secret'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fausertimesi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-user-times'></i>"><span></span> <i class='fa fa-user-times'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fausersi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-users'></i>"><span></span> <i class='fa fa-users'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawechati" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wechat'></i>"><span></span> <i class='fa fa-wechat'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faweiboi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-weibo'></i>"><span></span> <i class='fa fa-weibo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faweixini" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-weixin'></i>"><span></span> <i class='fa fa-weixin'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawhatsappi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-whatsapp'></i>"><span></span> <i class='fa fa-whatsapp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawheelchairi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wheelchair'></i>"><span></span> <i class='fa fa-wheelchair'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawifii" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wifi'></i>"><span></span> <i class='fa fa-wifi'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawikipediawi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wikipedia-w'></i>"><span></span> <i class='fa fa-wikipedia-w'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawindowsi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-windows'></i>"><span></span> <i class='fa fa-windows'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawoni" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-won'></i>"><span></span> <i class='fa fa-won'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawordpressi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wordpress'></i>"><span></span> <i class='fa fa-wordpress'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fawrenchi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-wrench'></i>"><span></span> <i class='fa fa-wrench'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faxingi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-xing'></i>"><span></span> <i class='fa fa-xing'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faxingsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-xing-square'></i>"><span></span> <i class='fa fa-xing-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fayahooi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-yahoo'></i>"><span></span> <i class='fa fa-yahoo'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fafayci" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-fa-yc'></i>"><span></span> <i class='fa fa-fa-yc'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-faycsquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-yc-square'></i>"><span></span> <i class='fa fa-yc-square'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fayelpi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-yelp'></i>"><span></span> <i class='fa fa-yelp'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fayoutubei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-youtube'></i>"><span></span> <i class='fa fa-youtube'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fayoutubeplayi" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-youtube-play'></i>"><span></span> <i class='fa fa-youtube-play'></i>
																					</label>
																					<label class="css-input css-radio css-radio-default push-10-r" style="width: 50px">
																					<input type="radio" id="modules_subject_icon_i-classfa-fayoutubesquarei" name="modules_subject_icon" class="modules_subject_icon"  value="<i class='fa fa-youtube-square'></i>"><span></span> <i class='fa fa-youtube-square'></i>
																					</label>
																					<div id="modules_subject_icon-check-loading"><?php echo translate("Loading"); ?>...</div>

																				<div class="help-block"><?php echo translate("Make menu of subject friendly with icon in backend mode"); ?></div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
									  </div>

									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-screen-desktop push-5-r"></i> <?php echo translate("Front-end Settings"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_pages_template"><?php echo translate("Front-end Inner Pages Default Template"); ?></label>
												<div class="col-md-5">
													<input class="form-control modules_pages_template" type="text" id="modules_pages_template" name="modules_pages_template" value="" placeholder="<?php echo translate("Template of inner pages created by module");?>" >

													<div class="help-block"><?php echo translate("Selected front-end template will be activated to inner pages created by module which was enabled pages component"); ?></div>
												</div>
												<div class="col-md-5">
													<a href="plugins/filemanager/dialog-template.php?type=2&amp;field_id=modules_pages_template&amp;relative_url=1" id="modules_pages_template-fileselector-btn" class="btn" style="padding: 0">
													<i class="si si-folder-alt fa-2x"></i> <?php echo translate("Select"); ?>
													</a>
												</div>
											</div>
										</div>

										<div class="col-md-12" style="display: none;" >
											<div class="form-group">
												<label class="col-md-12" for="modules_pages_template_settings"><?php echo translate("Front-end Inner Pages Default Template Settings"); ?></label>
												<div class="col-md-12">
													<textarea class="form-control modules_pages_template_settings" id="modules_pages_template_settings" name="modules_pages_template_settings"  placeholder="<?php echo translate("Template settings of inner pages created by module");?>"  style="visibility: hidden;" ><?php $modules_pages_template_settings_default_value[0]; ?></textarea>

													<div class="help-block"><?php echo translate("Front-end template settings will be assigned to inner pages which was enabled pages component"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_pages_parent_link_field"><?php echo translate("Front-end Parent Link Field"); ?></label>
												<div class="col-md-12">
													<select class="form-control modules_pages_parent_link_field" id="modules_pages_parent_link_field" name="modules_pages_parent_link_field" >

														<?php
														/* PHP variable for filter of dynamic selector "modules_pages_parent_link_field"*/
														/* Example:
														$modules_pages_parent_link_field_filters = array(
															"user_id" => "",
															"languages_short_name" => "",
														);
														*/
														$modules_pages_parent_link_field_filters = "";
														/* PHP variable for extended command of dynamic selector "modules_pages_parent_link_field"*/
														/* Example:
														$modules_pages_parent_link_field_extended_command = array(
															array(
																"conjunction" => "AND",
																"key" => "user_name",
																"operator" => "LIKE",
																"value" => "%Michael%",
															),
															array(
																"conjunction" => "OR",
																"key" => "user_name",
																"operator" => "LIKE",
																"value" => "%Ammy%",
															),
														)
														*/
														$modules_pages_parent_link_field_extended_command = "";
														?>
														<script>
														/* JavaScript variable for filter of dynamic selector "modules_pages_parent_link_field" */
														var modules_pages_parent_link_field_filters = $.parseJSON("<?php echo addslashes(json_encode($modules_pages_parent_link_field_filters)); ?>");
														</script>
														<?php
														$count_modules_pages_parent_link_field = count_modules_pages_parent_link_field_data_dynamic_list("", "", "", "", "1", $modules_pages_parent_link_field_filters, $modules_pages_parent_link_field_extended_command);
														if ($count_modules_pages_parent_link_field <= $configs["datatable_data_limit"]) {
														?>
														<option label="default" value=""><?php echo translate("Disable"); ?></option>
														<option label="seperator" value="" disabled>-</option>
														<?php
														$modules_pages_parent_link_field = get_modules_pages_parent_link_field_data_dynamic_list("", "", "", "", "1", $modules_pages_parent_link_field_filters, $modules_pages_parent_link_field_extended_command);
														for($i = 0; $i < count($modules_pages_parent_link_field); $i++){
															?>
																<option value="<?php echo htmlspecialchars($modules_pages_parent_link_field[$i]['pages_link']); ?>">
																	<?php echo htmlspecialchars($modules_pages_parent_link_field[$i]['pages_title'])." (ID: ".$modules_pages_parent_link_field[$i]['pages_link'].")"; ?>
																</option>
														<?php
														}
														?>
														<?php
														} else {
														?>
															<option label="default" value=""><?php echo translate("Loading");?>...</option>
														<?php
														}
														?>

													</select>
													<div id="modules_pages_parent_link_field-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>

													<div class="help-block"><?php echo translate("Front-end parent link field for inner pages selected inside a content"); ?></div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-doc push-5-r"></i> <?php echo translate("Individual Page Settings"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

										<?php
										$modules_individual_pages_parent_link_default_value = unserialize('a:1:{i:0;i:1;}');
										?>
										<script>
										/* JavaScript variable for dynamic default value "modules_individual_pages_parent_link" */
										var modules_individual_pages_parent_link_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_individual_pages_parent_link_default_value)); ?>");
										</script>
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_individual_pages_parent_link"><?php echo translate("Enable Module Individual Front-end Page Parent Link"); ?></label>
												<div class="col-md-12">
													<label class="css-input switch switch-lg switch-success">
														<input id="modules_individual_pages_parent_link" name="modules_individual_pages_parent_link" class="modules_individual_pages_parent_link" type="checkbox" value="<?php echo $modules_individual_pages_parent_link_default_value[0];?>"><span></span> <?php echo translate(""); ?>
													</label>
													<div class="help-block"><?php echo translate("Enable individual front-end page parent link of module"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="pages_id" style="margin-top: 7px;"><?php echo translate("Individual Front-end Page Parent Link"); ?></label>
												<div class="col-md-12">
													<select class="form-control js-select2" type="text" id="pages_id" name="pages_id">
														<option value=""><?php echo translate("Disable"); ?></option>
														<option value="" disabled>-</option>
														<?php
														$count_pages_id = count_pages_data_all();
														if ($count_pages_id <= $configs["datatable_data_limit"]) {
															$pages_id = get_pages_data_all();
															for($i=0;$i<count($pages_id);$i++){
															?>
																<option value="<?php echo $pages_id[$i]["pages_id"];?>"><?php echo $pages_id[$i]["pages_title"];?> (ID: <?php echo $pages_id[$i]["pages_id"];?>, <?php echo $pages_id[$i]["languages_short_name"];?>)</option>
															<?php
															}
														} else {
															?>
															<option value=""><?php echo translate("Loading");?>...</option>
															<?php
														}
														?>
													</select>
													<div id="pages_id-check-loading" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div>
													<div class="help-block"><?php echo translate("Requires if enabled Individual Front-end Page Parent Link. Selected page will be used as individual page and parent link for this module");?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<div class="col-md-12 push-10-t">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-picture push-5-r"></i> <?php echo translate("Images Settings"); ?></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">

										<?php
										$modules_image_crop_quality_default_value = unserialize('a:1:{i:0;i:75;}');
										?>
										<script>
										/* JavaScript variable for dynamic default value "modules_image_crop_quality" */
										var modules_image_crop_quality_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_quality_default_value)); ?>");
										</script>
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-md-12" for="modules_image_crop_quality"><?php echo translate("Image Quality"); ?></label>
												<div class="col-md-12">
													<input class="form-control modules_image_crop_quality" type="text" id="modules_image_crop_quality" name="modules_image_crop_quality" value="<?php echo $modules_image_crop_quality_default_value[0];?>" placeholder="<?php echo translate("");?>" data-min="0" data-max="100">

													<div class="help-block"><?php echo translate("Image quality setting (75 is best for Search Engine Optimization)"); ?></div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="block block-bordered">
												<div class="block-header">
													<label><i class="si si-picture push-5-r"></i> <?php echo translate("Thumbnail"); ?></label>
												</div>
												<div class="block-content">
													<div class="row">
														<div class="col-md-12">

															<?php
															$modules_image_crop_thumbnail_default_value = unserialize('a:1:{i:0;i:1;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_thumbnail" */
															var modules_image_crop_thumbnail_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_thumbnail_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_thumbnail"><?php echo translate("Enable Image Thumbnail"); ?></label>
																	<div class="col-md-12">
																		<label class="css-input switch switch-lg switch-success">
																			<input id="modules_image_crop_thumbnail" name="modules_image_crop_thumbnail" class="modules_image_crop_thumbnail" type="checkbox" value="<?php echo $modules_image_crop_thumbnail_default_value[0];?>"><span></span> <?php echo translate(""); ?>
																		</label>
																		<div class="help-block"><?php echo translate("Enable image thumbnail lets system create thumbnail for images automatically"); ?></div>
																		
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_thumbnail_aspectratio_default_value = unserialize('a:1:{i:0;i:1;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_thumbnail_aspectratio" */
															var modules_image_crop_thumbnail_aspectratio_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_thumbnail_aspectratio_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_thumbnail_aspectratio"><?php echo translate("Keep Aspect Ratio for Image Thumbnail"); ?></label>
																	<div class="col-md-12">
																		<label class="css-input switch switch-lg switch-success">
																			<input id="modules_image_crop_thumbnail_aspectratio" name="modules_image_crop_thumbnail_aspectratio" class="modules_image_crop_thumbnail_aspectratio" type="checkbox" value="<?php echo $modules_image_crop_thumbnail_aspectratio_default_value[0];?>"><span></span> <?php echo translate(""); ?>
																		</label>
																		<div class="help-block"><?php echo translate("Keep aspect ratio for image thumbnail withoutping to specific size"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_thumbnail_quality_default_value = unserialize('a:1:{i:0;i:75;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_thumbnail_quality" */
															var modules_image_crop_thumbnail_quality_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_thumbnail_quality_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_thumbnail_quality"><?php echo translate("Image Thumbnail Quality"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_thumbnail_quality" type="text" id="modules_image_crop_thumbnail_quality" name="modules_image_crop_thumbnail_quality" value="<?php echo $modules_image_crop_thumbnail_quality_default_value[0];?>" placeholder="<?php echo translate("");?>" data-min="0" data-max="100">

																		<div class="help-block"><?php echo translate("Image thumbnail quality setting (75 is best for Search Engine Optimization)"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_thumbnail_width_default_value = unserialize('a:1:{i:0;i:200;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_thumbnail_width" */
															var modules_image_crop_thumbnail_width_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_thumbnail_width_default_value)); ?>");
															</script>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_thumbnail_width"><?php echo translate("Image Thumbnail Width"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_thumbnail_width" type="number" id="modules_image_crop_thumbnail_width" name="modules_image_crop_thumbnail_width" value="<?php echo $modules_image_crop_thumbnail_width_default_value[0];?>" placeholder="<?php echo translate("");?>" min="0">

																		<div class="help-block"><?php echo translate("Image thumbnail width in pixel(s)"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_thumbnail_height_default_value = unserialize('a:1:{i:0;i:200;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_thumbnail_height" */
															var modules_image_crop_thumbnail_height_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_thumbnail_height_default_value)); ?>");
															</script>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_thumbnail_height"><?php echo translate("Image Thumbnail Height"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_thumbnail_height" type="number" id="modules_image_crop_thumbnail_height" name="modules_image_crop_thumbnail_height" value="<?php echo $modules_image_crop_thumbnail_height_default_value[0];?>" placeholder="<?php echo translate("");?>" min="0">

																		<div class="help-block"><?php echo translate("Image thumbnail height in pixel(s)"); ?></div>
																	</div>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="block block-bordered">
												<div class="block-header">
													<label><i class="si si-picture push-5-r"></i> <?php echo translate("Large Thumbnail"); ?></label>
												</div>
												<div class="block-content">
													<div class="row">
														<div class="col-md-12">

															<?php
															$modules_image_crop_large_default_value = unserialize('a:1:{i:0;i:1;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_large" */
															var modules_image_crop_large_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_large_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_large"><?php echo translate("Enable Image Large Thumbnail"); ?></label>
																	<div class="col-md-12">
																		<label class="css-input switch switch-lg switch-success">
																			<input id="modules_image_crop_large" name="modules_image_crop_large" class="modules_image_crop_large" type="checkbox" value="<?php echo $modules_image_crop_large_default_value[0];?>"><span></span> <?php echo translate(""); ?>
																		</label>
																		<div class="help-block"><?php echo translate("Enable image large thumbnail lets system create large thumbnail for images automatically"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_large_aspectratio_default_value = unserialize('a:1:{i:0;i:1;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_large_aspectratio" */
															var modules_image_crop_large_aspectratio_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_large_aspectratio_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_large_aspectratio"><?php echo translate("Keep Aspect Ratio for Image Large Thumbnail"); ?></label>
																	<div class="col-md-12">
																		<label class="css-input switch switch-lg switch-success">
																			<input id="modules_image_crop_large_aspectratio" name="modules_image_crop_large_aspectratio" class="modules_image_crop_large_aspectratio" type="checkbox" value="<?php echo $modules_image_crop_large_aspectratio_default_value[0];?>"><span></span> <?php echo translate(""); ?>
																		</label>
																		<div class="help-block"><?php echo translate("Keep aspect ratio for image large thumbnail withoutping to specific size"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_large_quality_default_value = unserialize('a:1:{i:0;i:75;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_large_quality" */
															var modules_image_crop_large_quality_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_large_quality_default_value)); ?>");
															</script>
															<div class="col-md-12">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_large_quality"><?php echo translate("Image Large Thumbnail Quality"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_large_quality" type="text" id="modules_image_crop_large_quality" name="modules_image_crop_large_quality" value="<?php echo $modules_image_crop_large_quality_default_value[0];?>" placeholder="<?php echo translate("");?>" data-min="0" data-max="100">

																		<div class="help-block"><?php echo translate("Image large thumbnail quality setting (75 is best for Search Engine Optimization)"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_large_width_default_value = unserialize('a:1:{i:0;i:400;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_large_width" */
															var modules_image_crop_large_width_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_large_width_default_value)); ?>");
															</script>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_large_width"><?php echo translate("Image Large Thumbnail Width"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_large_width" type="number" id="modules_image_crop_large_width" name="modules_image_crop_large_width" value="<?php echo $modules_image_crop_large_width_default_value[0];?>" placeholder="<?php echo translate("");?>" min="0">

																		<div class="help-block"><?php echo translate("Image large thumbnail width in pixel(s)"); ?></div>
																	</div>
																</div>
															</div>

															<?php
															$modules_image_crop_large_height_default_value = unserialize('a:1:{i:0;i:400;}');
															?>
															<script>
															/* JavaScript variable for dynamic default value "modules_image_crop_large_height" */
															var modules_image_crop_large_height_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_image_crop_large_height_default_value)); ?>");
															</script>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12" for="modules_image_crop_large_height"><?php echo translate("Image Large Thumbnail Height"); ?></label>
																	<div class="col-md-12">
																		<input class="form-control modules_image_crop_large_height" type="number" id="modules_image_crop_large_height" name="modules_image_crop_large_height" value="<?php echo $modules_image_crop_large_height_default_value[0];?>" placeholder="<?php echo translate("");?>" min="0">

																		<div class="help-block"><?php echo translate("Image large thumbnail height in pixel(s)"); ?></div>
																	</div>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="block block-bordered" >
							<div class="block-header">
								<label><i class="si si-layers"></i> <?php echo translate("Data Table Field"); ?> </label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12" id="modules_datatable_field_dynamic_form">
										<div class="block block-bordered modules_datatable_field" id="modules_datatable_field_0">
											<div class="block-header">
												<label><?php echo translate("Data Table Field"); ?> #1</label>
											</div>
											<div class="block-content">
												<div class="row">
													<?php
													$modules_datatable_field_display_default_value = unserialize("a:1:{i:0;i:1;}");
													?>
													<script>
													/* JavaScript variable for dynamic default value "modules_protected" */
													var modules_datatable_field_display_default_value = $.parseJSON("<?php echo addslashes(json_encode($modules_datatable_field_display_default_value)); ?>");
													</script>
													<div class="col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_0"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_0" name="modules_datatable_field_name_0" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?> <script> /* JavaScript variable for filter of dynamic selector "modules_datatable_field_name" */ var modules_datatable_field_name_filters = $.parseJSON("<?php echo addslashes(json_encode($modules_datatable_field_name_filters)); ?>"); </script> <?php $count_modules_datatable_field_name = count_modules_pages_parent_link_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?><?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_0" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_0"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_0" name="modules_datatable_field_display_0" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="block-footer">
								<div class="row">
									<div class="col-md-7">
										<?php echo translate(""); ?>
									</div>
									<div class="col-md-5 text-right">
										<button id="button-data-add-modules_datatable_field" class="btn btn-sm btn-primary" type="button">
										<i class="fa fa-plus" aria-hidden="true"></i> <?php echo translate("Add"); ?>
										</button>
										<button id="button-data-remove-modules_datatable_field" class="btn btn-sm btn-danger" type="button">
										<i class="fa fa-minus" aria-hidden="true"></i> <?php echo translate("Remove"); ?>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                       	<a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list">
                                        <div class="panel-heading">
                                            <div>
                                                <i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content");?></div>
                                        </div>
                                        </a>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="modules_order"><?php echo translate("Order"); ?></label>
							<div class="col-md-12">
								<input class="form-control" type="number" id="modules_order" name="modules_order" value="1" >
							</div>
						</div>
					</div>
					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-7">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="modules_activate" name="modules_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-5">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full bg-mama-green">
			</div>
		</div>
	</div>
</div>

<!-- sort table (begin) -->
<div class="row">

    <div id="module-sort" class="col-lg-12">
		
		<div class="block block-themed" id="sort-content">
            <div class="block-header">
                <ul class="block-options">
                    <li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-sort-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
                </ul>
                <h3 class="block-title"><?php echo translate("modules"); ?> <?php echo translate("Sort"); ?></h3>
            </div>

            <!-- Data Table -->
            <div class="block-content">
            	<ol class="sortable ui-sortable">
				</ol>
                
                <div class="row items-push">
					<div class="col-md-12 push-30-t push-30-b text-right">
						<button id="button-data-save-sort" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Save"); ?></button>
					</div>
				</div>
                
            </div>

        </div>
		
	</div>
	
</div>
<!-- sortle (end) -->

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<li>
                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
                    </li>
                    <li>
                        <button class="btn-sort" type="button" title="<?php echo translate("Sort"); ?>"><i class="si si-shuffle" data-toggle="tooltip"></i> <span class="hidden-xs">Sort</span></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("modules"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
            	<?php
            	$data_table_info = prepare_modules_table_defer($table_module_field);
                echo create_modules_table($data_table_info["values"], $table_module_field);
            	?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->


<?php
include("templates/".$configs["backend_template"]."/container-footer.php");
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
<link rel="stylesheet" href="plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->

<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/tinymce-jqerry/jquery.tinymce.min.js"></script>
<script src="plugins/tinymce-jqerry/tinymce.min.js"></script>
<script src="plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/nestedsort/jquery.mjs.nestedSortable.js"></script>
<!-- include: JavaScript (end) -->
<script>
<!-- initialization: JavaScript (begin) -->
/* JavaScript variable for AJAX function file */
var url = "<?php echo $module_function_page; ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;


function reset_modules_data() {

	$("#modules_id").val("");
	$("#modules_name").val("");
	$("#modules_description").val("");
	$(".modules_icon").first().prop('checked', true);
	$("#modules_category").val("");
	$("#modules_subject").val("");
	$(".modules_subject_icon").first().prop('checked', true);
	$("#modules_link").val("");
	$("#modules_function_link").val("");
	$("#modules_key").val("");
	$("#modules_db_name").val("");
	$("#modules_protected").prop("checked", false);
	$("#modules_individual_pages_parent_link").prop("checked", false);
	$.ajax({
		url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_main_language_data_all"
		},
		success: function(response) {
			$("#pages_id").empty();
			$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
			$("#pages_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#pages_id").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_title'] + " (ID: " + response.values[i]['pages_id'] + ", " + response.values[i]['languages_short_name'] + ")</option>");
				}
				$("#pages_id").val($("#pages_id option:first").val()).trigger("change");
			}
		}
	});
	$("#modules_pages_parent_link_field").val($("#modules_pages_parent_link_field option:first").val()).trigger("change");
	$("#modules_pages_template").val("");
	$("#modules_pages_template_settings").val("");
	$("#modules_image_crop_quality").val(modules_image_crop_quality_default_value[0]);
	$("#modules_image_crop_quality").data("ionRangeSlider").update({
		from: modules_image_crop_quality_default_value[0]
	});
	$("#modules_image_crop_thumbnail").prop("checked", false);
	$("#modules_image_crop_thumbnail_aspectratio").prop("checked", false);
	$("#modules_image_crop_thumbnail_quality").val(modules_image_crop_thumbnail_quality_default_value[0]);
	$("#modules_image_crop_thumbnail_quality").data("ionRangeSlider").update({
		from: modules_image_crop_thumbnail_quality_default_value[0]
	});
	$("#modules_image_crop_thumbnail_width").val(200);
	$("#modules_image_crop_thumbnail_height").val(200);
	$("#modules_image_crop_large").prop("checked", false);
	$("#modules_image_crop_large_aspectratio").prop("checked", false);
	$("#modules_image_crop_large_quality").val(modules_image_crop_large_quality_default_value[0]);
	$("#modules_image_crop_large_quality").data("ionRangeSlider").update({
		from: modules_image_crop_large_quality_default_value[0]
	});
	$("#modules_image_crop_large_width").val(400);
	$("#modules_image_crop_large_height").val(400);
	$("#modules_datatable_field_dynamic_form").empty();
	$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_0\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #1</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_0"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_0" name="modules_datatable_field_name_0" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_0" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_0"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_0" name="modules_datatable_field_display_0" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');

	$("#modules_datatable_field_name_0").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_table_field_data_dynamic_list",
			parameters: $("#modules_db_name").val(),
			filters: modules_datatable_field_name_filters
		},
		success: function(response) {
			$("#modules_datatable_field_name-check-loading_0").fadeOut("fast");
			$("#modules_datatable_field_name_0").empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_datatable_field_name_0").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_link'] + " (ID: " + response.values[i]['pages_id'] + ")</option>");
				}
				$("#modules_datatable_field_name_0").val($("#modules_datatable_field_name option:first").val()).trigger("change");
			}
		}
	});
	$("#modules_order").val(<?php echo $data_table_info["count_true"]; ?>);
	$("#modules_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#modules_action").val("");

}

function update_modules_data(target_id) {
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_pages_parent_link_field_data_dynamic_list",
			parameters: modules_pages_parent_link_field_filters
		},
		success: function(response) {
			var modules_pages_parent_link_field = $("#modules_pages_parent_link_field").val();
			$("#modules_pages_parent_link_field").empty();
			$("#modules_pages_parent_link_field").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
			$("#modules_pages_parent_link_field").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_pages_parent_link_field").append("<option value=\"" + response.values[i]['pages_link'] + "\">" + response.values[i]['pages_title'] + " (ID: " + response.values[i]['pages_link'] + ")</option>");
				}
				$("#modules_pages_parent_link_field").val(modules_pages_parent_link_field);
			}
		}
	});
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_pages_parent_link_field_data_dynamic_list",
			parameters: modules_datatable_field_name_filters
		},
		success: function(response) {
			var modules_datatable_field_name = $("#modules_datatable_field_name").val();
			$("#modules_datatable_field_name").empty();
			$("#modules_datatable_field_name").append("<option value=\"\"><?php echo translate(""); ?></option>");
			$("#modules_datatable_field_name").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_datatable_field_name").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_link'] + " (ID: " + response.values[i]['pages_id'] + ")</option>");
				}
				$("#modules_datatable_field_name").val(modules_datatable_field_name);
			}
		}
	});
	$.ajax({
		url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_main_language_data_all"
		},
		success: function(response) {
			var pages_id = $("#pages_id").val();
			$("#pages_id").empty();
			$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
			$("#pages_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#pages_id").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_title'] + " (ID: " + response.values[i]['pages_id'] + ", " + response.values[i]['languages_short_name'] + ")</option>");
				}
				$("#pages_id").val(pages_id);
			}
		}
	});
}

function submit_modules_data() {
	
	

	$("#form-content").addClass("block-opt-refresh");

    document.getElementById("button-data-submit").disable = true;

    if ($("#modules_id").val() == "") {
		var method = "create_modules_data";
	} else {
		var method = "update_modules_data";
	}

	if ($("#modules_activate").is(":checked") === true){
		var modules_activate = "1";
	} else {
		var modules_activate = "0";
	}
	var modules_icon = "";
	$(".modules_icon").each(function (i) {
		if ($(this).is(":checked")) {
			modules_icon = $(this).val();
		}
	});
	var modules_subject_icon = "";
	$(".modules_subject_icon").each(function (i) {
		if ($(this).is(":checked")) {
			modules_subject_icon = $(this).val();
		}
	});
		if ($("#modules_protected").is(":checked") === true){
			var modules_protected = modules_protected_default_value[0];
		} else {
			var modules_protected = "";
		}
		if ($("#modules_individual_pages_parent_link").is(":checked") === true){
			var modules_individual_pages_parent_link = modules_individual_pages_parent_link_default_value[0];
		} else {
			var modules_individual_pages_parent_link = "";
		}
		if ($("#modules_image_crop_thumbnail").is(":checked") === true){
			var modules_image_crop_thumbnail = modules_image_crop_thumbnail_default_value[0];
		} else {
			var modules_image_crop_thumbnail = "";
		}
		if ($("#modules_image_crop_thumbnail_aspectratio").is(":checked") === true){
			var modules_image_crop_thumbnail_aspectratio = modules_image_crop_thumbnail_aspectratio_default_value[0];
		} else {
			var modules_image_crop_thumbnail_aspectratio = "";
		}
		if ($("#modules_image_crop_large").is(":checked") === true){
			var modules_image_crop_large = modules_image_crop_large_default_value[0];
		} else {
			var modules_image_crop_large = "";
		}
		if ($("#modules_image_crop_large_aspectratio").is(":checked") === true){
			var modules_image_crop_large_aspectratio = modules_image_crop_large_aspectratio_default_value[0];
		} else {
			var modules_image_crop_large_aspectratio = "";
		}
	var modules_datatable_field = new Array();
	$(".modules_datatable_field").each(function (i) {
		if ($("#modules_datatable_field_display_" + i).is(":checked") === true){
			var modules_datatable_field_display = modules_datatable_field_display_default_value[0];
		} else {
			var modules_datatable_field_display = "";
		}
		modules_datatable_field[i]   = {
		"modules_datatable_field_name": $(".modules_datatable_field_name:eq(" + i + ")").select2("val"),
		"modules_datatable_field_display": modules_datatable_field_display,
		}
	});


	var form_values   = {
		"modules_id": $("#modules_id").val(),
		"modules_action": $("#modules_action").val(),
		"modules_name": $("#modules_name").val(),
		"modules_description": $("#modules_description").val(),
		"modules_icon": modules_icon,
		"modules_category": $("#modules_category").val(),
		"modules_subject": $("#modules_subject").val(),
		"modules_subject_icon": modules_subject_icon,
		"modules_link": $("#modules_link").val(),
		"modules_function_link": $("#modules_function_link").val(),
		"modules_key": $("#modules_key").val(),
		"modules_db_name": $("#modules_db_name").val(),
		"modules_protected": modules_protected,
		"modules_individual_pages_parent_link": modules_individual_pages_parent_link,
		"modules_pages_parent_link_field": $("#modules_pages_parent_link_field").select2("val"),
		"modules_pages_template": $("#modules_pages_template").val(),
		"modules_pages_template_settings": $("#modules_pages_template_settings").val().replace(/[\n\r]/g, "<br />"),
		"modules_image_crop_quality": $("#modules_image_crop_quality").val(),
		"modules_image_crop_thumbnail": modules_image_crop_thumbnail,
		"modules_image_crop_thumbnail_aspectratio": modules_image_crop_thumbnail_aspectratio,
		"modules_image_crop_thumbnail_quality": $("#modules_image_crop_thumbnail_quality").val(),
		"modules_image_crop_thumbnail_width": $("#modules_image_crop_thumbnail_width").val(),
		"modules_image_crop_thumbnail_height": $("#modules_image_crop_thumbnail_height").val(),
		"modules_image_crop_large": modules_image_crop_large,
		"modules_image_crop_large_aspectratio": modules_image_crop_large_aspectratio,
		"modules_image_crop_large_quality": $("#modules_image_crop_large_quality").val(),
		"modules_image_crop_large_width": $("#modules_image_crop_large_width").val(),
		"modules_image_crop_large_height": $("#modules_image_crop_large_height").val(),
		"modules_datatable_field": modules_datatable_field,
		"modules_order": $("#modules_order").val(),
		"pages_id": $("#pages_id").val(),

		"users_id": $("#users_id").val(),
		"users_username": $("#users_username").val(),
		"users_name": $("#users_name").val(),
		"users_last_name": $("#users_last_name").val(),
		"modules_activate": modules_activate,
	}
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				if (method == "create_modules_data") {
					create_modules_table_row(response.values);
					show_modules_modal_response(response.status, response.message);
					reset_modules_data();
					$("#form-content").slideUp("fast");
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");

					var limit = $("#datatable_length_selector").val();
					var page = get_url_param().page;
					if (page == null) {
						page = 1;
					}
					var search_text = $("#datatable_search").val();
					var published = get_url_param().published;
					var from = get_url_param().from;
					var to = get_url_param().to;
					if (limit != null) {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
							}
						}
					} else {
						if (search_text != "" && search_text != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
						} else {
							if (published != null || from != null || to != null) {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
							} else {
								window.history.pushState("", "", "<?php echo $module_page_link; ?>");
							}
						}
					}
					
					$("#button-data-submit").prop("disabled", false);
				} else if (method == "update_modules_data") {
					update_modules_table_row(response.values);
				    show_modules_modal_response(response.status, response.message);
					update_modules_data($("#modules_id").val());
					edit_modules_open($("#modules_id").val());
					$("html, body").animate({
							scrollTop: "0px"
					}, "fast");
					$("#form-content").removeClass("block-opt-refresh");
					$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
				$("#button-data-submit").prop("disabled", false);
			}

			$("#form-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		}
	});

}

function delete_modules_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_modules_data",
			parameters: target_id
		},
		success: function(response) {
			
			if (response.status == true) {
				delete_modules_table_row(target_id);
				if ($("#modules_id").val() == target_id) {
					reset_modules_data();
					form_modules_close();
				}
                show_modules_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus);
            document.getElementById("button-data-submit").disable = false;
		}
	});

}

function form_modules_close() {

	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}

}
	
function create_modules_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add")." ".$title; ?>");

	
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

    var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_modules_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#modules_action").val("create");

}
	
function edit_modules_open(target_id) {

	
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_modules_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_by_id",
			parameters: target_id
		},
		success: function(response) {
			
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#modules_id").val(response.values["modules_id"]);
				$("#modules_name").val(response.values["modules_name"]);
				$("#modules_description").val(response.values["modules_description"]);
				$(".modules_icon").each(function (i) {
					if ($(this).val() == response.values['modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_category").val(response.values["modules_category"]);
				$("#modules_subject").val(response.values["modules_subject"]);
				$(".modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_link").val(response.values["modules_link"]);
				$("#modules_function_link").val(response.values["modules_function_link"]);
				$("#modules_key").val(response.values["modules_key"]);
				$("#modules_db_name").val(response.values["modules_db_name"]);
				if(response.values["modules_protected"] == modules_protected_default_value[0]){
					$("#modules_protected").prop("checked", true);
				} else {
					$("#modules_protected").prop("checked", false);
				}
				$.ajax({
					url: url,
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_pages_parent_link_field_data_dynamic_list",
						parameters: response.values["modules_db_name"]
					},
					success: function(response_modules_pages_parent_link_field) {
						$("#modules_pages_parent_link_field-check-loading").fadeOut("fast");
						var modules_pages_parent_link_field = $("#modules_pages_parent_link_field").val();
						$("#modules_pages_parent_link_field").empty();
						$("#modules_pages_parent_link_field").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
						$("#modules_pages_parent_link_field").append("<option value=\"\" disabled>-</option>");
						if (response_modules_pages_parent_link_field.values != null) {
							for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
								$("#modules_pages_parent_link_field").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
								if (response_modules_pages_parent_link_field.values.length - i == 1) {
									if (response.values["modules_db_name"] != "") {
										$("#modules_pages_parent_link_field").select2("val", response.values["modules_pages_parent_link_field"]);
									} else {
										$("#modules_pages_parent_link_field").select2("val", "");
									}
								}
							}
						}
					}
				});
				if(response.values["modules_individual_pages_parent_link"] == modules_individual_pages_parent_link_default_value[0]){
					$("#modules_individual_pages_parent_link").prop("checked", true);
				} else {
					$("#modules_individual_pages_parent_link").prop("checked", false);
				}
				$.ajax({
					url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all"
					},
					success: function(response_pages_id) {
						$("#pages_id").empty();
						$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
						$("#pages_id").append("<option value=\"\" disabled>-</option>");
						if (response_pages_id.values != null) {
							for(var i = 0; i < response_pages_id.values.length; i++) {
								$("#pages_id").append("<option value=\"" + response_pages_id.values[i]['pages_id'] + "\">" + response_pages_id.values[i]['pages_title'] + " (ID: " + response_pages_id.values[i]['pages_id'] + ", " + response_pages_id.values[i]['languages_short_name'] + ")</option>");
								if (response_pages_id.values.length - i == 1) {
									if (response.values["pages_id"] != "") {
										$("#pages_id").select2("val", response.values["pages_id"]);
									} else {
										$("#pages_id").select2("val", "");
									}
								}
							}
						}
					}
				});
				$("#pages_id").select2("val", response.values["pages_id"]);
				$("#modules_pages_parent_link_field").select2("val", response.values["modules_pages_parent_link_field"]);
				$("#modules_pages_template").val(response.values["modules_pages_template"]);
				$("#modules_pages_template_settings").val(response.values["modules_pages_template_settings"]);
				$("#modules_image_crop_quality").val(response.values["modules_image_crop_quality"]);
				$("#modules_image_crop_quality").data("ionRangeSlider").update({
					from: response.values["modules_image_crop_quality"]
				});
				if(response.values["modules_image_crop_thumbnail"] == modules_image_crop_thumbnail_default_value[0]){
					$("#modules_image_crop_thumbnail").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail").prop("checked", false);
				}
				if(response.values["modules_image_crop_thumbnail_aspectratio"] == modules_image_crop_thumbnail_aspectratio_default_value[0]){
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_thumbnail_quality").val(response.values["modules_image_crop_thumbnail_quality"]);
				$("#modules_image_crop_thumbnail_quality").data("ionRangeSlider").update({
					from: response.values["modules_image_crop_thumbnail_quality"]
				});
				$("#modules_image_crop_thumbnail_width").val(response.values["modules_image_crop_thumbnail_width"]);
				$("#modules_image_crop_thumbnail_height").val(response.values["modules_image_crop_thumbnail_height"]);
				if(response.values["modules_image_crop_large"] == modules_image_crop_large_default_value[0]){
					$("#modules_image_crop_large").prop("checked", true);
				} else {
					$("#modules_image_crop_large").prop("checked", false);
				}
				if(response.values["modules_image_crop_large_aspectratio"] == modules_image_crop_large_aspectratio_default_value[0]){
					$("#modules_image_crop_large_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_large_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_large_quality").val(response.values["modules_image_crop_large_quality"]);
				$("#modules_image_crop_large_quality").data("ionRangeSlider").update({
					from: response.values["modules_image_crop_large_quality"]
				});
				$("#modules_image_crop_large_width").val(response.values["modules_image_crop_large_width"]);
				$("#modules_image_crop_large_height").val(response.values["modules_image_crop_large_height"]);
				
				$("#modules_datatable_field_dynamic_form").empty();
				if (response.values["modules_datatable_field"] != null) {
					
					if (response.values["modules_datatable_field"].length > 0) {
						
						for (var i = 0; i < response.values["modules_datatable_field"].length; i++) {
							var i_number = i + 1;
							$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
							$("#modules_datatable_field_name_" + i).select2();
							
							$.ajax({
								url: url,
								type: "POST",
								cache: false,
								dataType: "json",
								data: {
									method: "get_modules_data_table_field_data_dynamic_list",
									parameters: $("#modules_db_name").val(),
									target: i
								},
								success: function(response_modules_pages_parent_link_field) {
									if (response_modules_pages_parent_link_field.values != null) {
										for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
											$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - i == 1) {
												$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).select2("val", response.values["modules_datatable_field"][response_modules_pages_parent_link_field.target]["modules_datatable_field_name"]);
											}
										}
									} else {
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).empty();
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
									}
								}
							});
							$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
							if (i != response.values["modules_datatable_field"].length) {
								if(response.values["modules_datatable_field"][i]["modules_datatable_field_display"] == modules_datatable_field_display_default_value[0]){
									$("#modules_datatable_field_display_" + i).prop("checked", true);
								} else {
									$("#modules_datatable_field_display_" + i).prop("checked", false);
								}
							}
							
						}
					} else {
						$("#modules_datatable_field_dynamic_form").empty();
						$.ajax({
							url: url,
							type: "POST",
							cache: false,
							dataType: "json",
							data: {
								method: "get_modules_data_table_field_data_dynamic_list",
								parameters: $("#modules_db_name").val(),
							},
							success: function(response_modules_pages_parent_link_field) {

								if (response_modules_pages_parent_link_field.values != null) {
									
									for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {

										var i_number = i + 1;
										$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
										$("#modules_datatable_field_name_" + i).select2();

										$("#modules_datatable_field_name_" + i).empty();
										$("#modules_datatable_field_name_" + i).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
										$("#modules_datatable_field_name_" + i).append("<option value=\"\" disabled>-</option>");
										for(var j = 0; j < response_modules_pages_parent_link_field.values.length; j++) {
											$("#modules_datatable_field_name_" + i).append("<option value=\"" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - j == 1) {
												
												$("#modules_datatable_field_name_" + i).select2("val", response_modules_pages_parent_link_field.values[i]['COLUMN_NAME']);
											}

										}
										
										$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
									}
								}
							}
						});
						
					}
				}
				$("#button-data-add-modules_datatable_field").unbind();
				$("#button-data-add-modules_datatable_field").click(function () {
					$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + $(".modules_datatable_field").length + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + parseInt($(".modules_datatable_field").length + 1) + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + $(".modules_datatable_field").length + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?><?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + $(".modules_datatable_field").length + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + $(".modules_datatable_field").length + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2();
					$.ajax({
						url: url,
						type: "POST",
						cache: false,
						dataType: "json",
						data: {
							method: "get_modules_data_table_field_data_dynamic_list",
							parameters: $("#modules_db_name").val()
						},
						success: function(response_modules_pages_parent_link_field) {
							$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
							if (response_modules_pages_parent_link_field.values != null) {
								for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
									$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
									if (response_modules_pages_parent_link_field.values.length - i == 1) {
										$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", "");
									}
								}
							} else {
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
							}
						}
					});
					$("#modules_datatable_field_name-check-loading_" + parseInt($(".modules_datatable_field").length - 1)).fadeOut("fast");
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", response.values["modules_datatable_field_name"]);
				});


				$("#modules_order").val(response.values["modules_order"]);

				if(response.values["modules_activate"] == "1" ){
					$("#modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["modules_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

				$("#modules_action").val("edit");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&modules_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&modules_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {

					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_modules_modal_prompt($(this).attr("data-content"), "revision_modules_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function copy_modules_open(target_id) {

	$("#revision").hide();

	
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	reset_modules_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_by_id",
			parameters: target_id
		},
		success: function(response) {
			
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy")." ".$title; ?>");

				$("#modules_id").val("");
				$("#modules_name").val(response.values["modules_name"]);
				$("#modules_description").val(response.values["modules_description"]);
				$(".modules_icon").each(function (i) {
					if ($(this).val() == response.values['modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_category").val(response.values["modules_category"]);
				$("#modules_subject").val(response.values["modules_subject"]);
				$(".modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_link").val(response.values["modules_link"]);
				$("#modules_function_link").val(response.values["modules_function_link"]);
				$("#modules_key").val(response.values["modules_key"]);
				$("#modules_db_name").val(response.values["modules_db_name"]);
				if(response.values["modules_protected"] == modules_protected_default_value[0]){
					$("#modules_protected").prop("checked", true);
				} else {
					$("#modules_protected").prop("checked", false);
				}
				if(response.values["modules_individual_pages_parent_link"] == modules_individual_pages_parent_link_default_value[0]){
					$("#modules_individual_pages_parent_link").prop("checked", true);
				} else {
					$("#modules_individual_pages_parent_link").prop("checked", false);
				}
				$.ajax({
					url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all"
					},
					success: function(response_pages_id) {
						$("#pages_id").empty();
						$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
						$("#pages_id").append("<option value=\"\" disabled>-</option>");
						if (response_pages_id.values != null) {
							for(var i = 0; i < response_pages_id.values.length; i++) {
								$("#pages_id").append("<option value=\"" + response_pages_id.values[i]['pages_id'] + "\">" + response_pages_id.values[i]['pages_title'] + " (ID: " + response_pages_id.values[i]['pages_id'] + ", " + response_pages_id.values[i]['languages_short_name'] + ")</option>");
								if (response_pages_id.values.length - i == 1) {
									if (response.values["pages_id"] != "") {
										$("#pages_id").select2("val", response.values["pages_id"]);
									} else {
										$("#pages_id").select2("val", "");
									}
								}
							}
						}
					}
				});
				$("#modules_pages_parent_link_field").select2("val", response.values["modules_pages_parent_link_field"]);
				$("#modules_pages_template").val(response.values["modules_pages_template"]);
				$("#modules_pages_template_settings").val(response.values["modules_pages_template_settings"]);
				$("#modules_image_crop_quality").val(response.values["modules_image_crop_quality"]);
				$("#modules_image_crop_quality").data("ionRangeSlider").update({
					from: modules_image_crop_quality_default_value[0]
				});
				if(response.values["modules_image_crop_thumbnail"] == modules_image_crop_thumbnail_default_value[0]){
					$("#modules_image_crop_thumbnail").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail").prop("checked", false);
				}
				if(response.values["modules_image_crop_thumbnail_aspectratio"] == modules_image_crop_thumbnail_aspectratio_default_value[0]){
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_thumbnail_quality").val(response.values["modules_image_crop_thumbnail_quality"]);
				$("#modules_image_crop_thumbnail_quality").data("ionRangeSlider").update({
					from: modules_image_crop_thumbnail_quality_default_value[0]
				});
				$("#modules_image_crop_thumbnail_width").val(response.values["modules_image_crop_thumbnail_width"]);
				$("#modules_image_crop_thumbnail_height").val(response.values["modules_image_crop_thumbnail_height"]);
				if(response.values["modules_image_crop_large"] == modules_image_crop_large_default_value[0]){
					$("#modules_image_crop_large").prop("checked", true);
				} else {
					$("#modules_image_crop_large").prop("checked", false);
				}
				if(response.values["modules_image_crop_large_aspectratio"] == modules_image_crop_large_aspectratio_default_value[0]){
					$("#modules_image_crop_large_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_large_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_large_quality").val(response.values["modules_image_crop_large_quality"]);
				$("#modules_image_crop_large_quality").data("ionRangeSlider").update({
					from: modules_image_crop_large_quality_default_value[0]
				});
				$("#modules_image_crop_large_width").val(response.values["modules_image_crop_large_width"]);
				$("#modules_image_crop_large_height").val(response.values["modules_image_crop_large_height"]);
				$("#modules_datatable_field_dynamic_form").empty();
				if (response.values["modules_datatable_field"] != null) {
					$("#modules_datatable_field_dynamic_form").empty();
					if (response.values["modules_datatable_field"].length > 0) {
						
						for (var i = 0; i < response.values["modules_datatable_field"].length; i++) {
							var i_number = i + 1;
							$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
							$("#modules_datatable_field_name_" + i).select2();
							
							$.ajax({
								url: url,
								type: "POST",
								cache: false,
								dataType: "json",
								data: {
									method: "get_modules_data_table_field_data_dynamic_list",
									parameters: $("#modules_db_name").val(),
									target: i
								},
								success: function(response_modules_pages_parent_link_field) {
									if (response_modules_pages_parent_link_field.values != null) {
										for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
											$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - i == 1) {
												$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).select2("val", response.values["modules_datatable_field"][response_modules_pages_parent_link_field.target]["modules_datatable_field_name"]);
											}
										}
									} else {
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).empty();
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
									}
								}
							});
							$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
							if (i != response.values["modules_datatable_field"].length) {
								if(response.values["modules_datatable_field"][i]["modules_datatable_field_display"] == modules_datatable_field_display_default_value[0]){
									$("#modules_datatable_field_display_" + i).prop("checked", true);
								} else {
									$("#modules_datatable_field_display_" + i).prop("checked", false);
								}
							}
							
						}
					} else {
						$("#modules_datatable_field_dynamic_form").empty();
						$.ajax({
							url: url,
							type: "POST",
							cache: false,
							dataType: "json",
							data: {
								method: "get_modules_data_table_field_data_dynamic_list",
								parameters: $("#modules_db_name").val(),
							},
							success: function(response_modules_pages_parent_link_field) {

								if (response_modules_pages_parent_link_field.values != null) {
									
									for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {

										var i_number = i + 1;
										$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
										$("#modules_datatable_field_name_" + i).select2();

										$("#modules_datatable_field_name_" + i).empty();
										$("#modules_datatable_field_name_" + i).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
										$("#modules_datatable_field_name_" + i).append("<option value=\"\" disabled>-</option>");
										for(var j = 0; j < response_modules_pages_parent_link_field.values.length; j++) {
											$("#modules_datatable_field_name_" + i).append("<option value=\"" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - j == 1) {
												
												$("#modules_datatable_field_name_" + i).select2("val", response_modules_pages_parent_link_field.values[i]['COLUMN_NAME']);
											}

										}
										
										$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
									}
								}
							}
						});
						
					}
				}
				$("#button-data-add-modules_datatable_field").unbind();
				$("#button-data-add-modules_datatable_field").click(function () {
					$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + $(".modules_datatable_field").length + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + parseInt($(".modules_datatable_field").length + 1) + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + $(".modules_datatable_field").length + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?><?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + $(".modules_datatable_field").length + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + $(".modules_datatable_field").length + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2();
					$.ajax({
						url: url,
						type: "POST",
						cache: false,
						dataType: "json",
						data: {
							method: "get_modules_data_table_field_data_dynamic_list",
							parameters: $("#modules_db_name").val()
						},
						success: function(response_modules_pages_parent_link_field) {
							$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
							if (response_modules_pages_parent_link_field.values != null) {
								for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
									$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
									if (response_modules_pages_parent_link_field.values.length - i == 1) {
										$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", "");
									}
								}
							} else {
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
							}
						}
					});
					$("#modules_datatable_field_name-check-loading_" + parseInt($(".modules_datatable_field").length - 1)).fadeOut("fast");
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", response.values["modules_datatable_field_name"]);
				});

				$("#modules_order").val(response.values["modules_order"]);
				if(response.values["modules_activate"] == "1" ){
					$("#modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");

				$("#modules_action").val("copy");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&modules_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&modules_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&modules_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&modules_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function view_modules_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_modules_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_modules_modal_response(response.status, response.html);

				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_modules_open(target_id) {

	
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_modules_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {
			
			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit")." ".$title; ?>");

				$("#modules_id").val(response.values["modules_id"]);
				$("#modules_name").val(response.values["modules_name"]);
				$("#modules_description").val(response.values["modules_description"]);
				$(".modules_icon").each(function (i) {
					if ($(this).val() == response.values['modules_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_category").val(response.values["modules_category"]);
				$("#modules_subject").val(response.values["modules_subject"]);
				$(".modules_subject_icon").each(function (i) {
					if ($(this).val() == response.values['modules_subject_icon']) {
						$(this).prop("checked", true);
					} else {
						$(this).prop("checked", false);
					}
				});
				$("#modules_link").val(response.values["modules_link"]);
				$("#modules_function_link").val(response.values["modules_function_link"]);
				$("#modules_key").val(response.values["modules_key"]);
				$("#modules_db_name").val(response.values["modules_db_name"]);
				if(response.values["modules_protected"] == modules_protected_default_value[0]){
					$("#modules_protected").prop("checked", true);
				} else {
					$("#modules_protected").prop("checked", false);
				}
				if(response.values["modules_individual_pages_parent_link"] == modules_individual_pages_parent_link_default_value[0]){
					$("#modules_individual_pages_parent_link").prop("checked", true);
				} else {
					$("#modules_individual_pages_parent_link").prop("checked", false);
				}
				$.ajax({
					url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_pages_main_language_data_all"
					},
					success: function(response_pages_id) {
						$("#pages_id").empty();
						$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
						$("#pages_id").append("<option value=\"\" disabled>-</option>");
						if (response_pages_id.values != null) {
							for(var i = 0; i < response_pages_id.values.length; i++) {
								$("#pages_id").append("<option value=\"" + response_pages_id.values[i]['pages_id'] + "\">" + response_pages_id.values[i]['pages_title'] + " (ID: " + response_pages_id.values[i]['pages_id'] + ", " + response_pages_id.values[i]['languages_short_name'] + ")</option>");
								if (response_pages_id.values.length - i == 1) {
									if (response.values["pages_id"] != "") {
										$("#pages_id").select2("val", response.values["pages_id"]);
									} else {
										$("#pages_id").select2("val", "");
									}
								}
							}
						}
					}
				});
				$("#modules_pages_parent_link_field").select2("val", response.values["modules_pages_parent_link_field"]);
				$("#modules_pages_template").val(response.values["modules_pages_template"]);
				$("#modules_pages_template_settings").val(response.values["modules_pages_template_settings"]);
				$("#modules_image_crop_quality").val(response.values["modules_image_crop_quality"]);
				$("#modules_image_crop_quality").data("ionRangeSlider").update({
					from: modules_image_crop_quality_default_value[0]
				});
				if(response.values["modules_image_crop_thumbnail"] == modules_image_crop_thumbnail_default_value[0]){
					$("#modules_image_crop_thumbnail").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail").prop("checked", false);
				}
				if(response.values["modules_image_crop_thumbnail_aspectratio"] == modules_image_crop_thumbnail_aspectratio_default_value[0]){
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_thumbnail_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_thumbnail_quality").val(response.values["modules_image_crop_thumbnail_quality"]);
				$("#modules_image_crop_thumbnail_quality").data("ionRangeSlider").update({
					from: modules_image_crop_thumbnail_quality_default_value[0]
				});
				$("#modules_image_crop_thumbnail_width").val(response.values["modules_image_crop_thumbnail_width"]);
				$("#modules_image_crop_thumbnail_height").val(response.values["modules_image_crop_thumbnail_height"]);
				if(response.values["modules_image_crop_large"] == modules_image_crop_large_default_value[0]){
					$("#modules_image_crop_large").prop("checked", true);
				} else {
					$("#modules_image_crop_large").prop("checked", false);
				}
				if(response.values["modules_image_crop_large_aspectratio"] == modules_image_crop_large_aspectratio_default_value[0]){
					$("#modules_image_crop_large_aspectratio").prop("checked", true);
				} else {
					$("#modules_image_crop_large_aspectratio").prop("checked", false);
				}
				$("#modules_image_crop_large_quality").val(response.values["modules_image_crop_large_quality"]);
				$("#modules_image_crop_large_quality").data("ionRangeSlider").update({
					from: modules_image_crop_large_quality_default_value[0]
				});
				$("#modules_image_crop_large_width").val(response.values["modules_image_crop_large_width"]);
				$("#modules_image_crop_large_height").val(response.values["modules_image_crop_large_height"]);
				$("#modules_datatable_field_dynamic_form").empty();
				if (response.values["modules_datatable_field"] != null) {
					
					if (response.values["modules_datatable_field"].length > 0) {
						
						for (var i = 0; i < response.values["modules_datatable_field"].length; i++) {
							var i_number = i + 1;
							$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
							$("#modules_datatable_field_name_" + i).select2();
							
							$.ajax({
								url: url,
								type: "POST",
								cache: false,
								dataType: "json",
								data: {
									method: "get_modules_data_table_field_data_dynamic_list",
									parameters: $("#modules_db_name").val(),
									target: i
								},
								success: function(response_modules_pages_parent_link_field) {
									if (response_modules_pages_parent_link_field.values != null) {
										for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
											$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - i == 1) {
												$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).select2("val", response.values["modules_datatable_field"][response_modules_pages_parent_link_field.target]["modules_datatable_field_name"]);
											}
										}
									} else {
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).empty();
										$("#modules_datatable_field_name_" + response_modules_pages_parent_link_field.target).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
									}
								}
							});
							$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
							if (i != response.values["modules_datatable_field"].length) {
								if(response.values["modules_datatable_field"][i]["modules_datatable_field_display"] == modules_datatable_field_display_default_value[0]){
									$("#modules_datatable_field_display_" + i).prop("checked", true);
								} else {
									$("#modules_datatable_field_display_" + i).prop("checked", false);
								}
							}
							
						}
					} else {
						$("#modules_datatable_field_dynamic_form").empty();
						$.ajax({
							url: url,
							type: "POST",
							cache: false,
							dataType: "json",
							data: {
								method: "get_modules_data_table_field_data_dynamic_list",
								parameters: $("#modules_db_name").val(),
							},
							success: function(response_modules_pages_parent_link_field) {

								if (response_modules_pages_parent_link_field.values != null) {
									
									for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {

										var i_number = i + 1;
										$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + i + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + i_number + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + i + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + i + '" name="modules_datatable_field_name_' + i + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?> <?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + i + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + i + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + i + '" name="modules_datatable_field_display_' + i + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
										$("#modules_datatable_field_name_" + i).select2();

										$("#modules_datatable_field_name_" + i).empty();
										$("#modules_datatable_field_name_" + i).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
										$("#modules_datatable_field_name_" + i).append("<option value=\"\" disabled>-</option>");
										for(var j = 0; j < response_modules_pages_parent_link_field.values.length; j++) {
											$("#modules_datatable_field_name_" + i).append("<option value=\"" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[j]['COLUMN_NAME'] + ")</option>");
											if (response_modules_pages_parent_link_field.values.length - j == 1) {
												
												$("#modules_datatable_field_name_" + i).select2("val", response_modules_pages_parent_link_field.values[i]['COLUMN_NAME']);
											}

										}
										
										$("#modules_datatable_field_name-check-loading_" + i).fadeOut("fast");
									}
								}
							}
						});
						
					}
				}
				$("#button-data-add-modules_datatable_field").unbind();
				$("#button-data-add-modules_datatable_field").click(function () {
					$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + $(".modules_datatable_field").length + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + parseInt($(".modules_datatable_field").length + 1) + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + $(".modules_datatable_field").length + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command) + 1; if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?><?php $modules_datatable_field_name = get_modules_data_table_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_id']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['pages_link'])." (ID: ".$modules_datatable_field_name[$i]['pages_id'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + $(".modules_datatable_field").length + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + $(".modules_datatable_field").length + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2();
					$.ajax({
						url: url,
						type: "POST",
						cache: false,
						dataType: "json",
						data: {
							method: "get_modules_data_table_field_data_dynamic_list",
							parameters: $("#modules_db_name").val()
						},
						success: function(response_modules_pages_parent_link_field) {
							$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
							if (response_modules_pages_parent_link_field.values != null) {
								for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
									$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
									if (response_modules_pages_parent_link_field.values.length - i == 1) {
										$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", "");
									}
								}
							} else {
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
								$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
							}
						}
					});
					$("#modules_datatable_field_name-check-loading_" + parseInt($(".modules_datatable_field").length - 1)).fadeOut("fast");
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2("val", response.values["modules_datatable_field_name"]);
				});

				$("#modules_order").val(response.values["modules_order"]);
				if(response.values["modules_activate"] == "1" ){
					$("#modules_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#modules_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["modules_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_modules_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}

function sort_modules_open() {

	$("#sort-content").css("visibility", "visible");
	$("#sort-content").fadeTo("slow", 1);
	$("#sort-content").removeClass("block-opt-hidden");
	$("#sort-content").fadeIn("slow").slideDown("fast");
	$("#sort-content").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_sort_modules",
			table_module_field: table_module_field
		},
		success: function(response) {
			
			$("#module-table").slideUp("fast");
			$("#sort-content").slideDown("fast");
			$("#module-sort").find(".sortable").append(response.html);
			
			$("ol.sortable").nestedSortable({
				forcePlaceholderSize: true,
				handle: "div",
				helper:	"clone",
				items: "li",
				opacity: .6,
				placeholder: "placeholder",
				revert: 250,
				tabSize: 25,
				tolerance: "pointer",
				toleranceElement: "> div",
				maxLevels: 1,

				isTree: true,
				expandOnHover: 700,
				startCollapsed: true
			});

			$(".disclose").on("click", function() {
				$(this).closest("li").toggleClass("mjs-nestedSortable-collapsed").toggleClass("mjs-nestedSortable-expanded");
			});
			
			$("#sort-content").removeClass("block-opt-refresh");
			
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-sort").offset().top - 160 + "px"
    }, "fast");

}
	
function save_sort_modules () {
	
	$("#sort-content").addClass("block-opt-refresh");
	
	$("#button-data-submit").prop("disabled", true);
	
	arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
	
	$.ajax({
		type: "POST",
		url: url,
		data: {
			"method" : "sort_modules", 
			"parameters" : arraied 
		},
		dataType: "json", 
		success: function(response) {
			if (response.status == true) {
				$("#module-sort").find(".sortable").empty();
				show_modules_modal_response(response.status, response.message);
				$("#sort-content").slideUp("fast");
				$("#module-table").slideDown("fast");
			} else {
				show_modules_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
			}
			$("#button-data-submit").prop("disabled", false);
			$("#sort-content").removeClass("block-opt-refresh");
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_modules_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#sort-content").removeClass("block-opt-refresh");
            document.getElementById("button-data-submit").disable = false;
		}
	});
	
	
}
	
function sort_modules_close() {

	$("#sort-content").fadeTo("fast", 0);
	$("#sort-content").slideUp("fast").fadeOut("slow");
	$("#module-table").slideDown("fast");

}
	
function validate_modules_description_tinymce (element, content, options){

    if (content == "" || content == null) {
		var error_text = "<?php echo translate("Description") . " " . translate("is required"); ?>";
    } else if (content.length <= options.min_value && content.length <= options.max_value) {
		var error_text = "<?php echo translate("Description") . " " . translate("is shorter than minimum length of strings"); ?> " + options.min_value;
    } else if (content.length >= options.max_value) {
		if ($("#" + element.id).parent().find(".bootstrap-maxlength").length <= 0) {
        	$("#" + element.id).parent().append("<span class=\"bootstrap-maxlength label-danger label\" style=\"display: block; left: 50%; position: absolute; white-sapce: nowrap; z-index: 1099;\">" + content.length + "/" + options.max_value + "<span>");
		} else {
			$("#" + element.id).parent().find(".bootstrap-maxlength").empty();
			$("#" + element.id).parent().find(".bootstrap-maxlength").append(content.length + "/" + options.max_value);
		}
    } else {
		$("#" + element.id).parent().find(".bootstrap-maxlength").remove();
    }


    if (error_text != "" && error_text != null) {
      if ($("#" + element.id).parent().find(".help-block").length <= 0) {
          $("#" + element.id).closest(".form-group > div").append("<div class=\"help-block animated fadeInDown\">" + error_text + "</div>");
          $("#" + element.id).closest(".form-group").addClass("has-error");
      } else {
          $("#" + element.id).parent().find(".help-block").empty();
          $("#" + element.id).parent().find(".help-block").append(error_text);
      }

    } else {
      $("#" + element.id).parent().find(".help-block").remove();
      $("#" + element.id).closest(".form-group").removeClass("has-error");
    }

}
function create_modules_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: 20,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_modules_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "" && search_text != null) {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "" && search_text != null) {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_modules_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_modules_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_modules_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			if ($("#datatable-" + data_table["modules_id"]).length > 0) {
				$("#datatable").find("#datatable-" + data_table["modules_id"]).remove();
			}
			$("#datatable-list").prepend(response.html);
			if (count_data_table < datatable_data_limit) {
				create_modules_table();
			} else {
				create_modules_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_modules_open", $(this).attr("data-target"));
				} else {
					edit_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_modules_open", $(this).attr("data-target"));
				} else {
					copy_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_modules_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
				} else {
					translate_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_modules_modal_prompt($(this).attr("data-content"), "delete_modules_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_modules_open($(this).attr("data-target"));
			});
		}
	});

}

function update_modules_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_modules_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			$("#datatable").find("#datatable-" + data_table["modules_id"]).empty();
			$("#datatable").find("#datatable-" + data_table["modules_id"]).append(response.html);
			if (count_data_table < datatable_data_limit) {
				create_modules_table();
			} else {
				create_modules_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_modules_open", $(this).attr("data-target"));
				} else {
					edit_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_modules_open", $(this).attr("data-target"));
				} else {
					copy_modules_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_modules_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
				} else {
					translate_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_modules_modal_prompt($(this).attr("data-content"), "delete_modules_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_modules_open($(this).attr("data-target"));
			});
		}
	});

}

function delete_modules_table_row(field_id) {
	mainDataTable.fnDestroy();
	$("#datatable").find("#datatable-" + field_id).remove();
	if (count_data_table < datatable_data_limit) {
		create_modules_table();
	} else {
		create_modules_table_defer();
	}
}

function show_modules_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_modules_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {

	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_modules_open", $(this).attr("data-target"));
		} else {
			create_modules_open($(this).attr("data-target"));
		}
	});

	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_modules_open", $(this).attr("data-target"));
		} else {
			edit_modules_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_modules_open", $(this).attr("data-target"));
		} else {
			copy_modules_open($(this).attr("data-target"));
		}
	});

	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_modules_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
		} else {
			translate_modules_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});

	$(".btn-delete").click(function () {
		show_modules_modal_prompt($(this).attr("data-content"), "delete_modules_data", $(this).attr("data-target"));
	});

	$(".btn-view").click(function () {
		view_modules_open($(this).attr("data-target"));
	});

	$(".btn-form-close").click(function () {
		show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_modules_close", "");
	});
	
	$(".btn-sort").click(function () {
		sort_modules_open();
	});
	
	$(".btn-sort-close").click(function () {
		show_modules_modal_prompt("<?php echo translate("Are you sure you want to leave current sort") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "sort_modules_close", "");
	});
	
	$("#button-data-save-sort").click(function(){
		save_sort_modules();
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});

	$("#modules_activate").click(function () {
		if ($("#modules_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});

	$("#modules_description").tinymce({

		setup: function (editor) {
			editor.on("change", function (e) {
				var content = this.getContent({
					//Get only text not included HTML tags
					//format: "text"
				});
				var element = this.getElement();
				//var container = this.getContainer();
				//var container_area_container = this.getContentAreaContainer();
				var options = {}
				validate_modules_description_tinymce(element, content, options);
			})
		},
		theme: "modern",
		skin: "greenmama", width: "100%",height: 150,
		convert_urls : true,
		relative_urls : true,
		//autoresize : true,

		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor color responsivefilemanager code"],

		toolbar1: "responsivefilemanager | undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor  | forecolor backcolor color | print preview | code",

		image_advtab: true ,
		external_filemanager_path: "plugins/filemanager/",
		filemanager_title: "File Manager" ,
		filemanager_access_key: "hflsHFSj45fjg-dgfMMh",
		external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}

	});
	$(".modules_icon").first().prop('checked', true);
	$("#modules_icon-check-loading").fadeOut("fast");
	$(".modules_subject_icon").first().prop('checked', true);
	$("#modules_subject_icon-check-loading").fadeOut("fast");
	$("#modules_link-fileselector-btn").fancybox({
	  "width": 880,
	  "height": 900,
	  "minHeight": 500,
	  "type": "iframe",
	  "autoScale": "auto",
	  "padding": 0
	});
	$("#modules_function_link-fileselector-btn").fancybox({
	  "width": 880,
	  "height": 900,
	  "minHeight": 500,
	  "type": "iframe",
	  "autoScale": "auto",
	  "padding": 0
	});
	$("#modules_pages_parent_link_field").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_pages_parent_link_field_data_dynamic_list",
			parameters: "<?php echo $modules["modules_db_name"]; ?>",
			filters: modules_pages_parent_link_field_filters
		},
		success: function(response) {
			$("#modules_pages_parent_link_field-check-loading").fadeOut("fast");
			$("#modules_pages_parent_link_field").empty();
			$("#modules_pages_parent_link_field").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
			$("#modules_pages_parent_link_field").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_pages_parent_link_field").append("<option value=\"" + response.values[i]['pages_link'] + "\">" + response.values[i]['pages_title'] + " (ID: " + response.values[i]['pages_link'] + ")</option>");
				}
				$("#modules_pages_parent_link_field").val($("#modules_pages_parent_link_field option:first").val()).trigger("change");
			}
		}
	});
	$("#pages_id").select2();
	$.ajax({
		url: "system/core/<?php echo $configs["version"]; ?>/pages.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_pages_main_language_data_all"
		},
		success: function(response) {
			$("#pages_id-check-loading").fadeOut("fast");
			$("#pages_id").empty();
			$("#pages_id").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
			$("#pages_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#pages_id").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_title'] + " (ID: " + response.values[i]['pages_id'] + ", " + response.values[i]['languages_short_name'] + ")</option>");
				}
				$("#pages_id").val($("#pages_id option:first").val()).trigger("change");
			}
		}
	});
	$("#modules_pages_template-fileselector-btn").fancybox({
	  "width": 880,
	  "height": 900,
	  "minHeight": 500,
	  "type": "iframe",
	  "autoScale": "auto",
	  "padding": 0
	});
	$("#modules_image_crop_quality").ionRangeSlider();
	$("#modules_image_crop_quality").data("ionRangeSlider").update({
		from: modules_image_crop_quality_default_value[0]
	});
	$("#modules_image_crop_thumbnail_quality").ionRangeSlider();
	$("#modules_image_crop_thumbnail_quality").data("ionRangeSlider").update({
		from: modules_image_crop_thumbnail_quality_default_value[0]
	});
	$("#modules_image_crop_large_quality").ionRangeSlider();
	$("#modules_image_crop_large_quality").data("ionRangeSlider").update({
		from: modules_image_crop_large_quality_default_value[0]
	});
	$("#button-data-add-modules_datatable_field").click(function () {
		$("#modules_datatable_field_dynamic_form").append('<div class=\"block block-bordered modules_datatable_field\" id=\"modules_datatable_field_' + $(".modules_datatable_field").length + '\"><div class=\"block-header\"><label><?php echo translate("Data Table Field"); ?> #' + parseInt($(".modules_datatable_field").length + 1) + '</label></div><div class=\"block-content\"><div class=\"row\"><div class=\"col-md-12"><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_name_' + $(".modules_datatable_field").length + '"><?php echo translate("Field Name"); ?> </label> <div class="col-md-12"> <select class="form-control modules_datatable_field_name" id="modules_datatable_field_name_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_name' + $(".modules_datatable_field").length + '" > <?php /* PHP variable for filter of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_filters = array( "user_id" => "", "languages_short_name" => "", ); */ $modules_datatable_field_name_filters = ""; /* PHP variable for extended command of dynamic selector "modules_datatable_field_name"*/ /* Example: $modules_datatable_field_name_extended_command = array( array( "conjunction" => "AND", "key" => "user_name", "operator" => "LIKE", "value" => "%Michael%", ), array( "conjunction" => "OR", "key" => "user_name", "operator" => "LIKE", "value" => "%Ammy%", ), ) */ $modules_datatable_field_name_extended_command = ""; ?>  <?php $count_modules_datatable_field_name = count_modules_datatable_field_name_data_dynamic_list("", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); if ($count_modules_datatable_field_name <= $configs["datatable_data_limit"]) { ?><?php $modules_datatable_field_name = get_modules_pages_parent_link_field_data_dynamic_list("", "", "", "", "1", $modules_datatable_field_name_filters, $modules_datatable_field_name_extended_command); for($i = 0; $i < count($modules_datatable_field_name); $i++){ ?> <option value="<?php echo htmlspecialchars($modules_datatable_field_name[$i]['COLUMN_NAME']); ?>"> <?php echo htmlspecialchars($modules_datatable_field_name[$i]['COLUMN_NAME_HUMAN'])." (".$modules_datatable_field_name[$i]['COLUMN_NAME'].")"; ?> </option> <?php } ?> <?php } else { ?> <option label="default" value=""><?php echo translate("Loading");?>...</option> <?php } ?> </select> <div id="modules_datatable_field_name-check-loading_' + $(".modules_datatable_field").length + '" class="progress-greenmama" ><div class="progress-bar-greenmama"></div></div> </div> </div> </div><div class="col-md-6"> <div class="form-group"> <label class="col-md-12" for="modules_datatable_field_display_' + $(".modules_datatable_field").length + '"><?php echo translate("Display"); ?></label> <div class="col-md-12"> <label class="css-input switch switch-lg switch-success"> <input id="modules_datatable_field_display_' + $(".modules_datatable_field").length + '" name="modules_datatable_field_display' + $(".modules_datatable_field").length + '" class="modules_datatable_field_display" type="checkbox" value="" checked ><span></span> <?php echo translate(""); ?> </label> </div> </div> </div></div></div></div></div>');
	$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_table_field_data_dynamic_list",
			parameters: $("#modules_db_name").val(),
			filters: modules_datatable_field_name_filters
		},
		success: function(response) {
			$("#modules_datatable_field_name-check-loading_" + parseInt($(".modules_datatable_field").length - 1)).fadeOut("fast");
			$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).append("<option value=\"" + response.values[i]['COLUMN_NAME'] + "\">" + response.values[i]['COLUMN_NAME_HUMAN'] + " (" + response.values[i]['COLUMN_NAME'] + ")</option>");
				}
				$("#modules_datatable_field_name_" + parseInt($(".modules_datatable_field").length - 1)).val($("#modules_datatable_field_name option:first").val()).trigger("change");
			}
		}
	});
	});

	$("#button-data-remove-modules_datatable_field").click(function () {
		if ($("#modules_datatable_field_" + parseInt($(".modules_datatable_field").length - 2)).length > 0) {
			$("html, body").animate({
				scrollTop: $("#modules_datatable_field_" + parseInt($(".modules_datatable_field").length - 2)).offset().top - 80 + "px"
			}, "fast");
		}
		$("#modules_datatable_field_" + parseInt($(".modules_datatable_field").length - 1)).remove();
	});

	$("#modules_datatable_field_name_0").select2();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_table_field_data_dynamic_list",
			parameters: $("#modules_db_name").val(),
			filters: modules_datatable_field_name_filters
		},
		success: function(response) {
			$("#modules_datatable_field_name-check-loading_0").fadeOut("fast");
			$("#modules_datatable_field_name_0").empty();
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_datatable_field_name_0").append("<option value=\"" + response.values[i]['pages_id'] + "\">" + response.values[i]['pages_link'] + " (ID: " + response.values[i]['pages_id'] + ")</option>");
				}
				$("#modules_datatable_field_name_0").val($("#modules_datatable_field_name option:first").val()).trigger("change");
			}
		}
	});
	$("#modules_db_name").keyup(function () {
		$.ajax({
			url: url,
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				method: "get_modules_pages_parent_link_field_data_dynamic_list",
				parameters: $(this).val()
			},
			success: function(response_modules_pages_parent_link_field) {
				$("#modules_pages_parent_link_field-check-loading").fadeOut("fast");
				var modules_pages_parent_link_field = $("#modules_pages_parent_link_field").val();
				$("#modules_pages_parent_link_field").empty();
				$("#modules_pages_parent_link_field").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
				$("#modules_pages_parent_link_field").append("<option value=\"\" disabled>-</option>");
				if (response_modules_pages_parent_link_field.values != null) {
					for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
						$("#modules_pages_parent_link_field").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
						if (response_modules_pages_parent_link_field.values.length - i == 1) {
							$("#modules_pages_parent_link_field").select2("val", "");
						}
					}
				} else {
					$("#modules_pages_parent_link_field").empty();
					$("#modules_pages_parent_link_field").append("<option value=\"\"><?php echo translate("Disable"); ?></option>");
				}
			}
		});
		$.ajax({
			url: url,
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				method: "get_modules_data_table_field_data_dynamic_list",
				parameters: $(this).val()
			},
			success: function(response_modules_pages_parent_link_field) {
				$(".modules_datatable_field_name").empty();
				if (response_modules_pages_parent_link_field.values != null) {
					for(var i = 0; i < response_modules_pages_parent_link_field.values.length; i++) {
						$(".modules_datatable_field_name").append("<option value=\"" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + "\">" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME_HUMAN'] + " (" + response_modules_pages_parent_link_field.values[i]['COLUMN_NAME'] + ")</option>");
						if (response_modules_pages_parent_link_field.values.length - i == 1) {
							$(".modules_datatable_field_name").select2("val", "");
						}
					}
				} else {
					$(".modules_datatable_field_name").empty();
				}
			}
		});
	});

	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_modules_table();
	} else {
		create_modules_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_modules_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_modules_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_modules_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().modules_id != null) {
			edit_modules_open(get_url_param().modules_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().modules_id != null) {
			copy_modules_open(get_url_param().modules_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().modules_id != null) {
			translate_modules_open(get_url_param().modules_id, get_url_param().modules_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().modules_id != null) {
			view_modules_open(get_url_param().modules_id);
		}
	}
	/* URL response (end) */


	if ($("#modules_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}

	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");

});
</script>
<!-- initialization: JavaScript (end) -->

<script>
function validate_modules_description_tinymce (element, content, options){

    if (content == "" || content == null) {
		var error_text = "<?php echo translate("Description") . " " . translate("is required"); ?>";
    } else if (content.length <= options.min_value && content.length <= options.max_value) {
		var error_text = "<?php echo translate("Description") . " " . translate("is shorter than minimum length of strings"); ?> " + options.min_value;
    } else if (content.length >= options.max_value) {
		if ($("#" + element.id).parent().find(".bootstrap-maxlength").length <= 0) {
        	$("#" + element.id).parent().append("<span class=\"bootstrap-maxlength label-danger label\" style=\"display: block; left: 50%; position: absolute; white-sapce: nowrap; z-index: 1099;\">" + content.length + "/" + options.max_value + "<span>");
		} else {
			$("#" + element.id).parent().find(".bootstrap-maxlength").empty();
			$("#" + element.id).parent().find(".bootstrap-maxlength").append(content.length + "/" + options.max_value);
		}
    } else {
		$("#" + element.id).parent().find(".bootstrap-maxlength").remove();
    }


    if (error_text != "" && error_text != null) {
      if ($("#" + element.id).parent().find(".help-block").length <= 0) {
          $("#" + element.id).closest(".form-group > div").append("<div class=\"help-block animated fadeInDown\">" + error_text + "</div>");
          $("#" + element.id).closest(".form-group").addClass("has-error");
      } else {
          $("#" + element.id).parent().find(".help-block").empty();
          $("#" + element.id).parent().find(".help-block").append(error_text);
      }

    } else {
      $("#" + element.id).parent().find(".help-block").remove();
      $("#" + element.id).closest(".form-group").removeClass("has-error");
    }

}
/*
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");

jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_modules_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"modules_name": {
					required: true,
				},
				"modules_description": {
					required: false,
				},
				"modules_icon": {
					required: false,
				},
				"modules_category": {
					required: false,
				},
				"modules_subject": {
					required: false,
				},
				"modules_subject_icon": {
					required: false,
				},
				"modules_link": {
					required: true,
				},
				"modules_function_link": {
					required: false,
				},
				"modules_key": {
					required: true,
					no_specialchar_soft: true,
				},
				"modules_db_name": {
					required: true,
					no_specialchar_hard: true,
				},
				"modules_protected": {
					required: false,
				},
				"modules_individual_pages_parent_link": {
					required: false,
				},
				"modules_pages_parent_link_field": {
					required: false,
				},
				"modules_pages_template": {
					required: false,
				},
				"modules_pages_template_settings": {
					required: false,
				},
				"modules_image_crop_quality": {
					required: false,
					range: [0,100],
				},
				"modules_image_crop_thumbnail": {
					required: false,
				},
				"modules_image_crop_thumbnail_aspectratio": {
					required: false,
				},
				"modules_image_crop_thumbnail_quality": {
					required: false,
					range: [0,100],
				},
				"modules_image_crop_thumbnail_width": {
					required: false,
					min: 0,
				},
				"modules_image_crop_thumbnail_height": {
					required: false,
					min: 0,
				},
				"modules_image_crop_large": {
					required: false,
				},
				"modules_image_crop_large_aspectratio": {
					required: false,
				},
				"modules_image_crop_large_quality": {
					required: false,
					range: [0,100],
				},
				"modules_image_crop_large_width": {
					required: false,
					min: 0,
				},
				"modules_image_crop_large_height": {
					required: false,
					min: 0,
				},
			 },
			 messages: {
				"modules_name": {
					required: "<strong><?php echo translate("Name"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_description": {
					required: "<strong><?php echo translate("Description"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_icon": {
					required: "<strong><?php echo translate("Icon"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_category": {
					required: "<strong><?php echo translate("Category"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_subject": {
					required: "<strong><?php echo translate("Subject"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_subject_icon": {
					required: "<strong><?php echo translate("Subject Icon"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_link": {
					required: "<strong><?php echo translate("Backend Page"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_function_link": {
					required: "<strong><?php echo translate("Function Link"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_key": {
					required: "<strong><?php echo translate("Key"); ?></strong> <?php echo translate("is required"); ?>",
					no_specialchar_soft: "<strong><?php echo translate("Key"); ?></strong> <?php echo translate("does not allow special characters and white spaces excluding ., -, _"); ?>"
				},
				"modules_db_name": {
					required: "<strong><?php echo translate("Database Table Name"); ?></strong> <?php echo translate("is required"); ?>",
					no_specialchar_hard: "<strong><?php echo translate("Database Table Name"); ?></strong> <?php echo translate("does not allow special characters and white spaces"); ?>"
				},
				"modules_protected": {
					required: "<strong><?php echo translate("Protected"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_individual_pages_parent_link": {
					required: "<strong><?php echo translate("Enable Module Individual Front-end Page Parent Link"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_pages_parent_link_field": {
					required: "<strong><?php echo translate("Front-end Parent Link Field"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_pages_template": {
					required: "<strong><?php echo translate("Front-end Template Inner Pages"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_pages_template_settings": {
					required: "<strong><?php echo translate("Front-end Template Inner Pages Settings"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_image_crop_quality": {
					required: "<strong><?php echo translate("Image Quality"); ?></strong> <?php echo translate("is required"); ?>",
					range: "<strong><?php echo translate("Image Quality"); ?></strong> <?php echo translate("is out of range for number between"); ?> 0 - 100",
				},
				"modules_image_crop_thumbnail": {
					required: "<strong><?php echo translate("Enable Image Thumbnail"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_image_crop_thumbnail_aspectratio": {
					required: "<strong><?php echo translate("Keep Aspect Ratio for Image Thumbnail"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_image_crop_thumbnail_quality": {
					required: "<strong><?php echo translate("Image Thumbnail Quality"); ?></strong> <?php echo translate("is required"); ?>",
					range: "<strong><?php echo translate("Image Thumbnail Quality"); ?></strong> <?php echo translate("is out of range for number between"); ?> 0 - 100",
				},
				"modules_image_crop_thumbnail_width": {
					required: "<strong><?php echo translate("Image Thumbnail Width"); ?></strong> <?php echo translate("is required"); ?>",
					min: "<strong><?php echo translate("Image Thumbnail Width"); ?></strong> <?php echo translate("is less than minimum number"); ?> 0",
				},
				"modules_image_crop_thumbnail_height": {
					required: "<strong><?php echo translate("Image Thumbnail Height"); ?></strong> <?php echo translate("is required"); ?>",
					min: "<strong><?php echo translate("Image Thumbnail Height"); ?></strong> <?php echo translate("is less than minimum number"); ?> 0",
				},
				"modules_image_crop_large": {
					required: "<strong><?php echo translate("Enable Image Large Thumbnail"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_image_crop_large_aspectratio": {
					required: "<strong><?php echo translate("Keep Aspect Ratio for Image Large Thumbnail"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"modules_image_crop_large_quality": {
					required: "<strong><?php echo translate("Image Large Thumbnail Quality"); ?></strong> <?php echo translate("is required"); ?>",
					range: "<strong><?php echo translate("Image Large Thumbnail Quality"); ?></strong> <?php echo translate("is out of range for number between"); ?> 0 - 100",
				},
				"modules_image_crop_large_width": {
					required: "<strong><?php echo translate("Image Large Thumbnail Width"); ?></strong> <?php echo translate("is required"); ?>",
					min: "<strong><?php echo translate("Image Large Thumbnail Width"); ?></strong> <?php echo translate("is less than minimum number"); ?> 0",
				},
				"modules_image_crop_large_height": {
					required: "<strong><?php echo translate("Image Large Thumbnail Height"); ?></strong> <?php echo translate("is required"); ?>",
					min: "<strong><?php echo translate("Image Large Thumbnail Height"); ?></strong> <?php echo translate("is less than minimum number"); ?> 0",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once("templates/".$configs["backend_template"]."/footer.php");
?>
