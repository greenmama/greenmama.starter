<?php
require_once "system/include.php";

/* configurations: Media */
$media = get_modules_data_by_id("10");
if (isset($media) && !empty($media)) {
    $title = translate($media["modules_name"]);
    $module_page = $media["modules_link"];
    $module_page_link = backend_rewrite_url($media["modules_link"]);
    $module_function_page = htmlspecialchars("system/" . $media["modules_function_link"]);
} else {
    $title = translate("Media");
    $module_page = "media.php";
    $module_page_link = backend_rewrite_url("media.php");
    $module_function_page = htmlspecialchars("system/core/" . $configs["version"] . "/media.php");
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);

/* authentication: Media */
if (!authentication_session_users()) {
    authentication_deny();
}
if (!authentication_session_modules($module_page)) {
    authentication_permission_deny();
}

if (isset($media["modules_datatable_field"]) && !empty($media["modules_datatable_field"])) {
    $table_module_field = array();
    for ($i = 0; $i < count($media["modules_datatable_field"]); $i++) {
        if ($media["modules_datatable_field"][$i]["modules_datatable_field_display"] == "1") {
            $media_modules_datatable_field_display = true;
        } else {
            $media_modules_datatable_field_display = false;
        }
        $table_module_field[$media["modules_datatable_field"][$i]["modules_datatable_field_name"]] = $media_modules_datatable_field_display;
    }
} else {
    $table_module_field = array(
        "media_id" => true,
        "media_name" => true,
        "media_file" => true,
        "media_type" => false,
        "media_date_created" => true,
        "users_id" => false,
        "users_username" => false,
        "users_name" => true,
        "users_last_name" => false,
        "media_activate" => true,
        "modules_id" => false,
        "modules_name" => false,
        "media_actions" => true,
    );
}

require_once "templates/" . $configs["backend_template"] . "/header.php";
require_once "templates/" . $configs["backend_template"] . "/overlay.php";
require_once "templates/" . $configs["backend_template"] . "/sidebar.php";
require_once "templates/" . $configs["backend_template"] . "/navbar.php";
require_once "templates/" . $configs["backend_template"] . "/container-header.php";
?>

<!-- Forms Row -->
<div class="row">
	<div class="col-lg-12">
		<!-- Bootstrap Forms Validation -->
		<div class="block" id="form-content">
			<div class="block-header bg-mama-green">
				<ul class="block-options">
					<li>
						<button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
					</li>
					<li>
						<button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
					<li>
						<button type="button" class="btn-form-close" title="<?php echo translate("Close"); ?>" data-toggle="block-option"><i class="si si-close"></i></button>
					</li>
				</ul>
				<a href="javascript: void(0)"><h3 class="block-title" id="form-title" data-toggle="block-option" data-action="content_toggle"><?php echo translate("Add") . " " . $title; ?></h3></a>
			</div>
			<div class="block-content">
				<form id="module-form" class="js-validation-bootstrap form-horizontal" onsubmit="return false;" action="<?php echo htmlspecialchars($module_function_page); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="media">
					<input type="hidden" id="media_id" name="media_id">
					<input type="hidden" id="media_action" name="media_action" value="">
					<input type="hidden" id="users_id" name="users_id" value="<?php echo $_SESSION["users_id"]; ?>">
					<input type="hidden" id="users_username" name="users_username" value="<?php echo $_SESSION["users_username"]; ?>">
					<input type="hidden" id="users_name" name="users_name" value="<?php echo $_SESSION["users_name"]; ?>">
					<input type="hidden" id="users_last_name" name="users_last_name" value="<?php echo $_SESSION["users_last_name"]; ?>">

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="media_name"><?php echo translate("Media name"); ?><span id="media_name_stringlength" class="text-success pull-right push-10-l">0/200</span></label>
							<div class="col-md-12">
								<input class="form-control media_name" type="text" id="media_name" name="media_name" value="" placeholder="<?php echo translate("Name of media"); ?>" >

								<div class="help-block"><?php echo translate(""); ?></div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="media_file"><?php echo translate("File"); ?><span class="text-danger">*</span> </label>
							<div class="col-md-12">
								<input class="form-control media_file" type="file" id="media_file" name="media_file"  placeholder="<?php echo translate("File of media");?>" >
								
								<div class="help-block"><?php echo translate(""); ?></div>
								<div class="col-md-12 push-20-t">
									<div id="media_file_view_old">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-12" for="media_type"><?php echo translate("Type"); ?><span class="text-danger">*</span> <span id="media_type_stringlength" class="text-success pull-right push-10-l">0/10</span></label>
							<div class="col-md-12">
								<input class="form-control media_type" type="text" id="media_type" name="media_type" value="" placeholder="<?php echo translate("Type of media"); ?>" >

								<div class="help-block"><?php echo translate(""); ?></div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="block block-bordered">
							<div class="block-header">
								<label><i class="si si-grid push-5-r"></i> <?php echo translate("Modules"); ?> <a href="modules.php" target="_blank" title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>" data-toggle="tooltip" data-original-title="<?php echo translate("Manage"); ?> <?php echo translate("Modules"); ?>"><i class="si si-settings push-5-l"></i></a></h3></label>
							</div>
							<div class="block-content">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-12" for="modules_id"><?php echo translate("Module"); ?></label>
												<div class="col-md-12">
													<select class="form-control" type="text" id="modules_id" name="modules_id" disabled>
														<option value=""><?php echo translate("Automatically link by system"); ?></option>
														<option value="" disabled>-</option>
														<?php
														$modules = get_modules_data_all();
														for($i=0;$i<count($modules);$i++){
															?>
														<option value="<?php echo $modules[$i]["modules_id"];?>" data-key="<?php echo $modules[$i]["modules_key"];?>" data-name="<?php echo $modules[$i]["modules_name"];?>"><?php echo $modules[$i]["modules_name"];?> (ID: <?php echo $modules[$i]["modules_id"];?>)</option>
															<?php
														}
														?>
													</select>
													<div class="help-block"><?php echo translate("*You cannot link module from here");?></div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-12" for="modules_record_target"><?php echo translate("Module Content"); ?></label>
												<div class="col-md-12">
													<input type="hidden" id="modules_record_target_temp" name="modules_record_target_temp" value="" disabled />
													<select class="form-control" type="text" id="modules_record_target" name="modules_record_target" disabled>
													</select>
													<div class="help-block"><?php echo translate("*You cannot link module content from here");?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="revision" class="col-md-12">
						<div class="form-group">
                        	<div class="col-md-12">
                                <div id="revision_list_parent" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#revision_list_parent" href="#revision_list"><i class="si si-clock"></i> <span class="panel-title"><?php echo translate("Reversion"); ?></span></a>
                                            </div>
											<div class="help-block"><?php echo translate("Reverse back to another versions of content"); ?></div>
                                        </div>
                                        <div id="revision_list" class="panel-collapse collapse">
                                        	<div id="revision_list_data" class="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>

					<div class="row items-push">
						<div class="col-md-12 push-30-t">
							<div class="col-lg-10 col-md-10 col-sm-9 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<label class="css-input switch switch-lg switch-success">
											<input id="media_activate" name="media_activate" type="checkbox" value="1" checked><span></span> <?php echo translate("Publish"); ?>
										</label>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
								<div class="form-group">
									<div class="col-md-12 text-right">
										<button id="button-data-submit" class="btn btn-sm btn-primary" type="submit"><?php echo translate("Submit"); ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<div id="form-footer" class="block-content block-content-mini block-content-full">
			</div>
		</div>
	</div>
</div>

<!-- data table (begin) -->
<div class="row">

    <div id="module-table" class="col-lg-12">

        <div id="module-table-block" class="block block-themed">
            <div class="block-header">
                <ul class="block-options">
					<?php if ($_SESSION["users_id"] == "0" || $_SESSION["users_id"] == "1" || $_SESSION["groups_id"] == "0" || $_SESSION["groups_id"] == "1") {?>
						<li>
	                        <button class="btn-add" type="button" title="<?php echo translate("Add"); ?>"><i class="si si-plus" data-toggle="tooltip"></i> <span class="hidden-xs"><?php echo translate("Add"); ?></span></button>
	                    </li>
	                <?php }?>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Fullscreen"); ?>" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                    </li>
                    <li>
                        <button type="button" title="<?php echo translate("Toggle Show"); ?>" data-toggle="block-option" data-action="content_toggle"></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo translate("Media"); ?> <?php echo translate("List"); ?></h3>
            </div>

            <div class="block-content">
				<div id="filter_parent" class="panel-group">
					<div class="panel panel-default">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#filter_parent" href="#filter">
							<div class="panel-heading">
								<span class="panel-title"><?php echo translate("Advanced Filter"); ?></span> <i class="fa fa-angle-down"></i>
							</div>
						</a>
						<div id="filter" class="panel-collapse collapse">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<form action="" method="get">
											<input type="hidden" value="<?php if (isset($_GET["search"]) && !empty($_GET["search"])) {echo $_GET["search"];}?>">
											<div class="row items-push">
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-4 push-5-t" for="published"><?php echo translate("Status"); ?></label>
														<div class="col-md-8">
															<?php
if (isset($_GET["published"])) {
    if ($_GET["published"] == "") {
        $status_all_selected = " selected";
        $status_published_selected = "";
        $status_unpublished_selected = "";
    } else if ($_GET["published"] == "1") {
        $status_all_selected = "";
        $status_published_selected = " selected";
        $status_unpublished_selected = "";
    } else if ($_GET["published"] == "0") {
        $status_all_selected = "";
        $status_published_selected = "";
        $status_unpublished_selected = " selected";
    } else {
        $status_all_selected = " selected";
        $status_published_selected = "";
        $status_unpublished_selected = "";
    }
} else {
    $status_all_selected = " selected";
    $status_published_selected = "";
    $status_unpublished_selected = "";
}
?>
															<select name="published" aria-controls="datatable" class="form-control">
																<option value="" <?php echo $status_all_selected; ?>><?php echo translate("All"); ?></option>
																<option value="" disabled>-</option>
																<option value="1" <?php echo $status_published_selected; ?>><?php echo translate("Published"); ?></option>
																<option value="0" <?php echo $status_unpublished_selected; ?>><?php echo translate("Unpublished"); ?></option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="from"><?php echo translate("From"); ?></label>
														<div class="col-md-9">
															<input class="form-control from" type="text" data-role="date" id="from" name="from" value="<?php if (isset($_GET["from"]) && !empty($_GET["from"])) {echo $_GET["from"];}?>">
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="col-md-3 push-5-t" for="to"><?php echo translate("To"); ?></label>
														<div class="col-md-9">
															<input class="form-control to" type="text" data-role="date" id="to" name="to" value="<?php if (isset($_GET["to"]) && !empty($_GET["to"])) {echo $_GET["to"];}?>">
														</div>
													</div>
												</div>
												<div class="col-md-3 text-right">
													<div class="form-group">
														<div class="col-xs-6">
															<button id="button-filter-submit" class="btn btn-sm btn-primary" type="submit" style="width: 100%;"><?php echo translate("Show"); ?></button>
														</div>
														<div class="col-xs-6">
															<button id="button-filter-reset" class="btn btn-sm btn-default" type="reset" onclick="location.href = '<?php echo $module_page_link; ?>'" style="width: 100%;"><?php echo translate("Reset"); ?></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            	<?php
$data_table_info = prepare_media_table_defer($table_module_field);
echo create_media_table($data_table_info["values"], $table_module_field);
?>
            </div>
        </div>
    </div>
</div>
<!-- data table (end) -->

<?php
include "templates/" . $configs["backend_template"] . "/container-footer.php";
?>
<!-- notification: response (begin) -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> <?php echo translate("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- notification: response (end) -->

<!-- notification: prompt (begin) -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideup">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-warning">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="block-content">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- notification: prompt (end) -->

<!-- include: CSS styles (begin) -->
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="plugins/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/jquery-tags-input/jquery.tagsinput.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/greenmama.css" rel="stylesheet">
<link href="templates/<?php echo $configs["backend_template"]; ?>/styles/style.css" rel="stylesheet">
<!-- include: CSS styles (end) -->

<!-- include: JavaScript (begin) -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.js"></script>
<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/nestedsort/jquery.ui.touch-punch.js"></script>
<script src="plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "<script src=\"plugins/bootstrap-datepicker/locales/bootstrap-datepicker." . $configs["backend_language"] . ".min.js\"></script>";
}
?>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo htmlspecialchars($module_function_page); ?>";

/* JavaScript variable for table module field configurations */
var table_module_field = $.parseJSON("<?php echo addslashes(json_encode($table_module_field)); ?>");

/* JavaScript variable for table data configurations */
var data_table = $.parseJSON("<?php echo addslashes(json_encode($data_table_info["values"])); ?>");
var count_data_table = <?php echo $data_table_info["count_true"]; ?>;
var datatable_data_limit = <?php echo $configs["datatable_data_limit"]; ?>;

/* JavaScript variable for transalation */
var transalations = $.parseJSON("<?php echo addslashes(json_encode($translations)); ?>");

/* JavaScript variable for data table */
var mainDataTable;
</script>

<script>

function reset_media_data() {

	$("#media_id").val("");
	$("#media_name").val("");
	/* Reset data for upload */
	$("#media_file_old").empty();
	$("#media_file").val("");
	$("#media_type").val("");
	$("#modules_record_target").empty();
	$("#modules_record_target").append("<option value=\"\"><?php echo translate("Please select a module first"); ?></option>");
	$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_all"
		},
		success: function(response) {
			$("#modules_id").empty();
			$("#modules_id").append("<option value=\"\"><?php echo translate("Automatically link by system"); ?></option>");
			$("#modules_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_id").append("<option value=\"" + response.values[i]["modules_id"] + "\" data-key=\"" + response.values[i]["modules_key"] + "\" data-name=\"" + response.values[i]["modules_name"] + "\">" + response.values[i]["modules_name"] + " (ID: " + response.values[i]["modules_id"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#modules_id").val($("#modules_id option:first").val()).trigger("change");
					}
				}
			}
		}
	});
	$("#media_activate").prop("checked", true);
	$("#button-data-submit").removeAttr("formnovalidate");
	$("#media_action").val("");

}

function update_media_data(target_id) {
	$("#modules_record_target").empty();
	$.ajax({
		url: "system/core/<?php echo $configs["version"];?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_data_all"
		},
		success: function(response) {
			var modules_id = $("#modules_id").val();
			$("#modules_id").empty();
			$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
			$("#modules_id").append("<option value=\"\" disabled>-</option>");
			if (response.values != null) {
				for(var i = 0; i < response.values.length; i++) {
					$("#modules_id").append("<option value=\"" + response.values[i]["modules_id"] + "\" data-key=\"" + response.values[i]["modules_key"] + "\" data-name=\"" + response.values[i]["modules_name"] + "\">" + response.values[i]["modules_name"] + " (ID: " + response.values[i]["modules_id"] + ")</option>");
					if (response.values.length - i == 1) {
						$("#modules_id").select2("val", modules_id);
					}
				}
			}
		}
	});
}

function submit_media_data() {

	$("#form-content").addClass("block-opt-refresh");

    $("#button-data-submit").prop("disabled", true);

    if ($("#media_id").val() == "") {
		var method = "create_media_data";
	} else {
		var method = "update_media_data";
	}

	if ($("#media_activate").is(":checked") === true){
		var media_activate = "1";
	} else {
		var media_activate = "0";
	}
	var modules_id = $("#modules_id").val();
	var modules_name = $("#modules_id").find("option:selected", this).attr("data-name");
	var modules_record_target = $("#modules_record_target").val();







	var form_values   = {
		"media_id": $("#media_id").val().trim(),
		"media_action": $("#media_action").val().trim(),
		"media_name": $("#media_name").val(),
		"media_file": $("#media_file").val(),
		"media_type": $("#media_type").val(),
		"modules_id": modules_id.trim(),
		"modules_name": modules_name.trim(),
		"modules_record_target": modules_record_target.trim(),

		"users_id": $("#users_id").val().trim(),
		"users_username": $("#users_username").val().trim(),
		"users_name": $("#users_name").val().trim(),
		"users_last_name": $("#users_last_name").val().trim(),
		"media_activate": media_activate.trim(),
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: method,
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				$("#media_id").val(response.values["media_id"]);
				if (response.values["media_id"] !== undefined) {
					
					if (method == "create_media_data") {
						create_media_table_row(response.values);
					} else if (method == "update_media_data") {
						update_media_table_row(response.values);
					}
					
					var form_data = new FormData(document.querySelector("#module-form"));
					form_data.append("method", "upload_media_files");
					form_data.append("media_activate", media_activate.trim());
					
					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: form_data,
						processData: false,
						contentType: false,
						success: function(response_upload) {
							if (response_upload.status == true) {
								if (method == "create_media_data") {
									update_media_table_row(response.values);
									show_media_modal_response(response_upload.status, "<?php echo translate("Successfully created data"); ?>");
									reset_media_data();
									$("#form-content").slideUp("fast");
									$("html, body").animate({
											scrollTop: "0px"
									}, "fast");
									$("#form-content").removeClass("block-opt-refresh");
									$("#button-data-submit").prop("disabled", false);
								} else if (method == "update_media_data") {
									update_media_table_row(response.values);
									show_media_modal_response(response_upload.status, "<?php echo translate("Successfully updated data"); ?>");
									update_media_data($("#media_id").val());
									edit_media_open($("#media_id").val());
									$("html, body").animate({
											scrollTop: "0px"
									}, "fast");
									$("#form-content").removeClass("block-opt-refresh");
									var limit = $("#datatable_length_selector").val();
									var page = get_url_param().page;
									if (page == null) {
										page = 1;
									}
									var search_text = $("#datatable_search").val();
									var published = get_url_param().published;
									var from = get_url_param().from;
									var to = get_url_param().to;
									if (limit != null) {
										if (search_text != "" && search_text != null) {
											window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
										} else {
											if (published != null || from != null || to != null) {
												window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
											} else {
												window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
											}
										}
									} else {
										if (search_text != "" && search_text != null) {
											window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
										} else {
											if (published != null || from != null || to != null) {
												window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
											} else {
												window.history.pushState("", "", "<?php echo $module_page_link; ?>");
											}
										}
									}
									$("#button-data-submit").prop("disabled", false);
								}	
							} else {
								update_media_table_row(response.values);
								show_media_modal_response(response_upload.status, response_upload.message);
								edit_media_open($("#media_id").val());
								$("html, body").animate({
										scrollTop: "0px"
								}, "fast");
								$("#form-content").removeClass("block-opt-refresh");
								$("#button-data-submit").prop("disabled", false);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							show_media_modal_response(errorThrown, textStatus + ": " + errorThrown);
							$("#form-content").removeClass("block-opt-refresh");
							$("#button-data-submit").prop("disabled", false);
						}
					});

				} else {
					$("#media_id").val(response.values.media_id);
					$("#form-content").removeClass("block-opt-refresh");
            		$("#button-data-submit").prop("disabled", false);
				}

			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
				if (response.target !== undefined && response.target != "") {
					$("html, body").animate({
						scrollTop: $(response.target).offset().top - 130
					}, "fast");
				}
				$("#form-content").removeClass("block-opt-refresh");
            	$("#button-data-submit").prop("disabled", false);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus + ": " + errorThrown);
			$("#form-content").removeClass("block-opt-refresh");
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function delete_media_data(target_id) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_media_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {
				delete_media_table_row(target_id);
				if ($("#media_id").val() == target_id) {
					reset_media_data();
					form_media_close();
				}
                show_media_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
            $("#button-data-submit").prop("disabled", false);
		}
	});

}

function show_module_media_content_list(target_value) {
	
	var modules_record_target = $("#modules_record_target_temp").val();
	$("#modules_record_target").empty();
	$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("Loading"); ?>...</option>");
	
	var form_values   = {
		"modules_id": target_value,
		"modules_record_target": modules_record_target
	}

	$.ajax({
		type: "POST",
		url: "system/core/<?php echo $configs["version"]; ?>/modules.php",
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_modules_content_data_all_by_modules_datatable_field_name",
			parameters: form_values
		},
		success: function(response) {
			if(response.status == true){
				$("#modules_record_target").empty();
				$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("Please select a content"); ?></option>");
				$("#modules_record_target").append("<option label=\"seperator\" value=\"\" disabled>-</option>");
				if (response.values != null) {
					if (modules_record_target == null || modules_record_target == "") {
						for(var i = 0; i < response.values.length; i++) {
							$("#modules_record_target").append("<option value=\"" + response.values[i][0] + "\">" + response.values[i][1] + " (ID: " + response.values[i][0] + ")</option>");
						}
					} else {
						$("#modules_record_target").append("<option value=\"" + response.values[0] + "\">" + response.values[1] + " (ID: " + response.values[0] + ")</option>");
					}
				}
				$("#modules_record_target").select2("val", modules_record_target);
			} else {
				$("#modules_record_target").empty();
				$("#modules_record_target").append("<option label=\"default\" value=\"\"><?php echo translate("No content found"); ?></option>");
				$("#modules_record_target").val($("#modules_record_target option:first").val()).trigger("change");
			}
		}
	});

}

function form_media_close() {

	$("#form-content").fadeTo("fast", 0);
	$("#form-content").slideUp("fast").fadeOut("slow");
	var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit);
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text);
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to);
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>");
			}
		}
	}

}
function create_media_open() {

	$("#revision").hide();

	$("#form-title").empty();
	$("#form-title").append("<?php echo translate("Add") . " " . $title; ?>");

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").slideDown("fast").fadeIn("slow");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

    var limit = $("#datatable_length_selector").val();
	var page = get_url_param().page;
	if (page == null) {
		page = 1;
	}
	var search_text = $("#datatable_search").val();
	var published = get_url_param().published;
	var from = get_url_param().from;
	var to = get_url_param().to;
	if (limit != null) {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=create");
			}
		}
	} else {
		if (search_text != "" && search_text != null) {
			window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=create");
		} else {
			if (published != null || from != null || to != null) {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=create");
			} else {
				window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=create");
			}
		}
	}

	reset_media_data();

	$("#form-content").removeClass("block-opt-refresh");
	$("#media_action").val("create");

}

function edit_media_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_media_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_media_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit") . " " . $title; ?>");

				$("#media_id").val(response.values["media_id"]);
				$("#media_name").val(response.values["media_name"]);
				$("#media_name_stringlength").empty().append($("#media_name").val().length + "/200");
				$("#media_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_name").val().length < 240) {
					$("#media_name_stringlength").addClass("text-success");
				} else {
					$("#media_name_stringlength").addClass("text-danger");
				}
				/* set data for upload "media_file" (begin) */
				if (response.values["media_file"] != null && response.values["media_file"] != "") {
					$("#media_file_view_old").empty();
					$("#media_file_view_old").append('<?php echo translate("Old file(s)"); ?><br /><input name="media_file_old" id="media_file_old" type="hidden" value="' + response.values["media_file"] + '">');
						var media_file_name = "";
						if (typeof response.values["media_file_name"] !== "undefined") {
							if (response.values["media_file_name"].length > 13) {
								var media_file_name = response.values["media_file_name"].substring(0, 13) + "...";
							} else {
								var media_file_name = response.values["media_file_name"];
							}
						} else {
							var media_file_name = response.values["media_file"];
						}
						
						
						if (response.values["media_file_name"].match(/.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action datatable-image-popup\" rel=\"media_file\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\" style=\"background-image: url(" + response.values["media_file_path"] + ");\"><img src=\"plugins/drop/images/square-file-images-push.svg\"></div></div>");
						} else if (response.values["media_file_name"].match(/.(jpe|jif|jfif|jfi|jp2|j2k|jpf|jpx|jpm|mj2|tif|tiff|bmp|dip|pbm|pgm|ppm|pnm|svg|webp|heic|heif|raw|bpg)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-images-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(mov|qt|mpg|mpeg|mpe|mpv|mp2|m2v|m4v|mp4|m4v|avi|mpg|wma|flv|f4v|webm|mkv|vob|ogv|rm|rmvb|asf|amv|3gp|3g2|yuv|mng|gifv|drc|svi|nsv)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-video-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(mp3|m4a|ac3|aiff|mid|ogg|o|ga|wav|aa|aac|act|aiff|amr|m4a|m4b|mmf|mpc|tta|fla)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-audio-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(pdf)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-pdf-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(doc|docx|rtf)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-word-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(xls|xlsx|csv)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-excel-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(ppt|pptx)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-powerpoint-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(txt)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-text-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(html|php|css|js|json|sql|xhtml|xml)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-code-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(zip|rar|gz|tar|iso|dmg)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-zip-push.svg\"></div>");
						} else {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-misc-push.svg\"></div>");
						}
						$("#file-delete-media_file").click(function () {
							$("#modal-prompt").find(".block-content").empty();
							$("#modal-prompt").find(".block-content").append("<?php echo translate("Delete"); ?> <strong><span class=\"icon node-icon fa fa-file-o\"></span> " + $(this).attr("data-name") + "</strong>?");
							$("#modal-prompt").find(".modal-footer").empty();
							$("#modal-prompt").find(".modal-footer").append("<button class=\"btn btn-sm btn-primary\" type=\"button\" data-dismiss=\"modal\" onClick=\"delete_media_file_upload_file('" + $(this).attr("data-file") + "', '" + $(this).attr("data-target") + "', 'media_file', '" + $(this).attr("data-container") + "');\"><i class=\"fa fa-check\"></i> <?php echo translate("Yes"); ?></button><button class=\"btn btn-sm btn-default\" type=\"button\" data-dismiss=\"modal\"><i class=\"fa fa-close\"></i> <?php echo translate("No"); ?></button>");
							$("#modal-prompt").modal("show");
						});
				}
				/* set data for upload "media_file" (end) */
				
				$("#media_type").val(response.values["media_type"]);
				$("#media_type_stringlength").empty().append($("#media_type").val().length + "/10");
				$("#media_type_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_type").val().length < 240) {
					$("#media_type_stringlength").addClass("text-success");
				} else {
					$("#media_type_stringlength").addClass("text-danger");
				}
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"];?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
								if (response_modules.values.length - i == 1) {
									if (response.values["modules_id"] != "") {
										$("#modules_id").select2("val", response.values["modules_id"]);
									} else {
										$("#modules_id").select2("val", "");
									}
								}
							}
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				if(response.values["media_activate"] == "1" ){
					$("#media_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#media_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Created by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["media_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

				$("#media_action").val("edit");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=edit&media_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&media_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=edit&media_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=edit&media_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=edit&media_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=edit&media_id=" + target_id);
						}
					}
				}

			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("#revision_list_data").empty();
	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_media_edit_log_data",
			parameters: target_id
		},
		success: function(response) {
			if (response.status == true) {

				$("#revision").show();
				if (response.values != null) {

					if (response.values.length > 0) {
						for(var i = 0; i < response.values.length; i++) {
							$("#revision_list_data").append('<a href="javascript: void(0)" class="revision-item" data-target="' + response.values[i]["log_id"] + '" data-content="<?php echo translate("Reverse to"); ?> ' + response.values[i]["log_date_created_formatted"] + '? <br />(<?php echo translate("Your unsaved data will be lost"); ?>)"><i class="si si-reload"></i> ' + response.values[i]["log_date_created_formatted"] + '</a> <em><?php echo translate("Modified by"); ?> <a href="users.php?action=view&users_id=' + response.values[i]["users_id"] + '" target="_blank">' + response.values[i]["users_name"] + '</a></em><br />');
						}
					}
				}

				$(".revision-item").click(function () {
					show_media_modal_prompt($(this).attr("data-content"), "revision_media_open", $(this).attr("data-target"));
				});

			} else if (response.status == false) {
				$("#revision_list_data").append(response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#revision_list_data").append(textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function copy_media_open(target_id) {

	$("#revision").hide();

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	$("#form-footer").empty();

	reset_media_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_media_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Copy") . " " . $title; ?>");

				$("#media_id").val("");
				$("#media_name").val(response.values["media_name"]);
				$("#media_name_stringlength").empty().append($("#media_name").val().length + "/200");
				$("#media_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_name").val().length < 240) {
					$("#media_name_stringlength").addClass("text-success");
				} else {
					$("#media_name_stringlength").addClass("text-danger");
				}
				$("#media_file_old").empty();
				$("#media_file").val("");
				$("#media_type").val(response.values["media_type"]);
				$("#media_type_stringlength").empty().append($("#media_type").val().length + "/10");
				$("#media_type_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_type").val().length < 240) {
					$("#media_type_stringlength").addClass("text-success");
				} else {
					$("#media_type_stringlength").addClass("text-danger");
				}
				$.ajax({
					url: "system/core/<?php echo $configs["version"]; ?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
								if (response_modules.values.length - i == 1) {
									if (response.values["modules_id"] != "") {
										$("#modules_id").select2("val", response.values["modules_id"]);
									} else {
										$("#modules_id").select2("val", "");
									}
								}
							}
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				if(response.values["media_activate"] == "1" ){
					$("#media_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#media_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-content").removeClass("block-opt-refresh");

				$("#media_action").val("copy");

				var limit = $("#datatable_length_selector").val();
				var page = get_url_param().page;
				if (page == null) {
					page = 1;
				}
				var search_text = $("#datatable_search").val();
				var published = get_url_param().published;
				var from = get_url_param().from;
				var to = get_url_param().to;
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&search=" + search_text + "&action=copy&media_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to + "&action=edit&media_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?page=" + page + "&limit=" + limit + "&action=copy&media_id=" + target_id);
						}
					}
				} else {
					if (search_text != "" && search_text != null) {
						window.history.pushState("", "", "<?php echo $module_page_link; ?>?search=" + search_text + "&action=copy&media_id=" + target_id);
					} else {
						if (published != null || from != null || to != null) {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?published=" + published + "&from=" + from + "&to=" + to + "&action=copy&media_id=" + target_id);
						} else {
							window.history.pushState("", "", "<?php echo $module_page_link; ?>?action=copy&media_id=" + target_id);
						}
					}
				}
			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");

}

function view_media_open(target_id) {

	$("#module-table-block").addClass("block-opt-refresh");

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "view_media_table_row",
			parameters: target_id,
			table_module_field: table_module_field
		},
		success: function(response) {
			if (response.status == true) {
				show_media_modal_response(response.status, response.html);

				$("#module-table-block").removeClass("block-opt-refresh");
			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
				$("#module-table-block").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
			$("#module-table-block").removeClass("block-opt-refresh");
		}
	});

}

function revision_media_open(target_id) {

	$("#form-content").css("visibility", "visible");
	$("#form-content").fadeTo("slow", 1);
	$("#form-content").removeClass("block-opt-hidden");
	$("#form-content").fadeIn("slow").slideDown("fast");
	$("#form-content").addClass("block-opt-refresh");

	reset_media_data();

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "get_media_edit_log_data_by_id",
			parameters: target_id
		},
		success: function(response) {

			if (response.status == true) {

				$("#form-title").empty();
				$("#form-title").append("<?php echo translate("Edit") . " " . $title; ?>");

				$("#media_id").val(response.values["media_id"]);
				$("#media_name").val(response.values["media_name"]);
				$("#media_name_stringlength").empty().append($("#media_name").val().length + "/200");
				$("#media_name_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_name").val().length < 240) {
					$("#media_name_stringlength").addClass("text-success");
				} else {
					$("#media_name_stringlength").addClass("text-danger");
				}
				/* set data for upload "media_file" (begin) */
				if (response.values["media_file"] != null && response.values["media_file"] != "") {
					$("#media_file_view_old").empty();
					$("#media_file_view_old").append('<?php echo translate("Old file(s)"); ?><br /><input name="media_file_old" id="media_file_old" type="hidden" value="' + response.values["media_file"] + '">');
						var media_file_name = "";
						if (typeof response.values["media_file_name"] !== "undefined") {
							if (response.values["media_file_name"].length > 13) {
								var media_file_name = response.values["media_file_name"].substring(0, 13) + "...";
							} else {
								var media_file_name = response.values["media_file_name"];
							}
						} else {
							var media_file_name = response.values["media_file"];
						}
						
						
						if (response.values["media_file_name"].match(/.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action datatable-image-popup\" rel=\"media_file\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\" style=\"background-image: url(" + response.values["media_file_path"] + ");\"><img src=\"plugins/drop/images/square-file-images-push.svg\"></div></div>");
						} else if (response.values["media_file_name"].match(/.(jpe|jif|jfif|jfi|jp2|j2k|jpf|jpx|jpm|mj2|tif|tiff|bmp|dip|pbm|pgm|ppm|pnm|svg|webp|heic|heif|raw|bpg)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-images-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(mov|qt|mpg|mpeg|mpe|mpv|mp2|m2v|m4v|mp4|m4v|avi|mpg|wma|flv|f4v|webm|mkv|vob|ogv|rm|rmvb|asf|amv|3gp|3g2|yuv|mng|gifv|drc|svi|nsv)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-video-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(mp3|m4a|ac3|aiff|mid|ogg|o|ga|wav|aa|aac|act|aiff|amr|m4a|m4b|mmf|mpc|tta|fla)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-audio-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(pdf)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-pdf-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(doc|docx|rtf)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-word-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(xls|xlsx|csv)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-excel-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(ppt|pptx)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-powerpoint-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(txt)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-text-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(html|php|css|js|json|sql|xhtml|xml)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-code-push.svg\"></div>");
						} else if (response.values["media_file_name"].match(/.(zip|rar|gz|tar|iso|dmg)$/i)) {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-zip-push.svg\"></div>");
						} else {
							$("#media_file_view_old").append("<div id=\"media_file_old_0\" class=\"col-lg-2 col-md-4 col-sm-3 col-xs-12\" style=\"padding: 0;\"><div class=\"multiple_drop_filname\" style=\"width: 100%;\">" + media_file_name + "</div><div class=\"multiple_drop_actions\" style=\"width: 100%;\"><a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-eye\"></i><\a> <a href=\"" + response.values["media_file_path"] + "\" class=\"btn-action\" title=\"" + response.values["media_file_name"] + "\" download=\"" + response.values["media_file_name"] + "\" target=\"_blank\"><i class=\"si si-cloud-download\"></i><\a> <a href=\"javascript: void(0)\" id=\"file-delete-media_file\" class=\"btn-action\" data-target=\"" + response.values["media_id"] + "\" data-name=\"" + response.values["media_file_name"] + "\" data-file=\"" + response.values["media_file"] + "\" data-container=\"#media_file_old_0\" title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\" data-toggle=\"tooltip\" data-original-title=\"<?php echo translate("Delete"); ?> " + response.values["media_file_name"] + "\"><i class=\"si si-trash\"></i><\a></div><div class=\"multiple_drop_preview_item\"><img src=\"plugins/drop/images/square-file-misc-push.svg\"></div>");
						}
						$("#file-delete-media_file").click(function () {
							$("#modal-prompt").find(".block-content").empty();
							$("#modal-prompt").find(".block-content").append("<?php echo translate("Delete"); ?> <strong><span class=\"icon node-icon fa fa-file-o\"></span> " + $(this).attr("data-name") + "</strong>?");
							$("#modal-prompt").find(".modal-footer").empty();
							$("#modal-prompt").find(".modal-footer").append("<button class=\"btn btn-sm btn-primary\" type=\"button\" data-dismiss=\"modal\" onClick=\"delete_media_file_upload_file('" + $(this).attr("data-file") + "', '" + $(this).attr("data-target") + "', 'media_file', '" + $(this).attr("data-container") + "');\"><i class=\"fa fa-check\"></i> <?php echo translate("Yes"); ?></button><button class=\"btn btn-sm btn-default\" type=\"button\" data-dismiss=\"modal\"><i class=\"fa fa-close\"></i> <?php echo translate("No"); ?></button>");
							$("#modal-prompt").modal("show");
						});
				}
				/* set data for upload "media_file" (end) */
				
				$("#media_type").val(response.values["media_type"]);
				$("#media_type_stringlength").empty().append($("#media_type").val().length + "/10");
				$("#media_type_stringlength").removeClass("text-success").removeClass("text-danger");
				if ($("#media_type").val().length < 240) {
					$("#media_type_stringlength").addClass("text-success");
				} else {
					$("#media_type_stringlength").addClass("text-danger");
				}
				$("#modules_record_target_temp").val(response.values["modules_record_target"]);
				$.ajax({
					url: "system/core/<?php echo $configs["version"]; ?>/modules.php",
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						method: "get_modules_data_all",
					},
					success: function(response_modules) {
						$("#modules_id").empty();
						$("#modules_id").append("<option value=\"\"><?php echo translate("Please select a module"); ?></option>");
						$("#modules_id").append("<option value=\"\" disabled>-</option>");
						if (response_modules.values != null) {
							for(var i = 0; i < response_modules.values.length; i++) {
								$("#modules_id").append("<option value=\"" + response_modules.values[i]["modules_id"] + "\" data-key=\"" + response_modules.values[i]["modules_key"] + "\" data-name=\"" + response_modules.values[i]["modules_name"] + "\">" + response_modules.values[i]["modules_name"] + " (ID: " + response_modules.values[i]["modules_id"] + ")</option>");
							}
						}
						if (response.values["modules_id"] != "") {
							$("#modules_id").select2("val", response.values["modules_id"]);
						} else {
							$("#modules_id").select2("val", "");
						}
						if (response.values["modules_record_target"] != "") {
							$("#modules_record_target").select2("val", response.values["modules_record_target"]);
						} else {
							$("#modules_record_target").select2("val", "");
						}
					}
				});
				if(response.values["media_activate"] == "1" ){
					$("#media_activate").prop("checked", true);
					$("#button-data-submit").removeAttr("formnovalidate");
				} else {
					$("#media_activate").prop("checked", false);
					$("#button-data-submit").attr("formnovalidate", "formnovalidate");
				}

				$("#form-footer").empty();
				$("#form-footer").append("<div class=\"row\"><div class=\"col-xs-12 text-center\"><?php echo translate("Modified by"); ?> <a href=\"users.php?action=view&users_id=" + response.values["users_id"] + "\" target=\"_blank\">" + response.values["users_name"] + "</a> <?php echo translate("on"); ?> " + response.values["media_date_created_formatted"] + "</div></div>");

				$("#form-content").removeClass("block-opt-refresh");

			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
				$("#form-content").removeClass("block-opt-refresh");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
			$("#form-content").removeClass("block-opt-refresh");
		}
	});

	$("html, body").animate({
            scrollTop: $("#module-form").offset().top - 160 + "px"
    }, "fast");
}

function create_media_table() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				pageLength: <?php if (isset($configs["page_limit"]) && !empty($configs["page_limit"])) {echo $configs["page_limit"];} else {echo "20";}?>,
				lengthMenu: [[5, 10, 15, 20, 50, 100], [5, 10, 15, 20, 50, 100]]
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;

			/* Set the defaults for DataTables init */
			jQuery.extend( true, $DataTable.defaults, {
				dom:
					"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				renderer: 'bootstrap',
				oLanguage: {
					sLengthMenu: "_MENU_",
					sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
					oPaginate: {
						sPrevious: '<i class="fa fa-angle-left"></i>',
						sNext: '<i class="fa fa-angle-right"></i>'
					}
				}
			});

			/* Default class modification */
			jQuery.extend($DataTable.ext.classes, {
				sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
				sFilterInput: "form-control",
				sLengthSelect: "form-control"
			});

			/* Bootstrap paging button renderer */
			$DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
				var api     = new $DataTable.Api(settings);
				var classes = settings.oClasses;
				var lang    = settings.oLanguage.oPaginate;
				var btnDisplay, btnClass;

				var attach = function (container, buttons) {
					var i, ien, node, button;
					var clickHandler = function (e) {
						e.preventDefault();
						if (!jQuery(e.currentTarget).hasClass("disabled")) {
							api.page(e.data.action).draw(false);
						}
					};

					for (i = 0, ien = buttons.length; i < ien; i++) {
						button = buttons[i];

						if (jQuery.isArray(button)) {
							attach(container, button);
						}
						else {
							btnDisplay = '';
							btnClass = '';

							switch (button) {
								case 'ellipsis':
									btnDisplay = '&hellip;';
									btnClass = 'disabled';
									break;

								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ? '' : ' disabled');
									break;

								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages - 1 ? '' : ' disabled');
									break;

								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
											'active' : '';
									break;
							}

							if (btnDisplay) {
								node = jQuery('<li>', {
									'class': classes.sPageButton + ' ' + btnClass,
									'aria-controls': settings.sTableId,
									'tabindex': settings.iTabIndex,
									'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId + '_' + button :
											null
								})
								.append(jQuery('<a>', {
										'href': '#'
									})
									.html(btnDisplay)
								)
								.appendTo(container);

								settings.oApi._fnBindAction(
									node, {action: button}, clickHandler
								);
							}
						}
					}
				};

				attach(
					jQuery(host).empty().html('<ul class="pagination"/>').children("ul"),
					buttons
				);
			};

			/*  TableTools Bootstrap compatibility - Required TableTools 2.1+ */
			if ($DataTable.TableTools) {
				/*  Set the classes that TableTools uses to something suitable for Bootstrap */
				jQuery.extend(true, $DataTable.TableTools.classes, {
					"container": "DTTT btn-group",
					"buttons": {
						"normal": "btn btn-default",
						"disabled": "disabled"
					},
					"collection": {
						"container": "DTTT_dropdown dropdown-menu",
						"buttons": {
							"normal": "",
							"disabled": "disabled"
						}
					},
					"print": {
						"info": "DTTT_print_info"
					},
					"select": {
						"row": "active"
					}
				});

				/*  Have the collection use a bootstrap compatible drop down */
				jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
					"collection": {
						"container": "ul",
						"button": "li",

						"liner": "a"
					}
				});
			}
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){ mainDataTable = BaseTableDatatables.init(); });

}

function create_media_table_defer_firsttime() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			var search_text = $("#datatable_search").val();
			var published = get_url_param().published;
			var from = get_url_param().from;
			var to = get_url_param().to;
			if (limit != null) {
				if (search_text != "" && search_text != null) {
					location.href = "?limit=" + limit + "&search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?limit=" + limit + "&published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "?limit=" + limit;
					}
				}
			} else {
				if (search_text != "" && search_text != null) {
					location.href = "?search=" + search_text;
				} else {
					if (published != null || from != null || to != null) {
						location.href = "?published=" + published + "&from=" + from + "&to=" + to;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

		var sortby = get_url_param().sortby;
		var sortdirection = get_url_param().sortdirection;
		var count_tablehead = 0;

		var limit = $("#datatable_length_selector").val();
		var page = get_url_param().page;
		if (page == null) {
			page = 1;
		}
		var search_text = $("#datatable_search").val();
		if (limit != null) {
			if (search_text != "" && search_text != null) {
				var parameters = "?page=" + page + "&limit=" + limit + "&search=" + search_text + "&";
			} else {
				var parameters = "?page=" + page + "&limit=" + limit + "&";
			}
		} else {
			if (search_text != "" && search_text != null) {
				var parameters = "?search=" + search_text + "&";
			} else {
				var parameters = "?";
			}
		}

		$("#datatable > thead  > tr > th").each(function() {

			var tablehead_length = count_tablehead + 1;
			if (tablehead_length != $("#datatable > thead  > tr > th").length) {

				var sortby_each = $(this).attr("data-target");
				if (sortby == null) {
					if (count_tablehead == 0) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
					} else if (count_tablehead == 1) {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
						$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				} else {
					if (sortby == sortby_each) {
						if (sortdirection == "desc") {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
							$(this).append("<?php echo $data_table_info["sort_symbol_down"]; ?>");
						} else {
							$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=desc");
							$(this).append("<?php echo $data_table_info["sort_symbol_up"]; ?>");
						}
					} else {
						$(this).attr("target-click", parameters + "sortby=" + sortby_each + "&sortdirection=asc");
						$(this).append("<?php echo $data_table_info["sort_symbol"]; ?>");
					}
				}

				$(this).css("cursor", "pointer");
				$(this).click(function () {
					location.href = $(this).attr("target-click");
				});
			}
			count_tablehead++;
		});
	});

}

function create_media_table_defer() {

	var BaseTableDatatables = function() {
		/* Init full DataTable */
		var initDataTableFull = function() {
			var createDataTable = jQuery(".js-dataTable-full").dataTable({
				order: [[ 0, "desc" ],[ 1, "asc" ]],
				columnDefs: [ { orderable: false, targets: [ <?php echo $table_module_field_show; ?> ] } ],
				ordering: false,
				bPaginate: false,
				searching: false,
				bInfo : false
			});
			return createDataTable;
		};

		/* DataTables Bootstrap integration */
		var bsDataTables = function() {
			var $DataTable = jQuery.fn.dataTable;
		};

		return {
			init: function() {
				/*  Init Datatables */
				bsDataTables();
				return initDataTableFull();
			}
		};
	}();

	/* Initialize when page loads */
	jQuery(function(){
		mainDataTable = BaseTableDatatables.init();

		$("#datatable_wrapper").prepend("<?php echo $data_table_info["top_panel"]; ?>");

		$("#datatable_length_selector").change(function () {
			var limit = $(this).val();
			if (limit != null) {
				location.href = "?limit=" + limit;
			} else {
				location.href = "" + limit;
			}
		});

		$("#datatable_search").keypress(function(e) {
			if(e.which == 13) {
				var limit = $("#datatable_length_selector").val();
				var search_text = $(this).val();
				if (limit != null) {
					if (search_text != "" && search_text != null) {
						location.href = "?limit=" + limit + "&search=" + search_text;
					} else {
						location.href = "?limit=" + limit;
					}
				} else {
					if (search_text != "" && search_text != null) {
						location.href = "?search=" + search_text;
					} else {
						location.href = "";
					}
				}
			}
		});

		$("#datatable_wrapper").append("<?php echo $data_table_info["foot_panel"]; ?>");

	});

}

function create_media_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "create_media_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			if ($("#datatable-" + data_table["media_id"]).length > 0) {
				$("#datatable").find("#datatable-" + data_table["media_id"]).remove();
			}
			$("#datatable-list").prepend(response.html);
			if (count_data_table < datatable_data_limit) {
				create_media_table();
			} else {
				create_media_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_media_open", $(this).attr("data-target"));
				} else {
					edit_media_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_media_open", $(this).attr("data-target"));
				} else {
					copy_media_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_media_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_media_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_media_modal_prompt($(this).attr("data-content"), "delete_media_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_media_open($(this).attr("data-target"));
			});
		}
	});

}

function update_media_table_row(data_table) {

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "update_media_table_row",
			parameters: data_table,
			table_module_field: table_module_field
		},
		success: function(response) {
			mainDataTable.fnDestroy();
			$("#datatable").find("#datatable-" + data_table["media_id"]).empty();
			$("#datatable").find("#datatable-" + data_table["media_id"]).append(response.html);
			if (count_data_table < datatable_data_limit) {
				create_media_table();
			} else {
				create_media_table_defer();
			}
			$(".btn-edit").unbind();
			$(".btn-edit").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_media_open", $(this).attr("data-target"));
				} else {
					edit_media_open($(this).attr("data-target"));
				}
			});
			$(".btn-copy").unbind();
			$(".btn-copy").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_media_open", $(this).attr("data-target"));
				} else {
					copy_media_open($(this).attr("data-target"));
				}
			});
			$(".btn-translate").unbind();
			$(".btn-translate").click(function () {
				if ($("#form-content").is(":visible")) {
					show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_media_open", $(this).attr("data-target") + "'\, '" + $(this).attr("data-language"));
				} else {
					translate_media_open($(this).attr("data-target"), $(this).attr("data-language"));
				}
			});
			$(".btn-delete").unbind();
			$(".btn-delete").click(function () {
				show_media_modal_prompt($(this).attr("data-content"), "delete_media_data", $(this).attr("data-target"));
			});
			$(".btn-view").unbind();
			$(".btn-view").click(function () {
				view_media_open($(this).attr("data-target"));
			});
		}
	});

}

function delete_media_table_row(field_id) {
	mainDataTable.fnDestroy();
	$("#datatable").find("#datatable-" + field_id).remove();
	if (count_data_table < datatable_data_limit) {
		create_media_table();
	} else {
		create_media_table_defer();
	}
}

function show_media_modal_response(type, message) {
	$("#modal-response").find(".block-header").removeClass("bg-success");
	$("#modal-response").find(".block-header").removeClass("bg-danger");
	if (type == true) {
		$("#modal-response").find(".block-header").addClass("bg-success");
	} else {
		$("#modal-response").find(".block-header").addClass("bg-danger");
	}
	$("#modal-response").find(".block-content").empty();
	$("#modal-response").find(".block-content").append(message);
	$("#modal-response").modal("show");
}

function show_media_modal_prompt(message, function_name, function_parameter) {
	$("#modal-prompt").find(".block-content").empty();
	$("#modal-prompt").find(".block-content").append(message);
	$("#modal-prompt").find(".modal-footer").empty();
	$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> <?php echo translate("Yes"); ?></button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo translate("No"); ?></button>');
	$("#modal-prompt").modal("show");
}

</script>

<script>
$(document).ready(function() {

	$(".btn-add").click(function () {
		if ($("#form-content").is(":visible")) {
			show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "create_media_open", $(this).attr("data-target"));
		} else {
			create_media_open($(this).attr("data-target"));
		}
	});

	$(".btn-edit").click(function () {
		if ($("#form-content").is(":visible")) {
			show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open edit form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "edit_media_open", $(this).attr("data-target"));
		} else {
			edit_media_open($(this).attr("data-target"));
		}
	});

	$(".btn-copy").click(function () {
		if ($("#form-content").is(":visible")) {
			show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "copy_media_open", $(this).attr("data-target"));
		} else {
			copy_media_open($(this).attr("data-target"));
		}
	});

	$(".btn-translate").click(function () {
		if ($("#form-content").is(":visible")) {
			show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current and open create form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "translate_media_open", $(this).attr("data-target") + ", " + $(this).attr("data-language"));
		} else {
			translate_media_open($(this).attr("data-target"), $(this).attr("data-language"));
		}
	});

	$(".btn-delete").click(function () {
		show_media_modal_prompt($(this).attr("data-content"), "delete_media_data", $(this).attr("data-target"));
	});

	$(".btn-view").click(function () {
		view_media_open($(this).attr("data-target"));
	});

	$(".btn-form-close").click(function () {
		show_media_modal_prompt("<?php echo translate("Are you sure you want to leave current form") . "?<br />(" . translate("Your unsaved data will be lost") . ")"; ?>", "form_media_close", "");
	});

	$("#from").datepicker({
		<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "language: \"" . $configs["backend_language"] . "\",";
}
?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$("#to").datepicker({
		<?php
if (file_exists("../../../plugins/locales/bootstrap-datepicker/bootstrap-datepicker." . $configs["backend_language"] . ".min.js")) {
    echo "language: \"" . $configs["backend_language"] . "\",";
}
?>
		weekStart: 1,
		autoclose: true,
		todayHighlight: true,
		format: "yyyy-mm-dd",
	});

	$(".datatable-image-popup").fancybox({
		"autoScale": "auto",
		"padding": 0
	});

	$("#media_activate").click(function () {
		if ($("#media_activate").is(":checked") === true){
			$("#button-data-submit").removeAttr("formnovalidate");
		} else {
			$("#button-data-submit").attr("formnovalidate", "formnovalidate");
		}
	});


	$("#modules_id").select2();
	$("#modules_record_target").select2();
	$("#modules_id").change(function(event){
		show_module_media_content_list(this.value);
	});

	$("#media_name").keyup(function(event){
		$("#media_name_stringlength").empty().append($(this).val().length + "/200");
		$("#media_name_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#media_name_stringlength").addClass("text-success");
		} else {
			$("#media_name_stringlength").addClass("text-danger");
		}
	});
	/* Reset data for upload */
	$("#media_file_old").empty();
	$("#media_file").val("");
	$("#media_type").keyup(function(event){
		$("#media_type_stringlength").empty().append($(this).val().length + "/10");
		$("#media_type_stringlength").removeClass("text-success").removeClass("text-danger");
		if ($(this).val().length < 240) {
			$("#media_type_stringlength").addClass("text-success");
		} else {
			$("#media_type_stringlength").addClass("text-danger");
		}
	});

	$("#form-content").slideUp("fast");

	if (count_data_table < datatable_data_limit) {
		create_media_table();
	} else {
		create_media_table_defer_firsttime();
	}

	/* URL parameters (begin) */
	if (get_url_param().status != null) {
		if (get_url_param().status == "1") {
			show_media_modal_response(true, decodeURI(get_url_param().message));
		} else {
			show_media_modal_response(false, decodeURI(get_url_param().message));
		}
	}
	if (get_url_param().action == "create") {
		create_media_open();
	} else if (get_url_param().action == "edit") {
		if (get_url_param().media_id != null) {
			edit_media_open(get_url_param().media_id);
		}
	} else if (get_url_param().action == "copy") {
		if (get_url_param().media_id != null) {
			copy_media_open(get_url_param().media_id);
		}
	} else if (get_url_param().action == "translate") {
		if (get_url_param().media_id != null) {
			translate_media_open(get_url_param().media_id, get_url_param().media_language);
		}
	} else if (get_url_param().action == "view") {
		if (get_url_param().media_id != null) {
			view_media_open(get_url_param().media_id);
		}
	}
	/* URL response (end) */


	if ($("#media_activate").is(":checked") === true){
		$("#button-data-submit").removeAttr("formnovalidate");
	} else {
		$("#button-data-submit").attr("formnovalidate", "formnovalidate");
	}

	$("#page-container").removeClass("block-opt-refresh");
	$("#page-container").removeClass("block-themed");
	$("#page-container").removeClass("block");

});
</script>
<!-- initialization: JavaScript (end) -->

<script>
function delete_media_file_upload_file(target_file, target_id, target_field, file_div) {

	var form_values   = {
		"target_id": target_id,
		"target_field": target_field,
		"target_file": target_file
	}

	$.ajax({
		url: url,
		type: "POST",
		cache: false,
		dataType: "json",
		data: {
			method: "delete_media_file_upload_file",
			parameters: form_values
		},
		success: function(response) {
			if (response.status == true) {
				$(file_div).slideUp();
				$(file_div).empty();
				$("#datatable-" + target_field + "-" + target_id).empty();
				$("#datatable-" + target_field + "-" + target_id).append(response.values.data_content);
                show_media_modal_response(response.status, response.message);
			} else if (response.status == false) {
				show_media_modal_response(response.status, response.message);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			show_media_modal_response(errorThrown, textStatus);
		}
	});

}
/* 
form validation guides
----------------------
required: "This field is required.",
remote: "Please fix this field.",
email: "Please enter a valid email address.",
url: "Please enter a valid URL.",
date: "Please enter a valid date.",
dateISO: "Please enter a valid date ( ISO ).",
number: "Please enter a valid number.",
digits: "Please enter only digits.",
creditcard: "Please enter a valid credit card number.",
equalTo: "Please enter the same value again.",
maxlength: $.validator.format( "Please enter no more than {0} characters." ),
minlength: $.validator.format( "Please enter at least {0} characters." ),
rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
range: $.validator.format( "Please enter a value between {0} and {1}." ),
max: $.validator.format( "Please enter a value less than or equal to {0}." ),
min: $.validator.format( "Please enter a value greater than or equal to {0}." )
*/

jQuery.validator.addMethod("no_space", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "");

jQuery.validator.addMethod("prefix", function(value, element) {
	var regexp = '^(http://|https://)(.*)';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("suffix", function(value, element) {
	var regexp = '^(.*)\.(####_EXTENSION_SUFFIX_####)$';
	var re = new RegExp(regexp,'i');
	return this.optional(element) || re.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_soft", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_specialchar_hard", function(value, element) {
	var regex = /[^\$\&\+\,\:\;\=\?\@\#\|'\<\>\^\*\(\)\%\!\s\_\-\.]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("lower", function(value, element) {
	var regex = /^[a-z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("upper", function(value, element) {
	var regex = /^[A-Z0-9]+$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("secure_password", function(value, element) {
	var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])[0-9a-zA-Z\W]{0,}$/;
	return this.optional(element) || regex.test(value);
}, "");

jQuery.validator.addMethod("no_number", function(value, element) {
	var regex = /^[0-9]$/;
	return this.optional(element) || regex.test(value);
}, "");

var BaseFormValidation = function() {
	/* Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation */
	var initValidationBootstrap = function(){
		jQuery(".js-validation-bootstrap").validate({
			submitHandler: function(form) {
				submit_media_data();
			},
			errorClass: "help-block animated fadeInDown",
			errorElement: "div",
			errorPlacement: function(error, e) {
				jQuery(e).parents(".form-group > div").append(error);
			},
			highlight: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error").addClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			success: function(e) {
				jQuery(e).closest(".form-group").removeClass("has-error");
				jQuery(e).closest(".help-block").remove();
			},
			rules: {
				"media_name": {
					required: false,
				},
				"media_file": {
					required: true,
				},
				"media_type": {
					required: true,
				},
			 },
			 messages: {
				"media_name": {
					required: "<strong><?php echo translate("Media name"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"media_file": {
					required: "<strong><?php echo translate("File"); ?></strong> <?php echo translate("is required"); ?>",
				},
				"media_type": {
					required: "<strong><?php echo translate("Type"); ?></strong> <?php echo translate("is required"); ?>",
				},
			 }
		});
	};
    return {
        init: function () {
            /* Init Bootstrap Forms Validation */
            initValidationBootstrap();

			/* Init Meterial Forms Validation
			initValidationMaterial(); */
        }
    };
}();

/* Initialize when page loads */
jQuery(function(){ BaseFormValidation.init(); });
</script>

<?php
require_once "templates/" . $configs["backend_template"] . "/footer.php";
?>