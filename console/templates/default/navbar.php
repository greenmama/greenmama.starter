<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full">
	<!-- Header Navigation Right -->
	<ul class="nav-header pull-right">
		<li>
			<div class="btn-group">
				<button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
					<?php 
					if (!empty($_SESSION['users_image'])) {
						if (!empty($_SESSION['users_image_thumbnail_path'])) {
							$users_image = $_SESSION['users_image_thumbnail_path'];
						} else {
							$users_image = $_SESSION['users_image'];
						}
						echo '
						<img src="'.$users_image.'" alt="'.$_SESSION['users_username'].'">';
					} else {
						echo '
						<img src="'.$configs['base_url'].'/media/images/users/thumbnail/_default.jpg" alt="Default">';
					}
					?>
					<?php echo $_SESSION['users_name']; ?>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
					<li>
						<a tabindex="-1" href="<?php echo backend_rewrite_url("account.php"); ?>">
							<i class="si si-settings pull-right"></i><?php echo translate("Account Setting"); ?>
						</a>
					</li>
					<li>
						<a tabindex="-1" href="<?php echo backend_rewrite_url("logout.php"); ?>">
							<i class="si si-logout pull-right"></i><?php echo translate("Log out"); ?>
						</a>
					</li>
				</ul>
			</div>
		</li>
	</ul>
	<!-- END Header Navigation Right -->

	<!-- Header Navigation Left -->
	<ul class="nav-header pull-left">
		<li class="hidden-md hidden-lg">
			<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
			<button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
				<i class="fa fa-navicon"></i>
			</button>
		</li>
		<li class="hidden-xs hidden-sm">
			<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
			<button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
				<i class="fa fa-ellipsis-v"></i>
			</button>
		</li>
	</ul>
	<!-- END Header Navigation Left -->
</header>
<!-- END Header -->