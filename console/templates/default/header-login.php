<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus">
<![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus">
<!--<![endif]-->

<head>

    <meta charset="<?php echo $configs["encoding"]; ?>">

    <title><?php echo $configs['site_name']; ?> | <?php echo $title; ?></title>

    <meta name="description" content="<?php echo $configs['site_name']; ?> Management">
    <meta name="author" content="Bangkok Elite Marketing">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- include: favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/manifest.json">
    <link rel="mask-icon" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/safari-pinned-tab.svg" color="#58585a">

   	<!-- include: system styles -->
    <link href="plugins/select2/select2.min.css" rel="stylesheet">
    <link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">

    <!-- include: template styles -->
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/backend-font.css" rel="stylesheet">
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/bootstrap.min.css" rel="stylesheet">
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/greenmama.css" rel="stylesheet" id="css-main">
    <link href="https://fonts.googleapis.com/css?family=Kanit:200,300,400,500,600,700" rel="stylesheet">

    <!-- include: custom styles -->
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/style.css" rel="stylesheet">

    <!-- include: system scripts -->
    <script src="plugins/jquery.min.js"></script>
    <script src="plugins/bootstrap.min.js"></script>
    <script src="plugins/jquery.slimscroll.min.js"></script>
    <script src="plugins/jquery.scrollLock.min.js"></script>
    <script src="plugins/jquery.appear.min.js"></script>
    <script src="plugins/jquery.countTo.min.js"></script>
    <script src="plugins/jquery.placeholder.min.js"></script>
    <script src="plugins/js.cookie.min.js"></script>
    <script src="scripts/app.js"></script>

</head>

<body>