<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus">
<![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus" lang="<?php echo strtolower($configs["backend_language"]); ?>">
<!--<![endif]-->
<head>

    <meta charset="<?php echo $configs["encoding"]; ?>">

    <title><?php echo $configs['site_name']; ?> | <?php echo $title; ?></title>

    <meta name="description" content="<?php echo $configs['site_name']; ?> Management">
    <meta name="author" content="Bangkok Elite Marketing">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta name="viewport" content="width=device-width">

    <!-- include: favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/manifest.json">
    <link rel="mask-icon" href="templates/<?php echo $configs['backend_template']; ?>/images/favicons/safari-pinned-tab.svg" color="#58585a">
    <meta name="theme-color" content="#000000">

    <!-- include: system styles -->
	<link href="plugins/select2/select2.min.css" rel="stylesheet">
	<link href="plugins/select2/select2-bootstrap.min.css" rel="stylesheet">
	<link href="plugins/slick/slick.min.css" rel="stylesheet">
	<link href="plugins/nprogress/nprogress.css" rel="stylesheet">

    <!-- include: template styles -->
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/bootstrap.min.css" rel="stylesheet">
	<link id="css-main" href="templates/<?php echo $configs['backend_template']; ?>/styles/greenmama.css" rel="stylesheet">
   	<link href="templates/<?php echo $configs['backend_template']; ?>/styles/backend-font.css" rel="stylesheet">
   	<link href="https://fonts.googleapis.com/css?family=Kanit:200,300,400,500,600,700" rel="stylesheet">

   	<!-- include: custom styles -->
    <link href="templates/<?php echo $configs['backend_template']; ?>/styles/style.css" rel="stylesheet">

    <!-- include: system scripts -->
    <script src="plugins/jquery.min.js"></script>
    <script src="plugins/bootstrap.min.js"></script>
    <script src="plugins/jquery.slimscroll.min.js"></script>
    <script src="plugins/jquery.scrollLock.min.js"></script>
    <script src="plugins/jquery.appear.min.js"></script>
    <script src="plugins/jquery.countTo.min.js"></script>
    <script src="plugins/jquery.placeholder.min.js"></script>
	<script src="plugins/js.cookie.min.js"></script>
	<script src="plugins/nprogress/nprogress.js"></script>
    <script src="scripts/app.js"></script>
	<script src="scripts/environments.js"></script>

   	<!-- include: template scripts -->
    <script src="templates/<?php echo $configs['backend_template']; ?>/scripts/prefixfree.min.js"></script>



</head>

<body class="modal-open">



<!-- Page Container -->
<!--
Available Classes:

"enable-cookies"             Remembers active color theme between pages (when set through color theme list)

"sidebar-l"                  Left Sidebar and right Side Overlay
"sidebar-r"                  Right Sidebar and left Side Overlay
"sidebar-mini"               Mini hoverable Sidebar (> 991px)
"sidebar-o"                  Visible Sidebar by default (> 991px)
"sidebar-o-xs"               Visible Sidebar by default (< 992px)

"side-overlay-hover"         Hoverable Side Overlay (> 991px)
"side-overlay-o"             Visible Side Overlay by default (> 991px)

"side-scroll"                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

"header-navbar-fixed"        Enables fixed header
-->
<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

<script>
NProgress.configure({ showSpinner: false });
NProgress.start();
</script>

<!-- Main Container -->
<main id="main-container">