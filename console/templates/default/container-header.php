<!-- TITLE -->
<!-- Page Header -->
<div class="content bg-white-op">
	<div class="row items-push">
		<div class="col-sm-7">
			<h1 class="page-heading">
				<?php echo $title; ?>
			</h1>
		</div>
		<div class="col-sm-5 text-right hidden-xs">
			<ol class="breadcrumb push-10-t">
				<li><a class="link-effect" href="./" target="_parent"><?php echo translate("Home"); ?></a></li>
				<li><a class="link-effect" href="<?php echo $module_page_link; ?>" target="_parent"><?php echo $title; ?></a></li>
			</ol>
		</div>
	</div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">