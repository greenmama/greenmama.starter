<!-- Sidebar -->
<nav id="sidebar">
	<!-- Sidebar Scroll Container -->
	<div id="sidebar-scroll">
		<!-- Sidebar Content -->
		<!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
		<div class="sidebar-content">

			<!-- Side Header -->
			<div class="side-header side-content text-center">
				<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
				<button class="btn btn-default text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close" style="margin-top: 6px;">
					<i class="fa fa-times"></i>
				</button>
				<!-- Themes functionality initialized in App() -> uiHandleTheme() -->
				<div class="btn-group pull-right">
				</div>
				<a class="h5 text-white" href="./">
					<div class="logo-sidebar">
					&nbsp;
					</div>
				</a>
			</div>
			<!-- END Side Header -->

			<!-- Side Content -->
			<div class="side-content">
				<ul class="nav-main">					

					<li>
						<a href="./" title="" <?php echo menu_active('index.php', $module_page);?>>
							<div class="sidebar-icon"><i class="glyphicon glyphicon-dashboard"></i></div>
						<span class="sidebar-mini-hide"><?php echo translate("Dashboard"); ?></span>
						</a>
					</li>
					
<?php 
$modules = get_modules_data_all("", "", "modules_order", "asc", 1);
$html_category_last;
$html_category_activate = 0;
$html_subject_last;
$html_subject_activate = 0;

for($i = 0; $i < count($modules); $i++) {
	$modules[$i]['modules_rendered'] = "0";
}

for($i = 0; $i < count($modules); $i++) {

	if (authentication_check_modules($_SESSION['users_id'], $modules[$i]['modules_link']) && $modules[$i]['modules_link'] != "modules.php" && $modules[$i]['modules_link'] != "users.php" && $modules[$i]['modules_link'] != "groups.php" && $modules[$i]['modules_link'] != "languages.php" && $modules[$i]['modules_link'] != "pages.php" && $modules[$i]['modules_link'] != "media.php" && $modules[$i]['modules_link'] != "log.php" && $modules[$i]['modules_link'] != "api.php" && $modules[$i]['modules_link'] != "abstract-modules.php" && $modules[$i]['modules_link'] != "abstract-references.php" && $modules[$i]['modules_link'] != "abstract-spread-inputs.php" && $modules[$i]['modules_rendered'] != "1") {

		if (!empty($modules[$i]['modules_category'])) {
			$modules_category = $modules[$i]['modules_category'];
			$category_render = 0;
			for($j = 0; $j < count($modules); $j++) {

				if ($category_render <= 0) {
					echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate($modules_category).'</span></li>';
					$category_render = $category_render + 1;
				}

				if ($modules_category == $modules[$j]['modules_category']) {

					if (!empty($modules[$j]['modules_subject'])) {
						$modules_subject = $modules[$j]['modules_subject'];
						$modules_subject_icon = $modules[$j]['modules_subject_icon'];
						$subject_render = 0;
						$subject_modules_link = "";
						$html_inside = "";
						for($k = 0; $k < count($modules); $k++) {

							if (menu_open($modules[$k]['modules_link'], $module_page) != '' && $modules_subject == $modules[$k]['modules_subject']) {
								$subject_modules_link = menu_open($modules[$k]['modules_link'], $module_page);
							}

							if (authentication_check_modules($_SESSION['users_id'], $modules[$k]['modules_link']) && $modules[$k]['modules_link'] != "modules.php" && $modules[$k]['modules_link'] != "users.php" && $modules[$k]['modules_link'] != "groups.php" && $modules[$k]['modules_link'] != "languages.php" && $modules[$k]['modules_link'] != "pages.php" && $modules[$k]['modules_link'] != "media.php" && $modules[$k]['modules_link'] != "log.php" && $modules[$k]['modules_rendered'] != "1") {

								if ($modules_subject == $modules[$k]['modules_subject']) {

									$html_inside .= '
						<li>
							<a href="'. backend_rewrite_url($modules[$k]['modules_link']) . '" title="'.translate($modules[$k]['modules_name']).'" '.menu_active($modules[$k]['modules_link'], $module_page).' >
							<div class="sidebar-icon">'.$modules[$k]['modules_icon'].'</div>
							<div class="sidebar-label"><span class="sidebar-mini-hide">'.translate($modules[$k]['modules_name']).'</span></div>
							</a>
						</li>';

									$modules[$k]['modules_rendered'] = "1";
									$subject_render = $subject_render + 1;

								}

							}

						}

						if ($subject_render > 0) {
							echo '
						<li '.$subject_modules_link.'>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#">
						<div class="sidebar-icon">'.$modules_subject_icon.'</div>
						<div class="sidebar-label">'.translate($modules_subject).'</span></div>
						</a>
						<ul>';
						}

						echo $html_inside;

						if ($subject_render > 0) {
							echo '
						</ul>
						</li>';
						}

					} else {

						if (authentication_check_modules($_SESSION['users_id'], $modules[$j]['modules_link']) && $modules[$j]['modules_link'] != "modules.php" && $modules[$j]['modules_link'] != "users.php" && $modules[$j]['modules_link'] != "groups.php" && $modules[$j]['modules_link'] != "languages.php" && $modules[$j]['modules_link'] != "pages.php" && $modules[$j]['modules_link'] != "media.php" && $modules[$j]['modules_link'] != "log.php" && $modules[$j]['modules_rendered'] != "1") {

							echo '
							<li>
								<a href="'. backend_rewrite_url($modules[$j]['modules_link']) . '" title="'.translate($modules[$j]['modules_name']).'" '.menu_active($modules[$j]['modules_link'], $module_page).' >
								<div class="sidebar-icon">'.$modules[$j]['modules_icon'].'</div>
								<span class="sidebar-mini-hide">'.translate($modules[$j]['modules_name']).'</span>
								</a>
							</li>';
							$modules[$j]['modules_rendered'] = "1";

						}

					}

				}

			}

			if ($category_render > 0) {
					echo '
				';
			}

		} else {

			if (!empty($modules[$i]['modules_subject'])) {
				$modules_subject = $modules[$i]['modules_subject'];
				$modules_subject_icon = $modules[$i]['modules_subject_icon'];
				$subject_render = 0;
				$subject_modules_link = "";
				for($k = 0; $k < count($modules); $k++) {

					if (menu_open($modules[$k]['modules_link'], $module_page) != '' && $modules_subject == $modules[$k]['modules_subject']) {
						$subject_modules_link = menu_open($modules[$k]['modules_link'], $module_page);
					}

					if (authentication_check_modules($_SESSION['users_id'], $modules[$k]['modules_link']) && $modules[$k]['modules_link'] != "modules.php" && $modules[$k]['modules_link'] != "users.php" && $modules[$k]['modules_link'] != "groups.php" && $modules[$k]['modules_link'] != "languages.php" && $modules[$k]['modules_link'] != "pages.php" && $modules[$k]['modules_link'] != "media.php" && $modules[$k]['modules_link'] != "log.php" && $modules[$k]['modules_rendered'] != "1") {

						if ($modules_subject == $modules[$k]['modules_subject']) {

							$html_inside .= '
				<li>
					<a href="'. backend_rewrite_url($modules[$k]['modules_link']) . '" title="'.translate($modules[$k]['modules_name']).'" '.menu_active($modules[$k]['modules_link'], $module_page).' >
					<div class="sidebar-icon">'.$modules[$k]['modules_icon'].'</div>
					<span class="sidebar-mini-hide">'.translate($modules[$k]['modules_name']).'</span>
					</a>
				</li>';

							$subject_render = $subject_render + 1;
							$modules[$k]['modules_rendered'] = "1";

						}

					}

				}

				if ($subject_render > 0) {
					echo '
				<li '.$subject_modules_link.'>
				<a class="nav-submenu" data-toggle="nav-submenu" href="#">'.$modules_subject_icon.'
				<span class="sidebar-mini-hide">'.ucfirst($modules_subject).'</span>
				</a>
				<ul>';
				}

				echo $html_inside;

				if ($subject_render > 0) {
					echo '
				</ul>
				</li>';
				}

			} else {

				if (authentication_check_modules($_SESSION['users_id'], $modules[$i]['modules_link']) && $modules[$i]['modules_link'] != "modules.php" && $modules[$i]['modules_link'] != "users.php" && $modules[$i]['modules_link'] != "groups.php" && $modules[$i]['modules_link'] != "languages.php" && $modules[$i]['modules_link'] != "pages.php" && $modules[$i]['modules_link'] != "media.php" && $modules[$i]['modules_link'] != "log.php" && $modules[$i]['modules_rendered'] != "1") {

					echo '
					<li>
						<a href="' . backend_rewrite_url($modules[$i]['modules_link']) . '" title="'.translate($modules[$i]['modules_name']).'" '.menu_active($modules[$i]['modules_link'], $module_page).' >
						<div class="sidebar-icon">'.$modules[$i]['modules_icon'].'</div>
						<span class="sidebar-mini-hide">'.translate($modules[$i]['modules_name']).'</span>
						</a>
					</li>';
					$modules[$i]['modules_rendered'] = "1";

				}

			}

		}


	}


}

if (authentication_check_modules($_SESSION['users_id'],"media.php") || authentication_check_modules($_SESSION['users_id'],"pages.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {

	echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate("Files").'</span></li>';

	if (authentication_check_modules($_SESSION['users_id'], "media.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
		echo '
					<li>
						<a href="' . backend_rewrite_url("media.php") . '" title="" '.menu_active('media.php', $module_page).' '.menu_active('media.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-picture"></i></div>
						<span class="sidebar-mini-hide">'.translate("Media").'</span>
						</a>
					</li>';
	}

	if (authentication_check_modules($_SESSION['users_id'], "pages.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
		echo '
					<li>
						<a href="' . backend_rewrite_url("pages.php") . '" title="'.translate("Pages").'" '.menu_active('pages.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-docs"></i></div>
						<span class="sidebar-mini-hide">'.translate("Pages").'</span>
						</a>
					</li>';
	}

}

if (authentication_check_modules($_SESSION['users_id'],"users.php") || authentication_check_modules($_SESSION['users_id'],"groups.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
	echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate("Administrator").'</span></li>';

	if (authentication_check_modules($_SESSION['users_id'], "users.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
		echo '
					<li>
						<a href="' . backend_rewrite_url("users.php") . '" title="'.translate("Users").'" '.menu_active('users.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-user"></i></div>
						<span class="sidebar-mini-hide">'.translate("Users").'</span>
						</a>
					</li>';
	}

	if (authentication_check_modules($_SESSION['users_id'], "groups.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
		echo '
					<li>
						<a href="' . backend_rewrite_url("groups.php") . '" title="'.translate("Groups").'" '.menu_active('groups.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-users"></i></div>
						<span class="sidebar-mini-hide">'.translate("Groups").'</span>
						</a>
					</li>';
	}

}

if (authentication_check_modules($_SESSION['users_id'],"log.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {

	echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate("Maintenance").'</span></li>';


	if (authentication_check_modules($_SESSION['users_id'], "log.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {
		echo '
					<li>
						<a href="'. backend_rewrite_url("log.php") . '" title="'.translate("Log").'" '.menu_active('log.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-clock"></i></div>
						<span class="sidebar-mini-hide">'.translate("Log").'</span>
						</a>
					</li>';
	}

}

if (authentication_check_modules($_SESSION['users_id'],"languages.php") || authentication_check_modules($_SESSION['users_id'],"modules.php") || $_SESSION['users_id'] == "1") {

	echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate("System").'</span></li>';

	if (authentication_check_modules($_SESSION['users_id'],"languages.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
		echo '

					<li>
						<a href="' . backend_rewrite_url("languages.php") . '" title="'.translate("Languages").'" '.menu_active('languages.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-speech"></i></div>
						<span class="sidebar-mini-hide">'.translate("Languages").'</span>
						</a>
					</li>';
	}

	if ((authentication_check_modules($_SESSION['users_id'],"modules.php")) || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
		echo '
					<li>
						<a href="' . backend_rewrite_url("modules.php") . '" title="'.translate("Modules").'" '.menu_active('modules.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-grid"></i></div>
						<span class="sidebar-mini-hide">'.translate("Modules").'</span>
						</a>
					</li>';

	}

}

if (authentication_check_modules($_SESSION['users_id'],"api.php") || authentication_check_modules($_SESSION['users_id'],"abstract-modules.php") || authentication_check_modules($_SESSION['users_id'],"abstract-references.php") || authentication_check_modules($_SESSION['users_id'],"abstract-spread-inputs.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {

	echo '
					<li class="nav-main-heading"><span class="sidebar-mini-hide">'.translate("Developer").'</span></li>';

	if (authentication_check_modules($_SESSION['users_id'],"abstract-modules.php") || authentication_check_modules($_SESSION['users_id'],"abstract-references.php") || authentication_check_modules($_SESSION['users_id'],"abstract-spread-inputs.php") || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {

		if ($module_page == "abstract-modules.php" || $module_page == "abstract-references.php" || $module_page == "abstract-spread-inputs.php") {
			$subject_abstract_link = ' class="open"';
		}
				
		echo '
						<li'.$subject_abstract_link.'>
						<a class="nav-submenu" data-toggle="nav-submenu" href="#">
						<div class="sidebar-icon"><i class="si si-layers"></i></div>
						<div class="sidebar-label">'.translate("Creator").'</span></div>
						</a>
						<ul>';
				
		if ((authentication_check_modules($_SESSION['users_id'],"abstract-modules.php")) || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
			echo '
									<li>
										<a href="' . backend_rewrite_url("abstract-modules.php") . '" title="'.translate("Modules").'" '.menu_active('abstract-modules.php', $module_page).'>
										<div class="sidebar-icon"><i class="si si-grid"></i></div>
										<span class="sidebar-mini-hide">'.translate("Modules Abstract").'</span>
										</a>
									</li>';
				
		}
				
		if ((authentication_check_modules($_SESSION['users_id'],"abstract-references.php")) || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
			echo '
									<li>
										<a href="' . backend_rewrite_url("abstract-references.php") . '" title="'.translate("References").'" '.menu_active('abstract-references.php', $module_page).'>
										<div class="sidebar-icon"><i class="fa fa-database"></i></div>
										<span class="sidebar-mini-hide">'.translate("References").'</span>
										</a>
									</li>';
				
		}
				
		if ((authentication_check_modules($_SESSION['users_id'],"abstract-spread-inputs.php")) || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
			echo '
									<li>
										<a href="' . backend_rewrite_url("abstract-spread-inputs.php") . '" title="'.translate("Spread Inputs").'" '.menu_active('abstract-spread-inputs.php', $module_page).'>
										<div class="sidebar-icon"><i class="fa fa-list-ol"></i></div>
										<span class="sidebar-mini-hide">'.translate("Spread Inputs").'</span>
										</a>
									</li>';
				
		}
				
		echo '
						</ul>
						</li>';
				
	}

	if ((authentication_check_modules($_SESSION['users_id'],"api.php")) || $_SESSION['groups_id'] == "1" || $_SESSION['users_id'] == "1") {	
		echo '
					<li>
						<a href="' . backend_rewrite_url("api.php") . '" title="'.translate("API").'" '.menu_active('api.php', $module_page).'>
						<div class="sidebar-icon"><i class="si si-share"></i></div>
						<span class="sidebar-mini-hide">'.translate("API").'</span>
						</a>
					</li>';

	}

}
?>

							
				</ul>
			</div>
			<!-- END Side Content -->
		</div>
		<!-- Sidebar Content -->
	</div>
	<!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->