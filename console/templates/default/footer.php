</main>

<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<a class="font-w600" href="<?php echo $configs["base_url"]; ?>" target="_blank"><?php echo $configs["site_name"]; ?></a> © <span class="js-year-copy"></span>
	</div>
</footer>

<script>
$(document).ready(function() {
	NProgress.done();
	$("body").removeClass("modal-open");
});
</script>



</div>
<!-- END Page Container -->

</body>
</html>