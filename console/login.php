<?php 
require_once("system/include.php");

$facebook_credential_login = array(
  'app_id' => $configs["backend_facebook_app_id"],
  'app_secret' => $configs["backend_facebook_app_secret"],
  'redirect_url' => $configs["backend_facebook_app_redirect_url"],
  'scope' => explode(",", $configs["backend_facebook_app_scope"]),
);
$line_credential_login = array(
  'app_id' => $configs["backend_line_app_id"],
  'app_secret' => $configs["backend_line_app_secret"],
  'redirect_url' => $configs["backend_line_app_redirect_url"],
  'scope' => str_replace(",", " ", $configs["backend_line_app_scope"]),
);

/* configurations: Login */
$users = get_modules_data_by_id("6");
if (isset($users) && !empty($users)) {
	$title = translate($users["modules_name"]);
	$module_page = $users["modules_link"];
	$module_function_page = "system/".$users["modules_function_link"];
} else {
	$title = translate("Login");
	$module_page = "login.php";
	$module_function_page = "system/core/".$configs["version"]."/users.php";
}

/* configurations: local */
mb_internal_encoding($configs["encoding"]);
date_default_timezone_set($configs["timezone"]);
?>

<?php 
include("templates/".$configs["backend_template"]."/header-login.php");
?>

<div class="bg-login bg-image"></div>

<!-- Login Content -->
<div class="content overflow-hidden">
<div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
        <!-- Login Block -->
        <div class="block block-themed animated fadeIn">
            <div class="block-header bg-primary">
                <ul class="block-options">
                    <li>
                        <a href="./forgot-password.php">Forgot Password?</a>
                    </li>
                </ul>
                <h3 class="block-title">Login</h3>
            </div>
            <div class="block-content block-content-full block-content-narrow">
                <!-- Login Title -->
                <div class="text-center">
                <div class="logo-login">
                &nbsp;
                </div>
                <!--
                <p class="text-muted push-15-t"><?php echo translate("Administrator's Area"); ?></p>
                -->
            </div>
            <!-- END Login Title -->

            <!-- Login Form -->
            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
            <form class="js-validation-login form-horizontal push-30-t" id="login-form" name="login-form" method="post" enctype="multipart/form-data">
                  <?php $form_spam_protection = form_spam_protection(); ?>
                   <input name="redirecturl" id="redirecturl" type="hidden" value="<?php echo $redirecturl; ?>" />
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="username"><?php echo translate("Username"); ?></label>
                        <input class="form-control" type="text" id="username" name="username">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="password"><?php echo translate("Password"); ?></label>
                        <input class="form-control" type="password" id="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="css-input switch switch-sm switch-primary">
                            <input type="checkbox" id="remember_login" name="remember_login" value="1"><span></span> <?php echo translate("Remember me"); ?>
                        </label>
                    </div>
                </div>
                <div class="form-group push-30-t">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <button class="btn btn-sm btn-block btn-primary" type="submit" name="login_<?php echo $form_spam_protection; ?>" value="Log In"><?php echo translate("Login"); ?></button>
                    </div>
                </div>
                <?php 
                if (isset($configs["backend_facebook_app_id"]) && !empty($configs["backend_facebook_app_id"]) && isset($configs["backend_facebook_app_secret"]) && !empty($configs["backend_facebook_app_secret"])) { 
                ?>
                <div class="form-group push-10-t">
                    <div class="col-md-12 text-center">
                        <?php
                        $facebook_login_url = create_api_connect_link("Facebook", $facebook_credential_login);
                        ?>
                        <button class="btn btn-sm btn-block btn-primary" type="button" id="button-login-facebook" name="button-login-facebook" data-target="<?php echo $facebook_login_url;?>" value="Login"><i class="fa fa-facebook-square" style="margin-right: 10px;"></i> <?php echo translate("Login with Facebook"); ?></button>
                    </div>
                </div>
                <?php 
                }
                ?>
                <?php 
                if (isset($configs["backend_line_app_id"]) && !empty($configs["backend_line_app_id"]) && isset($configs["backend_line_app_secret"]) && !empty($configs["backend_line_app_secret"])) { 
                ?>
                <div class="form-group push-5-t">
                    <div class="col-md-12 text-center">
                        <?php
                        $line_login_url = create_api_connect_link("Line", $line_credential_login);
                        ?>
                        <button class="btn btn-sm btn-block btn-primary" type="button" id="button-login-line" name="button-login-line" data-target="<?php echo $line_login_url;?>" value="Login"><?php echo translate("Login with Line"); ?></button>
                    </div>
                </div>
                <?php 
                }
                ?>
               
            </form>
            <!-- END Login Form -->
            </div>
        </div>
        <!-- END Login Block -->
    </div>
</div>
</div>
<!-- END Login Content -->

<!-- notification response function -->
<div class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-slideup">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo $title; ?></h3>
            </div>
            <div class="block-content">

            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
        </div>
    </div>
</div>
</div>
<!-- end notification response function -->

<!-- Prompt confirmation -->
<div class="modal fade" id="modal-prompt" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-slideup">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-warning">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title"><?php echo $title; ?></h3>
            </div>
            <div class="block-content">
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
</div>
<!-- End Prompt confirmation -->

<!-- Login Footer -->
<div class="push-10-t text-center animated fadeInUp">
<small class="text-muted text-copyright">
    <?php echo strtoupper($translations["en"]["copyright"]); ?>  &copy; 
    <span class="js-year-copy"></span>, 
    <a class="font-w600" href="<?php echo $configs['website_url']; ?>" target="_blank">
    <?php echo $configs['site_name']; ?>
    </a>
</small>
<br /><br />
</div>
<!-- END Login Footer -->

<!-- include: JavaScript (begin) -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<!-- include: JavaScript (end) -->

<!-- initialization: JavaScript (begin) -->
<script>
/* JavaScript variable for AJAX function file */
var url = "<?php echo $module_function_page; ?>";
</script>
<!-- initialization: JavaScript (end) -->

<script>
$(document).ready(function() {

$("#form-content").css("visibility", "visible");
$("#form-content").fadeTo("slow", 1);
$("#form-content").removeClass("block-opt-hidden");
$("#form-content").removeClass("block-opt-refresh");
$("#form-content").fadeIn("slow").slideDown("fast");

$("#username").change(function () {
    $("#<?php echo $form_spam_protection; ?>").val("footprint");
});

$("#password").change(function () {
    $("#<?php echo $form_spam_protection; ?>").val("footprint");
});

$(document).keydown(function (e){
    if(e.keyCode == 13 || e.keyCode == 9){
        $("#<?php echo $form_spam_protection; ?>").val("");
    }
});

$(document).mousemove(function () {
    $("#<?php echo $form_spam_protection; ?>").val("");
});

$(document).scroll(function () {
    $("#<?php echo $form_spam_protection; ?>").val("");
});

$("#button-login-facebook").click(function () {
    location.href = $(this).data("target");
});

$("#button-login-line").change(function () {
    location.href = $(this).data("target");
});

});

function submit_users_data() {

$("#form-content").addClass("block-opt-refresh");

if ($("#remember_login").is(":checked") === true){
    var remember_login = "1";
} else {
    var remember_login = "";
}

var form_data = new FormData(document.querySelector("#login-form"));
form_data.append("method", "login");
form_data.append("remember_login", remember_login);

$.ajax({
    url: url,
    type: "POST",
    cache: false,
    dataType: "json",
    data: form_data,
    processData: false,
    contentType: false,
    success: function(response) {
        if (response.status == true) {
            location.href = "./";
        } else if (response.status == "violent") {
            show_login_modal_response(response.status, response.message);
            $(".btn").click(function () {
                location.reload();
            });
            $("#form-content").removeClass("block-opt-refresh");
        } else {
            show_login_modal_response(response.status, response.message);
            $("#form-content").removeClass("block-opt-refresh");
        }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
        show_login_modal_response(errorThrown, textStatus + ": " + errorThrown);
        $("#form-content").removeClass("block-opt-refresh");
    }
});

}

function show_login_modal_response(type, message) {
$("#modal-response").find(".block-header").removeClass("bg-success");
$("#modal-response").find(".block-header").removeClass("bg-danger");
if (type == true) {
    $("#modal-response").find(".block-header").addClass("bg-success");
} else {
    $("#modal-response").find(".block-header").addClass("bg-danger");
}
$("#modal-response").find(".block-content").empty();
$("#modal-response").find(".block-content").append(message);
$("#modal-response").modal("show");
}


function show_login_modal_prompt(message, function_name, function_parameter) {
$("#modal-prompt").find(".block-content").empty();
$("#modal-prompt").find(".block-content").append(message);
$("#modal-prompt").find(".modal-footer").empty();
$("#modal-prompt").find(".modal-footer").append('<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" onClick="' + function_name + '(\'' + function_parameter + '\');"><i class="fa fa-check"></i> Yes</button><button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> No</button>');
$("#modal-prompt").modal("show");
}
</script>

<script>
var BasePagesLogin = function() {
// Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
var initValidationLogin = function(){
    jQuery('.js-validation-login').validate({
        submitHandler: function(form) {
            $("#<?php echo $form_spam_protection; ?>").val("");
            submit_users_data();
        },
        errorClass: 'help-block text-right animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'login': {
                required: true,
            },
            'password': {
                required: true
            }
        },
        messages: {
            'login': {
                required: 'Please enter a username',
            },
            'password': {
                required: 'Please provide a password',
            }
        }
    });
};

return {
    init: function () {
        // Init Login Form Validation
        initValidationLogin();
    }
};
}();

// Initialize when page loads
jQuery(function(){ BasePagesLogin.init(); });
</script>

<?php 
include("templates/".$configs["backend_template"]."/footer-login.php");
?>