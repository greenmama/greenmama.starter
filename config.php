<?php
/* Configurations */
$configs = array(

    /* API settings */
    "api_key" => "",
    "api_secret" => "",

    "allowed_remote_address" => "127.0.0.1,::1,localhost",

    /* Basic settings */
    "site_name" => "Starter",
    "base_url" => "http://localhost/starter",

    "language" => "en",
    "backend_language" => "en",

    "translate" => true,
    "backend_translate" => true,
    "translate_value" => true,

    "template" => "default",
    "backend_template" => "default",

    /* Contact */
    "email" => "",
    "phone" => "",
    "address" => "",

    /* Social links */
    "facebook" => "",
    "twitter" => "",
    "instagram" => "",
    "google+" => "",
    "linkedin" => "",
    "pinterest" => "",
    "youtube" => "",
    "ask.fm" => "",
    "tumblr" => "",
    "foursquare" => "",

    /* Regional settings */
    "encoding" => "UTF-8",
    "timezone" => "Asia/Bangkok",

    /* Google analytics code */
    "google_analytics" => "",

    /* Facebook analytics code */
    "facebook_analytics" => "",

    /* Google webmaster tools */
    "google_site_verification" => "",

    /* Bing webmaster tools */
    "bing_site_verification" => "",

    /* Facebook Authentication App Configurations */
    "facebook_app_id" => "",
    "facebook_app_secret" => "",
    "facebook_app_redirect_url" => "",
    "facebook_app_scope" => "email", //id,name,email

    /* Google Authentication App Configurations */
    "google_app_api_key" => "",
    "google_app_client_config_path" => "",
    "google_app_redirect_url" => "",
    "google_app_scope" => "", //profile,email,openid

    /* Twitter Authentication App Configurations */
    "twitter_app_id" => "",
    "twitter_app_secret" => "",
    "twitter_app_redirect_url" => "",
    "twitter_app_scope" => "",

    /* Line Authentication App Configurations */
    "line_app_id" => "",
    "line_app_secret" => "",
    "line_app_redirect_url" => "",
    "line_app_scope" => "", //openid,profile

    /* Instagram Authentication App Configurations */
    "instagram_app_id" => "",
    "instagram_app_secret" => "",
    "instagram_app_redirect_url" => "",
    "instagram_app_scope" => "",

    /* Microsoft Authentication App Configurations */
    "microsoft_app_id" => "",
    "microsoft_app_secret" => "",
    "microsoft_app_redirect_url" => "",
    "microsoft_app_scope" => "",

    /* Pagination */
    "page_limit" => 10,
    "page_parameter_name" => "page",

    /* Data encryption */
    "encrypt_key" => "1234567890", //More than 8 characters

    /* Environments */
    "url_rewritting" => false,
    "compression" => false,
    "online" => true,

    /* Log */
    "log" => true, //This could reduce query time
    "backend_log" => true, //This could reduce query time

    /* Password */
    "password_salt" => "",

    /* Admin settings */
    "backend_url" => "console",
    "max_upload_size" => 100,
    "datatable_data_limit" => 100,
    "datatable_page_limit" => 20, //5, 10, 15, 20, 100

    /* Backend Facebook Authentication App ID */
    "backend_facebook_app_id" => "",
    "backend_facebook_app_secret" => "",
    "backend_facebook_app_redirect_url" => "",
    "backend_facebook_app_scope" => "", //id,name,email

    /* Backend Google Authentication App Configurations */
    "backend_google_app_api_key" => "",
    "backend_google_app_client_config_path" => "",
    "backend_google_app_redirect_url" => "",
    "backend_google_app_scope" => "", //userinfo.profile,userinfo.email,userinfo.openid

    /* Backend Twitter Authentication App Configurations */
    "backend_twitter_app_id" => "",
    "backend_twitter_app_secret" => "",
    "backend_twitter_app_redirect_url" => "",
    "backend_twitter_app_scope" => "",

    /* Backend Line Authentication App Configurations */
    "backend_line_app_id" => "",
    "backend_line_app_secret" => "",
    "backend_line_app_redirect_url" => "",
    "backend_line_app_scope" => "", //openid,profile

    /* Backend Instagram Authentication App Configurations */
    "backend_instagram_app_id" => "",
    "backend_instagram_app_secret" => "",
    "backend_instagram_app_redirect_url" => "",
    "backend_instagram_app_scope" => "",

    /* Backend Microsoft Authentication App Configurations */
    "backend_microsoft_app_id" => "",
    "backend_microsoft_app_secret" => "",
    "backend_microsoft_app_redirect_url" => "",
    "backend_microsoft_app_scope" => "",

    /* Commerce */
    "size_unit" => "cm", //cm (Centimeter), m (Metre), in (Inch), ft (Foot)
    "weight_unit" => "g", //G (Gram), kg (Kilogram), lb (Pound)
    "currency" => "THB", //USD, EUR, AUD, CAD, CNY, JPY, KRW, HKD, THB
    "vat_amount" => "", //VAT percent

    "project_api_key" => "",
    "project_api_secret" => "",

    "version" => "1.0",
);

/* Database details */
$database_access = array(

    "host" => "localhost",
    "name" => "starter",
    "login" => "root",
    "password" => "1234",

);

/* DEVELOPER MODE */

/* DEVELOPER MODE: Configurations */
$dev_configs = array(

    "ipaddress" => "127.0.0.1",
    "base_url" => "http://localhost/starter",

);

/* DEVELOPER MODE: Database details */
$dev_database_access = array(

    "host" => "127.0.0.1",
    "name" => "starter",
    "login" => "root",
    "password" => "1234",

);
